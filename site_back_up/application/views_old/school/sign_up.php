<!DOCTYPE html>
<html lang="en">
    <head>

        <title>School Sign Up</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <link href="<?php echo base_url(); ?>_css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>_css/bootstrap_3.4.1.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>_css/jquery-ui.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>_css/school.css" rel="stylesheet" type='text/css'>


        <script src="<?php echo base_url(); ?>_js/jquery-3.4.0.min.js"></script>          
        <script src="<?php echo base_url(); ?>_js/jquery.validationEngine-en.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validationEngine.js" type="text/javascript"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>_js/jquery-ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>_js/bootstrap_3.4.1.min.js" type="text/javascript"></script>



        <script type="text/javascript" >
            $(document).ready(function () {
                $("#reg_form").validationEngine({promptPosition: "bottomLeft", scroll: true});
            });



        </script>   
    <div class="Content_inner">

        <div class="container">
            <div class="row">
                <div class="col-lg-12 Container_registration_form">
                    <div class="registration_form">

                        <h3 class="heading3">Sugn Up Form</h3>



                        <form class="forms" id="reg_form" method="post" action="<?php echo base_url(); ?>school/main/createSchool">
                            <div class="home_parent_style">

                                <div class="form-group shadow_style">
                                    <input class="form-control validate[required]" placeholder="Name" name="name"  type="text">
                                </div>
                                <div class="form-group shadow_style">
                                    <input class="form-control validate[required]" placeholder="Contact Person" name="contact_person_name"  type="text">
                                </div>
                                <div class="form-group shadow_style">
                                    <input class="form-control validate[required]" placeholder="Mobile Number" name="mobile_no" type="text">
                                </div>
                                <div class="input-group exter_class1">
                                    <input class="form-control validate[required]" placeholder="Email Address " name="email_address" type="text">
                                </div>
                                <div class="form-group shadow_style">
                                    <input class="form-control validate[required]" placeholder="Password" name="password" type="password">
                                </div>
                                <div class="form-group shadow_style">
                                    <input class="form-control validate[required]" placeholder="Address" name="address" type="text">
                                </div>

                                
                                <div class="button__container">
                                    <input type="submit" value="Proceed" name="submit" class="btn btn-success">

                                </div>
                            </div>


                        </form> 

                    </div>
                </div>
            </div>
        </div>
    </div>