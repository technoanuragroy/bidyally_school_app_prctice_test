<?php $this->load->view("school/_include/school_header"); ?>
<link href="<?php echo base_url(); ?>css/admin_dashboard.css" rel="stylesheet">
<script type="text/javascript" >
    $(document).ready(function () {
        $("#frmRegister").validationEngine({promptPosition: "bottomLeft", scroll: true});

//        $("#frmRegister").submit(function(e) { 
//            var username = $("#username").val();
//            check_username(username);
//            
//            if($("#username_error").val() != "") {  
//                
//                var error = $("#username_error").val();
//                $('#username').validationEngine('showPrompt', error, 'red', 'bottomLeft', 1);   
//                $('html,body').animate({
//                    scrollTop: $("#username").offset().top},
//                'slow');
//                return false;
//            } else {
//                return true;
//            }
//        });
    });

    function check_username(username) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/user/validate_admin",
            data: "username=" + username,
            success: function (msg) {
                if (msg != "") {
                    $('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $("#username_error").val(msg);
                } else {
                    $("#username_error").val("");
                }
            }
        });
    }
</script>        

<?php echo form_open_multipart('', array('id' => 'frmRegister')); ?>
<div class="contentHeader">
    <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
        Edit Teacher / 
    </div>
    <div class="name_container">
        <div class="pageheadingContainer">
            <h1 class="Heading03">Edit teacher details</h1>
        </div>
    </div>
</div>
<!--                    <div class="topButtonset">
                        <span class="buttonMargin"><a href="<?php echo base_url(); ?>user/manage_admins" class="Button06">Back &nbsp;<i class="fa fa-angle-left fa-lg" aria-hidden="true"></i></a></span>
                    </div>-->
<div class="rowContent rowFirstContent">

    <div class="col-lg-12">
        <div class="rightContainer">
            <div class="invalid">
                <?php if ($this->session->flashdata("e_message")) {
                    echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
                } ?>
            </div>
            <div class="sucess">
<?php if ($this->session->flashdata("s_message")) {
    echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
} ?>
            </div>

            <!--                    <div class="form-group full-col">-->
            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Name</label>
                <input required type="text" class="form-control validate[required]" name="name" id="name" placeholder="Name" value="<?php echo $teacher['name']; ?>">
            </div>
            <div class="right-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Phone Number(Username)</label>
                <input type="number" class="form-control validate[required,phone]" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $teacher['username']; ?>" readonly=""  onBlur="return check_username(this.value);">
            </div>
           
            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Email</label>
                <input type="email" class="form-control validate[required,custom[email]]" name="email" id="email" placeholder="Account email" value="<?php echo $teacher['email']; ?>">
                
            </div>
             <div class="right-col mb-col-left">
                <label class="label-form">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" value="" autocomplete="new-password">
            </div>

            <div class="left-col mb-col-left" >
                <label class="label-form"><span class="symbolcolor">*</span>Status</label>
                <select name="status" class="form-control" id="status">
                    <option value="1" <?php if($teacher['status'] == 1){echo "selected";} ?>>Active</option>
                    <option value="0" <?php if($teacher['status'] == 0){echo "selected";} ?>>Inactive</option>
                </select>
            </div>
            <div class="right-col mb-col-left" style="margin-top: 16px !important;" >
                <label class="label-form"><span class="symbolcolor">*</span>Uplod profile picture</label>
                <input type="file" name="adminphoto" id="adminphoto">
            </div>
            

            <span class="buttonSbmit">
                 <input type="hidden" name="teacher_id" value="<?php echo $teacher['id']; ?>">
                <input type="submit" name="submit" value="Register" class="submitButton">
            </span>
        </div>
    </div>
</div>      
<?php echo form_close(); ?>

<?php $this->load->view("school/_include/school_footer"); ?>
