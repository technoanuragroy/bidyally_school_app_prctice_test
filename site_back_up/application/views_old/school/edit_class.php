<?php $this->load->view("school/_include/school_header"); ?>
<link href="<?php echo base_url(); ?>_css/admin_dashboard.css" rel="stylesheet">
<script type="text/javascript" >
    $(document).ready(function () {
        $("#frmRegister").validationEngine({promptPosition: "bottomLeft", scroll: true});

    });


</script>        

<?php echo form_open_multipart('', array('id' => 'frmRegister')); ?>
<div class="contentHeader">
    <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
        Edit class / 
    </div>
    <div class="name_container">
        <div class="pageheadingContainer">
            <h1 class="Heading03">Edit class details</h1>
        </div>
    </div>
</div>
<!--                    <div class="topButtonset">
                        <span class="buttonMargin"><a href="<?php echo base_url(); ?>user/manage_admins" class="Button06">Back &nbsp;<i class="fa fa-angle-left fa-lg" aria-hidden="true"></i></a></span>
                    </div>-->
<div class="rowContent rowFirstContent">

    <div class="col-lg-12">
        <!--                    <div class="rightContainer">-->
        <div class="invalid">
            <?php
            if ($this->session->flashdata("e_message")) {
                echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
            }
            ?>
        </div>
        <div class="sucess">
            <?php
            
            if ($this->session->flashdata("s_message")) {
                echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
            }
            ?>
        </div>
        

        <!--                    <div class="form-group full-col">-->
        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Class Name</label>
            <input required type="text" class="form-control validate[required]" name="class_name" id="name" placeholder="Class Name" value="<?php echo $class['class_name']; ?>">
        </div>
        <br><br><br><br><br>
        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Status</label>
            <select name="status" class="form-control validate[required]">
                <option value="1" <?php if($class['status'] == 1){echo "selected";} ?>>Active</option>
                <option value="0" <?php if($class['status'] == 0){echo "selected";} ?>>Inactive</option>
            </select>
        </div>
        <br><br><br><br><br>
        <div class="left-col mb-col-left">
            <span class="buttonSbmit" style="float: left !important;">
                <input type="hidden" name="class_id" value="<?php echo $class['id']; ?>">
                <input type="submit" name="submit" value="Submit" class="submitButton" >
            </span>
        </div>
    </div>
</div>      
<?php echo form_close(); ?>

<?php $this->load->view("school/_include/school_footer"); ?>

