<?php $this->load->view("school/_include/school_header"); ?>
<script type="text/javascript" >
    $(document).ready(function () {
        $("#showsearch").click(function () {
            $("#searchSection").slideToggle();
        });
    });

    function confirm_delete() {

        var check = confirm("Are you sure that you want to delete this class?");
        if (check == true) {
            return true;
        } else {
            return false;
        }
    }
</script>

<div class="ContainerList">
    <div class="contentHeader">
        <!--        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">-->
        <div class="col-md-4 no_padding_left form-group">
            <h3 class="Heading03 MBheadng03">Manage Classes</h3>
        </div>

        <!-- <div class="col-lg-4 no_padding_left form-group">

        </div> -->

        <div class="col-md-8 no_padding_left form-group">
            <span class="buttoncontainer">
                <span class="buttonSbmit" style="display:inline-block; float:none;">
                    <input id="showsearch" value="Search" class="submitButton" type="Button">
                </span>
                <span class="buttonMargin"><a href="<?php echo base_url(); ?>school/user/addClass" class="add_button Button09">Add Classes</a></span>
            </span>
        </div>
    </div>

    <div class="ListDataContainer">

        <div class="invalid">
            <?php
            if ($this->session->flashdata("e_message")) {
                echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
            }
            ?>
        </div>
        <div class="sucess">
            <?php
            if ($this->session->flashdata("s_message")) {
                echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
            }
            ?>
        </div>

        <div id="searchSection" style="display: none;">
            <div class="col-md-12 col-lg-5 no_padding_left form-group">
                <form action="<?php echo base_url() ?>admin/user/manage_admins" id="searchForm" method="post" accept-charset="utf-8">
                    <div class="col-lg-8 no_padding_left form-group">
                        <label class="label-form"><span class="symbolcolor">*</span>Admin type</label>
                        <select name="type" class="form-control validate[required]">
                            <option value="0">ALL</option>
                            <option value="1">Super admin</option>
                            <option value="2">Admin</option>
                            <option value="3">Operator</option>
                        </select>
                    </div>
                    <span class="buttonSbmit" style="margin-top:10px; float:left;">
                        <input id="search" name="search" value="Go" class="submitButton" type="submit">
                    </span>
                </form>
            </div>
            <div class="col-lg-6 no_padding_left form-group">

            </div>
        </div>

        <div class="form-group full-col table-responsive">
            <table class="data-table table table-striped">
                <thead>
                    <tr>
                        <th>Class name</th>
                        <th>Status</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //echo "<pre>";print_r($classes);
                    if (!empty($classes)) {
                        foreach ($classes as $row) {


                            //$encrypted = $this->my_custom_functions->ablEncrypt($admin['admin_id']);
                            $encrypted = $row['id'];
                            ?>
                            <tr>
                                <td><?php echo $row['class_name']; ?></td>
                                <td><?php
                                    if ($row['status'] == 1) {
                                        echo "Active";
                                    } else {
                                        echo "Inactive";
                                    }
                                    ?></td>
                                <td>                                    
                                    <a href="<?php echo base_url() . 'school/user/editClass/' . $encrypted; ?>" title="Edit">
                                        <img src="<?php echo base_url(); ?>_images/edit.png">
                                    </a>
                                </td>
                                <td>         

                                    <a href="<?php echo base_url() . 'school/user/deleteClass/' . $encrypted; ?>" title="Delete" onclick="return confirm_delete();">
                                        <img src="<?php echo base_url(); ?>_images/del.png" alt="Delete">
                                    </a>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>
                <div class="pagination"><?php echo $this->pagination->create_links(); ?></div>

            <?php } else { ?>
                <tr>
                    <td colspan="9">No classes found</td>
                </tr>
                <?php
            }
            ?>

        </div>
    </div>
</div>

<?php $this->load->view("school/_include/school_footer"); ?>
