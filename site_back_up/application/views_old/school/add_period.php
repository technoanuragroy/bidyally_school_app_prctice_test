<?php $this->load->view("school/_include/school_header"); ?>
<link href="<?php echo base_url(); ?>_css/admin_dashboard.css" rel="stylesheet">
<script type="text/javascript" >
    $(document).ready(function () {
        $("#frmRegister").validationEngine({promptPosition: "bottomLeft", scroll: true});




        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 5,
            minTime: '10',
            maxTime: '6:00pm',
            defaultTime: '10',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });



    });


</script>        

<?php echo form_open_multipart('', array('id' => 'frmRegister')); ?>
<div class="contentHeader">
    <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
        Create Period / 
    </div>
    <div class="name_container">
        <div class="pageheadingContainer">
            <h1 class="Heading03">Enter Period details</h1>
        </div>
    </div>
</div>
<!--                    <div class="topButtonset">
                        <span class="buttonMargin"><a href="<?php echo base_url(); ?>user/manage_admins" class="Button06">Back &nbsp;<i class="fa fa-angle-left fa-lg" aria-hidden="true"></i></a></span>
                    </div>-->
<div class="rowContent rowFirstContent">

    <div class="col-lg-12">
        <!--                    <div class="rightContainer">-->
        <div class="invalid">
            <?php
            if ($this->session->flashdata("e_message")) {
                echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
            }
            ?>
        </div>
        <div class="sucess">
            <?php
            if ($this->session->flashdata("s_message")) {
                echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
            }
            ?>
        </div>

        <!--                    <div class="form-group full-col">-->
<!--        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Select Class</label>
            <select name="class_id" class="form-control validate[required]">
                <option value="">Select Class</option>
                <?php //foreach($class_list as $classes){ ?>
                <option value="<?php //echo $classes['id']; ?>"><?php //echo $classes['class_name']; ?></option>
                <?php //} ?>
            </select>
        </div>
        <br><br><br><br><br>-->
        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Period Name</label>
            <input required type="text" class="form-control validate[required]" name="period_name" id="name" placeholder="Period Name" value="">
        </div>
        <br><br><br><br><br>
        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Period Start Time</label>
            <input required type="text" class="form-control validate[required] timepicker" name="period_start_time" id="period_start_time" placeholder="Period Start Time" value="">
        </div>
        <br><br><br><br><br>
        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Period End Time</label>
            <input required type="text" class="form-control validate[required] timepicker" name="period_end_time" id="period_end_time" placeholder="Period End Time" value="">
        </div>
        <br><br><br><br><br>
        <div class="left-col mb-col-left">
            <label class="label-form"><input type="checkbox" name="lunch_break" class="" value="1"> Mark as Lunch break</label>
            
        </div>
        <br><br><br>
        <div class="left-col mb-col-left">
            <label class="label-form"><span class="symbolcolor">*</span>Status</label>
            <select name="status" class="form-control validate[required]">
                <option value="1">Active</option>
                <option value="0">Inactive</option>
            </select>
        </div>
        <br><br><br><br><br>
        <div class="left-col mb-col-left">
            <span class="buttonSbmit" style="float: left !important;">
                <input type="submit" name="submit" value="Submit" class="submitButton" >
            </span>
        </div>
    </div>
</div>      
<?php echo form_close(); ?>

<?php $this->load->view("school/_include/school_footer"); ?>

