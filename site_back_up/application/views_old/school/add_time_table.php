<?php $this->load->view("school/_include/school_header"); ?>
<link href="<?php echo base_url(); ?>css/admin_dashboard.css" rel="stylesheet">
<script type="text/javascript" >
    $(document).ready(function () {
        $("#frmRegister").validationEngine({promptPosition: "bottomLeft", scroll: true});
        
        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 5,
            minTime: '10',
            maxTime: '6:00pm',
            defaultTime: '10',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

    });

    function get_section_list() {
        var class_id = $('.class').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });

        //get_subject_list_classwise(class_id);
    }

//    function get_subject_list_classwise(class_id){
//    $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>school/user/get_subject_list_classwise",
//            data: "class_id=" + class_id,
//            success: function (msg) {
//                if (msg != "") {
//                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
//                    $(".subject").html(msg);
//                } else {
//                    $(".subject").html("");
//                }
//            }
//        });
//    }
</script>        

<?php echo form_open_multipart('', array('id' => 'frmRegister')); ?>
<div class="contentHeader">
    <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
        Create Time Table / 
    </div>
    <div class="name_container">
        <div class="pageheadingContainer">
            <h1 class="Heading03">Enter time table details</h1>
        </div>
    </div>
</div>
<!--                    <div class="topButtonset">
                        <span class="buttonMargin"><a href="<?php echo base_url(); ?>user/manage_admins" class="Button06">Back &nbsp;<i class="fa fa-angle-left fa-lg" aria-hidden="true"></i></a></span>
                    </div>-->
<div class="rowContent rowFirstContent">

    <div class="col-lg-12">
        <div class="rightContainer">
            <div class="invalid">
                <?php
                if ($this->session->flashdata("e_message")) {
                    echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
                }
                ?>
            </div>
            <div class="sucess">
                <?php
                if ($this->session->flashdata("s_message")) {
                    echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
                }
                ?>
            </div>

            <!--                    <div class="form-group full-col">-->
            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Day</label>
                <?php $day_list = $this->config->item('days_list'); ?>
                <select name="day_id" class="form-control validate[required]">
                    <option value="">Select Day</option>
                    <?php foreach ($day_list as $kkey => $daylist) { ?>
                        <option value="<?php echo $kkey; ?>"><?php echo $daylist; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="right-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Select Class</label>
                <select name="class_id" class="form-control validate[required] class" onchange="get_section_list()">
                    <option value="">Select Class</option>
                    <?php foreach ($class_list as $class) { ?>
                        <option value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Select Section</label>
                <select name="section_id" class="form-control validate[required] section">
                    <option value="">Select Section</option>
                </select>

            </div>
            <!--             <div class="right-col mb-col-left">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Period</label>
                            <select name="period_id" class="form-control validate[required]">
                                 <option value="">Select Period</option>
            <?php
//            foreach ($period_list as $period) {
//                $start_time = date('h:i A', strtotime($period['period_start_time']));
//                $end_time = date('h:i A', strtotime($period['period_end_time']));
                ?>
                                        <option value="<?php //echo $period['id']; ?>"><?php //echo $period['period_name'] . ' (' . $start_time . ' - ' . $end_time . ')'; ?></option>
            <?php //} ?>
                            </select>
                        </div>-->
            <div class="right-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Enter Period label</label>
                <input type="text" name="period_name" class="form-control">
            </div>
            
            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Period Start Time</label>
                <input required type="text" class="form-control validate[required] timepicker" name="period_start_time" id="period_start_time" placeholder="Period Start Time" value="">
            </div>
            <div class="right-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Perion End Time</label>
                <input required type="text" class="form-control validate[required] timepicker" name="period_end_time" id="period_end_time" placeholder="Period End Time" value="">
            </div>

            <div class="left-col mb-col-left" >
                <label class="label-form">Select Subject</label>
                <select name="subject_id" class="form-control subject">
                    <option value="">Select Subject</option> 
                    <?php foreach ($subject_list as $subject) { ?>
                        <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                    <?php } ?>

                </select>
            </div>
            <div class="right-col mb-col-left" >
                <label class="label-form">Select Teacher</label>
                <select name="teacher_id" class="form-control">
                    <option value="">Select Teacher</option>
                    <?php foreach ($teacher_list as $teacher) { ?>
                        <option value="<?php echo $teacher['id']; ?>"><?php echo $teacher['name']; ?></option>
                    <?php } ?>
                </select>
            </div>


            <span class="buttonSbmit">
                <input type="submit" name="submit" value="Save" class="submitButton">
            </span>
        </div>
    </div>
</div>      
<?php echo form_close(); ?>

<?php $this->load->view("school/_include/school_footer"); ?>
