<?php $this->load->view("school/_include/school_header"); ?>
<script type="text/javascript" >
    $(document).ready(function () {
        $("#showsearch").click(function () {
            $("#searchSection").slideToggle();
        });
        
        
        
        var class_id = $('.class').val();
        var sec_id = $('.post_sec_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: {
                'class_id': class_id,
                'sec_id': sec_id
            },
            success: function (msg) {
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });
        
        
    });

    function confirm_delete() {

        var check = confirm("Are you sure that you want to delete this subject?");
        if (check == true) {
            return true;
        } else {
            return false;
        }
    }

    function get_section_list() {
        var class_id = $('.class').val();
        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });


    }
</script>

<div class="ContainerList">
    <div class="contentHeader">
        <!--        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">-->
        <div class="col-md-4 no_padding_left form-group">
            <h3 class="Heading03 MBheadng03">Manage Time Table</h3>
        </div>

        <!-- <div class="col-lg-4 no_padding_left form-group">

        </div> -->

        <div class="col-md-8 no_padding_left form-group">
            <span class="buttoncontainer">
                <span class="buttonSbmit" style="display:inline-block; float:none;">
                    <input id="showsearch" value="Search" class="submitButton" type="Button">
                </span>
                <span class="buttonMargin"><a href="<?php echo base_url(); ?>school/user/addTimeTable" class="add_button Button09">Add Time Table</a></span>
            </span>
        </div>
    </div>

    <div class="ListDataContainer">

        <div class="invalid">
            <?php
            if ($this->session->flashdata("e_message")) {
                echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
            }
            ?>
        </div>
        <div class="sucess">
            <?php
            if ($this->session->flashdata("s_message")) {
                echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
            }
            ?>
        </div>
        <?php //echo "<pre>";print_r($post_data); ?>
        <div id="searchSection" style="display: block;">
            <div class="col-md-12 col-lg-5 no_padding_left form-group">
                <form action="<?php echo current_url(); ?>" id="searchForm" method="post" accept-charset="utf-8">
                    <div class="col-lg-8 no_padding_left form-group">
                        <label class="label-form"><span class="symbolcolor">*</span>Select Class</label>
                        <select name="class_id" class="form-control validate[required] class" onchange="get_section_list();">
                            <option value="">Select Class</option>
                            <?php 
                            $selected = '';
                            foreach ($class_list as $class) { 
                                if($post_data['class_id'] != ''){
                                    if($post_data['class_id'] == $class['id']){
                                        
                                        $selected = 'selected="selected"';
                                    }else{
                                        
                                        $selected = '';
                                    }
                                }
                                
                                ?>
                                <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-lg-8 no_padding_left form-group">
                        <label class="label-form"><span class="symbolcolor">*</span>Select Section</label>
                        <select name="section_id" class="form-control validate[required] section">
                            <option value="">Select Section</option>
                        </select>
                        <input type="hidden" name="post_sec_id" class="post_sec_id" value="<?php if($post_data['section_id'] != ''){echo $post_data['section_id'];} ?>">
                    </div>
                    <span class="buttonSbmit" style="margin-top:10px; float:left;">
                        <input id="search" name="submit" value="Go" class="submitButton" type="submit">
                    </span>
                </form>
            </div>
            <?php //echo "<pre>";print_r($period); ?>
            <div class="col-lg-6 no_padding_left form-group">

            </div>
        </div>

        <div class="form-group full-col table-responsive">
            <table class="data-table table table-striped">
<!--                <thead>
                    <tr>
                        <th></th>
                        <?php //foreach ($period_list as $p_list) { ?>
                            <th><?php //echo $p_list['period_name']; ?></th>
                        <?php //} ?>
                    </tr>
                </thead>-->
                <tbody>
                    <?php
                    $day_list = $this->config->item('days_list');
                    if (!empty($period)) {
                        foreach ($period as $day => $row) {
                            ?>
                            <tr>
                                <td><?php echo $day_list[$day]; ?></td>
                                <?php foreach ($row as $subject_id=>$p_list) { ?>
                                    <td><?php
                                    
                                        echo "<b>".$p_list['period_name']."</b><br>";
                                        if($p_list['subject_id'] != 0){
                                        echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $p_list['subject_id'] . '"');
                                        echo "<br>";
                                        }
//                            echo "T - ".$this->my_custom_functions->get_particular_field_value(TBL_TEACHER,'name','and id = "'.$period_data['teacher_id'].'"');
                                        if($p_list['teacher_id'] != 0){
                                        $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $p_list['teacher_id'] . '"');
                                        $break_teacher_name = explode(' ', trim($teacher_name));

                                        $length = count($break_teacher_name);
                                        $join_teacher_name = '';
                                        for ($i = 0; $i < $length; $i++) {

                                            $join_teacher_name .= substr($break_teacher_name[$i], 0, 1) . '.';
                                        }

                                        $final_name = rtrim($join_teacher_name, '.');
                                        echo '('.$final_name.')';
                                        }else{
                                            echo "Tiffin Break";
                                        }
                                        echo "<br>";
                                        echo '('.date('h:i A',strtotime($p_list['period_start_time'])).' - '.date('h:i A',strtotime($p_list['period_end_time'])).')';
                                         echo "<br>";
                                        
                                        echo '<a href="' . base_url() . 'school/user/editTimeTable/' . $p_list['id'] . '">Edit</a>';
                                    
                                    ?></td>
                                    <?php } ?>


                            </tr>
                        <?php }
                        ?>
                    </tbody>
                </table>


            <?php } else { ?>
                <tr>
                    <td colspan="9">No other admin found</td>
                </tr>
                <?php
            }
            ?>

        </div>
    </div>
</div>

<?php $this->load->view("school/_include/school_footer"); ?>
