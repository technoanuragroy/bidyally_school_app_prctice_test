<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">    
    $(document).ready(function(){
        $("#passwordchange_form").validationEngine({promptPosition : "bottomLeft", scroll: true});
    });        
</script>

<?php echo form_open('', array('name' => 'passwordchange_form', 'id' => 'passwordchange_form', 'onsubmit' => 'isSubmit(); return false;')); ?>               
            
            <div class="ContainerList">
                <div class="contentHeader">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 noPadding">
                        <h3 class="Heading03 MBheadng03">Change password here</h3>
                    </div>                             
                </div>
            
                <div class="ListDataContainer"> 
            
                    <div class="invalid">
                        <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>                            
                    </div>
                    <div class="sucess">
                        <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                    </div>

                    <div class="form-group full-col">
                        <label class="label-form"><span class="symbolcolor">*</span>Old Password</label>
                        <?php echo form_password(array("name" => "o_password", "id" => "o_password", "class" => "form-control validate[required]", "placeholder" => "Old password")); ?>                                   
                    </div>

                    <div class="form-group full-col">
                        <label class="label-form"><span class="symbolcolor">*</span>New Password</label>
                        <?php echo form_password(array("name" => "n_password", "id" => "n_password", "class" => "form-control validate[required,minSize[6]]", "placeholder" => "New password")); ?>                                    
                    </div>

                    <div class="form-group full-col">
                        <label class="label-form"><span class="symbolcolor">*</span>Confirm New Password</label>
                        <?php echo form_password(array("name" => "c_password", "id" => "c_password", "class" => "form-control validate[required,minSize[6],equals[n_password]]", "placeholder" => "Confirm new password")); ?>
                    </div>

                    <span class="buttonSbmit">                                              
                         <input type="submit" name="change_password" id="change_password" value="Update" class="submitButton">                        
                    </span>                                                              
                </div>   
            </div>
<?php echo form_close(); ?>          
        
<?php $this->load->view("admin/include/footer"); ?>