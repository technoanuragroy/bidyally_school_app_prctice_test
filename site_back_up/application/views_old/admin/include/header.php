<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo SITE_NAME; ?>: Admin Panel</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <link href="<?php echo base_url(); ?>_css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>_css/bootstrap_3.4.1.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>_css/jquery-ui.css" rel="stylesheet" type='text/css'>
<!--        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" type='text/css'>-->
        <link href="<?php echo base_url(); ?>_css/style.css" rel="stylesheet" type='text/css'>
<!--        <link href="<?php echo base_url(); ?>css/jquery.timepicker.css" rel="stylesheet" type='text/css'>-->



        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


        <script src="<?php echo base_url(); ?>_js/jquery-3.4.0.min.js"></script>          
        <script src="<?php echo base_url(); ?>_js/jquery.validationEngine-en.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validationEngine.js" type="text/javascript"></script>
<!--        <script src="<?php echo base_url(); ?>editor/scripts/innovaeditor.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery.tablesorter.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery.timepicker.js" type="text/javascript"></script>-->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>_js/jquery-ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>_js/bootstrap_3.4.1.min.js" type="text/javascript"></script>


        <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.js" type="text/javascript"></script>

        <script type="text/javascript">
            var tdBefore;
            $(document).ready(function() {

                $(".displaydiv").click(function() {
                    $(this).toggleClass("triangle1 triangle");
                    $(".togDivcontainer").slideToggle(300);
                });

                //$(".datepicker").attr("readonly", "true");
                
                $("table.data-table th").each(function(i,e) {                     
                    tdBefore = $(this).text();
                    $(this).parent().parent().parent().find("tr").each(function() {
                        $(this).find("td").eq(i).attr('data-before',tdBefore);
                    });
                    //$(this).attr('data-before',$(this).text());
                    //$(this).parent().parent().parent().find("tr td").eq(i).attr('data-before',$(this).text());                    
                });
            });
        </script>
        <script type="text/javascript">
                setTimeout(function() {
                    $('.s_message').hide('slow');
                }, 7000);
                setTimeout(function() {
                    $('.e_message').hide('slow');
                }, 7000);
        </script>
    </head>

    <body class="Admin-bg CreatAccount_bg">
        <?php if($this->uri->segment(3) == "edit_admin" || $this->uri->segment(3) == "edit_profile"){ ?>
        <div class="wrapper">
                <div id="confirmBox1" class="confirmBox">
                             <div class="alert_header"><h1></h1></div>
                             <div class="confirm_msg"><span id="confirm_msg"></span></div>

                             <div class="confirm_button_container">
                                 <span class="buttonSbmit yes buttonSbmitMargin red_danger_container" ><button name="yes" class="submitButton yes_button red_danger" style="background: red;">Yes, Delete</button></span>
                                 <span class="buttonSbmit no" ><button name="no" class="submitButton no_button" >No</button></span>
                             </div>

                             <span class="caution_container"></span>
                         </div>
        <?php } ?>
        <div class="full-wrapper">
            <div class="main-header">
                <div class="container-fluid mobile_header">
                    <div class="col-sm-3 header_left">
                            <a class="logo" href="#">
                                <span class="logo-lg" style="padding:4px 0 4px;">
                                    <img src="<?php echo base_url();?>_images/admin_logo.png" alt="logo" style="width: 128px;">
                                </span>
                            </a>
                    </div>

                <?php if($this->session->userdata('admin_id') && $this->session->userdata('admin_id') != "") { ?>
                        <div class="col-sm-9 acc-profile">
                            <ul class="nav navbar-nav navbar-right">
                                  <li class="">
                                      <a aria-expanded="false" data-toggle="dropdown" class="user-profile dropdown-toggle" href="javascript:;">

                                    <span class="profilePhoto">
                                        <?php $filename = 'uploads/admin/'.$this->session->userdata['admin_id'].'.jpg';
                                        if(file_exists($filename)){ ?>
                                        <img alt="Profile Photo" src="<?php echo base_url().$filename; ?>">
                                       <?php }else{ ?>
                                        <img alt="Profile Photo" src="<?php echo base_url(); ?>images/profile_pic.png">
                                       <?php }?>
                                    </span>
                                          <span class="ProfileName"><?php echo Ucfirst($this->session->userdata('admin_username')); ?> &nbsp;<i class="fa fa-angle-down" style="font-size: 18px; position: relative; top: 2px;" aria-hidden="true"></i></span>
                                      </a>
                                      <ul class="dropdown-menu dropdown-usermenu pull-right">
                                            <li><a href="<?php echo base_url(); ?>admin/user/change_password">Change Password</a></li>
                                            <li><a href="<?php echo base_url(); ?>admin/user/edit_profile">Edit Admin Profile</a></li>
                                            <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Logout</a></li>
                                      </ul>
                                  </li>
                            </ul>
                         </div>
                <?php } ?>

              </div>
            </div>

            <?php if($this->session->userdata('admin_id') && $this->session->userdata('admin_id') != "") { ?>
                    <nav class="navbar navbar-default sidebar" role="navigation">
<!--                        <span class="text_navi">Navigation</span>-->
                        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="navbarCollapse">
                                <ul class="nav navbar-nav">
                                      <li <?php if(isset($active_menu) && $active_menu == "") { echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>admin">Home</a></li>
                                      <?php //if($this->session->userdata('admin_type') == 1) { ?>
                                                  <li <?php if(isset($active_menu) && $active_menu == "manage_admins") { echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>admin/user/manage_admins">Manage Admins</a></li>
                                      <?php //} ?>
                                      <li <?php if(isset($active_menu) && $active_menu == "manage_companies") { echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>admin/user/manage_companies">Manage Schools</a></li>
      
                                      
                                      </ul>
                            </div>
                        </div>
                    </nav>
            <?php } ?>


            <div class="right_col">

                <div class="container-fluid noPadding">
