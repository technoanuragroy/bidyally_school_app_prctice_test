<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
    <head>
        <title>School Admin Panel</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <link href="<?php echo base_url(); ?>_css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>_css/bootstrap_3.4.1.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>_css/jquery-ui.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>_css/font-awesome.min.css" rel="stylesheet" type='text/css'>  
<!--        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type='text/css'> -->
        <link href="<?php echo base_url(); ?>_css/login_style.css" rel="stylesheet" type='text/css'>
                                       
        
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
        
        <script src="<?php echo base_url(); ?>_js/jquery-3.4.0.min.js"></script>          
        <script src="<?php echo base_url(); ?>_js/jquery.validationEngine-en.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validationEngine.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>_editor/scripts/innovaeditor.js" type="text/javascript"></script>
                
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>_js/jquery-ui.js" type="text/javascript"></script>        
        <script src="<?php echo base_url(); ?>_js/bootstrap_3.4.1.min.js" type="text/javascript"></script>
 

        <script type="text/javascript" >
            $(document).ready(function(){
                $("#frmLogin").validationEngine({promptPosition : "bottomLeft", scroll: true});
            });
            setTimeout(function() {
                $('.s_message').hide('slow');
                }, 5000);
                setTimeout(function() {
                $('.e_message').hide('slow');
                }, 5000);
        </script>
    </head>
    <body id="CreatAccount_bg" class="CreatAccount_bg">
        <div class="LoginContainer">
            <span class="LogoContainer">
                <h1>Learning School</h1>
<!--                <img src="<?php //echo base_url();?>images/logo.png" alt="logo">-->
            </span>
                    <div class="AccountContent">
                        <div class="content_boxshadw">
                            <div class="HeadingDiv"><h1 class="heading">Login</h1></div>
                                <?php echo form_open('', array('id' => 'frmLogin')); ?>
                                    <div class="LoginformContent">                    
                                        <div class="container-form">
                                            <div class="invalid">
                                                <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>                            
                                            </div>
                                            <div class="sucess">
                                                <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                                            </div>        
                 
                                            <div class="form-group full-col">
                                                <label for="username" class="label-form">Username</label>
                                                <?php echo form_input(array('name' => 'username', 'id' => 'username', 'class' => 'form-control validate[required]', 'placeholder' => 'Enter username')); ?>
                                            </div>

                                            <div class="form-group full-col">
                                                <label for="password" class="label-form">Password</label>
                                                <?php echo form_password(array('name' => 'password', 'id' => 'password', 'class' => 'form-control validate[required]', 'placeholder' => 'Enter password')); ?>
                                            </div>

                                            <span class="buttonSbmit">                                                                                                
                                                <input type="submit" name="submit" value="Login" class="submitButton">
                                            </span>
                                        </div>                
                                    </div>
                                    <div class="loginfooter"> 
                                        
                                        <span class="loginfooterright"><a href="<?php echo base_url(); ?>admin/main/forgot_password" class="forget_pasword">Forgot Password ?</a></span>
                                    </div>     
                 
                      <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </body>
    
</html>