<?php $this->load->view("admin/include/header"); ?>
<link href="<?php echo base_url(); ?>_css/admin_dashboard.css" rel="stylesheet">
<script type="text/javascript" >
    $(document).ready(function () {
        $("#frmEdit").validationEngine({promptPosition: "bottomLeft", scroll: true});
    });
    function doConfirm(header, msg, caution_msg, yes_button, no_button, yesFn, noFn) {

        var confirmBox = $("#confirmBox1");
        confirmBox.find(".alert_header h1").html(header);
        confirmBox.find("#confirm_msg").html(msg);
        confirmBox.find(".caution_container").html(caution_msg);
        confirmBox.find(".yes_button").text(yes_button);
        confirmBox.find(".no_button").text(no_button);

        confirmBox.find(".yes,.no").unbind().click(function () {
            confirmBox.hide();
        });
        confirmBox.find(".yes").click(yesFn);
        confirmBox.find(".no").click(noFn);
        confirmBox.show();

        $('.overlay_back').addClass('fadeMe');
        $('#confirmBox1').addClass('overlay');
    }
    function deleteImg() {
        var confirm_header = "Delete profile picture";
        var confirmation_msg = "Are you sure you want to do so?";
        var caution_msg = "By clicking on Remove, image will be deleted.";
        var yes_button = "Yes, Remove";
        var no_button = "Go Back";

        doConfirm(confirm_header, confirmation_msg, caution_msg, yes_button, no_button, function yes() {
            window.location.href = '<?php echo base_url(); ?>admin/user/delete_profilePic/';
        }, function no() {

            $('.overlay_back').removeClass('fadeMe');
            $('#confirmBox1').removeClass('overlay');

            return false;
        });
        return false;
    }
</script>
<style>
    .icon_link > input {
        visibility:hidden;
        width:0;
        height:0
    }
</style>

<?php
//echo "<pre>";print_r($admin_details);

echo form_open_multipart('', array('id' => 'frmEdit'));
?>
<div class="contentHeader">
    <div style="font-size: 17px; color: #72838b; float: left; margin-top: 10px; padding-right: 5px;">
        <span class="Iconheading"><?php echo $admin_details['image']; ?></span><span><?php //echo $admin_details['account_type'];  ?>  </span>
    </div>
    <div class="name_container">
        <div class="pageheadingContainer">
            <h1 class="Heading03"><?php echo $admin_details['name']; ?></h1>
        </div>
    </div>
</div>
<?php //echo "<pre>"; print_r($admin_details);  ?>
<!--                    <div class="topButtonset">
                        <span class="buttonMargin"><a href="<?php echo base_url(); ?>user/manage_admins" class="Button06">Back &nbsp;<i class="fa fa-angle-left fa-lg" aria-hidden="true"></i></a></span>
                    </div>-->
<div class="rowContent rowFirstContent">
    <div class="col-md-12 col-lg-3">
        <h2 class="Heading02" style="font-weight: 700;">Profile picture</h2>
        <span class="subheading"></span>
        <?php $filename = 'uploads/admin/' . $admin_details['admin_id'] . '.jpg';
        if (file_exists($filename)) {
            ?>
            <div class="edit_photo_container">
                <div class="icon_delete_container">
                    <a href="#"  class="icon_link" onclick="deleteImg();" >
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                    <div class="icon_link">
                        <label for="file-input" >
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                        <input id="file-input"  type="file" name="adminphoto" id="adminphoto">
                    </div>
                </div>
                <img src="<?php echo base_url() . 'uploads/admin/' . $admin_details['admin_id'] . '.jpg' . '?' . time(); ?>">

            </div>
<?php } else { ?>

            <div class="edit_photo_container">
                <div class="icon_delete_container">
                    <div class="icon_link">
                        <label for="file-input" >
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                        <input id="file-input"  type="file" name="adminphoto" id="adminphoto">
                    </div>
                </div>
                <img src="https://cdn1.iconfinder.com/data/icons/metro-ui-dock-icon-set--icons-by-dakirby/256/User_No-Frame.png">


            </div>
<?php } ?>
    </div>
    <div class="col-lg-9">
        <div class="rightContainer">
            <div class="invalid">
                <?php if ($this->session->flashdata("e_message")) {
                    echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
                } ?>
            </div>
            <div class="sucess">
<?php if ($this->session->flashdata("s_message")) {
    echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
} ?>
            </div>
<?php //echo "<pre>"; print_r($admin_details);  ?>
            <!--                    <div class="form-group full-col">-->
            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Name</label>
                <input required type="text" class="form-control validate[required]" name="name" id="name" placeholder="Name" value="<?php echo $admin_details['name']; ?>">
            </div>
            <div class="right-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Phone</label>
                <input type="number" class="form-control validate[required,phone]" name="phone" id="phone" placeholder="Phone number" value="<?php echo $admin_details['phone']; ?>">
            </div>
            <div class="left-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Account Email</label>
                <input type="email" class="form-control validate[required,custom[email]]" name="email" id="email" placeholder="Account email" value="<?php echo $admin_details['email']; ?>">
            </div>

            <div class="right-col mb-col-left">
                <label class="label-form"><span class="symbolcolor">*</span>Username</label>
                <input type="text" class="form-control validate[required,minSize[5]]" name="username" id="username" placeholder="Enter username" value="<?php echo $admin_details['username']; ?>" readonly="true">
            </div>
            <div class="mb-col-left" id="companyListing">
                <label class="label-form"><span class="symbolcolor">*</span>Status</label>
                <select name="status" class="form-control" id="status">
                    <option value="1" <?php if ($admin_details['status'] == 1) { ?>selected="selected" <?php } ?>>Active</option>
                    <option value="0" <?php if ($admin_details['status'] == 0) { ?>selected="selected" <?php } ?>>Inactive</option>
                </select>
            </div>
            <br>

            <span class="buttonSbmit">
                <input type="submit" name="save" value="Update" class="submitButton">
            </span>
        </div>
    </div>
</div>      
<?php echo form_close(); ?>

<?php $this->load->view("admin/include/footer"); ?>
