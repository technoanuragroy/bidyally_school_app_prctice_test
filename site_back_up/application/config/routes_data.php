<?php

$route["admin"] = "admin/main";
$route["admin/logout"] = "admin/main/logout";
$route["admin/main/(:any).*"] = "admin/main/$1";


$route["school"] = "school/main";
$route["school/signUp"] = "school/main/signUp";
$route["school/login"] = "school/main/login";

$route["app"] = "app/index";
$route["teacher"] = "teacher/main";
$route["signUp"] = "app/signUp";
$route["enterOTP"] = "app/enterOTP";
$route["enterDetail"] = "app/enterDetail";
$route["forgetPassword"] = "app/forgetPassword";
$route["userResetPassword/(:any).*"] = "app/userResetPassword/$1";



 ?>
