<?php

$config['company_forgot_password'] = array(
    'subject' => SITE_NAME . ' password reset',
    'addressing_user' => 'Hi There,',
    'mail_body' => '<div style="text-align: left;">
                        Someone requested a new password for your ' . SITE_NAME . ' account.<br><a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:Lato,Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="%reset_link%" target="_blank">Reset Password</a>
                        <br><br>If you didn`t make this request then you can safely ignore this email.<br>
                    </div>',    
    'unsubscribe' => '%unsubscribe%'    
);
