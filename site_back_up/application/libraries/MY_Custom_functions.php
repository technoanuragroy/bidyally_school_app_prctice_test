<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Custom_functions {
    /* 	function __construct()
      {
      parent::__construct();
      } */

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function get_particular_field_value($tablename, $fieldname, $where = "") {

        $CI = & get_instance();
        $str = "select " . $fieldname . " from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = "";
        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $field_arr = explode(" as ", $fieldname);
                if (count($field_arr) > 1) {
                    $fieldname = $field_arr[1];
                } else {
                    $fieldname = $field_arr[0];
                }
                $record = $row[$fieldname];
            }
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function get_details_from_id($id = "", $table, $extra = array()) {

        $CI = & get_instance();
        if ($id != "") {
            $CI->db->where("id", $id);
        }
        if (count($extra) > 0) {
            $CI->db->where($extra);
        }
        $query = $CI->db->get($table);

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function get_multiple_data($table = "", $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $table . " where 1=1 " . $where;

        $query = $CI->db->query($str);

        $record = array();
        $count = 0;
        foreach ($query->result_array() as $row) {
            $record[$count] = $row;
            $count++;
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function get_perticular_count($tablename, $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = $query->num_rows();
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function insert_data($data, $table) {

        $CI = & get_instance();
        $CI->db->insert($table, $data);
        return 1;
    }

    function insert_data_last_id($data, $table) {

        $CI = & get_instance();
        $CI->db->insert($table, $data);
        return $CI->db->insert_id();
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function delete_data($table, $where = array()) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->delete($table);
        return 1;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function update_data($data, $table, $where) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->update($table, $data);
        return 1;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function propercase($string) {

        $string = ucwords(strtolower($string));

        foreach (array('-', '\'', '(', ',', ', ') as $delimiter) {
            if (strpos($string, $delimiter) !== false) {
                $string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
            }
        }
        return $string;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function aasort(&$array, $key) {

        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function encrypt_string($string) {

        return openssl_digest($string, 'sha512');
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function check_super_admin_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('super_admin_id')) {
            redirect('super_admin');
        }
    }

    public function check_admin_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('admin_id')) {
            redirect('admin');
        }
    }

    public function check_school_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('school_id')) {
            redirect('school');
        }
    }

    public function check_teacher_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('teacher_id')) {
            redirect('app');
        }
    }

    public function check_parent_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('parent_id')) {
            redirect('app');
        }
    }

    public function check_coach_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('coach_id')) {
            redirect('coach');
        }
    }

    public function check_client_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('client_id')) {
            redirect('client');
        }
    }

    public function check_rater_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('rater_id')) {
            redirect('rater');
        }
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function SendEmail($from, $to, $subject, $email_message, $attachment = "", $alternative_text = "") {

        $CI = & get_instance();

        $from_array = explode(',', $from);
        $from_email = $from_array[0];
        $from_name = $from_array[1];

        $CI->load->library("email");
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $CI->email->initialize($config);

        $CI->email->from($from_email, $from_name);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($email_message);

        if ($attachment != '') {
            $CI->email->attach($attachment);
        }

        if ($alternative_text != '') {
            $CI->email->set_alt_message($alternative_text);
        }

        $CI->email->send();
        return true;
    }

    /////////////////////////////////////////////////////////////
    //	Function to create safe alise
    /////////////////////////////////////////////////////////////

    public function CreateAlise($name) {

        $name = strtolower(trim($name));

        $name = str_replace("_", "-", $name);
        $name = str_replace("+", "-Plus", $name);

        $name = str_replace("~", "", $name);
        $name = str_replace("!", "", $name);
        $name = str_replace("@", "", $name);
        $name = str_replace("#", "", $name);
        $name = str_replace("$", "", $name);
        $name = str_replace("%", "", $name);
        $name = str_replace("^", "", $name);
        $name = str_replace("&amp;", "", $name);
        $name = str_replace("&", "n", $name);
        $name = str_replace("*", "", $name);
        $name = str_replace("(", "", $name);
        $name = str_replace(")", "", $name);
        $name = str_replace("=", "", $name);
        $name = str_replace("{", "", $name);
        $name = str_replace("}", "", $name);
        $name = str_replace("[", "", $name);
        $name = str_replace("]", "", $name);
        $name = str_replace("'", "", $name);
        $name = str_replace("|", "", $name);
        $name = str_replace(":", "", $name);
        $name = str_replace(";", "", $name);
        $name = str_replace("<", "", $name);
        $name = str_replace(">", "", $name);
        $name = str_replace(",", "", $name);
        $name = str_replace(".", "", $name);
        $name = str_replace("?", "", $name);
        $name = str_replace("\"", "-", $name);
        $name = str_replace("/", "-", $name);
        $name = str_replace("\\", "-", $name);
        //	$name = ereg_replace("[^ -A-Za-z]", "", $name);
        //	$name = str_replace("1","",$name);
        //	$name = str_replace("2","",$name);
        //	$name = str_replace("3","",$name);
        //	$name = str_replace("4","",$name);
        //	$name = str_replace("5","",$name);
        //	$name = str_replace("6","",$name);
        //	$name = str_replace("7","",$name);
        //	$name = str_replace("8","",$name);
        //	$name = str_replace("9","",$name);
        //	$name = str_replace("0","",$name);

        $name = str_replace(" ", "-", $name);  //preg_replace($pattern, $replacement, $string);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);

        $first = substr($name, 0, 1);

        if ($first == "-") {
            $name = substr($name, 1);
        }

        if (strlen($name) > 50) {
            $name = substr($name, 0, 50);
        }

        $alise = $name;

        return $alise;
    }

    function get_mysqli() {

        $db = (array) get_instance()->db;
        return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
    }

    function clean($string) {

        $string = preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
        $string = preg_replace('/\s+/', '-', $string); // Replaces all whitespaces with hyphens.

        return rtrim($string, '-');
    }

    function CreateFixedSizedImage($source, $dest, $width, $height) {

        $source_path = $source;

        list( $source_width, $source_height, $source_type ) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;

            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;

            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $width / $height;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            //
            // Triggered when source image is wider
            //
    $temp_height = $height;
            $temp_width = (int) ( $height * $source_aspect_ratio );
        } else {
            //
            // Triggered otherwise (i.e. source image is similar or taller)
            //
    $temp_width = $width;
            $temp_height = (int) ( $width / $source_aspect_ratio );
        }

        //
        // Resize the image into a temporary GD image
        //

  $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
                $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
        );

        //
        // Copy cropped region from temporary image into the desired GD image
        //

  $x0 = ( $temp_width - $width ) / 2;
        $y0 = ( $temp_height - $height ) / 2;
        //$y0 = 0; //if the cropping needs to be done form top

        $desired_gdim = imagecreatetruecolor($width, $height);
        imagecopy(
                $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $width, $height
        );

        //
        // Render the image
        // Alternatively, you can save the image in file-system or database
        //

  //header( 'Content-type: image/jpeg' );
        imagejpeg($desired_gdim, $dest);

        //
        // Add clean-up code here
    //
}

    function country_list() {
        $CI = & get_instance();
        $query = $CI->db->get('tbl_countries');
        $result = $query->result_array();
        return $result;
    }

    /////////////////////////////////////////////////////////////
    //	Function to convert view date to DATABASE format DATE
    /////////////////////////////////////////////////////////////
    function database_date($date) {
        $date_array = explode("/", $date);
        $new_date = @$date_array[2] . '-' . @$date_array[1] . '-' . @$date_array[0];
        //echo $new_date;die;
        return $new_date;
    }

    function get_random_password($chars_min = 8, $chars_max = 8, $use_upper_case = true, $include_numbers = true, $include_special_chars = true) {

        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if ($include_numbers) {
            $selection .= "1234567890";
        }
        if ($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $password .= $current_letter;
        }

        return $password;
    }

    function RandomString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            @$randstring .= $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

    function create_username() {
        $characters = '0123456789';
        $randstring = '';
        for ($i = 0; $i < 6; $i++) {
            @$randstring .= $characters[rand(0, strlen($characters))];
        }

        $check_duplicate = $this->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $randstring . '"');

        if ($check_duplicate > 0) {
            $this->create_username();
        }
        return $randstring;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    /// Encrypt function
    ///////////////////////////////////////////////////////////////////////////////////
    function ablEncrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = openssl_encrypt($string, ENCRYPT_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    /// Decrypt function
    ///////////////////////////////////////////////////////////////////////////////////
    function ablDecrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = base64_decode($string);
        $output = openssl_decrypt($output, ENCRYPT_METHOD, $key, 0, $iv);

        return $output;
    }

    function get_student_list_class_sectionwise($class_id, $section_id) {
        $CI = & get_instance();
        $CI->db->where('school_id', $CI->session->userdata('school_id'));
        $CI->db->where('class_id', $class_id);
        $CI->db->where('section_id', $section_id);
        $query = $CI->db->get(TBL_STUDENT);
        
        $result = $query->result_array();
        return $result;
    }

    function get_student_attendance_list_class_sectionwise($class_id, $section_id, $period, $today_date) {
        $CI = & get_instance();
        $count = 0;
        $data = array();
        //$CI->db->where('school_id',$CI->session->userdata('school_id'));
        //$student_count = $this->get_perticular_count(TBL_STUDENT,'and school_id = "'.$CI->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'. $section_id .'"');


        $CI->db->where('class_id', $class_id);
        $CI->db->where('section_id', $section_id);
        $CI->db->where('period_id', $period);
        $CI->db->where('attendance_date', $today_date);
        
        $query = $CI->db->get(TBL_ATTENDANCE);
        //echo $CI->db->last_query();

        $result = $query->result_array();
//        foreach($result as $row){
//            $data[$count] = $row['student_id'];
//        $count++;}
        return $result;
    }

    function clean_alise($input_string) {

        $input_string = str_replace("_", "", $input_string);
        $input_string = str_replace(" ", "-", $input_string);
        $input_string = str_replace("'", "", $input_string);
        $input_string = str_replace("&#39;", "", $input_string);
        $input_string = str_replace(",", "-", $input_string);
        $input_string = str_replace("*", "", $input_string);
        $input_string = str_replace("&quot;", "", $input_string);
        $input_string = str_replace("&amp;", "&", $input_string);
        $input_string = str_replace("&#039;", "", $input_string);

        return $input_string;
    }

    function generateOTP($length = 4, $chars = '123456789') {
        $chars_length = (strlen($chars) - 1);
        $string = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($string)) {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1})
                $string .= $r;
        }
        //echo $string;die;
        return $string;
    }

    function check_running_class_for_teacher() {
        $CI = & get_instance();
        $teacher_id = $CI->session->userdata('teacher_id');
        $today = date('Y-m-d');
        $current_time = date('H:i:00');
        $day_no = date('N', strtotime($today));
        $check_running_class = $this->get_perticular_count(TBL_TIMETABLE, 'and school_id = "' . $CI->session->userdata('school_id') . '" and day_id = "' . $day_no . '" and teacher_id = "' . $teacher_id . '" and period_start_time <= "' . $current_time . '" and period_end_time >= "' . $current_time . '" ');
        $time_table_id = $this->get_particular_field_value(TBL_TIMETABLE,'id', 'and school_id = "' . $CI->session->userdata('school_id') . '" and day_id = "' . $day_no . '" and teacher_id = "' . $teacher_id . '" and period_start_time <= "' . $current_time . '" and period_end_time >= "' . $current_time . '" ');
        $data = array(
            'running_class' => $check_running_class,
            'period_id' => $time_table_id
        );
        
        return $data;
        
    }
    
    /*     * *********************************************************************** */
    /*     * *********************************************************************** */
    
    function sprintf_email($str='', $vars=array(), $char='%') {
        
        if (!$str) return '';
        if (count($vars) > 0) {
            foreach ($vars as $k => $v) {
                $str = str_replace($char . $k . $char, $v, $str);
            }
        }

        return $str;
    }
    
    
     /*     * *********************************************************************** */
    /*     * *********************************************************************** */
    
    function CreateSystemEmail($addressing_user, $message, $unsubscribe = '') {

        $CI = & get_instance();

        $CI->load->helper('file');

        $mail_template = read_file("application/views/mail_template/system/mail_template.php");
        $dynamic = $this->sprintf_email($mail_template, 
            array(
                'addressing_user' => $addressing_user,
                'mail_body' => $message,                
                'unsubscribe' => $unsubscribe
            )
        );
        
        return $dynamic;
    }
    
    function getDateReturnDayNumber($date) {
        $query_date = strtotime($date);
        $day = date('w', $query_date);
        return $day;
    }
    
    function check_date_range(){
         $CI = & get_instance();
         $first_day_of_week = date("Y-m-d", strtotime('monday this week'));
         $last_day_of_week = date("Y-m-d", strtotime('sunday this week'));
         if($CI->session->userdata('query_date') >= $first_day_of_week && $CI->session->userdata('query_date') <= $last_day_of_week){
             return 1;
         }else{
             return 2;
         }
    }
    
       function user_validation($user_id) {
        //return 0;

        
        $user_status = $this->get_particular_field_value(TBL_COMMON_LOGIN, 'status', 'and id = "' . $user_id . '"');

        if ($user_status == 1) {
            return 1;   ////////////////   Active
        } else {
            return 2;   ///////////////   inactive
        } 
    }

}

?>
