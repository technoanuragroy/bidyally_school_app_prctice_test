<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_admin_security();
        $this->load->model("admin/Admin_user_model");
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {


        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $this->session->userdata('admin_id')));
        $this->load->view("admin/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Change password from admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function change_password() {

        if ($this->input->post("change_password") && $this->input->post("change_password") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            $this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/change_password");
            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value(TBL_ADMIN, "password", " and admin_id='" . $this->session->userdata('admin_id') . "'");

                if (password_verify($o_password, $password)) {

                    $data = array(
                        "password" => password_hash($this->input->post("n_password"), PASSWORD_DEFAULT)
                    );

                    $table = TBL_ADMIN;

                    $where = array(
                        "admin_id" => $this->session->userdata('admin_id')
                    );
                    $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("admin/user/change_password");
                } else {
                    $this->session->set_flashdata("e_message", "Old password is incorrect.");
                    redirect("admin/user/change_password");
                }
            }
        } else {

            $data['active_menu'] = '';
            $this->load->view("admin/change_password", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_profile() {

        if ($this->input->post("save") && $this->input->post("save") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("email", "Account Email", "trim|required|valid_email");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/edit_profile");
            } else { /// Update admin
                $admin_id = $this->session->userdata('admin_id');
                $update = $this->Admin_user_model->update_admin($admin_id);
                $this->load->library('image_lib');
                $this->load->library('upload');

                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                            ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                            ($_FILES['adminphoto']['type'] == 'image/png') ||
                            ($_FILES['adminphoto']['type'] == 'image/gif')) {

                        list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $new_width = "";
                        $new_height = "";

                        $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                        $new_width = 185;
                        $new_height = $new_width / $ratio;

                        $config['image_library'] = 'gd2';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                        $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                        $config['maintain_ratio'] = FALSE;
                        $config['width'] = $new_width;
                        $config['height'] = $new_height;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();


                        $this->session->set_flashdata("s_message", "Record has been successfully updated.");
                        redirect("admin/user/edit_profile/");
                    } else {
                        $this->session->set_flashdata('invalid_img_type', 'Only .jpg, .jpeg, .png, .gif types are allowed');
                        redirect("admin/user/edit_profile/");
                    }
                }

                if ($update) {
                    $this->session->set_flashdata('s_message', "Successfully updated admin details.");
                    redirect("admin/user/edit_profile");
                } else {
                    $this->session->set_flashdata('e_message', "Error updating admin details.");
                    redirect("admin/user/edit_profile");
                }
            }
        } else {

            $data['active_menu'] = '';

            $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $this->session->userdata('admin_id')));
            $data['admin_details']['image'] = "<img src='" . base_url() . "_images/superadmin_icon.png'>";
            $this->load->view("admin/edit_profile", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Reset password(linked with forgot password)
    ///////////////////////////////////////////////////////////////////////////////
    public function reset_password() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('re_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/main/forgot_password");
            } else {
                $data = array(
                    "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                );

                $table = TBL_ADMIN;

                $where = array(
                    "admin_id" => $this->session->userdata("reset_admin_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_admin_id");

                if ($password_updated) {
                    $this->session->set_flashdata("s_message", 'Password has been changed successfully.<a href="' . base_url() . 'admin">Login here</a>.');
                    redirect("admin/main/forgot_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {

            $admin_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);

            if (isset($admin_id) && isset($code)) {
                $admin_details = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $admin_id));

                if ($code == $this->my_custom_functions->encrypt_string($admin_details['email'])) {
                    $this->session->set_userdata("reset_admin_id", $admin_id);
                    $this->load->view("admin/reset_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            } else {
                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("admin/main/forgot_password");
            }
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   TEACHER PART
    //////////////////////////////////////////////////////////////////////

    public function manageAdmin() {


        $data['admins'] = $this->Admin_user_model->get_admin_data();
        $this->load->view('admin/manage_admins', $data);
    }

    public function addAdmin() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            // echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/addAdmin");
            } else { /// Update admin
                $teacher_data = array(
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => $this->input->post('status'),
                );

                $admin_id = $this->my_custom_functions->insert_data_last_id($teacher_data, TBL_ADMIN);

                if ($admin_id) {

                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                                ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                                ($_FILES['adminphoto']['type'] == 'image/png') ||
                                ($_FILES['adminphoto']['type'] == 'image/gif')) {

                            list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                            $new_width = "";
                            $new_height = "";

                            $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                            $new_width = 185;
                            $new_height = $new_width / $ratio;

                            $config['image_library'] = 'gd2';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                            $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                            $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                            $config['maintain_ratio'] = FALSE;
                            $config['width'] = $new_width;
                            $config['height'] = $new_height;

                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                        }
                    }

                    $this->session->set_flashdata("s_message", 'Admin added successfully.');
                    redirect("admin/user/manageAdmin");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/user/manageAdmin");
                }
            }
        } else {
            $this->load->view('admin/add_admin');
        }
    }

    public function editAdmin() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $admin_id = $this->input->post('admin_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/editAdmin/" . $admin_id);
            } else { /// Update class
                $admin_data = array(
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'status' => $this->input->post('status'),
                );
                if ($this->input->post('password') && $this->input->post('password') != '') {
                    $admin_data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                }

                $condition = array(
                    'admin_id' => $admin_id
                );

                $edit_admin_data = $this->my_custom_functions->update_data($admin_data, TBL_ADMIN, $condition);

                $this->load->library('image_lib');
                $this->load->library('upload');

                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                            ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                            ($_FILES['adminphoto']['type'] == 'image/png') ||
                            ($_FILES['adminphoto']['type'] == 'image/gif')) {

                        list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $new_width = "";
                        $new_height = "";

                        $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                        $new_width = 185;
                        //$new_height = $new_width / $ratio;
                        $new_height = 185;

                        $config['image_library'] = 'gd2';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                        $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                        $config['maintain_ratio'] = FALSE;
                        $config['width'] = $new_width;
                        $config['height'] = $new_height;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                }
                if ($edit_admin_data) {
                    $this->session->set_flashdata("s_message", 'Admin edited successfully.');
                    redirect("admin/user/manageAdmin");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/user/manageAdmin");
                }
            }
        } else {
            $subject_id = $this->uri->segment(4);
            $data['admin'] = $this->my_custom_functions->get_details_from_id('', TBL_ADMIN, array('admin_id' => $subject_id));

            $this->load->view('admin/edit_admin', $data);
        }
    }

    public function deleteAdmin() {
        $admin_id = $this->uri->segment(4);
        $delete_teacher = array('admin_id' => $admin_id);
        $delete = $this->my_custom_functions->delete_data(TBL_ADMIN, $delete_teacher);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Admin deleted successfully.');
            redirect("admin/user/manageAdmin");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("admin/user/manageAdmin");
        }
    }

    public function logout() {


        $session_data = array('admin_id', 'admin_username', 'admin_email', 'admin_is_logged_in');
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("admin");
    }

    function manageSchool() {
        $data['schools'] = $this->Admin_user_model->get_school_data();
        $this->load->view('admin/manage_school', $data);
    }

    function loginAsSchool() {
        $school_id = $this->uri->segment(4);
        $school_details = $this->my_custom_functions->get_details_from_id($school_id, TBL_SCHOOL);


        $session_data = array(
            "school_id" => $school_details["id"],
            "school_username" => $school_details["username"],
            "school_email" => $school_details["email_address"],
            "school_is_logged_in" => 1
        );
        $this->session->set_userdata($session_data);
        redirect("school/user/dashBoard");
    }
    
    function checkAdminUsername(){
        
        $adminUsername = $this->input->post('reg_val');

        $check_registratin_no = $this->my_custom_functions->get_perticular_count(TBL_ADMIN, ' and username = "' . $adminUsername . '"');
        if ($check_registratin_no > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
