<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model("admin/Admin_main_model");
    }

    public function index() {
        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $response = $this->Admin_main_model->login_check();
            if ($response == "1") { /// If credentials match with system                
                
                    redirect("admin/user/dashBoard");
              
            } else { /// If credentials don't match                
                $this->session->set_flashdata("e_message", "Invalid username/password.");
                redirect("admin");
            }
        } else {

            if ($this->session->userdata('admin_id')) { /// If the admin is already logged in, don't show the login page                
                redirect("admin/user/dashBoard");
            }

            $this->load->view("admin/login");
        }
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////
    /// Logout admin
    ///////////////////////////////////////////////////////////////////////////////
    public function logout() {
        
        $session_data = array('admin_id', 'admin_username', 'admin_email');
        $this->session->unset_userdata($session_data);   
                        
        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("admin");
    }
       
     ///////////////////////////////////////////////////////////////////////////////
    /// Forgot password
    ///////////////////////////////////////////////////////////////////////////////  
    public function forgot_password() {
	
        if($this->input->post('submit') && $this->input->post('submit') != "") {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Username', 'trim|required');

            if($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));    
                redirect("admin/main/forgot_password");

            } else {
                
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $return = $this->my_custom_functions->get_perticular_count(TBL_ADMIN, " AND username LIKE '" . $username . "'");
                
                if($return) {

                    $admin_details = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("username" => $username));
                    $reset_link = base_url() . 'admin/main/reset_password/' . $admin_details['admin_id'] . '/' . $this->my_custom_functions->encrypt_string($admin_details['email']);
                    
                    $this->load->helper('file');
                    
                    $message = "";               
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= 'Hello '.$admin_details['username'].',<br><br>';
                    $message .= 'You are receiving this email because you have requested for a change of password.<br><br>';
                    $message .= 'You may <a href="'.$reset_link.'">click here</a> to reset your password now.<br><br>Thank you.';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");
                    $this->my_custom_functions->SendEmail(SITE_EMAIL.','.SITE_NAME, $admin_details['email'], SITE_NAME.' : Password recovery link', $message);

                    $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                    redirect("admin/main/forgot_password");

                } else { 

                    $this->session->set_flashdata("e_message", "You are not a registered admin!");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {
            $this->load->view("admin/forgot_password");
        }
    }
      
    ///////////////////////////////////////////////////////////////////////////////
    /// Reset password(linked with forgot password)
    ///////////////////////////////////////////////////////////////////////////////
    public function reset_password() {
	
        if ($this->input->post('submit') AND ($this->input->post('submit') != "")) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('re_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));    
                redirect("admin/main/forgot_password");

            } else {
                $data = array(
                    "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                );

                $table = TBL_ADMIN;

                $where = array(
                    "admin_id" => $this->session->userdata("reset_admin_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_admin_id");

                if ($password_updated) {
                    $this->session->set_flashdata("s_message", 'Password has been changed successfully.<a href="'.base_url().'admin">Login here</a>.'); 
                    redirect("admin/main/forgot_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {

            $admin_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);

            if(isset($admin_id) && isset($code)) {
                $admin_details = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $admin_id));

                if ($code == $this->my_custom_functions->encrypt_string($admin_details['email'])) {
                    $this->session->set_userdata("reset_admin_id", $admin_id);         
                    $this->load->view("admin/reset_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");        
                    redirect("admin/main/forgot_password");
                }
            } else {
                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("admin/main/forgot_password");
            }
        }
    }

}
