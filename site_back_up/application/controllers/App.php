<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model("Main_model");
    }

    public function index() {

        if ($this->input->cookie('student_id', true)) {

            $student_id = $this->input->cookie('student_id');
            $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
            $session_student_data = array(
                "student_id" => $student_id,
                "class_id" => $student_detail['class_id'],
                "section_id" => $student_detail['section_id'],
                "school_id" => $student_detail['school_id'],
                "student_flag" => 1
            );
            $this->session->set_userdata($session_student_data);
            redirect('parent/user/dashBoard');
        } else if ($this->input->cookie('teacher_id', true)) {

            $teacher_id = $this->input->cookie('teacher_id');
            $teacher_detail = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_TEACHER);
            $session_data = array(
                "teacher_id" => $teacher_id,
                "school_id" => $teacher_detail['school_id'],
                "school_is_logged_in" => 1
            );
            $this->session->set_userdata($session_data);
            redirect('teacher/user/dashBoard');
        } else {

            //echo "<pre>";print_r($this->session->all_userdata()); 
            $this->load->view('login');
        }
    }

    public function login() {
        //
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "New Password", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("app");
            } else {
                $response = $this->Main_model->login_check();
                //echo $response;die;
                if ($response == TEACHER) { /// If credentials match with system  
                    $cookie = array(
                        'name' => 'teacher_id',
                        'value' => $this->session->userdata('teacher_id'),
                        'expire' => '8650000',
                    );
                    $this->input->set_cookie($cookie);

                    redirect("teacher/user/dashBoard");
                } elseif ($response == PARENTS) {

                    $children_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $this->session->userdata('parent_id') . '"');


                    if ($children_count == 1) {
                        $student_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'student_id', 'and parent_id = "' . $this->session->userdata('parent_id') . '"');
                        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and parent_id = "' . $this->session->userdata('parent_id') . '"');
                        $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                        //$this->load->helper('cookie');
                        $cookie = array(
                            'name' => 'student_id',
                            'value' => $student_id,
                            'expire' => '8650000',
                        );
                        $this->input->set_cookie($cookie);
                        $session_student_data = array(
                            "student_id" => $student_id,
                            "class_id" => $student_detail['class_id'],
                            "section_id" => $student_detail['section_id'],
                            "school_id" => $school_id,
                        );
                        $this->session->set_userdata($session_student_data);
                    }
                    redirect("parent/user/dashBoard");
                } else {
                    
                    $this->session->set_flashdata("e_message", "Invalid username/password.");
                    redirect("app");
                }
            }
        }
    }

    public function signUp() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("materialMobileno", "Mobile Number", "trim|required|min_length[10]|max_length[10]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("signUp");
            } else {
                $mobile_no = $this->input->post('materialMobileno');
                $parent_id = $this->Main_model->check_parent_existance($mobile_no);
                if ($parent_id > 0) {
                    $otp = $this->my_custom_functions->generateOTP();
                    $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");

                    while ($check_pin > 0) {
                        $otp = $this->my_custom_functions->generateOTP();
                        $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");
                    }
                    $update_data = array('otp' => $otp);
                    $condition = array('id' => $parent_id);
                    $update_otp = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);

                    ///////// CODE FOR SENDING OTP THROUGH MESSAGE
                    
                        $message = $otp.' is your one time password for sign up.';
                        
                        $msgen = urlencode($message);
                        $send_url = $this->config->item("SEND_SMS_URL") . "number=" . $mobile_no . "&text=" . $msgen;
                        //echo $send_url;die;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $send_url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($ch);

                        curl_close($ch);
                        //echo '<pre>';print_r($response);die;
                        
                    //$this->session->set_flashdata("s_message", "Your otp is " . $otp);
                        $this->session->set_flashdata("s_message", "Please enter otp.");
                    redirect("enterOTP");
                } else {
                    $this->session->set_flashdata("e_message", "Invalid username");
                    redirect("signUp");
                }
            }
        } else {
            //$this->load->view('register');
            $this->load->view('getmobileno');
        }
    }

    function enterOTP() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("enter_otp", "Enter OTP", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("enterOTP");
            } else {
                $otp = $this->input->post('enter_otp');
                $check_correct_otp = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and id="' . $this->session->userdata('parent_id') . '" and otp = "' . $otp . '"');

                if ($check_correct_otp > 0) {
                    redirect('enterDetail');
                } else {
                    $this->session->set_flashdata("e_message", "Invalid otp");
                    redirect("enterOTP");
                }
            }
        } else {
            
            $this->load->view('enterOTP');
        }
    }

    function enterDetail() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("parent_name", "Enter Name", "trim|required");
            //$this->form_validation->set_rules("parent_email", "Enter Email", "trim|required");
            $this->form_validation->set_rules("parent_password", "Enter Password", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("enterOTP");
            } else {

                $parent_password = array(
                    'password' => password_hash($this->input->post('parent_password'), PASSWORD_DEFAULT),
                );

                $condition = array('id' => $this->session->userdata('parent_id'));
                $password = $this->my_custom_functions->update_data($parent_password, TBL_COMMON_LOGIN, $condition);




                $parent_detail = array(
                    'name' => $this->input->post('parent_name'),
                    'email' => $this->input->post('parent_email'),
                );

                $condition = array('id' => $this->session->userdata('parent_id'));

                $detail = $this->my_custom_functions->update_data($parent_detail, TBL_PARENT, $condition);
                if ($detail) {

                    $children_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $this->session->userdata('parent_id') . '"');
                    if ($children_count == 1) {
                        $student_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'student_id', 'and parent_id = "' . $this->session->userdata('parent_id') . '"');

                        $cookie = array(
                            'name' => 'student_id',
                            'value' => $student_id,
                            'expire' => '8650000',
                        );
                        $this->input->set_cookie($cookie);
                        $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                        $session_student_data = array(
                            "student_id" => $student_id,
                            "class_id" => $student_detail['class_id'],
                            "section_id" => $student_detail['section_id'],
                            "school_id" => $student_detail['school_id'],
                            "student_flag" => 1
                        );
                        $this->session->set_userdata($session_student_data);

                        redirect('parent/user/dashBoard');
                    }
                }
            }
        } else {
            $this->load->view('enter_detail');
        }
    }

    function forgetPassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
            $return = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, " AND username LIKE '" . $username . "' AND status=1 ");
            if ($return) {
//                $email_address = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'email', 'and id = "' . $user_details['id'] . '"');
//                $reset_link = base_url() . 'userResetPassword/' . $this->my_custom_functions->ablEncrypt($user_details['type']) . '/' . $this->my_custom_functions->ablEncrypt($user_details['id']);
//                // Email template
//                $email_data = $this->config->item("company_forgot_password");
//                $from_email = SITE_EMAIL;
//                // Email variables
//                $subject = $email_data['subject'];
//                $addressing_user = $email_data['addressing_user'];
//                $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
//                    'reset_link' => $reset_link
//                        )
//                );
//                $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message);         
//                $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $email_address, $subject, $full_mail);
                
                $user_details = $this->my_custom_functions->get_details_from_id("", TBL_COMMON_LOGIN, array("username" => $username));
                
                
                if(!is_numeric($username)){
                    $mobile_no = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER,'phone_no','and id = "'.$user_details['id'].'"');
                }else{
                    $mobile_no = $username;
                }
                
                ///////////////////////////   SEND OTP TO USER   ////////////////////////////////
                
                    $otp = $this->my_custom_functions->generateOTP();
                    $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");

                    while ($check_pin > 0) {
                        $otp = $this->my_custom_functions->generateOTP();
                        $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");
                    }
                    $update_data = array('otp' => $otp);
                    $condition = array('id' => $user_details['id']);
                    $update_otp = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
                    //echo $this->db->last_query();die;
                    ///////////////////  SET SESSION FOR USER REQUESTING FOR NEW PASSWORD   ////////////////
                    $session_data = array(
                        "user_id" => $user_details["id"],
                    );
                    $this->session->set_userdata($session_data);
                   
                    ///////// CODE FOR SENDING OTP THROUGH MESSAGE
                    
                    
                        $message = $otp.' is your one time password for create new password.';
                    
                        $msgen = urlencode($message);
                        $send_url = $this->config->item("SEND_SMS_URL") . "number=" . $mobile_no . "&text=" . $msgen;

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $send_url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $response = curl_exec($ch);

                        curl_close($ch);
                        
                        
                    //$this->session->set_flashdata("s_message", "Your otp is " . $otp);
                    $this->session->set_flashdata("s_message", "Please enter otp.");
                    redirect("app/enterOTPforgotPassword");
                    
            } else {

                $this->session->set_flashdata("e_message", "You are not a registered user!");
                redirect("forgetPassword");
            }
        } else {
            $this->load->view('forget_password');
        }
    }
    
    function enterOTPforgotPassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("enter_otp", "Enter OTP", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("app/enterNewPassword");
            } else {
                $otp = $this->input->post('enter_otp');
                $check_correct_otp = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and id="' . $this->session->userdata('user_id') . '" and otp = "' . $otp . '"');

                if ($check_correct_otp > 0) {
                    redirect('app/enterNewPassword');
                } else {
                    $this->session->set_flashdata("e_message", "Invalid otp");
                    redirect("app/enterOTPforgotPassword");
                }
            }
        } else {
            $this->load->view('enterFPWOTP');
        }
    }
    
    function enterNewPassword() {
         if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $user_id = $this->session->userdata('user_id');

            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $update_data = array('password' => $password);
            $condition = array('id' => $user_id);
            $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
            
            $this->session->set_flashdata("s_message", "Password successfully updated. Please login here!");
            redirect("app");
        } else {
           // echo "<pre>";print_r($this->session->all_userdata());
        $this->load->view('reset_password');
        }
    }
////////////////////////////////////////////////////////////////////////////
//////////////  NOT USED 
//////////////////////////////////////////////////////////////////////////
    function userResetPassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $user_id = $this->my_custom_functions->ablDecrypt($this->input->post('user_id'));

            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $update_data = array('password' => $password);
            $condition = array('id' => $user_id);
            $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
            $this->session->set_flashdata("s_message", "Password successfully updated. Please login here!");
            redirect("app");
        } else {
            $user_type = $this->my_custom_functions->ablDecrypt($this->uri->segment(2));
            $user_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(3));
            $check_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and type = "' . $user_type . '" and id = "' . $user_id . '"');
            if ($check_exist == 0) {
                $this->session->set_flashdata("e_message", "Invalid link!");
                redirect("forgetPassword");
            }

            $this->load->view('reset_password');
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////
    function app_login() {
        $user_id_encr = $this->uri->segment(3);
        $user_id = $this->my_custom_functions->ablDecrypt($user_id_encr);
        $user_details = $this->my_custom_functions->get_details_from_id($user_id, TBL_COMMON_LOGIN);
        //echo "<pre>";print_r($user_details);die;      
        if ($user_details['type'] == TEACHER) { /// If credentials match with system  
            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $user_id . '"');
            $session_data = array(
                "teacher_id" => $user_id,
                "school_id" => $school_id,
                "school_is_logged_in" => 1
            );
            $this->session->set_userdata($session_data);
            $cookie = array(
                'name' => 'teacher_id',
                'value' => $user_id,
                'expire' => '8650000',
            );
            $this->input->set_cookie($cookie);
            //echo "<pre>";print_r($this->session->all_userdata());die;      
            redirect("teacher/user/dashBoard");
        } elseif ($user_details['type'] == PARENTS) {

            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id = "' . $user_id . '"');
            $session_data = array(
                "parent_id" => $user_id,
                "school_id" => $school_id,
                "parent_is_logged_in" => 1
            );
            $this->session->set_userdata($session_data);
            $children_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $user_id . '"');


            if ($children_count == 1) {
                $student_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'student_id', 'and parent_id = "' . $user_id . '"');
                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and parent_id = "' . $user_id . '"');
                $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                //$this->load->helper('cookie');
                $cookie = array(
                    'name' => 'student_id',
                    'value' => $student_id,
                    'expire' => '8650000',
                );
                $this->input->set_cookie($cookie);
                $session_student_data = array(
                    "student_id" => $student_id,
                    "class_id" => $student_detail['class_id'],
                    "section_id" => $student_detail['section_id'],
                    "school_id" => $school_id,
                );
                $this->session->set_userdata($session_student_data);
            }
            redirect("parent/user/dashBoard");
        } else {
//                    $this->session->set_flashdata("e_message", "Invalid username/password.");
//                    redirect("app");
        }
    }

}
