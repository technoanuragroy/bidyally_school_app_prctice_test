<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        //echo "<pre>";print_r($this->session->all_userdata());
        if ($this->uri->segment(5) && $this->uri->segment(5) == DOWNLOAD_KEY) {
            
        } else {
            $this->my_custom_functions->check_teacher_security();
        }
        $this->load->model("teacher/Teacher_user_model");

        $prefs = array(
            'start_day' => 'monday',
            'month_type' => 'long',
            'day_type' => 'short',
            'show_next_prev' => true,
            //'next_prev_url' => base_url() . "admin/calendar/event",
            'template' => '
					   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender" >{/table_open}
                                           
                                            {heading_row_start}<tr>{/heading_row_start}
                                           
					   {heading_previous_cell}<th id="pre_link"><a href="{previous_url}" title="previous">&laquo;</a></th>{/heading_previous_cell}
					   
                                           {heading_title_cell}<th colspan="{colspan}" id="month_heading">{heading}</th>{/heading_title_cell}
					   {heading_next_cell}<th id="next_link"><a href="{next_url}" title="next">&raquo;</a></th>{/heading_next_cell}
					
					   {heading_row_end}</tr>{/heading_row_end}
                                           
					   {week_row_start}<tr>{/week_row_start}
					   {week_day_cell}<th>{week_day}</th>{/week_day_cell}
					   {week_row_end}</tr>{/week_row_end}
					
					   {cal_row_start}<tr>{/cal_row_start}
					   {cal_cell_start}<td width="5">{/cal_cell_start}
					
					   {cal_cell_content}<div class="date {custom_class}">{day}</div><div class="content_box sort_box_{day}"></div> {/cal_cell_content}
					   {cal_cell_content_today}<div class="highlight"><div class="date">{day}</div><div class="content_box sort_box_{day}"></div></div>{/cal_cell_content_today}
					
					   {cal_cell_no_content}<div class="date">{day}</div><div class="content_box"></div>{/cal_cell_no_content}
					   {cal_cell_no_content_today}<div class="highlight"><div class="highlight">{day}</div></div>{/cal_cell_no_content_today}
					
					   {cal_cell_blank}&nbsp;{/cal_cell_blank}
					
					   {cal_cell_end}</td>{/cal_cell_end}
					   {cal_row_end}</tr>{/cal_row_end}
					
					   {table_close}</table>{/table_close}'
        );
        $this->load->library('calendar', $prefs);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Teacher area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {

        //print_r($_COOKIE);
        $data['page_title'] = 'Dashboard';
        $data['back_button'] = "";
        $data['class_detail'] = $this->Teacher_user_model->get_teacher_dashboard_classes();
        $data['weekly_class_data'] = $this->Teacher_user_model->get_weekly_classes();
        $data['activity_data'] = $this->Teacher_user_model->get_three_months_activity_data();
        $data['pending_works'] = $this->Teacher_user_model->get_last_seven_days_activity_data();
        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata('school_id')));
        $this->load->view("teacher/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Logout
    ///////////////////////////////////////////////////////////////////////////////

    public function logout() {
        delete_cookie('teacher_id');
        $session_data = array('teacher_id', 'teacher_is_logged_in');
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("app");
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Get Todays classes for a perticular teacher
    ///////////////////////////////////////////////////////////////////////////////
    public function todaysClasses() {
        $teacher_id = $this->session->userdata('teacher_id');
        $data['get_todays_classes'] = $this->Teacher_user_model->get_todays_classes($teacher_id);
        if ($this->session->userdata('query_date') == date('Y-m-d')) {
            $day = "Today's classes";
        } else {
            $day = date('D d M', strtotime($this->session->userdata('query_date')));
        }
        $data['page_title'] = $day;
        $data['back_button'] = "";
        //echo "<pre>";print_r($get_todays_classes);
        $this->load->view('teacher/todays_classes', $data);
    }

    public function listingScreen() {
        $this->load->view('teacher/listing_screen');
    }

    public function dailyWorks() {
        $data['page_title'] = "Today's Works";
        $data['back_button'] = "";
        $this->load->view('teacher/dailyWorks', $data);
    }

    public function attendance() {
        //echo "<pre>";print_r($this->session->all_userdata());die;
        if ($this->input->post('submit') && $this->input->post('submit') == 'Save') {
            //echo "<pre>";print_r($_POST);
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $post_attendance_list = $this->input->post('student_id');
            $student_list = $this->my_custom_functions->get_student_list_class_sectionwise($class_id, $section_id);
            foreach ($student_list as $stdnt_list) {
                $attendance_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $class_id,
                    'section_id' => $section_id,
                    'attendance_date' => date('Y-m-d'),
                    'period_id' => $this->input->post('period_id'),
                    'attendance_time' => time()
                );
                if (!empty($post_attendance_list)) {
                    if (in_array($stdnt_list['id'], $post_attendance_list)) {
                        //Present

                        $attendance_data['student_id'] = $stdnt_list['id'];
                        $attendance_data['attendance_status'] = PRESENT;
                    } else {
                        //Absent
                        $attendance_data['student_id'] = $stdnt_list['id'];
                        $attendance_data['attendance_status'] = ABSENT;
                    }
                } else {
                    $attendance_data['student_id'] = $stdnt_list['id'];
                    $attendance_data['attendance_status'] = ABSENT;
                }
                $check_exist = $this->my_custom_functions->get_perticular_count(TBL_ATTENDANCE, 'and school_id = "' . $this->session->userdata('school_id') . '" and student_id = "' . $attendance_data['student_id'] . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" and attendance_date = "' . date('Y-m-d') . '" and period_id = "' . $this->input->post('period_id') . '"');
                if ($check_exist == 0) {
                    $this->my_custom_functions->insert_data($attendance_data, TBL_ATTENDANCE);
                } else {
                    $update_data = array(
                        'attendance_status' => $attendance_data['attendance_status']
                    );
                    $condition = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'student_id' => $attendance_data['student_id'],
                        'class_id' => $class_id,
                        'section_id' => $section_id,
                        'attendance_date' => date('Y-m-d'),
                        'period_id' => $this->input->post('period_id')
                    );
                    $this->my_custom_functions->update_data($update_data, TBL_ATTENDANCE, $condition);
                    //echo $this->db->last_query();echo "<br>";
                }
            }
            $this->session->set_flashdata("s_message", 'Attendance successfully saved.');
            redirect('teacher/user/attendance/' . $class_id . '/' . $section_id . '/' . $this->input->post('period_id'));
        } else if ($this->input->post('submit') && $this->input->post('submit') == 'Publish') {

            $absent_data = array();
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $post_attendance_list = $this->input->post('student_id');
            $student_list = $this->my_custom_functions->get_student_list_class_sectionwise($class_id, $section_id);
//            echo "<pre>";
//            print_r($student_list);
//            echo "<pre>";
//            print_r($post_attendance_list);
            $count = 0;
            foreach ($student_list as $stdnt_list) {


                if (!empty($post_attendance_list)) {

                    if (!in_array($stdnt_list['id'], $post_attendance_list)) {
                        //TRIGGER MESSAGE TO PARENTS FOR ABSENT STUDENT
                        $absent_data[$count]['student_id'] = $stdnt_list['id'];
                        $absent_data[$count]['attendance_status'] = ABSENT;
                    }
                } else {
                    //TRIGGER MESSAGE TO PARENTS FOR ABSENT STUDENT (ALL ABSENT)
                    $absent_data[$count]['student_id'] = $stdnt_list['id'];
                    $absent_data[$count]['attendance_status'] = ABSENT;
                }

                $count++;
            }
            $update_data = array(
                'publish_data' => 1
            );
            $condition = array(
                'school_id' => $this->session->userdata('school_id'),
                'class_id' => $class_id,
                'section_id' => $section_id,
                'attendance_date' => date('Y-m-d'),
                'period_id' => $this->input->post('period_id')
            );
            $this->my_custom_functions->update_data($update_data, TBL_ATTENDANCE, $condition);
//            echo "<pre>";
//            print_r($_POST);
//            
//            echo "<pre>";
//            print_r($absent_data);
            //die;
            if (!empty($absent_data)) {
                foreach ($absent_data as $row) {
                    // SEND MESSAGE HERE.
                }
            }
            $this->session->set_flashdata("s_message", 'Attendance successfully published.');
            redirect('teacher/user/attendance/' . $class_id . '/' . $section_id . '/' . $this->input->post('period_id'));
        } else {
            $class_id = $this->uri->segment(4);
            $section_id = $this->uri->segment(5);
            $period_id = $this->uri->segment(6);
            if ($this->uri->segment(7)) {
                //echo "<pre>";print_r($this->session->all_userdata());
                $today_date = $this->session->userdata('query_date');
            } else {
                $today_date = date('Y-m-d');
            }
            $data['student_list'] = $this->my_custom_functions->get_student_list_class_sectionwise($class_id, $section_id);

            $data['attendance_list'] = $this->my_custom_functions->get_student_attendance_list_class_sectionwise($class_id, $section_id, $period_id, $today_date);
            //echo "<pre>";print_r($data);die;
            $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $this->uri->segment(4) . '"');
            $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $this->uri->segment(5) . '"');
            $data['page_title'] = "Class name : " . $class_name . $section_name;
            $data['back_button'] = "";


            $this->load->view('teacher/attendance', $data);
        }
    }

    function notes_not_used() {
        $this->load->view('teacher/notes');
    }

    function classWorks() {
        if ($this->input->post('submit') && $this->input->post('submit') == 'Save') {

            // Remove images which are deleted in preview section
            $deleted_images = trim($this->input->post('deleted_images'), ',');
            $deleted_images_array = explode(',', $deleted_images);

            if (!empty($deleted_images_array)) {
                foreach ($deleted_images_array as $d_i_a) {
                    if ($d_i_a != "") {
                        if (array_key_exists($d_i_a, $_FILES['doc_img_upload']['name'])) {
                            unset($_FILES['doc_img_upload']['name'][$d_i_a]);
                            unset($_FILES['doc_img_upload']['type'][$d_i_a]);
                            unset($_FILES['doc_img_upload']['tmp_name'][$d_i_a]);
                            unset($_FILES['doc_img_upload']['error'][$d_i_a]);
                            unset($_FILES['doc_img_upload']['size'][$d_i_a]);
                        }
                    }
                }
            }

            //echo '<pre>'; print_r($_FILES['doc_img_upload']); echo '</pre>'; die;
            //echo "<pre>";print_r($this->session->all_userdata());die;

            if ($this->session->userdata('query_date') && $this->session->userdata('query_date') != '') {
                if ($this->session->userdata('query_date') == date('Y-m-d')) {
                    $query_date = date('Y-m-d');
                } else {
                    $query_date = $this->session->userdata('query_date');
                }
            }
            //echo $query_date.' 13:01:00';die;
            $curr_time = date('H:i:s');
            $note_issue_time = strtotime($query_date . ' ' . $curr_time);

            $today_start_date = strtotime($query_date . '00:00:00');
            $today_end_date = strtotime($query_date . '23:59:59');
            $check_existance = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and section_id = "' . $this->input->post('section_id') . '" and period_id = "' . $this->input->post('period_id') . '" and note_issuetime >= "' . $today_start_date . '" and note_issuetime <= "' . $today_end_date . '"');
            if ($check_existance == 0) {
                $note_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_id' => $this->input->post('period_id'),
                    'topic_name' => $this->input->post('topic_name'),
                    'classnote_text' => $this->input->post('classnote_text'),
                    'note_issuetime' => $note_issue_time
                );

                $note_id = $this->my_custom_functions->insert_data_last_id($note_data, TBL_NOTE);
            } else {
                $note_id = $this->my_custom_functions->get_particular_field_value(TBL_NOTE, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and section_id = "' . $this->input->post('section_id') . '" and period_id = "' . $this->input->post('period_id') . '"');

                $note_data_update = array(
                    'topic_name' => $this->input->post('topic_name'),
                    'classnote_text' => $this->input->post('classnote_text'),
                    'note_issuetime' => $note_issue_time
                );
                $condition = array(
                    'id' => $note_id
                );

                $this->my_custom_functions->update_data($note_data_update, TBL_NOTE, $condition);
            }
            //echo "<pre>";print_r($_FILES);
            if ($note_id) {
                if (isset($_FILES['doc_upload'])) {
                    $file_count = count($_FILES['doc_upload']['name']);
                    $file_type_list = $this->config->item('file_type');

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);

                        if ((in_array($ext, $file_type_list))) {
                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;


                                $timestamp = time();
                                $key = 'classnotes_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => CLASSNOTE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('note_id' => $note_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {

                            //$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Note successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        //$error_ext = rtrim($error, ',');
                        //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
//echo '<pre>'; print_r($_FILES['doc_img_upload']); echo '</pre>'; die;
                if (isset($_FILES['doc_img_upload'])) {
                    $file_count = count($_FILES['doc_img_upload']['name']);
                    $file_type_list = $this->config->item('file_type');

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    //for ($i = 0; $i < $file_count; $i++) {
                    foreach ($_FILES['doc_img_upload']['name'] as $i => $fname) {

                        $filename = $_FILES['doc_img_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);

                        if ((in_array($ext, $file_type_list))) {

                            if ($ext == 'pdf' || $ext == 'PDF') {
                                $content_type = 'application/pdf';
                            } else {
                                $content_type = 'text/plain';
                            }

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_img_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_img_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_img_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'classnotes_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_img_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => CLASSNOTE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => $content_type,
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array('Content-Disposition' => 'attachment')
                                    ));


                                    $file_data = array('note_id' => $note_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
//                            $error_count++;
//                            $error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Note successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        $error_ext = rtrim($error, ',');
                        $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
                $this->session->set_flashdata("s_message", 'Note successfully saved.');
                redirect('teacher/user/classWorks/' . $this->input->post('class_id') . '/' . $this->input->post('section_id') . '/' . $this->input->post('period_id'));
            }
        } else if ($this->input->post('submit') && $this->input->post('submit') == 'Publish') {

            $update_publish_data = $this->Teacher_user_model->update_note_publish_data();
            $this->session->set_flashdata("s_message", 'Note successfully published.');
            redirect('teacher/user/classWorks/' . $this->input->post('class_id') . '/' . $this->input->post('section_id') . '/' . $this->input->post('period_id'));
        } else {
//            $today_date = array('query_date' => date('Y-m-d'));
//            $this->session->set_userdata($today_date);
            $data['note_detail'] = $this->Teacher_user_model->get_note_detail();
            $class_id = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $this->uri->segment(4) . '"');
            $sec_id = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $this->uri->segment(5) . '"');
            $subject_id = $this->my_custom_functions->get_particular_field_value(TBL_TIMETABLE, 'subject_id', 'and id = "' . $this->uri->segment(6) . '"');
            $subject = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $subject_id . '"');

            if ($this->session->userdata('query_date') == date('Y-m-d')) {

                $day = $class_id . $sec_id . ', ' . $subject;
                
            } else {
                $day = date('D d M', strtotime($this->session->userdata('query_date'))).', '.$class_id . $sec_id . ' - ' . $subject;
            }
            $data['page_title'] = $day;
            $data['back_button'] = "";
            //echo "<pre>";print_r($data);die;
            $this->load->view('teacher/notes', $data);
        }
    }

    function homeWorks() {
        if ($this->input->post('submit') && $this->input->post('submit') == 'Save') {

            if ($this->session->userdata('query_date') && $this->session->userdata('query_date') != '') {
                if ($this->session->userdata('query_date') == date('Y-m-d')) {
                    $query_date = date('Y-m-d');
                } else {
                    $query_date = $this->session->userdata('query_date');
                }
            }
            $curr_time = date('H:i:s');
            $note_issue_time = strtotime($query_date . ' ' . $curr_time);


            $today_start_date = strtotime($query_date . '00:00:00');
            $today_end_date = strtotime($query_date . '23:59:59');

            $check_existance = $this->my_custom_functions->get_perticular_count(TBL_HOME_NOTE, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and section_id = "' . $this->input->post('section_id') . '" and period_id = "' . $this->input->post('period_id') . '" and note_issuetime >= "' . $today_start_date . '" and note_issuetime <= "' . $today_end_date . '"');
            if ($check_existance == 0) {
                $note_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_id' => $this->input->post('period_id'),
                    'topic_name' => $this->input->post('topic_name'),
                    'classnote_text' => $this->input->post('classnote_text'),
                    'note_issuetime' => $note_issue_time
                );

                $note_id = $this->my_custom_functions->insert_data_last_id($note_data, TBL_HOME_NOTE);
            } else {
                $note_id = $this->my_custom_functions->get_particular_field_value(TBL_HOME_NOTE, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and section_id = "' . $this->input->post('section_id') . '" and period_id = "' . $this->input->post('period_id') . '"');

                $note_data_update = array(
                    'topic_name' => $this->input->post('topic_name'),
                    'classnote_text' => $this->input->post('classnote_text'),
                    'note_issuetime' => $note_issue_time
                );
                $condition = array(
                    'id' => $note_id
                );

                $this->my_custom_functions->update_data($note_data_update, TBL_HOME_NOTE, $condition);
            }
            if ($note_id) {
                if (isset($_FILES['doc_upload'])) {
                    $file_count = count($_FILES['doc_upload']['name']);
                    $file_type_list = $this->config->item('file_type');

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);

                        if ((in_array($ext, $file_type_list))) {

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'homenotes_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => HOMENOTE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('note_id' => $note_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_HOME_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            //$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Note successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        $error_ext = rtrim($error, ',');
                        $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }

                if (isset($_FILES['doc_img_upload'])) {
                    $file_count = count($_FILES['doc_img_upload']['name']);
                    $file_type_list = $this->config->item('file_type');

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_img_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);

                        if ((in_array($ext, $file_type_list))) {

                            if ($ext == 'pdf' || $ext == 'PDF') {
                                $content_type = 'application/pdf';
                            } else {
                                $content_type = 'text/plain';
                            }

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_img_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_img_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_img_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'homenotes_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_img_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => HOMENOTE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => $content_type,
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array('Content-Disposition' => 'attachment')
                                    ));


                                    $file_data = array('note_id' => $note_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_HOME_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            ///$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Note successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        $error_ext = rtrim($error, ',');
                        $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
                $this->session->set_flashdata("s_message", 'Note successfully saved.');
                redirect('teacher/user/homeWorks/' . $this->input->post('class_id') . '/' . $this->input->post('section_id') . '/' . $this->input->post('period_id'));
            }
        } else if ($this->input->post('submit') && $this->input->post('submit') == 'Publish') {
            //echo "<pre>";print_r($_POST);die;
            $update_publish_data = $this->Teacher_user_model->update_note_publish_data_home();
            $this->session->set_flashdata("s_message", 'Note successfully Published.');
            redirect('teacher/user/homeWorks/' . $this->input->post('class_id') . '/' . $this->input->post('section_id') . '/' . $this->input->post('period_id'));
        } else {
            $class_id = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $this->uri->segment(4) . '"');
            $sec_id = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $this->uri->segment(5) . '"');
            $subject_id = $this->my_custom_functions->get_particular_field_value(TBL_TIMETABLE, 'subject_id', 'and id = "' . $this->uri->segment(6) . '"');
            $subject = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $subject_id . '"');
            $data['note_detail'] = $this->Teacher_user_model->get_home_note_detail();
            if ($this->session->userdata('query_date') == date('Y-m-d')) {
                $day = $class_id . $sec_id . ', ' . $subject;
            } else {
                $day = date('D d M', strtotime($this->session->userdata('query_date'))).', '.$class_id . $sec_id . ' - ' . $subject;
            }
            $data['page_title'] = $day;
            $data['back_button'] = "";
            $this->load->view('teacher/home_notes', $data);
        }
    }

    function assignment() {
        if ($this->input->post('submit') && $this->input->post('submit') == 'Save') {
            if ($this->session->userdata('query_date') && $this->session->userdata('query_date') != '') {
                if ($this->session->userdata('query_date') == date('Y-m-d')) {
                    $query_date = date('Y-m-d');
                } else {
                    $query_date = $this->session->userdata('query_date');
                }
            }

            $curr_time = date('H:i:s');
            $note_issue_time = strtotime($query_date . ' ' . $curr_time);

            $today_start_date = strtotime($query_date . '00:00:00');
            $today_end_date = strtotime($query_date . '23:59:59');



            $check_existance = $this->my_custom_functions->get_perticular_count(TBL_ASSIGNMENT_NOTE, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and section_id = "' . $this->input->post('section_id') . '" and period_id = "' . $this->input->post('period_id') . '" and note_issuetime >= "' . $today_start_date . '" and note_issuetime <= "' . $today_end_date . '"');
            if ($check_existance == 0) {
                $note_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_id' => $this->input->post('period_id'),
                    'topic_name' => $this->input->post('topic_name'),
                    'classnote_text' => $this->input->post('classnote_text'),
                    'note_issuetime' => $note_issue_time
                );

                $note_id = $this->my_custom_functions->insert_data_last_id($note_data, TBL_ASSIGNMENT_NOTE);
            } else {
                $note_id = $this->my_custom_functions->get_particular_field_value(TBL_ASSIGNMENT_NOTE, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and section_id = "' . $this->input->post('section_id') . '" and period_id = "' . $this->input->post('period_id') . '"');

                $note_data_update = array(
                    'topic_name' => $this->input->post('topic_name'),
                    'classnote_text' => $this->input->post('classnote_text'),
                    'note_issuetime' => $note_issue_time
                );
                $condition = array(
                    'id' => $note_id
                );

                $this->my_custom_functions->update_data($note_data_update, TBL_ASSIGNMENT_NOTE, $condition);
            }
            if ($note_id) {
                if (isset($_FILES['doc_upload'])) {
                    $file_count = count($_FILES['doc_upload']['name']);
                    $file_type_list = $this->config->item('file_type');

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);

                        if ((in_array($ext, $file_type_list))) {

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'assignmentnotes_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => ASSIGNMENTNOTE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('note_id' => $note_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_ASSIGNMENT_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            //$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Note successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        $error_ext = rtrim($error, ',');
                        $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }

                if (isset($_FILES['doc_img_upload'])) {
                    $file_count = count($_FILES['doc_img_upload']['name']);
                    $file_type_list = $this->config->item('file_type');

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_img_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);

                        if ((in_array($ext, $file_type_list))) {

                            if ($ext == 'pdf' || $ext == 'PDF') {
                                $content_type = 'application/pdf';
                            } else {
                                $content_type = 'text/plain';
                            }

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_img_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_img_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_img_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'assignmentnotes_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_img_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => ASSIGNMENTNOTE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => $content_type,
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array('Content-Disposition' => 'attachment')
                                    ));


                                    $file_data = array('note_id' => $note_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_ASSIGNMENT_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            ///$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Note successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        $error_ext = rtrim($error, ',');
                        $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
                $this->session->set_flashdata("s_message", 'Note successfully saved.');
                redirect('teacher/user/assignment/' . $this->input->post('class_id') . '/' . $this->input->post('section_id') . '/' . $this->input->post('period_id'));
            }
        } elseif ($this->input->post('submit') && $this->input->post('submit') == 'Publish') {
            //echo "<pre>";print_r($_POST);die;
            $update_publish_data = $this->Teacher_user_model->update_note_publish_data_assignment();
            $this->session->set_flashdata("s_message", 'Note successfully Published.');
            redirect('teacher/user/assignment/' . $this->input->post('class_id') . '/' . $this->input->post('section_id') . '/' . $this->input->post('period_id'));
        } else {
//             $today_date = array('query_date' => date('Y-m-d'));
//            $this->session->set_userdata($today_date);
            $data['note_detail'] = $this->Teacher_user_model->get_assignment_note_detail();
            $class_id = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $this->uri->segment(4) . '"');
            $sec_id = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $this->uri->segment(5) . '"');
            $subject_id = $this->my_custom_functions->get_particular_field_value(TBL_TIMETABLE, 'subject_id', 'and id = "' . $this->uri->segment(6) . '"');
            $subject = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $subject_id . '"');

            if ($this->session->userdata('query_date') == date('Y-m-d')) {

                $day = $class_id . $sec_id . ', ' . $subject;
                
            } else {
                $day = date('D d M', strtotime($this->session->userdata('query_date'))).', '.$class_id . $sec_id . ' - ' . $subject;
            }
            $data['page_title'] = $day;
            $data['back_button'] = "";
            $this->load->view('teacher/assignment_notes', $data);
        }
    }

    function diary() {

        $check_running_class = $this->my_custom_functions->check_running_class_for_teacher();
        $data['running_class'] = $check_running_class;
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $data['page_title'] = "Search student";
        $data['back_button'] = "";
        $this->load->view('teacher/search_student', $data);
    }

    function search() {

        $check_running_class = $this->my_custom_functions->check_running_class_for_teacher();
        $data['running_class'] = $check_running_class;
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $data['page_title'] = "Search student";
        $data['back_button'] = "";
        $this->load->view('teacher/search_student', $data);
    }

    function getStudentList() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $data['student_list'] = $this->Teacher_user_model->student_list_search();
            $data['page_title'] = "student list";
            if($this->uri->segment(4) == 'src'){
            $data['back_button'] = base_url() . "teacher/user/search"; 
            }else{
            $data['back_button'] = base_url() . "teacher/user/composeDiary";
            }
            $this->load->view('teacher/student_search_list', $data);
        } else {
            $period_id = $this->uri->segment(4);
            $class_detail = $this->my_custom_functions->get_details_from_id($period_id, TBL_TIMETABLE);
            //echo $this->db->last_query();
            //echo "<pre>";print_r($class_detail);die;
            $data['student_list'] = $this->my_custom_functions->get_multiple_data(TBL_STUDENT, 'and class_id = "' . $class_detail['class_id'] . '" and section_id = "' . $class_detail['section_id'] . '"');

            $data['page_title'] = "student list";
            $data['back_button'] = "";
            $this->load->view('teacher/student_search_list', $data);
        }
    }
    
    function searchStudentList() {
        $data['student_list'] = $this->Teacher_user_model->student_list_search();
            $data['page_title'] = "student list";
            $data['back_button'] = base_url() . "teacher/user/composeDiary";
            $this->load->view('teacher/student_search_list', $data);
    }

    function showStudentDetail() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $student_selected = $this->input->post('student_check');
            $student_selected_count = count($student_selected);
            //echo $student_selected_count;die;
            if ($student_selected_count == 1) {
                foreach ($student_selected as $student_id) {
                    $data['student'] = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                }
                $data['page_title'] = "student list";
                $data['back_button'] = "";
                //$this->load->view('teacher/show_student_individual_list', $data);
                redirect('teacher/user/writeDiary/' . $student_id);
            } else {
                $data['student_list'] = $this->input->post('student_check');
                $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" order by id ASC');
                $data['page_title'] = "Write Diary For All Student";
                $data['back_button'] = "";
                $this->load->view('teacher/write_diary_for_all', $data);
            }
        }
    }

    function showStudentIndividualDetails() {
        $student_id = $this->uri->segment(4);
        $data['students'] = $this->Teacher_user_model->get_student_detail($student_id);
        $data['page_title'] = "student Detail";
        $data['back_button'] = base_url()."teacher/user/search";
        //echo "<pre>";print_r($data);die;
        $this->load->view('teacher/show_student_individual_list', $data);
    }

    function composeDiary() {
        $data['page_title'] = "Diary";
        $data['back_button'] = "";
        $search = '';
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            if ($this->input->post('start_date') != '') {
                $start_date = strtotime($this->input->post('start_date') . ' 00:00:00');
                $search .= ' and issue_date >= "' . $start_date . '"';
            }
            if ($this->input->post('end_date') != '') {
                $end_date = strtotime($this->input->post('end_date') . ' 23:59:59');
                $search .= ' and issue_date <= "' . $end_date . '"';
            }
            if ($this->input->post('in_message') && $this->input->post('out_message')) {
                $search .= ' and (status = 1 or status = 2)';
            } else if ($this->input->post('in_message')) {
                $search .= ' and status = 2';
            } else if ($this->input->post('out_message')) {
                $search .= ' and status = 1';
            }
            $data['diary_detail'] = $this->my_custom_functions->get_multiple_data(TBL_DIARY, ' and teacher_id = "' . $this->session->userdata('teacher_id') . '" ' . $search . ' order by id desc ');
            //echo $this->db->last_query();die;
        } else {
            $today = strtotime(date('Y-m-d') . ' 23:59:59');
            $seven_days_past = strtotime(date('Y-m-d', strtotime('-7 days')) . ' 23:59:59');
            $data['diary_detail'] = $this->my_custom_functions->get_multiple_data(TBL_DIARY, ' and teacher_id = "' . $this->session->userdata('teacher_id') . '" and issue_date >= "' . $seven_days_past . '" and issue_date <= "' . $today . '"  and status = 2 order by id desc ');
        }
//        echo $today = date('Y-m-d').' 00:00:00';echo "<br>";
//       echo $seven_days_past = date('Y-m-d', strtotime('-7 days')).' 23:59:59';
        //$data['diary_detail'] = $this->my_custom_functions->get_multiple_data(TBL_DIARY, ' and teacher_id = "' . $this->session->userdata('teacher_id') . '" and status = 2 order by id desc ');
        $check_running_class = $this->my_custom_functions->check_running_class_for_teacher();
        $data['running_class'] = $check_running_class;
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

        $this->load->view('teacher/compose_diary', $data);
    }

    function writeDiary() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $student_id = $this->input->post('student_id');

            $student_list_count = count(json_decode($student_id));

            if ($student_list_count > 1) {
                $student_list_count = json_decode($student_id);

                foreach ($student_list_count as $student_id) {
                    $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK,'parent_id','and school_id = "'.$this->session->userdata('school_id').'" and student_id = "'.$student_id.'"');
                    $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                    $diary_data = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'teacher_id' => $this->session->userdata('teacher_id'),
                        'subject_id' => $this->input->post('subject_id'),
                        'student_id' => $student_id,
                        'class_id' => $student_detail['class_id'],
                        'section_id' => $student_detail['section_id'],
                        'roll_no' => $student_detail['roll_no'],
                        'heading' => $this->input->post('heading'),
                        'diary_note' => $this->input->post('diary_note'),
                        'issue_date' => time(),
                        'parent_id' => $parent_id,
                        'status' => 1,
                        'teacher_read_msg' => 2,
                        'parent_read_msg' => 1
                    );
                    $diary_data_insert = $this->my_custom_functions->insert_data($diary_data, TBL_DIARY);
                }
                $this->session->set_flashdata("s_message", 'Diary successfully updated.');
                redirect('teacher/user/diary');
            } else {

                $student_post_id = $this->input->post('student_id');
                $student_detail = $this->my_custom_functions->get_details_from_id($student_post_id, TBL_STUDENT);
                $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK,'parent_id','and school_id = "'.$this->session->userdata('school_id').'" and student_id = "'.$student_id.'"');
                $diary_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'teacher_id' => $this->session->userdata('teacher_id'),
                    'subject_id' => $this->input->post('subject_id'),
                    'student_id' => $student_id,
                    'class_id' => $student_detail['class_id'],
                    'section_id' => $student_detail['section_id'],
                    'roll_no' => $student_detail['roll_no'],
                    'heading' => $this->input->post('heading'),
                    'diary_note' => $this->input->post('diary_note'),
                    'issue_date' => time(),
                    'parent_id' => $parent_id,
                    'status' => 1,
                    'teacher_read_msg' => 2,
                    'parent_read_msg' => 1
                );
                $diary_data_insert = $this->my_custom_functions->insert_data($diary_data, TBL_DIARY);

                $this->session->set_flashdata("s_message", 'Diary successfully updated.');
                redirect("teacher/user/writeDiary/" . $student_id);
            }
        } else {
            $student_id = $this->uri->segment(4);
            $data['student'] = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
            $data['diary_detail'] = $this->my_custom_functions->get_multiple_data(TBL_DIARY, 'and student_id = "' . $student_id . '" and teacher_id = "' . $this->session->userdata('teacher_id') . '" order by id desc');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" order by id ASC');
            $data['page_title'] = "Write Diary";
            $data['back_button'] = base_url() . "teacher/user/composeDiary/search";
            $this->load->view('teacher/write_diary', $data);
        }
    }

    function diaryDetail() {
        $diary_id = $this->uri->segment(4);
        $update_data = array('teacher_read_msg' => 2);
        $condition = array('id' => $diary_id);
        $this->my_custom_functions->update_data($update_data, TBL_DIARY, $condition);

        $data['diary_detail'] = $this->Teacher_user_model->get_diary_detail($diary_id);
        $data['page_title'] = "Diary Details";
        $data['back_button'] = '';
        //echo "<pre>";print_r($data);die;
        $this->load->view('teacher/diary_detail', $data);
    }

    function get_section_list() {
        $class_id = $this->input->post('class_id');

        $sec_id = $this->input->post('sec_id');
        $get_section_list = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and status = 1');
        echo '<option value="">Select Section</option>';
        foreach ($get_section_list as $sec_list) {
            if ($sec_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['section_name'] . '</option>';
        }
    }

    function editDiary() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $diary_id = $this->input->post('diary_id');
            $student_id = $this->my_custom_functions->get_particular_field_value(TBL_DIARY, 'student_id', 'and id = "' . $diary_id . '"');
            $diary_data = array(
                'subject_id' => $this->input->post('subject_id'),
                'heading' => $this->input->post('heading'),
                'diary_note' => $this->input->post('diary_note')
            );
            $condition = array(
                'id' => $diary_id
            );
            $update = $this->my_custom_functions->update_data($diary_data, TBL_DIARY, $condition);
            if ($update) {
                $this->session->set_flashdata("s_message", 'Diary successfully updated.');
                redirect("teacher/user/writeDiary/" . $student_id);
            } else {
                $this->session->set_flashdata("s_message", 'Diary successfully updated.');
                redirect("teacher/user/writeDiary/" . $student_id);
            }
        } else {
            $diary_id = $this->uri->segment(4);
            $data['dairy_detail'] = $this->my_custom_functions->get_details_from_id($diary_id, TBL_DIARY);
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" order by id ASC');
            $data['page_title'] = "Edit Diary";
            $data['back_button'] = "";
            $this->load->view('teacher/edit_diary', $data);
        }
    }

    function deleteDiary() {
        $diary_id = $this->uri->segment(4);
        $student_id = $this->my_custom_functions->get_particular_field_value(TBL_DIARY, 'student_id', 'and id = "' . $diary_id . '"');
        $condition = array(
            'id' => $diary_id
        );
        $delete = $this->my_custom_functions->delete_data(TBL_DIARY, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Diary successfully updated.');
            redirect("teacher/user/writeDiary/" . $student_id);
        } else {
            $this->session->set_flashdata("s_message", 'Diary successfully updated.');
            redirect("teacher/user/writeDiary/" . $student_id);
        }
    }

    function changePassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $old_pass = $this->input->post('oldpass');
            $new_pass = $this->input->post('newpass');
            $confirm_pass = $this->input->post('confirmpass');

            $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE id = '" . $this->session->userdata('teacher_id') . "' AND status=1";
            $query = $this->db->query($sql);

            $record = array();
            if ($query->num_rows() > 0) {
                $record = $query->row_array();

                if (password_verify($old_pass, $record['password'])) {
                    $update_data = array(
                        'password' => password_hash($new_pass, PASSWORD_DEFAULT)
                    );
                    $condition = array('id' => $this->session->userdata('teacher_id'));
                    $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
                } else {
                    $this->session->set_flashdata("e_message", 'Old password is wrong.');
                    redirect("teacher/user/changePassword");
                }
            }
            if ($update) {
                $this->session->set_flashdata("s_message", 'Password successfully updated.');
                redirect("teacher/user/changePassword");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong. Please try again later');
                redirect("teacher/user/changePassword");
            }
        } else {
            $data['page_title'] = "Change password";
            $data['back_button'] = "";
            $this->load->view('teacher/change_password', $data);
        }
    }

    function downloadFile() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }
    function downloadHomeFile() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_HOME_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }
    function downloadAssignmentFile() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_ASSIGNMENT_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }

    function downloadFileSyllabus() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_SYLLABUS_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }

    function deleteFile() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');

        $note_id = $this->my_custom_functions->get_particular_field_value(TBL_NOTE_FILES, 'note_id', 'and id = "' . $file_id . '" ');

        $note_detail = $this->my_custom_functions->get_details_from_id($note_id, TBL_NOTE);
        $class_id = $note_detail['class_id'];
        $section_id = $note_detail['section_id'];
        $period_id = $note_detail['period_id'];

        $file_url = $filepath;
        $fileUrlArray = explode("/", $file_url);
        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
        if ($aws_key != "") {
            $s3 = new S3Client(array(
                'version' => 'latest',
                'region' => 'ap-south-1',
                'credentials' => array(
                    'key' => AWS_KEY,
                    'secret' => AWS_SECRET,
                ),
            ));
            $bucket = AMAZON_BUCKET;
            try {
                if ($aws_key != '') {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => CLASSNOTE . '/' . $aws_key
                    ));
                }
                $this->my_custom_functions->delete_data(TBL_NOTE_FILES, array("id" => $file_id));
            } catch (S3Exception $e) {

                $encode[] = array(
                    "msg" => "Operation Failed",
                    "status" => "true"
                );
            }
        }

        $this->session->set_flashdata("s_message", 'Image Deleted.');
        redirect("teacher/user/classWorks/" . $class_id . "/" . $section_id . "/" . $period_id);
    }

    function syllabus() {
        $data = array();
        $data['post_data'] = array();
        if ($this->input->post('class_id')) {
            $class_id = $this->input->post('class_id');
            $data['get_syllabus'] = $this->Teacher_user_model->get_syllabus_classwise($class_id);
            $data['post_data'] = $class_id;
        }

        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = "Syllabus";
        $data['back_button'] = "";
        $this->load->view('teacher/syllabus', $data);
    }

    function getWeeklyRoutine() {
        $data['page_title'] = "Timetable";
        $data['back_button'] = "";
        $data['schedule'] = $this->Teacher_user_model->get_teacher_weekly_schedule();

        $this->load->view('teacher/weekly_schedule', $data);
    }

    function searchPastClasses() {
        $sessionarray = array('query_date' => $this->input->post('class_date'));
        $this->session->set_userdata($sessionarray);
        $query_date = strtotime($this->input->post('class_date'));

        $day = date('w', $query_date);
        $teacher_id = $this->session->userdata('teacher_id');
        //$get_todays_classes = $this->Teacher_user_model->get_todays_classes_search($teacher_id,$day);
        //echo "<pre>";print_r($this->session->all_userdata());die;
        redirect('teacher/user/todaysClasses/1');
    }

    function getNotice() {
        $data['school_notice'] = $this->Teacher_user_model->get_noice_for_teacher();
        $data['page_title'] = "Notice";
        $data['back_button'] = "";
        $this->load->view('teacher/school_notice', $data);
    }

    function noticeDetail() {
        $notice_id = $this->uri->segment(4);
        $data['school_notice_detail'] = $this->Teacher_user_model->get_school_notice_detail($notice_id);
        $data['page_title'] = "Notice Details";
        $data['back_button'] = "";
        //echo "<pre>";print_r($data);die;
        $this->load->view('teacher/notice_detail', $data);
    }

    function updateProfile() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $teacher_id = $this->session->userdata('teacher_id');
            $teacher_data = array(
                'phone_no' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'name' => $this->input->post('name'),
            );


            $condition = array(
                'id' => $teacher_id
            );
            //echo "<pre>";print_r($teacher_data);die;
            $edit_teacher_data = $this->my_custom_functions->update_data($teacher_data, TBL_TEACHER, $condition);
            if ($this->input->post('del_img')) {

                $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER_FILES, 'file_url', 'and teacher_id = "' . $teacher_id . '" ');
                $file_url = $filepath;
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => TEACHER_FOLD . '/' . $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
            /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

            if ($_FILES AND $_FILES['adminphoto']['name']) {



                $timestamp = strtotime(date('Y-m-d H:i:s'));
                $emp_name = str_replace(' ', '', $this->input->post("name"));

                $mime_type = $_FILES['adminphoto']['type'];
                $split = explode('/', $mime_type);
                $type = $split[1];

                $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                $img_width = IMAGE_WIDTH;
                $img_height = IMAGE_HEIGHT;

                if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                    $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);

                    $ImageSize = filesize($temp_file); /* get the image size */

                    //if ($ImageSize < ALLOWED_FILE_SIZE) {

                    $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                    // Instantiate an Amazon S3 client.
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));

                    $bucket = AMAZON_BUCKET;

                    try {
                        $result = $s3->putObject(array(
                            'Bucket' => $bucket,
                            'Key' => TEACHER_FOLD . '/' . $emp_name . '_' . $timestamp,
                            'SourceFile' => $temp_file,
                            'ContentType' => 'text/plain',
                            'ACL' => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY',
                            'Metadata' => array()
                        ));

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }

                        //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
                        $this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
                        //$this->School_user_model->update_employee_photo($teacher_id, $result['ObjectURL']);
                        $this->my_custom_functions->insert_data(array("teacher_id" => $teacher_id, 'file_url' => $result['ObjectURL']), TBL_TEACHER_FILES);
                    } catch (S3Exception $e) {
                        //echo $e->getMessage() . "\n";
                    }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                } else {
                    $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                }
            }
            $this->session->set_flashdata("s_message", 'Profile successfully updated.');
            redirect('teacher/user/updateProfile');
        } else {
            $data['profile_detail'] = $this->my_custom_functions->get_details_from_id($this->session->userdata('teacher_id'), TBL_TEACHER);
            $data['page_title'] = "Update Profile";
            $data['back_button'] = "";
            $this->load->view('teacher/update_profile', $data);
        }
    }

    /////////////////////////////////////////////////////////////////////
    /////////// SCHOOL HOLIDAY
    ////////////////////////////////////////////////////////////////////
    function schoolCalander() {
        $school_id = $this->session->userdata('school_id');
        $data['school_calender'] = $this->Teacher_user_model->get_school_calender($school_id);
        $data['page_title'] = 'School Calendar';
        $data['back_button'] = '';
        //echo "<pre>";print_r($data);die;
        $this->load->view('teacher/school_calender', $data);
    }

    function attendanceReport() {

        $data['page_title'] = 'Attendance Report';
        $data['back_button'] = '';
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $this->load->view('teacher/attendance_report', $data);
    }

    function generateAttendanceData() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $data['page_title'] = 'Attendance Report';
            $data['back_button'] = base_url() . 'teacher/user/attendanceReport';
            $data['attendance_data'] = $this->Teacher_user_model->get_student_attendance_list();
            $this->load->view('teacher/attendance_list', $data);
        }
    }

    function attendance_detail() {
        $data = array();
        $student_id = $this->uri->segment(4);

        $month = $this->uri->segment(4);
        $year = date('Y');

        $date = $this->Teacher_user_model->get_attendance_detail($month, $year);
        foreach ($date as $year => $atten_data) {
            foreach ($atten_data as $month => $attenda_data) {
                $data['atten_data_list'][] = array(
                    'year' => $year,
                    'month' => $month,
                    'event' => $attenda_data
                );
            }
        }
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = "Attendance Details";
        $data['back_button'] = base_url() . 'teacher/user/attendanceReport';
        $this->load->view('teacher/attendance_detail', $data);
    }
    
    function location() {
        $data['page_title'] = "Location";
        $data['back_button'] = '';
        $this->load->view('teacher/location',$data);
    }

    function items() {
        $this->load->view('teacher/html_items');
    }

}
