<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_school_security();
        $this->load->model("school/School_user_model");
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {

        $data['no_of_students'] = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $data['no_of_parents'] = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $data['no_of_parents_active'] = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and id = "' . $this->session->userdata('school_id') . '" and type = "3" and  otp != "0"');
        $data['events'] = $this->my_custom_functions->get_multiple_data(TBL_HOLIDAY, 'and school_id = "' . $this->session->userdata('school_id') . '" and start_date >= ' . date('Y-m-d') . ' order by start_date ASC limit 0,3');
        $data['previous_week_data'] = $this->School_user_model->get_previous_week_data();
        $data['current_week_data'] = $this->School_user_model->get_current_week_data();
        $data['attendance_count'] = $this->School_user_model->get_attendance_count();
        $data['prev_week_acivity_data'] = $this->School_user_model->get_previous_week_activity_data();
        $data['current_week_acivity_data'] = $this->School_user_model->get_current_week_activity_data();
        $data['activity_count'] = $this->School_user_model->get_activity_count();

        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata('school_id')));
        $this->load->view("school/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Change password from admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function changePassword() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            //$this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/changePassword");
            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, "password", " and id='" . $this->session->userdata('school_id') . "'");

                if (password_verify($o_password, $password)) {

                    $data = array(
                        "password" => password_hash($this->input->post("n_password"), PASSWORD_DEFAULT)
                    );

                    $table = TBL_COMMON_LOGIN;

                    $where = array(
                        "id" => $this->session->userdata('school_id')
                    );
                    $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("school/user/changePassword");
                } else {
                    $this->session->set_flashdata("e_message", "Old password is incorrect.");
                    redirect("school/user/changePassword");
                }
            }
        } else {

            $data['active_menu'] = '';
            $this->load->view("school/change_password", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_profile() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            /// Update admin
            

            $school_id = $this->input->post('school_id');
            $update_data = array(
                'name' => $this->input->post('name'),
                'contact_person_name' => $this->input->post('contact_person_name'),
                'mobile_no' => $this->input->post('mobile_no'),
                'email_address' => $this->input->post('email_address'),
                'address' => $this->input->post('address'),
            );
            $condition = array(
                'id' => $this->input->post('school_id'),
            );
            $update = $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);

            if ($this->input->post('del_img')) {
                $filepath = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_LOGO_FILES, 'file_url', 'and school_id = "' . $this->input->post('school_id') . '" ');
                $file_url = $filepath;
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                 if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => SCHOOL_LOGO . '/' . $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data(TBL_SCHOOL_LOGO_FILES, array("school_id" => $this->input->post('school_id')));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
            
             /////////////////////// UPLOAD IMAGE FOR School logo///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        $timestamp = strtotime(date('Y-m-d H:i:s'));
                        $emp_name = str_replace(' ', '', $this->input->post("name"));

                        $mime_type = $_FILES['adminphoto']['type'];
                        $split = explode('/', $mime_type);
                        $type = $split[1];
                        $original_size = getimagesize($_FILES['adminphoto']['tmp_name']);
//                        $img_width = $original_size[0];
//                        $img_height = $original_size[0];
                        $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;
                        $temp_file = 'file_upload/' . $school_id . '_' . $timestamp . '.jpg';
                        

                        if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                            $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                            $ImageSize = filesize($temp_file); /* get the image size */

                            //if ($ImageSize < ALLOWED_FILE_SIZE) {

                            $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => SCHOOL_LOGO . '/' . $school_id . '_' . $timestamp,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                if (file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }

                                //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                $file_data = array('school_id' => $school_id, 'file_url' => $result['ObjectURL']);
                                $this->my_custom_functions->insert_data($file_data, TBL_SCHOOL_LOGO_FILES);
                            } catch (S3Exception $e) {
                                //echo $e->getMessage() . "\n";
                            }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                        } else {
                            $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                        }
                    }
            

                    $this->session->set_flashdata("s_message", "Record has been successfully updated.");
                    redirect("school/user/edit_profile/");
               
           
    }else {

            $data['active_menu'] = '';

            $data['school_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata('school_id')));
            $this->load->view("school/edit_profile", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Reset password(linked with forgot password)
    ///////////////////////////////////////////////////////////////////////////////
    public function reset_password() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('re_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/main/forgot_password");
            } else {
                $data = array(
                    "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                );

                $table = "tbl_admins";

                $where = array(
                    "admin_id" => $this->session->userdata("reset_admin_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_admin_id");

                if ($password_updated) {
                    $this->session->set_flashdata("s_message", 'Password has been changed successfully.<a href="' . base_url() . 'admin">Login here</a>.');
                    redirect("admin/main/forgot_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {

            $admin_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);

            if (isset($admin_id) && isset($code)) {
                $admin_details = $this->my_custom_functions->get_details_from_id("", "tbl_admins", array("admin_id" => $admin_id));

                if ($code == $this->my_custom_functions->encrypt_string($admin_details['email'])) {
                    $this->session->set_userdata("reset_admin_id", $admin_id);
                    $this->load->view("admin/reset_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            } else {
                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("admin/main/forgot_password");
            }
        }
    }

    public function logout() {

        $session_data = array('school_id', 'school_username', 'school_email', 'school_is_logged_in');
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("school");
    }

    //////////////////////////////////////////////////////////////////////
    //////////   CLASSES PART
    //////////////////////////////////////////////////////////////////////

    public function manageClasses() {
        $sql = "Select * from " . TBL_CLASSES . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('classes_sql', $sql);
        $data['classes'] = $this->School_user_model->get_classes_data($sql);
        $this->load->view('school/manage_classes', $data);
    }

    public function addClass() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_name", "Class Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addClass");
            } else { /// Update admin
                $class_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_name' => $this->input->post('class_name'),
                    'status' => $this->input->post('status'),
                );

                $add_class_data = $this->my_custom_functions->insert_data($class_data, TBL_CLASSES);
                if ($add_class_data) {
                    $this->session->set_flashdata("s_message", 'Class added successfully.');
                    redirect("school/user/manageClasses");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageClasses");
                }
            }
        } else {

            $this->load->view('school/add_class');
        }
    }

    public function editClass() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_name", "Class Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $class_id);
            } else { /// Update class
                $class_data = array(
                    'class_name' => $this->input->post('class_name'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $class_id
                );

                $edit_class_data = $this->my_custom_functions->update_data($class_data, TBL_CLASSES, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Class edited successfully.');
                    redirect("school/user/manageClasses");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageClasses");
                }
            }
        } else {
            $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['class'] = $this->my_custom_functions->get_details_from_id($class_id, TBL_CLASSES);

            $this->load->view('school/edit_class', $data);
        }
    }

    public function deleteClass() {
        $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_class = array('id' => $class_id);
        $delete = $this->my_custom_functions->delete_data(TBL_CLASSES, $delete_class);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Class deleted successfully.');
            redirect("school/user/manageClasses");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageClasses");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   SECTION PART
    //////////////////////////////////////////////////////////////////////
    function manageSections() {
        $sql = "Select * from " . TBL_SECTION . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('section_sql', $sql);
        $data['classes'] = $this->School_user_model->get_section_data($sql);
        $this->load->view('school/manage_section', $data);
    }

    public function addSection() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSection");
            } else { /// Update admin
                $class_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_name' => $this->input->post('section_name'),
                    'status' => $this->input->post('status'),
                );

                $add_section_data = $this->my_custom_functions->insert_data($class_data, TBL_SECTION);
                if ($add_section_data) {
                    $this->session->set_flashdata("s_message", 'Section added successfully.');
                    redirect("school/user/manageSections");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSections");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/add_section', $data);
        }
    }

    public function editSection() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $section_id = $this->input->post('section_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $this->my_custom_functions->ablEncrypt($section_id));
            } else { /// Update class
                $class_data = array(
                    'class_id' => $this->input->post('class_id'),
                    'section_name' => $this->input->post('section_name'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $section_id
                );

                $edit_class_data = $this->my_custom_functions->update_data($class_data, TBL_SECTION, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Section edited successfully.');
                    redirect("school/user/manageSections");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSections");
                }
            }
        } else {
            $section_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['section'] = $this->my_custom_functions->get_details_from_id($section_id, TBL_SECTION);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/edit_section', $data);
        }
    }

    public function deleteSection() {
        $section_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_section = array('id' => $section_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SECTION, $delete_section);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Section deleted successfully.');
            redirect("school/user/manageSections");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSections");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////   SUBJECT PART
    //////////////////////////////////////////////////////////////////////

    public function manageSubjects() {
        $sql = "Select * from " . TBL_SUBJECT . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('subject_sql', $sql);
        $data['subjects'] = $this->School_user_model->get_subject_data($sql);
        $this->load->view('school/manage_subject', $data);
    }

    public function addSubject() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSubject");
            } else { /// Update admin
                $class_data = array(
                    //'class_id' => $this->input->post('class_id'),
                    'school_id' => $this->session->userdata('school_id'),
                    'subject_name' => $this->input->post('subject_name'),
                    //'subject_code' => $this->input->post('subject_code'),
                    'status' => $this->input->post('status'),
                );

                $add_subject_data = $this->my_custom_functions->insert_data($class_data, TBL_SUBJECT);
                if ($add_subject_data) {
                    $this->session->set_flashdata("s_message", 'Subject added successfully.');
                    redirect("school/user/manageSubjects");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSubjects");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_subject', $data);
        }
    }

    public function editSubject() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $subject_id = $this->input->post('subject_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $this->my_custom_functions->ablEncrypt($subject_id));
            } else { /// Update class
                $subject_data = array(
                    //'class_id' => $this->input->post('class_id'),
                    'subject_name' => $this->input->post('subject_name'),
                    //'subject_code' => $this->input->post('subject_code'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $subject_id
                );

                $edit_subject_data = $this->my_custom_functions->update_data($subject_data, TBL_SUBJECT, $condition);
                if ($edit_subject_data) {
                    $this->session->set_flashdata("s_message", 'Subject edited successfully.');
                    redirect("school/user/manageSubjects");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSubjects");
                }
            }
        } else {
            $subject_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['subject'] = $this->my_custom_functions->get_details_from_id($subject_id, TBL_SUBJECT);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_subject', $data);
        }
    }

    public function deleteSubject() {
        $subject_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_class = array('id' => $subject_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SUBJECT, $delete_class);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Subject deleted successfully.');
            redirect("school/user/manageSubjects");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSubjects");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   TEACHER PART
    //////////////////////////////////////////////////////////////////////

    public function manageTeachers() {

        $sql = "Select * from " . TBL_TEACHER . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('teacher_sql', $sql);
        $data['teacher'] = $this->School_user_model->get_teacher_data($sql);
        $this->load->view('school/manage_teacher', $data);
    }

    public function check_username() {
        $username = $this->input->post('username');
        $sql = "Select * from " . TBL_COMMON_LOGIN . " where username = '" . $username . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function addTeacher() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            // echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("username", "Username", "required|trim|is_unique[" . TBL_COMMON_LOGIN . ".username]");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTeacher");
            } else { /// Update admin
                $teacher_login_data = array(
                    'type' => TEACHER,
                    'username' => $this->input->post('username'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => 1,
                );

                $teacher_id = $this->my_custom_functions->insert_data_last_id($teacher_login_data, TBL_COMMON_LOGIN);


                $teacher_data = array(
                    'id' => $teacher_id,
                    'school_id' => $this->session->userdata('school_id'),
                    'staff_type' => $this->input->post('staff_type'),
                    'email' => $this->input->post('email'),
                    'phone_no' => $this->input->post('phone'),
                    'name' => $this->input->post('name'),
                );

                $this->my_custom_functions->insert_data_last_id($teacher_data, TBL_TEACHER);

                if ($teacher_id) {

                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        $timestamp = strtotime(date('Y-m-d H:i:s'));
                        $emp_name = str_replace(' ', '', $this->input->post("name"));

                        $mime_type = $_FILES['adminphoto']['type'];
                        $split = explode('/', $mime_type);
                        $type = $split[1];

                        $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                       $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;

                        if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                            $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);



                            $ImageSize = filesize($temp_file); /* get the image size */

                            //if ($ImageSize < ALLOWED_FILE_SIZE) {

                            $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => TEACHER_FOLD . '/' . $emp_name . '_' . $timestamp,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                if (file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }

                                //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                $file_data = array('teacher_id' => $teacher_id, 'file_url' => $result['ObjectURL']);
                                $this->my_custom_functions->insert_data($file_data, TBL_TEACHER_FILES);
                            } catch (S3Exception $e) {
                                //echo $e->getMessage() . "\n";
                            }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                        } else {
                            $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                        }
                    }

                    $this->session->set_flashdata("s_message", 'Teacher added successfully.');
                    redirect("school/user/manageTeachers");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTeachers");
                }
            }
        } else {
            $this->load->view('school/add_teacher');
        }
    }

    public function editTeacher() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die;
            $teacher_id = $this->input->post('teacher_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTeacher/" . $this->my_custom_functions->ablEncrypt($teacher_id));
            } else { /// Update class
                if ($this->input->post('password')) {
                    $login_detail = array(
                        'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
                    );

                    $condition = array(
                        'id' => $teacher_id
                    );

                    $edit_teacher_login_data = $this->my_custom_functions->update_data($login_detail, TBL_COMMON_LOGIN, $condition);
                }


                if ($this->input->post('status') && $this->input->post('status') != '') {

                    $login_detail_s = array(
                        'status' => $this->input->post('status')
                    );

                    $condition = array(
                        'id' => $teacher_id
                    );

                    $edit_teacher_login_data = $this->my_custom_functions->update_data($login_detail_s, TBL_COMMON_LOGIN, $condition);
                }

                $teacher_data = array(
                    'staff_type' => $this->input->post('staff_type'),
                    'phone_no' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                );


                $condition = array(
                    'id' => $teacher_id
                );
                //echo "<pre>";print_r($teacher_data);die;
                $edit_teacher_data = $this->my_custom_functions->update_data($teacher_data, TBL_TEACHER, $condition);

                ///////////////////////// REMOVE ONLY IMAGE  //////////////////////////////////
                if ($this->input->post('del_img')) {

                    $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER_FILES, 'file_url', 'and teacher_id = "' . $teacher_id . '" ');
                    $file_url = $filepath;
                    $fileUrlArray = explode("/", $file_url);
                    $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                    if ($aws_key != "") {
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));
                        $bucket = AMAZON_BUCKET;
                        try {
                            if ($aws_key != '') {
                                $result = $s3->deleteObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => TEACHER_FOLD . '/' . $aws_key
                                ));
                            }
                            $this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
                        } catch (S3Exception $e) {

                            $encode[] = array(
                                "msg" => "Operation Failed",
                                "status" => "true"
                            );
                        }
                    }
                }
                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    $timestamp = strtotime(date('Y-m-d H:i:s'));
                    $emp_name = str_replace(' ', '', $this->input->post("name"));

                    $mime_type = $_FILES['adminphoto']['type'];
                    $split = explode('/', $mime_type);
                    $type = $split[1];

                    $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                    $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;

                    if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                        $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);

                        $ImageSize = filesize($temp_file); /* get the image size */

                        //if ($ImageSize < ALLOWED_FILE_SIZE) {

                        $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                        // Instantiate an Amazon S3 client.
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));

                        $bucket = AMAZON_BUCKET;

                        try {
                            $result = $s3->putObject(array(
                                'Bucket' => $bucket,
                                'Key' => TEACHER_FOLD . '/' . $emp_name . '_' . $timestamp,
                                'SourceFile' => $temp_file,
                                'ContentType' => 'text/plain',
                                'ACL' => 'public-read',
                                'StorageClass' => 'REDUCED_REDUNDANCY',
                                'Metadata' => array()
                            ));

                            if (file_exists($temp_file)) {
                                @unlink($temp_file);
                            }

                            //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
                            $this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
                            //$this->School_user_model->update_employee_photo($teacher_id, $result['ObjectURL']);
                            $this->my_custom_functions->insert_data(array("teacher_id" => $teacher_id, 'file_url' => $result['ObjectURL']), TBL_TEACHER_FILES);
                        } catch (S3Exception $e) {
                            //echo $e->getMessage() . "\n";
                        }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                    } else {
                        $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                    }
                }
                if ($edit_teacher_data) {
                    $this->session->set_flashdata("s_message", 'Teacher edited successfully.');
                    redirect("school/user/manageTeachers");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTeachers");
                }
            }
        } else {
            $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

            $data['teacher'] = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_TEACHER);
            $data['teacher_img_file'] = $this->my_custom_functions->get_details_from_id('', TBL_TEACHER_FILES, array('teacher_id' => $teacher_id));

            $this->load->view('school/edit_teacher', $data);
        }
    }

    public function deleteTeacher() {
        $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER_FILES, 'file_url', 'and teacher_id = "' . $teacher_id . '" ');
        $file_url = $filepath;
        $fileUrlArray = explode("/", $file_url);
        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
        if ($aws_key != "") {
            $s3 = new S3Client(array(
                'version' => 'latest',
                'region' => 'ap-south-1',
                'credentials' => array(
                    'key' => AWS_KEY,
                    'secret' => AWS_SECRET,
                ),
            ));
            $bucket = AMAZON_BUCKET;
            try {
                if ($aws_key != '') {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => TEACHER_FOLD . '/' . $aws_key
                    ));
                }
                $this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
            } catch (S3Exception $e) {

                $encode[] = array(
                    "msg" => "Operation Failed",
                    "status" => "true"
                );
            }
        }

        $delete_teacher_diary = array('teacher_id' => $teacher_id);
        $delete_diary = $this->my_custom_functions->delete_data(TBL_DIARY, $delete_teacher_diary);


        $delete_teacher_login = array('id' => $teacher_id);
        $delete_log = $this->my_custom_functions->delete_data(TBL_COMMON_LOGIN, $delete_teacher_login);

        $delete_teacher = array('id' => $teacher_id);
        $delete = $this->my_custom_functions->delete_data(TBL_TEACHER, $delete_teacher);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Teacher deleted successfully.');
            redirect("school/user/manageTeachers");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageTeachers");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   PERIOD PART
    //////////////////////////////////////////////////////////////////////

    public function managePeriods() {

        $sql = "Select * from " . TBL_PERIODS . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('period_sql', $sql);
        $data['period'] = $this->School_user_model->get_period_data($sql);
        $this->load->view('school/manage_period', $data);
    }

    public function addPeriod() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("class_id", "Select Class", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addPeriod");
            } else { /// Update admin
                $max_sort_query = $this->db->query("select max(sort_order) as max_sort_id from " . TBL_PERIODS . "");
                $max_sort = $max_sort_query->row();
                $max_sort_id = $max_sort->max_sort_id + 1;

                $period_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    //'class_id' => $this->input->post('class_id'),
                    'period_name' => $this->input->post('period_name'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'status' => $this->input->post('status'),
                    'sort_order' => $max_sort_id,
                );
                if ($this->input->post('lunch_break')) {
                    $period_data['lunch_break'] = $this->input->post('lunch_break');
                }

                $add_period_data = $this->my_custom_functions->insert_data($period_data, TBL_PERIODS);
                if ($add_period_data) {
                    $this->session->set_flashdata("s_message", 'Period added successfully.');
                    redirect("school/user/managePeriods");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/managePeriods");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/add_period', $data);
        }
    }

    public function editPeriod() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $period_id = $this->input->post('period_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Select Class", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editPeriod/" . $period_id);
            } else { /// Update class
                $period_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'period_name' => $this->input->post('period_name'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $period_id
                );

                $edit_period_data = $this->my_custom_functions->update_data($period_data, TBL_PERIODS, $condition);
                if ($edit_period_data) {
                    $this->session->set_flashdata("s_message", 'Period edited successfully.');
                    redirect("school/user/managePeriods");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/managePeriods");
                }
            }
        } else {
            $subject_id = $this->uri->segment(4);
            $data['period'] = $this->my_custom_functions->get_details_from_id($subject_id, TBL_PERIODS);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/edit_period', $data);
        }
    }

    public function deletePeriod() {
        $period_id = $this->uri->segment(4);
        $delete_period = array('id' => $period_id);
        $delete = $this->my_custom_functions->delete_data(TBL_PERIODS, $delete_period);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Period deleted successfully.');
            redirect("school/user/managePeriods");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/managePeriods");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   MANAGE TIME TABLE PART
    //////////////////////////////////////////////////////////////////////

    public function manageTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $data['period'] = $this->School_user_model->get_time_table_data($class_id, $section_id);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['post_data'] = array('class_id' => $class_id, 'section_id' => $section_id);
        } else {
            $data['period'] = array();
            $data['post_data'] = array('class_id' => '', 'section_id' => '');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        }
        $this->load->view('school/manage_time_table', $data);
    }

    public function addTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die; 
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTimeTable");
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }


                $routine_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'day_id' => $this->input->post('day_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'attendance_class' => $attendance_period,
                    'status' => 1,
                );

                $add_routine_data = $this->my_custom_functions->insert_data($routine_data, TBL_TIMETABLE);
                if ($add_routine_data) {
                    $this->session->set_flashdata("s_message", 'Routine added successfully.');
                    redirect("school/user/manageTimeTable");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTimeTable");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '" and staff_type = 1');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_time_table', $data);
        }
    }

    function get_section_list() {
        $class_id = $this->input->post('class_id');

        $sec_id = $this->input->post('sec_id');
        $get_section_list = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and status = 1');
        echo '<option value="">Select Section</option>';
        foreach ($get_section_list as $sec_list) {
            if ($sec_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['section_name'] . '</option>';
        }
    }

    function get_subject_list_classwise() {
        $class_id = $this->input->post('class_id');

        $get_subject_list = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        echo '<option value="">Select Subject</option>';
        foreach ($get_subject_list as $subject_list) {
            echo '<option value="' . $subject_list['id'] . '">' . $subject_list['subject_name'] . '</option>';
        }
    }

    public function editTimeTable() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $routine_id = $this->input->post('routine_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("subject_id", "Subject", "trim|required");
            //$this->form_validation->set_rules("teacher_id", "Teacher", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }


                $routine_data_update = array(
                    'day_id' => $this->input->post('day_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'attendance_class' => $attendance_period,
                );

                //echo "<pre>";print_r($routine_data_update);die;
                $condition = array(
                    'id' => $routine_id
                );
                $update = $this->my_custom_functions->update_data($routine_data_update, TBL_TIMETABLE, $condition);
                if ($update) {
                    $this->session->set_flashdata("s_message", 'Routine successfully updated.');
                    redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
                }
            }
        } else {
            $routine_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['routine_data'] = $this->my_custom_functions->get_details_from_id($routine_id, TBL_TIMETABLE);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_time_table', $data);
        }
    }

    function deleteUsername() {
        $parent_id = $this->input->post('id');
        $student_id = $this->input->post('student_id');
        $child_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $parent_id . '"');
        if ($child_count > 1) {
            $delete_linked_student = array('student_id' => $student_id, 'parent_id' => $parent_id);
            $delete = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_linked_student);
            echo 1;
        } else {
            $delete_linked_student = array('student_id' => $student_id, 'parent_id' => $parent_id);
            $delete = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_linked_student);

            $delete_username = array('id' => $parent_id);
            $delete_user_cmd = $this->my_custom_functions->delete_data(TBL_PARENT, $delete_username);
            echo 1;
        }
    }

    function addMoreUsernames() {
//        echo "<pre>";
//        print_r($_POST);
        $username = $this->input->post('user_phone_no');
        $student_id = $this->input->post('stdnt_id_for_usrname');
        $check_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $username . '"');
        if ($check_exist > 0) {
            //echo "1st";die;
            $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'id', 'and username = "' . $username . '"');
            $insert_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'parent_id' => $parent_id,
                'student_id' => $this->input->post('stdnt_id_for_usrname')
            );
            $insert_link_data = $this->my_custom_functions->insert_data($insert_data, TBL_PARENT_KIDS_LINK);
        } else {
            //echo "2nd";die;
            $insert_username_data = array(
                'type' => PARENTS,
                'username' => $username,
                //'school_id' => $this->session->userdata('school_id'),
                'status' => 1
            );
            $parent_id = $this->my_custom_functions->insert_data_last_id($insert_username_data, TBL_COMMON_LOGIN);
            $insert_parent_data = array(
                'id' => $parent_id,
                'school_id' => $this->session->userdata('school_id'),
                    //'username' => $username
            );
            $insert_parent_id = $this->my_custom_functions->insert_data($insert_parent_data, TBL_PARENT);

            $insert_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'parent_id' => $parent_id,
                'student_id' => $this->input->post('stdnt_id_for_usrname')
            );
            $insert_link_data = $this->my_custom_functions->insert_data($insert_data, TBL_PARENT_KIDS_LINK);
        }
        $this->session->set_flashdata("s_message", 'Username successfully added.');
        redirect('school/user/editStudent/' . $student_id . '#user');
    }

    public function deleteStudent() {
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_FILES, 'file_url', 'and student_id = "' . $student_id . '" ');

        $delete_file_data = array(
            'folder_name' => STUDENT_FOLD,
            'file_url' => $filepath
        );
        $delete_data_insert = $this->my_custom_functions->insert_data($delete_file_data, TBL_DELETE_FILES);

        $delete_student_attachment = array('student_id' => $student_id);
        $delete_attachment = $this->my_custom_functions->delete_data(TBL_STUDENT_FILES, $delete_student_attachment);


        $delete_student_attendance = array('student_id' => $student_id);
        $delete_attendance = $this->my_custom_functions->delete_data(TBL_ATTENDANCE, $delete_student_attendance);

        $delete_student_diary = array('student_id' => $student_id);
        $delete_diary = $this->my_custom_functions->delete_data(TBL_DIARY, $delete_student_attendance);

        $delete_student_parent_link = array('student_id' => $student_id);
        $delete_parent_stdnt_link = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_student_parent_link);

        $delete_student_detail = array('student_id' => $student_id);
        $delete_studnt_del = $this->my_custom_functions->delete_data(TBL_STUDENT_DETAIL, $delete_student_detail);

        $delete_student = array('id' => $student_id);
        $delete = $this->my_custom_functions->delete_data(TBL_STUDENT, $delete_student);


        if ($delete) {
            $this->session->set_flashdata("s_message", 'Teacher deleted successfully.');
            redirect("school/user/manageTeachers");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageTeachers");
        }
    }

    function deleteTimeTable() {

        $timeTableTable_id = $this->input->post('timetable_id');

        $delete_data = array('id' => $timeTableTable_id);
        $delete_timetable = $this->my_custom_functions->delete_data(TBL_TIMETABLE, $delete_data);
        echo 1;
    }

    function manageStudent() {
        $data['students'] = $this->my_custom_functions->get_multiple_data(TBL_STUDENT, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        //echo $this->db->last_query();
        $this->load->view('school/manage_student', $data);
    }
    
    function searchStudent() {
        $data['students'] = $this->School_user_model->search_student();
       $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
       $this->load->view('school/manage_student', $data); 
    }

    function addStudent() {
        $numbers = '';
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("reg_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("date_of_birth", "Date Of Birth", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "trim|required");
            $this->form_validation->set_rules("father_name", "Father Name", "trim|required");
            $this->form_validation->set_rules("mother_name", "Mother Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addStudent");
            } else { /// Update admin
                $registration_no_chk = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and registration_no = "' . $this->input->post('reg_no') . '"');
                if ($registration_no_chk == 0) {
                    $basic_data = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'registration_no' => $this->input->post('reg_no'),
                        'name' => $this->input->post('name'),
                        'dob' => $this->my_custom_functions->database_date($this->input->post('date_of_birth')),
                        'gender' => $this->input->post('gender'),
                        'father_name' => $this->input->post('father_name'),
                        'mother_name' => $this->input->post('mother_name'),
                        'class_id' => $this->input->post('class_id'),
                        'section_id' => $this->input->post('section_id'),
                        'roll_no' => $this->input->post('roll_no'),
                        'status' => $this->input->post('status'),
                    );
                    $student_id = $this->my_custom_functions->insert_data_last_id($basic_data, TBL_STUDENT);
                    $student_detail = array(
                        'student_id' => $student_id,
                        'address' => $this->input->post('address'),
                        'aadhar_no' => $this->input->post('aadhar_no'),
                        'emergency_contact_person' => $this->input->post('emg_contact_person'),
                        'emergency_contact_no' => $this->input->post('emg_contact_no'),
                        'blood_group' => $this->input->post('blood_group'),
                    );
                    $student_details = $this->my_custom_functions->insert_data($student_detail, TBL_STUDENT_DETAIL);

                    $parent_username = $this->input->post('contact_no');
                    if (!empty($parent_username)) {
                        foreach ($parent_username as $number) {
                            $username_exist_for_teacher = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $number . '" and type = "' . TEACHER . '"');
                            if ($username_exist_for_teacher > 0) {
                                $username_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $number . '" and type = "' . PARENTS . '"');
                                if ($username_exist > 0) {
                                    $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'id', 'and username = "' . $number . '" and type = "' . PARENTS . '"');
                                } else {
                                    $parent_username_data = array(
                                        'type' => PARENTS,
                                        'username' => $number,
                                        'status' => 1
                                    );
                                    $parent_id = $this->my_custom_functions->insert_data_last_id($parent_username_data, TBL_COMMON_LOGIN);
                                }

                                $parent_detail_data = array(
                                    'id' => $parent_id,
                                    'school_id' => $this->session->userdata('school_id'),
                                );
                                $parent_student_link_data = $this->my_custom_functions->insert_data($parent_detail_data, TBL_PARENT);

                                $parent_student_link = array(
                                    'school_id' => $this->session->userdata('school_id'),
                                    'parent_id' => $parent_id,
                                    'student_id' => $student_id
                                );
                                $parent_student_link_data = $this->my_custom_functions->insert_data($parent_student_link, TBL_PARENT_KIDS_LINK);
                            } else {
                                $numbers .= $number . ',';
                                $ph_nos = rtrim($numbers, ',');
                                $this->session->set_flashdata("s_message", $ph_nos . ' number(s) are already used by another user. Please use enter different username');
                                redirect("school/user/manageStudent");
                            }
                        }
                    }



                    if ($student_id) {

                        /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                        if ($_FILES AND $_FILES['adminphoto']['name']) {


                            $timestamp = strtotime(date('Y-m-d H:i:s'));
                            $emp_name = str_replace(' ', '', $this->input->post("name"));

                            $mime_type = $_FILES['adminphoto']['type'];
                            $split = explode('/', $mime_type);
                            $type = $split[1];

                            $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                            $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;

                            if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                                $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);

                                $ImageSize = filesize($temp_file); /* get the image size */

                                //if ($ImageSize < ALLOWED_FILE_SIZE) {

                                $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => STUDENT_FOLD . '/' . $emp_name . '_' . $timestamp,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));

                                    if (file_exists($temp_file)) {
                                        @unlink($temp_file);
                                    }

                                    //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                    $file_data = array('student_id' => $student_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_STUDENT_FILES);
                                } catch (S3Exception $e) {
                                    //echo $e->getMessage() . "\n";
                                }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                            } else {
                                $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                            }
                        }




                        $this->session->set_flashdata("s_message", 'Student successfully created.');
                        redirect("school/user/manageStudent");
                    } else {
                        $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                        redirect("school/user/manageStudent");
                    }
                } else {
                    $this->session->set_flashdata("e_message", 'another student already registered with this registration number.');
                    redirect("school/user/addStudent");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_student', $data);
        }
    }

    function checkRegistrationNumber() {
        $school_id = $this->session->userdata('school_id');
        $reg_val = $this->input->post('reg_val');

        $check_registratin_no = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $school_id . '" and registration_no = "' . $reg_val . '"');
        if ($check_registratin_no > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function editStudent() {



        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);die;
            $student_id = $this->input->post('student_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("reg_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("date_of_birth", "Date Of Birth", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "trim|required");
            $this->form_validation->set_rules("father_name", "Father Name", "trim|required");
            $this->form_validation->set_rules("mother_name", "Mother Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addStudent");
            } else { /// Update admin
//                echo "<pre>";
//                print_r($_POST);
//                die;
                $basic_data = array(
                    //'registration_no' => $this->input->post('reg_no'),
                    'name' => $this->input->post('name'),
                    'dob' => $this->my_custom_functions->database_date($this->input->post('date_of_birth')),
                    'gender' => $this->input->post('gender'),
                    'father_name' => $this->input->post('father_name'),
                    'mother_name' => $this->input->post('mother_name'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'roll_no' => $this->input->post('roll_no'),
                    'status' => $this->input->post('status'),
                );
                $condition = array('id' => $student_id);
                $this->my_custom_functions->update_data($basic_data, TBL_STUDENT, $condition);

                $student_detail = array(
                    //'student_id' => $student_id,
                    'address' => $this->input->post('address'),
                    'aadhar_no' => $this->input->post('aadhar_no'),
                    'emergency_contact_person' => $this->input->post('emg_contact_person'),
                    'emergency_contact_no' => $this->input->post('emg_contact_no'),
                    'blood_group' => $this->input->post('blood_group'),
                );
                $condition_det = array('student_id' => $student_id);
                $student_details = $this->my_custom_functions->update_data($student_detail, TBL_STUDENT_DETAIL, $condition_det);
                //echo $student_id;die;
                if ($student_id) {

                    ///////////////////////// REMOVE ONLY IMAGE  //////////////////////////////////
                    if ($this->input->post('del_img')) {

                        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_FILES, 'file_url', 'and student_id = "' . $student_id . '" ');
                        $file_url = $filepath;
                        $fileUrlArray = explode("/", $file_url);
                        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                        if ($aws_key != "") {
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));
                            $bucket = AMAZON_BUCKET;
                            try {
                                if ($aws_key != '') {
                                    $result = $s3->deleteObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => STUDENT_FOLD . '/' . $aws_key
                                    ));
                                }
                                $this->my_custom_functions->delete_data(TBL_STUDENT_FILES, array("student_id" => $student_id));
                            } catch (S3Exception $e) {

                                $encode[] = array(
                                    "msg" => "Operation Failed",
                                    "status" => "true"
                                );
                            }
                        }
                    }

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////
                    //echo "<pre>";print_r($_FILES);die;
                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        $timestamp = strtotime(date('Y-m-d H:i:s'));
                        $emp_name = str_replace(' ', '', $this->input->post("name"));

                        $mime_type = $_FILES['adminphoto']['type'];
                        $split = explode('/', $mime_type);
                        $type = $split[1];

                        $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                        $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;

                        if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                            $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                            $ImageSize = filesize($temp_file); /* get the image size */

                            //if ($ImageSize < ALLOWED_FILE_SIZE) {

                            $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => STUDENT_FOLD . '/' . $emp_name . '_' . $timestamp,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                if (file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }

                                //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                $this->my_custom_functions->delete_data(TBL_STUDENT_FILES, array("student_id" => $student_id));
                                //$this->School_user_model->update_employee_photo($teacher_id, $result['ObjectURL']);
                                $this->my_custom_functions->insert_data(array("student_id" => $student_id, 'file_url' => $result['ObjectURL']), TBL_STUDENT_FILES);
                            } catch (S3Exception $e) {
                                //echo $e->getMessage() . "\n";
                            }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                        } else {
                            $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                        }
                    }



                    $this->session->set_flashdata("s_message", 'Students record successfully updated.');
                    redirect("school/user/manageStudent");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageStudent");
                }
            }
        } else {
            $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['get_student_detail'] = $this->School_user_model->get_student_detail($student_id);
            $data['student_img_file'] = $this->my_custom_functions->get_details_from_id('', TBL_STUDENT_FILES, array('student_id' => $student_id));
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_student', $data);
        }
    }

    function manageNotice() {
        $data['notice'] = $this->my_custom_functions->get_multiple_data(TBL_NOTICE, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $this->load->view('school/manage_notice', $data);
    }

    function issueNoice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $timestamp = time();

            if ($this->input->post('type') == 1 || $this->input->post('type') == 2) {
                $data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'issue_date' => date('Y-m-d'),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                } else {
                    $data['publish_date'] = date('Y-m-d');
                }
            } else {
                $class_list_implode = '';

                $class_list_array = $this->input->post('class');
                foreach ($class_list_array as $class_list) {
                    $class_list_implode .= $class_list . ',';
                }
                $finalclass_list = rtrim($class_list_implode, ',');
                $data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'issue_date' => date('Y-m-d'),
                    'notice_text' => $this->input->post('notice_text'),
                    'class_id' => $finalclass_list,
                    'status' => '1'
                );

                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                } else {
                    $data['publish_date'] = date('Y-m-d');
                }
            }

            $notice_id = $this->my_custom_functions->insert_data_last_id($data, TBL_NOTICE);
            //$notice_id = 5;
            if ($notice_id) {
                if (isset($_FILES['doc_upload'])) {
                    $file_type_list = $this->config->item('file_type');
                    $file_count = count($_FILES['doc_upload']['name']);
                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if ((in_array($ext, $file_type_list))) {

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                //$key = '';
//                                if (
//                                        $ext == "jpg" ||
//                                        $ext == "jpeg" ||
//                                        $ext == "png") {
//                                    $key .= 'image_';
//                                } else if (
//                                        $ext == "pdf" ||
//                                        $ext == "doc" ||
//                                        $ext == "docx" ||
//                                        $ext == "xls" ||
//                                        $ext == "xlsx" ||
//                                        $ext == "pptx" ||
//                                        $ext == "ppt"
//                                ) {
//                                    $key .= 'notice_';
//                                }
                                $key = $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => NOTICE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('notice_id' => $notice_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_NOTICE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            //$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Notice successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        //$error_ext = rtrim($error, ',');
                        //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
            }
            redirect("school/user/manageNotice");
        } else {
            $this->load->view('school/issue_notice');
        }
    }

    function get_class_list() {
        $school_id = $this->session->userdata('school_id');
        $class_list = $this->School_user_model->get_class_list($school_id);
        $db_class = $this->input->post('class_id');
        $checked = '';
        if ($db_class != '') {
            $class_list_exp = explode(',', $db_class);
        }
        if (!empty($class_list)) {
            echo '<div class="custom-control custom-checkbox custom-control-inline mb-5"><input class="custom-control-input" type="checkbox" name="example-inline-checkboxall" id="checkboxall" value="0">
                  <label class="custom-control-label" for="checkboxall">Select All</label>
                  </div><br>';
            foreach ($class_list as $row) {
                if (!empty($class_list_exp)) {
                    $checked = '';
                    foreach ($class_list_exp as $class_idd) {
                        if ($class_idd == $row['id']) {
                            $checked = 'checked="checked"';
                        }
                    }
                } else {
                    $checked = '';
                }
                echo '<div class="custom-control custom-checkbox custom-control-inline mb-5"><input class="custom-control-input checkboxTeacher" type="checkbox" name="class[]" id="example-inline-checkbox' . $row['id'] . '" value="' . $row['id'] . '" ' . $checked . '>
                  <label class="custom-control-label" for="example-inline-checkbox' . $row['id'] . '"">' . $row['class_name'] . '</label>
                  </div>';
            }
        }
    }

    function editNotice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);

            if ($this->input->post('type') == 1 || $this->input->post('type') == 2) {
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    //'issue_date' => $this->my_custom_functions->database_date($this->input->post('date_of_issue')),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                }
            } else {
                $class_list_implode = '';

                $class_list_array = $this->input->post('class');
                foreach ($class_list_array as $class_list) {
                    $class_list_implode .= $class_list . ',';
                }
                $finalclass_list = rtrim($class_list_implode, ',');
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    //'issue_date' => $this->my_custom_functions->database_date($this->input->post('date_of_issue')),
                    'notice_text' => $this->input->post('notice_text'),
                    'class_id' => $finalclass_list,
                    'status' => 1
                );

                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                }
            }
            $condition = array('id' => $this->input->post('notice_id'));

            //UPLOAD FILES;
            $this->my_custom_functions->update_data($data, TBL_NOTICE, $condition);



            $notice_id = $this->input->post('notice_id');
            //echo "<pre>";print_r($_FILES);
            if ($_FILES['doc_upload']['name'] != '') {
                $file_type_list = $this->config->item('file_type');
                $file_count = count($_FILES['doc_upload']['name']);
                $success_count = 0;
                $error_count = 0;
                $error = '';
                $errorAttch = 0;
                $successfulAttch = 0;
                for ($i = 0; $i < $file_count; $i++) {

                    echo $filename = $_FILES['doc_upload']['name'][$i];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if ((in_array($ext, $file_type_list))) {

                        $uploaddir = 'file_upload/';
                        $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                        move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                        $temp_file = $uploadfile;


                        $fileSize = $_FILES['doc_upload']['size'][$i];
                        if ($fileSize <= ALLOWED_FILE_SIZE) {

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;
                            $key = '';
                            if (
                                    $ext == "jpg" ||
                                    $ext == "jpeg" ||
                                    $ext == "png") {
                                $key .= 'image_';
                            } else if (
                                    $ext == "pdf" ||
                                    $ext == "doc" ||
                                    $ext == "docx" ||
                                    $ext == "xls" ||
                                    $ext == "xlsx" ||
                                    $ext == "pptx" ||
                                    $ext == "ppt"
                            ) {
                                $key .= 'notice_';
                            }
                            $timestamp = time();
                            $key .= $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                            //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                            //echo $key;die;
                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => NOTICE . '/' . $key,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));


                                $file_data = array('notice_id' => $notice_id, 'file_url' => $result['ObjectURL']);
                                $this->my_custom_functions->insert_data($file_data, TBL_NOTICE_FILES);

                                $successfulAttch++;
                            } catch (S3Exception $e) {

                                $errorAttch++;
                            }

                            $success_count++;
                        } else {
                            $errorAttch++;
                        }
                    } else {
                        //$error_count++;
                        //$error .= '.' . $ext . ',';
                    }

                    if (file_exists($temp_file)) {
                        @unlink($temp_file);
                    }
                }



                if ($success_count > 0) {
                    $this->session->set_flashdata("s_message", 'Notice successfully added.');
                    //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                }
                if ($error_count > 0) {
                    $error_ext = rtrim($error, ',');
                    $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                }
            }





            redirect("school/user/manageNotice");
        } else {
            $notice_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

            $data['notice_detail'] = $this->my_custom_functions->get_details_from_id($notice_id, TBL_NOTICE);
            $data['notice_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_NOTICE_FILES, 'and notice_id = "' . $notice_id . '"');
            $this->load->view('school/edit_notice', $data);
        }
    }

    function deleteNotice() {
        $notice_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $notice_file_list_data = $this->my_custom_functions->get_multiple_data(TBL_NOTICE_FILES, 'and notice_id = "' . $notice_id . '"');

        if (!empty($notice_file_list_data)) {
            foreach ($notice_file_list_data as $notice_file_list) {
                $file_url = $notice_file_list['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => NOTICE . '/' . $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data(TBL_NOTICE_FILES, array("id" => $notice_file_list['id']));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
        }
        $condition = array('id' => $notice_id);

        $delete = $this->my_custom_functions->delete_data(TBL_NOTICE, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Notice successfully deleted.');
            redirect("school/user/manageNotice");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/manageNotice");
        }
    }

    function deleteIndividualFile() {
        $indv_file_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $notice_file_list = $this->my_custom_functions->get_details_from_id($indv_file_id, TBL_NOTICE_FILES);

        if (!empty($notice_file_list)) {
            //foreach ($notice_file_data as $notice_file_list) {echo $notice_file_list['id'];die;
            $file_url = $notice_file_list['file_url'];
            $fileUrlArray = explode("/", $file_url);
            $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

            if ($aws_key != "") {
                $s3 = new S3Client(array(
                    'version' => 'latest',
                    'region' => 'ap-south-1',
                    'credentials' => array(
                        'key' => AWS_KEY,
                        'secret' => AWS_SECRET,
                    ),
                ));
                $bucket = AMAZON_BUCKET;
                try {
                    if ($aws_key != '') {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => NOTICE . '/' . $aws_key
                        ));
                    }
                    $delete = $this->my_custom_functions->delete_data(TBL_NOTICE_FILES, array("id" => $notice_file_list['id']));
                } catch (S3Exception $e) {

                    $encode[] = array(
                        "msg" => "Operation Failed",
                        "status" => "true"
                    );
                }
            }
            //}
        }

        if ($delete) {
            $this->session->set_flashdata("s_message", 'file successfully deleted.');
            redirect("school/user/editNotice/" . $this->my_custom_functions->ablEncrypt($notice_file_list['notice_id']));
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/editNotice/" . $this->my_custom_functions->ablEncrypt($notice_file_list['notice_id']));
        }
    }

    function manageHolidays() {

        $data['holiday_list'] = $this->my_custom_functions->get_multiple_data(TBL_HOLIDAY, 'and school_id = "' . $this->session->userdata('school_id') . '"');

        $this->load->view('school/manage_holiday', $data);
    }

    function addHolidays() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);
            //echo "<pre>";print_r($_POST);die;

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("office_close", "Office Open", "trim|required");



            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addHolidays");
            } else { /// Update admin
                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'start_date' => $this->my_custom_functions->database_date($this->input->post('from_date')),
                    'office_open' => $this->input->post('office_close'),
                    'status' => 1,
                );
                if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
                    $basic_data['end_date'] = $this->my_custom_functions->database_date($this->input->post('to_date'));
                }
                //echo "<pre>";print_r($basic_data);die;
                $holiday = $this->my_custom_functions->insert_data($basic_data, TBL_HOLIDAY);
                if ($holiday) {
                    $this->session->set_flashdata("s_message", 'Holiday successfully created.');
                    redirect("school/user/manageHolidays");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageHolidays");
                }
            }
        } else {
            $this->load->view('school/add_holiday');
        }
    }

    function editHolidays() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);
            //echo "<pre>";print_r($_POST);die;
            $holiday_id = $this->input->post('holiday_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("office_close", "Office Open", "trim|required");



            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editHolidays/" . $this->my_custom_functions->ablEncrypt($holiday_id));
            } else { /// Update admin
                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'start_date' => $this->my_custom_functions->database_date($this->input->post('from_date')),
                    'office_open' => $this->input->post('office_close'),
                    'status' => $this->input->post('status'),
                );
                if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
                    $basic_data['end_date'] = $this->my_custom_functions->database_date($this->input->post('to_date'));
                }
                //echo "<pre>";print_r($basic_data);die;
                $condition = array('id' => $holiday_id);
                $holiday = $this->my_custom_functions->update_data($basic_data, TBL_HOLIDAY, $condition);
                if ($holiday) {
                    $this->session->set_flashdata("s_message", 'Holiday successfully updated.');
                    redirect("school/user/manageHolidays");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageHolidays");
                }
            }
        } else {
            $holiday_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['holiday_detail'] = $this->my_custom_functions->get_details_from_id($holiday_id, TBL_HOLIDAY);
            $this->load->view('school/edit_holiday', $data);
        }
    }

    //////////////////////////////////////////////////////////
    ////// Syllabus Part
    //////////////////////////////////////////////////////////


    function manageSyllabus() {
        $data['syllabus'] = $this->my_custom_functions->get_multiple_data(TBL_SYLLABUS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $this->load->view('school/manage_syllabus', $data);
    }

    function addSyllabus() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class", "trim|required");




            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSyllabus");
            } else { /// Update admin
                $syllabus = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'syllabus_text' => $this->input->post('syllabus_text'),
                );
                $syllabus_id = $this->my_custom_functions->insert_data_last_id($syllabus, TBL_SYLLABUS);


                if ($syllabus_id) {
                    if (isset($_FILES['doc_upload'])) {
                        $file_type_list = $this->config->item('file_type');
                        $file_count = count($_FILES['doc_upload']['name']);
                        $success_count = 0;
                        $error_count = 0;
                        $error = '';
                        $errorAttch = 0;
                        $successfulAttch = 0;
                        for ($i = 0; $i < $file_count; $i++) {

                            $filename = $_FILES['doc_upload']['name'][$i];
                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            if ((in_array($ext, $file_type_list))) {

                                $uploaddir = 'syllabus_upload/';
                                $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                                move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                                $temp_file = $uploadfile;


                                $fileSize = $_FILES['doc_upload']['size'][$i];
                                if ($fileSize <= ALLOWED_FILE_SIZE) {

                                    // Instantiate an Amazon S3 client.
                                    $s3 = new S3Client(array(
                                        'version' => 'latest',
                                        'region' => 'ap-south-1',
                                        'credentials' => array(
                                            'key' => AWS_KEY,
                                            'secret' => AWS_SECRET,
                                        ),
                                    ));

                                    $bucket = AMAZON_BUCKET;
                                    $key = '';
                                    if (
                                            $ext == "jpg" ||
                                            $ext == "jpeg" ||
                                            $ext == "png") {
                                        $key .= 'image_';
                                    } else if (
                                            $ext == "pdf" ||
                                            $ext == "doc" ||
                                            $ext == "docx" ||
                                            $ext == "xls" ||
                                            $ext == "xlsx" ||
                                            $ext == "pptx" ||
                                            $ext == "ppt"
                                    ) {
                                        
                                    }
                                    $timestamp = time();
                                    $key = 'syllabus_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                    //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                    try {
                                        $result = $s3->putObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => SYLLABUS . '/' . $key,
                                            'SourceFile' => $temp_file,
                                            'ContentType' => 'text/plain',
                                            'ACL' => 'public-read',
                                            'StorageClass' => 'REDUCED_REDUNDANCY',
                                            'Metadata' => array()
                                        ));


                                        $file_data = array('syllabus_id' => $syllabus_id, 'file_url' => $result['ObjectURL']);
                                        $this->my_custom_functions->insert_data($file_data, TBL_SYLLABUS_FILES);

                                        $successfulAttch++;
                                    } catch (S3Exception $e) {

                                        $errorAttch++;
                                    }

                                    $success_count++;
                                } else {
                                    $errorAttch++;
                                }
                            } else {
                                //$error_count++;
                                //$error .= '.' . $ext . ',';
                            }

                            if (file_exists($temp_file)) {
                                @unlink($temp_file);
                            }
                        }



                        if ($success_count > 0) {
                            $this->session->set_flashdata("s_message", 'Syllabus successfully added.');
                            //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                        }
                        if ($error_count > 0) {
                            //$error_ext = rtrim($error, ',');
                            //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                        }
                    }
                }


                redirect('school/user/manageSyllabus');
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/add_syllabus', $data);
        }
    }

    function editSyllabus() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editSyllabus/" . $this->my_custom_functions->ablEncrypt($this->input->post('syllabus_id')));
            } else { /// Update admin
                $syllabus_id = $this->input->post('syllabus_id');
                $syllabus = array(
                    'class_id' => $this->input->post('class_id'),
                    'syllabus_text' => $this->input->post('syllabus_text'),
                );
                $condition = array(
                    'id' => $this->input->post('syllabus_id')
                );
                $this->my_custom_functions->update_data($syllabus, TBL_SYLLABUS, $condition);

                if (isset($_FILES['doc_upload'])) {
                    $file_type_list = $this->config->item('file_type');
                    $file_count = count($_FILES['doc_upload']['name']);
                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if ((in_array($ext, $file_type_list))) {

                            $uploaddir = 'syllabus_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'syllabus_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => SYLLABUS . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('syllabus_id' => $syllabus_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_SYLLABUS_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            //$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Syllabus successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        //$error_ext = rtrim($error, ',');
                        //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
                redirect('school/user/manageSyllabus');
            }
        } else {
            $syllabus_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['syllabus_detail'] = $this->my_custom_functions->get_details_from_id($syllabus_id, TBL_SYLLABUS);
            $data['syllabus_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_SYLLABUS_FILES, 'and syllabus_id = "' . $syllabus_id . '"');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/edit_syllabus', $data);
        }
    }

    function deleteSyllabus() {
        $syllabus_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $syllabus_file_list_data = $this->my_custom_functions->get_multiple_data(TBL_SYLLABUS_FILES, 'and syllabus_id = "' . $syllabus_id . '"');

        if (!empty($syllabus_file_list_data)) {
            foreach ($syllabus_file_list_data as $syllabus_file_list) {
                $file_url = $syllabus_file_list['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                //echo $aws_key; die;
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => SYLLABUS . '/' . $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data(TBL_SYLLABUS_FILES, array("id" => $syllabus_file_list['id']));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
        }
        $condition = array('id' => $syllabus_id);

        $delete = $this->my_custom_functions->delete_data(TBL_SYLLABUS, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Syllabus successfully deleted.');
            redirect("school/user/manageSyllabus");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/manageSyllabus");
        }
    }

    function deleteIndividualSyllabusFile() {
        $indv_file_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $syllabus_file_list = $this->my_custom_functions->get_details_from_id($indv_file_id, TBL_SYLLABUS_FILES);

        if (!empty($syllabus_file_list)) {

            //foreach ($notice_file_data as $notice_file_list) {echo $notice_file_list['id'];die;
            $file_url = $syllabus_file_list['file_url'];

            $fileUrlArray = explode("/", $file_url);
            $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

            if ($aws_key != "") {
                $s3 = new S3Client(array(
                    'version' => 'latest',
                    'region' => 'ap-south-1',
                    'credentials' => array(
                        'key' => AWS_KEY,
                        'secret' => AWS_SECRET,
                    ),
                ));
                $bucket = AMAZON_BUCKET;
                try {
                    if ($aws_key != '') {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => SYLLABUS . '/' . $aws_key
                        ));
                    }
                    $delete = $this->my_custom_functions->delete_data(TBL_SYLLABUS_FILES, array("id" => $syllabus_file_list['id']));
                } catch (S3Exception $e) {

                    $encode[] = array(
                        "msg" => "Operation Failed",
                        "status" => "true"
                    );
                }
            }
            //}
        }

        if ($delete) {
            $this->session->set_flashdata("s_message", 'file successfully deleted.');
            redirect("school/user/editSyllabus/" . $this->my_custom_functions->ablEncrypt($syllabus_file_list['syllabus_id']));
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/editSyllabus/" . $this->my_custom_functions->ablEncrypt($syllabus_file_list['syllabus_id']));
        }
    }

    function generalInformation() {
        $data['general_info'] = $this->my_custom_functions->get_multiple_data(TBL_GENERAL_INFO, 'and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
        $this->load->view('school/manage_general_info', $data);
    }

    function addGeneralInformation() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $info_text = array(
                'school_id' => $this->session->userdata('school_id'),
                'info_text' => $this->input->post('info_text'),
                'status' => 1
            );

            $update_info = $this->my_custom_functions->insert_data($info_text, TBL_GENERAL_INFO);

            if ($update_info) {
                $this->session->set_flashdata("s_message", 'Information successfully Added.');
                redirect("school/user/generalInformation");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/generalInformation");
            }
        } else {

            $this->load->view('school/add_info');
        }
    }

    function editGeneralInformation() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $info_id = $this->input->post('info_id');
            $info_text = array(
                'info_text' => $this->input->post('info_text')
            );
            $condition = array('id' => $info_id);
            $update_info = $this->my_custom_functions->update_data($info_text, TBL_GENERAL_INFO, $condition);

            if ($update_info) {
                $this->session->set_flashdata("s_message", 'Information successfully updated.');
                redirect("school/user/generalInformation");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/generalInformation");
            }
        } else {
            $data['general_info'] = $this->my_custom_functions->get_multiple_data(TBL_GENERAL_INFO, 'and status = 1');
            $this->load->view('school/edit_general_info', $data);
        }
    }

    function manageFeesStructure() {
        $data['fees_structure'] = $this->my_custom_functions->get_multiple_data(TBL_FEES_STRUCTURE, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $this->load->view('school/manage_fees_structure', $data);
    }

    function addFeesStructure() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $label = $this->input->post('label');
            $amount = $this->input->post('amount');
            $option = $this->input->post('option');
            $comment = $this->input->post('comment');
            //echo "<pre>";print_r($label);
            foreach ($label as $key => $lab) {
                if ($lab != '') {
                    $data[] = array(
//                        'school_id' => $this->session->userdata('school_id'),
//                        'class_id' => $this->input->post('class_id'),
                        'label' => $lab,
                        'amount' => $amount[$key],
                        'option' => $option[$key]
                    );
                    $convert_data = json_encode($data);
                }
            }
            $fees_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'class_id' => $this->input->post('class_id'),
                'fees_structure' => $convert_data,
                'comment' => $comment
            );

            $insert_fees_structure = $this->my_custom_functions->insert_data($fees_data, TBL_FEES_STRUCTURE);

            if ($insert_fees_structure) {
                $this->session->set_flashdata("s_message", 'Fees Structure successfully Added.');
                redirect("school/user/manageFeesStructure");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/manageFeesStructure");
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_fees_structure', $data);
        }
    }

    function editFeesStructure() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $label = $this->input->post('label');
            $amount = $this->input->post('amount');
            $option = $this->input->post('option');
            $comment = $this->input->post('comment');
            //echo "<pre>";print_r($label);
            foreach ($label as $key => $lab) {
                if ($lab != '') {
                    $data[] = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'class_id' => $this->input->post('class_id'),
                        'label' => $lab,
                        'amount' => $amount[$key],
                        'option' => $option[$key]
                    );
                    $convert_data = json_encode($data);
                }
            }
            $fees_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'class_id' => $this->input->post('class_id'),
                'fees_structure' => $convert_data,
                'comment' => $comment
            );
            $condotion = array(
                'id' => $this->input->post('fees_id')
            );

            $update_fees_structure = $this->my_custom_functions->update_data($fees_data, TBL_FEES_STRUCTURE, $condotion);

            if ($update_fees_structure) {
                $this->session->set_flashdata("s_message", 'Fees Structure successfully Added.');
                redirect("school/user/manageFeesStructure");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/manageFeesStructure");
            }
        } else {
            $fees_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure_detail'] = $this->my_custom_functions->get_details_from_id($fees_id, TBL_FEES_STRUCTURE);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_fees_structure', $data);
        }
    }

    function deleteFeesStructure() {
        $fees_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $condition = array(
            'id' => $fees_id
        );
        $delete = $this->my_custom_functions->delete_data(TBL_FEES_STRUCTURE, $condition);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Fees Structure successfully Deleted.');
            redirect("school/user/manageFeesStructure");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageFeesStructure");
        }
    }

    function downloadFilenotice() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTICE_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }

    function downloadFilesyllabus() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_SYLLABUS_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }
    
    function permission() {
        $staff_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $data["permissions"] = $this->School_user_model->get_permissions();
        $data["login"] = $this->School_user_model->get_login_details($this->uri->segment(4));
        //echo "<pre>";print_r($data);die;
        $this->load->view('school/permission', $data);
    }

    public function check_existing_permission($uid, $pid) {

        $response["exper"] = $this->School_user_model->get_existing_permission($uid, $pid);

        return $response;
    }

}
