<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;
class Main extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model("school/School_main_model");
    }
    
    

    public function index() {
        $this->load->view('school/login');
    }

    public function signUp() {
        $this->load->view('school/sign_up');
    }

    public function createSchool() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("contact_person_name", "Contact Person Name", "trim|required");
            $this->form_validation->set_rules("mobile_no", "Mobile Number", "trim|required");
            $this->form_validation->set_rules("email_address", "Email", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/main/signUp");
            } else {
                //echo "<pre>";print_r($_POST);
                $create_username = $this->my_custom_functions->create_username();
                
                
                $school_login_detail = array(
                    'type' => SCHOOL,
                    'username' => $create_username,
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => 1,
                );
                //echo "<pre>";print_r($school_detail);die;

                $school_id = $this->my_custom_functions->insert_data_last_id($school_login_detail, TBL_COMMON_LOGIN);
                
                
                
                $school_detail = array(
                    'id' => $school_id,
                    'name' => $this->input->post('name'),
                    'contact_person_name' => $this->input->post('contact_person_name'),
                    'mobile_no' => $this->input->post('mobile_no'),
                    'email_address' => $this->input->post('email_address'),
                    'address' => $this->input->post('address'),
                );
                //echo "<pre>";print_r($school_detail);die;
                $add_school = $this->my_custom_functions->insert_data($school_detail, TBL_SCHOOL);
                 /////////////////////// UPLOAD IMAGE FOR School logo///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        $timestamp = strtotime(date('Y-m-d H:i:s'));
                        $emp_name = str_replace(' ', '', $this->input->post("name"));

                        $mime_type = $_FILES['adminphoto']['type'];
                        $split = explode('/', $mime_type);
                        $type = $split[1];
                        $original_size = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $img_width = $original_size[0];
                        $img_height = $original_size[0];
                        $temp_file = 'file_upload/' . $school_id . '_' . $timestamp . '.jpg';
                        

                        if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                            $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                            $ImageSize = filesize($temp_file); /* get the image size */

                            //if ($ImageSize < ALLOWED_FILE_SIZE) {

                            $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => SCHOOL_LOGO . '/' . $school_id . '_' . $timestamp,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                if (file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }

                                //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                $file_data = array('school_id' => $school_id, 'file_url' => $result['ObjectURL']);
                                $this->my_custom_functions->insert_data($file_data, TBL_SCHOOL_LOGO_FILES);
                            } catch (S3Exception $e) {
                                //echo $e->getMessage() . "\n";
                            }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                        } else {
                            $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                        }
                    }

                
                if ($add_school) {
                    $this->session->set_flashdata("s_message", "School successfully created. Your username is " . $create_username . ".");
                    redirect("school/login");
                } else {
                    $this->session->set_flashdata("e_message", "Something went wrong, please try again later");
                    redirect("school/signUp");
                }
            }
        }
    }

    public function login() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("username", "Userame", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                
                redirect("school/login");
            } else {
                
                $response = $this->School_main_model->login_check();
                
                if ($response == "1") { /// If credentials match with system  
                    redirect("school/user/dashBoard");
                } else {
                    $this->session->set_flashdata("e_message", "Invalid username/password.");
                    redirect("school");
                }
            }
        } else {
            if ($this->session->userdata('school_id')) { /// If the admin is already logged in, don't show the login page                
                redirect("school/user/dashBoard");
            }
            $this->load->view('school/login');
        }
    }

}
