<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_school_security();
        $this->load->model("school/School_user_model");
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {


        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata('school_id')));
        $this->load->view("school/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Change password from admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function change_password() {

        if ($this->input->post("change_password") && $this->input->post("change_password") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            $this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/change_password");
            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value("tbl_admins", "password", " and admin_id='" . $this->session->userdata('admin_id') . "'");

                if (password_verify($o_password, $password)) {

                    $data = array(
                        "password" => password_hash($this->input->post("n_password"), PASSWORD_DEFAULT)
                    );

                    $table = "tbl_admins";

                    $where = array(
                        "admin_id" => $this->session->userdata('admin_id')
                    );
                    $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("admin/user/change_password");
                } else {
                    $this->session->set_flashdata("e_message", "Old password is incorrect.");
                    redirect("admin/user/change_password");
                }
            }
        } else {

            $data['active_menu'] = '';
            $this->load->view("admin/change_password", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_profile() {

        if ($this->input->post("save") && $this->input->post("save") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("email", "Account Email", "trim|required|valid_email");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/edit_profile");
            } else { /// Update admin
                $admin_id = $this->session->userdata('admin_id');
                $update = $this->Admin_main_model->update_admin($admin_id);
                $this->load->library('image_lib');
                $this->load->library('upload');

                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                            ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                            ($_FILES['adminphoto']['type'] == 'image/png') ||
                            ($_FILES['adminphoto']['type'] == 'image/gif')) {

                        list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $new_width = "";
                        $new_height = "";

                        $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                        $new_width = 185;
                        $new_height = $new_width / $ratio;

                        $config['image_library'] = 'gd2';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                        $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                        $config['maintain_ratio'] = FALSE;
                        $config['width'] = $new_width;
                        $config['height'] = $new_height;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();


                        $this->session->set_flashdata("s_message", "Record has been successfully updated.");
                        redirect("admin/user/edit_profile/");
                    } else {
                        $this->session->set_flashdata('invalid_img_type', 'Only .jpg, .jpeg, .png, .gif types are allowed');
                        redirect("admin/user/edit_profile/");
                    }
                }

                if ($update) {
                    $this->session->set_flashdata('s_message', "Successfully updated admin details.");
                    redirect("admin/user/edit_profile");
                } else {
                    $this->session->set_flashdata('e_message', "Error updating admin details.");
                    redirect("admin/user/edit_profile");
                }
            }
        } else {

            $data['active_menu'] = '';

            $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", "tbl_admins", array("admin_id" => $this->session->userdata('admin_id')));
            $data['admin_details']['image'] = "<img src='" . base_url() . "_images/superadmin_icon.png'>";
            $this->load->view("admin/edit_profile", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Reset password(linked with forgot password)
    ///////////////////////////////////////////////////////////////////////////////
    public function reset_password() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('re_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/main/forgot_password");
            } else {
                $data = array(
                    "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                );

                $table = "tbl_admins";

                $where = array(
                    "admin_id" => $this->session->userdata("reset_admin_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_admin_id");

                if ($password_updated) {
                    $this->session->set_flashdata("s_message", 'Password has been changed successfully.<a href="' . base_url() . 'admin">Login here</a>.');
                    redirect("admin/main/forgot_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {

            $admin_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);

            if (isset($admin_id) && isset($code)) {
                $admin_details = $this->my_custom_functions->get_details_from_id("", "tbl_admins", array("admin_id" => $admin_id));

                if ($code == $this->my_custom_functions->encrypt_string($admin_details['email'])) {
                    $this->session->set_userdata("reset_admin_id", $admin_id);
                    $this->load->view("admin/reset_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            } else {
                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("admin/main/forgot_password");
            }
        }
    }

    public function logout() {

        $session_data = array('school_id', 'school_username', 'school_email', 'school_is_logged_in');
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("school");
    }

    //////////////////////////////////////////////////////////////////////
    //////////   CLASSES PART
    //////////////////////////////////////////////////////////////////////

    public function manageClasses() {
        $sql = "Select * from " . TBL_CLASSES . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('classes_sql', $sql);
        $data['classes'] = $this->School_user_model->get_classes_data($sql);
        $this->load->view('school/manage_classes', $data);
    }

    public function addClass() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_name", "Class Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addClass");
            } else { /// Update admin
                $class_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_name' => $this->input->post('class_name'),
                    'status' => $this->input->post('status'),
                );

                $add_class_data = $this->my_custom_functions->insert_data($class_data, TBL_CLASSES);
                if ($add_class_data) {
                    $this->session->set_flashdata("s_message", 'Class added successfully.');
                    redirect("school/user/manageClasses");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageClasses");
                }
            }
        } else {

            $this->load->view('school/add_class');
        }
    }

    public function editClass() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_name", "Class Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $class_id);
            } else { /// Update class
                $class_data = array(
                    'class_name' => $this->input->post('class_name'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $class_id
                );

                $edit_class_data = $this->my_custom_functions->update_data($class_data, TBL_CLASSES, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Class edited successfully.');
                    redirect("school/user/manageClasses");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageClasses");
                }
            }
        } else {
            $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['class'] = $this->my_custom_functions->get_details_from_id($class_id, TBL_CLASSES);

            $this->load->view('school/edit_class', $data);
        }
    }

    public function deleteClass() {
        $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_class = array('id' => $class_id);
        $delete = $this->my_custom_functions->delete_data(TBL_CLASSES, $delete_class);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Class deleted successfully.');
            redirect("school/user/manageClasses");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageClasses");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   SECTION PART
    //////////////////////////////////////////////////////////////////////
    function manageSections() {
        $sql = "Select * from " . TBL_SECTION . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('section_sql', $sql);
        $data['classes'] = $this->School_user_model->get_section_data($sql);
        $this->load->view('school/manage_section', $data);
    }

    public function addSection() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSection");
            } else { /// Update admin
                $class_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_name' => $this->input->post('section_name'),
                    'status' => $this->input->post('status'),
                );

                $add_section_data = $this->my_custom_functions->insert_data($class_data, TBL_SECTION);
                if ($add_section_data) {
                    $this->session->set_flashdata("s_message", 'Section added successfully.');
                    redirect("school/user/manageSections");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSections");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and status = 1');
            $this->load->view('school/add_section', $data);
        }
    }

    public function editSection() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $section_id = $this->input->post('section_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $this->my_custom_functions->ablEncrypt($section_id));
            } else { /// Update class
                $class_data = array(
                    'class_id' => $this->input->post('class_id'),
                    'section_name' => $this->input->post('section_name'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $section_id
                );

                $edit_class_data = $this->my_custom_functions->update_data($class_data, TBL_SECTION, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Section edited successfully.');
                    redirect("school/user/manageSections");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSections");
                }
            }
        } else {
            $section_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['section'] = $this->my_custom_functions->get_details_from_id($section_id, TBL_SECTION);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and status = 1');
            $this->load->view('school/edit_section', $data);
        }
    }

    public function deleteSection() {
        $section_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_section = array('id' => $section_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SECTION, $delete_section);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Section deleted successfully.');
            redirect("school/user/manageSections");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSections");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////   SUBJECT PART
    //////////////////////////////////////////////////////////////////////

    public function manageSubjects() {
        $sql = "Select * from " . TBL_SUBJECT . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('subject_sql', $sql);
        $data['subjects'] = $this->School_user_model->get_subject_data($sql);
        $this->load->view('school/manage_subject', $data);
    }

    public function addSubject() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSubject");
            } else { /// Update admin
                $class_data = array(
                    //'class_id' => $this->input->post('class_id'),
                    'school_id' => $this->session->userdata('school_id'),
                    'subject_name' => $this->input->post('subject_name'),
                    'subject_code' => $this->input->post('subject_code'),
                    'status' => $this->input->post('status'),
                );

                $add_subject_data = $this->my_custom_functions->insert_data($class_data, TBL_SUBJECT);
                if ($add_subject_data) {
                    $this->session->set_flashdata("s_message", 'Subject added successfully.');
                    redirect("school/user/manageSubjects");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSubjects");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_subject', $data);
        }
    }

    public function editSubject() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $subject_id = $this->input->post('subject_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $this->my_custom_functions->ablEncrypt($subject_id));
            } else { /// Update class
                $subject_data = array(
                    //'class_id' => $this->input->post('class_id'),
                    'subject_name' => $this->input->post('subject_name'),
                    'subject_code' => $this->input->post('subject_code'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $subject_id
                );

                $edit_subject_data = $this->my_custom_functions->update_data($subject_data, TBL_SUBJECT, $condition);
                if ($edit_subject_data) {
                    $this->session->set_flashdata("s_message", 'Subject edited successfully.');
                    redirect("school/user/manageSubjects");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSubjects");
                }
            }
        } else {
            $subject_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['subject'] = $this->my_custom_functions->get_details_from_id($subject_id, TBL_SUBJECT);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_subject', $data);
        }
    }

    public function deleteSubject() {
        $subject_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_class = array('id' => $subject_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SUBJECT, $delete_class);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Subject deleted successfully.');
            redirect("school/user/manageSubjects");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSubjects");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   TEACHER PART
    //////////////////////////////////////////////////////////////////////

    public function manageTeachers() {

        $sql = "Select * from " . TBL_TEACHER . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('teacher_sql', $sql);
        $data['teacher'] = $this->School_user_model->get_teacher_data($sql);
        $this->load->view('school/manage_teacher', $data);
    }

    public function addTeacher() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            // echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("username", "Username", "required|trim|is_unique[" . TBL_COMMON_LOGIN . ".username]");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTeacher");
            } else { /// Update admin
                $teacher_login_data = array(
                    'type' => TEACHER,
                    'username' => $this->input->post('username'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => 1,
                );

                $teacher_id = $this->my_custom_functions->insert_data_last_id($teacher_login_data, TBL_COMMON_LOGIN);


                $teacher_data = array(
                    'id' => $teacher_id,
                    'school_id' => $this->session->userdata('school_id'),
                    'email' => $this->input->post('email'),
                    'phone_no' => $this->input->post('phone'),
                    'name' => $this->input->post('name'),
                );

                $this->my_custom_functions->insert_data_last_id($teacher_data, TBL_TEACHER);

                if ($teacher_id) {

                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                                ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                                ($_FILES['adminphoto']['type'] == 'image/png') ||
                                ($_FILES['adminphoto']['type'] == 'image/gif')) {

                            list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                            $new_width = "";
                            $new_height = "";

                            $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                            $new_width = 185;
                            $new_height = $new_width / $ratio;

                            $config['image_library'] = 'gd2';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                            $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                            $config['new_image'] = "uploads/teacher/" . $teacher_id . ".jpg";
                            $config['maintain_ratio'] = FALSE;
                            $config['width'] = $new_width;
                            $config['height'] = $new_height;

                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                        }
                    }

                    $this->session->set_flashdata("s_message", 'Teacher added successfully.');
                    redirect("school/user/manageTeachers");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTeachers");
                }
            }
        } else {
            $this->load->view('school/add_teacher');
        }
    }

    public function editTeacher() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die;
            $teacher_id = $this->input->post('teacher_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTeacher/" . $this->my_custom_functions->ablEncrypt($teacher_id));
            } else { /// Update class
                if ($this->input->post('password')) {
                    $login_detail = array(
                        'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
                    );

                    $condition = array(
                        'id' => $teacher_id
                    );

                    $edit_teacher_login_data = $this->my_custom_functions->update_data($login_detail, TBL_COMMON_LOGIN, $condition);
                }


                if ($this->input->post('status') && $this->input->post('status') != '') {

                    $login_detail_s = array(
                        'status' => $this->input->post('status')
                    );

                    $condition = array(
                        'id' => $teacher_id
                    );

                    $edit_teacher_login_data = $this->my_custom_functions->update_data($login_detail_s, TBL_COMMON_LOGIN, $condition);
                }

                $teacher_data = array(
                    'phone_no' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                );


                $condition = array(
                    'id' => $teacher_id
                );

                $edit_teacher_data = $this->my_custom_functions->update_data($teacher_data, TBL_TEACHER, $condition);

                $this->load->library('image_lib');
                $this->load->library('upload');

                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                            ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                            ($_FILES['adminphoto']['type'] == 'image/png') ||
                            ($_FILES['adminphoto']['type'] == 'image/gif')) {

                        list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $new_width = "";
                        $new_height = "";

                        $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                        $new_width = 185;
                        //$new_height = $new_width / $ratio;
                        $new_height = 185;

                        $config['image_library'] = 'gd2';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                        $config['new_image'] = "uploads/teacher/" . $teacher_id . ".jpg";
                        $config['maintain_ratio'] = FALSE;
                        $config['width'] = $new_width;
                        $config['height'] = $new_height;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                }
                if ($edit_teacher_data) {
                    $this->session->set_flashdata("s_message", 'Teacher edited successfully.');
                    redirect("school/user/manageTeachers");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTeachers");
                }
            }
        } else {
            $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['teacher'] = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_TEACHER);

            $this->load->view('school/edit_teacher', $data);
        }
    }

    public function deleteTeacher() {
        $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_teacher = array('id' => $teacher_id);
        $delete = $this->my_custom_functions->delete_data(TBL_TEACHER, $delete_teacher);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Teacher deleted successfully.');
            redirect("school/user/manageTeachers");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageTeachers");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   PERIOD PART
    //////////////////////////////////////////////////////////////////////

    public function managePeriods() {

        $sql = "Select * from " . TBL_PERIODS . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $sql = base64_encode($sql);
        $this->session->set_userdata('period_sql', $sql);
        $data['period'] = $this->School_user_model->get_period_data($sql);
        $this->load->view('school/manage_period', $data);
    }

    public function addPeriod() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("class_id", "Select Class", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addPeriod");
            } else { /// Update admin
                $max_sort_query = $this->db->query("select max(sort_order) as max_sort_id from " . TBL_PERIODS . "");
                $max_sort = $max_sort_query->row();
                $max_sort_id = $max_sort->max_sort_id + 1;

                $period_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    //'class_id' => $this->input->post('class_id'),
                    'period_name' => $this->input->post('period_name'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'status' => $this->input->post('status'),
                    'sort_order' => $max_sort_id,
                );
                if ($this->input->post('lunch_break')) {
                    $period_data['lunch_break'] = $this->input->post('lunch_break');
                }

                $add_period_data = $this->my_custom_functions->insert_data($period_data, TBL_PERIODS);
                if ($add_period_data) {
                    $this->session->set_flashdata("s_message", 'Period added successfully.');
                    redirect("school/user/managePeriods");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/managePeriods");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/add_period', $data);
        }
    }

    public function editPeriod() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $period_id = $this->input->post('period_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Select Class", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editPeriod/" . $period_id);
            } else { /// Update class
                $period_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'period_name' => $this->input->post('period_name'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $period_id
                );

                $edit_period_data = $this->my_custom_functions->update_data($period_data, TBL_PERIODS, $condition);
                if ($edit_period_data) {
                    $this->session->set_flashdata("s_message", 'Period edited successfully.');
                    redirect("school/user/managePeriods");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/managePeriods");
                }
            }
        } else {
            $subject_id = $this->uri->segment(4);
            $data['period'] = $this->my_custom_functions->get_details_from_id($subject_id, TBL_PERIODS);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/edit_period', $data);
        }
    }

    public function deletePeriod() {
        $period_id = $this->uri->segment(4);
        $delete_period = array('id' => $period_id);
        $delete = $this->my_custom_functions->delete_data(TBL_PERIODS, $delete_period);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Period deleted successfully.');
            redirect("school/user/managePeriods");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/managePeriods");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   MANAGE TIME TABLE PART
    //////////////////////////////////////////////////////////////////////

    public function manageTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $data['period'] = $this->School_user_model->get_time_table_data($class_id, $section_id);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['post_data'] = array('class_id' => $class_id, 'section_id' => $section_id);
        } else {
            $data['period'] = array();
            $data['post_data'] = array('class_id' => '', 'section_id' => '');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        }
        $this->load->view('school/manage_time_table', $data);
    }

    public function addTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die; 
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTimeTable");
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }


                $routine_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'day_id' => $this->input->post('day_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'attendance_class' => $attendance_period,
                    'status' => 1,
                );

                $add_routine_data = $this->my_custom_functions->insert_data($routine_data, TBL_TIMETABLE);
                if ($add_routine_data) {
                    $this->session->set_flashdata("s_message", 'Routine added successfully.');
                    redirect("school/user/manageTimeTable");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTimeTable");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_time_table', $data);
        }
    }

    function get_section_list() {
        $class_id = $this->input->post('class_id');

        $sec_id = $this->input->post('sec_id');
        $get_section_list = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and status = 1');
        echo '<option value="">Select Section</option>';
        foreach ($get_section_list as $sec_list) {
            if ($sec_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['section_name'] . '</option>';
        }
    }

    function get_subject_list_classwise() {
        $class_id = $this->input->post('class_id');

        $get_subject_list = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        echo '<option value="">Select Subject</option>';
        foreach ($get_subject_list as $subject_list) {
            echo '<option value="' . $subject_list['id'] . '">' . $subject_list['subject_name'] . '</option>';
        }
    }

    public function editTimeTable() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $routine_id = $this->input->post('routine_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("subject_id", "Subject", "trim|required");
            //$this->form_validation->set_rules("teacher_id", "Teacher", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }


                $routine_data_update = array(
                    'day_id' => $this->input->post('day_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'attendance_class' => $attendance_period,
                );

                //echo "<pre>";print_r($routine_data_update);die;
                $condition = array(
                    'id' => $routine_id
                );
                $update = $this->my_custom_functions->update_data($routine_data_update, TBL_TIMETABLE, $condition);
                if ($update) {
                    $this->session->set_flashdata("s_message", 'Routine successfully updated.');
                    redirect("school/user/editTimeTable/" . $routine_id);
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/editTimeTable/" . $routine_id);
                }
            }
        } else {
            echo $routine_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['routine_data'] = $this->my_custom_functions->get_details_from_id($routine_id, TBL_TIMETABLE);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_time_table', $data);
        }
    }

    function deleteUsername() {
        $parent_id = $this->input->post('id');
        $student_id = $this->input->post('student_id');
        $child_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $parent_id . '"');
        if ($child_count > 1) {
            $delete_linked_student = array('student_id' => $student_id, 'parent_id' => $parent_id);
            $delete = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_linked_student);
            echo 1;
        } else {
            $delete_linked_student = array('student_id' => $student_id, 'parent_id' => $parent_id);
            $delete = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_linked_student);

            $delete_username = array('id' => $parent_id);
            $delete_user_cmd = $this->my_custom_functions->delete_data(TBL_PARENT, $delete_username);
            echo 1;
        }
    }

    function addMoreUsernames() {
        echo "<pre>";
        print_r($_POST);
        $username = $this->input->post('user_phone_no');
        $student_id = $this->input->post('stdnt_id_for_usrname');
        $check_exist = $this->my_custom_functions->get_perticular_count(TBL_PARENT, 'and username = "' . $username . '"');
        if ($check_exist > 0) {
            $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'id', 'and username = "' . $username . '"');
            $insert_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'parent_id' => $parent_id,
                'student_id' => $this->input->post('stdnt_id_for_usrname')
            );
            $insert_link_data = $this->my_custom_functions->insert_data($insert_data, TBL_PARENT_KIDS_LINK);
        } else {
            $insert_username_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'username' => $username
            );
            $parent_id = $this->my_custom_functions->insert_data_last_id($insert_username_data, TBL_PARENT);

            $insert_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'parent_id' => $parent_id,
                'student_id' => $this->input->post('stdnt_id_for_usrname')
            );
            $insert_link_data = $this->my_custom_functions->insert_data($insert_data, TBL_PARENT_KIDS_LINK);
        }
        $this->session->set_flashdata("s_message", 'Username successfully added.');
        redirect('school/user/editStudent/' . $student_id . '#user');
    }

    function deleteTimeTable() {

        $timeTableTable_id = $this->input->post('timetable_id');

        $delete_data = array('id' => $timeTableTable_id);
        $delete_timetable = $this->my_custom_functions->delete_data(TBL_TIMETABLE, $delete_data);
        echo 1;
    }

    function manageStudent() {
        $data['students'] = $this->my_custom_functions->get_multiple_data(TBL_STUDENT, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $this->load->view('school/manage_student', $data);
    }

    function addStudent() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("reg_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("date_of_birth", "Date Of Birth", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "trim|required");
            $this->form_validation->set_rules("father_name", "Father Name", "trim|required");
            $this->form_validation->set_rules("mother_name", "Mother Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addStudent");
            } else { /// Update admin
                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'registration_no' => $this->input->post('reg_no'),
                    'name' => $this->input->post('name'),
                    'dob' => $this->my_custom_functions->database_date($this->input->post('date_of_birth')),
                    'gender' => $this->input->post('gender'),
                    'father_name' => $this->input->post('father_name'),
                    'mother_name' => $this->input->post('mother_name'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'roll_no' => $this->input->post('roll_no'),
                    'status' => $this->input->post('status'),
                );
                $student_id = $this->my_custom_functions->insert_data_last_id($basic_data, TBL_STUDENT);
                $student_detail = array(
                    'student_id' => $student_id,
                    'address' => $this->input->post('address'),
                    'aadhar_no' => $this->input->post('aadhar_no'),
                    'emergency_contact_person' => $this->input->post('emg_contact_person'),
                    'emergency_contact_no' => $this->input->post('emg_contact_no'),
                    'blood_group' => $this->input->post('blood_group'),
                );
                $student_details = $this->my_custom_functions->insert_data($student_detail, TBL_STUDENT_DETAIL);
                $parent_username = $this->input->post('contact_no');
                if (!empty($parent_username)) {
                    foreach ($parent_username as $number) {
                        $username_exist = $this->my_custom_functions->get_perticular_count(TBL_PARENT, 'and school_id = "' . $this->session->userdata('school_id') . '" and username = "' . $number . '"');
                        if ($username_exist > 0) {
                            $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and username = "' . $number . '" ');
                        } else {
                            $parent_username_data = array(
                                'school_id' => $this->session->userdata('school_id'),
                                'username' => $number
                            );
                            $parent_id = $this->my_custom_functions->insert_data_last_id($parent_username_data, TBL_PARENT);
                        }
                        $parent_student_link = array(
                            'school_id' => $this->session->userdata('school_id'),
                            'parent_id' => $parent_id,
                            'student_id' => $student_id
                        );
                        $parent_student_link_data = $this->my_custom_functions->insert_data($parent_student_link, TBL_PARENT_KIDS_LINK);
                    }
                }
                if ($student_id) {
                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                                ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                                ($_FILES['adminphoto']['type'] == 'image/png') ||
                                ($_FILES['adminphoto']['type'] == 'image/gif')) {

                            list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                            $new_width = "";
                            $new_height = "";

                            $ratio = $width / $height;

                            $new_width = 800;
                            $new_height = $new_width / $ratio;
//                            $new_width = 185;
//                            $new_height = $new_width / $ratio;

                            $config['image_library'] = 'gd2';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                            $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                            $config['new_image'] = "uploads/student/" . $student_id . ".jpg";
                            $config['maintain_ratio'] = FALSE;
                            $config['width'] = $new_width;
                            $config['height'] = $new_height;
                            $this->image_lib->initialize($config);
                            //$this->image_lib->resize();
                            if (!$this->image_lib->resize()) {
                                // $this->session->set_flashdata('e_message', $this->image_lib->display_errors());
                            } else {
                                $thumb_width = 100;
                                $thumb_height = 125;
                                $source = "uploads/student/" . $student_id . ".jpg";
                                $destination = "uploads/student/thumbs/" . $student_id . ".jpg";
                                $this->my_custom_functions->CreateFixedSizedImage($source, $destination, $thumb_width, $thumb_height);
                                $this->session->set_flashdata('add_data', 'success');
                            }
                        }
                    }




                    $this->session->set_flashdata("s_message", 'Student successfully created.');
                    redirect("school/user/manageStudent");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageStudent");
                }
            }
        } else {
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_student', $data);
        }
    }

    function checkRegistrationNumber() {
        $school_id = $this->session->userdata('school_id');
        $reg_val = $this->input->post('reg_val');

        $check_registratin_no = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $school_id . '" and registration_no = "' . $reg_val . '"');
        if ($check_registratin_no > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function editStudent() {



        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);die;
            $student_id = $this->input->post('student_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("reg_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("date_of_birth", "Date Of Birth", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "trim|required");
            $this->form_validation->set_rules("father_name", "Father Name", "trim|required");
            $this->form_validation->set_rules("mother_name", "Mother Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addStudent");
            } else { /// Update admin
//                echo "<pre>";
//                print_r($_POST);
//                die;
                $basic_data = array(
                    //'registration_no' => $this->input->post('reg_no'),
                    'name' => $this->input->post('name'),
                    'dob' => $this->my_custom_functions->database_date($this->input->post('date_of_birth')),
                    'gender' => $this->input->post('gender'),
                    'father_name' => $this->input->post('father_name'),
                    'mother_name' => $this->input->post('mother_name'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'roll_no' => $this->input->post('roll_no'),
                    'status' => $this->input->post('status'),
                );
                $condition = array('id' => $student_id);
                $this->my_custom_functions->update_data($basic_data, TBL_STUDENT, $condition);

                $student_detail = array(
                    //'student_id' => $student_id,
                    'address' => $this->input->post('address'),
                    'aadhar_no' => $this->input->post('aadhar_no'),
                    'emergency_contact_person' => $this->input->post('emg_contact_person'),
                    'emergency_contact_no' => $this->input->post('emg_contact_no'),
                    'blood_group' => $this->input->post('blood_group'),
                );
                $condition_det = array('student_id' => $student_id);
                $student_details = $this->my_custom_functions->update_data($student_detail, TBL_STUDENT_DETAIL, $condition_det);

                if ($student_id) {
                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////
                    //echo "<pre>";print_r($_FILES);die;
                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                                ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                                ($_FILES['adminphoto']['type'] == 'image/png') ||
                                ($_FILES['adminphoto']['type'] == 'image/gif')) {

                            list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                            $new_width = "";
                            $new_height = "";

                            $ratio = $width / $height;

                            $new_width = 800;
                            $new_height = $new_width / $ratio;
//                            $new_width = 185;
//                            $new_height = $new_width / $ratio;

                            $config['image_library'] = 'gd2';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                            $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                            $config['new_image'] = "uploads/student/" . $student_id . ".jpg";
                            $config['maintain_ratio'] = FALSE;
                            $config['width'] = $new_width;
                            $config['height'] = $new_height;

                            $this->image_lib->initialize($config);
                            if (!$this->image_lib->resize()) {
                                // $this->session->set_flashdata('e_message', $this->image_lib->display_errors());
                            } else {
                                $thumb_width = 100;
                                $thumb_height = 125;
                                $source = "uploads/student/" . $student_id . ".jpg";
                                $destination = "uploads/student/thumbs/" . $student_id . ".jpg";
                                $this->my_custom_functions->CreateFixedSizedImage($source, $destination, $thumb_width, $thumb_height);
                                $this->session->set_flashdata('add_data', 'success');
                            }
                        }
                    }



                    $this->session->set_flashdata("s_message", 'Students record successfully updated.');
                    redirect("school/user/manageStudent");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageStudent");
                }
            }
        } else {
            $student_id = $this->uri->segment(4);
            $data['get_student_detail'] = $this->School_user_model->get_student_detail($student_id);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_student', $data);
        }
    }

    function manageNotice() {
        $data['notice'] = $this->my_custom_functions->get_multiple_data(TBL_NOTICE);
        $this->load->view('school/manage_notice', $data);
    }

    function issueNoice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $timestamp = time();

            if ($this->input->post('type') == 1 || $this->input->post('type') == 2) {
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                }
            } else {
                $class_list_implode = '';

                $class_list_array = $this->input->post('class');
                foreach ($class_list_array as $class_list) {
                    $class_list_implode .= $class_list . ',';
                }
                $finalclass_list = rtrim($class_list_implode, ',');
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'notice_text' => $this->input->post('notice_text'),
                    'class_id' => $finalclass_list,
                    'status' => '1'
                );

                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                }
            }
            $notice_id = $this->my_custom_functions->insert_data_last_id($data, TBL_NOTICE);
            //$notice_id = 5;
            if ($notice_id) {
                if (isset($_FILES['doc_upload'])) {
                    $file_type_list = $this->config->item('file_type');
                    $file_count = count($_FILES['doc_upload']['name']);
                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if ((in_array($ext, $file_type_list))) {

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    $key .= 'file_';
                                }
                                $key .= $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('notice_id' => $notice_id, 'file_url' => $result['ObjectURL']);
                                    $this->my_custom_functions->insert_data($file_data, TBL_NOTICE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            $error_count++;
                            $error .= '.' . $ext . ',';
                        }

                        if (file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Notice successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        $error_ext = rtrim($error, ',');
                        $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
            }
            redirect("school/user/manageNotice");
        } else {
            $this->load->view('school/issue_notice');
        }
    }

    function get_class_list() {
        $school_id = $this->session->userdata('school_id');
        $class_list = $this->School_user_model->get_class_list($school_id);
        $db_class = $this->input->post('class_id');
        $checked = '';
        if ($db_class != '') {
            $class_list_exp = explode(',', $db_class);
        }
        if (!empty($class_list)) {
            echo '<div class="custom-control custom-checkbox custom-control-inline mb-5"><input class="custom-control-input" type="checkbox" name="example-inline-checkboxall" id="checkboxall" value="0">
                  <label class="custom-control-label" for="checkboxall">Select All</label>
                  </div><br>';
            foreach ($class_list as $row) {
                if (!empty($class_list_exp)) {
                    $checked = '';
                    foreach ($class_list_exp as $class_idd) {
                        if ($class_idd == $row['id']) {
                            $checked = 'checked="checked"';
                        }
                    }
                } else {
                    $checked = '';
                }
                echo '<div class="custom-control custom-checkbox custom-control-inline mb-5"><input class="custom-control-input checkboxTeacher" type="checkbox" name="class[]" id="example-inline-checkbox' . $row['id'] . '" value="' . $row['id'] . '" ' . $checked . '>
                  <label class="custom-control-label" for="example-inline-checkbox' . $row['id'] . '"">' . $row['class_name'] . '</label>
                  </div>';
            }
        }
    }

    function editNotice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);

            if ($this->input->post('type') == 1 || $this->input->post('type') == 2) {
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                }
            } else {
                $class_list_implode = '';

                $class_list_array = $this->input->post('class');
                foreach ($class_list_array as $class_list) {
                    $class_list_implode .= $class_list . ',';
                }
                $finalclass_list = rtrim($class_list_implode, ',');
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'notice_text' => $this->input->post('notice_text'),
                    'class_id' => $finalclass_list,
                    'status' => 1
                );

                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = $this->my_custom_functions->database_date($this->input->post('date_of_publish'));
                }
            }
            $condition = array('id' => $this->input->post('notice_id'));

            //UPLOAD FILES;
            $this->my_custom_functions->update_data($data, TBL_NOTICE, $condition);



            $notice_id = $this->input->post('notice_id');

            if ($_FILES['doc_upload'][0]['name'] != '') {
                $file_type_list = $this->config->item('file_type');
                $file_count = count($_FILES['doc_upload']['name']);
                $success_count = 0;
                $error_count = 0;
                $error = '';
                $errorAttch = 0;
                $successfulAttch = 0;
                for ($i = 0; $i < $file_count; $i++) {

                    $filename = $_FILES['doc_upload']['name'][$i];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if ((in_array($ext, $file_type_list))) {

                        $uploaddir = 'file_upload/';
                        $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                        move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                        $temp_file = $uploadfile;


                        $fileSize = $_FILES['doc_upload']['size'][$i];
                        if ($fileSize <= ALLOWED_FILE_SIZE) {

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;
                            $key = '';
                            if (
                                    $ext == "jpg" ||
                                    $ext == "jpeg" ||
                                    $ext == "png") {
                                $key .= 'image_';
                            } else if (
                                    $ext == "pdf" ||
                                    $ext == "doc" ||
                                    $ext == "docx" ||
                                    $ext == "xls" ||
                                    $ext == "xlsx" ||
                                    $ext == "pptx" ||
                                    $ext == "ppt"
                            ) {
                                $key .= 'file_';
                            }
                            $key .= $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                            //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => $key,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));


                                $file_data = array('notice_id' => $notice_id, 'file_url' => $result['ObjectURL']);
                                $this->my_custom_functions->insert_data($file_data, TBL_NOTICE_FILES);

                                $successfulAttch++;
                            } catch (S3Exception $e) {

                                $errorAttch++;
                            }

                            $success_count++;
                        } else {
                            $errorAttch++;
                        }
                    } else {
                        $error_count++;
                        $error .= '.' . $ext . ',';
                    }

                    if (file_exists($temp_file)) {
                        @unlink($temp_file);
                    }
                }



                if ($success_count > 0) {
                    $this->session->set_flashdata("s_message", 'Notice successfully added.');
                    //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                }
                if ($error_count > 0) {
                    $error_ext = rtrim($error, ',');
                    $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                }
            }





            redirect("school/user/manageNotice");
        } else {
            $notice_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

            $data['notice_detail'] = $this->my_custom_functions->get_details_from_id($notice_id, TBL_NOTICE);
            $data['notice_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_NOTICE_FILES, 'and notice_id = "' . $notice_id . '"');
            $this->load->view('school/edit_notice', $data);
        }
    }

    function deleteNotice() {
        $notice_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $notice_file_list_data = $this->my_custom_functions->get_multiple_data(TBL_NOTICE_FILES, 'and notice_id = "' . $notice_id . '"');

        if (!empty($notice_file_list_data)) {
            foreach ($notice_file_list_data as $notice_file_list) {
                $file_url = $notice_file_list['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data("tbl_notice_files", array("id" => $notice_file_list['id']));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
        }
        $condition = array('id' => $notice_id);

        $delete = $this->my_custom_functions->delete_data(TBL_NOTICE, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Notice successfully deleted.');
            redirect("school/user/manageNotice");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/manageNotice");
        }
    }

    function deleteIndividualFile() {
        $indv_file_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $notice_file_list = $this->my_custom_functions->get_details_from_id($indv_file_id, TBL_NOTICE_FILES);

        if (!empty($notice_file_list)) {
            //foreach ($notice_file_data as $notice_file_list) {echo $notice_file_list['id'];die;
                $file_url = $notice_file_list['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $aws_key
                            ));
                        }
                        $delete = $this->my_custom_functions->delete_data("tbl_notice_files", array("id" => $notice_file_list['id']));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            //}
        }
        
        if ($delete) {
            $this->session->set_flashdata("s_message", 'file successfully deleted.');
            redirect("school/user/editNotice/".$this->my_custom_functions->ablEncrypt($notice_file_list['notice_id']));
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/editNotice/".$this->my_custom_functions->ablEncrypt($notice_file_list['notice_id']));
        }
    }

    function manageHolidays() {

        $data['holiday_list'] = $this->my_custom_functions->get_multiple_data(TBL_HOLIDAY, 'and school_id = "' . $this->session->userdata('school_id') . '"');

        $this->load->view('school/manage_holiday', $data);
    }

    function addHolidays() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);
            //echo "<pre>";print_r($_POST);die;

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("office_close", "Office Open", "trim|required");



            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addHolidays");
            } else { /// Update admin
                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'title' => $this->input->post('title'),
                    'start_date' => $this->my_custom_functions->database_date($this->input->post('from_date')),
                    'office_open' => $this->input->post('office_close'),
                    'status' => 1,
                );
                if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
                    $basic_data['end_date'] = $this->my_custom_functions->database_date($this->input->post('to_date'));
                }
                //echo "<pre>";print_r($basic_data);die;
                $holiday = $this->my_custom_functions->insert_data($basic_data, TBL_HOLIDAY);
                if ($holiday) {
                    $this->session->set_flashdata("s_message", 'Holiday successfully created.');
                    redirect("school/user/manageHolidays");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageHolidays");
                }
            }
        } else {
            $this->load->view('school/add_holiday');
        }
    }

    function editHolidays() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);
            //echo "<pre>";print_r($_POST);die;
            $holiday_id = $this->input->post('holiday_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("office_close", "Office Open", "trim|required");



            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editHolidays/" . $this->my_custom_functions->ablEncrypt($holiday_id));
            } else { /// Update admin
                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'title' => $this->input->post('title'),
                    'start_date' => $this->my_custom_functions->database_date($this->input->post('from_date')),
                    'office_open' => $this->input->post('office_close'),
                    'status' => $this->input->post('status'),
                );
                if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
                    $basic_data['end_date'] = $this->my_custom_functions->database_date($this->input->post('to_date'));
                }
                //echo "<pre>";print_r($basic_data);die;
                $condition = array('id' => $holiday_id);
                $holiday = $this->my_custom_functions->update_data($basic_data, TBL_HOLIDAY, $condition);
                if ($holiday) {
                    $this->session->set_flashdata("s_message", 'Holiday successfully updated.');
                    redirect("school/user/manageHolidays");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageHolidays");
                }
            }
        } else {
            $holiday_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['holiday_detail'] = $this->my_custom_functions->get_details_from_id($holiday_id, TBL_HOLIDAY);
            $this->load->view('school/edit_holiday', $data);
        }
    }

}
