<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->uri->segment(3) != CRON_KEY) {
            die();
        }
        $this->load->model("Cron_model");
    }

    function deleteFiles() {
        $get_file_list = $this->Cron_model->get_file_list();
        if (!empty($get_file_list)) {
            foreach ($get_file_list as $row) {
                
                $file_url = $row['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => $row['folder_name'] . '/' . $aws_key
                            ));
                        }
                        
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
        }
    }

}
