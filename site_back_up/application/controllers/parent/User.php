<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->uri->segment(5) && $this->uri->segment(5) == DOWNLOAD_KEY) {
            
        } else {
            
            $this->my_custom_functions->check_parent_security();
        }
        $this->load->model("parent/Parent_user_model");
        $prefs = array(
            'start_day' => 'monday',
            'month_type' => 'long',
            'day_type' => 'short',
            'show_next_prev' => true,
            //'next_prev_url' => base_url() . "admin/calendar/event",
            'template' => '
					   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender" >{/table_open}
                                           
                                            {heading_row_start}<tr>{/heading_row_start}
                                           
					   {heading_previous_cell}<th id="pre_link"><a href="{previous_url}" title="previous">&laquo;</a></th>{/heading_previous_cell}
					   
                                           {heading_title_cell}<th colspan="{colspan}" id="month_heading">{heading}</th>{/heading_title_cell}
					   {heading_next_cell}<th id="next_link"><a href="{next_url}" title="next">&raquo;</a></th>{/heading_next_cell}
					
					   {heading_row_end}</tr>{/heading_row_end}
                                           
					   {week_row_start}<tr>{/week_row_start}
					   {week_day_cell}<th>{week_day}</th>{/week_day_cell}
					   {week_row_end}</tr>{/week_row_end}
					
					   {cal_row_start}<tr>{/cal_row_start}
					   {cal_cell_start}<td width="5">{/cal_cell_start}
					
					   {cal_cell_content}<div class="date {custom_class}">{day}</div><div class="content_box sort_box_{day}"></div> {/cal_cell_content}
					   {cal_cell_content_today}<div class="highlight"><div class="date">{day}</div><div class="content_box sort_box_{day}"></div></div>{/cal_cell_content_today}
					
					   {cal_cell_no_content}<div class="date">{day}</div><div class="content_box"></div>{/cal_cell_no_content}
					   {cal_cell_no_content_today}<div class="highlight"><div class="highlight">{day}</div></div>{/cal_cell_no_content_today}
					
					   {cal_cell_blank}&nbsp;{/cal_cell_blank}
					
					   {cal_cell_end}</td>{/cal_cell_end}
					   {cal_row_end}</tr>{/cal_row_end}
					
					   {table_close}</table>{/table_close}'
        );
        $this->load->library('calendar', $prefs);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Teacher area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {
         
        if ($this->input->cookie('student_id', true)) {
            //echo "<pre>";print_r($this->session->all_userdata());
            $data['student_detail'] = $this->my_custom_functions->get_details_from_id($this->session->userdata('student_id'), TBL_STUDENT);
            //echo $this->db->last_query();
            //echo "<pre>";print_r($data);die;
            $student_flag = array("student_flag" => 1);
            $this->session->set_userdata($student_flag);
            $data['page_title'] = 'Dashboard';
            $data['back_url'] = '';
            $this->load->view("parent/dashboard", $data);
        } else {
            redirect('parent/user/select_student');
        }
    }

    function select_student() {
         //print_r($_COOKIE);die;
        $data['child_list'] = $this->Parent_user_model->get_child_list();
        $data['page_title'] = 'Select Student';
        $data['back_url'] = '';
        $this->load->view('parent/select_student', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Logout
    ///////////////////////////////////////////////////////////////////////////////

    public function logout() {

        $session_data = array('parent_id', 'parent_is_logged_in','student_flag');
        $this->session->unset_userdata($session_data);

        delete_cookie('student_id');



        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("app");
    }

    function changePassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $old_pass = $this->input->post('oldpass');
            $new_pass = $this->input->post('newpass');
            $confirm_pass = $this->input->post('confirmpass');

            $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE id = '" . $this->session->userdata('parent_id') . "' AND status=1";
            $query = $this->db->query($sql);

            $record = array();
            if ($query->num_rows() > 0) {
                $record = $query->row_array();

                if (password_verify($old_pass, $record['password'])) {
                    $update_data = array(
                        'password' => password_hash($new_pass, PASSWORD_DEFAULT)
                    );
                    $condition = array('id' => $this->session->userdata('parent_id'));
                    $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
                } else {
                    $this->session->set_flashdata("e_message", 'Old password is wrong.');
                    redirect("parent/user/changePassword");
                }
            }
            if ($update) {
                $this->session->set_flashdata("s_message", 'Password successfully updated.');
                redirect("parent/user/changePassword");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong. Please try again later');
                redirect("parent/user/changePassword");
            }
        } else {
            $data['page_title'] = 'Change Password';
            $data['back_url'] = '';
            $this->load->view('parent/change_password', $data);
        }
    }

    function switch_student() {

        $student_id = $this->uri->segment(4);
        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and student_id = "' . $student_id . '"');
        $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'parent_id', 'and student_id = "' . $student_id . '"');
        $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
        $cookie = array(
            'name' => 'student_id',
            'value' => $student_id,
            'expire' => '86500000000',
        );
        $this->input->set_cookie($cookie);
        $session_student_data = array(
            "student_id" => $student_id,
            "class_id" => $student_detail['class_id'],
            "section_id" => $student_detail['section_id'],
            "school_id" => $school_id,
            "parent_id" => $parent_id, 
            "student_flag" => 1
        );
        $this->session->set_userdata($session_student_data);
        redirect('parent/user/dashBoard');
    }

    /////////////////////////////////////////////////////////////////////
    /////////// SCHOOL SYLLABUS
    ////////////////////////////////////////////////////////////////////

    function syllabus() {
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $syllabus_detail = $this->my_custom_functions->get_details_from_id('', TBL_SYLLABUS, array('school_id' => $school_id, 'class_id' => $class_id));
        if (!empty($syllabus_detail)) {
            $syllabus_id = $syllabus_detail['id'];
            $data['syllabus_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_SYLLABUS_FILES, 'and syllabus_id = ' . $syllabus_id . '');
            $data['syllabus_detail'] = $syllabus_detail;
        }
        $data['page_title'] = 'Syllabus';
        $data['back_url'] = '';
        $this->load->view('parent/syllabus', $data);
    }

    /////////////////////////////////////////////////////////////////////
    /////////// SCHOOL HOLIDAY
    ////////////////////////////////////////////////////////////////////
    function schoolCalander() {
        $school_id = $this->session->userdata('school_id');
        $data['school_calender'] = $this->Parent_user_model->get_school_calender($school_id);
        $data['page_title'] = 'School Calender';
        $data['back_url'] = '';
        //echo "<pre>";print_r($data);die;
        $this->load->view('parent/school_calender', $data);
    }

    /////////////////////////////////////////////////////////////////////
    /////////// DAILY CLASS WORKS
    ////////////////////////////////////////////////////////////////////

    function dailyWorks() {

        $data['class_list_daywise'] = $this->Parent_user_model->get_class_list_daywise();
        $data['page_title'] = 'Daily works';
        $data['back_url'] = '';
        if ($this->input->post('class_date')) {
            $data['display_class_date'] = $this->input->post('class_date');
        } else {
            $data['display_class_date'] = date('Y-m-d');
        }

        $this->load->view('parent/daily_works', $data);
    }

    function getClassNoteDetail() {
        $subject_id = $this->uri->segment(4);
        $data['note_detail'] = $this->Parent_user_model->get_class_note_detail($subject_id);
        $data['note_detail_home'] = $this->Parent_user_model->get_home_note_detail($subject_id);
        $data['note_detail_assignment'] = $this->Parent_user_model->get_assignment_note_detail($subject_id);
        if ($this->session->userdata('class_date') == date('Y-m-d')) {
            $data['page_title'] = "Today's class";
        } else {
            $data['page_title'] = date('D d M', strtotime($this->session->userdata('class_date')));
        }
        $data['back_url'] = '';
        $this->load->view('parent/works', $data);
    }

    function attendance() {
        $data['attendance_data'] = $this->Parent_user_model->get_attendance_data();
        $data['page_title'] = "Attendance";
        $data['back_url'] = '';
        $this->load->view('parent/attendance_list', $data);
    }

    function timeTable() {
        $data['schedule'] = $this->Parent_user_model->get_teacher_weekly_schedule();
        $data['page_title'] = "Timetable";
        $data['back_url'] = '';
        $this->load->view('parent/weekly_schedule', $data);
    }

    function generalInformation() {
        $data['general_info'] = $this->Parent_user_model->get_general_info();
        $data['page_title'] = "School Info";
        $data['back_url'] = '';
        $this->load->view('parent/general_info', $data);
    }

    function notice() {
        $school_id = $this->session->userdata('school_id');
        $data['school_notice'] = $this->Parent_user_model->get_school_notice($school_id);
        $data['page_title'] = "Notice";
        $data['back_url'] = '';
        $this->load->view('parent/school_notice', $data);
    }

    function noticeDetail() {
        $notice_id = $this->uri->segment(4);
        $data['school_notice_detail'] = $this->Parent_user_model->get_school_notice_detail($notice_id);
        $data['page_title'] = "Notice Details";
        $data['back_url'] = '';
        $this->load->view('parent/notice_detail', $data);
    }

    function downloadFile() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTICE_FILES, 'file_url', 'and id = "' . $file_id . '" ');
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;

//        header('Content-Description: File Transfer');
//        header('Content-Type: application/octet-stream');
//        header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
//        header('Expires: 0');
//        header('Cache-Control: must-revalidate');
//        header('Pragma: public');
//        header('Content-Length: ' . filesize($filepath));
//        flush(); // Flush system output buffer
//        readfile($filepath);
//        exit;
        //}
    }

    function attendance_detail() {
        $data = array();
        $month = $this->uri->segment(4);
        $year = date('Y');

        $date = $this->Parent_user_model->get_attendance_detail($month, $year);
        
        $data = array(
            'year' => $year,
            'month' => $month,
            'event' => $date
        );
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = "Attendance Details";
        $data['back_url'] = '';
        $this->load->view('parent/attendance_detail', $data);
    }
    
    function studentDiary() {
        
        if($this->input->post('submit') && $this->input->post('submit') != ''){
            $first_day_this_month = date('Y-m-01');
            $last_dayo_of_the_month = date('Y-m-t', strtotime(date('Y-m-d')));
            $start = strtotime($first_day_this_month.'00:00:00');
            $end = strtotime($last_dayo_of_the_month.'23:59:59');
            $diary_count_per_day = $this->my_custom_functions->get_perticular_count(TBL_DIARY,'and school_id = "'.$this->session->userdata('school_id').'" and parent_id = "'.$this->session->userdata('parent_id').'" and status = 2 and issue_date >="'.$start.'" and issue_date <="'.$end.'" ');
           
            if($diary_count_per_day < PARENT_DIARY_COUNT){
                
            $roll_no = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT,'roll_no','and id = "'.$this->session->userdata('student_id').'"');
            $post_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'teacher_id' => $this->input->post('teacher_id'),
                'subject_id' => $this->input->post('subject_id'),
                'student_id' => $this->session->userdata('student_id'),
                'class_id' => $this->session->userdata('class_id'),
                'section_id' => $this->session->userdata('section_id'),
                'roll_no' => $roll_no,
                'heading' => $this->input->post('heading'),
                'diary_note' => $this->input->post('diary_note'),
                'issue_date' => time(),
                'parent_id' => $this->session->userdata('parent_id'),
                'status' => 2,
                'parent_read_msg' => 2,
                'teacher_read_msg' => 1
            );
            $this->my_custom_functions->insert_data($post_data,TBL_DIARY);
            $this->session->set_flashdata("s_message", 'record successfully saved.');
            redirect('parent/user/studentDiary');
           }else{
               
            $this->session->set_flashdata("e_message", 'You can write upto 10 diary per day. Diary limit exceeds.');
            redirect('parent/user/studentDiary');  
           }
        }else{
        
        $school_id = $this->session->userdata('school_id');
        $data['diarylist'] = $this->Parent_user_model->getDiaryList($school_id);
        $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, ' and school_id = "' . $this->session->userdata('school_id') . '"');
        $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" order by id ASC');
        $data['page_title'] = "Dairy";
        $data['back_url'] = '';
        $this->load->view('parent/diary',$data);
        }
    }
    function diaryDetail() {
        $diary_id = $this->uri->segment(4);
        $update_data = array('parent_read_msg' => 2);
        $condition = array('id' => $diary_id);
        $this->my_custom_functions->update_data($update_data,TBL_DIARY,$condition);
                
        $data['diary_detail'] = $this->Parent_user_model->get_diary_detail($diary_id);
        $data['page_title'] = "Diary Details";
        $data['back_url'] = '';
        //echo "<pre>";print_r($data);die;
        $this->load->view('parent/diary_detail', $data);
    }
    function feeStructure(){
        $data['page_title'] = "Fees structure";
        $data['back_url'] = '';
        $data['fees_data'] = $this->Parent_user_model->get_fees_detail();
        $this->load->view('parent/fees_structure',$data);
    }
    function updateProfile() {
        $data['page_title'] = "Update Profile";
        $data['back_url'] = '';
        $this->load->view('parent/update_profile',$data);
    }
    

}
