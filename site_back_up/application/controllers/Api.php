<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Api extends CI_Controller {

    function __construct() {
        parent:: __construct();

        if ($this->input->post("key") != API_KEY) {
            $encode[] = array(
                "error" => "Invalid Key"
            );
            $json_encode = json_encode($encode);
            print_r($json_encode);
            die();
        }
        
        $this->load->model("Api_model");
    }
    
    function authentication() {
        if ($this->input->post("username") != "" && $this->input->post("password") != "") {
            $response = $this->Api_model->user_verification();
            if(!empty($response)){
                $user_id = $this->my_custom_functions->ablEncrypt($response['id']);
                $login_link = base_url().'app/app_login/'.$user_id;    
                $encode[] = array(
                            "msg" => "validation Success",
                            "user_id" => $response['id'],
                            "login_link" => $login_link,
                            "status" => "true"

                        );
                        $json_encode = json_encode($encode);
                        print_r($json_encode);
                
            }else{ 
                $encode[] = array(
                    "msg" => "Invalid username and password",
                    "status" => "false"
                );
                $json_encode = json_encode($encode);
                print_r($json_encode);
            }
        }
    }
     ///////////////////////////////////////////////////////////////////////////////
    /// Register Device Api
    ///////////////////////////////////////////////////////////////////////////////
    function register_device() {

        if ($this->input->post("user_id") != "" && $this->input->post("device_id") != "" && $this->input->post("device_type") != "") {

            $validation_flag = $this->my_custom_functions->user_validation($this->input->post("user_id"));
            if ($validation_flag == 1) {
                $data = array(
                    'user_id' => $this->input->post("user_id"),
                    'device_id' => $this->input->post("device_id"),
                    'device_type' => $this->input->post("device_type"),
                    'last_update' => date('Y-m-d H:i:s')
                );

                $attendance_id = $this->Api_model->register_device($data);
                $encode[] = array(
                    "msg" => "Device Successfully Registered",
                    "status" => "true"
                );
                $json_encode = json_encode($encode);
                print_r($json_encode);
            } else {
                
                    $encode[] = array(
                        "msg" => "User is inactive",
                        "status" => "false"
                    );
                    $json_encode = json_encode($encode);
                    print_r($json_encode);
                
            }
        } else {
            $encode[] = array(
                "msg" => "Validation Failed",
                "status" => "false"
            );
            $json_encode = json_encode($encode);
            print_r($json_encode);
        }

    }
}