<?php //$this->load->view("school/_include/school_header");              ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Edit Syllabus</title>

        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this file?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
            }
            function confirm_delete(id) {
                window.location.href = "<?php echo base_url(); ?>school/user/deleteIndividualSyllabusFile/" + id;
            }
        </script>
    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit Syllabus</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-12">
                                    <?php
                                    echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material'));
                                    //echo "<pre>";print_r($notice_detail);
                                    ?>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select name="class_id" class="form-control" id="class_id" required>
                                                <option value="">Select</option>
                                                <?php
                                                foreach ($class_list as $row) {
                                                    if ($syllabus_detail['class_id'] == $row['id']) {
                                                        $selected = 'selected="selected"';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php echo $selected; ?>><?php echo $row['class_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                            <label for="class_id">Select Class</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="class_list" class="class_list" value="<?php echo $syllabus_detail['class_id']; ?>">



                                    <br>
                                    <div class="form-group row">
                                        <label class="col-12" for="example-textarea-input">Notice</label>
                                        <div class="col-12">
                                            <textarea class="form-control" id="editor1" name="syllabus_text" rows="6" placeholder="Content.."><?php echo $syllabus_detail['syllabus_text']; ?></textarea>
                                        </div>
                                    </div>



                                    <div class="form-group row">
                                        <?php
//                                        echo "<pre>";
//                                        print_r($syllabus_file_list);

                                        if (!empty($syllabus_file_list)) {
                                            foreach ($syllabus_file_list as $filess) {
                                                $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                $path = $filess['file_url'];

                                                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                    
                                                    $display_path = $path;
                                                    $download_path = $path;
                                                    $width = '72px';
                                                } else if ($ext == 'xls' || $ext == 'xlsx') {
                                                    
                                                    $display_path = base_url() . 'app_images/XLS.png';
                                                    $download_path = $path;
                                                    $width = '100px';
                                                } else if ($ext == 'doc' || $ext == 'docx') {
                                                    
                                                    $display_path = base_url() . 'app_images/DOC.png';
                                                    $download_path = $path;
                                                    $width = '48px';
                                                }
                                                else if ($ext == 'pdf' || $ext == 'PDF') {
                                                    
                                                    $display_path = base_url() . 'app_images/PDF.png';
                                                    $download_path = $path;
                                                    $width = '48px';
                                                }
                                                ?>
                                                <div class="col-2" style="text-align:center;">
<!--                                                    <a href="<?php //echo $download_path; ?>" download=""> <img src="<?php //echo $display_path; ?>"></a>-->
                                                    <a href="<?php echo base_url(); ?>school/user/downloadFilesyllabus/<?php echo $filess['id'];?>" download=""> <img src="<?php echo $display_path; ?>"></a>
                                                    
                                                    <a href="<?php echo base_url(); ?>school/user/deleteIndividualSyllabusFile/<?php echo $filess['id']; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $this->my_custom_functions->ablEncrypt($filess['id']);
                                         ?>');"><i class="fa fa-trash"></i></a>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-12" for="example-file-multiple-input">Upload Docs (Multiple)</label>
                                        <div class="col-12">
                                            <input type="file" id="example-file-multiple-input" name="doc_upload[]" multiple="">
                                        </div>
                                    </div>


                                    <input type="hidden" name="syllabus_id" value="<?php echo $syllabus_detail['id']; ?>">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1');
            </script>
            <script type="text/javascript">
                function all_class() {
                    $('.sub_type').text('All Classes');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>school/user/get_class_list",
                        //data: "class_id=" + class_id,
                        success: function (msg) {
                            if (msg != "") {
                                $('.check_lists').html(msg);
                                $('#checkboxall').on('click', function () {

                                    if (this.checked) {
                                        $('.checkboxTeacher').each(function () {
                                            this.checked = true;
                                        });
                                    } else {
                                        $('.checkboxTeacher').each(function () {
                                            this.checked = false;
                                        });
                                    }
                                });

                                $('.checkboxTeacher').on('click', function () {
                                    if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
                                        $('#checkboxall').prop('checked', true);
                                    } else {
                                        $('#checkboxall').prop('checked', false);
                                    }
                                });

                            }
                        }
                    });

                }
                $(document).ready(function () {
                    $('#date_of_birth').datepicker({
                        dateFormat: 'dd/mm/yy',
                    });

                    if ($('.type_3').prop('checked') == true) {
                        var class_id = $('.class_list').val();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>school/user/get_class_list",
                            data: "class_id=" + class_id,
                            success: function (msg) {
                                //alert(msg);
                                if (msg != "") {
                                    $('.check_lists').html(msg);
                                    $('#checkboxall').on('click', function () {

                                        if (this.checked) {
                                            $('.checkboxTeacher').each(function () {
                                                this.checked = true;
                                            });
                                        } else {
                                            $('.checkboxTeacher').each(function () {
                                                this.checked = false;
                                            });
                                        }
                                    });

                                    $('.checkboxTeacher').on('click', function () {
                                        if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
                                            $('#checkboxall').prop('checked', true);
                                        } else {
                                            $('#checkboxall').prop('checked', false);
                                        }
                                    });

                                }
                            }
                        });
                    }


                });
            </script>


        </div>


    </body>
</html>
