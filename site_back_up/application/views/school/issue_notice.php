<?php //$this->load->view("school/_include/school_header");        ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Issue Notice</title>


    </head>
    <body>

<?php $this->load->view('school/_include/loader'); ?>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Create Notice</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-12">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-group row">
                                        <label class="col-12">Type</label>
                                        <div class="col-12">
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio1" value="1" >
                                                <label class="custom-control-label" for="example-inline-radio1">All Teachers</label>
                                            </div>
                                            <div  class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio2" value="2">
                                                <label class="custom-control-label" for="example-inline-radio2">All parents</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio3" value="3" onclick="all_class();">
                                                <label class="custom-control-label" for="example-inline-radio3">All parents by class</label>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-12 sub_type"></label>
                                        <div class="col-12 check_lists">
                                            

                                        </div>
                                    </div>
<!--                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="date_of_issue" name="date_of_issue" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                                            <label for="date_of_issue">Issue Date</label>
                                        </div>
                                    </div>-->
                                    <div class="form-group">
                                            <div class="form-material">
                                                    <input type="text" class="form-control" id="notice_heading" name="notice_heading" placeholder="Heading">
                                                <label for="notice_heading">Notice Heading</label>
                                            </div>
                                    </div>
                                        
                                    <div class="form-group row">
                                                <label class="col-12" for="example-textarea-input">Notice</label>
                                                <div class="col-12">
                                                    <textarea class="form-control" id="editor1" name="notice_text" rows="6" placeholder="Content.."></textarea>
                                                </div>
                                            </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="date_of_birth" name="date_of_publish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                                            <label for="date_of_birth">Date of Publish</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                            <label class="col-12" for="example-file-multiple-input">Upload Docs (Multiple)</label>
                                            <div class="col-12">
                                                <input type="file" id="example-file-multiple-input" name="doc_upload[]" multiple="">
                                            </div>
                                        </div>

                                    

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
             <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1');
            </script>
            <script type="text/javascript">
                function all_class() {
                    $('.sub_type').text('All Classes');

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>school/user/get_class_list",
                        //data: "class_id=" + class_id,
                        success: function (msg) {
                            if (msg != "") {
                                $('.check_lists').html(msg);
                                $('#checkboxall').on('click', function () {

                                    if (this.checked) {
                                        $('.checkboxTeacher').each(function () {
                                            this.checked = true;
                                        });
                                    } else {
                                        $('.checkboxTeacher').each(function () {
                                            this.checked = false;
                                        });
                                    }
                                });

                                $('.checkboxTeacher').on('click', function () {
                                    if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
                                        $('#checkboxall').prop('checked', true);
                                    } else {
                                        $('#checkboxall').prop('checked', false);
                                    }
                                });

                            }
                        }
                    });
                }
                $(document).ready(function () {
                $('#date_of_birth').datepicker({
                    dateFormat: 'dd/mm/yy',
                });
                $('#date_of_issue').datepicker({
                    dateFormat: 'dd/mm/yy',
                });
            });
            </script>


        </div>


    </body>
</html>
