<?php //$this->load->view("school/_include/school_header");        ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Manage Permission</title>

        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this notice?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
            }
            function confirm_delete(id) {
                window.location.href = "<?php echo base_url(); ?>school/user/deleteNotice/" + id;
            }
        </script>
    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Manage Permission</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->

                            <?php
                            if (!empty($permissions)) {
                                $string = '';
                                $i = 1;
                                ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    
                                    <tbody>
                                        <?php
                                        foreach ($permissions as $row) {
                                            $CI =& get_instance(); 
                                            $exists = $CI->check_existing_permission($login['admin_id'],$row['id']); 
						  //echo '<pre>'; print_r($exists); die();
						foreach($exists as $exist ){  }  

                                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                            //$encrypted = $row['id'];
                                            ?>
                                            <tr>
								<input type="hidden" name="login_id" value="<?php echo $login['admin_id']; ?>"  />
								
								<td style="text-align:left;" title="<?php echo $row['page_urls'] ; ?>">
                                                                    <strong><?php echo $row['section_name']; ?></strong>
                                                                </td>
							
								<td>
                                                                    <label>
                                                                        <span class="spanwidth" style="float:left;font-weight:bold;">
                                                                            <input type="radio" name="type[<?php echo $row['id']; ?>]" value="1" <?php if($exist[0]["allowed_type"]==1) { echo ' checked="checked" ' ;} ?> />Allowed
                                                                        </span>
                                                                    </label>
                                                                </td>
						
								<td>
                                                                    <label>
                                                                        <span class="spanwidth" style="float:left;font-weight:bold;">
                                                                            <input type="radio" name="type[<?php echo $row['id']; ?>]" value="2" <?php if($exist[0]["allowed_type"]==2 || $exist[0]["allowed_type"]==0) { echo ' checked="checked" ' ;} ?> />Not Allowed
                                                                        </span>
                                                                    </label>
                                                                </td>
												
						</tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>  
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="9">No notice found</td>
                                </tr>
                                <?php
                            }
                            ?>
                                <center><?php echo form_submit('submit', 'Update', 'id="submit" class="add_button"'); ?></center>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->



                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>

    </body>
</html>

