<?php //$this->load->view("school/_include/school_header");           ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Edit Staff</title>


    </head>
    <body>

<?php $this->load->view('school/_include/loader'); ?>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit Staff</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-group row">
                                        <label class="col-12">Select Staff Type</label>
                                        <div class="col-12">
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required class="custom-control-input" type="radio" name="staff_type" id="example-inline-radio1" value="1" <?php
                                                if ($teacher['staff_type'] == 1) {
                                                    echo "checked";
                                                }
                                                ?>>
                                                <label class="custom-control-label" for="example-inline-radio1">Teaching Staff</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required class="custom-control-input" type="radio" name="staff_type" id="example-inline-radio2" value="2" <?php
                                                if ($teacher['staff_type'] == 2) {
                                                    echo "checked";
                                                }
                                                ?>>
                                                <label class="custom-control-label" for="example-inline-radio2">Non-teaching Staff</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $teacher['name']; ?>">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <?php $username = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $teacher['id'] . '"'); ?>
                                            <input required type="text" class="form-control" readonly="" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>">
                                            <label for="username">Username</label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" value="" autocomplete="new-password">
                                            <label for="password">Password</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="number" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $teacher['phone_no']; ?>"  onBlur="return check_username(this.value);">
                                            <label for="phone">Phone Number</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="email" class="form-control" name="email" id="Email" placeholder="Account email" value="<?php echo $teacher['email']; ?>">
                                            <label for="email">Email Address</label>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="form-material">
                                            <?php $status = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'status', 'and id = "' . $teacher['id'] . '"'); ?>
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" <?php
                                                if ($status == 1) {
                                                    echo "selected";
                                                }
                                                ?>>Active</option>
                                                <option value="2" <?php
                                                if ($status == 2) {
                                                    echo "selected";
                                                }
                                                ?>>Inactive</option>
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                       
                                        <?php
                                        $check_teacher_pic = $this->my_custom_functions->get_perticular_count(TBL_TEACHER_FILES, 'and teacher_id = "' . $teacher['id'] . '"');
                                        if ($check_teacher_pic > 0) {
                                            $imgfile = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER_FILES,'file_url','and teacher_id = "' . $teacher['id'] . '"');
                                            ?>
                                            <img src="<?php echo $imgfile; ?>" style="width:100px;height:100px;" />
                                            <span class="manu_chk"><input type="checkbox" name="del_img" class="form-group" value="1">Remove Image</span>

                                            <?php
                                        } else {
                                            $imgfile = base_url() . '_images/avatar15.jpg';
                                            ?>

                                            <img src="<?php echo $imgfile; ?>" style="width:100px;height:100px;" />
                                        <?php } ?>

                                        <label class="col-12" for="adminphoto">Upload Photo</label>
                                        <div class="col-12">
                                            <input type="file" id="adminphoto" name="adminphoto"><br>
                                            (Image size should be 300px*400px )
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <input type="hidden" name="teacher_id" value="<?php echo $teacher['id']; ?>">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>


    </body>
</html>



