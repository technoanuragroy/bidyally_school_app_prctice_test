<?php //$this->load->view("school/_include/school_header");         ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Create Student</title>


    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Create Student</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <h2 class="content-heading">Basic Details</h2>
                                    <div class="form-group reg_valid">
                                        <div class="form-material">
                                            <input required type="text" class="form-control validate_usr" name="reg_no" id="reg_no" placeholder="Enter Registration Number" value="" onblur="checkRegistrationNumber();">
                                            <label for="reg_no">Registration Number</label>
                                        </div>
                                        <div class="invalid-feedback animated fadeInDown regg" style="display:none;">Username not available</div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" id="date_of_birth" name="date_of_birth" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                                            <label for="date_of_birth">Date of Birth</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required class="form-control" id="status" name="gender">
                                                <option value="">Select Gender</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                            </select>
                                            <label for="status">Select Gender</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="father_name" id="father_name" placeholder="Enter Father Name" value="">
                                            <label for="father_name">Father Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Enter Mother Name" value="">
                                            <label for="mother_name">Mother Name</label>
                                        </div>
                                    </div>
                                    <h2 class="content-heading">Class Detail</h2>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required name="class_id" class="form-control class" onchange="get_section_list()">
                                                <option value="">Select Class</option>
                                                <?php foreach ($class_list as $class) { ?>
                                                    <option value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <label for="day_id">Select Class</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required name="section_id" class="form-control section">
                                                <option value="">Select Section</option>
                                            </select>

                                            <label for="day_id">Select Section</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="roll_no" id="roll_no" placeholder="Enter Roll Number" value="" >
                                            <label for="roll_no">Roll Number</label>
                                        </div>
                                    </div>

                                    <h2 class="content-heading">Personal Detail</h2>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" value="" >
                                            <label for="address">Address</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="number" class="form-control" name="aadhar_no" id="aadhar_no" placeholder="Enter Aadhar Number" value="" >
                                            <label for="aadhar_no">Aadhar Number</label>
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="emg_contact_person" id="emg_contact_person" placeholder="Enter Emergency Contact Name" value="" >
                                            <label for="emg_contact_person">Emergency Contact Name</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="number" class="form-control" name="emg_contact_no" id="emg_contact_no" placeholder="Enter Emergency Contact Number" value="" >
                                            <label for="emg_contact_no">Emergency Contact Number</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="blood_group" id="blood_group" placeholder="Enter Blood Group" value="" autocomplete="new-password">
                                            <label for="blood_group">Blood Group</label>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" selected>Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12" for="userphoto">Upload Photo</label>
                                        <div class="col-12">
                                            <input type="file" id="adminphoto" name="adminphoto"><br>
                                            (Image size should be 300px*400px )
                                        </div>
                                    </div>

                                    <h2 class="content-heading">Login Details</h2>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="number" class="form-control" name="contact_no[]" id="phone" placeholder="Enter Contact Number" value="" >
                                            <label for="contact_no">Contact Number</label>
                                        </div>

                                    </div>
                                    <div id="username_append"></div>

                                    <a href="javascript:" id="add_field">Add Number(s)</a>


                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <script src="<?php echo base_url(); ?>_js/jquery-ui.js"></script>
            <!-- END Footer -->
        </div>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#date_of_birth').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd/mm/yy',
                });
                $("#add_field").click(function () {
                    $("#username_append").append('<div class="form-group"><div class="form-material"><input type="number" class="form-control" name="contact_no[]" placeholder="Enter Contact Number" value="" ><label for="contact_no">Contact Number</label></div><a href="havascript:" class="remove_fiels();">Remove</a></div>');
                });

                $(".remove_fiels").click(function () {
                    $(this).remove();
                });



            });

            function get_section_list() {
                var class_id = $('.class').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/get_section_list",
                    data: "class_id=" + class_id,
                    success: function (msg) {
                        if (msg != "") {
                            //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                            $(".section").html(msg);
                        } else {
                            $(".section").html("");
                        }
                    }
                });


            }


            function checkRegistrationNumber() {
                var reg_val = $('#reg_no').val();

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/checkRegistrationNumber",
                    data: "reg_val=" + reg_val,
                    success: function (msg) {
                        if (msg == 0) {
                            //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                            $(".section").html(msg);
                        } else {
                            $('.validate_usr').removeClass('valid');
                            $('.reg_valid').addClass('is-invalid');
                            $('.regg').slideDown(500);

                        }
                    }
                });
            }

        </script>

    </body>
</html>



