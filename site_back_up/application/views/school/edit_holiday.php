<?php //$this->load->view("school/_include/school_header");           ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Edit holiday</title>


    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit holiday</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>




                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php echo $holiday_detail['title']; ?>">
                                            <label for="title">Title</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12" for="example-textarea-input">Content</label>
                                        <div class="col-12">
                                            <textarea class="form-control" id="editor1" name="description" rows="6" placeholder="Content.."></textarea>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="from_date" name="from_date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y', strtotime($holiday_detail['start_date'])); ?>">
                                            <label for="from_date">From Date</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="to_date" name="to_date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php
                                            if ($holiday_detail['end_date'] != '0000-00-00') {
                                                echo date('d/m/Y', strtotime($holiday_detail['end_date']));
                                            }
                                            ?>">
                                            <label for="to_date">To date</label>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-12">Office open</label>
                                        <div class="col-12">
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="office_close" id="example-inline-radio1" value="1" <?php
                                                if ($holiday_detail['office_open'] == 1) {
                                                    echo 'checked';
                                                }
                                                ?>>
                                                <label class="custom-control-label" for="example-inline-radio1">Yes</label>
                                            </div>
                                            <div  class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="office_close" id="example-inline-radio2" value="2" <?php
                                                if ($holiday_detail['office_open'] == 2) {
                                                    echo 'checked';
                                                }
                                                ?>>
                                                <label class="custom-control-label" for="example-inline-radio2">No</label>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" <?php
                                                if ($holiday_detail['status'] == 1) {
                                                    echo 'selected';
                                                }
                                                ?>>Active</option>
                                                <option value="0" <?php
                                                if ($holiday_detail['status'] == 0) {
                                                    echo 'selected';
                                                }
                                                ?>>Inactive</option>
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="holiday_id" value="<?php echo $holiday_detail['id']; ?>">

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1');
            </script>
            <script type="text/javascript">

                $(document).ready(function () {
                    $('#from_date').datepicker({
                        dateFormat: 'dd/mm/yy',
                    });

                    $('#to_date').datepicker({
                        dateFormat: 'dd/mm/yy',
                    });
                });
            </script>


        </div>


    </body>
</html>
