<?php //$this->load->view("school/_include/school_header");            ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Edit Student</title>


    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit Student</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>
                        <?php //echo "<pre>";print_r($get_student_detail); ?>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <h2 class="content-heading">Basic Details</h2>
                                    <div class="form-group reg_valid">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="reg_no" id="reg_no" placeholder="Enter Registration Number" value="<?php echo $get_student_detail['student_detail'][0]['registration_no']; ?>" onblur="checkRegistrationNumber();">
                                            <label for="reg_no">Registration Number</label>
                                        </div>
                                        <div class="invalid-feedback animated fadeInDown regg" style="display:none;">Username not available</div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?php echo $get_student_detail['student_detail'][0]['name']; ?>">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" id="date_of_birth" name="date_of_birth" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y', strtotime($get_student_detail['student_detail'][0]['dob'])); ?>">
                                            <label for="date_of_birth">Date of Birth</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required class="form-control" id="gender" name="gender">
                                                <option value="">Select Gender</option>
                                                <option value="1" <?php
                                                if ($get_student_detail['student_detail'][0]['gender'] == 1) {
                                                    echo "selected";
                                                };
                                                ?>>Male</option>
                                                <option value="2" <?php
                                                if ($get_student_detail['student_detail'][0]['gender'] == 2) {
                                                    echo "selected";
                                                };
                                                ?>>Female</option>
                                            </select>
                                            <label for="status">Select Gender</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="father_name" id="father_name" placeholder="Enter Father Name" value="<?php echo $get_student_detail['student_detail'][0]['father_name']; ?>">
                                            <label for="father_name">Father Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Enter Mother Name" value="<?php echo $get_student_detail['student_detail'][0]['mother_name']; ?>">
                                            <label for="mother_name">Mother Name</label>
                                        </div>
                                    </div>
                                    <h2 class="content-heading">Class Detail</h2>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required name="class_id" class="form-control class" onchange="get_section_list()">
                                                <option value="">Select Class</option>
                                                <?php
                                                foreach ($class_list as $class) {
                                                    if ($get_student_detail['student_detail'][0]['class_id'] == $class['id']) {
                                                        $selected = 'selected="selected"';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <label for="day_id">Select Class</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required name="section_id" class="form-control section">
                                                <option value="">Select Section</option>
                                            </select>

                                            <label for="day_id">Select Section</label>
                                        </div>
                                        <input type="hidden" name="sec_id_db" class="sec_id_db" value="<?php echo $get_student_detail['student_detail'][0]['section_id']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="roll_no" id="roll_no" placeholder="Enter Roll Number" value="<?php echo $get_student_detail['student_detail'][0]['roll_no']; ?>" >
                                            <label for="roll_no">Roll Number</label>
                                        </div>
                                    </div>

                                    <h2 class="content-heading">Personal Detail</h2>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" value="<?php echo $get_student_detail['student_detail'][0]['address']; ?>" >
                                            <label for="address">Address</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="number" class="form-control" name="aadhar_no" id="aadhar_no" placeholder="Enter Aadhar Number" value="<?php if($get_student_detail['student_detail'][0]['aadhar_no'] != 0){ echo $get_student_detail['student_detail'][0]['aadhar_no'];} ?>" >
                                            <label for="aadhar_no">Aadhar Number</label>
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="emg_contact_person" id="emg_contact_person" placeholder="Enter Emergency Contact Name" value="<?php echo $get_student_detail['student_detail'][0]['emergency_contact_person']; ?>" >
                                            <label for="emg_contact_person">Emergency Contact Name</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="number" class="form-control" name="emg_contact_no" id="emg_contact_no" placeholder="Enter Emergency Contact Number" value="<?php if($get_student_detail['student_detail'][0]['emergency_contact_no'] != 0){ echo $get_student_detail['student_detail'][0]['emergency_contact_no'];} ?>" >
                                            <label for="emg_contact_no">Emergency Contact Number</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="blood_group" id="blood_group" placeholder="Enter Blood Group" value="<?php echo $get_student_detail['student_detail'][0]['blood_group']; ?>" autocomplete="new-password" >
                                            <label for="blood_group">Blood Group</label>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" <?php
                                                if ($get_student_detail['student_detail'][0]['status'] == 1) {
                                                    echo "selected";
                                                };
                                                ?>>Active</option>
                                                <option value="0" <?php
                                                if ($get_student_detail['student_detail'][0]['status'] == 0) {
                                                    echo "selected";
                                                };
                                                ?>>Inactive</option>
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <?php
                                        if (!empty($student_img_file)) {

                                            $imgfile = $student_img_file['file_url'];
                                            ?>
                                            <img src="<?php echo $imgfile; ?>" style="width:100px;height:100px;" />
                                            <span class="manu_chk"><input type="checkbox" name="del_img" class="form-group" value="1">Remove Image</span>

                                            <?php
                                        } else {
                                            $imgfile = base_url() . '_images/avatar15.jpg';
                                            ?>

                                            <img src="<?php echo $imgfile; ?>" style="width:100px;height:100px;" />
                                        <?php } ?>
                                        <label class="col-12" for="userphoto">Upload Photo</label>
                                        <div class="col-12">
                                            <input type="file" id="adminphoto" name="adminphoto"><br>
                                            (Image size should be 300px*400px )
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <input type="hidden" name="student_id" value="<?php echo $get_student_detail['student_detail'][0]['id']; ?>">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->


                                    <h2 class="content-heading">Username Connected to Parents</h2>
                                    <div id="user"></div>
                                     <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                            <?php } ?>
                                    
                                    
                                    <?php foreach ($get_student_detail['username_detail'] as $users) { ?>
                                        <div class="form-group del_section_<?php echo $users['id']; ?>">
                                            <div class="form-material">
                                                <div class="noDel_wrap">
                                                    <label for="contact_no" class="contactNo">Contact Number</label>
                                                    <span class="showNo"><?php echo $users['username']; ?></span>
                                                    <a href="javascript:" data-toggle="modal" data-target="#modal-top" onclick="deleteUsername('<?php echo $users['id']; ?>','<?php echo $this->uri->segment(4); ?>');"><span class="del_icon"><i class="si si-trash"></i></span></a>

                                                </div>
                                            </div>
                                        </div>
                                    <div class="show_msg_<?php echo $users['id']; ?>"></div>
                                    <?php } ?>
                                    <div class="popup_btn_wrap">

                                        <a href="javascript:" class="btn btn-alt-primary open_pop">Add Number(s)</a>
                                        <div class="popWrap">
                                            <div class="popInn">
                                                <form method="post" action="<?php echo base_url(); ?>school/user/addMoreUsernames">
                                                    <input type="text" name="user_phone_no" class="usr" placeholder="Enter Phone No">
                                                    <input type="hidden" name="stdnt_id_for_usrname" value="<?php echo $this->uri->segment(4); ?>">
                                                    <input type="submit" value="Add" class="btn btn-alt-primary">
                                                    
                                                    
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="username_append"></div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#date_of_birth').datepicker({
                    dateFormat: 'dd/mm/yy',
                });
                $("#add_field").click(function () {
                    $("#username_append").append('<div class="form-group"><div class="form-material"><input required type="number" class="form-control" name="contact_no[]" placeholder="Enter Contact Number" value="" ><label for="contact_no">Contact Number</label></div><a href="javascript:" onClick="remove_fiels();">Remove</a></div>');
                });

                $(".remove_fiels").click(function () {
                    $(this).remove();
                });


                var class_id = $('.class').val();
                var sec_id = $('.sec_id_db').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/get_section_list",
                    data: "class_id=" + class_id + "& sec_id=" + sec_id,
                    success: function (msg) {

                        if (msg != "") {

                            //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                            $(".section").html(msg);
                        } else {
                            $(".section").html("");
                        }
                    }
                });




            });

            function get_section_list() {
                var class_id = $('.class').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/get_section_list",
                    data: "class_id=" + class_id,
                    success: function (msg) {
                        if (msg != "") {
                            //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                            $(".section").html(msg);
                        } else {
                            $(".section").html("");
                        }
                    }
                });


            }


            function checkRegistrationNumber() {
                var reg_val = $('#reg_no').val();

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/checkRegistrationNumber",
                    data: "reg_val=" + reg_val,
                    success: function (msg) {
                        if (msg == 0) {
                            //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                            $(".section").html(msg);
                        } else {
                            $('.reg_valid').addClass('is-invalid');
                            $('.regg').slideDown(500);

                        }
                    }
                });
            }
            
            function deleteUsername(id,student_id){
                $('.message_block').html('<p>Are you sure that you want to delete this number?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('"+id+"','" +student_id+"')");
            }
            
            function confirm_delete(id,student_id) {
                
                
                   $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/deleteUsername",
                    data: "id=" + id +" & student_id="+student_id,
                    success: function (msg) {
                        $('.del_section_'+id).slideUp(500);
                        $('.show_msg_'+id).html('<div class="alert alert-success alert-dismissable s_message" role="alert"><button type="button" class="close" data-dismiss="alert" arialabel="Close"><span aria-hidden="true">&times;</span></button><h3 class="alert-heading font-size-h4 font-w400">Success</h3><p class="mb-0">Username successfully deleted</p></div>')
                        $('.show_msg_'+id).slideDown(500);
                    }
                });
            }
            $(document).ready(function () {
                $(".open_pop").click(function () {
                    $(".popWrap").toggleClass("openPopWrap");
                });
            });

        </script>

    </body>
</html>



