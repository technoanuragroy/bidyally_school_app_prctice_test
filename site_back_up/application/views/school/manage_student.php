<?php //$this->load->view("school/_include/school_header");            ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Manage Student</title>

        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this student?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
            }
            function confirm_delete(id) {
                //alert(id);
                window.location.href = "<?php echo base_url(); ?>school/user/deleteStudent/" + id;
            }


        </script>





    </head>
    <body>


        <?php $this->load->view('school/_include/loader'); ?>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
         
            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Manage Student</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <a href="<?php echo base_url(); ?>school/user/addStudent" class="btn btn-primary">Add Student</a>
                            <a href="javascript:" class="btn btn-primary" id="clickbtn">Search Student</a>

                        </div>
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>


                        <div class="block-content block-content-full">
                            <div id="div_slideDown">
                                <form method="post" action="<?php echo base_url(); ?>school/user/searchStudent">
                                    <div class="form-row">
                                        <div class="form-material col-md-6">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="">
                                            <label for="name">Name</label>
                                        </div>
                                        <div class="form-material col-md-6">
                                            <label for="inputPassword4">Roll Number</label>
                                            <input type="text" class="form-control" id="rollno" name="rollno" placeholder="rollno">
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-material col-md-6">
                                            <select class="form-control class" id="class" name="class" onchange="get_section_list();">
                                                <option value="">Select Class</option>
                                                <?php
                                                $selected = '';
                                                foreach ($class_list as $class) {
                                                    if ($post_data['class_id'] != '') {
                                                        if ($post_data['class_id'] == $class['id']) {

                                                            $selected = 'selected="selected"';
                                                        } else {

                                                            $selected = '';
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                                <?php } ?>

                                            </select>

                                        </div>
                                        <div class="form-material col-md-6">
                                            <select class="form-control section" id="section" name="section">
                                                <option value="">Select Section</option>
                                            </select>
                                            <label for="status">Select Section</label>
                                        </div>


                                    </div>

                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>


                            </div>

                            <?php
                            if (!empty($students)) {
                                $i = 1;
                                ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                        <tr>
                                            <th class="text-center"></th>
                                            <th>Student name</th>
                                            <th>Class</th>
                                            <th>Section</th>
                                            <th>ROll No</th>
                                            <th>Father Name</th>
                                            <th>Mother Name</th>
                                            <th>Status</th>
                                            <th class="text-center" style="width: 15%;">Edit</th>
                                            <th class="text-center" style="width: 15%;">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($students as $row) {


                                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                            //$encrypted = $row['id'];
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $i; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $row['class_id'] . '"'); ?></td>
                                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $row['section_id'] . '"'); ?></td>
                                                <td><?php echo $row['roll_no']; ?></td>
                                                <td><?php echo $row['father_name']; ?></td>
                                                <td><?php echo $row['mother_name']; ?></td>

                                                <td><?php
                                                    if ($row['status'] == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?></td>
                                                <td class="text-center">                                    
                                                    <a href="<?php echo base_url() . 'school/user/editStudent/' . $encrypted; ?>" title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center">         

                                                    <a href="<?php echo base_url() . 'school/user/deleteStudent/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>  
                                    </tbody>
                                </table>

                            <?php } else { ?>
                                <tr>
                                    <td colspan="9">No student found</td>
                                </tr>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->



                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->

            <script type="text/javascript">
                function get_section_list() {
                    var class_id = $('.class').val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>school/user/get_section_list",
                        data: "class_id=" + class_id,
                        success: function (msg) {
                            if (msg != "") {
                                //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                                $(".section").html(msg);
                            } else {
                                $(".section").html("");
                            }
                        }
                    });


                }
            </script>
        </div>

    </body>
</html>
