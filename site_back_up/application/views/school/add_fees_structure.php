<?php //$this->load->view("school/_include/school_header");        ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Add Fees Structure</title>


    </head>
    <body>

<?php $this->load->view('school/_include/loader'); ?>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Add Fees Structure</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-12">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select name="class_id" class="form-control" id="class_id" required>
                                                <option value="">Select</option>
                                                <?php foreach ($class_list as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['class_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                            <label for="class_id">Select Class</label>
                                        </div>
                                    </div>
                                    <div class="variant_container_append_to">
                                        <div class="container_del">
                                            <div class="form-group custom-block">
                                                <div class="form-material">
                                                    <input required type="text" class="form-control" name="label[]" id="section_name_1" placeholder="Label">
                                                    <label for="section_name_1">label</label>
                                                </div>
                                            </div>

                                            <div class="form-group custom-block">
                                                <div class="form-material">
                                                    <input required type="text" class="form-control" name="amount[]" id="section_amount_1" placeholder="Amount">
                                                    <label for="section_amount_1">Amount</label>
                                                </div>
                                            </div> 
                                            <div class="form-group custom-block">
                                                <div class="form-material">
                                                    <select required="" name="option[]" id="section_option_1" class="form-control">
                                                        <option value="1">Yes</option>
                                                        <option value="2">No</option>
                                                        
                                                    </select>
                                                    <label for="section_amount_1">Option</label>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>

                                    <div class="variant_container_copier" style="display: none;">
                                        <div class="container_del">
                                            <div class="form-group custom-block">
                                                <div class="form-material">
                                                    <input required type="text" class="form-control" name="label[]" id="section_name_1" placeholder="Label">
                                                    <label for="section_name_1">label</label>
                                                </div>
                                            </div>

                                            <div class="form-group custom-block">
                                                <div class="form-material">
                                                    <input required type="text" class="form-control" name="amount[]" id="section_amount_1" placeholder="Amount">
                                                    <label for="section_amount_1">Amount</label>
                                                </div>
                                            </div>
                                            <div class="form-group custom-block">
                                                <div class="form-material">
                                                    <select required="" name="option[]" id="section_option_1" class="form-control">
                                                        <option value="1">Yes</option>
                                                        <option value="2">No</option>
                                                        
                                                    </select>
                                                    <label for="section_amount_1">Option</label>
                                                </div>
                                            </div>

                                            <a href="javascript:" class="" onclick="remove_variant(this);"><i class="fa fa-minus-circle"></i></a>
                                        </div>                                        
                                    </div>

                                    <div class="append_data"></div>
                                    <a href="javascript:" class="add_variant_link"><i class="fa fa-plus-circle"></i></a>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <textarea class="form-control" name="comment" id="section_comment"></textarea>
                                            <label for="section_comment">Comment</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>

        <script type="text/javascript">
            $(".add_variant_link").click(function () {

                var variant_container_html = $(".variant_container_copier").html();
                $(".variant_container_append_to").append(variant_container_html);

                $(".variant_container_append_to").find(".attribute_id").each(function () {
                    $(this).attr("name", "attribute_id[]");
                });
            });
            function remove_variant(e) {

                $(e).closest(".container_del").remove();
            }
        </script>
    </body>
</html>
