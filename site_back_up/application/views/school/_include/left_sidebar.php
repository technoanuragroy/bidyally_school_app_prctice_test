
<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <!--        <div class="content-header content-header-fullrow px-15">
                     Mini Mode 
                    <div class="content-header-section sidebar-mini-visible-b">
                         Logo 
                        <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                            <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                        </span>
                         END Logo 
                    </div>
                     END Mini Mode 
        
                     Normal Mode 
                    <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                         Close Sidebar, Visible only on mobile screens 
                         Layout API, functionality initialized in Template._uiApiLayout() 
                        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                            <i class="fa fa-times text-danger"></i>
                        </button>
                         END Close Sidebar 
        
                         Logo 
                        <div class="content-header-item">
                            <a class="link-effect font-w700" href="index.html">
                                            <i class="si si-fire text-primary"></i>
                                <span class="font-size-xl text-dual-primary-dark">
        <?php
        if ($this->session->userdata('usertype') == SCHOOL) {
            echo $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"');
        } else {
            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $this->session->userdata('school_id') . '"');
            echo $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $school_id . '"');
        }
        ?>
                                </span>
                            </a>
                        </div>
                         END Logo 
                    </div>
                     END Normal Mode 
                </div>-->
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="<?php echo base_url(); ?>_images/avatar15.jpg" alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="be_pages_generic_profile.html">
                    <?php
                    $check_school_logo = $this->my_custom_functions->get_perticular_count(TBL_SCHOOL_LOGO_FILES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
                    if ($check_school_logo > 0) {
                        $school_url = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_LOGO_FILES, 'file_url', 'and school_id = "' . $this->session->userdata('school_id') . '"');
                        ?>
                        <img class="img-avatar" src="<?php echo $school_url; ?>" alt="">
                    <?php } else { ?>
                        <img class="img-avatar" src="<?php echo base_url(); ?>_images/avatar15.jpg" alt="">
                    <?php } ?>
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="be_pages_generic_profile.html"><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"'); ?></a>
                    </li>
                    <li class="list-inline-item">
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                            <i class="si si-drop"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="<?php echo base_url(); ?>school/user/logout">
                            <i class="si si-logout"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">

                <li>
                    <a class="" href="<?php echo base_url(); ?>school/user/dashBoard"><i class="fas fa-home"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                </li>

                <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Information</span></a>
                    <ul>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/manageClasses">
<!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                <span class="sidebar-mini-hide">Manage Classes</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/manageSections">
        <!--                        <i class="fas fa-users"></i>-->
                                <span class="sidebar-mini-hide">Manage Sections</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/manageSubjects">
<!--                                <i class="fas fa-book-open"></i>-->
                                <span class="sidebar-mini-hide">Manage Subjects</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a class="" href="<?php echo base_url(); ?>school/user/manageTeachers"><i class="fas fa-user-friends"></i><span class="sidebar-mini-hide">Manage Staff</span></a>
                </li>
                <!--                            <li>
                                                <a class="active" href="<?php //echo base_url();        ?>school/user/managePeriods"><i class="si si-cup"></i><span class="sidebar-mini-hide">Manage Periods</span></a>
                                            </li>-->
                <li>
                    <a class="" href="<?php echo base_url(); ?>school/user/manageTimeTable"><i class="fas fa-calendar-week"></i><span class="sidebar-mini-hide">Manage Time Table</span></a>
                </li>
                <li>
                    <a class="" href="<?php echo base_url(); ?>school/user/manageStudent"><i class="fas fa-user-graduate"></i><span class="sidebar-mini-hide">Manage Student</span></a>
                </li>
                <li>
                    <a class="" href="<?php echo base_url(); ?>school/user/manageNotice"><i class="fas fa-bell"></i><span class="sidebar-mini-hide">Manage Notice</span></a>
                </li>

                <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">General Info</span></a>
                    <ul>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/manageHolidays">
        <!--                        <i class="far fa-calendar-alt"></i>-->
                                <span class="sidebar-mini-hide">Manage Calender</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/manageSyllabus">
        <!--                        <i class="fas fa-book-reader"></i>-->
                                <span class="sidebar-mini-hide">Manage Syllabus</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/generalInformation">
        <!--                        <i class="fas fa-info-circle"></i>-->
                                <span class="sidebar-mini-hide">General Information</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="<?php echo base_url(); ?>school/user/manageFeesStructure">
        <!--                        <i class="fas fa-money-check"></i>-->
                                <span class="sidebar-mini-hide">manage Fees Structure</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Reports</span></a>
                    <ul>
                        <li>
                            <a class="" href="#">
<!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                <span class="sidebar-mini-hide">Attendance Report</span>
                            </a>
                        </li>
                        <li>
                            <a class="" href="#">
        <!--                        <i class="fas fa-users"></i>-->
                                <span class="sidebar-mini-hide">Class activity report</span>
                            </a>
                        </li>

                    </ul>
                </li>





<!--                <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">User Interface</span></li>-->

                <!--                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-moustache"></i><span class="sidebar-mini-hide">Reports</span></a>
                                    <ul>
                                        <li>
                                            <a href="be_blocks_widgets_users.html">Report1</a>
                                        </li>
                                        <li>
                                            <a href="be_blocks_widgets_stats.html">Report2</a>
                                        </li>
                                        <li>
                                            <a href="be_blocks_widgets_media.html">Report3</a>
                                        </li>
                                    </ul>
                                </li>-->
                <!--                            <li>
                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-energy"></i><span class="sidebar-mini-hide">Elements</span></a>
                                                <ul>
                                                    <li>
                                                        <a href="be_ui_grid.html">Grid</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_icons.html">Icons</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_typography.html">Typography</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_activity.html">Activity</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_buttons.html">Buttons</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_navigation.html">Navigation</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_tabs.html">Tabs</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_modals_tooltips.html">Modals &amp; Tooltips</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_images.html">Images</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_animations.html">Animations</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_ribbons.html">Ribbons</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_timeline.html">Timeline</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_accordion.html">Accordion</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_ui_color_themes.html">Color Themes</a>
                                                    </li>
                                                </ul>
                                            </li>-->
                <!--                            <li>
                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Tables</span></a>
                                                <ul>
                                                    <li>
                                                        <a href="be_tables_styles.html">Styles</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_tables_responsive.html">Responsive</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_tables_helpers.html">Helpers</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_tables_pricing.html">Pricing</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_tables_datatables.html">DataTables</a>
                                                    </li>
                                                </ul>
                                            </li>-->
                <!--                            <li>
                                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-note"></i><span class="sidebar-mini-hide">Forms</span></a>
                                                <ul>
                                                    <li>
                                                        <a href="be_forms_elements_bootstrap.html">Bootstrap Elements</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_elements_material.html">Material Elements</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_css_inputs.html">CSS Inputs</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_plugins.html">Plugins</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_editors.html">Editors</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_validation.html">Validation</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_wizard.html">Wizard</a>
                                                    </li>
                                                    <li>
                                                        <a href="be_forms_premade.html">Pre-made</a>
                                                    </li>
                                                </ul>
                                            </li>-->






            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>