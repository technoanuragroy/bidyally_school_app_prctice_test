<?php //$this->load->view("school/_include/school_header");    ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Add Time Table</title>
        

    </head>
    <body>

<?php $this->load->view('school/_include/loader'); ?>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Create Time Table</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">

                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <?php $day_list = $this->config->item('days_list'); ?>
                                            <select required name="day_id" id="day_id" class="form-control">
                                                <option value="">Select Day</option>
                                                <?php foreach ($day_list as $kkey => $daylist) { ?>
                                                    <option value="<?php echo $kkey; ?>"><?php echo $daylist; ?></option>
                                                <?php } ?>
                                            </select>
                                            <label for="day_id">Select Day</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required name="class_id" class="form-control class" onchange="get_section_list()">
                                                <option value="">Select Class</option>
                                                <?php foreach ($class_list as $class) { ?>
                                                    <option value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <label for="day_id">Select Class</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select required name="section_id" class="form-control section">
                                                <option value="">Select Section</option>
                                            </select>

                                            <label for="day_id">Select Section</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" name="period_name" id="period_name" class="form-control" placeholder="Period Name">
                                            <label for="period_name">Enter Period Name</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control timepicker" name="period_start_time" id="period_start_time" placeholder="Period Start Time" value="">
                                            <label for="period_start_time">Period Start Time</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control timepicker" name="period_end_time" id="period_end_time" placeholder="Period End Time" value="">
                                            <label for="period_end_time">Period End Time</label>
                                        </div>
                                    </div>

                                   <div class="form-group">
                                        <div class="form-material">
                                            <select name="subject_id" id="subject_id" class="form-control subject">
                                            <option value="">Select Subject</option> 
                                            <?php foreach ($subject_list as $subject) { ?>
                                                <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>

                                        </select>
                                    <label for="subject_id">Select Subject</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select  name="teacher_id" id="teacher_id" class="form-control">
                                            <option value="">Select Teacher</option>
                                            <?php foreach ($teacher_list as $teacher) { ?>
                                                <option value="<?php echo $teacher['id']; ?>"><?php echo $teacher['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    <label for="teacher_id">Select Teacher</label>
                                        </div>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                        <input class="custom-control-input checkboxTeacher" type="checkbox" name="attendance_class" id="attendance_class" value="1">
                                        <label class="custom-control-label" for="attendance_class">Make as attendance period</label>
                                    </div>

                                    <div class="form-group">
                                            <input type="submit" class="btn btn-alt-primary" name="submit" value="Save">
                                        </div>
                                        <?php echo form_close(); ?>
<!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            
            <!-- END Footer -->
            
            <script type="text/javascript" >
            $(document).ready(function () {
                

                $('.timepicker').timepicker({
                    timeFormat: 'h:mm p',
                    interval: 5,
                    minTime: '6',
                    maxTime: '7:00pm',
                    defaultTime: '6',
                    startTime: '06:00',
                    dynamic: false,
                    dropdown: true,
                    scrollbar: true
                });

            });

            function get_section_list() {
                var class_id = $('.class').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/get_section_list",
                    data: "class_id=" + class_id,
                    success: function (msg) {
                        if (msg != "") {
                            //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                            $(".section").html(msg);
                        } else {
                            $(".section").html("");
                        }
                    }
                });


            }

        </script>    
        </div>
        

    </body>
</html>


