<?php //$this->load->view("school/_include/school_header");           ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Edit Staff</title>


    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit Staff</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $school_details['name']; ?>">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="contact_person_name" id="contact_person_name"  type="text" value="<?php echo $school_details['contact_person_name']; ?>">
                                                <label for="contact_person_name">Contact person name</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="mobile_no" id="mobile_no" type="text" value="<?php echo $school_details['mobile_no']; ?>">
                                                <label for="mobile_no">Mobile Number</label>
                                            </div></div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="email_address" id="email_address" type="text" value="<?php echo $school_details['email_address']; ?>">
                                                <label for="email_address">Email Address</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="address" id="Address" type="text" value="<?php echo $school_details['address']; ?>">
                                                <label for="Address">Address</label>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-group row">
                                        <?php
                                        $check_school_logo = $this->my_custom_functions->get_perticular_count(TBL_SCHOOL_LOGO_FILES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
                                        if ($check_school_logo > 0) {
                                            $imgfile = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_LOGO_FILES, 'file_url', 'and school_id = "' . $this->session->userdata('school_id') . '"');
                                            ?>
                                            <img src="<?php echo $imgfile; ?>" style="width:200px;height:200px;" />
                                            <span class="manu_chk"><input type="checkbox" name="del_img" class="form-group" value="1">Remove Image</span>

                                            <?php
                                        } else {
                                            $imgfile = base_url() . '_images/avatar15.jpg';
                                            ?>

                                            <img src="<?php echo $imgfile; ?>" />
                                        <?php } ?>


                                        <div class="form-group row">
                                            <label class="col-12" for="userphoto">Upload Photo</label>
                                            <div class="col-12">
                                                <input type="file" id="adminphoto" name="adminphoto"><br>
                                                (Image size should be 400px*400px )
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <input type="hidden" name="school_id" value="<?php echo $school_details['id']; ?>">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Save">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>


    </body>
</html>



