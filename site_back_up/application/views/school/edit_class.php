<?php //$this->load->view("school/_include/school_header");      ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Dashboard</title>


    </head>
    <body>

<?php $this->load->view('school/_include/loader'); ?>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    <!-- Bootstrap Forms Validation -->


                    <!-- Bootstrap Forms Validation -->

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit class</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <!-- jQuery Validation functionality is initialized in js/pages/be_forms_validation.min.js which was auto compiled from _es6/pages/be_forms_validation.js -->
                                    <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
                                    <!--                                    <form class="js-validation-material" action="be_forms_validation.html" method="post">-->
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" id="class_name" name="class_name" placeholder="Enter a class name" value="<?php echo $class['class_name']; ?>">
                                            <label for="class_name">Class Name</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" <?php
                                                if ($class['status'] == 1) {
                                                    echo "selected";
                                                }
                                                ?>>Active</option>
                                                <option value="0" <?php
                                                if ($class['status'] == 0) {
                                                    echo "selected";
                                                }
                                                ?>>Inactive</option> 
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="hidden" name="class_id" value="<?php echo $class['id']; ?>">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Onboarding Modal functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->



        <!--        <div class="modal fade" id="modal-onboarding" tabindex="-1" role="dialog" aria-labelledby="modal-onboarding" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-popout" role="document">
                        <div class="modal-content rounded">
                            <div class="block block-rounded block-transparent mb-0 bg-pattern" style="background-image: url('assets/media/various/bg-pattern-inverse.png');">
                                <div class="block-header justify-content-end">
                                    <div class="block-options">
                                        <a class="font-w600 text-danger" href="#" data-dismiss="modal" aria-label="Close">
                                            Skip Intro
                                        </a>
                                    </div>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="js-slider slick-dotted-inner" data-dots="true" data-arrows="false" data-infinite="false">
                                        <div class="pb-50">
                                            <div class="row justify-content-center text-center">
                                                <div class="col-md-10 col-lg-8">
                                                    <i class="si si-fire fa-4x text-primary"></i>
                                                    <h3 class="font-size-h2 font-w300 mt-20">Welcome to Codebase!</h3>
                                                    <p class="text-muted">
                                                        This is a modal you can show to your users when they first sign in to their dashboard. It is a great place to welcome and introduce them to your application and its functionality.
                                                    </p>
                                                    <button type="button" class="btn btn-sm btn-hero btn-noborder btn-primary mb-10 mx-5" onclick="jQuery('.js-slider').slick('slickGoTo', 1);">
                                                        Key features <i class="fa fa-arrow-right ml-5"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide pb-50">
                                            <div class="row justify-content-center">
                                                <div class="col-md-10 col-lg-8">
                                                    <h3 class="font-size-h2 font-w300 mb-5">Backup</h3>
                                                    <p class="text-muted">
                                                        Backups are taken with every new change to ensure complete piece of mind. They are kept safe for immediate restores.
                                                    </p>
                                                    <h3 class="font-size-h2 font-w300 mb-5">Invoices</h3>
                                                    <p class="text-muted">
                                                        They are sent automatically to your clients with the completion of every project, so you don't have to worry about getting paid.
                                                    </p>
                                                    <div class="text-center">
                                                        <button type="button" class="btn btn-sm btn-hero btn-noborder btn-primary mb-10 mx-5" onclick="jQuery('.js-slider').slick('slickGoTo', 2);">
                                                            Complete Profile <i class="fa fa-arrow-right ml-5"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slick-slide pb-50">
                                            <div class="row justify-content-center text-center">
                                                <div class="col-md-10 col-lg-8">
                                                    <i class="si si-note fa-4x text-primary"></i>
                                                    <h3 class="font-size-h2 font-w300 mt-20">Finally, let us know your name</h3>
                                                    <form class="push">
                                                        <input type="text" class="form-control form-control-lg py-20 border-2x" id="onboard-first-name" name="onboard-first-name" placeholder="Enter your first name..">
                                                    </form>
                                                    <button type="button" class="btn btn-sm btn-hero btn-noborder btn-success mb-10 mx-5" data-dismiss="modal" aria-label="Close">
                                                        Get Started <i class="fa fa-check ml-5"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->



        <!-- END Onboarding Modal -->


        <!--
            Codebase JS Core

            Vital libraries and plugins used in all pages. You can choose to not include this file if you would like
            to handle those dependencies through webpack. Please check out assets/_es6/main/bootstrap.js for more info.

            If you like, you could also include them separately directly from the assets/js/core folder in the following
            order. That can come in handy if you would like to include a few of them (eg jQuery) from a CDN.

            assets/js/core/jquery.min.js
            assets/js/core/bootstrap.bundle.min.js
            assets/js/core/simplebar.min.js
            assets/js/core/jquery-scrollLock.min.js
            assets/js/core/jquery.appear.min.js
            assets/js/core/jquery.countTo.min.js
            assets/js/core/js.cookie.min.js
        -->

    </body>
</html>


