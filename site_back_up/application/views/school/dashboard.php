<?php //$this->load->view("school/_include/school_header");  ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Dashboard</title>

        <script src="<?php echo base_url(); ?>_js/chart.js"></script>
    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>


        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('school/_include/right_side_overlay'); ?>
            <!-- END Side Overlay -->

            <!-- Left Sidebar start -->
            <?php $this->load->view('school/_include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('school/_include/school_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    <div class="row invisible" data-toggle="appear">
                        <!-- Row #1 -->
                        
                        <div class="col-6 col-xl-3">
                            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix" data-toggle="tooltip" data-placement="top" title="Active Students/Total License">
                                    <div class="float-left mt-10 d-none d-sm-block">
                                        <i class="si si-bag fa-3x text-body-bg-dark"></i>
                                    </div>
                                    <?php $no_of_license = $no_of_students + 10; ?>
                                    <div class="font-size-h3 font-w600 license" data-toggle="countTo" data-speed="1000" data-to="<?php echo $no_of_students; ?>"></div><span class="license_font">/<?php echo $no_of_license; ?></span>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Students</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-xl-3">
                            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix" data-toggle="tooltip" data-placement="top" title="Active Parents/Total Parents">
                                    <div class="float-left mt-10 d-none d-sm-block">
                                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                                    </div>
                                    <div class="font-size-h3 font-w600 license" data-toggle="countTo" data-speed="1000" data-to="<?php echo $no_of_parents_active; ?>"></span></div><span class="license_font">/<?php echo $no_of_parents; ?></span>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Parents</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-xl-3">
                            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix">
                                    <div class="float-left mt-10 d-none d-sm-block">
                                        <i class="si si-envelope-open fa-3x text-body-bg-dark"></i>
                                    </div>
                                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="1247">0</div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">SMS Credits</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-xl-3">
                            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix">
                                    <div class="float-left mt-10 d-none d-sm-block">
                                        <i class="si si-users fa-3x text-body-bg-dark"></i>
                                    </div>
                                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="4589">0</div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">online</div>
                                </div>
                            </a>
                        </div>
                        <!-- END Row #1 -->
                    </div>
                    <div class="row invisible" data-toggle="appear">
                        <!-- Row #2 -->
                        <div class="col-md-6">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">
                                        Overall <small>attendance</small>
                                    </h3>
                                    <div class="block-options">
<!--                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                            <i class="si si-refresh"></i>
                                        </button>
                                        <button type="button" class="btn-block-option">
                                            <i class="si si-wrench"></i>
                                        </button>-->
                                    </div>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="pull-all">
                                        <!-- Lines Chart Container functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->
                                        <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
<!--                                        <canvas class="js-chartjs-dashboard-lines"></canvas>-->
                                        <canvas id="speedChart"></canvas>
                                    </div>
                                </div>
                                <?php //echo "<pre>";print_r($attendance_count); ?>
                                <div class="block-content">
                                    <div class="row items-push">
                                        <div class="col-6 col-sm-4 text-center text-sm-left">
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">This Month</div>
                                            <div class="font-size-h4 font-w600"><?php echo $attendance_count['present_month_attendance']; ?></div>
<!--                                            <div class="font-w600 text-success">
                                                <?php //if($attendance_count['month_flag'] == 1){ ?>
                                                <i class="fa fa-caret-up"></i> +<?php //echo $attendance_count['avg_attendance']; ?>%
                                                <?php //}else{ ?>
                                                <i class="fa fa-caret-down"></i> -3%
                                                <?php //} ?>
                                            </div>-->
                                        </div>
                                        <div class="col-6 col-sm-4 text-center text-sm-left">
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">This Week</div>
                                            <div class="font-size-h4 font-w600"><?php echo $attendance_count['present_week_attendance']; ?></div>
<!--                                            <div class="font-w600 text-danger">
                                                <i class="fa fa-caret-down"></i> -3%
                                            </div>-->
                                        </div>
                                        <div class="col-12 col-sm-4 text-center text-sm-left">
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">Today</div>
                                            <div class="font-size-h4 font-w600"><?php echo $attendance_count['todays_attendance']; ?></div>
<!--                                            <div class="font-w600 text-success">
                                                <i class="fa fa-caret-up"></i> +9%
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">
                                        Overall <small>class activities</small>
                                    </h3>
                                    <div class="block-options">
<!--                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                            <i class="si si-refresh"></i>
                                        </button>
                                        <button type="button" class="btn-block-option">
                                            <i class="si si-wrench"></i>
                                        </button>-->
                                    </div>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="pull-all">
                                        <!-- Lines Chart Container functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->
                                        <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
<!--                                        <canvas class="js-chartjs-dashboard-lines2"></canvas>-->
                                            <canvas id="speedChartClass"></canvas>
                                    </div>
                                </div>
                                <div class="block-content bg-white">
                                    <div class="row items-push">
                                        <div class="col-6 col-sm-4 text-center text-sm-left">
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">This Month</div>
                                            <div class="font-size-h4 font-w600"><?php echo $activity_count['month_activity_list']; ?></div>
<!--                                            <div class="font-w600 text-success">
                                                <i class="fa fa-caret-up"></i> +4%
                                            </div>-->
                                        </div>
                                        <div class="col-6 col-sm-4 text-center text-sm-left">
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">This Week</div>
                                            <div class="font-size-h4 font-w600"><?php echo $activity_count['week_activity_list']; ?></div>
<!--                                            <div class="font-w600 text-danger">
                                                <i class="fa fa-caret-down"></i> -7%
                                            </div>-->
                                        </div>
                                        <div class="col-12 col-sm-4 text-center text-sm-left">
                                            <div class="font-size-sm font-w600 text-uppercase text-muted">Today</div>
                                            <div class="font-size-h4 font-w600"><?php echo $activity_count['day_activity_list']; ?></div>
<!--                                            <div class="font-w600 text-success">
                                                <i class="fa fa-caret-up"></i> +35%
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

<!--                        <canvas id="myChart1" width="400" height="400"></canvas>-->


                        <script type="text/javascript">



                            var speedCanvas = document.getElementById("speedChart").getContext('2d');

                            Chart.defaults.global.defaultFontFamily = "Lato";
                            Chart.defaults.global.defaultFontSize = 18;

                            var dataFirst = {
                                label: "Last week",
                                data: [<?php echo $previous_week_data; ?>],
                                lineTension: .4,
                                fill: true,
                                borderColor: '#42A5F5',
                                backgroundColor: 'rgba(208, 232, 252, .4)'
                            };

                            var dataSecond = {
                                label: "This week",
                                data: [<?php echo $current_week_data; ?>],
                                lineTension: .4,
                                fill: true,
                                borderColor: '#42A5F5',
                                backgroundColor: 'rgba(99, 180, 245, .6)'
                            };

                            var speedData = {
                                labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
                                datasets: [dataFirst, dataSecond]
                            };

                            var chartOptions = {
                                legend: {
                                    display: false,
                                    position: 'top',
                                    labels: {
                                        boxWidth: 80,
                                        fontColor: 'black',
                                    }
                                },
                                scales: {
                                    yAxes: [{
                                            display: false,
                                            ticks: {
                                                display: false
                                            },
                                            gridLines: {
                                                display: false,
                                            },
                                        }],
                                    xAxes: [{
                                            ticks: {
                                                display: false
                                            },
                                            gridLines: {
                                                display: false,
                                            },
                                        }]
                                }

                            };

                            var lineChart = new Chart(speedCanvas, {
                                type: 'line',
                                data: speedData,
                                options: chartOptions,
                            });

                        </script>
                        
                        <script type="text/javascript">



                            var speedCanvas = document.getElementById("speedChartClass").getContext('2d');

                            Chart.defaults.global.defaultFontFamily = "Lato";
                            Chart.defaults.global.defaultFontSize = 18;

                            var dataFirst = {
                                label: "Last week",
                                data: [<?php echo $prev_week_acivity_data; ?>],
                                lineTension: .4,
                                fill: true,
                                borderColor: '#9CCC65',
                                backgroundColor: 'rgba(190,232,143, .4)'
                            };

                            var dataSecond = {
                                label: "This week",
                                data: [<?php echo $current_week_acivity_data; ?>],
                                lineTension: .4,
                                fill: true,
                                borderColor: '#9CCC65',
                                backgroundColor: 'rgba(210,232,186, .6)'
                            };

                            var speedData = {
                                labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
                                datasets: [dataFirst, dataSecond]
                            };

                            var chartOptions = {
                                legend: {
                                    display: false,
                                    position: 'top',
                                    labels: {
                                        boxWidth: 80,
                                        fontColor: 'black',
                                    }
                                },
                                scales: {
                                    yAxes: [{
                                            display: false,
                                            ticks: {
                                                display: false
                                            },
                                            gridLines: {
                                                display: false,
                                            },
                                        }],
                                    xAxes: [{
                                            ticks: {
                                                display: false
                                            },
                                            gridLines: {
                                                display: false,
                                            },
                                        }]
                                }

                            };

                            var lineChart = new Chart(speedCanvas, {
                                type: 'line',
                                data: speedData,
                                options: chartOptions,
                            });

                        </script>
                        <!-- END Row #2 -->
                        <?php //echo "<pre>";print_r($events);  ?>
                    </div>

                    <div class="row invisible" data-toggle="appear">
                        <?php if (!empty($events)) {
                            foreach ($events as $row) {
                                ?>

                                <!-- Row #3 -->
                                <div class="col-md-4">
                                    <div class="block">
                                        <div class="block-content block-content-full">
                                            <div class="py-20 text-center">
                                                <div class="mb-20">
                                                    <i class="fa fa-envelope-open fa-4x text-primary"></i>
                                                </div>
                                                <div class="font-size-h4 font-w600"><?php echo $row['title']; ?></div>
                                                <div class="text-muted"><?php echo date('d M Y', strtotime($row['start_date'])); ?></div>
                                                <div class="pt-20">
                                                    <!--                                            <a class="btn btn-rounded btn-alt-primary" href="javascript:void(0)">
                                                                                                    <i class="fa fa-cog mr-5"></i> Manage list
                                                                                                </a>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                        
                        <!-- END Row #3 -->
                    </div>
                    <div class="row invisible" data-toggle="appear">
                        <!-- Row #4 -->
<!--                        <div class="col-md-6">
                            <a class="block block-link-shadow overflow-hidden" href="javascript:void(0)">
                                <div class="block-content block-content-full">
                                    <i class="si si-briefcase fa-2x text-body-bg-dark"></i>
                                    <div class="row py-20">
                                        <div class="col-6 text-right border-r">
                                            <div class="invisible" data-toggle="appear" data-class="animated fadeInLeft">
                                                <div class="font-size-h3 font-w600">16</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-muted">Projects</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="invisible" data-toggle="appear" data-class="animated fadeInRight">
                                                <div class="font-size-h3 font-w600">2</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-muted">Active</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>-->
<!--                        <div class="col-md-6">
                            <a class="block block-link-shadow overflow-hidden" href="javascript:void(0)">
                                <div class="block-content block-content-full">
                                    <div class="text-right">
                                        <i class="si si-users fa-2x text-body-bg-dark"></i>
                                    </div>
                                    <div class="row py-20">
                                        <div class="col-6 text-right border-r">
                                            <div class="invisible" data-toggle="appear" data-class="animated fadeInLeft">
                                                <div class="font-size-h3 font-w600 text-info">63250</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-muted">Accounts</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="invisible" data-toggle="appear" data-class="animated fadeInRight">
                                                <div class="font-size-h3 font-w600 text-success">97%</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-muted">Active</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>-->
                        <!-- END Row #4 -->
                    </div>
                    <div class="row invisible" data-toggle="appear">
                        <!-- Row #5 -->
                        <div class="col-6 col-md-4 col-xl-2">
                            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/manageTimeTable">
                                <div class="block-content ribbon ribbon-bookmark ribbon-success ribbon-left">
<!--                                    <div class="ribbon-box">15</div>-->
                                    <p class="mt-5">
                                        <i class="si si-envelope-letter fa-3x"></i>
                                    </p>
                                    <p class="font-w600">Timetable</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/manageNotice">
                                <div class="block-content">
                                    <p class="mt-5">
                                        <i class="si si-user fa-3x"></i>
                                    </p>
                                    <p class="font-w600">Notice</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/addStudent">
                                <div class="block-content ribbon ribbon-bookmark ribbon-primary ribbon-left">
<!--                                    <div class="ribbon-box">3</div>-->
                                    <p class="mt-5">
                                        <i class="si si-bubbles fa-3x"></i>
                                    </p>
                                    <p class="font-w600">Student Add</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <a class="block block-link-shadow text-center" href="#">
                                <div class="block-content">
                                    <p class="mt-5">
                                        <i class="si si-magnifier fa-3x"></i>
                                    </p>
                                    <p class="font-w600">Search</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <a class="block block-link-shadow text-center" href="#">
                                <div class="block-content">
                                    <p class="mt-5">
                                        <i class="si si-bar-chart fa-3x"></i>
                                    </p>
                                    <p class="font-w600">Setting</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 col-md-4 col-xl-2">
                            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/logout">
                                <div class="block-content">
                                    <p class="mt-5">
                                        <i class="si si-settings fa-3x"></i>
                                    </p>
                                    <p class="font-w600">Logout</p>
                                </div>
                            </a>
                        </div>
                        <!-- END Row #5 -->
                    </div>
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
<?php $this->load->view('school/_include/school_footer'); ?>
            <!-- END Footer -->
        </div>
        
    </body>
</html>



    <div class="ListDataContainer">
        <div class="invalid">
<?php if ($this->session->flashdata("e_message")) {
    echo '<p class="e_message">' . $this->session->flashdata("e_message") . '</p>';
} ?>
        </div>
        <div class="sucess">
<?php if ($this->session->flashdata("s_message")) {
    echo '<p class="s_message">' . $this->session->flashdata("s_message") . '</p>';
} ?>
        </div>
         
                                       
    </div>
</div>-->

<?php //$this->load->view("school/_include/school_footer");  ?>
<script type="text/javascript">
//    jQuery(document).ready(function ($) {
//        $("#speedChart").css("width", "400px");
//        $("#speedChart").css("height", "350px");
//    });
</script>    