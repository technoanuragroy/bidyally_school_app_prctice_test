<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SchoolApp|Register</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>


    </head>
    <body>
        <div class="wrapper">
            <div class="wrap_content">
                <!-- Default form register -->
                <form class="text-center p-5 js-validation-material" method="post" action="<?php echo current_url(); ?>">
                    <h1>School App</h1>
                    <div class="form-group">
                        <div class="form-material">
                            <div class="md-form">
                                <input required type="text" id="parent_name" name="parent_name" class="form-control">
                                <label for="parent_name">Name</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <div class="md-form">
                                <input type="email" id="parent_email" name="parent_email" class="form-control">
                                <label for="parent_email">Email address</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <div class="md-form">
                                <input required type="password" id="parent_password" name="parent_password" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock" autocomplete="new-password">
                                <label for="parent_password">Password</label>
                            </div>
                        </div>
                    </div>


                    <!-- Sign up button -->
                    <input type="submit" name="submit" class="btn btn001" value="Submit"/>
                </form>
                <div class="bgBottom"></div>
            </div>
        </div>


    </body>
</html>
