<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <div class="defaultContent">
                            <?php
                            if (!empty($syllabus_detail)) {
                                //echo "<pre>";print_r($syllabus_detail);
                                ?>
                                <h3>Class - <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $syllabus_detail['class_id'] . '"') ?></h3>
                                <p>
                                    <?php echo $syllabus_detail['syllabus_text']; ?>

                                </p>

                            



                                <?php
                                if (!empty($syllabus_file_list)) {
                                    $count = 1;
                                    foreach ($syllabus_file_list as $filess) {
                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                        $path = $filess['file_url'];

                                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                            $display_path = $path;
                                            $download_path = $path;
                                            $width = '72px';
                                        } else if ($ext == 'xls' || $ext == 'xlsx') {
                                            $display_path = base_url() . 'app_images/XLSX.png';
                                            $download_path = $path;
                                            $width = '100px';
                                        } else if ($ext == 'doc' || $ext == 'docx') {
                                            $display_path = base_url() . 'app_images/DOC.png';
                                            $download_path = $path;
                                            $width = '48px';
                                        } else if ($ext == 'pdf' || $ext == 'PDF') {
                                            $display_path = base_url() . 'app_images/PDF.png';
                                            $download_path = $path;
                                            $width = '48px';
                                        }
                                        ?>

                                        <span class="fileDocumenticon">
                                            <a href="<?php echo $download_path; ?>" download=""> 
                                                <img src="<?php echo $display_path; ?>">
                                            </a>
                                            <span>File <?php echo $count; ?></span>
                                        </span>


                                        <?php
                                        $count++;
                                    }
                                }
                                ?>
                                <?php } else { ?>
                                No Syllabus found
                            <?php } ?>

                        </div>
                    </div>
                    <?php $this->load->view('parent/include/footer'); ?>
