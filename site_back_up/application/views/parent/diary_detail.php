<?php $this->load->view('parent/include/header'); ?>
<body>
    <?php //echo "<pre>";print_r($school_notice_detail); ?>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    
                    <div class="rowBox">
                        <h2 class="page_head"><?php echo $page_title; ?></h2>
                        <div class="togglebtnContainer">
<!--                            <ul class="nav nav-tabs diaryTab" id="myTab" role="tablist">
                                <li class="nav-item">
                            <a class="buttontab presentbtn nav-link active" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="view" aria-selected="true">View</a>
                                </li>
                                <li class="nav-item">
                            <a class="buttontab absentbtn nav-link" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">Create</a>
                                </li>
                            </ul>
                            <a href="javascript:" class="buttontab clearbtn">clear</a>-->
                        </div>
                        <div class="noticedetails">
<!--                            <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="view" role="tabpanel" aria-labelledby="view-tab">-->
                            <?php
                            if (!empty($diary_detail)) {//echo "<pre>";print_r($diary_detail);
                                foreach($diary_detail as $diary){
                                    $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER,'name','and id = "'.$diary['teacher_id'].'"');
                                    $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT,'subject_name','and id = "'.$diary['subject_id'].'"');
                                    
                                ?>
                                <h2><?php echo $diary['heading']; ?></h2>
                                <span class="date__notice"><?php echo date('M d Y', ($diary['issue_date'])); ?></span>
                                <p>
                                    <?php echo $diary['diary_note']; ?>
                                </p>
                                
                                <div class="teacher_detail_left">
                                    By - <?php echo $teacher_name; ?><br>
                                    Subject - <?php echo $subject_name; ?>
                                </div>
                                <div class="teacher_detail_right">
                                    <a href="<?php echo base_url(); ?>parent/user/studentDiary/<?php echo $diary['id']; ?>" class="btn002"><i class="fas fa-arrow-left"></i> Reply</a>
                                </div>
                                    
                                <?php
                            }}
                            ?>


                        </div>
                        
                        <?php $diary_id = $this->uri->segment(4); 
                        $diar_row_detail = $this->my_custom_functions->get_details_from_id($diary_id,TBL_DIARY);
                        if(!empty($diar_row_detail)){
                          $school_id = $diar_row_detail['school_id'];
                          $student_id = $diar_row_detail['student_id'];
                          $teachher_id = $diar_row_detail['teacher_id'];
                          $parent_id = $diar_row_detail['parent_id'];
                        $diary_conv = $this->my_custom_functions->get_multiple_data(TBL_DIARY,'and school_id = "'.$school_id.'" and student_id = "'.$student_id.'" and teacher_id = "'.$teachher_id.'" and parent_id = "'.$parent_id.'" order by id desc limit 0,5');
                        if(!empty($diary_conv)){
                        ?>
                        <div class="accordion" id="accordionExample">
                            <?php 
                            $show = '';
                            $count = 1;
                            foreach($diary_conv as $diary_convertation){ 
                                if($count == 1){
                                    $show = 'show';
                                }else{
                                    $show = '';
                                }
                                ?>
                            <div class="card">
                                <div class="card-header" id="heading<?php echo $diary_convertation['id']; ?>">
                                    <h2 class="mb-0">
                                        <button class="btn" type="button" data-toggle="collapse" data-target="#collapse<?php echo $diary_convertation['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $diary_convertation['id']; ?>">
                                            <?php echo $diary_convertation['heading']; ?>
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse<?php echo $diary_convertation['id']; ?>" class="collapse <?php echo $show; ?>" aria-labelledby="heading<?php echo $diary_convertation['id']; ?>" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <?php echo $diary_convertation['diary_note']; ?>
                                    </div>
                                </div>
                            </div>
                            
                       <?php $count++;} ?>
                        </div>
                        <?php }} ?>
                        
                        
                        
                        
                        </div>
                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
