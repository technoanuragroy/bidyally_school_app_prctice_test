<nav id="sidebar">
    <div class="scrollContainer">
        <div id="dismiss">
            <!-- <i class="fas fa-arrow-left"></i> -->
            <i class="fas fa-times"></i>
        </div>
        <div class="sidebar-header">
            <span class="profileimg">
                <?php
                $school_name = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"');
                $address = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'address', 'and id = "' . $this->session->userdata('school_id') . '"');

                $check_school_logo = $this->my_custom_functions->get_perticular_count(TBL_SCHOOL_LOGO_FILES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
                if ($check_school_logo > 0) {
                    $school_url = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_LOGO_FILES, 'file_url', 'and school_id = "' . $this->session->userdata('school_id') . '"');
                    ?>
        <img src="<?php echo $school_url;?>" alt="" class="">
                    <?php } else {  ?>
                    <img src="<?php echo base_url(); ?>_images/School-img.png" alt="" class="">
                    <?php }  ?>
                </span>
                <h3>
                    <?php echo $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $this->session->userdata('teacher_id') . '"'); ?>
                </h3>
                <span class="tag_location"><?php echo $school_name; ?><br><?php echo $address; ?></span>
            </div>
            <ul class="list-unstyled components">
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/dashBoard"><i class="fas fa-home"></i>Dashboard</a>
                </li>
                <?php if ($this->session->userdata('student_flag') && $this->session->userdata('student_flag') == 1) { ?>
                    <li class="">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-external-link-alt"></i>Dashboard Links</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="<?php echo base_url(); ?>parent/user/dailyWorks"><i class="fas fa-book-reader"></i>Daily Work</a>
                            </li>
                            <!--                <li>
                                                <a href="#secondlabelhomeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home 2</a>
                                                <ul class="collapse list-unstyled secondlabel" id="secondlabelhomeSubmenu">
                                                    <li>
                                                        <a href="#">Home 11</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Home 12</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Home 13</a>
                                                    </li>
                                                </ul>
                                            </li>-->
                            <li>
                                <a href="<?php echo base_url(); ?>parent/user/studentDiary"><i class="fas fa-book-open"></i>Diary</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>parent/user/attendance"><i class="fas fa-user-check"></i>Attendance</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>parent/user/notice"><i class="fas fa-bell"></i>Notice</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>parent/user/timeTable"><i class="fas fa-calendar-week"></i>Time Table</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>parent/user/syllabus"><i class="fas fa-book-reader"></i>Syllabus</a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a href="<?php echo base_url(); ?>parent/user/schoolCalander"><i class="far fa-calendar-alt"></i>School Calendar</a>
                    </li>

                    <li>
                        <a href="<?php echo base_url(); ?>parent/user/generalInformation"><i class="fas fa-info-circle"></i>School Info</a>
                    </li>
                    <!--        <li>
                                <a href="<?php echo base_url(); ?>parent/user/updateProfile"><i class="far fa-address-card"></i>Update Profile</a>
                            </li>-->

                    <!--                    <li>
                                            <a href="<?php echo base_url(); ?>teacher/user/dailyWorks"><i class="far fa-image"></i>Listing Details</a>
                                        </li>-->
                    <!--                    <li>
                                            <a href="<?php echo base_url(); ?>teacher/user/attendance"><i class="far fa-image"></i>Attendance</a>
                                        </li>-->
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="far fa-sticky-note"></i>Pages</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="#">Page 1</a>
                            </li>
                            <li>
                                <a href="#">Page 2</a>
                            </li>
                            <li>
                                <a href="#">Page 3</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>parent/user/feeStructure"><i class="fas fa-money-check"></i>Fees Structure</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>parent/user/changePassword"><i class="fas fa-unlock-alt"></i>Change Password</a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
            </li>
        </ul>

    </div>
</nav>
