<nav id="sidebar">
  <div class="scrollContainer">
    <div id="dismiss">
        <!-- <i class="fas fa-arrow-left"></i> -->
        <i class="fas fa-times"></i>
    </div>
    <div class="sidebar-header">
        <span class="profileimg">
            <?php
            $pic_path = 'uploads/teacher/' . $this->session->userdata('teacher_id') . '.jpg';
            $disp_pic_path = base_url() . 'uploads/teacher/' . $this->session->userdata('teacher_id') . '.jpg';

            if (file_exists($pic_path)) {
                ?>
                <img src="<?php echo $disp_pic_path.'?'.time(); ?>" alt="" class="">
            <?php } else { ?>
                <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
            <?php } ?>
        </span>
        <h3>
        <?php echo $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $this->session->userdata('teacher_id') . '"'); ?>
        </h3>
        <span class="tag_location">Seattle, USA</span>
    </div>
    <ul class="list-unstyled components">
        <li>
            <a href="<?php echo base_url(); ?>parent/user/dashBoard"><i class="fas fa-home"></i>Dashboard</a>
        </li>
        <?php if($this->session->userdata('student_id') && $this->session->userdata('student_id') != ''){ ?>
        <li class="">
            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-home"></i>Dashboard Links</a>
            <ul class="collapse list-unstyled" id="homeSubmenu">
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/dailyWorks">Daily Work</a>
                </li>
<!--                <li>
                    <a href="#secondlabelhomeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home 2</a>
                    <ul class="collapse list-unstyled secondlabel" id="secondlabelhomeSubmenu">
                        <li>
                            <a href="#">Home 11</a>
                        </li>
                        <li>
                            <a href="#">Home 12</a>
                        </li>
                        <li>
                            <a href="#">Home 13</a>
                        </li>
                    </ul>
                </li>-->
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/studentDiary">Diary</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/attendance">Attendance</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/notice">Notice</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/timeTable">Time Table</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/syllabus">Syllabus</a>
                </li>

            </ul>
        </li>

        <li>
            <a href="<?php echo base_url(); ?>parent/user/schoolCalander"><i class="far fa-image"></i>School Calender</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>parent/user/notes"><i class="far fa-image"></i>Notes</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>parent/user/generalInformation"><i class="far fa-image"></i>School Info</a>
        </li>

        <!--                    <li>
                                <a href="<?php echo base_url(); ?>teacher/user/dailyWorks"><i class="far fa-image"></i>Listing Details</a>
                            </li>-->
        <!--                    <li>
                                <a href="<?php echo base_url(); ?>teacher/user/attendance"><i class="far fa-image"></i>Attendance</a>
                            </li>-->
        <li>
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="far fa-image"></i>Pages</a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
                <li>
                    <a href="#">Page 1</a>
                </li>
                <li>
                    <a href="#">Page 2</a>
                </li>
                <li>
                    <a href="#">Page 3</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>parent/user/feeStructure"><i class="far fa-image"></i>Fees Structure</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>parent/user/changePassword"><i class="fas fa-sign-out-alt"></i>Change Password</a>
        </li>
        <?php } ?>
        <li>
            <a href="<?php echo base_url(); ?>parent/user/logout"><i class="fas fa-sign-out-alt"></i>logout</a>
        </li>
    </ul>

</div>
</nav>
