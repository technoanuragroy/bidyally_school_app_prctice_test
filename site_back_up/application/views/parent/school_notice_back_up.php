<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <!-- <i class="fas fa-align-left"></i> -->
                            <i class="fas fa-bars"></i>
                            <!-- <span>Menu</span> -->
                        </button>
                        <div class="headingDiv"><h2>Notice</h2></div>
                        <span class="iconsDiv"><i class="fas fa-arrow-left"></i>
                    </div>
                </nav>
                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="noticeGrid">
                            <?php
                            //echo "<pre>";print_r($school_notice); 
                            foreach ($school_notice as $notice_date => $notice_data) {//echo "<pre>";print_r($notice_data); 
                                ?>
                                <div class="grid_row">
                                    <div class="grid_row_left">
                                        <h2><?php
                                            $break_start_date = explode('-', $notice_date);
                                            $year = $break_start_date[0];
                                            $monthe = $break_start_date[1];
                                            $day = $break_start_date[2];

                                            $monthNum = $monthe;
                                            $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                            $monthName = $dateObj->format('M'); // March

                                            echo $day;
                                            ?></h2>
                                        <span class="dayText"><?php echo $monthName; ?></span>
                                    </div>

                                    <div class="grid_row_right">
                                        <?php
                                        foreach ($notice_data['notice_data'] as $data) {
                                            ?>
                                            <div class="grid_row_sub"><?php echo $data['notice_heading']; ?></div>
                                            <p>
                                                <?php echo $data['notice_text']; ?>
                                            </p>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if(array_key_exists('notice_file_data', $notice_data)){
                                        foreach ($notice_data['notice_file_data'] as $file_data) {
                                            
                                            ?>
                                            <span class="files_icon"><a href="<?php echo base_url(); ?>parent/user/downloadFile/<?php echo $file_data['id']; ?>" ><i class="fas fa-file"></i></a></span>
                                        <?php }} ?>
                                        </div>
                                    </div>




    <?php } ?>
                            </div>
                        </div>
    <?php $this->load->view('teacher/include/footer'); ?>
