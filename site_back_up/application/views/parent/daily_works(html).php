<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
           <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <!-- <i class="fas fa-align-left"></i> -->
                            <i class="fas fa-bars"></i>
                            <!-- <span>Menu</span> -->
                        </button>
                        <div class="headingDiv"><h2>Attendance List</h2></div>
                        <a href="javascript:" onclick="history.back();"> <span class="iconsDiv"><i class="fas fa-arrow-left"></i></span></a>
                    </div>
                </nav>
                <div class="bodycontent">
                   <div class="rowBox">
                        <div class="">
                              <ul class="worksDaliy">
                                <li>
                                  <div class="row_works">
                                  <span class="leftworks">
                                    <span class="subject_1"><h3>environmental science</h3></span>
                                    <span class="subject_2"><h3>english</h3></span>
                                  </span>
                                  <span class="rightworks" style="background-image: url(<?php echo base_url();?>app_images/books_img.jpg)">

                                  </span>
                                </div>
                                </li>

                                <li>
                                  <div class="row_works">
                                  <span class="rightworks" style="background-image: url(<?php echo base_url();?>app_images/background-school.jpg)">
                                    <!-- <img src="<?php echo base_url();?>app_images/background-school.jpg" alt=""/> -->
                                  </span>
                                  <span class="leftworks">
                                    <span class="subject_1"><h3>environmental science</h3></span>
                                    <span class="subject_2"><h3>english</h3></span>
                                  </span>
                                  </div>
                                </li>

                                <li>
                                  <div class="row_works">
                                  <span class="leftworks">
                                    <span class="subject_1"><h3>environmental science</h3></span>
                                    <span class="subject_2"><h3>english</h3></span>
                                  </span>
                                  <span class="rightworks" style="background-image: url(<?php echo base_url();?>app_images/chemical-laboratory.jpg)">

                                  </span>
                                  </div>
                                </li>
                              </ul>
                        </div>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
