<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <div class="defaultContent">
                            <?php
                            if (!empty($general_info)) { ?>
                                
                                <p>
                                    <?php echo $general_info[0]['info_text']; ?>

                                </p>

                            <?php } ?>

                          
                        </div>
                    </div>
                    <?php $this->load->view('parent/include/footer'); ?>
