<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
           <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                    <div class="rowBox">
                        
                        <?php if(!empty($child_list)){ 
                            foreach($child_list as $child){
                                $file_path = 'uploads/student/'.$child['id'].'.jpg';
                                
                            ?>
                        <div class="profileGrid">
                            <a href="todays_classes.php"></a>
                            <span class="profileimg">
                                <?php if(file_exists($file_path)){ ?>
                                <img src="<?php echo base_url().$file_path.'?'.time() ?>" alt="" class="">
                                <?php }else{ ?>
                                <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                <?php } ?>
                            </span>
                            <h3><?php echo $child['name']; ?></h3>
<!--                            <span class="tag_section">Art director</span>-->
                            <a href="<?php echo base_url(); ?>parent/user/switch_student/<?php echo $child['id']; ?>" class="btn_select">Select</a>
                        </div>
                        <?php }} ?>
                        

                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
