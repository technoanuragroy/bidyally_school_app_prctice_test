<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
             <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="formContent">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                                <?php } ?>
                                <?php if ($this->session->flashdata("e_message")) { ?>
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                <?php } ?>
                            </div>

                            <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                            <div class="rowBox">
                                <div class="formContent">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputFirstname">Old Password</label>

                                            <input required="" type="password" name="oldpass" class="form-control">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="inputLastname">New Password</label>

                                            <input required="" type="password" name="newpass" class="form-control" id="val-password2">
                                        </div>

                                        <div class="form-group buttonContainer">
                                            <input type="submit" name="submit" class="btn002" value="Save">
                                        </div>

                                        <?php echo form_close(); ?>
                                        <!--                            <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php $this->load->view('teacher/include/footer'); ?>
