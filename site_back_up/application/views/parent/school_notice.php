<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <?php
                        //echo "<pre>";print_r($this->session->all_userdata());
                                    
                        if (!empty($school_notice)) {
                            //echo "<pre>";print_r($this->session->all_userdata());
                            foreach ($school_notice as $notice) {
                                if ($notice['type'] == 2) {
                                    $break_start_date = explode('-', $notice['issue_date']);
                                    $year = $break_start_date[0];
                                    $monthe = $break_start_date[1];
                                    $day = $break_start_date[2];

                                    $monthNum = $monthe;
                                    $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                    $monthName = $dateObj->format('M'); // March
                                    $check_attachment_exist = $this->my_custom_functions->get_perticular_count(TBL_NOTICE_FILES,'and notice_id = "'.$notice['id'].'"');
                                    ?>
                                    <a href="<?php echo base_url(); ?>parent/user/noticeDetail/<?php echo $notice['id']; ?>">
                                        <div class="noticeGrid">
                                            <div class="grid_row">
                                                <span class="pin_icon"><img src="<?php echo base_url();?>app_images/pin_icon.png"></span>
                                                <?php if($check_attachment_exist > 0){ ?>
                                                <span class="ribbon_icon"><img src="<?php echo base_url();?>app_images/ribbon_icon.png"></span>
                                                <?php } ?>
                                                <div class="grid_row_left">
                                                    <h2><?php echo $day; ?></h2>
                                                    <span class="dayText"><?php echo $monthName; ?></span>
                                                </div>
                                                <div class="grid_row_right">
                                                    <h2 class="grid_row_sub"><?php echo $notice['notice_heading']; ?></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <?php
                                } else {
                                    //echo "<pre>";print_r($this->session->all_userdata());
                                   /// echo $notice['class_id'];
                                    if ($this->session->userdata('class_id') == $notice['class_id']) {

                                        $break_start_date = explode('-', $notice['issue_date']);
                                        $year = $break_start_date[0];
                                        $monthe = $break_start_date[1];
                                        $day = $break_start_date[2];

                                        $monthNum = $monthe;
                                        $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                        $monthName = $dateObj->format('M'); // March
                                        ?>
                                        <a href="<?php echo base_url(); ?>parent/user/noticeDetail/<?php echo $notice['id']; ?>">
                                            <div class="noticeGrid">
                                                <div class="grid_row">
                                                    <div class="grid_row_left">
                                                        <h2><?php echo $day; ?></h2>
                                                        <span class="dayText"><?php echo $monthName; ?></span>
                                                    </div>
                                                    <div class="grid_row_right">
                                                        <h2 class="grid_row_sub"><?php echo $notice['notice_heading']; ?></h2>
                <!--                                            <p>
                                                        <?php //echo substr($notice['notice_text'],0,70); ?>...

                                                        </p>-->
                                                        <!-- <span class="files_icon"><a href=""><i class="fas fa-file"></i></a></span> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </a>  
                                    <?php
                                    }
                                }
                            }
                        }
                        ?>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
