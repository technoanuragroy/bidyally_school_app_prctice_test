<?php $this->load->view('parent/include/header'); ?>
<style>
    /*    .content_box {
            font-size: 10px;
            margin: 5px 0px;
            min-height: 40px;
    
        }
        .calender {
            background: #ffffff;
            border: 1px solid #CCCCCC;
            width: 100%;
            -moz-box-shadow: 0px 0px 10px #CCCCCC;
            -webkit-box-shadow: 0px 0px 10px #CCCCCC;
            -o-box-shadow: 0px 0px 10px #CCCCCC;
            box-shadow: 0px 0px 10px #CCCCCC;
        }
        .calender td {
        border-bottom: 1px solid #CCCCCC;
        border-right: 1px solid #CCCCCC;
        vertical-align: top;
        padding: 2px;
        width: 100px;
        min-height: 50px;
    }
    .calender th {
        font-size: 15px;
        text-decoration: none;
        background-color: #F5F5F5;
        color: #3A3939;
        line-height: 37px;
    }*/
</style>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <ul class="legends">
                            <li><span class="greenLegend"></span>Present </li>
                            <li><span class="redLegend"></span>Absent </li>
                            <li><span class="orangeLegend"></span>Partial Present </li>
                        </ul>
                        <div class="profileGrid wrapperCalender">
                            <div id="calendar_box">
                                <?php echo $this->calendar->generate($year, $month, $event); ?>
                            </div>
                        </div>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $("div.highlight").eq(0).removeClass("highlight").addClass("date");
                            $("div.highlight").eq(1).removeClass("highlight").addClass("content_box");
                        });                        
                    </script>