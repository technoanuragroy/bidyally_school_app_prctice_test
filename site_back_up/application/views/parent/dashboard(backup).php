<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
           <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <!-- <i class="fas fa-align-left"></i> -->
                            <i class="fas fa-bars"></i>
                            <!-- <span>Menu</span> -->
                        </button>
                        <div class="headingDiv"><h2>Dashboard</h2></div>
                        <a href="javascript:" onclick="history.back();"> <span class="iconsDiv"><i class="fas fa-arrow-left"></i></span></a>
                    </div>
                </nav>
                <div class="bodycontent">
                    
                    
                    
                    
                   <div class="rowBox">
                        <div class="profileGrid">
                            <?php $pic_path = 'uploads/student/'.$student_detail['id'].'.jpg';
                            $disp_pic_path = base_url().'uploads/student/'.$student_detail['id'].'.jpg';
                            $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_detail['class_id'] . '"');
                            $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_detail['class_id'] . '"');
                            ?>
                            
                            <a href="todays_classes.php">
                            <span class="profileimg">
                                <?php if(file_exists($pic_path)){ ?>
                                <img src="<?php echo $disp_pic_path ?>" alt="" class="">
                                <?php } else { ?>
                                <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                <?php } ?>
                            </span>
                            </a>
                            
                            <h3><?php echo $student_detail['name']; ?></h3>
                            <span class="tag_section">Class - <?php echo $class_name; ?>(<?php echo $section_name; ?>) </span>
                            <span class="tag_section">Roll No: <?php echo $student_detail['roll_no']; ?></span>
                            <a href="#" class="btn_select">Select</a>
                        </div>
                       

                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
