<?php $this->load->view('teacher/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="formContent">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                                <?php } ?>
                                <?php if ($this->session->flashdata("e_message")) { ?>
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                <?php } ?>
                            </div>
                            <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                            <div class="rowBox">
                                <div class="formContent">

                                    <form action="<?php echo current_url(); ?>" method="post" enctype='multipart/form-data'>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputFirstname">Name</label>
                                                <input type="text" class="form-control" name="name" id="inputFirstname" placeholder="First name" value="<?php echo $profile_detail['name']; ?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Email</label>
                                                <input type="email" class="form-control" name="email" id="inputEmail4" placeholder="Email" value="<?php echo $profile_detail['email']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                                <label for="inputPassword4">Phone No.</label>
                                                <input type="text" class="form-control" id="inputPassword4" placeholder="Phone" name="phone" value="<?php echo $profile_detail['phone_no']; ?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                
                                                
                                                <label for="inputPassword4">Profile Photo</label>
                                                
                                                <?php
                                                $image_file = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER_FILES,'file_url','and teacher_id = "'.$profile_detail['id'].'"');
                                                if($image_file != ''){?>
                                                <br>
                                                <img src="<?php echo $image_file; ?>" style="width: 100px;" class="picbrowse"/>
                                                <span class="removeImgg"><input type="checkbox" name="del_img" value="1">Remove Image</span>
                                                <?php }else{ 
                                                    $imgfile = base_url() . '_images/avatar15.jpg';
                                                    ?>
                                                <img src="<?php echo $image_file; ?>"  />
                                                <?php } ?>
                                                
                                                <div class="container_browsephoto"><input type="file" class="form-control browse_input" name="adminphoto" id="inputPassword4"></div>
                                            </div>
                                        </div>





                                        <div class="form-group buttonContainer">
                                            <input type="submit" class="btn002 " name="submit" value="Save"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
