<?php //echo "<pre>";print_r($running_class);    ?>
<?php $this->load->view('teacher/include/header'); ?>
<script type="text/javascript">
    $('#checkboxall').on('click', function () {

        if (this.checked) {
            $('.checkboxTeacher').each(function () {
                this.checked = true;
            });
        } else {
            $('.checkboxTeacher').each(function () {
                this.checked = false;
            });
        }
    });
</script>
<body>
    <div class="wrapper">
        <div class="wrap_content ">
            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="innerbodycontent">
                    <div class="sectionrowBox">
                        <div class="innerbodycontent3">
                            <?php //echo form_open_multipart('teacher/user/showStudentDetail', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                           
                            
                            <ul class="studentlist">
                                
                                <?php
                                if (!empty($students)) {
                                    foreach($students as $student){
                                        ?>
                                        <li>
                                            <a href="javascript:">
                                                <span class="profileimg">
                                                    <?php
                                                    $path = 'uploads/student/' . $student['id'] . '.jpg';
                                                    $display_path = base_url() . 'uploads/student/' . $student['id'] . '.jpg?'.time();
                                                    if (file_exists($path)) {
                                                        ?>
                                                        <img src="<?php echo $display_path; ?>"> 
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>_images/NoImageFound.png"> 
                                                    <?php } ?>
                                                </span>
                                                <?php
                                                $student_detail = $this->my_custom_functions->get_details_from_id($student['id'], TBL_STUDENT);
                                                ?>
                                                
                                                <h3><?php echo $student_detail['name']; ?></h3>
                                                <span class="tag_section"><?php
                                                    $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_detail['class_id'] . '"');
                                                    $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_detail['section_id'] . '"');
                                                    echo 'Class - ' . $class_name . '' . $section_name;
                                                    ?></span>
                                                <span class="tag_section"><?php echo 'Roll No - ' . $student_detail['roll_no']; ?></span>
                                                <span class="tag_section"><?php echo 'Date of birth - ' . date('d/m/Y',strtotime($student_detail['dob'])); ?></span>
                                                <span class="tag_section"><?php echo 'Father Name - ' . $student_detail['father_name']; ?></span>
                                                <span class="tag_section"><?php echo 'Mother Name - ' . $student_detail['mother_name']; ?></span>
                                                <span class="custom-control custom-checkbox">
                                                </span>
                                            </a>
                                        </li>
                                        <?php
                                    
                                }}
                                ?>

                            </ul>

                            
                            
                            
                            
                            
                            
                        </div>
                        
                        
                        
                        
                        
                        
                        

                        <div class="col-lg-12 buttonContainer" style="float:left;">
                            
                            <a href="<?php echo base_url(); ?>teacher/user/writeDiary/<?php echo $student['id']; ?>" class="btn002">Write Diary</a>


                            
                            <!--                            <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->
                        </div>
                        <?php //echo form_close(); ?>
                        <?php $this->load->view('teacher/include/footer'); ?>
