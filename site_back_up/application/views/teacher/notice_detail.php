<?php $this->load->view('teacher/include/header'); ?>
<body>
    <?php //echo "<pre>";print_r($school_notice_detail); ?>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="noticedetails">
                            
                            <?php
                            if (!empty($school_notice_detail)) {
                                ?>
                                <h2><?php echo $school_notice_detail['notice_data']['notice_heading']; ?></h2>
                                <span class="date__notice"><?php echo @date('M d Y', strtotime($school_notice_detail['notice_data']['issue_date'])); ?></span>
                                
                                <div style="border:1px solid">
                                   
                                    <?php
                            if (!empty($school_notice_detail['notice_file_data'])) {
                                foreach ($school_notice_detail['notice_file_data'] as $notice_file) {
                                    ?>
                                    <div class="downloadFiles">
                                        <span class="files_icon"><a href="<?php echo base_url(); ?>parent/user/downloadFile/<?php echo $notice_file['id']; ?>/<?php echo DOWNLOAD_KEY; ?>"><i class="fas fa-file"></i></a> <i class="fa fa-download" aria-hidden="true"></i></span>
                                        <span></span>
                                    </div>
                                        <?php }
                                    }
                                    ?>
                                </div>
                                
                                <p>
                                    <?php echo $school_notice_detail['notice_data']['notice_text']; ?>
                                </p>
                                <?php
                            }
                            ?>
                            
                        </div>
                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
