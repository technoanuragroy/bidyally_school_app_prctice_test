<?php $this->load->view('teacher/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">
                    <div class="rowBox">
                        <?php
                        if (!empty($school_notice)) {
                            foreach ($school_notice as $notice) {
                                $break_start_date = explode('-', $notice['issue_date']);
                                $year = $break_start_date[0];
                                $monthe = $break_start_date[1];
                                $day = $break_start_date[2];

                                $monthNum = $monthe;
                                $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                $monthName = $dateObj->format('M'); // March
                                $check_attachment_exist = $this->my_custom_functions->get_perticular_count(TBL_NOTICE_FILES,'and notice_id = "'.$notice['id'].'"');
                                
                                ?>
                        <a href="<?php echo base_url(); ?>teacher/user/noticeDetail/<?php echo $notice['id']; ?>">
                                <div class="noticeGrid">
                                    <div class="grid_row">
                                      <span class="pin_icon"><img src="<?php echo base_url();?>app_images/pin_icon.png"/></span>
                                      <?php if($check_attachment_exist > 0){ ?>
                                      <span class="ribbon_icon"><img src="<?php echo base_url();?>app_images/ribbon_icon.png"/></span>
                                      <?php } ?>
                                        <div class="grid_row_left">
                                            <h2><?php echo $day; ?></h2>
                                            <span class="dayText"><?php echo $monthName; ?></span>
                                        </div>
                                        <div class="grid_row_right">
                                            <h2 class="grid_row_sub"><?php echo $notice['notice_heading']; ?></h2>
<!--                                            <p>
                                                <?php //echo substr($notice['notice_text'],0,70); ?>...

                                            </p>-->
                                            <!-- <span class="files_icon"><a href=""><i class="fas fa-file"></i></a></span> -->
                                        </div>
                                    </div>
                                </div>
                        </a>
                            <?php
                            }
                        }
                        ?>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
