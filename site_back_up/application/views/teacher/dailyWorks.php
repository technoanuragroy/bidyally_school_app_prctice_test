<?php $this->load->view('teacher/include/header'); ?>
  <body>
    <div class="wrapper">
      <div class="wrap_content ">
        <!-- Sidebar  -->
        <?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">
        <!-- <div class="innerHeadingContent">
            <h1>Today's class</h1>
        </div> -->

         <?php $this->load->view('teacher/include/header_nav'); ?>

        <div class="innerbodycontent">

          <div class="sectionrowBox">
              <?php 
               if($this->session->userdata('query_date') && $this->session->userdata('query_date') != ''){
                            $query_flag = 1;
                        }else{
                            $query_flag = '';
                        }
              $check_attendance_class = $this->my_custom_functions->get_particular_field_value(TBL_TIMETABLE,'attendance_class','and id="'.$this->uri->segment(6).'"');
              if($check_attendance_class == 1){
              ?>
              <div class="section_Grid2">
                <a href="<?php echo base_url(); ?>teacher/user/attendance/<?php echo $this->uri->segment(4); ?>/<?php echo $this->uri->segment(5); ?>/<?php echo $this->uri->segment(6); ?>/<?php echo $query_flag; ?>" class="btn003">
                <span class="icon_section">
                  <img src="<?php echo base_url(); ?>app_images/attendence_icon.png" alt="">
                </span>
                   attendance
                 </a>
              </div>
              <?php } ?>
              <?php 
              $today_start_date = strtotime(date('Y-m-d').'00:00:00');
              $today_end_date = strtotime(date('Y-m-d').'23:59:59');
              $check_existance_note = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "'.$today_start_date.'" and note_issuetime <= "'.$today_end_date.'" and publish_data = 0'); ?>
              <div class="section_Grid2">
                <a href="<?php echo base_url(); ?>teacher/user/classWorks/<?php echo $this->uri->segment(4); ?>/<?php echo $this->uri->segment(5); ?>/<?php echo $this->uri->segment(6); ?>/<?php echo $query_flag; ?>" class="btn003">
                  <span class="icon_section">
                  <img src="<?php echo base_url(); ?>app_images/Class-work.png" alt="">
                </span>
                  Class work
                  <?php 
                  
                  if($check_existance_note >0){ ?>
                  <span class="draft"><i class="far fa-edit"></i></i></span>
                  <?php } ?>
                </a>
              </div>
              <div class="section_Grid2">
                  <a href="<?php echo base_url(); ?>teacher/user/homeWorks/<?php echo $this->uri->segment(4); ?>/<?php echo $this->uri->segment(5); ?>/<?php echo $this->uri->segment(6); ?>/<?php echo $query_flag; ?>" class="btn003">
                    <span class="icon_section">
                    <img src="<?php echo base_url(); ?>app_images/homework.png" alt="">
                  </span>
                    home work
                    <?php 
                   $check_existance_home = $this->my_custom_functions->get_perticular_count(TBL_HOME_NOTE, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "'.$today_start_date.'" and note_issuetime <= "'.$today_end_date.'" and publish_data = 0'); 
                  if($check_existance_home >0){ ?>
                  <span class="draft"><i class="far fa-edit"></i></i></span>
                  <?php } ?>
                  </a>
              </div>
              <div class="section_Grid2">
                  <a href="<?php echo base_url(); ?>teacher/user/assignment/<?php echo $this->uri->segment(4); ?>/<?php echo $this->uri->segment(5); ?>/<?php echo $this->uri->segment(6); ?>/<?php echo $query_flag; ?>" class="btn003">
                    <span class="icon_section">
                    <img src="<?php echo base_url(); ?>app_images/assignment.png" alt="">
                  </span>
                    assignment
                    <?php 
                   $check_existance_assignment = $this->my_custom_functions->get_perticular_count(TBL_ASSIGNMENT_NOTE, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "'.$today_start_date.'" and note_issuetime <= "'.$today_end_date.'" and publish_data = 0'); 
                  if($check_existance_assignment >0){ ?>
                  <span class="draft"><i class="far fa-edit"></i></i></span>
                  <?php } ?>
                  </a>
              </div>

<!--              <div class="col-lg-12 buttonContainer">  <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->
        </div>
         <?php $this->load->view('teacher/include/footer'); ?>
