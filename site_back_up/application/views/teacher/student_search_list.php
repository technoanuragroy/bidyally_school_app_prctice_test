<?php //echo "<pre>";print_r($running_class);                ?>
<?php $this->load->view('teacher/include/header'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#ckbCheckAll").click(function () {
            $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        });

        $(".checkBoxClass").change(function () {
            if (!$(this).prop("checked")) {
                $("#ckbCheckAll").prop("checked", false);
            }
        });
        $("#f_click").click(function () {
            $(".forceClick").trigger('click');
        });
    });
</script>
<body>
    <div class="wrapper">
        <div class="wrap_content ">
            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="innerbodycontent">
                    <div class="sectionrowBox">
                        <?php if ($this->uri->segment(4) == 'src' || $this->uri->segment(5) == 'src') { ?>

                            <div class="innerbodycontent3">



                                <ul class="studentlist">

                                    <?php
                                    if (!empty($student_list)) {
                                        foreach ($student_list as $student) {
                                            ?>
                                            <li>
                                                <a href="<?php echo base_url(); ?>teacher/user/showStudentIndividualDetails/<?php echo $student['id']; ?>">
                                                    <span class="profileimg">
                                                        <?php
                                                        $path = 'uploads/student/' . $student['id'] . '.jpg';
                                                        $display_path = base_url() . 'uploads/student/' . $student['id'] . '.jpg?' . time();
                                                        $paths = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_FILES, 'file_url', 'and student_id = "' . $student['id'] . '"');
                                                        //echo $paths;die;
                                                        if ($paths != '') {
                                                            ?>
                                                            <img src="<?php echo $paths; ?>" alt="" class="">
                                                        <?php } else { ?>
                                                            <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                                        <?php } ?>
                                                    </span>
                                                    <?php
                                                    $student_detail = $this->my_custom_functions->get_details_from_id($student['id'], TBL_STUDENT);
                                                    ?>
                                                    <h3><?php echo $student_detail['name']; ?></h3>
                                                    <span class="tag_section"><?php
                                                        $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_detail['class_id'] . '"');
                                                        $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_detail['section_id'] . '"');
                                                        echo $class_name . '' . $section_name . ', ' . $student_detail['roll_no'];
                                                        ?></span>
            <!--                                                    <span class="tag_section"><?php echo 'Roll No - ' . $student_detail['roll_no']; ?></span>-->

                                                </a>
                                            </li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <li>No record found</li>
                                    <?php } ?>
                                </ul>

                            </div>







                        <?php } else {
                            ?>
                            <div class="innerbodycontent3">
                                <?php echo form_open_multipart('teacher/user/showStudentDetail', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                <?php if (!empty($student_list)) { ?>
                                    <div class="fakeGo"> <a href="javascript:" class="btn002" id="f_click">Go</a></div>
                                <?php } ?>
                                <ul class="studentlist no_record">


                                    <?php if (!empty($student_list)) { ?>
                                        <li>
                                            <div class="custom-control custom-checkbox selectall">
                                                <input type="checkbox" class="custom-control-input" id="ckbCheckAll" required>
                                                <label class="custom-control-label" for="ckbCheckAll">Select All</label>

                                            </div>
                                        </li>
                                        <?php foreach ($student_list as $student) {
                                            ?>
                                            <li>
                                                <a href="javascript:">
                                                    <span class="custom-control custom-checkbox">
                                                        <span class="profileimg">
                                                            <?php
//                                                            $path = 'uploads/student/' . $student['id'] . '.jpg';
//                                                            $display_path = base_url() . 'uploads/student/' . $student['id'] . '.jpg?' . time();
                                                            $paths = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_FILES, 'file_url', 'and student_id = "' . $student['id'] . '"');

                                                            if ($paths != '') {
                                                                ?>
                                                                <img src="<?php echo $paths; ?>" alt="" class="">
                                                            <?php } else { ?>
                                                                <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                                            <?php } ?>
                                                        </span>
                                                        <?php
                                                        $student_detail = $this->my_custom_functions->get_details_from_id($student['id'], TBL_STUDENT);
                                                        ?>
                                                        <h3><?php echo $student_detail['name']; ?></h3>
                                                        <span class="tag_section"><?php
                                                            $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_detail['class_id'] . '"');
                                                            $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_detail['section_id'] . '"');
                                                            echo $class_name . '' . $section_name . ', ' . $student_detail['roll_no'];
                                                            ?></span>
            <!--                                                        <span class="tag_section"><?php //echo 'Roll No - ' . $student_detail['roll_no'];     ?></span>-->

                                                        <input required type="checkbox" name="student_check[]" value="<?php echo $student['id']; ?>" class="custom-control-input checkBoxClass no-select" id="customControlValidation<?php echo $student['id']; ?>" required>
                                                        <label class="custom-control-label" for="customControlValidation<?php echo $student['id']; ?>"></label>
                                                    </span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <li class="no_rec">No record found</li>    
                                    <?php } ?>
                                </ul>

                            </div>

                            <?php if (!empty($student_list)) { ?>
                                <div class="col-lg-12 buttonContainer" style="float:left;">
                                    <input type="submit" name="submit" class="btn002 forceClick" value="GO" style="opacity: 0">

                                </div>
                            <?php } ?>
                            <?php echo form_close(); ?>
                        <?php } ?>
                        <?php $this->load->view('teacher/include/footer'); ?>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#f_click").hide();
                                $('.checkBoxClass').click(function () {
                                    if ($('.checkBoxClass:checked').length > 0) {
                                        $("#f_click").show();
                                    } else {
                                        $("#f_click").hide();
                                    }
                                });
                                $('#ckbCheckAll').click(function () {
                                    if ($('#ckbCheckAll:checked').length > 0) {
                                        $("#f_click").show();
                                    } else {
                                        $("#f_click").hide();
                                    }
                                });
                            });
                        </script>