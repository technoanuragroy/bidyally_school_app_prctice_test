<?php $this->load->view('teacher/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent calender_wrapper">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">

                        <div class="wrapperGrid">
                            <?php
                            if (!empty($school_calender)) {
                                foreach ($school_calender as $cal_date => $cal_data) {
                                    ?>    

                                    <h1><?php echo date('F Y', strtotime($cal_date)) ?></h1>
                                    <div class="wrappergrid_row">
                                        <div class="wrappergrid_row_left">
                                            <h2><?php echo date('d', strtotime($cal_date)) ?></h2>

                                        </div>
                                        <div class="wrappergrid_row_right">
                                            <?php foreach ($cal_data as $calender) { ?>
                                                <h2 class="grid_row_sub"><?php echo $calender['title']; ?></h2>
                                                <?php 
                                                $str = '';
                                                if($calender['end_date'] != '0000-00-00' ){
                                                    $str = ' - '.date('d M Y',strtotime($calender['end_date']));
                                                } ?>
                                                <span>(<?php echo date('d M Y',strtotime($calender['start_date'])).$str ?>)</span>
                                                <p>
                                                    <?php echo $calender['description']; ?>
                                                </p>
                                                <?php if($calender['office_open'] == 1){
                                                    $office_open = 'Yes';
                                                }else{
                                                     $office_open = 'No';
                                                } ?>
                                                <span class="officestatus">office open- <?php echo $office_open; ?></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php
                                }
                            }
                            ?>

                        </div>




                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
