<?php //echo "<pre>";print_r($running_class);         ?>
<?php $this->load->view('teacher/include/header'); ?>

<body>
    <div class="wrapper">
        <div class="wrap_content ">
            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="formContent">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                                <?php } ?>
                                <?php if ($this->session->flashdata("e_message")) { ?>
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                <?php } ?>
                            </div>
                            <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                            <div class="rowBox">
                                <div class="formContent">
                                    <?php
                                    //echo "<pre>";print_r($student_list);die;
                                    $count = 0;
                                    foreach ($student_list as $student_idd) {
                                        if ($count == 0) {
                                            $student = $this->my_custom_functions->get_details_from_id($student_idd, TBL_STUDENT);
                                        }
                                        $count++;
                                    }

                                    echo form_open_multipart('teacher/user/writeDiary', array('id' => 'frmRegister', 'class' => 'js-validation-material'));
                                    ?>

                                    <h5>Name : Send to multiple</h5>
                                    <h5>Class : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student['class_id'] . '"'); ?></h5>
                                    <h5>Section : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student['section_id'] . '"'); ?></h5>
                                    <h5>Roll No : To all</h5>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Select subject</label>
                                            <select name="subject_id" class="form-control">
                                                <option value="">Select</option>
                                                <?php foreach ($subject_list as $subject) { ?>
                                                    <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                                <?php } ?>
                                                <option value="0">other</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Diary heading</label>
                                            <input type="text" name="heading" class="form-control">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Diary note</label>
                                            <textarea name="diary_note" class="form-control"></textarea>
                                        </div>


                                        <div class="form-group buttonContainer">
                                            <input type="hidden" name="student_id" value='<?php echo json_encode($student_list); ?>'>
                                            <input type="submit" name="submit" class="btn002" value="Save">

                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                   

                    <?php $this->load->view('teacher/include/footer'); ?>
