<?php $this->load->view('teacher/include/header'); ?>
<style>
    .gridPdf li{
        height:0px !important;
    }
    .down{
        width:100% !important;
        float: left;
        text-align: center;
        
    }
</style>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="innerbodycontent container_teacherdashboard">


                    <div class="rowBox">
                        <button id="find_btn">Find Me</button>
                        <div id="result"></div> 
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="gridPdf">
                                    <li>
                                        <label>Browse file</label>
                                        <input type="file" name="doc_upload[]" class="upload_ico_file" multiple="">

                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="gridPdf">
                                    <li>
                                        <label>Download file</label>
                                        <p class="down"><a class="downloadFile down" href="<?php echo base_url() . 'teacher/user/downloadFile/171' ?>/<?php echo DOWNLOAD_KEY; ?>" download="hello">
                                            <i class="fas fa-download"></i>
                                        </a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('teacher/include/footer'); ?>
                    <script type="text/javascript">
                        $("#find_btn").click(function () { //user clicks button
                            if ("geolocation" in navigator) { //check geolocation available
                                //try to get user current location using getCurrentPosition() method
                                navigator.geolocation.getCurrentPosition(function (position) {
                                    $("#result").html("Found your location <br />Lat : " + position.coords.latitude + " </br>Lang :" + position.coords.longitude);
                                });
                            } else {
                                console.log("Browser doesn't support geolocation!");
                            }
                        });
                    </script>

