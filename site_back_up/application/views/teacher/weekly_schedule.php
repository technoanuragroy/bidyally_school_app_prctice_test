<?php $this->load->view('teacher/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                 <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                       
                        <div class="routine_timetable">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <?php
                                $weeklist = $this->config->item('days_list');
                                $active_flag = 0;
                                $tab_cnt = 0;
                                foreach ($weeklist as $day => $val) {
                                    if ($active_flag == 0) {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php echo $active; ?>" id="<?php echo $weeklist[$day]; ?>-tab" data-toggle="tab" href="#<?php echo $weeklist[$day]; ?>" role="tab" aria-controls="<?php echo $weeklist[$day]; ?>" aria-selected="true"><?php echo substr($weeklist[$day], 0, 3); ?></a>
                                    </li>
                                    <?php
                                    $active_flag++;
                                }
                                ?>


                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <?php
                                if (!empty($schedule)) {

                                    foreach ($schedule as $day => $class_list_data) {
                                        if($tab_cnt == 0){
                                            $tab_active = 'active';
                                        }else{
                                            $tab_active = '';
                                        }
                                        ?>

                                        <div class="tab-pane fade show <?php echo $tab_active; ?>" id="<?php echo $weeklist[$day]; ?>" role="tabpanel" aria-labelledby="<?php echo $weeklist[$day]; ?>-tab">
                                            <ul class="routinelist">
                                                <?php foreach ($class_list_data as $class_list) { ?>
                                                    <li>
                                                        <span class="subjectsDiv"><small>
                                                            <?php $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $class_list['subject_id'] . '" '); 
                                                                    if($subject_name != ''){
                                                                        echo $subject_name;
                                                                    }else{
                                                                      echo $class_list['period_name'];
                                                                    }
                                                        ?></small></span>
                                                        <h3><?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $class_list['class_id'] . '" '); ?>
                                                            <?php echo '(' . $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $class_list['section_id'] . '" ') . ')'; ?>
                                                        </h3>
                                                        <span class="tag_section"><i class="far fa-clock"></i> <?php echo date('h:i a', strtotime($class_list['period_start_time'])) . ' - <i class="far fa-clock"></i> ' . date('h:i a', strtotime($class_list['period_end_time'])); ?></span>
                                                    </li>
                                                <?php } ?>

                                            </ul>


                                        </div>
                                        <?php
                                    $tab_cnt++;}
                                }
                                ?>


                            </div>
                        </div>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
