<?php //echo "<pre>";print_r($running_class);          ?>
<?php $this->load->view('teacher/include/header'); ?>

<script type="text/javascript">

    function CustomConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

    
</script>

<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">
            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>

                    <div class="col-md-12">
                        <?php if ($this->session->flashdata("s_message")) { ?>
                            <!-- Success Alert -->
                            <div class="alert alert-success alert-dismissable s_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                            </div>
                            <!-- END Success Alert -->
                        <?php } ?>
                        <?php if ($this->session->flashdata("e_message")) { ?>
                            <!-- Danger Alert -->
                            <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                            </div>
                            <!-- END Danger Alert -->
                        <?php } ?>
                    </div>

                    <ul class="nav nav-tabs diaryTab highlightTab" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="buttontab viewClass nav-link textpos active" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="view" aria-selected="true">View</a>
                        </li>
                        <li class="nav-item">
                            <a class="buttontab createClass nav-link textpos" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">Create</a>
                        </li>
                    </ul>
                    <div class="rowBox">
                        <div class="tab-content gapContent highlightContent" id="myTabContent">
                            <div class="tab-pane fade show" id="view" role="tabpanel" aria-labelledby="view-tab">
                                <?php
                                if (!empty($diary_detail)) {//echo "<pre>";print_r($diarylist);
                                    foreach ($diary_detail as $roww) {
                                        $subject_id = '';
                                    if($this->uri->segment(5) && $this->uri->segment(5) != ''){
                                        $diary_id = $this->uri->segment(5);
                                        
                                        $subject_id = $this->my_custom_functions->get_particular_field_value(TBL_DIARY,'subject_id','and id = "'.$diary_id.'"');
                                        if($subject_id != 0){
                                            $subject_id = $subject_id;
                                        }else{
                                            $subject_id = 0;
                                        }
                                    }
                                        

                                        $break_start_date = explode('-', date('Y-m-d', $roww['issue_date']));
                                        $year = $break_start_date[0];
                                        $monthe = $break_start_date[1];
                                        $day = $break_start_date[2];

                                        $monthNum = $monthe;
                                        $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                        $monthName = $dateObj->format('M'); // March
                                        ?>
                                        <a href="<?php echo base_url(); ?>teacher/user/diaryDetail/<?php echo $roww['id']; ?>">
                                            <?php
                                            $read_cls = '';
                                            if ($roww['teacher_read_msg'] == 2) {
                                                $read_cls = 'read_cls';
                                                ?>

                                            <?php } ?>
                                            <div class="noticeGrid <?php echo $read_cls; ?>">
                                                <div class="grid_row">
                                                    <div class="grid_row_left">
                                                        <h2><?php echo $day; ?></h2>
                                                        <span class="dayText"><?php echo $monthName; ?></span>
                                                    </div>
                                                    <div class="grid_row_right">
                                                        <h2 class="grid_row_sub"><?php echo $roww['heading']; ?></h2>
                                                        <span class="arrow_ind">
                                                            <?php if ($roww['status'] == 1) { ?>
                                                                <i class="fas fa-arrow-right outgoing"></i>
                                                            <?php } else { ?>
                                                                <i class="fas fa-arrow-left incoming"></i>

                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <?php
                                    }
                                }
                                ?>


                            </div>
                            <div class="tab-pane fade show active" id="create" role="tabpanel" aria-labelledby="create-tab">
                                <div class="formContent">

                                    <?php echo form_open_multipart('teacher/user/writeDiary', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <h5>Name : <?php echo $student['name']; ?></h5>
                                    <h5>Class : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student['class_id'] . '"'); ?></h5>
                                    <h5>Section : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student['section_id'] . '"'); ?></h5>
                                    <h5>Roll No : <?php echo $student['roll_no']; ?></h5>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Select subject</label>
                                            <select name="subject_id" class="form-control">
                                                <option value="">Select</option>
                                                <?php foreach ($subject_list as $subject) { 
                                                    if($subject['id'] == $subject_id){
                                                        $selected = "selected='selected'";
                                                    }else{
                                                        $selected = ""; 
                                                    }
                                                    ?>
                                                    <option value="<?php echo $subject['id']; ?>" <?php echo $selected; ?>><?php echo $subject['subject_name']; ?></option>
                                                <?php } ?>
                                                <option value="0">other</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Diary heading</label>
                                            <input type="text" name="heading" class="form-control">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Diary note</label>
                                            <textarea name="diary_note" class="form-control" rows="20"></textarea>
                                        </div>


                                        <div class="form-group buttonContainer">
                                            <input type="hidden" name="student_id" value="<?php echo $student['id']; ?>">
                                            <input type="submit" name="submit" class="btn002" value="Send">

                                        </div>

                                        <?php echo form_close(); ?>


                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php $this->load->view('teacher/include/footer'); ?>
<script type="text/javascript">
                       <?php if($this->uri->segment(5) && $this->uri->segment(5) != ''){ ?> 
                        $('[href="#create"]').tab('show');
                        <?php } ?>
                        </script>