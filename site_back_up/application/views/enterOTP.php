<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SchoolApp|Register</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>


    </head>
    <body>
        <div class="wrapper">
            <div class="wrap_content heightFullwraper">
                <!-- Default form register -->
                <form class="text-center p-5 js-validation-material" method="post" action="<?php echo current_url(); ?>">
                    
                    <h1>Enter OTP</h1>
                    <div class="col-md-12">
                        <?php if ($this->session->flashdata("s_message")) { ?>
                            <!-- Success Alert -->
                            <div class="alert alert-success alert-dismissable s_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                            </div>
                            <!-- END Success Alert -->
                        <?php } ?>
                        <?php if ($this->session->flashdata("e_message")) { ?>
                            <!-- Danger Alert -->
                            <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                            </div>
                            <!-- END Danger Alert -->
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <div class="md-form">
                                <input required type="number" id="enter_otp" name="enter_otp" class="form-control">
                                <label for="enter_otp">Enter OTP</label>
                            </div>
                        </div>
                    </div>


                    <input type="submit" name="submit" class="btn btn001" value="validate OTP"/>
                </form>
                <div class="bgBottom"></div>
            </div>
        </div>


    </body>
</html>
