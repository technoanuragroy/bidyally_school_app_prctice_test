<?php //$this->load->view("school/_include/school_header");        ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Manage Schools</title>

        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this admin?</p>');
                $('.btn-alt-success').attr('onclick', 'confirm_delete(' + id + ')');
            }
            function confirm_delete(id) {
                window.location.href = "<?php echo base_url(); ?>admin/user/deleteAdmin/" + id;
            }
        </script>
    </head>
    <body>



        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('admin/include/right_side_overlay'); ?>
            <!-- END Side Overlay -->

           
            <!-- Left Sidebar start -->
            <?php $this->load->view('admin/include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('admin/include/admin_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Manage Schools</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
<!--                            <a href="<?php //echo base_url(); ?>admin/user/addAdmin" class="btn btn-primary">Add Admin</a>-->
                        </div>
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>
                        <div class="block-content block-content-full">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th class="text-center"></th>
                                        <th>School Name</th>
                                        <th>Contact Person Name</th>
                                        <th>Mobile Number</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th class="text-center" style="width: 15%;">Login</th>
<!--                                        <th class="text-center" style="width: 15%;">Delete</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($schools)) {
                                        $i = 1;
                                        foreach ($schools as $row) {//echo "<pre>";print_r($row);


                                            //$encrypted = $this->my_custom_functions->ablEncrypt($admin['admin_id']);
                                            $encrypted = $row['id'];
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $i; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo $row['contact_person_name']; ?></td>
                                                <td><?php echo $row['mobile_no']; ?></td>
                                                 <td><?php echo $row['email_address']; ?></td>

                                                <td><?php
                                                
                                                    $status = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN,'status','and id = "'.$row['id'].'"');
                                                    if ($status == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?></td>
                                                <td class="text-center">                                    
                                                    <a href="<?php echo base_url() . 'admin/user/loginAsSchool/' . $encrypted; ?>" title="Login As School" target="_blank" >
                                                        <i class="fa fa-user"></i>
                                                    </a>
                                                </td>
<!--                                                <td class="text-center">         

                                                    <a href="<?php echo base_url() . 'admin/user/deleteAdmin/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete(<?php echo $encrypted; ?>);">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>-->
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>  


                                    <?php } else { ?>
                                        <tr>
                                            <td colspan="9">No classes found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->



                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <?php $this->load->view('admin/include/admin_footer'); ?>
            <!-- END Footer -->
        </div>

    </body>
</html>
