<?php //$this->load->view("school/_include/school_header");       ?>
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Dashboard</title>


    </head>
    <body>



        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            <?php $this->load->view('admin/include/right_side_overlay'); ?>

            <!-- Left Sidebar start -->
            <?php $this->load->view('admin/include/left_sidebar'); ?>
            <!-- Left Sidebar End -->

            <!-- END Sidebar -->
            <!-- Header -->
            <?php $this->load->view('admin/include/admin_header'); ?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit Admin</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $admin['name']; ?>">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="number" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $admin['phone']; ?>">
                                            <label for="phone">Phone Number</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="email" class="form-control" name="email" id="Email" placeholder="Account email" value="<?php echo $admin['email']; ?>">
                                            <label for="email">Email Address</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" value="" autocomplete="new-password">
                                            <label for="password">Password</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" <?php
                                                if ($admin['status'] == 1) {
                                                    echo "selected";
                                                }
                                                ?>>Active</option>
                                                <option value="0" <?php
                                                if ($admin['status'] == 0) {
                                                    echo "selected";
                                                }
                                                ?>>Inactive</option>
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12" for="adminphoto">Upload Photo</label>
                                        <div class="col-12">
                                            <input type="file" id="adminphoto" name="adminphoto">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <input type="hidden" name="admin_id" value="<?php echo $admin['admin_id']; ?>">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    </div>
<?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            <!-- Footer -->
<?php $this->load->view('admin/include/admin_footer'); ?>
            <!-- END Footer -->
        </div>


    </body>
</html>



