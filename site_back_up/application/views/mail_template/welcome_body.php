                <br><span style="color:#333333;font-weight:700">Select your package</span>
                <br>Whether you want to just try twib or is serious about maximizing your business growth and simplify sales process, just select your free or paid package or can try our full featured trial options.
                <br>
                <a href="https://web.twib.online/company/user/selectPackage" bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px">
                    Select Your Package
                </a>
                <br>
                <br><span style="color:#333333;font-weight:700">Add users</span>
                <br>Add your sales personals in the twib company panel and start tracking your users sales activity on the field.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/user/add_user">
                    Add users
                </a>
                <br>
                <br><span style="color:#333333;font-weight:700">Assign work remotely</span>
                <br>Assign work to your users so that they can view and work accordingly.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/user/add_work">
                  Assign work now
              </a>
                <br>
                <br><span style="color:#333333;font-weight:700">View reports</span>
                <br>Once your users start using twib on the field then you can just sit back and view their activity and statistics, detail charts and reports.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/report/checkin_report">
                    Check Activity
                </a>
                <br><br>
                <br> Let me know if you have any questions.
                <br>
                <br> Have a great day,
                <br> <span style="color:#333;font-weight: 700;">Team Twib</span>
              