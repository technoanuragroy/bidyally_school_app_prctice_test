<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
</head>
<body style="margin:0;">
    <div style="background-color:#f4f4f4;margin:0!important;padding:0!important">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#ECB101">
                        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr bgcolor="#ECB101">
                                    <td style="padding:40px 10px 40px 10px" valign="top" align="center" bgcolor="#ECB101">
<!--                                      <img alt="Bidyaaly" src="https://twib.online/global_assets/email_logo.png?t=01042019" style="display:block;width:140px;max-width:140px;min-width:40px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#ffffff;font-size:18px" class="CToWUd" width="140" height="" border="0">-->
                                        <span style="color:#000;font-size:34px;">Bidyaaly</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="padding:0px 10px 0px 10px" align="center" bgcolor="#ECB101">
                        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="padding:20px 0px 20px 0px;border-radius:8px 8px 0px 0px;color:#111111;font-family:'Lato',Helvetica,Arial,sans-serif;font-size:48px;font-weight:400;letter-spacing:4px;line-height:48px" valign="top" align="center" bgcolor="#fff">
                                        <!-- <img alt="healthifyme-hero-image" src="https://ci5.googleusercontent.com/proxy/RbY6kixqc4W3WluQGspTZcFFJdP9oTngW_cDiGCI4dtlHn_q1iX6SUQGdQN3LO6f8PaGU3O1o7eVSBSCYqk_Fvzf73856reHsu9jtE5u2WJwPCWvi_XJKFciT2stbsyGpfmRWa4r4g=s0-d-e1-ft#https://s3.ap-south-1.amazonaws.com/transactional-email-assets-healthifyme/c2.png" width="600" style="display:block;width:100%;max-width:100%;min-width:100px;border-radius:8px 8px 0px 0px;object-fit:contain" border="0" class="CToWUd a6T" tabindex="0"> -->
                                        <div class="a6S" dir="ltr" style="opacity: 0.01; left: 875.6px; top: 305.2px;"><div id=":uv" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <!-- Header ends -->
                <!-- Body starts -->
                <tr>
                    <td style="padding:0px 10px 0px 10px;background: #f4f4f4;" align="center" bgcolor="">
                        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f4f4f4">

                            <tr bgcolor="#ffffff">
                                <td style="padding:20px 30px 5px 30px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:1.1rem;font-weight:400;line-height:25px" valign="top" align="left" bgcolor="#ffffff">
                                    <span style="text-align:left;color:#666666">
                                        %addressing_user% <!-- Addressing user -->
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:20px 30px 30px 30px;border-radius:0px 0px 8px 8px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:25px" valign="top" align="center" bgcolor="#ffffff">
                                    <p style="margin:0;text-align:left">
                                        %mail_body% <!-- Mail body -->
                                    </p>
                                    <p style="text-align:left">
                                        Regards,<br>Team Bidyaaly
                                    </p>
                                </td>
                            </tr>

<!--                            <tr>
                                <td style="padding:30px 10px 0px 10px" align="center" bgcolor="#f4f4f4">
                                    <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding:30px 30px 0px 30px;border-radius:4px 4px 4px 4px;color:#666666;font-family:'Lato',Helvetica,Arial,sans-serif;font-size:18px;font-weight:100!important;line-height:25px" align="center" bgcolor="transparent">
                                                    <p style="margin:0;color:#606060">Let's connect!</p>
                                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                              <tr>
                                                                  <td style="padding:10px 0 8px 0" valign="top" align="center">
                                                                    <a style="display:inline-block" href="https://www.facebook.com/Twib-330812297388421/" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://twib.online/global_assets/small_fb.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>&nbsp;&nbsp;&nbsp;
                                                                    <a style="display:inline-block" href="https://twitter.com/TwibApp" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://twib.online/global_assets/small_twitter.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>&nbsp;&nbsp;&nbsp;
                                                                    <a style="display:inline-block" href="https://twib.online/" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://twib.online/global_assets/globe.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>&nbsp;&nbsp;&nbsp;
                                                                    <a style="display:inline-block" href="https://www.youtube.com/channel/UC_5Sv4zjYabCvzTmhp9ku-Q" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://twib.online/global_assets/small_youtube.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>
                                                                  </td>
                                                              </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table style="max-width:600px; margin: 5px 0 10px;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="right" bgcolor="transparent">
                                                    <a style="text-decoration:none">
                                                        <span style="text-align:center;color:#aaa!important;text-decoration:none;display:block;">
                                                            <a href="https://play.google.com/store/apps/details?id=com.twib.online">
                                                                <img src="https://twib.online/global_assets/google_play_icon.png?t=01042019" style="vertical-align: top;">
                                                            </a>
                                                        </span>
                                                    </a>
                                                </td>
                                                <td align="left" bgcolor="transparent">
                                                    <a style="text-decoration:none">
                                                        <span style="text-align:center;color:#aaa!important;text-decoration:none;display:block;">
                                                            <a href="https://itunes.apple.com/in/app/twib/id1453389043">
                                                                <img src="https://twib.online/global_assets/apple_app_store.png?t=01042019" style="padding-left: 18px;vertical-align: top;">
                                                            </a>
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>    

                                    
                                </td>
                            </tr>-->
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
       </div>
</body>
</html>