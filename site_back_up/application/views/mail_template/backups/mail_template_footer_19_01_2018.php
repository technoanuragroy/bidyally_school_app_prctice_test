
</td>
          </tr>
        </tbody></table>

      </td>
    </tr>

    <tr>
      <td style="padding:30px 10px 0px 10px" align="center" bgcolor="#f4f4f4">

        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">

          <tbody><tr>
            <td style="padding:30px 30px 0px 30px;border-radius:4px 4px 4px 4px;color:#666666;font-family:'Lato',Helvetica,Arial,sans-serif;font-size:18px;font-weight:100!important;line-height:25px" align="center" bgcolor="transparent">
              <p style="margin:0;color:#606060">Let's connect!</p>
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                  <td style="padding:10px 0 8px 0" valign="top" align="center">
                    <a style="display:inline-block" href="https://www.facebook.com/Twib-330812297388421/" target="_blank" >
                      <img style="display:block;border-width:0" src="http://twib.online/global_assets/small_fb.png" alt="" class="CToWUd" width="35" height="35" border="0">
                    </a>&nbsp;&nbsp;&nbsp;
                    <a style="display:inline-block" href="https://twitter.com/TwibApp" target="_blank" >
                      <img style="display:block;border-width:0" src="http://twib.online/global_assets/small_twitter.png" alt="" class="CToWUd" width="35" height="35" border="0">
                    </a>&nbsp;&nbsp;&nbsp;
                    <a style="display:inline-block" href="https://twib.online/" target="_blank" >
                      <img style="display:block;border-width:0" src="http://twib.online/global_assets/globe.png" alt="" class="CToWUd" width="35" height="35" border="0">
                    </a>&nbsp;&nbsp;&nbsp;
                    <a style="display:inline-block" href="https://www.youtube.com/watch?v=_CCRuhqCHYU&feature=youtu.be" target="_blank" >
                      <img style="display:block;border-width:0" src="http://twib.online/global_assets/small_youtube.png" alt="" class="CToWUd" width="35" height="35" border="0">
                    </a>
                  </td>
                </tr>
              </tbody></table>
            </td>
          </tr>
        </tbody></table>


        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">

          <tbody><tr>
            <td style="padding:0px 0px 0px 0px;border-radius:4px 4px 4px 4px;color:#666666;font-family:'Lato',Helvetica,Arial,sans-serif;font-size:0.8rem;font-weight:400!important;line-height:25px" align="center" bgcolor="transparent">
              <a style="text-decoration:none">
                <span style="text-align:center;color:#aaa!important;text-decoration:none;display:block">
                  YOUR SALES PROCESS, SIMPLIFIED
                </span>
              </a>
            </td>
          </tr>
        </tbody></table>
      </td>
    </tr>

  </tbody></table>
  <br>
<!--  <div id="m_-1868215405246589843m_-2455390189241147832_t"></div>-->
  <img src="emailtemplate_files/K6lytvSXyjiMKpi2fUNfF3laPVKwTDbKjA_Q5wtyaD50sgUO2FCpzDp2YMGt.gif" alt="" class="CToWUd" width="1" height="1" border="0">
<br>
<p style="text-align:center;font-size:13px;color:#6c737a; font-family:Arial;">Don't want to hear from us anymore? 
    <br> That's okay, you can <a href="#" target="_blank" >unsubscribe</a>. We'll be sad, but no hard feelings. </p><img alt="" src="emailtemplate_files/IFqaxe6peYKJsvVxUzs_-OAXffKr9B8_sRqO_WAY6rNQrIREoCXPMRxC0tlR.gif" class="CToWUd" width="1px" height="1px"></div>

</body></html>