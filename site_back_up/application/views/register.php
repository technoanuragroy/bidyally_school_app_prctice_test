<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SchoolApp|Register</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
    <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>


  </head>
  <body>
    <div class="wrapper">
      <div class="wrap_content">
        <!-- Default form register -->
        <form class="text-center p-5">
              <h1>School App</h1>

              <div class="md-form">
                  <input type="text" id="materialName" class="form-control">
                  <label for="materialName">Name</label>
              </div>

            <div class="md-form">
                <input type="text" id="materialContactPerson" class="form-control">
                <label for="materialContactPerson">Contact Person</label>
            </div>
            <div class="md-form">
                <input type="number" id="materialnumber" class="form-control">
                <label for="materialnumber">mobile number</label>
            </div>
            <div class="md-form">
                <input type="email" id="materialemail" class="form-control">
                <label for="materialemail">Email address</label>
            </div>

          <div class="md-form">
              <input type="password" id="materialPassword" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock" autocomplete="new-password">
              <label for="materialPassword">Password</label>
          </div>
          <div class="md-form">
              <input type="text" id="materialaddress" class="form-control">
              <label for="materialaddress">address</label>
          </div>


              <!-- Sign up button -->
              <input type="submit" class="btn btn001" value="Submit"/>
            </form>
            <div class="bgBottom"></div>
    </div>
    </div>


  </body>
</html>
