<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School_main_model extends CI_Model {

    public function __construct() {
            parent::__construct();
            

    }
    
    
    function login_check() {

        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $sql = "SELECT * FROM ".TBL_COMMON_LOGIN." WHERE username = '".$username."' AND status=1";
        $query = $this->db->query($sql);
                
        $password = $this->input->post('password');                        
        $record = array();
        if ($query->num_rows() > 0) {

            $record = $query->row_array();

            if(password_verify($password, $record['password'])) {
                //echo "<pre>";print_r($record);die;
                if($record['type'] == SCHOOL){
                $school_detail = $this->my_custom_functions->get_details_from_id($record["id"],TBL_SCHOOL);
                $session_data = array(
                    "usertype" => $record['type'],
                    "school_id" => $record["id"],
                    "school_username" => $record["username"],
                    "school_email" => $school_detail["email_address"],  
                    "school_is_logged_in" => 1
                );
                }else if($record['type'] == TEACHER){
                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER,'school_id','and id = "'.$record["id"].'"');
                  $session_data = array(
                    "usertype" => $record['type'],
                    "school_id" => $school_id,
                    "teacher_id" => $record["id"],
                    "school_username" => $record["username"], 
                    "school_is_logged_in" => 1
                );  
                }
                $this->session->set_userdata($session_data);
                

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
}
