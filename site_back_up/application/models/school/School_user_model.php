<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_classes_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
//pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageClasses/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_section_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
//pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageSections/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_subject_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
//pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageSubjects/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_teacher_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
//pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageTeachers/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_time_table_data($class_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();

        $day_list = $this->config->item('days_list');
//$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

        foreach ($day_list as $day => $val) {
//foreach ($period_list as $ress) {
//$create_data[$day][$ress['id']] = '';
            $counter = 0;
            foreach ($result as $routine) {
                if ($day == $routine['day_id']) {
                    $create_data[$day][$counter] = $routine;
                }

                $counter++;
            }
//}
        }
//echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_period_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
//pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/managePeriods/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function create_school($admin_id) {


        $update_data = array(
            "name" => $this->input->post("name"),
            "phone" => $this->input->post("phone"),
            "status" => $this->input->post("status"),
            "email" => $this->input->post("email"),
        );


        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }

    function get_student_detail($student_id) {
        $parents_list = '';
        $sql = 'Select t_s.*,t_s_d.* from ' . TBL_STUDENT . ' t_s join ' . TBL_STUDENT_DETAIL . ' t_s_d on t_s.id = t_s_d.student_id where t_s.id = ' . $student_id . '';
        $query = $this->db->query($sql);

        $res = $query->result_array();


        $sql1 = "Select parent_id from " . TBL_PARENT_KIDS_LINK . " where student_id = " . $student_id . "";
        $query1 = $this->db->query($sql1);
        $res1 = $query1->result_array();

        foreach ($res1 as $ress1) {
            $parents_list .= $ress1['parent_id'] . ',';
        }

        $parents = rtrim($parents_list, ',');


        $sql2 = "Select id,username from " . TBL_COMMON_LOGIN . " where id IN ( " . $parents . " ) ";
        $query2 = $this->db->query($sql2);
        $res2 = $query2->result_array();



        $data['student_detail'] = $res;
        $data['username_detail'] = $res2;
//echo "<pre>";print_r($data);
        return $data;
    }

    function get_class_list($school_id) {
        $sql = "SELECT * FROM " . TBL_CLASSES . " WHERE school_id = '" . $school_id . "'";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }

    function update_employee_photo($teacher_id, $photo_url) {
        $update_data = array(
            "file_url" => $photo_url
        );

        $this->db->where('teacher_id', $teacher_id);
        $return = $this->db->update(TBL_TEACHER_FILES, $update_data);
    }

    function get_previous_week_data() {
        $res = array();
        $attendance_count = 0;
        $final_rec = '';


        $week_start = date('Y-m-d', strtotime('monday last week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));
        //die;
//echo $start_week . ' // ' . $end_week;die;
        $sql = "SELECT * from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_date>='" . $week_start . "' and attendance_date<='" . $week_end . "'  order by attendance_date ASC";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }

        foreach ($data as $row) {
            if ($row['attendance_status'] == 1) {
                //$attendance_count++;

                $res[$row['attendance_date']] += 1;
            }
        }


        foreach ($res as $ress) {
            $final_rec .= $ress . ',';
        }
        $records = rtrim($final_rec, ',');
        return $records;
    }

    function get_current_week_data() {
        $final_rec = '';
        $day = date('w');
        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));
        $sql = "SELECT * from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_date>='" . $week_start . "' and attendance_date<='" . $week_end . "'  order by attendance_date ASC";
        $query = $this->db->query($sql);
        $data = $query->result_array();

        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }


        foreach ($data as $row) {
            if ($row['attendance_status'] == 1) {
                //$attendance_count++;

                $res[$row['attendance_date']] += 1;
            }
        }
        foreach ($res as $ress) {
            $final_rec .= $ress . ',';
        }
        $records = rtrim($final_rec, ',');
        return $records;
    }

    function get_attendance_count() {
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');
        $sql = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_status = 1 and attendance_date>='" . $first_day_this_month . "' and attendance_date <='" . $last_day_this_month . "'";
        $query = $this->db->query($sql);
        $data = $query->result_array();

        foreach ($data as $present_month_attendance) {
            $row['present_month_attendance'] = $present_month_attendance['count(attendance_status)'];
        }

        $first_day_last_month = date('Y-m-d', strtotime('first day of last month'));
        $last_day_last_month = date('Y-m-d', strtotime('last day of last month'));
        $sql1 = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and  attendance_status = 1 and attendance_date>='" . $first_day_last_month . "' and attendance_date <='" . $last_day_last_month . "'";
        $query1 = $this->db->query($sql1);
        $data1 = $query1->result_array();

        foreach ($data1 as $prev_month_attendance) {
            $row['prev_month_attendance'] = $prev_month_attendance['count(attendance_status)'];
        }
        $diff = abs($row['present_month_attendance'] - $row['prev_month_attendance']);


        @$avg_atten = ($diff / $row['present_month_attendance']) * 100;
        $row['avg_attendance'] = $avg_atten;


        if ($row['present_month_attendance'] > $row['prev_month_attendance']) {
            $row['month_flag'] = 1;  /// Upgoing 
        } else {
            $row['month_flag'] = 0;  /// Downgoing  
        }

        ///////////////////////////////////////  WEEKLY ATTENDANCE COUNT  ////////////////////////////////

        $day = date('w');
        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));
        $sql2 = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_status = 1 and attendance_date>='" . $week_start . "' and attendance_date <='" . $week_end . "'";
        $query2 = $this->db->query($sql2);
        $data2 = $query2->result_array();

        foreach ($data2 as $present_week_attendance) {
            $row['present_week_attendance'] = $present_week_attendance['count(attendance_status)'];
        }

        // echo "<pre>";print_r($row);die;
        $today = date('Y-m-d');
        $sql3 = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_status = 1 and attendance_date>='" . $today . "' and attendance_date <='" . $today . "'";
        $query3 = $this->db->query($sql3);
        $data3 = $query3->result_array();

        foreach ($data3 as $todays_attendance) {
            $row['todays_attendance'] = $todays_attendance['count(attendance_status)'];
        }


        return $row;
    }

    function get_previous_week_activity_data() {
        $data_class_works = array();
        $data_home_works = array();
        $data_assignment_works = array();
        $prev_week_data_list = '';

        $week_start = date('Y-m-d', strtotime('monday last week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

        $start_previous_week = strtotime($week_start);
        $end_previous_week = strtotime($week_end);


        $sql = "SELECT * from " . TBL_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_previous_week . "' and note_issuetime <= '" . $end_previous_week . "'";
        $query = $this->db->query($sql);
        //$data_class_works = $query->num_rows();
        if ($query->num_rows() > 0) {
            $data_class_works = $query->result_array();
        }

        $sql1 = "SELECT * from " . TBL_HOME_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_previous_week . "' and note_issuetime <= '" . $end_previous_week . "'";
        $query1 = $this->db->query($sql1);
        //$data_home_works = $query1->num_rows();
        if ($query1->num_rows() > 0) {
            $data_home_works = $query1->result_array();
        }

        $sql2 = "SELECT * from " . TBL_ASSIGNMENT_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_previous_week . "' and note_issuetime <= '" . $end_previous_week . "'";
        $query2 = $this->db->query($sql2);
        //$data_assignment_works = $query2->num_rows();
        if ($query2->num_rows() > 0) {
            $data_assignment_works = $query2->result_array();
        }
        $total_activities_list = array_merge($data_class_works, $data_home_works, $data_assignment_works);

        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }
        foreach ($total_activities_list as $row) {
            $activity_date = date('Y-m-d', $row['note_issuetime']);
            $res[$activity_date] += 1;
        }
        foreach ($res as $prev_week_data) {
            $prev_week_data_list .= $prev_week_data . ',';
        }

        $prev_week_final_data_list = rtrim($prev_week_data_list, ',');
        return $prev_week_final_data_list;
    }

    function get_current_week_activity_data() {
        $data_class_works = array();
        $data_home_works = array();
        $data_assignment_works = array();
        $current_week_data_list = '';

        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

        $start_current_week = strtotime($week_start);
        $end_current_week = strtotime($week_end);


        $sql = "SELECT * from " . TBL_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query = $this->db->query($sql);
        //$data_class_works = $query->num_rows();
        if ($query->num_rows() > 0) {
            $data_class_works = $query->result_array();
        }

        $sql1 = "SELECT * from " . TBL_HOME_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query1 = $this->db->query($sql1);
        //$data_home_works = $query1->num_rows();
        if ($query1->num_rows() > 0) {
            $data_home_works = $query1->result_array();
        }

        $sql2 = "SELECT * from " . TBL_ASSIGNMENT_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query2 = $this->db->query($sql2);
        //$data_assignment_works = $query2->num_rows();
        if ($query2->num_rows() > 0) {
            $data_assignment_works = $query2->result_array();
        }
        $total_activities_list = array_merge($data_class_works, $data_home_works, $data_assignment_works);

        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }
        foreach ($total_activities_list as $row) {
            $activity_date = date('Y-m-d', $row['note_issuetime']);
            $res[$activity_date] += 1;
        }

        foreach ($res as $current_week_data) {
            $current_week_data_list .= $current_week_data . ',';
        }

        $current_week_final_data_list = rtrim($current_week_data_list, ',');
        return $current_week_final_data_list;
    }

    function get_activity_count() {
        $data_class_works_a = 0;
        $data_home_works_b = 0;
        $data_assignment_works_c = 0;
        $data_class_works_d = 0;
        $data_home_works_e = 0;
        $data_assignment_works_f = 0;
        $data_class_works_g = 0;
        $data_home_works_h = 0;
        $data_assignment_works_i = 0;
        $record = array();
        $total_activities_list = 0;

        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');

        $start_current_month = strtotime($first_day_this_month);
        $end_current_month = strtotime($last_day_this_month);


        $sql = "SELECT * from " . TBL_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_month . "' and note_issuetime <= '" . $end_current_month . "'";
        $query = $this->db->query($sql);
        //$data_class_works = $query->num_rows();
        ///echo $query->num_rows();die;
        if ($query->num_rows() > 0) {
            $data_class_works_a = $query->num_rows();
        }

        $sql1 = "SELECT * from " . TBL_HOME_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_month . "' and note_issuetime <= '" . $end_current_month . "'";
        $query1 = $this->db->query($sql1);
        //$data_home_works = $query1->num_rows();
        if ($query1->num_rows() > 0) {
            $data_home_works_b = $query1->num_rows();
        }

        $sql2 = "SELECT * from " . TBL_ASSIGNMENT_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_month . "' and note_issuetime <= '" . $end_current_month . "'";
        $query2 = $this->db->query($sql2);
        //$data_assignment_works = $query2->num_rows();
        if ($query2->num_rows() > 0) {
            $data_assignment_works_c = $query2->num_rows();
        }


        $total_activities_list_month = $data_class_works_a + $data_home_works_b + $data_assignment_works_c;




        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

        $start_current_week = strtotime($week_start);
        $end_current_week = strtotime($week_end);


        $sql3 = "SELECT * from " . TBL_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query3 = $this->db->query($sql3);
        //$data_class_works = $query->num_rows();
        if ($query3->num_rows() > 0) {
            $data_class_works_d = $query3->num_rows();
        }

        $sql4 = "SELECT * from " . TBL_HOME_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query4 = $this->db->query($sql4);
        //$data_home_works = $query1->num_rows();
        if ($query4->num_rows() > 0) {
            $data_home_works_e = $query4->num_rows();
        }

        $sql5 = "SELECT * from " . TBL_ASSIGNMENT_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query5 = $this->db->query($sql5);
        //$data_assignment_works = $query2->num_rows();
        if ($query5->num_rows() > 0) {
            $data_assignment_works_f = $query5->num_rows();
        }
        $total_activities_list_current_week = $data_class_works_d + $data_home_works_e + $data_assignment_works_f;




        $day_start = date('Y-m-d 00:00:00');
        $day_end = date('Y-m-d 23:59:59');

        $start_current_day = strtotime($day_start);
        $end_current_day = strtotime($day_end);


        $sql6 = "SELECT * from " . TBL_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_day . "' and note_issuetime <= '" . $end_current_day . "'";
        $query6 = $this->db->query($sql6);
        //$data_class_works = $query->num_rows();
        if ($query6->num_rows() > 0) {
            $data_class_works_g = $query6->num_rows();
        }

        $sql7 = "SELECT * from " . TBL_HOME_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_day . "' and note_issuetime <= '" . $end_current_day . "'";
        $query7 = $this->db->query($sql7);
        //$data_home_works = $query1->num_rows();
        if ($query7->num_rows() > 0) {
            $data_home_works_h = $query7->num_rows();
        }

        $sql8 = "SELECT * from " . TBL_ASSIGNMENT_NOTE . " where school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_day . "' and note_issuetime <= '" . $end_current_day . "'";
        $query8 = $this->db->query($sql8);
        //$data_assignment_works = $query2->num_rows();
        if ($query8->num_rows() > 0) {
            $data_assignment_works_i = $query8->num_rows();
        }

        $total_activities_list_current_day = $data_class_works_g + $data_home_works_h + $data_assignment_works_i;


        $record['month_activity_list'] = $total_activities_list_month;
        $record['week_activity_list'] = $total_activities_list_current_week;
        $record['day_activity_list'] = $total_activities_list_current_day;

        return $record;
    }

    function search_student() {

        $data = array();
        $search = '';
        if ($this->input->post('name') != '') {
            $search .= ' and name like "%' . $this->input->post('name') . '%"';
        }
        if ($this->input->post('rollno') != '') {
            $search .= ' and roll_no = "' . $this->input->post('rollno') . '"';
        }
        if ($this->input->post('class') != '') {
            $search .= ' and class_id = "' . $this->input->post('class') . '"';
        }
        if ($this->input->post('section') != '') {
            $search .= ' and section_id = "' . $this->input->post('section') . '"';
        }

        $sql = "SELECT * FROM " . TBL_STUDENT . " WHERE school_id = '" . $this->session->userdata('school_id') . "'" . $search . "";
        $query = $this->db->query($sql);
        //$data_home_works = $query1->num_rows();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

    function get_permissions() {

        $query = $this->db->query("SELECT * FROM ".TBL_PERMISSION." ORDER BY id ASC");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }
    }

    function get_login_details($id) {
        $this->db->where('id', $id);
        $query = $this->db->get(TBL_TEACHER);
        return $query->row_array();
    }

    function get_existing_permission($uid, $pid) {

        $query = $this->db->query("SELECT * FROM ".TBL_USER_PERMISSION." WHERE uid ='$uid' AND permission = '$pid'");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

}
