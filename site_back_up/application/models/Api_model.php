<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function user_verification() {
        $record = array();
        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username = '" . $username . "' AND status=1";
        $query = $this->db->query($sql);

        $password = $this->input->post('password');
        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();

            if (password_verify($password, $record['password'])) {
                return $record;
            }
        }
    }
    
    function register_device($data) {
        
        $query = $this->db->get_where(TBL_REGISTER_DEVICE, array('user_id' => $data['user_id']));
        
        if ($query->num_rows() > 0) {
            $existing_record = $query->row();
            $update_data = array('device_id' => $data['device_id'],
                'device_type' => $data['device_type'],
                'last_update' => date('Y-m-d H:i:s')
            );
            $this->db->where('id', $existing_record->id);
            $this->db->update(TBL_REGISTER_DEVICE, $update_data);
            return $existing_record->id;
        } else {
            $this->db->insert(TBL_REGISTER_DEVICE, $data);
            $register_device_id = $this->db->insert_id();
            return $register_device_id;
        }
    }

}
