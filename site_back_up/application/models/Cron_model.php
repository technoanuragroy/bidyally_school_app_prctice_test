<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_file_list() {
        $data = array();
        $sql = "SELECT * FROM " . TBL_DELETE_FILES . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }

        return $data;
    }

}
