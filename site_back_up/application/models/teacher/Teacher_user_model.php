<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacher_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_todays_classes($teacher_id) {

        if ($this->input->post('class_date')) {
            $today = $this->input->post('class_date');
        } else {
            $today = date('Y-m-d');
        }
        $class_date_sess = array('query_date' => $today);
        $this->session->set_userdata($class_date_sess);
        $daynum = date('N', strtotime($today));





        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE teacher_id = "' . $teacher_id . '" and day_id = "' . $daynum . '" ORDER BY period_start_time ASC';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_todays_classes_search($teacher_id, $day) {

        $daynum = $day;
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE teacher_id = "' . $teacher_id . '" and day_id = "' . $daynum . '" ORDER BY period_start_time ASC';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_time_table_data($class_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();

        $day_list = $this->config->item('days_list');
        //$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

        foreach ($day_list as $day => $val) {
            //foreach ($period_list as $ress) {
            //$create_data[$day][$ress['id']] = '';
            $counter = 0;
            foreach ($result as $routine) {
                if ($day == $routine['day_id']) {
                    $create_data[$day][$counter] = $routine;
                }

                $counter++;
            }
            //}
        }
        //echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_note_detail() {
        $data = array();
        //echo "<pre>";print_r($this->session->all_userdata());
        $custom_date = $this->session->userdata('query_date');
        $custom_start_date_time = $custom_date . ' 00:00:00';
        $custom_end_date_time = $custom_date . ' 23:59:59';
        $start_time_range = strtotime($custom_start_date_time);
        $end_time_range = strtotime($custom_end_date_time);
        $sql1 = 'SELECT * FROM ' . TBL_NOTE . ' where school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "' . $start_time_range . '" and note_issuetime <= "' . $end_time_range . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        //echo "<pre>";print_r($result1);
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['notes'] = $row;
            $data['note_file'] = $result2;
        }

        return $data;
    }

    function get_home_note_detail() {
        $data = array();
        $custom_date = $this->session->userdata('query_date');
        $custom_start_date_time = $custom_date . ' 00:00:00';
        $custom_end_date_time = $custom_date . ' 23:59:59';
        $start_time_range = strtotime($custom_start_date_time);
        $end_time_range = strtotime($custom_end_date_time);
        $sql1 = 'SELECT * FROM ' . TBL_HOME_NOTE . ' where school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "' . $start_time_range . '" and note_issuetime <= "' . $end_time_range . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_HOME_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['notes'] = $row;
            $data['note_file'] = $result2;
        }

        return $data;
    }

    function get_assignment_note_detail() {
        $data = array();
        $custom_date = $this->session->userdata('query_date');
        $custom_start_date_time = $custom_date . ' 00:00:00';
        $custom_end_date_time = $custom_date . ' 23:59:59';
        $start_time_range = strtotime($custom_start_date_time);
        $end_time_range = strtotime($custom_end_date_time);
        $sql1 = 'SELECT * FROM ' . TBL_ASSIGNMENT_NOTE . ' where school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "' . $start_time_range . '" and note_issuetime <= "' . $end_time_range . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_ASSIGNMENT_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['notes'] = $row;
            $data['note_file'] = $result2;
        }

        return $data;
    }

    function student_list_search() {
        $result = array();
        $serch_string = '';
        if ($this->input->post('class_id') != '') {
            $serch_string .= 'and class_id = "' . $this->input->post('class_id') . '"';
        }
        if ($this->input->post('section_id') != '') {
            $serch_string .= 'and section_id = "' . $this->input->post('section_id') . '"';
        }
        if ($this->input->post('roll_no') != '') {
            $serch_string .= 'and roll_no = "' . $this->input->post('roll_no') . '"';
        }
        if ($this->input->post('student_name') != '') {
            $serch_string .= 'and name like "%' . $this->input->post('student_name') . '%"';
        }

        $sql = "SELECT * FROM " . TBL_STUDENT . " WHERE 1=1 and school_id = " . $this->session->userdata('school_id') . " " . $serch_string;
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_teacher_weekly_schedule() {
        $school_id = $this->session->userdata('school_id');
        $teacher_id = $this->session->userdata('teacher_id');
        $weeklist = $this->config->item('days_list');

        foreach ($weeklist as $day => $val) {
            $data[$day] = array();
            $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "' and teacher_id = '" . $teacher_id . "' and day_id = '" . $day . "' order by day_id ASC, period_start_time ASC ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $count = 0;

                foreach ($result as $row) {
                    $data[$day][] = $row;
                    $count++;
                }
            }
        }
        return $data;
    }

    function update_note_publish_data() {
        $today_date = $this->session->userdata('query_date');
        $today_date_time_start = $today_date . ' 00:00:00';
        $day_start = strtotime($today_date_time_start);
        $today_date_time_end = $today_date . ' 23:59:59';
        $day_end = strtotime($today_date_time_end);

        $sql = "UPDATE " . TBL_NOTE . " set publish_data = 1 WHERE school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $this->input->post('class_id') . "' and section_id = '" . $this->input->post('section_id') . "' and period_id = '" . $this->input->post('period_id') . "' and note_issuetime >='" . $day_start . "' and  note_issuetime <='" . $day_end . "'";
        $query = $this->db->query($sql);
        return 1;
    }

    function update_note_publish_data_home() {
        $today_date = $this->session->userdata('query_date');
        $today_date_time_start = $today_date . ' 00:00:00';
        $day_start = strtotime($today_date_time_start);
        $today_date_time_end = $today_date . ' 23:59:59';
        $day_end = strtotime($today_date_time_end);

        $sql = "UPDATE " . TBL_HOME_NOTE . " set publish_data = 1 WHERE school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $this->input->post('class_id') . "' and section_id = '" . $this->input->post('section_id') . "' and period_id = '" . $this->input->post('period_id') . "' and note_issuetime >='" . $day_start . "' and  note_issuetime <='" . $day_end . "'";
        $query = $this->db->query($sql);
        return 1;
    }

    function update_note_publish_data_assignment() {
        $today_date = $this->session->userdata('query_date');
        $today_date_time_start = $today_date . ' 00:00:00';
        $day_start = strtotime($today_date_time_start);
        $today_date_time_end = $today_date . ' 23:59:59';
        $day_end = strtotime($today_date_time_end);

        $sql = "UPDATE " . TBL_ASSIGNMENT_NOTE . " set publish_data = 1 WHERE school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $this->input->post('class_id') . "' and section_id = '" . $this->input->post('section_id') . "' and period_id = '" . $this->input->post('period_id') . "' and note_issuetime >='" . $day_start . "' and  note_issuetime <='" . $day_end . "'";
        $query = $this->db->query($sql);
        return 1;
    }

    function get_noice_for_teacher() {
        $result = array();
        $sql = 'SELECT * FROM ' . TBL_NOTICE . ' WHERE school_id = ' . $this->session->userdata('school_id') . ' and type = 1 and ' . date('Y-m-d') . ' >= publish_date ';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function get_school_notice_detail($notice_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_NOTICE . " WHERE id = '" . $notice_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;

        foreach ($result as $res) {

            $data['notice_data'] = $res;
            $sql1 = "SELECT * FROM " . TBL_NOTICE_FILES . " WHERE notice_id = '" . $res['id'] . "' ";
            $query1 = $this->db->query($sql1);
            $result1 = $query1->result_array();
            foreach ($result1 as $row1) {
                $data['notice_file_data'][] = $row1;
            }

            $count++;
        }

        return $data;
    }

    function get_syllabus_classwise($class_id) {
        $data = array();
        $sql1 = 'SELECT * FROM ' . TBL_SYLLABUS . ' where school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_SYLLABUS_FILES . ' where syllabus_id = "' . $row['id'] . '" ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['syllabus'] = $row;
            $data['syllabus_file'] = $result2;
        }

        return $data;
    }

    function get_student_detail($student_id) {
        $result = array();
        $sql = "SELECT t_s_d.*,t_s.* from " . TBL_STUDENT . " t_s JOIN " . TBL_STUDENT_DETAIL . " t_s_d on t_s.id = t_s_d.student_id WHERE t_s.id = '" . $student_id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function get_diary_detail($diary_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE id = '" . $diary_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();


        return $result;
    }

    function get_school_calender($school_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_HOLIDAY . " WHERE school_id = '" . $school_id . "' order by start_date ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;
        foreach ($result as $res) {
            $break_start_date = explode('-', $res['start_date']);
            $year = $break_start_date[0];
            $monthe = $break_start_date[1];
            $day = $break_start_date[2];
            $data[$res['start_date']][$count] = $res;
            $count++;
        }

        return $data;
    }

    function get_student_attendance_list() {
        //echo '<pre>';print_r($_POST);die;
        $data = array();
        $data_a = array();
        $data_b = array();
        $school_id = $this->session->userdata('school_id');
        $search = '';
        if ($this->input->post('class_id')) {
            $search .= ' and t_a.class_id = ' . $this->input->post('class_id') . '';
        }
        if ($this->input->post('section_id')) {
            $search .= ' and t_a.section_id = ' . $this->input->post('section_id') . '';
        }
        if ($this->input->post('roll_no')) {
            $search .= ' and t_s.roll_no = ' . $this->input->post('roll_no') . '';
        }
        if ($this->input->post('student_name')) {
            $search .= ' and t_s.name like "%' . $this->input->post('student_name') . '%"';
        }
        if ($this->input->post('start_date')) {
            $search .= ' and t_a.attendance_date >= "' . $this->input->post('start_date') . '"';
            $start_date = array('att_start_date' => $this->input->post('start_date'));
            $this->session->set_userdata($start_date);
        } else {
            $start_date = array('att_start_date');
            $this->session->unset_userdata($start_date);
        }

        if ($this->input->post('end_date')) {
            $search .= ' and t_a.attendance_date <= "' . $this->input->post('end_date') . '"';
            $end_date = array('att_end_date' => $this->input->post('end_date'));
            $this->session->set_userdata($end_date);
        } else {
            $end_date = array('att_end_date');
            $this->session->unset_userdata($end_date);
        }

        $sql = "SELECT t_a.*,t_s.name FROM " . TBL_ATTENDANCE . " t_a JOIN " . TBL_STUDENT . " t_s on t_a.student_id = t_s.id  WHERE t_a.school_id = " . $school_id . "  " . $search;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $count = 0;
            foreach ($result as $row) {//echo "<pre>";print_r($row);
                $data[$row['student_id']][$count] = $row;

                $count++;
            }
            //echo "<pre>";print_r($data);
            foreach ($data as $student_id => $attendance_data_set) {
                $present = 0;
                $class = 0;
                foreach ($attendance_data_set as $attend) {
                    $class++;
                    if ($attend['attendance_status'] == PRESENT) {
                        $present++;
                    }
                    $data_a[$student_id]['total_present'] = $present;
                    $data_a[$student_id]['total_class'] = $class;
                }
            }
            foreach ($data_a as $student_id => $rec) {
                $avg = $rec['total_present'] / $rec['total_class'] * 100;
                $data_b[$student_id]['total_present'] = $rec['total_present'];
                $data_b[$student_id]['total_class'] = $rec['total_class'];
                $data_b[$student_id]['percentage'] = $avg;
            }
            //echo "<pre>";print_r($data_b);die;
        }
        return $data_b;
    }

    function get_attendance_detail($student_id) {
        $start_date = '';
        $end_date = '';
        $search = '';
        if ($this->session->userdata('att_start_date') && $this->session->userdata('att_start_date') != '') {
            $start_date = $this->session->userdata('att_start_date');
            $search .= ' and attendance_date >="' . $start_date . '"';
        }
        if ($this->session->userdata('att_end_date') && $this->session->userdata('att_end_date') != '') {
            $end_date = $this->session->userdata('att_end_date');
            $search .= ' and attendance_date <="' . $end_date . '"';
        }
        $school_id = $this->session->userdata('school_id');

        $return_data = array();
        $count = 0;

        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and student_id = '" . $student_id . "' " . $search . " ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $res) {
                $data[$res['attendance_date']][] = $res;
            }
            foreach ($data as $attendance_date => $attendance_data) {
                $total_absent = 0;
                $total_present = 0;
                $total_attendance = count($attendance_data);
                $explode_date = explode('-', $attendance_date);
                $day = abs($explode_date[2]);
                $month = abs($explode_date[1]);
                $year = abs($explode_date[0]);
                foreach ($attendance_data as $atten_data) {
                    if ($atten_data['attendance_status'] == PRESENT) {
                        $total_present++;
                    }
                    if ($atten_data['attendance_status'] == ABSENT) {
                        $total_absent++;
                    }
                }
                if ($total_attendance == $total_present) {
                    //$class = 'present_class';
                    $class = 1;
                } else if ($total_attendance == $total_absent) {
                    //$class = 'absent_class';
                    $class = 2;
                } else {
                    //$class = 'pertial_present_class';
                    $class = 3;
                }
                $return_data[$year][$month][$day]['attendance_class'] = $class;
                $return_data[$year][$month][$day]['attendance_date'] = $attendance_date;
                $count++;
            }
        }

        //echo "<pre>";print_r($return_data);die;
        return $return_data;
    }

    function get_teacher_dashboard_classes() {
        $result = array();
        $today = date('Y-m-d');
        $day_num = date('N', strtotime($today));
        $current_time = date('H:i:s');
        $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and day_id = '" . $day_num . "' and teacher_id = '" . $this->session->userdata('teacher_id') . "' and period_start_time <= '" . $current_time . "' and period_end_time >= '" . $current_time . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        } else {
            $sql1 = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and day_id = '" . $day_num . "' and teacher_id = '" . $this->session->userdata('teacher_id') . "' and period_start_time >= '" . $current_time . "' Order by period_start_time ASC LIMIT 0,1";
            $query1 = $this->db->query($sql1);
            if ($query1->num_rows() > 0) {
                $result = $query1->result_array();
            }
        }
        return $result;
    }

    function get_weekly_classes() {

        $today = date('Y-m-d');
        $day_num = date('N', strtotime($today));
        $current_time = date('H:i:s');
        $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and (day_id < '" . $day_num . "' OR (day_id = '" . $day_num . "' and period_start_time < '" . $current_time . "' )) and teacher_id = '" . $this->session->userdata('teacher_id') . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result['taken_classes'] = $query->num_rows();
        }
        $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and (day_id > '" . $day_num . "' OR (day_id = '" . $day_num . "' and period_start_time > '" . $current_time . "')) and teacher_id = '" . $this->session->userdata('teacher_id') . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result['pending_classes'] = $query->num_rows();
        }
        return $result;
    }

    function get_three_months_activity_data() {
        $classwork_data = array();
        $homework_data = array();
        $assignment_data = array();
        $data = array();
        $last_three_month = date(strtotime('today - 90 days'));
        $sql = "SELECT * FROM " . TBL_NOTE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $last_three_month . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $classwork_data = $query->result_array();
        }

        $sql1 = "SELECT * FROM " . TBL_HOME_NOTE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $last_three_month . "'";
        $query1 = $this->db->query($sql1);
        if ($query1->num_rows() > 0) {
            $homework_data = $query1->result_array();
        }

        $sql2 = "SELECT * FROM " . TBL_ASSIGNMENT_NOTE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $last_three_month . "'";
        $query2 = $this->db->query($sql2);
        if ($query2->num_rows() > 0) {
            $assignment_data = $query2->result_array();
        }

        $total_data = array_merge($classwork_data, $homework_data, $assignment_data);
        //echo "<pre>";print_r($total_data);die;
        for ($i = 1; $i < 8; $i++) {
            $data[$i] = 0;
        }

        foreach ($total_data as $row) {
            $activity_date = $row['note_issuetime'];
            $day_num = date('N', $activity_date);
            $data[$day_num] +=1;
        }
        return $data;
    }

    function get_last_seven_days_activity_data() {
        $data = array();
        $first_dat_of_week = date("Y-m-d", strtotime('monday this week'));
        $today = date('Y-m-d');
        $class_list_data = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE, ' and school_id = "' . $this->session->userdata('school_id') . '" and teacher_id = "' . $this->session->userdata('teachjer_id') . '" group by class_id,section_id');

        foreach ($class_list_data as $class_list) {//echo "<pre>";print_r($class_list);
            for ($i = $first_dat_of_week; $i <= $today; $i++) {
                $q_s_date = strtotime($i . ' 00:00:00');
                $q_e_date = strtotime($i . ' 23:59:59');
                $counter = 0;
                $counter += $this->my_custom_functions->get_perticular_count(TBL_NOTE, ' and school_id = "' . $this->session->userdata('school_id') . '" and note_issuetime >="' . $q_s_date . '" and note_issuetime <= "' . $q_e_date . '" and class_id = "'.$class_list['class_id'].'" and section_id = "'.$class_list['section_id'].'"');
                $counter += $this->my_custom_functions->get_perticular_count(TBL_HOME_NOTE, ' and school_id = "' . $this->session->userdata('school_id') . '" and note_issuetime >="' . $q_s_date . '" and note_issuetime <= "' . $q_e_date . '" and class_id = "'.$class_list['class_id'].'" and section_id = "'.$class_list['section_id'].'"');
                $counter += $this->my_custom_functions->get_perticular_count(TBL_ASSIGNMENT_NOTE, ' and school_id = "' . $this->session->userdata('school_id') . '" and note_issuetime >="' . $q_s_date . '" and note_issuetime <= "' . $q_e_date . '" and class_id = "'.$class_list['class_id'].'" and section_id = "'.$class_list['section_id'].'"');
                //echo $i." - ".$counter;echo "<br>";
                if ($counter == 0) {
                    $data[$i][]= array(
                        'class_id' => $class_list['class_id'],
                        'section_id' => $class_list['section_id']
                    );
                           
                }
            }
        }
//        echo "<pre>";print_r($data);
//        die;
        return $data;
        
    }

}
