<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function login_check() {

        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username = '" . $username . "' AND status=1";
        $query = $this->db->query($sql);
        
        $password = $this->input->post('password');
        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
             
            if (password_verify($password, $record['password'])) {
                
                
                if ($record['type'] == TEACHER) {
                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $record["id"] . '"');
                    $session_data = array(
                        "teacher_id" => $record["id"],
                        "school_id" => $school_id,
                        "school_is_logged_in" => 1
                    );
                    $this->session->set_userdata($session_data);
                    return TEACHER;
                } else if ($record['type'] == PARENTS) {
                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id = "' . $record["id"] . '"');
                    $session_data = array(
                        "parent_id" => $record["id"],
                        "school_id" => $school_id,
                        "parent_is_logged_in" => 1
                    );
                    $this->session->set_userdata($session_data);
                    //echo "<pre>";print_r($this->session->all_userdata());die;
                    return PARENTS;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function check_parent_existance($mobile_no) {
        $sql = "SELECT * from ".TBL_COMMON_LOGIN." where username = '".$mobile_no."' and type = ".PARENTS." and status = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id = "' . $record["id"] . '"');
            $session_data = array(
                        "parent_id" => $record["id"],
                        "school_id" => $school_id,
                        "school_is_logged_in" => 1
                    );
                    $this->session->set_userdata($session_data);
                    
                    
                    return $record["id"];
        }else{
            return 0;
        }
    }

}
