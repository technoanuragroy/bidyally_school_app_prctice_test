<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }
    
    function update_admin($admin_id) {
        
       
                $update_data = array(
                                    "name" => $this->input->post("name"),
                                    "phone" => $this->input->post("phone"),
                                    "status" => $this->input->post("status"),
                                    "email" => $this->input->post("email"),
                                    );
       
        
        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }
    

    function get_admin_data() {

        $sql = "Select * from ".TBL_ADMIN."";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_section_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageSections/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_subject_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageSubjects/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_teacher_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageTeachers/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_time_table_data($class_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        
        $day_list = $this->config->item('days_list');
        //$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        
        foreach ($day_list as $day => $val) {
            //foreach ($period_list as $ress) {
                //$create_data[$day][$ress['id']] = '';
            $counter = 0;
                foreach ($result as $routine) {
                    if ($day == $routine['day_id']) {
                        $create_data[$day][$counter] = $routine;
                    }

                $counter++;}
            //}
        }
        //echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_period_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/managePeriods/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function create_school($admin_id) {


        $update_data = array(
            "name" => $this->input->post("name"),
            "phone" => $this->input->post("phone"),
            "status" => $this->input->post("status"),
            "email" => $this->input->post("email"),
        );


        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }
    
    function get_school_data() {

        $sql = "Select * from ".TBL_SCHOOL."";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
