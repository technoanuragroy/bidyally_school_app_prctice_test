<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parent_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_child_list() {
        $student_id_list = '';
        $parent_id = $this->session->userdata('parent_id');
        $sql = "SELECT student_id from " . TBL_PARENT_KIDS_LINK . " where parent_id = '" . $parent_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();


        foreach ($result as $dchilds_id) {
            $student_id_list .= $dchilds_id['student_id'] . ',';
        }

        $student_id = rtrim($student_id_list, ',');
        $sql_a = "SELECT * from " . TBL_STUDENT . " where id IN(" . $student_id . ")";
        $query_a = $this->db->query($sql_a);
        $result_a = $query_a->result_array();
        //echo "<pre>";print_r($result_a);die;
        return $result_a;
    }

    function get_school_calender($school_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_HOLIDAY . " WHERE school_id = '" . $school_id . "' order by start_date ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;
        foreach ($result as $res) {
            $break_start_date = explode('-', $res['start_date']);
            $year = $break_start_date[0];
            $monthe = $break_start_date[1];
            $day = $break_start_date[2];
            $data[$res['start_date']][$count] = $res;
            $count++;
        }
        
        return $data;
    }

    function get_class_list_daywise() {
        $result = array();
        $data = array();
        //echo "<pre>";print_r($_POST);die;
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $i = 1;
        $j = 1;
        $count = 1;
        if($this->input->post('class_date')){
          $today = $this->input->post('class_date');
        }else{
        $today = date('Y-m-d');
        }
        $class_date_sess = array('class_date' => $today);
        $this->session->set_userdata($class_date_sess);
        $weekday = date('N', strtotime($today));
        $sql = "SELECT subject_id FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and day_id = '" . $weekday . "' and subject_id != 0 order by id ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $row) {
                if ($i <= 6) {
                    $data['structured'][$j][$count] = $row;

                    if ($i % 2 == 0) {
                        $j++;
                        $count = 0;
                    }

                    $i++;
                    $count++;
                } else {
                    $data['unstructured'][$j][$count] = $row;
                    if ($i % 2 == 0) {
                        $j++;
                        $count = 0;
                    }

                    $i++;
                    $count++;
                }
            }
        }
//        echo "<pre>";
//            print_r($data); die;

        return $data;
    }

    function get_class_note_detail($subject_id) {
        $result = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
       
        $today = $this->session->userdata('class_date');
        $custom_start_date_time = $today.' 00:00:00';
        $custom_end_date_time = $today.' 23:59:59';
        $beginOfDay = strtotime($custom_start_date_time);
        $endOfDay = strtotime($custom_end_date_time);
        
//        $beginOfDay = strtotime("midnight", $today);
//        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;
        $sql = "SELECT t_n.*,t_t_t.subject_id from " . TBL_NOTE . " t_n join " . TBL_TIMETABLE . " t_t_t on t_n.period_id=t_t_t.id  WHERE t_n.school_id = '" . $school_id . "' and t_n.class_id = '" . $class_id . "' and t_n.section_id = '" . $section_id . "' and t_t_t.subject_id = '" . $subject_id . "' and note_issuetime >= " . $beginOfDay . " and note_issuetime <= " . $endOfDay . " and t_n.publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result_array();
                $data['notes'] = $row;
                $data['note_file'] = $result2;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    function get_home_note_detail($subject_id) {
        $result = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $today = strtotime($this->session->userdata('class_date'));
        $beginOfDay = strtotime("midnight", $today);
        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;
        $sql = "SELECT t_n.*,t_t_t.subject_id from " . TBL_HOME_NOTE . " t_n join " . TBL_TIMETABLE . " t_t_t on t_n.period_id=t_t_t.id  WHERE t_n.school_id = '" . $school_id . "' and t_n.class_id = '" . $class_id . "' and t_n.section_id = '" . $section_id . "' and t_t_t.subject_id = '" . $subject_id . "' and note_issuetime > " . $beginOfDay . " and note_issuetime < " . $endOfDay . " and t_n.publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $sql2 = 'SELECT * FROM ' . TBL_HOME_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result_array();
                $data['notes'] = $row;
                $data['note_file'] = $result2;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    function get_assignment_note_detail($subject_id) {
        $result = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $today = strtotime($this->session->userdata('class_date'));
        $beginOfDay = strtotime("midnight", $today);
        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;
        $sql = "SELECT t_n.*,t_t_t.subject_id from " . TBL_ASSIGNMENT_NOTE . " t_n join " . TBL_TIMETABLE . " t_t_t on t_n.period_id=t_t_t.id  WHERE t_n.school_id = '" . $school_id . "' and t_n.class_id = '" . $class_id . "' and t_n.section_id = '" . $section_id . "' and t_t_t.subject_id = '" . $subject_id . "' and note_issuetime > " . $beginOfDay . " and note_issuetime < " . $endOfDay . " and t_n.publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $sql2 = 'SELECT * FROM ' . TBL_ASSIGNMENT_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result_array();
                $data['notes'] = $row;
                $data['note_file'] = $result2;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    function get_attendance_data() {
        $result = array();
        $data = array();
        $data_a = array();
        $data_b = array();
        
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $student_id = $this->session->userdata('student_id');

        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and student_id = '" . $student_id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            $count = 0;
            foreach ($result as $row) {

                $attendance_date = explode('-', $row['attendance_date']);
                $month = $attendance_date[1];
                $data[$month][$count] = $row;

                $count++;
            }
            foreach ($data as $month => $attendance_data_set) {
                $present = 0;
                $class = 0;
                foreach ($attendance_data_set as $attend) {
                    $class++;
                    if ($attend['attendance_status'] == PRESENT) {
                        $present++;
                    }
                    $data_a[$month]['total_present'] = $present;
                    $data_a[$month]['total_class'] = $class;
                }
            }
            foreach ($data_a as $month => $rec) {
            $avg = $rec['total_present'] / $rec['total_class'] * 100;
            $data_b[$month]['total_present'] = $rec['total_present'];
            $data_b[$month]['total_class'] = $rec['total_class'];
            $data_b[$month]['percentage'] = $avg;
        }
        return $data_b;
        }

        
    }

    function get_attendance_detail($month, $year) {
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $student_id = $this->session->userdata('student_id');
        $return_data = array();
        $count = 0;

        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and student_id = '" . $student_id . "' and month(attendance_date)='" . $month . "' and year(attendance_date)='" . $year . "' and publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $res) {
                $data[$res['attendance_date']][] = $res;
            }
            foreach ($data as $attendance_date => $attendance_data) {
                $total_absent = 0;
                $total_present = 0;
                $total_attendance = count($attendance_data);
                $explode_date = explode('-', $attendance_date);
                $day = abs($explode_date[2]);
                foreach ($attendance_data as $atten_data) {
                    if ($atten_data['attendance_status'] == PRESENT) {
                        $total_present++;
                    }
                    if ($atten_data['attendance_status'] == ABSENT) {
                        $total_absent++;
                    }
                }
                if ($total_attendance == $total_present) {
                    //$class = 'present_class';
                    $class = 1;
                } else if ($total_attendance == $total_absent) {
                    //$class = 'absent_class';
                    $class = 2;
                } else {
                    //$class = 'pertial_present_class';
                    $class = 3;
                }
                $return_data[$day]['attendance_class'] = $class;
                $return_data[$day]['attendance_date'] = $attendance_date;
                $count++;
            }
        }

        //echo "<pre>";print_r($return_data);die;
        return $return_data;
    }

    function get_teacher_weekly_schedule() {

        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $weeklist = $this->config->item('days_list');

        foreach ($weeklist as $day => $val) {
            $data[$day] = array();
            $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and day_id = '" . $day . "' order by day_id ASC, period_start_time ASC ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $count = 0;

                foreach ($result as $row) {
                    $data[$day][] = $row;
                    $count++;
                }
            }
        }

        return $data;
    }

    function get_general_info() {
        $sql = "SELECT * FROM " . TBL_GENERAL_INFO . " ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function get_school_notice($school_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_NOTICE . " WHERE school_id = '" . $school_id . "' and type IN(2,3) and ".date('Y-m-d')." >=publish_date order by issue_date DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //$count = 0;

//        foreach ($result as $res) {
//            $break_start_date = explode('-', $res['issue_date']);
//            $year = $break_start_date[0];
//            $monthe = $break_start_date[1];
//            $day = $break_start_date[2];
//            $data[$res['id']]['notice_data'] = $res;
//            $sql1 = "SELECT * FROM " . TBL_NOTICE_FILES . " WHERE notice_id = '" . $res['id'] . "' ";
//            $query1 = $this->db->query($sql1);
//            $result1 = $query1->result_array();
//            foreach($result1 as $row1){
//              $data[$row1['notice_id']]['notice_file_data'] = $row1; 
//            }
//
//            $count++;
//        }
        //echo "<pre>";print_r($result);
        return $result;
    }

    function get_school_notice_detail($notice_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_NOTICE . " WHERE id = '" . $notice_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;

        foreach ($result as $res) {

            $data['notice_data'] = $res;
            $sql1 = "SELECT * FROM " . TBL_NOTICE_FILES . " WHERE notice_id = '" . $res['id'] . "' ";
            $query1 = $this->db->query($sql1);
            $result1 = $query1->result_array();
            foreach ($result1 as $row1) {
                $data['notice_file_data'][] = $row1;
            }

            $count++;
        }

        return $data;
    }
    
     function getDiaryList($school_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE school_id = '" . $school_id . "' and student_id = '".$this->session->userdata('student_id')."' order by issue_date DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        
        //echo "<pre>";print_r($result);
        return $result;
    }
    function get_diary_detail($diary_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE id = '" . $diary_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        

        return $result;
    }
    function get_fees_detail() {
        $result = array();
        $data = array();
        $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and class_id = '".$this->session->userdata('class_id')."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
        $result = $query->result_array();
        $fees_list_arr = json_decode($result[0]['fees_structure'],true);
        $data['id'] = $result[0]['id'];
        $data['school_id'] = $result[0]['school_id'];
        $data['class_id'] = $result[0]['class_id'];
        $data['fees_strucure'] = $fees_list_arr;
        }
        return $data;
    }

}
