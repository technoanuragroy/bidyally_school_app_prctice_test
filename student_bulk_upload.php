<?php $this->load->view('school/_include/header'); ?>
<script type="text/javascript">
    function toggle_div() {
        $("#errors").toggle("slow");
    }
</script>
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Student Bulk Upload</h2>
    <div class="block">
        <div class="col-md-12">                        
            <?php 
                if(isset($counts)) {
                    if($counts != "") {
            ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $counts; ?></p>
                        </div>
                        <!-- END Success Alert -->
            <?php
                    }
                }
            ?>
                        
            <?php 
                if(isset($err_msg)) {
                    if($err_msg != "") {
            ?>     
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $err_msg; ?></p>
                            <div style="display: none;" id="errors">
                                <?php echo $error; ?>
                            </div>
                        </div>
                        <!-- END Danger Alert -->
            <?php
                    }
                }
            ?>            
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <select required class="form-control" id="semester" name="semester">
                                        <option value="">Select Semester</option>
                                        <?php foreach ($semester_list as $sem) { ?>
                                                    <option value="<?php echo $sem['id']; ?>"><?php echo $sem['semester_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="semester">Semester</label>
                                </div>
                            </div>

                            <div class="form-group row">
        <!--                    <small style="color:#777;font-weight: bold;font-size: 14px;"></small><br>-->                        
                                <label class="col-12" for="adminphoto" style="color:#777;font-weight: bold;font-size: 14px;">Please click on the icon below to download a sample Excel template.</label><br>
                                <div class="col-12">
                                    <a href="<?php echo base_url(); ?>csv/Student+Database.xlsx"><img src="<?php echo base_url(); ?>app_images/XLS.png" width="80px;"></a>
        <!--                        <a href="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/Student+Database.xlsx"><img src="<?php echo base_url(); ?>app_images/XLS.png" width="80px;"></a>-->
                                </div>                       
                            </div>

                            <div class="form-group row">
                                <label class="col-12" for="adminphoto">Upload File</label>
                                <div class="col-12">
                                    <input type="file" id="adminphoto" name="adminphoto"><br>
                                    <span class="inst"> you are allowed to upload only .xls file </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                            </div>
                                                                                           
                    <?php echo form_close(); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->


<!-- END Footer -->

<?php $this->load->view('school/_include/footer'); ?>


