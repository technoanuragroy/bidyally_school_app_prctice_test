<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent:: __construct();

        if ($this->input->post("key") != API_KEY) {
            $encode[] = array(
                "error" => "Invalid Key"
            );
            $json_encode = json_encode($encode);
            print_r($json_encode);
            die();
        }

        $this->load->model("Api_model");
    }

    ////////////////////////////////////////////////////////////////////////////
    // Login
    ////////////////////////////////////////////////////////////////////////////
    function authentication() {
        
        if ($this->input->post("username") != "" && $this->input->post("password") != "") {
            
            $request_data = $this->input->post();
            unset($request_data['password']);

            $table = TBL_API_LOG;
            $req_data = json_encode($request_data);

            $insert_data = array(
                'function_name' => 'authentication',
                'receive_data' => $req_data,
                'transaction_time' => date('Y-m-d H:i:s')
            );
            $return_id = $this->my_custom_functions->insert_data_last_id($insert_data, $table);

            $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

            if ($chek_maintainance_mode['maintenance_mode'] == 0) {

                $response = $this->Api_model->user_verification();

                if (!empty($response)) {
                    $user_id = $this->my_custom_functions->ablEncrypt($response['id']);
                    $login_link = base_url() . 'app/app_login/' . $user_id;
                    $encode[] = array(
                        "msg" => "validation Success",
                        "user_id" => $response['id'],
                        "login_link" => $login_link,
                        "status" => "true"
                    );
                    $json_encode = json_encode($encode);
                    print_r($json_encode);

                    $data = array('response_data' => $json_encode, 'user_id' => $response['id']);
                    $where = array('id' => $return_id);
                    $this->my_custom_functions->update_data($data, $table, $where);
                } else {
                    $encode[] = array(
                        "msg" => "Invalid username and password",
                        "status" => "false"
                    );
                    $json_encode = json_encode($encode);
                    print_r($json_encode);

                    $data = array('response_data' => $json_encode);
                    $where = array('id' => $return_id);
                    $this->my_custom_functions->update_data($data, $table, $where);
                }
            } else {
                $encode[] = array(
                    "msg" => $chek_maintainance_mode['maintenance_message'],
                    "status" => "false"
                );
                $json_encode = json_encode($encode);
                print_r($json_encode);
                $data = array('response_data' => $json_encode);
                $where = array('id' => $return_id);
                $this->my_custom_functions->update_data($data, $table, $where);
                die();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Register Device Api
    ////////////////////////////////////////////////////////////////////////////
    function register_device() {

        if ($this->input->post("user_id") != "" && $this->input->post("device_id") != "" && $this->input->post("device_type") != "") {
//            $message = '';
//            $from_email = SITE_EMAIL;
//            $to_email = 'biplob@ablion.in';
//            $email_subject = 'hit register device';
//            $message .= " user id - ".$this->input->post("user_id");
//            $message .= " device id - ".$this->input->post("device_id");
//            $message .= " device type - ".$this->input->post("device_type");
//            $this->my_custom_functions->SendEmail($from_email . ',' . SITE_EMAIL, $to_email, SITE_NAME . ":" . $email_subject, $message);
            $request_data = $this->input->post();
            $table = TBL_API_LOG;
            $req_data = json_encode($request_data);

            $insert_data = array(
                'function_name' => 'register_device',
                'receive_data' => $req_data,
                'transaction_time' => date('Y-m-d H:i:s')
            );
            $return_id = $this->my_custom_functions->insert_data_last_id($insert_data, $table);




            $validation_flag = $this->my_custom_functions->user_validation($this->input->post("user_id"));
            if ($validation_flag == 1) {
                $data = array(
                    'user_id' => $this->input->post("user_id"),
                    'device_id' => $this->input->post("device_id"),
                    'device_type' => $this->input->post("device_type"),
                    'last_update' => date('Y-m-d H:i:s')
                );

                $attendance_id = $this->Api_model->register_device($data);
                $encode[] = array(
                    "msg" => "Device Successfully Registered",
                    "status" => "true"
                );
                $json_encode = json_encode($encode);
                print_r($json_encode);
            } else {

                $encode[] = array(
                    "msg" => "User is inactive",
                    "status" => "false"
                );
                $json_encode = json_encode($encode);
                print_r($json_encode);
            }
        } else {
            $encode[] = array(
                "msg" => "Validation Failed",
                "status" => "false"
            );
            $json_encode = json_encode($encode);
            print_r($json_encode);
        }

        $data = array('response_data' => $json_encode, 'user_id' => $this->input->post("user_id"));
        $where = array('id' => $return_id);
        $this->my_custom_functions->update_data($data, $table, $where);
    }

}
