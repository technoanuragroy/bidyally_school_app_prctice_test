<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Enrollment extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_is_logged_in')) {
            $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

            if ($chek_maintainance_mode['maintenance_mode'] == 1) {
                $this->session->set_flashdata("maintainance_message", $chek_maintainance_mode['maintenance_message']);
                redirect("school/user/logout");
            }
        }
        $this->my_custom_functions->check_school_security();
        $this->load->model("school/School_enrollment_model");
        $this->load->library('Classes/PHPExcel');
        $user_type = $this->session->userdata('usertype');
        if ($user_type == 2) {
            $this->my_custom_functions->check_permission();
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Enroll students to a semester
    ////////////////////////////////////////////////////////////////////////////
    function enrollStudent() {
        
        $data['page_title'] = 'Enroll Students';
        
        $data['sessions'] = $this->my_custom_functions->get_all_sessions_of_a_school($this->session->userdata('school_id'));  
        $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));        
        
        if($this->input->post('student')) {
            
            $this->School_enrollment_model->enroll_students();
            
            $this->session->set_flashdata("s_message", 'Students successfully enrolled');
            redirect("school/enrollment/enrollStudent");
                                 
        } else if($this->input->post('submit')) {
            
            $semester_id = $this->input->post("semester_id");
            $section_id = $this->input->post("section_id");
                        
            $data['students'] = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id'), "semester_id" => $semester_id, "section_id" => $section_id));                           
            
            if($this->input->post('semester_id') == 'unenroll'){
             $data['students'] = $this->my_custom_functions->get_unenrolled_student_list();   
            }
            
            
            $this->load->view('school/enroll_students', $data);
            
        } else {
            
            $data['students'] = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id')));
            $this->load->view('school/enroll_students', $data);
        }                      
    }   
    
    ////////////////////////////////////////////////////////////////////////////
    // Update enrollment data of a student
    ////////////////////////////////////////////////////////////////////////////
    function update_enrollment_data_ajax() {
        
        $update_data = array();     
        $key = $this->input->post('key');
        $enrollment_id = $this->input->post("enrollment");
        
        $key_array = explode("_", $key);
        $semester_id = $key_array[0];
        $student_id = $key_array[1];
        
        $enrollment_details = $this->my_custom_functions->get_details_from_id($enrollment_id, TBL_STUDENT_ENROLLMENT);
        
        if($this->input->post("section")) {
            
            $update_data = array(
                'section_id' => $this->input->post("section")
            );
            
        } else if($this->input->post("rollno")) {
            
            $update_data = array(
                'roll_no' => $this->input->post("rollno")
            );
        }
        
        $response = '';
        if(!empty($update_data)) {
            if(!empty($enrollment_details)) {
                if($enrollment_details['student_id'] == $student_id AND $enrollment_details['semester_id'] == $semester_id) {
            
                    $condition = array('id' => $enrollment_id);
                    $this->my_custom_functions->update_data($update_data, TBL_STUDENT_ENROLLMENT, $condition);
                    
                    if($this->input->post("section")) {
            
                        $section_id = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_ENROLLMENT, 'section_id', ' and id="' . $enrollment_id . '" ');
                        $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id="' . $section_id . '" ');
                        $response = $section_name;

                    } else if($this->input->post("rollno")) {

                        $roll_no = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_ENROLLMENT, 'roll_no', ' and id="' . $enrollment_id . '" ');
                        $response = $roll_no;                        
                    }
                }
            }
        }   
        
        echo $response;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Unenroll a student
    ////////////////////////////////////////////////////////////////////////////
    function unenroll_ajax() {
        
        $key = $this->input->post('key');
        $enrollment_id = $this->input->post('enrollment');
        
        $key_array = explode("_", $key);
        $semester_id = $key_array[0];
        $student_id = $key_array[1];
                
        $enrollment_details = $this->my_custom_functions->get_details_from_id($enrollment_id, TBL_STUDENT_ENROLLMENT);
        
        $response = '';
        if(!empty($enrollment_details)) {
            if($enrollment_details['student_id'] == $student_id AND $enrollment_details['semester_id'] == $semester_id) {
                
                $condition = array('id' => $enrollment_id);
                $this->my_custom_functions->delete_data(TBL_STUDENT_ENROLLMENT, $condition);
                
                $response = '1';                
            }
        }
        
        echo $response;
    }
    
    
    
}
