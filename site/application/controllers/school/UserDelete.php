<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserDelete extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->uri->segment(3) != CRON_KEY) {
            die();
        }

        //parent::__construct();
        //$this->my_custom_functions->check_school_security();
        $this->load->model("school/School_delete_model");
    }

    ////////////////////////////////////////////////////////////////////////////
    /////  Cron will run once in every day that will delete School and students, 
    /////  parents and all informative data related to school
    //     http://server1/webdev/Bidyaaly/site/cron/deleteSchool/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////
    function deleteSchool() {
        $school_list = array();
        $school_list = $this->School_delete_model->get_school_list_for_delete();
        //echo "<pre>";print_r($school_list);die;
        if (!empty($school_list)) {
            foreach ($school_list as $school) {
                if ($school['file_url'] != '') {
                    $file_url = $school['file_url'];
                    $data = array('file_url' => $file_url);

                    $insert_data = $this->my_custom_functions->insert_data($data, TBL_DELETE_FILES);
                }

                $notice = $this->School_delete_model->delete_school_notice($school['id']);
                $syllabus = $this->School_delete_model->delete_school_syllabus($school['id']);
                $delete_records = $this->School_delete_model->delete_school_records($school['id']);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /////  Cron will run once in every day that will delete Notes
    //     http://server1/webdev/Bidyaaly/site/cron/deleteNotes/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////
    function deleteNotes() {
        $note = array();
        $note = $this->School_delete_model->get_note_for_delete();

        if (!empty($note)) {
            foreach ($note as $notes) {
                $school_id = $notes['school_id'];
                $note_files = $this->School_delete_model->get_note_files_for_delete($notes['id']);
                if (!empty($note_files)) {
                    foreach ($note_files as $note_files_data) {
                        $file_data = array('file_url' => $note_files_data['file_url']);
                        $this->my_custom_functions->insert_data($file_data, TBL_DELETE_FILES);
                        $del_condition = array('id' => $note_files_data['id']);
                        $this->my_custom_functions->delete_data(TBL_NOTE_FILES, $del_condition);
                    }
                }
            }
            $del_note_condition = array('is_deleted' => 1, 'school_id' => $school_id);
            $this->my_custom_functions->delete_data(TBL_NOTE, $del_note_condition);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    /////  Cron will run once in every day that will delete students and parents
    //http://server1/webdev/Bidyaaly/site/cron/deleteStudent/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////

    function deleteStudent() {

        $student_list = array();
        $student_list = $this->School_delete_model->get_student_list_for_delete();
        
        if (!empty($student_list)) {
            foreach ($student_list as $student) {
                if ($student['file_url'] != '') {
                    //////////////////////////////  MOVE IMAGES URL /////////////////////////////

                    $insert_data = array(
                        'file_url' => $student['file_url'],
                    );
                    $table = TBL_DELETE_FILES;
                    $move_img_file = $this->School_delete_model->insert_file_data($insert_data, $table);
                }


                //////////////////////////////  PARENT DELETE/////////////////////////////

                $delete_parents = $this->deleteParents($student['id']);


                ///////////////////////////// DELETE ALL RECORDS START  ////////////////////////

                $delete_records = $this->School_delete_model->delete_student_records($student['id'], $student['is_deleted']);
            }
        }
    }

    function deleteParents($student_id) {
        $delete_parent = $this->School_delete_model->delete_parents($student_id);
        return $delete_parent;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    /////  Cron will run once in every day that will delete Teachers
    //     http://server1/webdev/Bidyaaly/site/cron/deleteTeacher/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////

    function deleteTeacher() {
        $teacher_list = array();
        $teacher_list = $this->School_delete_model->get_teacher_list_for_delete();
        //echo "<pre>";print_r($teacher_list);die;

        if (!empty($teacher_list)) {
            foreach ($teacher_list as $teacher) {
                //////////////////////////////  MOVE IMAGES URL /////////////////////////////
                if ($teacher['file_url'] != '') {
                    $insert_data = array(
                        'file_url' => $teacher['file_url'],
                    );
                    $table = TBL_DELETE_FILES;
                    $move_img_file = $this->School_delete_model->insert_file_data($insert_data, $table);
                }
                $delete_records = $this->School_delete_model->delete_teacher_records($teacher['id']);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /////  Cron will run once in every day that will delete note files, images
    //   http://server1/webdev/Bidyaaly/site/cron/file_deletion/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////
    function file_deletion() {
        $this->School_delete_model->delete_photos_and_files();
    }

}
