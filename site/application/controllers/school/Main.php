<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('admin_is_logged_in')) {
            $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

            if ($chek_maintainance_mode['maintenance_mode'] == 1) {
                $this->session->set_flashdata("maintainance_message", $chek_maintainance_mode['maintenance_message']);
            }
        }
        $this->load->model("school/School_main_model");
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // School login from app
    ////////////////////////////////////////////////////////////////////////////
    function appLogin() {
                        
        $encrypted_id = $this->uri->segment(4);
        $school_id = $this->my_custom_functions->ablDecrypt($encrypted_id);
        $login_details = $this->my_custom_functions->get_details_from_id($school_id, TBL_COMMON_LOGIN);
                       
        // Update api log if redirected from multiple login screen in app
        if($this->input->get('log')) {
            
            $this->session->set_userdata('multiple_login_redirect', $this->my_custom_functions->ablEncrypt('school/main/appLogin/' . $encrypted_id));
            
            $encrypted_apilog_id = $this->input->get('log');   
            $apilog_id = $this->my_custom_functions->ablDecrypt($encrypted_apilog_id);

            $data = array('user_id' => $school_id);
            $table = TBL_API_LOG;
            $where = array('id' => $apilog_id);
            $this->my_custom_functions->update_data($data, $table, $where);
        }
        
        if ($login_details['type'] == SCHOOL) { /// If credentials match with system                          
            
            $school_details = $this->my_custom_functions->get_details_from_id($school_id, TBL_SCHOOL);
                        
            // Get the current session of the school
            $session_selected = "";
            $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_school($school_id);
            
            if(!empty($school_sessions)) {
                foreach($school_sessions as $school_ses) {
                    
                    $today = strtotime(date("Y-m-d"));
                    if(strtotime($school_ses['from_date']) <= $today AND strtotime($school_ses['to_date']) >= $today) {
                        $session_selected = $school_ses['id'];
                    }
                }
                
                if($session_selected == "") {
                    $session_selected = $school_sessions[0]['id'];
                }
            }

            // Set school related session variables 
            $session_data = array(
                "usertype" => $login_details['type'],
                "school_id" => $school_id,
                "session_id" => $session_selected,
                "school_username" => $login_details["username"],
                "school_email" => $school_details["email_address"],
                "school_is_logged_in" => 1,
                "mobile" => 1
            );
            $this->session->set_userdata($session_data);

            // Update last login data
            $last_login_data = array(
                "last_login_time" => time()                            
            );
            $this->db->where('id', $school_id);
            $updated = $this->db->update(TBL_COMMON_LOGIN, $last_login_data);

            // Redirect according to otp verification status
            $otp_verified = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $school_id . '"');
            
            if($otp_verified == 1) {
                redirect('school/user/dashBoard');
            } else {
                redirect("school/main/userVerification");
            }   
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Teacher login from app
    ////////////////////////////////////////////////////////////////////////////
    public function appLoginTeacher() {
        
        $encrypted_id = $this->uri->segment(4);
        $teacher_id = $this->my_custom_functions->ablDecrypt($encrypted_id);
        $login_details = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_COMMON_LOGIN);
                                       
        if ($login_details['type'] == TEACHER) { /// If credentials match with system                          
            
            $teacher_details = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_TEACHER);
            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $teacher_id . '"');
                        
            // Get the current session of the school
            $session_selected = "";
            $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_school($school_id);
            
            if(!empty($school_sessions)) {
                foreach($school_sessions as $school_ses) {
                    
                    $today = strtotime(date("Y-m-d"));
                    if(strtotime($school_ses['from_date']) <= $today AND strtotime($school_ses['to_date']) >= $today) {
                        $session_selected = $school_ses['id'];
                    }
                }
                
                if($session_selected == "") {
                    $session_selected = $school_sessions[0]['id'];
                }
            }

            // Set school related session variables 
            $session_data = array(                                
                "usertype" => $login_details['type'],
                "school_id" => $school_id,
                "session_id" => $session_selected,
                "teacher_id" => $teacher_id,
                "school_username" => $login_details["username"],
                "school_is_logged_in" => 1,
                "mobile" => 1
            );
            $this->session->set_userdata($session_data);

            // Update last login data
            $last_login_data = array(
                "last_login_time" => time()                            
            );
            $this->db->where('id', $teacher_id);
            $updated = $this->db->update(TBL_COMMON_LOGIN, $last_login_data);

            // Redirect according to otp verification status
            $otp_verified = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $school_id . '"');
            
            if($otp_verified == 1) {
                redirect('school/user/dashBoard');
            } else {
                redirect("school/main/userVerification");
            }   
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    
    public function index() {
        
        if ($this->session->userdata("school_id")) {
            
            $otp_verified = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $this->session->userdata("school_id") . '"');
            
            if($otp_verified == 1) {
                redirect('school/user/dashBoard');
            } else {
                redirect("school/main/userVerification");
            }            
        } else {
            
            $this->load->view('school/login');
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    
    public function signUp() {
        
        if ($this->session->userdata("school_id")) {
            
            $otp_verified = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $this->session->userdata("school_id") . '"');
            
            if($otp_verified == 1) {
                redirect('school/user/dashBoard');
            } else {
                redirect("school/main/userVerification");
            }            
        } else {
                
            $this->load->view('school/sign_up');
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function usernameValidation() {
              
        if(strlen(trim($this->input->post('username'))) <> 10) {
            
            $return['msg'] = '<span class="username_error">Please enter a valid mobile number.</span>';
            $return['error'] = 1;
        } else {
                     
            $uservalidation = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username="' . trim($this->input->post('username')) . '" and type="' . SCHOOL . '"');

            $return = array();

            if ($this->input->post('username') == trim($this->input->post('username')) AND strpos($this->input->post('username'), ' ') !== false) {

                $return['msg'] = '<span class="username_error">Please enter a valid mobile number</span>';
                $return['error'] = 1;
            } else {
                if ($uservalidation > 0) {

                    $return['msg'] = '<span class="username_error">Mobile number already exists</span>';
                    $return['error'] = 1;
                } else {

                    $return['msg'] = '<span class="username_success">Mobile number available</span>';
                    $return['error'] = 0;
                }
            }
        }

        echo json_encode($return);
    }
    
    ////////////////////////////////////////////////////////////////////////////

    public function createSchool() {
        
        // If school session found, redirect user to dashBoard
        if($this->session->userdata("school_id")) {
            
            $otp_verified = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $this->session->userdata("school_id") . '"');
            
            if($otp_verified == 1) {
                redirect('school/user/dashBoard');
            } else {
                redirect("school/main/userVerification");
            }     
        }
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {            

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("mobile_no", "Mobile Number", "trim|required|min_length[10]|max_length[10]");
            $this->form_validation->set_rules("password", "Password", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("contact_person_name", "Contact Person Name", "trim|required");            
            $this->form_validation->set_rules("email_address", "Email", "trim|required");            

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/main/signUp");
            } else {
                
                //$create_username = $this->my_custom_functions->create_username();
                $create_username = $this->input->post('mobile_no');

                $school_login_detail = array(
                    'type' => SCHOOL,
                    'username' => $create_username,
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => 1,
                );

                $school_id = $this->my_custom_functions->insert_data_last_id($school_login_detail, TBL_COMMON_LOGIN);
                //$school_id = 1;
                $license_exp_date = date('Y-m-d', strtotime("+" . DEFAULT_LICENSE_VALIDITY . " days"));

                $ip = $this->getUserIP();
                //$ip = '103.43.80.131';
                $country = $this->ipDetails($ip, "countryId");


                //$city = $this->ipDetails($ip, "city");
                //$state = $this->ipDetails($ip, "state");
                //$region = $this->ipDetails($ip, "region");
                $address = $this->ipDetails($ip, "address");
                $school_detail = array(
                    'id' => $school_id,
                    'name' => $this->input->post('name'),
                    'contact_person_name' => $this->input->post('contact_person_name'),
                    'mobile_no' => $this->input->post('mobile_no'),
                    'email_address' => $this->input->post('email_address'),
                    'ip_address' => $ip,
                    'country_id' => $country,
                    'address' => $address,
                    'no_of_license' => DEFAULT_LICENSE,
                    'license_expiry_date' => $license_exp_date,
                    'created_at' => time(),
                    'updated_at' => time()
                );
                //echo "<pre>";print_r($school_detail);die;
                $add_school_id = $this->my_custom_functions->insert_data_last_id($school_detail, TBL_SCHOOL);

                ////////////////////////// VERIFICATION TOKEN  ///////////////////////////////////////
                $token = $this->my_custom_functions->get_verification_token_code();
                $expiry_duration = '+' . VERIFICATION_EXPIRY_TIME . ' minutes';
                $varification_data = array(
                    'login_id' => $school_id,
                    'token' => $token,
                    'expiry_time' => date("Y-m-d H:i:s", strtotime($expiry_duration)),
                    'type' => 1 //1=email verification
                );
                $this->db->insert("tbl_verification_token", $varification_data);

                $verification_token = $token;
                $verify_link = base_url() . 'email_Verification/' . $verification_token;



                //////////////////////   SEND EMAIL TO SCHOOL ADMIN  //////////////////////////////////


                $email_address = $this->input->post('email_address');
                //$reset_link = base_url() . 'userResetPassword/' . $this->my_custom_functions->ablEncrypt($user_details['type']) . '/' . $this->my_custom_functions->ablEncrypt($user_details['id']);
                // Email template
                $email_data = $this->config->item("school_sign_up");
                $from_email = DONOT_REPLY_EMAIL;
                // Email variables
                $subject = $email_data['subject'];
                $addressing_user = $this->my_custom_functions->sprintf_email($email_data['addressing_user'], array(
                    'ContactPerson' => $this->input->post('contact_person_name')
                        )
                );
                $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                    'Organization_Name' => $this->input->post('name'),
                    'username' => $create_username,
                    'verify_link' => $verify_link
                        )
                );
                $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                    'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                        )
                );
                $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                    'institute_name' => 'Team Bidyaaly'
                        )
                );
                $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                    'systemtext' => SYSTEM_TEXT
                        )
                );
                $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                //echo $full_mail;die;
                //$this->my_custom_functions->SendNotification($school_id, array("subject" => $subject, "message" => $full_mail,"from" => $from_email), NOTIFICATION_TYPE_SCHOOL_REGISTRATION); // done notification
                $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $email_address, $subject, $full_mail);
                
                /////////////////////////////////////  REGISTRATION DETAIL SEND TO ADMIN  ///////////////////////////////////////////////////////
                
//                $admin_email_address = ADMIN_EMAIL;
//                $from_email = DONOT_REPLY_EMAIL;
//                
//                $email_data = $this->config->item("registration_detail");
//                
//                $admin_subject = $this->my_custom_functions->sprintf_email($email_data['subject'], array(
//                        'institute_name' => $this->input->post('name')
//                    )
//                );
//                
//                $admin_addressing_user = $email_data['addressing_user'];
//                
//                $admin_message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
//                        'institute_name' => $this->input->post('name'),
//                        'contact_person_name' => $this->input->post('contact_person_name'),
//                        'mobile_number' => $this->input->post('mobile_no'),
//                        'email_address' => $this->input->post('email_address'),
//                        'ip_address' => $ip,
//                        'address' => $address
//                    )
//                );
//                
//                $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
//                        'institute_name' => 'Team Bidyaaly'
//                    )
//                );
//                
//                $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
//                        'systemtext' => SYSTEM_TEXT
//                    )
//                );
//                $full_mail = $this->my_custom_functions->CreateSystemEmail($admin_addressing_user, $admin_message,'',$institute_name, $system_text);
//                
//                //$this->my_custom_functions->SendNotification($school_id, array("subject" => $subject, "message" => $full_mail,"from" => $from_email), NOTIFICATION_TYPE_SCHOOL_REGISTRATION); // done notification
//                $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $admin_email_address, $admin_subject, $full_mail);
//                                
                /////////////////////////////////////  SEND USERNAME TO SCHOOL MOBILE THROUGH SMS (NOT REQUIRED NOW)  ///////////////////////////////////////////////////////
                
//                $smsText = "Thank you for showing your interest in ".SITE_NAME.". Your institute's username is ".$create_username;                                
//                $smsSent = $this->my_custom_functions->sendSMS($this->input->post('mobile_no'), $smsText);
//                
//                if($smsSent) {
//                    // Insert into SMS log
//                    $this->my_custom_functions->insert_data(array(
//                        "user_id" => 0,
//                        "school_id" => $school_id,
//                        "mobile_no" => $this->input->post('mobile_no'),
//                        "message" => $smsText,
//                        "request_time" => time()
//                    ), TBL_SMS_LOG);
//                }
                //////////////////////////////////////////////// END ///////////////////////////////////////////////////////////////////////////
                
                
                
                
                /////////////////////// UPLOAD IMAGE FOR School logo///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    $timestamp = strtotime(date('Y-m-d H:i:s'));
                    $emp_name = str_replace(' ', '', $this->input->post("name"));

                    $mime_type = $_FILES['adminphoto']['type'];
                    $split = explode('/', $mime_type);
                    $type = $split[1];
                    $original_size = getimagesize($_FILES['adminphoto']['tmp_name']);
                    $img_width = $original_size[0];
                    $img_height = $original_size[0];
                    $temp_file = 'file_upload/' . $school_id . '_' . $timestamp . '.jpg';


                    if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                        $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                        $ImageSize = filesize($temp_file); /* get the image size */

                        //if ($ImageSize < ALLOWED_FILE_SIZE) {

                        $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                        // Instantiate an Amazon S3 client.
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));

                        $bucket = AMAZON_BUCKET;

                        try {
                            $result = $s3->putObject(array(
                                'Bucket' => $bucket,
                                'Key' => SCHOOL_LOGO . '/' . $school_id . '_' . $timestamp,
                                'SourceFile' => $temp_file,
                                'ContentType' => 'text/plain',
                                'ACL' => 'public-read',
                                'StorageClass' => 'REDUCED_REDUNDANCY',
                                'Metadata' => array()
                            ));

                            if (file_exists($temp_file)) {
                                @unlink($temp_file);
                            }

                            //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                            $file_data = array('school_id' => $school_id, 'file_url' => $result['ObjectURL']);
                            $this->my_custom_functions->insert_data($file_data, TBL_SCHOOL_LOGO_FILES);
                        } catch (S3Exception $e) {
                            //echo $e->getMessage() . "\n";
                        }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                    } else {
                        $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                    }
                }

                ////////////////////////  SAVE NOTIFICATION SETTINGS  ////////////////////////////////

                $notification_type = $this->config->item('notification_type');

                foreach ($notification_type as $key => $val) {
                    $email_notification = 0;
                    $sms_notification = 0;
                    if ($key == NOTIFICATION_TYPE_NOTICE || $key == NOTIFICATION_TYPE_DIARY || $key == NOTIFICATION_TYPE_ATTENDANCE_ABSENT || $key == NOTIFICATION_TYPE_FEES_PAYMENT) {
                        $email_notification = 1;
                    }
                    if ($key == NOTIFICATION_TYPE_ATTENDANCE_ABSENT) {
                        $sms_notification = 1;
                    }
                    $data = array(
                        'school_id' => $school_id,
                        'notification_type' => $key,
                        'push_notification' => 1,
                        'email_notification' => $email_notification,
                        'sms_notification' => $sms_notification
                    );
                    $insert = $this->my_custom_functions->insert_data($data, TBL_NOTIFICATION_SETTINGS);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////







                if ($school_id) {
                    $school_detail = $this->my_custom_functions->get_details_from_id($school_id, TBL_SCHOOL);
                    $session_data = array(
                        "usertype" => SCHOOL,
                        "school_id" => $school_id,
                        "school_username" => $create_username,
                        "school_email" => $school_detail["email_address"],
                        "school_is_logged_in" => 1
                    );
                    $this->session->set_userdata($session_data);
                    //$this->session->set_flashdata("s_message", "School successfully created. An email has been sent on your email address. Your username is " . $create_username . ".");
                    //$this->session->set_flashdata("s_message", "School successfully created. An email has been sent on your email address.");
                    //redirect("school/login");
                    //echo "<pre>";print_r($this->session->all_userdata());die;
                    //$this->session->set_flashdata("s_message", "Welcome, Your username is " . $create_username . ".");
                    //redirect("school/user/dashBoard");

                    redirect("school/main/userVerification");
                } else {
                    $this->session->set_flashdata("e_message", "Something went wrong, please try again later");
                    redirect("school/signUp");
                }
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    public function userVerification() {
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            
            $school_id = $this->session->userdata('school_id');
            $db_otp = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'otp', 'and id = "' . $school_id . '"');
            
            if ($db_otp == $this->input->post('enter_otp')) {
                
                $update_data = array(
                    'otp_verification' => 1
                );
                $condition = array(
                    'id' => $school_id
                );
                $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);
                                
                redirect('school/user/dashBoard');
            } else {
                
                $this->session->set_flashdata("e_message", "Incorrect OTP, Please try again.");
                redirect("school/main/userVerification");
            }
        } else {
                       
            $data['page_title'] = 'User Verification';
            $data['school_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata("school_id")));                             
            
            // Account progress checklinks        
            $students = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '"');

            // Show the account progress until the progress is 100%(until a student is created)
            if($students == 0) {
                
                $this->session->set_userdata('account_progress', 1);
            } else {
            
            	if($this->session->userdata('account_progress')) {
                    $this->session->unset_userdata('account_progress');
                }  
            } 
            
            $this->load->view('school/user_verification', $data);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    public function login() {
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("username", "Userame", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");

            if ($this->form_validation->run() == false) { 
                
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/login");
                
            } else {

                $response = $this->School_main_model->login_check();

                if ($response == "1") { /// If credentials match with system  
                    $mobile_varification_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $this->session->userdata('school_id') . '"');

                    if ($mobile_varification_check == 0) {
                        redirect('school/main/userVerification');
                    }
                    $expiry_date = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'license_expiry_date', 'and id = "' . $this->session->userdata('school_id') . '"');
                    $today = date('Y-m-d');
                    if ($today > $expiry_date) {
                        $expiry_module = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'expiry_module', 'and id = "' . $this->session->userdata('school_id') . '"');
                        if ($expiry_module == 0) {
                            redirect('school/main/accountExpired');
                        }
                    }
                    //echo "<pre>";print_r($this->session->all_userdata());die;

                    redirect("school/user/dashBoard");
                } else {
                    $this->session->set_flashdata("e_message", "Invalid username/password.");
                    redirect("school");
                }
            }
        } else {
            if ($this->session->userdata('school_id')) { /// If the admin is already logged in, don't show the login page                
                redirect("school/user/dashBoard");
            }
            $this->load->view('school/login');
        }
    }

    function accountExpired() {
        $data['page_title'] = 'Account expired';
        $this->load->view('school/account_expired', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function checkMultipleUsernameForgotPassword() {
        
        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $username_count = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, " AND username LIKE '" . $username . "' AND (type='" . SCHOOL . "' OR type='" . TEACHER . "') AND status=1");
        
        if($username_count > 1) {
            echo '<select name="user_type" id="user_type" class="form-control">'
                    . '<option value="'.SCHOOL.'">For School</option>'
                    . '<option value="'.TEACHER.'">For Teacher</option>'
               . '</select>'
               . '<label for="user_type">Select Account</label>';   
        } else {
            echo '';
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function forgetPassword() {
        
        $verification_token = "";
        if ($this->input->post('submit') && $this->input->post('submit') != "") {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('username', 'Username', 'trim|required');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/main/forgetPassword");
                
            } else {
                
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                
                $extra = "";
                $where = array("username" => $username);
                if($this->input->post("user_type")) {
                    $user_type = $this->input->post("user_type");
                    
                    if($user_type == SCHOOL) {
                        
                        $extra .= "AND type='".SCHOOL."'"; 
                        $where["type"] = SCHOOL;
                        
                    } else if($user_type == TEACHER) {
                        
                        $extra .= "AND type='".TEACHER."'"; 
                        $where["type"] = TEACHER;
                    }
                }
                $return = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, " AND username LIKE '" . $username . "' ".$extra." AND status=1");
              
                if ($return) {
                    
                    $user_details = $this->my_custom_functions->get_details_from_id("", TBL_COMMON_LOGIN, $where);
                    $create_new_token = $this->School_main_model->create_new_token($user_details['id']);
                    $verification_token = $this->my_custom_functions->get_details_from_id("", TBL_VERIFICATION_TOKEN, array("login_id" => $user_details['id'], "type" => 2)); // 1 = Email verification, 2 = Forgot password
                    
                    //$reset_link = base_url() . 'userResetPassword/'.$verification_token['token'].'/'.$this->my_custom_functions->ablEncrypt($other_details['emp_id']);
                    $reset_link = base_url() . 'school/userResetPassword/' . $verification_token['token'];

                    if($user_details['type'] == SCHOOL) {
                        
                        $other_details = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $user_details['id']));
                        $email = $other_details['email_address'];
                        
                    } else if($user_details['type'] == TEACHER) {
                        
                        $other_details = $this->my_custom_functions->get_details_from_id("", TBL_TEACHER, array("id" => $user_details['id']));
                        $email = $other_details['email'];
                    }

                    if ($email != "") {
                        
                        $email_data = $this->config->item("company_forgot_password");
                        $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='company_forgot_password'");
                        $from_email = $this->config->item($from_email_variable);

                        // Email variables
                        $subject = $email_data['subject'];

                        $addressing_user = $email_data['addressing_user'];

                        $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                                'reset_link' => $reset_link
                            )
                        );

                        $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                                'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($user_details["id"]) . '" target="_blank">unsubscribe</a>'
                            )
                        );
                        $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                                'institute_name' => 'Team Bidyaaly'
                            )
                        );
                        $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                                'systemtext' => SYSTEM_TEXT
                            )
                        );

                        $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe,$institute_name,$system_text);                        
                        $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $email, $subject, $full_mail);

                        $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                        redirect("school/main/forgetPassword");
                    } else {

                        $this->session->set_flashdata("e_message", "Sorry you dont have any email associated with your account. Please ask your company admin to add an email to your account.");
                        redirect("school/main/forgetPassword");
                    }
                } else {

                    $this->session->set_flashdata("e_message", "You are not a registered user!");
                    redirect("school/main/forgetPassword");
                }
            }
        } else {
            $this->load->view('school/forget_password');
        }
    }        

    ////////////////////////////////////////////////////////////////////////////
    
    function userResetPassword() {
        $verfication_token = $this->uri->segment(3);
        if ($verfication_token != "") {
            $token_validation = $this->my_custom_functions->token_validation($verfication_token, 2); // 1 = Email verification, 2 = Forgot password
            if ($token_validation == 1) {
                $user_details = $this->my_custom_functions->get_details_from_id("", TBL_VERIFICATION_TOKEN, array("token" => $verfication_token, "type" => 2)); // 1 = Email verification, 2 = Forgot password
                if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
                    $this->load->library('form_validation');

                    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
                    $this->form_validation->set_rules('confirm_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

                    if ($this->form_validation->run() == FALSE) {

                        $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                        redirect("school/userResetPassword/" . $verfication_token);
                    } else {
                        $data = array(
                            "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                        );

                        $table = TBL_COMMON_LOGIN;

                        $where = array(
                            "id" => $user_details['login_id']
                        );

                        // Update password
                        $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                        if ($password_updated) {
                            $delete_verification_token = $this->School_main_model->delete_verification_token($verfication_token, 2); // 1 = Email verification, 2 = Forgot password

                            $this->session->set_flashdata("s_message", "Your password has been changed successfully, you can login to your account using this new password.");
                            redirect("school");
                        } else {

                            $this->session->set_flashdata("e_message", "Something went wrong. Please make the request to reset the password once again.");
                            redirect("school");
                        }
                    }
                } else {

                    $this->load->view("school/reset_password");
                }
            } else {

                $this->session->set_flashdata("e_message", "Link is expired.");
                redirect("school");
            }
        } else {

            $this->session->set_flashdata("e_message", "Something went wrong. Please make the request to reset the password once again.");
            redirect("school");
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get user ip
    ////////////////////////////////////////////////////////////////////////////
    function getUserIP() {

        $ipaddress = '';

        if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else
            $ipaddress = 'Unknown IP Address';

        return $ipaddress;
    }

    function ipDetails($ip = NULL, $purpose = "location", $deep_detect = TRUE) {

        if ($purpose == "countryId") {
            $output = DEFAULT_COUNTRY_ID; //230;
        } else {
            $output = "";
        }

        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }

        $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support = array("country", "countrycode", "state", "region", "city", "location", "address", "countryid");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America",
            "IN" => "India"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city" => @$ipdat->geoplugin_city,
                            "state" => @$ipdat->geoplugin_regionName,
                            "country" => @$ipdat->geoplugin_countryName,
                            "country_code" => @$ipdat->geoplugin_countryCode,
                            "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                    case "countryid":
                        $output = $this->my_custom_functions->get_particular_field_value("tbl_country", "country_id", " AND code LIKE '" . @$ipdat->geoplugin_countryCode . "'");
                        // If no country matches, send the default country                       
                        if (!$output) {
                            $output = DEFAULT_COUNTRY_ID;
                        }
                        break;
                }
            }
        }
        return $output;
    }

    function send_otp() {
        $school_id = $this->session->userdata('school_id');
        $mobile_no = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, "mobile_no", " AND id = '" . $school_id . "'");

        $last_otp_sending_time = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, "otp_sending_time", " AND id = '" . $school_id . "'");

        $current_time = time();
        $diff = $current_time - $last_otp_sending_time;
//        $fullDays = floor($diff / (60 * 60 * 24));
//        $fullHours = floor(($diff - ($fullDays * 60 * 60 * 24)) / (60 * 60));
        $fullMinutes = floor($diff / 60);

        if ($fullMinutes > 10) {

            $otp = $this->my_custom_functions->generateOTP();
            $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");

            while ($check_pin > 0) {
                $otp = $this->my_custom_functions->generateOTP();
                $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");
            }
            $update_data = array('otp' => $otp, 'otp_sending_time' => time());
            $condition = array('id' => $school_id);
            $update_otp = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
        } else {
            $otp = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, "otp", " AND id = '" . $school_id . "'");
        }

        ///////// CODE FOR SENDING OTP THROUGH MESSAGE
        $message = $otp . ' is your one time password for verification.';
        //$msgen = urlencode($message);
        $msgen = $message;
        $sms_sent = $this->my_custom_functions->sendSMS($mobile_no, $msgen);
        
        if($sms_sent) {
            // Insert into SMS log
            $this->my_custom_functions->insert_data(array(
                "user_id" => 0,
                "school_id" => $school_id,
                "mobile_no" => $mobile_no,
                "message" => $msgen,
                "request_time" => time()
            ), TBL_SMS_LOG);
        }
        
        echo 1;
    }

    function changeMobileNumber() {

        $phone_number = $this->input->post('mobilenumber');
        $update_data = array('mobile_no' => $phone_number);
        $condition = array('id' => $this->session->userdata('school_id'));
        $update = $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);
        $this->session->set_flashdata("s_message", "Mobile number successfully changed.");
        redirect("school/main/userVerification");
    }

    function changeEmailAddress() {

        $email_address = $this->input->post('email');
        $update_data = array('email_address' => $email_address);
        $condition = array('id' => $this->session->userdata('school_id'));
        $update = $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);
        $this->session->set_flashdata("s_message", "Email address successfully changed.");
        redirect("school/main/userVerification");
    }

    function sendVerificationEmail() {
        $school_id = $this->session->userdata('school_id');
        $school_details = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $school_id));
        $token = $this->my_custom_functions->get_verification_token_code();
        $expiry_duration = '+' . VERIFICATION_EXPIRY_TIME . ' minutes';
        $varification_data = array(
            'login_id' => $school_id,
            'token' => $token,
            'expiry_time' => date("Y-m-d H:i:s", strtotime($expiry_duration)),
            'type' => 1 //1=email verification
        );
        $this->db->insert("tbl_verification_token", $varification_data);

        $verification_token = $token;
        $verify_link = base_url() . 'email_Verification/' . $verification_token;

        ////////////////////////// send verification email //////////////////////////////

        $email_data = $this->config->item("institute_email_verification");
        $from_email_variable = $this->my_custom_functions->get_particular_field_value("tbl_system_emails", "from_email_variable", " and variable_name='institute_email_verification'");
        $from_email = $this->config->item($from_email_variable);

        $subject = $email_data['subject'];
        $addressing_user = $email_data['addressing_user'];

        $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
            'verify_link' => $verify_link
                )
        );
        $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
            'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                )
        );
        $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
            'institute_name' => 'Team Bidyaaly'
                )
        );
        $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
            'systemtext' => SYSTEM_TEXT
                )
                        );
        $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name,$system_text);
        //echo $school_details['email_address'];
        //echo $full_mail;
        //die;
        $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $school_details['email_address'], $subject, $full_mail);
        echo 'seccess';
        //$this->session->set_flashdata("s_message", 'Email successfully sent.');
        //redirect("school/main/userVerification");
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Email verification redirect url
    ///////////////////////////////////////////////////////////////////////////////
    function email_verification() {

        $verfication_token = $this->uri->segment(2);

        if ($verfication_token != "") {

            $token_validation = $this->my_custom_functions->token_validation($verfication_token, 1); // 1 = Email verification, 2 = Forgot password

            if ($token_validation == 1) {

                $user_details = $this->my_custom_functions->get_details_from_id("", "tbl_verification_token", array("token" => $verfication_token, "type" => 1)); // 1 = Email verification, 2 = Forgot password

                if (!empty($user_details)) {

                    // Update email verification status
                    $updated = $this->my_custom_functions->update_data(array("email_verification" => 1), TBL_SCHOOL, array("id" => $user_details['login_id']));

                    if ($updated) {

                        // Delete verification token
                        $delete_verification_token = $this->School_main_model->delete_verification_token($verfication_token, 1); // 1 = Email verification, 2 = Forgot password

                        $this->session->set_flashdata("s_message", 'Your account email has been verified successfully.');
                    } else {

                        $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    }
                } else {

                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                }
            } else {

                $this->session->set_flashdata("e_message", "Link is expired. You can send another verification link from your company panel.");
            }
        } else {

            $this->session->set_flashdata("e_message", "Some error occurred.");
        }

        if ($this->session->userdata('school_id')) {
            $mobile_verification_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $this->session->userdata('school_id') . '"');
            if ($mobile_verification_check == 1) {
                redirect("school/user/dashBoard");
            } else {
                redirect("school/main/userVerification");
            }
        } else {
            redirect("school");
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Change main session from menu dropdown
    //////////////////////////////////////////////////////////////////////////// 
    function change_session() {
        
        if($this->input->post("main_session_dropdown")) {
            $session_data = array("session_id" => $this->input->post("main_session_dropdown"));
            $this->session->set_userdata($session_data);
        }
        
        redirect("school/user/dashBoard");
        //redirect($_SERVER['HTTP_REFERER']);
    }
}
