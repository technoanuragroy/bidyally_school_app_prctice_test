<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class User extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_is_logged_in')) {
            $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

            if ($chek_maintainance_mode['maintenance_mode'] == 1) {
                $this->session->set_flashdata("maintainance_message", $chek_maintainance_mode['maintenance_message']);
                redirect("school");
            }
        }


        $this->my_custom_functions->check_school_security();
        $this->load->model("school/School_user_model");
        $this->load->library('Classes/PHPExcel');
//        echo "<pre>";
//        print_r($this->session->all_userdata());
//        die;
        $user_type = $this->session->userdata('usertype');
        if ($user_type == 2) {
            //$chk_permission = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'mark_as_admin', 'and id = "' . $this->session->userdata('teacher_id') . '"');
            //if ($chk_permission == 0) {
            $this->my_custom_functions->check_permission();
            //}
        }

        $prefs = array(
            'start_day' => 'monday',
            'month_type' => 'long',
            'day_type' => 'short',
            'show_next_prev' => true,
            //'next_prev_url' => base_url() . "admin/calendar/event",
            'template' => '
					   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender" >{/table_open}
                                           
                                            {heading_row_start}<tr>{/heading_row_start}
                                           
					   {heading_previous_cell}<th id="pre_link"><a href="{previous_url}" title="previous">&laquo;</a></th>{/heading_previous_cell}
					   
                                           {heading_title_cell}<th colspan="{colspan}" id="month_heading">{heading}</th>{/heading_title_cell}
					   {heading_next_cell}<th id="next_link"><a href="{next_url}" title="next">&raquo;</a></th>{/heading_next_cell}
					
					   {heading_row_end}</tr>{/heading_row_end}
                                           
					   {week_row_start}<tr>{/week_row_start}
					   {week_day_cell}<th>{week_day}</th>{/week_day_cell}
					   {week_row_end}</tr>{/week_row_end}
					
					   {cal_row_start}<tr>{/cal_row_start}
					   {cal_cell_start}<td width="5">{/cal_cell_start}
					
					   {cal_cell_content}<div class="date {custom_class}">{day}</div><div class="content_box sort_box_{day}"></div> {/cal_cell_content}
					   {cal_cell_content_today}<div class="highlight"><div class="date">{day}</div><div class="content_box sort_box_{day}"></div></div>{/cal_cell_content_today}
					
					   {cal_cell_no_content}<div class="date">{day}</div><div class="content_box"></div>{/cal_cell_no_content}
					   {cal_cell_no_content_today}<div class="highlight"><div class="highlight">{day}</div></div>{/cal_cell_no_content_today}
					
					   {cal_cell_blank}&nbsp;{/cal_cell_blank}
					
					   {cal_cell_end}</td>{/cal_cell_end}
					   {cal_row_end}</tr>{/cal_row_end}
					
					   {table_close}</table>{/table_close}'
        );
        $this->load->library('calendar', $prefs);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {

        $data['page_title'] = 'Dashboard';

        $data['no_of_students'] = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $data['no_of_parents'] = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        //$data['no_of_parents_active'] = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and id = "' . $this->session->userdata('school_id') . '" and type = "3" and  otp != "0"');
        $data['no_of_parents_active'] = $this->my_custom_functions->get_active_parents();
        $data['events'] = $this->my_custom_functions->get_multiple_data(TBL_HOLIDAY, 'and school_id = "' . $this->session->userdata('school_id') . '" and start_date >= "' . date('Y-m-d') . '" order by start_date ASC limit 0,3');


//        $data['previous_week_data'] = $this->School_user_model->get_previous_week_data();
//        $data['current_week_data'] = $this->School_user_model->get_current_week_data();                         
//        $data['attendance_count'] = $this->School_user_model->get_attendance_count();
//        $data['prev_week_acivity_data'] = $this->School_user_model->get_previous_week_activity_data();
//        $data['current_week_acivity_data'] = $this->School_user_model->get_current_week_activity_data();
//        $data['activity_count'] = $this->School_user_model->get_activity_count();                                 
        
        // Account progress checklinks        
        $students = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        
        // Show the account progress until the progress is 100%(until a student is created)
        if($students == 0) {
            
            $data['account_progress'] = $this->School_user_model->get_school_account_progress();
            $this->session->set_userdata('account_progress', 1);
        } 
        // Show the charts when the account progress is 100%
        else {
            
            $return = $this->School_user_model->get_dashboard_attendance_chart();
            $data['previous_week_data'] = $return['previous_week_data'];
            $data['current_week_data'] = $return['current_week_data'];
            $data['attendance_count']['present_month_attendance'] = $return['present_month_attendance'];
            $data['attendance_count']['present_week_attendance'] = $return['present_week_attendance'];
            $data['attendance_count']['todays_attendance'] = $return['todays_attendance'];

            $return = $this->School_user_model->get_dashboard_activity_chart();
            $data['prev_week_acivity_data'] = $return['prev_week_acivity_data'];
            $data['current_week_acivity_data'] = $return['current_week_acivity_data'];
            $data['activity_count']['month_activity_list'] = $return['month_activity_list'];
            $data['activity_count']['week_activity_list'] = $return['week_activity_list'];
            $data['activity_count']['day_activity_list'] = $return['day_activity_list'];  
            
            if($this->session->userdata('account_progress')) {
                $this->session->unset_userdata('account_progress');
            }    
        }
        
        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata('school_id')));
                
        $this->load->view("school/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Change password from admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function changePassword() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            //$this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/changePassword");
            } else {
                if ($this->session->userdata('usertype') == SCHOOL) {
                    $change_id = $this->session->userdata('school_id');
                } else if ($this->session->userdata('usertype') == TEACHER) {
                    $change_id = $this->session->userdata('teacher_id');
                }
                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, "password", " and id='" . $change_id . "'");

                if (password_verify($o_password, $password)) {

                    $data = array(
                        "password" => password_hash($this->input->post("n_password"), PASSWORD_DEFAULT)
                    );
                    //print_r($data);die;

                    if ($this->session->userdata('usertype') == SCHOOL) {
                        $change_id = $this->session->userdata('school_id');
                    } else if ($this->session->userdata('usertype') == TEACHER) {
                        $change_id = $this->session->userdata('teacher_id');
                    }
                    $table = TBL_COMMON_LOGIN;

                    $where = array(
                        "id" => $change_id
                    );
                    $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("school/user/changePassword");
                } else {
                    $this->session->set_flashdata("e_message", "Old password is incorrect.");
                    redirect("school/user/changePassword");
                }
            }
        } else {
            //echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";die;
            $data['page_title'] = 'Change password';
            $data['active_menu'] = '';
            $this->load->view("school/change_password", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_profile() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            
            $school_id = $this->input->post('school_id');
            
            $due_reminder_settings = 0;
            if($this->input->post('due_reminder_settings')) {
                $due_reminder_settings = $this->input->post('due_reminder_settings');
            }
            
            $update_data = array(
                'name' => $this->input->post('name'),
                'contact_person_name' => $this->input->post('contact_person_name'),
                'mobile_no' => $this->input->post('mobile_no'),
                'email_address' => $this->input->post('email_address'),
                'address' => $this->input->post('address'),
                'country_id' => $this->input->post('country'),
                'updated_at' => time(),
                'due_reminder_settings' => $due_reminder_settings
            );
            $condition = array(
                'id' => $this->input->post('school_id'),
            );
            $update = $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);
            
            // Update due reminder settings
            $delete_schedules = array('school_id' => $school_id);
            $this->my_custom_functions->delete_data(TBL_DUE_REMINDER_SCHEDULES, $delete_schedules);
            if($this->input->post('due_reminder_settings')) {
                
                // After due date reminders
                if($this->input->post('days_after')) {
                    
                    $days_after = $this->input->post('days_after');
                    $reminder_push = $this->input->post('reminder_push_after');
                    $reminder_email = $this->input->post('reminder_email_after');
                    $reminder_sms = $this->input->post('reminder_sms_after');
                    
                    if(!empty($days_after)) {
                        foreach($days_after as $key => $days) {
                            
                            $schedule = array(
                                'school_id' => $school_id,
                                'reminder_schedule_type' => 2,
                                'days' => $days
                            );                            
                            if(is_array($reminder_push)) {
                                if(array_key_exists($key, $reminder_push)) {
                                    $schedule['reminder_push'] = $reminder_push[$key];
                                }
                            }
                            if(is_array($reminder_email)) {
                                if(array_key_exists($key, $reminder_email)) {
                                    $schedule['reminder_email'] = $reminder_email[$key];
                                }
                            }
                            if(is_array($reminder_sms)) {
                                if(array_key_exists($key, $reminder_sms)) {
                                    $schedule['reminder_sms'] = $reminder_sms[$key];
                                }
                            }
                            
                            $this->my_custom_functions->insert_data($schedule, TBL_DUE_REMINDER_SCHEDULES);
                        }
                    }
                }
                
                // Before due date reminders
                if($this->input->post('days_before')) {
                    
                    $days_before = $this->input->post('days_before');
                    $reminder_push = $this->input->post('reminder_push_before');
                    $reminder_email = $this->input->post('reminder_email_before');
                    $reminder_sms = $this->input->post('reminder_sms_before');
                    
                    if(!empty($days_before)) {
                        foreach($days_before as $key => $days) {
                            
                            $schedule = array(
                                'school_id' => $school_id,
                                'reminder_schedule_type' => 1,
                                'days' => $days
                            );                            
                            if(is_array($reminder_push)) {
                                if(array_key_exists($key, $reminder_push)) {
                                    $schedule['reminder_push'] = $reminder_push[$key];
                                }
                            }
                            if(is_array($reminder_email)) {
                                if(array_key_exists($key, $reminder_email)) {
                                    $schedule['reminder_email'] = $reminder_email[$key];
                                }
                            }
                            if(is_array($reminder_sms)) {
                                if(array_key_exists($key, $reminder_sms)) {
                                    $schedule['reminder_sms'] = $reminder_sms[$key];
                                }
                            }
                            
                            $this->my_custom_functions->insert_data($schedule, TBL_DUE_REMINDER_SCHEDULES);
                        }
                    }
                }
            }

            if ($this->input->post('del_img')) {
                $filepath = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'file_url', 'and id = "' . $this->input->post('school_id') . '" ');
                $file_url = $filepath;
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => SCHOOL_LOGO . '/' . $aws_key
                            ));
                        }
                        $update_data = array('file_url' => "");
                        $condition = array('id' => $school_id);
                        $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }

            /////////////////////// UPLOAD IMAGE FOR School logo///////////////////////////////

            if ($_FILES AND $_FILES['adminphoto']['name']) {
                $filepath = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'file_url', 'and id = "' . $this->input->post('school_id') . '" ');
                $file_url = $filepath;
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => SCHOOL_LOGO . '/' . $aws_key
                            ));
                        }
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }




                $timestamp = strtotime(date('Y-m-d H:i:s'));
                $emp_name = str_replace(' ', '', $this->input->post("name"));

                $mime_type = $_FILES['adminphoto']['type'];
                $split = explode('/', $mime_type);
                $type = $split[1];
                $original_size = getimagesize($_FILES['adminphoto']['tmp_name']);
//                        $img_width = $original_size[0];
//                        $img_height = $original_size[0];
                $img_width = IMAGE_WIDTH;
                $img_height = IMAGE_HEIGHT;
                $temp_file = 'file_upload/' . $school_id . '_' . $timestamp . '.jpg';


                if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                    $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                    $ImageSize = filesize($temp_file); /* get the image size */

                    //if ($ImageSize < ALLOWED_FILE_SIZE) {

                    $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                    // Instantiate an Amazon S3 client.
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));

                    $bucket = AMAZON_BUCKET;

                    try {
                        $result = $s3->putObject(array(
                            'Bucket' => $bucket,
                            'Key' => SCHOOL_LOGO . '/' . $school_id . '_' . $timestamp,
                            'SourceFile' => $temp_file,
                            'ContentType' => 'text/plain',
                            'ACL' => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY',
                            'Metadata' => array()
                        ));

                        if (@file_exists($temp_file)) {
                            @unlink($temp_file);
                        }

                        $update_data = array('file_url' => $result['ObjectURL']);
                        $condition = array('id' => $school_id);
                        $this->my_custom_functions->update_data($update_data, TBL_SCHOOL, $condition);
                    } catch (S3Exception $e) {
                        //echo $e->getMessage() . "\n";
                    }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                } else {
                    $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                }
            }


            $this->session->set_flashdata("s_message", "Record has been successfully updated.");
            redirect("school/user/edit_profile/");
        } else {

            $data['active_menu'] = '';
            $data['page_title'] = 'Edit profile';
            $data['school_details'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL, array("id" => $this->session->userdata('school_id')));
            $data['country_list'] = $this->my_custom_functions->get_multiple_data(TBL_COUNTRY);
            $data['after_schedules'] = $this->my_custom_functions->get_multiple_data(TBL_DUE_REMINDER_SCHEDULES, " and school_id=".$this->session->userdata('school_id')." and reminder_schedule_type='2'");
            $data['before_schedules'] = $this->my_custom_functions->get_multiple_data(TBL_DUE_REMINDER_SCHEDULES, " and school_id=".$this->session->userdata('school_id')." and reminder_schedule_type='1'");

            $this->load->view("school/edit_profile", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_teacher_profile() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            /// Update admin


            $teacher_id = $this->input->post('teacher_id');
            $update_data = array(
                'name' => $this->input->post('name'),
                'phone_no' => $this->input->post('mobile_no'),
                'email' => $this->input->post('email_address'),
            );
            $condition = array(
                'id' => $teacher_id,
            );
            $update = $this->my_custom_functions->update_data($update_data, TBL_TEACHER, $condition);

            if ($this->input->post('del_img')) {
                $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $teacher_id . '" ');


                $file_url = $filepath;
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => TEACHER_FOLD . '/' . $aws_key
                            ));
                        }
                        $update_data = array('file_url' => "");
                        $condition = array('id' => $teacher_id);
                        $this->my_custom_functions->update_data($update_data, TBL_TEACHER, $condition);
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }

            /////////////////////// UPLOAD IMAGE FOR School logo///////////////////////////////

            if ($_FILES AND $_FILES['adminphoto']['name']) {
                $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $teacher_id . '" ');
                $file_url = $filepath;
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => TEACHER_FOLD . '/' . $aws_key
                            ));
                        }
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }




                $timestamp = strtotime(date('Y-m-d H:i:s'));
                $emp_name = str_replace(' ', '', $this->input->post("name"));

                $mime_type = $_FILES['adminphoto']['type'];
                $split = explode('/', $mime_type);
                $type = $split[1];
                $original_size = getimagesize($_FILES['adminphoto']['tmp_name']);
//                        $img_width = $original_size[0];
//                        $img_height = $original_size[0];
                $img_width = IMAGE_WIDTH;
                $img_height = IMAGE_HEIGHT;
                $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';


                if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                    $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                    $ImageSize = filesize($temp_file); /* get the image size */

                    //if ($ImageSize < ALLOWED_FILE_SIZE) {

                    $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                    // Instantiate an Amazon S3 client.
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));

                    $bucket = AMAZON_BUCKET;

                    try {
                        $result = $s3->putObject(array(
                            'Bucket' => $bucket,
                            'Key' => TEACHER_FOLD . '/' . $aws_key,
                            'SourceFile' => $temp_file,
                            'ContentType' => 'text/plain',
                            'ACL' => 'public-read',
                            'StorageClass' => 'REDUCED_REDUNDANCY',
                            'Metadata' => array()
                        ));

                        if (@file_exists($temp_file)) {
                            @unlink($temp_file);
                        }

                        $update_data = array('file_url' => $result['ObjectURL']);
                        $condition = array('id' => $teacher_id);
                        $this->my_custom_functions->update_data($update_data, TBL_TEACHER, $condition);
                    } catch (S3Exception $e) {
                        //echo $e->getMessage() . "\n";
                    }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                } else {
                    $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                }
            }


            $this->session->set_flashdata("s_message", "Record has been successfully updated.");
            redirect("school/user/edit_teacher_profile/");
        } else {

            $data['active_menu'] = '';
            $data['page_title'] = 'Edit teacher profile';
            $data['teacher_details'] = $this->my_custom_functions->get_details_from_id("", TBL_TEACHER, array("id" => $this->session->userdata('teacher_id')));
            $this->load->view("school/edit_teacher_profile", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Reset password(linked with forgot password)
    ///////////////////////////////////////////////////////////////////////////////
    public function reset_password() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('re_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/main/forgot_password");
            } else {
                $data = array(
                    "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                );

                $table = "tbl_admins";

                $where = array(
                    "admin_id" => $this->session->userdata("reset_admin_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_admin_id");

                if ($password_updated) {
                    $this->session->set_flashdata("s_message", 'Password has been changed successfully.<a href="' . base_url() . 'admin">Login here</a>.');
                    redirect("admin/main/forgot_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {

            $admin_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);

            if (isset($admin_id) && isset($code)) {
                $admin_details = $this->my_custom_functions->get_details_from_id("", "tbl_admins", array("admin_id" => $admin_id));

                if ($code == $this->my_custom_functions->encrypt_string($admin_details['email'])) {
                    $this->session->set_userdata("reset_admin_id", $admin_id);
                    $this->load->view("admin/reset_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            } else {
                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("admin/main/forgot_password");
            }
        }
    }

    public function logout() {

        $session_data = array('school_id', 'school_username', 'school_email', 'school_is_logged_in', 'usertype', 'teacher_id', 'session_id', 'account_progress');
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        
        if($this->session->userdata("mobile")) {
            $this->session->unset_userdata("mobile");
            redirect("app/" . WEB_KEY);
        } else {
            redirect("school");
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Session wise semesters
    ////////////////////////////////////////////////////////////////////////////
    public function manageSemester() {

        $data['page_title'] = 'Manage Semester';

        $data['semester'] = $this->School_user_model->get_semester_data();

        $this->load->view('school/manage_semester', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Add semester
    ////////////////////////////////////////////////////////////////////////////
    public function addSemester() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("class", "Class Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("semester_name", "Semester Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSemester");
            } else {

                $chek_unique = $this->my_custom_functions->get_perticular_count(TBL_SEMESTER, 'and school_id="' . $this->session->userdata('school_id') . '" and session_id="' . $this->session->userdata('session_id') . '" and class_id="' . $this->input->post('class') . '"');
                if ($chek_unique > 0) {
                    $this->session->set_flashdata("e_message", 'Semester already exists with selected session and class');
                    redirect("school/user/addSemester");
                }

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }

                $semester_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'session_id' => $this->session->userdata('session_id'),
                    'class_id' => $this->input->post('class'),
                    'semester_name' => $this->input->post('semester_name'),
                    'combined_term_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
                $add_session_data = $this->my_custom_functions->insert_data($semester_data, TBL_SEMESTER);

                if ($add_session_data) {
                                                            
                    $this->session->set_flashdata("s_message", 'Semester added successfully.');
                    redirect("school/user/manageSemester");
                } else {
                    
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSemester");
                }
            }
        } else {

            $data['page_title'] = 'Add Semester';

            $data['classes'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

            $this->load->view('school/add_semester', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit semester
    ////////////////////////////////////////////////////////////////////////////
    public function editSemester() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $semester_id = $this->input->post('semester_id');

            $this->load->library("form_validation");

            $this->form_validation->set_rules("class", "Class Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("semester_name", "Semester Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editSemester/" . $semester_id);
            } else {

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }
                $semester_data = array(
                    'session_id' => $this->session->userdata('session_id'),
                    'class_id' => $this->input->post('class'),
                    'semester_name' => $this->input->post('semester_name'),
                    'combined_term_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
                $condition = array(
                    'id' => $this->my_custom_functions->ablDecrypt($semester_id)
                );
                $edit_class_data = $this->my_custom_functions->update_data($semester_data, TBL_SEMESTER, $condition);

                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Semester edited successfully.');
                    redirect("school/user/manageSemester");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSemester");
                }
            }
        } else {

            $data['page_title'] = 'Edit Semester';

            $semester_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['semester'] = $this->my_custom_functions->get_details_from_id($semester_id, TBL_SEMESTER);
            $data['classes'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['semester_list'] = $this->my_custom_functions->get_multiple_data(TBL_SEMESTER, ' and school_id = "' . $this->session->userdata('school_id') . '" and id !="' . $semester_id . '"');

            $this->load->view('school/edit_semester', $data);
        }
    }

    public function deleteSemester() {
        $semester_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $student_count = $this->my_custom_functions->get_perticular_count(TBL_STUDENT_ENROLLMENT, 'and semester_id = "' . $semester_id . '" and school_id = "' . $this->session->userdata('school_id') . '"');
        if ($student_count > 0) {
            $this->session->set_flashdata("e_message", 'There are ' . $student_count . ' student(s) present under this semester. Please move those students to another semester first.');
            redirect("school/user/manageSemester");
        }

        $delete_semester = array('id' => $semester_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SEMESTER, $delete_semester);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Class deleted successfully.');
            redirect("school/user/manageSemester");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSemester");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////   Session PART
    //////////////////////////////////////////////////////////////////////

    public function manageSession() {
        $data['page_title'] = 'Manage Session';
        $data['session'] = $this->School_user_model->get_session_data();
        $this->load->view('school/manage_session', $data);
    }

    public function addSession() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("session_name", "Session Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSession");
            } else { /// Update admin
                $session_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'session_name' => $this->input->post('session_name'),
                    'from_date' => $this->my_custom_functions->database_date($this->input->post('session_start')),
                    'to_date' => $this->my_custom_functions->database_date($this->input->post('session_end')),
                );
                //echo "<pre>";print_r($session_data);die;
                $add_session_data = $this->my_custom_functions->insert_data_last_id($session_data, TBL_SESSION);

                $existing_sessions = $this->my_custom_functions->get_perticular_count(TBL_SESSION, 'and school_id = "' . $this->session->userdata('school_id') . '"');
                if ($existing_sessions == 1) {
                    $this->session->set_userdata('session_id', $add_session_data);
                }

                if ($add_session_data) {
                                                            
                    $this->session->set_flashdata("s_message", 'Session added successfully.');
                    redirect("school/user/manageSession");
                } else {
                    
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSession");
                }
            }
        } else {
            $data['page_title'] = 'Add Session';
            $this->load->view('school/add_session', $data);
        }
    }

    public function editSession() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $session_id = $this->input->post('session_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("session_name", "Session Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editSession/" . $session_id);
            } else { /// Update class
                $session_data = array(
                    'session_name' => $this->input->post('session_name'),
                    'from_date' => $this->my_custom_functions->database_date($this->input->post('session_start')),
                    'to_date' => $this->my_custom_functions->database_date($this->input->post('session_end')),
                );
                $condition = array(
                    'id' => $this->my_custom_functions->ablDecrypt($session_id)
                );

                $edit_class_data = $this->my_custom_functions->update_data($session_data, TBL_SESSION, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Session edited successfully.');
                    redirect("school/user/manageSession");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSession");
                }
            }
        } else {
            $data['page_title'] = 'Edit session';
            $session_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['session'] = $this->my_custom_functions->get_details_from_id($session_id, TBL_SESSION);

            $this->load->view('school/edit_session', $data);
        }
    }

    public function deleteSession() {
        $session_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

        $semesters_count = $this->my_custom_functions->get_perticular_count(TBL_SEMESTER, 'and session_id = "' . $session_id . '" and school_id = "' . $this->session->userdata('school_id') . '"');
        if ($semesters_count > 0) {
            $this->session->set_flashdata("e_message", 'There are ' . $semesters_count . ' semester(s) under this session. Please delete semester first.');
            redirect("school/user/manageSession");
        }

        $delete_session = array('id' => $session_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SESSION, $delete_session);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Session deleted successfully.');
            redirect("school/user/manageSession");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSession");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////   CLASSES PART
    //////////////////////////////////////////////////////////////////////

    public function manageClasses() {
        $data['page_title'] = 'Manage class';
//        $sql = "Select * from " . TBL_CLASSES . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $sql = base64_encode($sql);
//        $this->session->set_userdata('classes_sql', $sql);
        $data['classes'] = $this->School_user_model->get_classes_data();
        $this->load->view('school/manage_classes', $data);
    }

    public function addClass() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_name", "Class Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addClass");
            } else { /// Update admin
                $class_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_name' => $this->input->post('class_name'),
                    'status' => $this->input->post('status'),
                );

                $add_class_data = $this->my_custom_functions->insert_data($class_data, TBL_CLASSES);
                
                if ($add_class_data) {
                                                            
                    $this->session->set_flashdata("s_message", 'Class added successfully.');
                    redirect("school/user/manageClasses");
                } else {
                    
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageClasses");
                }
            }
        } else {
            $data['page_title'] = 'Add class';
            $this->load->view('school/add_class', $data);
        }
    }

    public function editClass() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_name", "Class Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $class_id);
            } else { /// Update class
                $class_data = array(
                    'class_name' => $this->input->post('class_name'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $class_id
                );

                $edit_class_data = $this->my_custom_functions->update_data($class_data, TBL_CLASSES, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Class edited successfully.');
                    redirect("school/user/manageClasses");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageClasses");
                }
            }
        } else {
            $data['page_title'] = 'Edit class';
            $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['class'] = $this->my_custom_functions->get_details_from_id($class_id, TBL_CLASSES);

            $this->load->view('school/edit_class', $data);
        }
    }

    public function deleteClass() {
        $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_class = array('id' => $class_id);
        $delete = $this->my_custom_functions->delete_data(TBL_CLASSES, $delete_class);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Class deleted successfully.');
            redirect("school/user/manageClasses");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageClasses");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   SECTION PART
    //////////////////////////////////////////////////////////////////////
    function manageSections() {
        $data['page_title'] = 'Manage section';
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $data['post_data'] = array('class_id' => $this->input->post('class'));
            $data['classes'] = $this->School_user_model->get_section_data_search();
        } else {
            $data['classes'] = $this->School_user_model->get_section_data();
        }
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $this->load->view('school/manage_section', $data);
    }

    public function addSection() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSection");
            } else { /// Update admin
                $class_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_name' => $this->input->post('section_name'),
                    'status' => $this->input->post('status'),
                );

                $add_section_data = $this->my_custom_functions->insert_data($class_data, TBL_SECTION);
                
                if ($add_section_data) {
                                                            
                    $this->session->set_flashdata("s_message", 'Section added successfully.');
                    redirect("school/user/manageSections");
                } else {
                    
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSections");
                }
            }
        } else {
            $data['page_title'] = 'Add section';
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/add_section', $data);
        }
    }

    public function editSection() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $section_id = $this->input->post('section_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $this->my_custom_functions->ablEncrypt($section_id));
            } else { /// Update class
                $class_data = array(
                    'class_id' => $this->input->post('class_id'),
                    'section_name' => $this->input->post('section_name'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $section_id
                );

                $edit_class_data = $this->my_custom_functions->update_data($class_data, TBL_SECTION, $condition);
                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Section edited successfully.');
                    redirect("school/user/manageSections");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSections");
                }
            }
        } else {
            $data['page_title'] = 'Edit section';
            $section_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['section'] = $this->my_custom_functions->get_details_from_id($section_id, TBL_SECTION);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/edit_section', $data);
        }
    }

    public function deleteSection() {
        $section_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_section = array('id' => $section_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SECTION, $delete_section);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Section deleted successfully.');
            redirect("school/user/manageSections");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSections");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////   SUBJECT PART
    //////////////////////////////////////////////////////////////////////

    public function manageSubjects() {
        $data['page_title'] = 'Manage subject';
//        $sql = "Select * from " . TBL_SUBJECT . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $sql = base64_encode($sql);
//        $this->session->set_userdata('subject_sql', $sql);
        $data['subjects'] = $this->School_user_model->get_subject_data();
        $this->load->view('school/manage_subject', $data);
    }

    public function addSubject() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSubject");
            } else { /// Update admin
                $class_data = array(
                    //'class_id' => $this->input->post('class_id'),
                    'school_id' => $this->session->userdata('school_id'),
                    'subject_name' => $this->input->post('subject_name'),
                    //'subject_code' => $this->input->post('subject_code'),
                    'status' => $this->input->post('status'),
                );

                $add_subject_data = $this->my_custom_functions->insert_data($class_data, TBL_SUBJECT);
                if ($add_subject_data) {
                    $this->session->set_flashdata("s_message", 'Subject added successfully.');
                    redirect("school/user/manageSubjects");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSubjects");
                }
            }
        } else {
            $data['page_title'] = 'Add subject';
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_subject', $data);
        }
    }

    public function editSubject() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $subject_id = $this->input->post('subject_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editClass/" . $this->my_custom_functions->ablEncrypt($subject_id));
            } else { /// Update class
                $subject_data = array(
                    //'class_id' => $this->input->post('class_id'),
                    'subject_name' => $this->input->post('subject_name'),
                    //'subject_code' => $this->input->post('subject_code'),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $subject_id
                );

                $edit_subject_data = $this->my_custom_functions->update_data($subject_data, TBL_SUBJECT, $condition);
                if ($edit_subject_data) {
                    $this->session->set_flashdata("s_message", 'Subject edited successfully.');
                    redirect("school/user/manageSubjects");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageSubjects");
                }
            }
        } else {
            $data['page_title'] = 'Edit subject';
            $subject_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['subject'] = $this->my_custom_functions->get_details_from_id($subject_id, TBL_SUBJECT);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_subject', $data);
        }
    }

    public function deleteSubject() {
        $subject_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_class = array('id' => $subject_id);
        $delete = $this->my_custom_functions->delete_data(TBL_SUBJECT, $delete_class);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Subject deleted successfully.');
            redirect("school/user/manageSubjects");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageSubjects");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   TEACHER PART
    //////////////////////////////////////////////////////////////////////

    public function manageTeachers() {
        $data['page_title'] = 'Manage Staff';
//        $sql = "Select * from " . TBL_TEACHER . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $sql = base64_encode($sql);
//        $this->session->set_userdata('teacher_sql', $sql);
        $data['teacher'] = $this->School_user_model->get_teacher_data();
        $this->load->view('school/manage_teacher', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Username validation for teacher
    ////////////////////////////////////////////////////////////////////////////
    public function check_username() {
        
        $username = $this->input->post('username');
        
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username='" . $username . "' AND type='".TEACHER."'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function addTeacher() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            // echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("username", "Username", "required|trim");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTeacher");
            } else { /// Update admin
                $teacher_login_data = array(
                    'type' => TEACHER,
                    'username' => $this->input->post('username'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => 1,
                );

                $teacher_id = $this->my_custom_functions->insert_data_last_id($teacher_login_data, TBL_COMMON_LOGIN);


                $teacher_data = array(
                    'id' => $teacher_id,
                    'school_id' => $this->session->userdata('school_id'),
                    'staff_type' => $this->input->post('staff_type'),
                    'email' => $this->input->post('email'),
                    'phone_no' => $this->input->post('phone'),
                    'name' => $this->input->post('name'),
                        //'mark_as_admin' => $this->input->post('mark_as_admin')
                );

                $this->my_custom_functions->insert_data_last_id($teacher_data, TBL_TEACHER);

                if ($teacher_id) {

                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        $timestamp = strtotime(date('Y-m-d H:i:s'));
                        $emp_name = str_replace(' ', '', $this->input->post("name"));

                        $mime_type = $_FILES['adminphoto']['type'];
                        $split = explode('/', $mime_type);
                        $type = $split[1];

                        $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                        $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;

                        if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                            $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);



                            $ImageSize = filesize($temp_file); /* get the image size */

                            //if ($ImageSize < ALLOWED_FILE_SIZE) {

                            $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => TEACHER_FOLD . '/' . $emp_name . '_' . $timestamp,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                if (@file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }

                                //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                $file_data = array('file_url' => $result['ObjectURL']);
                                $condition = array('id' => $teacher_id);
                                $this->my_custom_functions->update_data($file_data, TBL_TEACHER, $condition);
                            } catch (S3Exception $e) {
                                //echo $e->getMessage() . "\n";
                            }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                        } else {
                            $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                        }
                    }
                                        
                    $this->session->set_flashdata("s_message", 'Teacher added successfully.');
                    redirect("school/user/manageTeachers");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTeachers");
                }
            }
        } else {
            $data['page_title'] = 'Add Staff';
            $this->load->view('school/add_teacher', $data);
        }
    }

    public function editTeacher() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die;
            $teacher_id = $this->input->post('teacher_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTeacher/" . $this->my_custom_functions->ablEncrypt($teacher_id));
            } else { /// Update class
                if ($this->input->post('password')) {
                    $login_detail = array(
                        'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
                    );

                    $condition = array(
                        'id' => $teacher_id
                    );

                    $edit_teacher_login_data = $this->my_custom_functions->update_data($login_detail, TBL_COMMON_LOGIN, $condition);
                }


                if ($this->input->post('status') && $this->input->post('status') != '') {

                    $login_detail_s = array(
                        'status' => $this->input->post('status')
                    );

                    $condition = array(
                        'id' => $teacher_id
                    );

                    $edit_teacher_login_data = $this->my_custom_functions->update_data($login_detail_s, TBL_COMMON_LOGIN, $condition);
                }

                $teacher_data = array(
                    'staff_type' => $this->input->post('staff_type'),
                    'phone_no' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                        //'mark_as_admin' => $this->input->post('mark_as_admin')
                );


                $condition = array(
                    'id' => $teacher_id
                );
                //echo "<pre>";print_r($teacher_data);die;
                $edit_teacher_data = $this->my_custom_functions->update_data($teacher_data, TBL_TEACHER, $condition);

                ///////////////////////// REMOVE ONLY IMAGE  //////////////////////////////////
                if ($this->input->post('del_img')) {

                    $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $teacher_id . '" ');
                    $file_url = $filepath;
                    $fileUrlArray = explode("/", $file_url);
                    $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                    if ($aws_key != "") {
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));
                        $bucket = AMAZON_BUCKET;
                        try {
                            if ($aws_key != '') {
                                $result = $s3->deleteObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => TEACHER_FOLD . '/' . $aws_key
                                ));
                            }
                            //$this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
                            $file_data = array('file_url' => '');
                            $condition = array('id' => $teacher_id);
                            $this->my_custom_functions->update_data($file_data, TBL_TEACHER, $condition);
                        } catch (S3Exception $e) {

                            $encode[] = array(
                                "msg" => "Operation Failed",
                                "status" => "true"
                            );
                        }
                    }
                }
                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {




                    $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $teacher_id . '" ');
                    $file_url = $filepath;
                    $fileUrlArray = explode("/", $file_url);
                    $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                    if ($aws_key != "") {
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));
                        $bucket = AMAZON_BUCKET;
                        try {
                            if ($aws_key != '') {
                                $result = $s3->deleteObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => TEACHER_FOLD . '/' . $aws_key
                                ));
                            }
                            //$this->my_custom_functions->delete_data(TBL_TEACHER_FILES, array("teacher_id" => $teacher_id));
                            $file_data = array('file_url' => '');
                            $condition = array('id' => $teacher_id);
                            $this->my_custom_functions->update_data($file_data, TBL_TEACHER, $condition);
                        } catch (S3Exception $e) {

                            $encode[] = array(
                                "msg" => "Operation Failed",
                                "status" => "true"
                            );
                        }
                    }


                    $timestamp = strtotime(date('Y-m-d H:i:s'));
                    $emp_name = str_replace(' ', '', $this->input->post("name"));

                    $mime_type = $_FILES['adminphoto']['type'];
                    $split = explode('/', $mime_type);
                    $type = $split[1];

                    $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                    $img_width = IMAGE_WIDTH;
                    $img_height = IMAGE_HEIGHT;

                    if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                        $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);

                        $ImageSize = filesize($temp_file); /* get the image size */

                        //if ($ImageSize < ALLOWED_FILE_SIZE) {

                        $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                        // Instantiate an Amazon S3 client.
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));

                        $bucket = AMAZON_BUCKET;

                        try {
                            $result = $s3->putObject(array(
                                'Bucket' => $bucket,
                                'Key' => TEACHER_FOLD . '/' . $emp_name . '_' . $timestamp,
                                'SourceFile' => $temp_file,
                                'ContentType' => 'text/plain',
                                'ACL' => 'public-read',
                                'StorageClass' => 'REDUCED_REDUNDANCY',
                                'Metadata' => array()
                            ));

                            if (@file_exists($temp_file)) {
                                @unlink($temp_file);
                            }

                            $file_data = array('file_url' => $result['ObjectURL']);
                            $condition = array('id' => $teacher_id);
                            $this->my_custom_functions->update_data($file_data, TBL_TEACHER, $condition);
                        } catch (S3Exception $e) {
                            //echo $e->getMessage() . "\n";
                        }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                    } else {
                        $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                    }
                }
                if ($edit_teacher_data) {
                    $this->session->set_flashdata("s_message", 'Teacher edited successfully.');
                    redirect("school/user/manageTeachers");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTeachers");
                }
            }
        } else {
            $data['page_title'] = 'Edit Staff';
            $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

            $data['teacher'] = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_TEACHER);
            //$data['teacher_img_file'] = $this->my_custom_functions->get_details_from_id('', TBL_TEACHER_FILES, array('teacher_id' => $teacher_id));

            $this->load->view('school/edit_teacher', $data);
        }
    }

//    public function deleteTeacher() {
//        $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
//        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $teacher_id . '" ');
//        $file_url = $filepath;
//        $fileUrlArray = explode("/", $file_url);
//        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
//        if ($aws_key != "") {
//            $s3 = new S3Client(array(
//                'version' => 'latest',
//                'region' => 'ap-south-1',
//                'credentials' => array(
//                    'key' => AWS_KEY,
//                    'secret' => AWS_SECRET,
//                ),
//            ));
//            $bucket = AMAZON_BUCKET;
//            try {
//                if ($aws_key != '') {
//                    $result = $s3->deleteObject(array(
//                        'Bucket' => $bucket,
//                        'Key' => TEACHER_FOLD . '/' . $aws_key
//                    ));
//                }
//                $file_data = array('file_url' => '');
//                $condition = array('id' => $teacher_id);
//                $this->my_custom_functions->update_data($file_data, TBL_TEACHER, $condition);
//            } catch (S3Exception $e) {
//
//                $encode[] = array(
//                    "msg" => "Operation Failed",
//                    "status" => "true"
//                );
//            }
//        }
//
//        $delete_teacher_diary = array('teacher_id' => $teacher_id);
//        $delete_diary = $this->my_custom_functions->delete_data(TBL_DIARY, $delete_teacher_diary);
//
//
//        $delete_teacher_login = array('id' => $teacher_id);
//        $delete_log = $this->my_custom_functions->delete_data(TBL_COMMON_LOGIN, $delete_teacher_login);
//
//        $delete_teacher = array('id' => $teacher_id);
//        $delete = $this->my_custom_functions->delete_data(TBL_TEACHER, $delete_teacher);
//        if ($delete) {
//            $this->session->set_flashdata("s_message", 'Teacher deleted successfully.');
//            redirect("school/user/manageTeachers");
//        } else {
//            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
//            redirect("school/user/manageTeachers");
//        }
//    }

    public function deleteTeacher() {
        $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_date_pre = date('Y-m-d', strtotime("+" . DELETE_DAY . " days"));
        $delete_date = $delete_date_pre . ' 00:00:00';
        $data = array(
            'is_deleted' => DELETE,
            'delete_time' => $delete_date
        );
        $condition = array(
            'id' => $teacher_id
        );
        $delete = $this->my_custom_functions->update_data($data, TBL_TEACHER, $condition);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Teacher deleted successfully.');
            redirect("school/user/manageTeachers");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageTeachers");
        }
    }

    function restoreTeacher() {
        $teacher_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

        $restore_date = '0000-00-00 00:00:00';
        $data = array(
            'is_deleted' => NOT_DELETE,
            'delete_time' => $restore_date
        );
        $condition = array(
            'id' => $teacher_id
        );
        $restore = $this->my_custom_functions->update_data($data, TBL_TEACHER, $condition);

        if ($restore) {
            $this->session->set_flashdata("s_message", 'Student restored successfully.');
            redirect("school/user/manageTeachers");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageTeachers");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   PERIOD PART
    //////////////////////////////////////////////////////////////////////

    public function managePeriods() {
        $data['page_title'] = 'Manage timetable';
//        $sql = "Select * from " . TBL_PERIODS . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $sql = base64_encode($sql);
//        $this->session->set_userdata('period_sql', $sql);
        $data['period'] = $this->School_user_model->get_period_data();
        $this->load->view('school/manage_period', $data);
    }

    public function addPeriod() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("class_id", "Select Class", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addPeriod");
            } else { /// Update admin
                $max_sort_query = $this->db->query("select max(sort_order) as max_sort_id from " . TBL_PERIODS . "");
                $max_sort = $max_sort_query->row();
                $max_sort_id = $max_sort->max_sort_id + 1;

                $period_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    //'class_id' => $this->input->post('class_id'),
                    'period_name' => $this->input->post('period_name'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'status' => $this->input->post('status'),
                    'sort_order' => $max_sort_id,
                );
                if ($this->input->post('lunch_break')) {
                    $period_data['lunch_break'] = $this->input->post('lunch_break');
                }

                $add_period_data = $this->my_custom_functions->insert_data($period_data, TBL_PERIODS);
                if ($add_period_data) {
                    $this->session->set_flashdata("s_message", 'Period added successfully.');
                    redirect("school/user/managePeriods");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/managePeriods");
                }
            }
        } else {
            $data['page_title'] = 'Add timetable';
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/add_period', $data);
        }
    }

    public function editPeriod() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $period_id = $this->input->post('period_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            $this->form_validation->set_rules("class_id", "Select Class", "trim|required");
            $this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            $this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editPeriod/" . $period_id);
            } else { /// Update class
                $period_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'period_name' => $this->input->post('period_name'),
                    'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'status' => $this->input->post('status'),
                );
                $condition = array(
                    'id' => $period_id
                );

                $edit_period_data = $this->my_custom_functions->update_data($period_data, TBL_PERIODS, $condition);
                if ($edit_period_data) {
                    $this->session->set_flashdata("s_message", 'Period edited successfully.');
                    redirect("school/user/managePeriods");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/managePeriods");
                }
            }
        } else {
            $data['page_title'] = 'Edit timetable';
            $subject_id = $this->uri->segment(4);
            $data['period'] = $this->my_custom_functions->get_details_from_id($subject_id, TBL_PERIODS);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/edit_period', $data);
        }
    }

    public function deletePeriod() {
        $period_id = $this->uri->segment(4);
        $delete_period = array('id' => $period_id);
        $delete = $this->my_custom_functions->delete_data(TBL_PERIODS, $delete_period);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Period deleted successfully.');
            redirect("school/user/managePeriods");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/managePeriods");
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   MANAGE TIME TABLE PART
    //////////////////////////////////////////////////////////////////////

    public function manageTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and session_id = "' . $this->session->userdata('session_id') . '" and class_id = "' . $class_id . '" ');
            $data['period'] = $this->School_user_model->get_time_table_data($semester_id, $section_id);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['post_data'] = array('class_id' => $class_id, 'section_id' => $section_id);
        } else {
            $data['period'] = array();
            $data['post_data'] = array('class_id' => '', 'section_id' => '');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        }
        $data['page_title'] = 'Manage timetable';
        $this->load->view('school/manage_time_table', $data);
    }

    public function addTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            //$this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTimeTable");
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }
                if ($this->input->post('start_minute') < 10) {
                    $start_min = "0" . $this->input->post('start_minute');
                } else {
                    $start_min = $this->input->post('start_minute');
                }
                $start_time = $this->input->post('start_hour') . ':' . $start_min . ' ' . $this->input->post('start_meridian');
                $period_start_time = date('H:i', strtotime("$start_time"));

                if ($this->input->post('end_minute') < 10) {
                    $end_min = "0" . $this->input->post('end_minute');
                } else {
                    $end_min = $this->input->post('end_minute');
                }
                $end_time = $this->input->post('end_hour') . ':' . $end_min . ' ' . $this->input->post('end_meridian');
                $period_end_time = date('H:i', strtotime("$end_time"));



                $routine_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'day_id' => $this->input->post('day_id'),
                    'semester_id' => $this->input->post('semester_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    //'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    //'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'period_start_time' => $period_start_time,
                    'period_end_time' => $period_end_time,
                    'attendance_class' => $attendance_period,
                    'status' => 1,
                );
                //echo "<pre>";print_r($routine_data);die;
                $add_routine_data = $this->my_custom_functions->insert_data($routine_data, TBL_TIMETABLE);
                if ($add_routine_data) {
                    $this->session->set_flashdata("s_message", 'Routine added successfully.');
                    redirect("school/user/manageTimeTable");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTimeTable");
                }
            }
        } else {
            $data['page_title'] = 'Add timetable';
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '" and staff_type = 1');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/add_time_table', $data);
        }
    }
    
    //////////////////////////////////  CHECK DUPLICATE PERIOD AJAX ///////////////////////////
    
    function check_duplicate_period(){
         $day_id = $this->input->post('day_id');
         $class_id = $this->input->post('class_id');
         $semester_id = $this->input->post('semester_id');
         $section_id = $this->input->post('section_id');
         $start_time = date('H:i:s', strtotime($this->input->post('start_time')));
         $end_time = date('H:i:s', strtotime($this->input->post('end_time')));
         $subject_id = $this->input->post('subject_id');
         $teacher_id = $this->input->post('teacher_id');
        
        //$perion_list = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE,'and school_id = "' . $this->session->userdata('school_id') . '" and day_id = "'.$day_id.'" and semester_id = "'.$semester_id.'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and ((period_start_time <="'.$start_time.'" and period_end_time >= "'.$start_time.'") OR (period_start_time <="'.$end_time.'" and period_end_time >= "'.$end_time.'")) and subject_id = "'.$subject_id.'" and teacher_id = "'.$teacher_id.'"');
        $perion_list = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE,'and school_id = "' . $this->session->userdata('school_id') . '" and day_id = "'.$day_id.'"  and ((period_start_time <="'.$start_time.'" and period_end_time >= "'.$start_time.'") OR (period_start_time <="'.$end_time.'" and period_end_time >= "'.$end_time.'"))  and teacher_id = "'.$teacher_id.'"');
        //echo $this->db->last_query();
        if(!empty($perion_list)){
            foreach($perion_list as $period){
                $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $period['teacher_id'] . '"');
                $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $period['class_id'] . '"');
                $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $period['section_id'] . '"');
                $day_name = $this->config->item('days_list')[$period['day_id']];
                $class_start_time = date('H:i A',strtotime($period['period_start_time']));
                $class_end_time = date('H:i A',strtotime($period['period_end_time']));
            }
            
            //$msg = "Teacher ".$teacher_name." is already assigned in ".$class_name."-".$section_name." on ".$day_name." from ".$class_start_time." to ".$class_end_time.". Do you want to proceed?";
              $msg = "class is already assigned to this teacher do you still want to proceed?";
        }else{
            $msg = "Do you want to proceed?";
        }
        echo $msg;
        
        
    }

    function get_section_list() {
        $class_id = $this->input->post('class_id');

        $sec_id = $this->input->post('sec_id');
        $get_section_list = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and status = 1');

        echo '<option value="">Select Section</option>';
        foreach ($get_section_list as $sec_list) {
            if ($sec_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['section_name'] . '</option>';
        }
    }

    function get_semester_list() {
        $class_id = $this->input->post('class_id');

        $sem_id = $this->input->post('sem_id');
        $get_semester_list = $this->my_custom_functions->get_multiple_data(TBL_SEMESTER, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '"');

        echo '<option value="">Select Semester</option>';
        foreach ($get_semester_list as $sec_list) {
            if ($sem_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['semester_name'] . '</option>';
        }
    }

    function get_semester_list_tt() {
        $class_id = $this->input->post('class_id');

        $sem_id = $this->input->post('sem_id');
        $get_semester_list = $this->my_custom_functions->get_multiple_data(TBL_SEMESTER, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and session_id = "' . $this->session->userdata('session_id') . '"');

        echo '<option value="">Select Semester</option>';
        foreach ($get_semester_list as $sec_list) {
            if ($sem_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['semester_name'] . '</option>';
        }
    }

    function get_section_list_move() {
        $class_id = $this->input->post('class_id');

        $sec_id = $this->input->post('sec_id');
        $get_section_list = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '"');

        echo '<option value="">Select Section</option>';
        foreach ($get_section_list as $sec_list) {
            if ($sec_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['section_name'] . '</option>';
        }
    }

    function get_semester_list_move() {
        $class_id = $this->input->post('class_id');


        $get_section_list = $this->my_custom_functions->get_multiple_data(TBL_SEMESTER, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '"');

        echo '<option value="">Select Semester</option>';
        foreach ($get_section_list as $sec_list) {

            echo '<option value="' . $sec_list['id'] . '" >' . $sec_list['semester_name'] . '</option>';
        }
    }

    function get_subject_list_classwise() {
        $class_id = $this->input->post('class_id');

        $get_subject_list = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        echo '<option value="">Select Subject</option>';
        foreach ($get_subject_list as $subject_list) {
            echo '<option value="' . $subject_list['id'] . '">' . $subject_list['subject_name'] . '</option>';
        }
    }

    public function editTimeTable() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $routine_id = $this->input->post('routine_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("subject_id", "Subject", "trim|required");
            //$this->form_validation->set_rules("teacher_id", "Teacher", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }

                if ($this->input->post('start_minute') < 10) {
                    $start_min = "0" . $this->input->post('start_minute');
                } else {
                    $start_min = $this->input->post('start_minute');
                }
                $start_time = $this->input->post('start_hour') . ':' . $start_min . ' ' . $this->input->post('start_meridian');
                $period_start_time = date('H:i', strtotime("$start_time"));

                if ($this->input->post('end_minute') < 10) {
                    $end_min = "0" . $this->input->post('end_minute');
                } else {
                    $end_min = $this->input->post('end_minute');
                }
                $end_time = $this->input->post('end_hour') . ':' . $end_min . ' ' . $this->input->post('end_meridian');
                $period_end_time = date('H:i', strtotime("$end_time"));


                $routine_data_update = array(
                    'day_id' => $this->input->post('day_id'),
                    'semester_id' => $this->input->post('semester_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    //'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    //'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'period_start_time' => $period_start_time,
                    'period_end_time' => $period_end_time,
                    'attendance_class' => $attendance_period,
                );

                //echo "<pre>";print_r($routine_data_update);die;
                $condition = array(
                    'id' => $routine_id
                );
                $update = $this->my_custom_functions->update_data($routine_data_update, TBL_TIMETABLE, $condition);
                if ($update) {
                    $this->session->set_flashdata("s_message", 'Routine successfully updated.');
                    redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/editTimeTable/" . $this->my_custom_functions->ablEncrypt($routine_id));
                }
            }
        } else {
            $routine_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['routine_data'] = $this->my_custom_functions->get_details_from_id($routine_id, TBL_TIMETABLE);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '" and staff_type = 1');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['page_title'] = 'Edit timetable';
            $this->load->view('school/edit_time_table', $data);
        }
    }

    function deleteUsername() {
        $parent_id = $this->input->post('id');
        $student_id = $this->my_custom_functions->ablDecrypt($this->input->post('student_id'));
        $child_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $parent_id . '"');
        if ($child_count > 1) {
            $delete_linked_student = array('student_id' => $student_id, 'parent_id' => $parent_id);
            $delete = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_linked_student);

            $delete_username = array('id' => $parent_id);
            $delete_user_cmd = $this->my_custom_functions->delete_data(TBL_PARENT, $delete_username);
            //echo 1;
        } else {
            $delete_linked_student = array('student_id' => $student_id, 'parent_id' => $parent_id);
            $delete = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_linked_student);

            $delete_username = array('id' => $parent_id);
            $delete_user_cmd = $this->my_custom_functions->delete_data(TBL_PARENT, $delete_username);
            //echo 1;
        }
        $delete_user = array('id' => $parent_id);
        $delete_user_cmd = $this->my_custom_functions->delete_data(TBL_COMMON_LOGIN, $delete_user);
        echo 1;
    }

    function addMoreUsernames() {
//        echo "<pre>";
//        print_r($_POST);
//        die;
        $username = $this->input->post('user_phone_no');
        $student_id = $this->input->post('stdnt_id_for_usrname');
        $check_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $username . '"');

        if ($check_exist > 0) {

//            $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'id', 'and username = "' . $username . '"');
//
//
//            $insert_data = array(
//                'school_id' => $this->session->userdata('school_id'),
//                'parent_id' => $parent_id,
//                'student_id' => $this->my_custom_functions->ablDecrypt($this->input->post('stdnt_id_for_usrname'))
//            );
//            $insert_link_data = $this->my_custom_functions->insert_data($insert_data, TBL_PARENT_KIDS_LINK);
            $flashdata = 'Username already added.';
            $msg_flag = "e_message";
        } else {

            $insert_username_data = array(
                'type' => PARENTS,
                'username' => $username,
                //'school_id' => $this->session->userdata('school_id'),
                'status' => 1
            );
            if ($this->input->post('user_password') == 1) {
                $student_id_raw = $this->my_custom_functions->ablDecrypt($this->input->post('stdnt_id_for_usrname'));
                $student_dob = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'dob', 'and id = "' . $student_id_raw . '" ');
                $dob = explode('-', $student_dob);

                $combine_dob = $dob[2] . '' . $dob[1] . '' . $dob[0];
                $dob_password = password_hash($combine_dob, PASSWORD_DEFAULT);
                $insert_username_data['password'] = $dob_password;
            }
//            echo "<pre>";print_r($insert_username_data);
//            die;
            $parent_id = $this->my_custom_functions->insert_data_last_id($insert_username_data, TBL_COMMON_LOGIN);


            $insert_parent_data = array(
                'id' => $parent_id,
                'school_id' => $this->session->userdata('school_id'),
                'name' => $this->input->post('user_full_name'),
                'email' => $this->input->post('user_email'),
                    //'username' => $username
            );

            $insert_parent_id = $this->my_custom_functions->insert_data($insert_parent_data, TBL_PARENT);

            $insert_data = array(
                'school_id' => $this->session->userdata('school_id'),
                'parent_id' => $parent_id,
                'student_id' => $this->my_custom_functions->ablDecrypt($this->input->post('stdnt_id_for_usrname'))
            );

            $insert_link_data = $this->my_custom_functions->insert_data($insert_data, TBL_PARENT_KIDS_LINK);
            $flashdata = 'Username successfully added.';
            $msg_flag = "s_message";
        }

        $this->session->set_flashdata("$msg_flag", "$flashdata");
        redirect('school/user/editStudent/' . $student_id . '#user');
    }

//    public function deleteStudent() {
//        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
//        
//        
//        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL, 'file_url', 'and student_id = "' . $student_id . '" ');
//
//
//        $file_url = $filepath;
//        $fileUrlArray = explode("/", $file_url);
//        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
//
//        if ($aws_key != "") {
//            $s3 = new S3Client(array(
//                'version' => 'latest',
//                'region' => 'ap-south-1',
//                'credentials' => array(
//                    'key' => AWS_KEY,
//                    'secret' => AWS_SECRET,
//                ),
//            ));
//            $bucket = AMAZON_BUCKET;
//            try {
//                if ($aws_key != '') {
//                    $result = $s3->deleteObject(array(
//                        'Bucket' => $bucket,
//                        'Key' => STUDENT_FOLD . '/' . $aws_key
//                    ));
//                }
//                $file_data = array('file_url' => '');
//                $condition = array('student_id' => $student_id);
//                $this->my_custom_functions->update_data($file_data, TBL_STUDENT_DETAIL, $condition);
//            } catch (S3Exception $e) {
//
//                $encode[] = array(
//                    "msg" => "Operation Failed",
//                    "status" => "true"
//                );
//            }
//        }
//
//
//        $delete_student_attendance = array('student_id' => $student_id);
//        $delete_attendance = $this->my_custom_functions->delete_data(TBL_ATTENDANCE, $delete_student_attendance);
//
//        $delete_student_diary = array('student_id' => $student_id);
//        $delete_diary = $this->my_custom_functions->delete_data(TBL_DIARY, $delete_student_attendance);
//
//        $delete_student_parent_link = array('student_id' => $student_id);
//        $delete_parent_stdnt_link = $this->my_custom_functions->delete_data(TBL_PARENT_KIDS_LINK, $delete_student_parent_link);
//
//        $delete_student_detail = array('student_id' => $student_id);
//        $delete_studnt_del = $this->my_custom_functions->delete_data(TBL_STUDENT_DETAIL, $delete_student_detail);
//
//        $delete_student = array('id' => $student_id);
//        $delete = $this->my_custom_functions->delete_data(TBL_STUDENT, $delete_student);
//
//
//        if ($delete) {
//            $this->session->set_flashdata("s_message", 'Student deleted successfully.');
//            redirect("school/user/manageStudent");
//        } else {
//            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
//            redirect("school/user/manageStudent");
//        }
//    }

    public function deleteStudent() {
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_date_pre = date('Y-m-d', strtotime("+" . DELETE_DAY . " days"));
        $delete_date = $delete_date_pre . ' 00:00:00';
        $data = array(
            'is_deleted' => DELETE_BUT_KEEP_ACCOUNT_RECORDS, ///  here we update the flag to delete student but keeping some account related data, Means some account related data will not be deleted for that student
            'delete_time' => $delete_date
        );
        $condition = array(
            'id' => $student_id
        );
        $delete = $this->my_custom_functions->update_data($data, TBL_STUDENT, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Student deleted successfully.');
            redirect("school/user/manageStudent");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageStudent");
        }
    }

    function restoreStudent() {
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

        $restore_date = '0000-00-00 00:00:00';
        $data = array(
            'is_deleted' => NOT_DELETE,
            'delete_time' => $restore_date
        );
        $condition = array(
            'id' => $student_id
        );
        $restore = $this->my_custom_functions->update_data($data, TBL_STUDENT, $condition);

        if ($restore) {
            $this->session->set_flashdata("s_message", 'Student restored successfully.');
            redirect("school/user/manageStudent");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageStudent");
        }
    }

    function deleteTimeTable() {

        $timeTableTable_id = $this->input->post('timetable_id');

        $delete_data = array('id' => $timeTableTable_id);
        $delete_timetable = $this->my_custom_functions->delete_data(TBL_TIMETABLE, $delete_data);
        echo 1;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Manage students
    ////////////////////////////////////////////////////////////////////////////
    function manageStudent() {

        $data['page_title'] = 'Manage student';

        $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

        if ($this->input->post('submit')) {

            $semester_id = $this->input->post("semester_id");
            $section_id = $this->input->post("section_id");
            $name = $this->input->post("name");
            $roll_no = $this->input->post("roll_no");

            $data['students'] = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id'), "semester_id" => $semester_id, "section_id" => $section_id, "name" => $name, "roll_no" => $roll_no));
            $this->load->view('school/manage_student', $data);
        } else {

            $data['students'] = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id')));
            $this->load->view('school/manage_student', $data);
        }
    }

//    function searchStudent() {
//        $data['page_title'] = 'Search student';
//        $data['students'] = $this->School_user_model->search_student();
//        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
//        $this->load->view('school/manage_student', $data);
//    }

    ////////////////////////////////////////////////////////////////////////////
    // Add student
    ////////////////////////////////////////////////////////////////////////////
    function addStudent() {
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("reg_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("date_of_birth", "Date Of Birth", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "trim|required");
            $this->form_validation->set_rules("father_name", "Father Name", "trim|required");
            $this->form_validation->set_rules("mother_name", "Mother Name", "trim|required");
            //$this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");

            if ($this->form_validation->run() == false) { 
                
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addStudent");
                
            } else { 
                
                $count_total_students = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
                $allowed_license = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'no_of_license', 'and id = "' . $this->session->userdata('school_id') . '"');

                if ($allowed_license > $count_total_students) {

                    $registration_no_chk = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and registration_no = "' . $this->input->post('reg_no') . '"');
                    
                    if ($registration_no_chk == 0) {
                        
                        // Converting the date of birth to password 
                        $dob = explode('-', $this->input->post('date_of_birth'));
                        $combine_dob = $dob[0] . '' . $dob[1] . '' . $dob[2];
                        $dob_password = password_hash($combine_dob, PASSWORD_DEFAULT);
                        
                        // Basic details
                        $basic_data = array(
                            'school_id' => $this->session->userdata('school_id'),
                            'registration_no' => $this->input->post('reg_no'),
                            'name' => $this->input->post('name'),
                            'dob' => $this->my_custom_functions->database_date_dash($this->input->post('date_of_birth')),
                            'gender' => $this->input->post('gender'),
                            'father_name' => $this->input->post('father_name'),
                            'mother_name' => $this->input->post('mother_name'),                            
                            'status' => $this->input->post('status'),
                        );
                        $student_id = $this->my_custom_functions->insert_data_last_id($basic_data, TBL_STUDENT);

                        // Student details
                        $student_detail = array(
                            'student_id' => $student_id,
                            'address' => $this->input->post('address'),
                            'aadhar_no' => $this->input->post('aadhar_no'),
                            'emergency_contact_person' => $this->input->post('emg_contact_person'),
                            'emergency_contact_no' => $this->input->post('emg_contact_no'),
                            'blood_group' => $this->input->post('blood_group'),
                        );
                        $student_details = $this->my_custom_functions->insert_data($student_detail, TBL_STUDENT_DETAIL);

                        // Student semester link details
                        $semester_id = $this->input->post('semester_id');
                        $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id="' . $semester_id . '" ');

                        $student_semester_link_data = array(
                            'school_id' => $this->session->userdata('school_id'),
                            'student_id' => $student_id,
                            'class_id' => $class_id,
                            'semester_id' => $semester_id,
                            'section_id' => $this->input->post('section_id'),
                            'roll_no' => $this->input->post('roll_no'),
                            'enrollment_time' => time()
                        );
                        $this->my_custom_functions->insert_data($student_semester_link_data, TBL_STUDENT_ENROLLMENT);

                        // Parent contacts details
                        $parent_contacts = array_filter($this->input->post('input_mobile'));

                        if (!empty($parent_contacts)) {
                            
                            $parent_names = $this->input->post("input_name");
                            $parent_emails = $this->input->post("input_email");
                            $parent_password_types = $this->input->post('password_type');
                            $parent_passwords = $this->input->post('input_password');
                            
                            foreach ($parent_contacts as $key => $number) {                                                                
                                    
                                // Prepare parent data
                                $parent_name = "";
                                $parent_email = "";
                                $parent_password = $dob_password; // Initially setting parent password with dob

                                if(array_key_exists($key, $parent_names)) {
                                    $parent_name = $parent_names[$key];
                                }

                                if(array_key_exists($key, $parent_emails)) {
                                    $parent_email = $parent_emails[$key];
                                }

                                if(array_key_exists($key, $parent_password_types)) {
                                    $password_type = $parent_password_types[$key];

                                    // If password type is set as manual
                                    if($password_type == 2) {

                                        if(array_key_exists($key, $parent_passwords)) {
                                            $password = $parent_passwords[$key];

                                            if($password != "") {
                                                $parent_password = password_hash($password, PASSWORD_DEFAULT);
                                            }
                                        }
                                    }                                        
                                }

                                // Check if this mobile number is already being used by any parent
                                $username_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $number . '" and type = "' . PARENTS . '"');

                                if ($username_exist > 0) {

                                    $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'id', 'and username = "' . $number . '" and type = "' . PARENTS . '"');

                                } else {

                                    $parent_username_data = array(
                                        'type' => PARENTS,
                                        'username' => $number,
                                        'password' => $parent_password,
                                        'status' => 1
                                    );
                                    $parent_id = $this->my_custom_functions->insert_data_last_id($parent_username_data, TBL_COMMON_LOGIN);
                                }

                                // Parent details
                                // Check if parent details already exists
                                $parent_details = $this->my_custom_functions->get_details_from_id($parent_id, TBL_PARENT);

                                // Update parent details if existing name or email is blank
                                if(!empty($parent_details)) {

                                    if($parent_details['name'] == "" OR $parent_details['email'] == "") {

                                        $update_data = array();

                                        if($parent_details['name'] == "" AND $parent_name != "") {                                                
                                            $update_data['name'] = $parent_name;                                                
                                        }

                                        if($parent_details['email'] == "" AND $parent_email != "") {                                                
                                            $update_data['email'] = $parent_email;                                                
                                        }

                                        if(!empty($update_data)) {
                                            $this->my_custom_functions->update_data($update_data, TBL_PARENT, array("id" => $parent_id));
                                        }
                                    }
                                } 
                                // Insert new parent details
                                else {

                                    $parent_detail_data = array(
                                        'id' => $parent_id,
                                        'school_id' => $this->session->userdata('school_id'),
                                        'name' => $parent_name,
                                        'email' => $parent_email
                                    );
                                    $this->my_custom_functions->insert_data($parent_detail_data, TBL_PARENT);
                                }

                                // Parent student link data
                                $parent_student_link = array(
                                    'school_id' => $this->session->userdata('school_id'),
                                    'parent_id' => $parent_id,
                                    'student_id' => $student_id
                                );
                                $this->my_custom_functions->insert_data($parent_student_link, TBL_PARENT_KIDS_LINK);                                    
                            }
                        }


                        if ($student_id) {

                            /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                            if ($_FILES AND $_FILES['adminphoto']['name']) {


                                $timestamp = strtotime(date('Y-m-d H:i:s'));
                                $emp_name = str_replace(' ', '', $this->input->post("name"));

                                $mime_type = $_FILES['adminphoto']['type'];
                                $split = explode('/', $mime_type);
                                $type = $split[1];

                                $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                                $img_width = IMAGE_WIDTH;
                                $img_height = IMAGE_HEIGHT;

                                if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                                    $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);

                                    $ImageSize = filesize($temp_file); /* get the image size */

                                    //if ($ImageSize < ALLOWED_FILE_SIZE) {

                                    $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                                    // Instantiate an Amazon S3 client.
                                    $s3 = new S3Client(array(
                                        'version' => 'latest',
                                        'region' => 'ap-south-1',
                                        'credentials' => array(
                                            'key' => AWS_KEY,
                                            'secret' => AWS_SECRET,
                                        ),
                                    ));

                                    $bucket = AMAZON_BUCKET;

                                    try {
                                        $result = $s3->putObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => STUDENT_FOLD . '/' . $emp_name . '_' . $timestamp,
                                            'SourceFile' => $temp_file,
                                            'ContentType' => 'text/plain',
                                            'ACL' => 'public-read',
                                            'StorageClass' => 'REDUCED_REDUNDANCY',
                                            'Metadata' => array()
                                        ));

                                        if (@file_exists($temp_file)) {
                                            @unlink($temp_file);
                                        }

                                        //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                                        $file_data = array('file_url' => $result['ObjectURL']);
                                        $condition = array('student_id' => $student_id);
                                        $this->my_custom_functions->update_data($file_data, TBL_STUDENT_DETAIL, $condition);
                                    } catch (S3Exception $e) {
                                        //echo $e->getMessage() . "\n";
                                    }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                                } else {
                                    $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                                }
                            }

                            // Account progress box will disappear once a student is created
                            if($this->session->userdata('account_progress')) {
                                $this->session->unset_userdata('account_progress');
                            }
                            
                            $this->session->set_flashdata("s_message", 'Student successfully created.');
                            redirect("school/user/manageStudent");
                        } else {
                            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                            redirect("school/user/manageStudent");
                        }
                    } else {
                        $this->session->set_flashdata("e_message", 'another student already registered with this registration number.');
                        redirect("school/user/addStudent");
                    }
                } else {
                    $this->session->set_flashdata("e_message", 'No of license reached. Adding student not allowed.');
                    redirect("school/user/addStudent");
                }
            }
        } else {

            $data['page_title'] = 'Add student';
            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

            //echo '<pre>'; print_r($data); echo '</pre>'; die;
            $this->load->view('school/add_student', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_semester_dropdown_from_session() {

        $session_id = $this->input->post('session_id');
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($session_id);

        echo '<option value="">Select Section</option>';
        foreach ($semester_list as $semester) {
            echo '<option value="' . $semester['id'] . '">' . $semester['semester_name'] . '</option>';
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_section_dropdown_from_semester() {

        $semester_id = $this->input->post('semester_id');
        $section_list = $this->my_custom_functions->get_section_list("", $semester_id);

        echo '<option value="">Select Section</option>';
        foreach ($section_list as $section) {
            echo '<option value="' . $section['id'] . '">' . $section['section_name'] . '</option>';
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    function checkRegistrationNumber() {
        
        $school_id = $this->session->userdata('school_id');
        $reg_val = $this->input->post('reg_val');

        // Check validity from add student page
        if($this->input->post('student_id')) {
            $check_registratin_no = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $school_id . '" and registration_no = "' . $reg_val . '" and id != "' . $this->input->post('student_id') . '"');
        } 
        // Check validity from edit student page
        else {
            $check_registratin_no = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $school_id . '" and registration_no = "' . $reg_val . '"');
        }
        
        if ($check_registratin_no > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit student
    ////////////////////////////////////////////////////////////////////////////
    function editStudent() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $student_id = $this->input->post('student_id');

            $this->load->library("form_validation");

            $this->form_validation->set_rules("reg_no", "Registration Number", "trim|required");
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("date_of_birth", "Date Of Birth", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "trim|required");
            $this->form_validation->set_rules("father_name", "Father Name", "trim|required");
            $this->form_validation->set_rules("mother_name", "Mother Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addStudent");
            } else {

                $basic_data = array(
                    //'registration_no' => $this->input->post('reg_no'),
                    'name' => $this->input->post('name'),
                    'dob' => $this->my_custom_functions->database_date_dash($this->input->post('date_of_birth')),
                    'gender' => $this->input->post('gender'),
                    'father_name' => $this->input->post('father_name'),
                    'mother_name' => $this->input->post('mother_name'),
                    //'class_id' => $this->input->post('class_id'),
                    //'semester_id' => $this->input->post('semester_id'),
                    //'section_id' => $this->input->post('section_id'),
                    //'roll_no' => $this->input->post('roll_no'),
                    'status' => $this->input->post('status'),
                );
                $condition = array('id' => $student_id);
                $this->my_custom_functions->update_data($basic_data, TBL_STUDENT, $condition);

                $student_detail = array(
                    //'student_id' => $student_id,
                    'address' => $this->input->post('address'),
                    'aadhar_no' => $this->input->post('aadhar_no'),
                    'emergency_contact_person' => $this->input->post('emg_contact_person'),
                    'emergency_contact_no' => $this->input->post('emg_contact_no'),
                    'blood_group' => $this->input->post('blood_group'),
                );
                $condition_det = array('student_id' => $student_id);
                $student_details = $this->my_custom_functions->update_data($student_detail, TBL_STUDENT_DETAIL, $condition_det);
                //echo $student_id;die;
                if ($student_id) {

                    ///////////////////////// REMOVE ONLY IMAGE  //////////////////////////////////
                    if ($this->input->post('del_img')) {

                        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL, 'file_url', 'and student_id = "' . $student_id . '" ');
                        $file_url = $filepath;
                        $fileUrlArray = explode("/", $file_url);
                        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                        if ($aws_key != "") {
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));
                            $bucket = AMAZON_BUCKET;
                            try {
                                if ($aws_key != '') {
                                    $result = $s3->deleteObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => STUDENT_FOLD . '/' . $aws_key
                                    ));
                                }
                                $file_data = array('file_url' => '');
                                $condition = array('student_id' => $student_id);
                                $this->my_custom_functions->update_data($file_data, TBL_STUDENT_DETAIL, $condition);
                            } catch (S3Exception $e) {

                                $encode[] = array(
                                    "msg" => "Operation Failed",
                                    "status" => "true"
                                );
                            }
                        }
                    }

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////
                    //echo "<pre>";print_r($_FILES);die;
                    if ($_FILES AND $_FILES['adminphoto']['name']) {



                        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL, 'file_url', 'and student_id = "' . $student_id . '" ');
                        $file_url = $filepath;
                        $fileUrlArray = explode("/", $file_url);
                        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                        if ($aws_key != "") {
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));
                            $bucket = AMAZON_BUCKET;
                            try {
                                if ($aws_key != '') {
                                    $result = $s3->deleteObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => STUDENT_FOLD . '/' . $aws_key
                                    ));
                                }
                                $file_data = array('file_url' => '');
                                $condition = array('student_id' => $student_id);
                                $this->my_custom_functions->update_data($file_data, TBL_STUDENT_DETAIL, $condition);
                            } catch (S3Exception $e) {

                                $encode[] = array(
                                    "msg" => "Operation Failed",
                                    "status" => "true"
                                );
                            }
                        }


                        $timestamp = strtotime(date('Y-m-d H:i:s'));
                        $emp_name = str_replace(' ', '', $this->input->post("name"));

                        $mime_type = $_FILES['adminphoto']['type'];
                        $split = explode('/', $mime_type);
                        $type = $split[1];

                        $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                        $img_width = IMAGE_WIDTH;
                        $img_height = IMAGE_HEIGHT;

                        if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                            $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);


                            $ImageSize = filesize($temp_file); /* get the image size */

                            //if ($ImageSize < ALLOWED_FILE_SIZE) {

                            $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => STUDENT_FOLD . '/' . $emp_name . '_' . $timestamp,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                if (@file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }

                                $file_data = array('file_url' => $result['ObjectURL']);
                                $condition = array('student_id' => $student_id);
                                $this->my_custom_functions->update_data($file_data, TBL_STUDENT_DETAIL, $condition);
                            } catch (S3Exception $e) {
                                //echo $e->getMessage() . "\n";
                            }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                        } else {
                            $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                        }
                    }



                    $this->session->set_flashdata("s_message", 'Students record successfully updated.');
                    redirect("school/user/manageStudent");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageStudent");
                }
            }
        } else {

            $data['page_title'] = 'Edit student';

            $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['get_student_detail'] = $this->School_user_model->get_student_detail($student_id);
            //$data['student_img_file'] = $this->my_custom_functions->get_details_from_id('', TBL_STUDENT_FILES, array('student_id' => $student_id));
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['enrollment_history'] = $this->my_custom_functions->get_semesters_of_a_student($student_id);

            $this->load->view('school/edit_student', $data);
        }
    }

    function manageNotice() {
        $data['page_title'] = 'Manage notice';
        $data['notice'] = $this->my_custom_functions->get_multiple_data(TBL_NOTICE, 'and school_id = "' . $this->session->userdata('school_id') . '" ORDER BY issue_date DESC');
        $this->load->view('school/manage_notice', $data);
    }

    function issueNoice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);die;
            //$deviceID = 'fmdAR3cANOc:APA91bGYGDdDqsWdKTIuzW4xgkQlreW9FVZLqEQAmNTF60fGuE2Hst-UeWpBUjTGa8Ljt9HwgHQ_4QlcioRApXfuSRLCfOtEbh-0jT-x86ffLT0ePFFUdrPLHNGh6a2b1nUTSpljwSWX';
            //$headingText = $this->input->post('notice_heading');
            //$textContent = $this->input->post('notice_text');
            //$send_notification = $this->my_custom_functions->sendSingleUserNotification($deviceID, $textContent, $url = "", $bigPic = "", $headingText, $user_id = "");
            //echo "<pre>";print_r($send_notification);die;
//            $subject = 'Push Notification';
//            $message = 'Push Notification test for notice';
//            $this->my_custom_functions->SendNotification(2, array("subject" => $subject, "message" => $message), NOTIFICATION_TYPE_ATTENDANCE);


            $timestamp = time();
            $school_id = $this->session->userdata('school_id');
            $school_name = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"');
            // Email template
            $email_data = $this->config->item("notice");
            $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='send_notice'");
            $from_email = $this->config->item($from_email_variable);

            // Email variables
            //$subject = $email_data['subject'];
            $subject = $this->my_custom_functions->sprintf_email($email_data['subject'], array(
                'ActualNoticeSubject' => $this->input->post('notice_heading'),
                    )
            );
            $addressing_user = $email_data['addressing_user'];

            //$message_post = strip_tags($this->input->post('notice_text'));
            $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                'SchoolName' => $school_name,
                'NOTICEBODY' => $this->input->post('notice_text'),
                    )
            );
            $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                    )
            );
            $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                'institute_name' => $school_name
                    )
            );
            $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                'systemtext' => SYSTEM_TEXT
                    )
            );
            $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
            //echo $full_mail;die;
            if ($this->input->post('type') == 1 || $this->input->post('type') == 2) {
                $data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'issue_date' => date('Y-m-d'),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = strtotime($this->my_custom_functions->database_date_dash($this->input->post('date_of_publish')));
                } else {
                    $data['publish_date'] = strtotime(date('Y-m-d'));
                }

                $notice_id = $this->my_custom_functions->insert_data_last_id($data, TBL_NOTICE);
                ////////////////////////  SEND PUSH NOTIFICATION TO ALL TEACHERS  //////////////////////////////
                //echo preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $message));

                if ($this->input->post('type') == 1) { // to all teachers
                    $teacher_list = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '"');
                    if (!empty($teacher_list)) {

                        foreach ($teacher_list as $teachers) {

                            $this->my_custom_functions->SendNotification($teachers['id'], array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_NOTICE); // done notification
                        }
                    }
                }

                ////////////////////////  SEND PUSH NOTIFICATION TO ALL TEACHERS ENDS  //////////////////////////////
                ////////////////////////  SEND PUSH NOTIFICATION TO ALL PARENTS  //////////////////////////////
                if ($this->input->post('type') == 2) { // to all parents
                    $student_list = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id')));


                    foreach ($student_list as $student) {
                        $parent_ids[] = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'parent_id', 'and student_id = "' . $student['id'] . '"');
                    }
                    //$parent_list = $parent_ids;
                    $parent_list = array_unique($parent_ids);
                    //echo "<pre>";print_r($parent_list);die;
                    //$parent_list = $this->my_custom_functions->get_multiple_data(TBL_PARENT_KIDS_LINK, 'and school_id = "' . $this->session->userdata('school_id') . '" group by parent_id');

                    if (!empty($parent_list)) {
                        $subject = $this->input->post('notice_heading');
                        $message = $this->input->post('notice_text');
                        foreach ($parent_list as $parents) {//echo "<pre>";print_r($parents);
                            $this->my_custom_functions->SendNotification($parents, array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_NOTICE); // done notification
                        }
                    }
                }

                ////////////////////////  SEND PUSH NOTIFICATION TO ALL PARENTS ENDS  //////////////////////////////
            } else {
                $class_list_implode = '';
                $semester_list_implode = '';
                $class_list_array = $this->input->post('class');


                foreach ($class_list_array as $class_list) {
                    $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and session_id = "' . $this->session->userdata('session_id') . '" and class_id = "' . $class_list . '"');



                    $parent_already_sent = array();
                    $class_list_implode .= $class_list . ',';
                    $semester_list_implode .= $semester_id . ',';
                    $students_list = $this->School_user_model->get_student_list_semesterwise($semester_id);
                    //echo "<pre>";print_r($students_list);die;
                    if (!empty($students_list)) {
                        foreach ($students_list as $student) {

                            $parent_id_list = $this->my_custom_functions->get_multiple_data(TBL_PARENT_KIDS_LINK, 'and student_id = "' . $student['id'] . '"');

                            if (!empty($parent_id_list)) {

                                foreach ($parent_id_list as $parent) {// to all parents by class
                                    if (!in_array($parent['parent_id'], $parent_already_sent)) {
                                        $this->my_custom_functions->SendNotification($parent['parent_id'], array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_NOTICE); // done notification
                                        $parent_already_sent[] = $parent['parent_id'];
                                    }
                                }
                            }
                        }
                    }
                }


                $finalclass_list = rtrim($class_list_implode, ',');
                $final_semester_list = rtrim($semester_list_implode, ',');


                $data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    'issue_date' => date('Y-m-d'),
                    'notice_text' => $this->input->post('notice_text'),
                    'semester_id' => $final_semester_list,
                    'status' => '1',
                );

                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = strtotime($this->my_custom_functions->database_date_dash($this->input->post('date_of_publish')));
                } else {
                    $data['publish_date'] = strtotime(date('Y-m-d'));
                }
                $notice_id = $this->my_custom_functions->insert_data_last_id($data, TBL_NOTICE);
            }


//            $notice_id = 5;
//            echo '<pre>'; 
//            print_r($_FILES);  
//            print_r($_POST);  
//            $img_string = rtrim($_POST['img'], '|');                    
//            $img = explode('|', $img_string);
//            print_r($img);  
//            echo '</pre>'; 
//            die;

            if ($notice_id) {

                if (isset($_FILES['doc_upload'])) {

                    // Instantiate an Amazon S3 client.
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));

                    $bucket = AMAZON_BUCKET;

                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $file_type_list = $this->config->item('file_type');

                    if ($_POST['img'] != "") {

                        $img_string = rtrim($_POST['img'], '|');
                        $img = explode('|', $img_string);
                        $file_count = count($img);

                        for ($i = 0; $i < $file_count; $i++) {

                            $ext = '';
                            if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
                                $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);
                                $ext = 'jpg';
                            }
                            if (strpos($img[$i], 'data:image/png;base64,') === 0) {
                                $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]);
                                $ext = 'png';
                            }

                            if (in_array($ext, $file_type_list)) {

                                $uploaddir = 'file_upload/';
                                $file_name = $this->my_custom_functions->RandomString() . '.jpg';
                                $img[$i] = str_replace(' ', '+', $img[$i]);
                                $data = base64_decode($img[$i]);
                                $uploadfile = $uploaddir . $file_name;

                                $success = file_put_contents($uploadfile, $data);
                                $temp_file = $uploadfile;
                                $key = $timestamp . '_' . $file_name;

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => NOTICE . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));

                                    $file_data = array('type' => 4, 'note_id' => $notice_id, 'file_url' => $result['ObjectURL'], 'last_update' => date('Y-m-d H:i:s'));
                                    $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                    $success_count++;
                                } catch (S3Exception $e) {

                                    $error_count++;
                                }

                                if (@file_exists($temp_file)) {
                                    @unlink($temp_file);
                                }
                            } else {

                                $error_count++;
                            }
                        }
                    }

                    $ignore_file_types = array('jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG');
                    $file_count = count($_FILES['doc_upload']['name']);

                    for ($i = 0; $i < $file_count; $i++) {

                        $file_name = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($file_name, PATHINFO_EXTENSION);

                        if (in_array($ext, $file_type_list) AND ! in_array($ext, $ignore_file_types)) {

                            $uploaddir = 'file_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);
                            $temp_file = $uploadfile;
                            $key = $timestamp . '_' . $file_name;

                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => NOTICE . '/' . $key,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));

                                $file_data = array('type' => 4, 'note_id' => $notice_id, 'file_url' => $result['ObjectURL'], 'last_update' => date('Y-m-d H:i:s'));
                                $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                $success_count++;
                            } catch (S3Exception $e) {

                                $error_count++;
                            }

                            if (@file_exists($temp_file)) {
                                @unlink($temp_file);
                            }
                        }
                    }

                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Notice successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        //$error_ext = rtrim($error, ',');
                        //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
            }
            redirect("school/user/manageNotice");
        } else {
            $data['page_title'] = 'Add notice';
            $this->load->view('school/issue_notice', $data);
        }
    }

    function get_class_list_semesterwise() {
        $school_id = $this->session->userdata('school_id');
        $class_list = $this->School_user_model->get_class_list_semester_wise($school_id);

        $db_class = $this->input->post('class_id');
        $checked = '';
        if ($db_class != '') {
            $class_list_exp = explode(',', $db_class);
        }
        if (!empty($class_list)) {
            echo '<div class="custom-control custom-checkbox custom-control-inline mb-5"><input class="custom-control-input" type="checkbox" name="example-inline-checkboxall" id="checkboxall" value="0">
                  <label class="custom-control-label" for="checkboxall">Select All</label>
                  </div><br>';
            foreach ($class_list as $rox) {
                foreach ($rox as $row) {
                    if (!empty($class_list_exp)) {
                        $checked = '';
                        foreach ($class_list_exp as $class_idd) {
                            if ($class_idd == $row['id']) {
                                $checked = 'checked="checked"';
                            }
                        }
                    } else {
                        $checked = '';
                    }
                    echo '<div class="custom-control custom-checkbox custom-control-inline mb-5"><input required class="custom-control-input checkboxTeacher" type="checkbox" name="class[]" id="example-inline-checkbox' . $row['id'] . '" value="' . $row['id'] . '" ' . $checked . '>
                  <label class="custom-control-label" for="example-inline-checkbox' . $row['id'] . '"">' . $row['class_name'] . '</label>
                  </div>';
                }
            }
        }
    }

    function editNotice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);

            if ($this->input->post('type') == 1 || $this->input->post('type') == 2) {
                $data = array(
                    'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    //'issue_date' => $this->my_custom_functions->database_date($this->input->post('date_of_issue')),
                    'semester_id' => '',
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = strtotime($this->my_custom_functions->database_date_dash($this->input->post('date_of_publish')));
                }
            } else {
//                $class_list_implode = '';
//
//                $class_list_array = $this->input->post('class');
//                foreach ($class_list_array as $class_list) {
//                    $class_list_implode .= $class_list . ',';
//                }
//                $finalclass_list = rtrim($class_list_implode, ',');
                $data = array(
                    // 'type' => $this->input->post('type'),
                    'notice_heading' => $this->input->post('notice_heading'),
                    //'issue_date' => $this->my_custom_functions->database_date($this->input->post('date_of_issue')),
                    'notice_text' => $this->input->post('notice_text'),
                    // 'class_id' => $finalclass_list,
                    'status' => 1
                );

                if ($this->input->post('date_of_publish') != '') {
                    $data['publish_date'] = strtotime($this->my_custom_functions->database_date_dash($this->input->post('date_of_publish')));
                }
            }
            $condition = array('id' => $this->input->post('notice_id'));
            //echo "<pre>";print_r($data);die;
            //UPLOAD FILES;
            $this->my_custom_functions->update_data($data, TBL_NOTICE, $condition);



            $notice_id = $this->input->post('notice_id');
            //echo "<pre>";print_r($_FILES);
            if ($_FILES['doc_upload']['name'] != '') {
                $file_type_list = $this->config->item('file_type');
                $file_count = count($_FILES['doc_upload']['name']);
                $success_count = 0;
                $error_count = 0;
                $error = '';
                $errorAttch = 0;
                $successfulAttch = 0;
                for ($i = 0; $i < $file_count; $i++) {

                    $filename = $_FILES['doc_upload']['name'][$i];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if ((in_array($ext, $file_type_list))) {

                        $uploaddir = 'file_upload/';
                        $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                        move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                        $temp_file = $uploadfile;


                        $fileSize = $_FILES['doc_upload']['size'][$i];
                        if ($fileSize <= ALLOWED_FILE_SIZE) {

                            // Instantiate an Amazon S3 client.
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));

                            $bucket = AMAZON_BUCKET;
                            $key = '';
                            if (
                                    $ext == "jpg" ||
                                    $ext == "jpeg" ||
                                    $ext == "png") {
                                $key .= 'image_';
                            } else if (
                                    $ext == "pdf" ||
                                    $ext == "doc" ||
                                    $ext == "docx" ||
                                    $ext == "xls" ||
                                    $ext == "xlsx" ||
                                    $ext == "pptx" ||
                                    $ext == "ppt"
                            ) {
                                $key .= 'notice_';
                            }
                            $timestamp = time();
                            $key .= $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                            //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                            //echo $key;die;
                            try {
                                $result = $s3->putObject(array(
                                    'Bucket' => $bucket,
                                    'Key' => NOTICE . '/' . $key,
                                    'SourceFile' => $temp_file,
                                    'ContentType' => 'text/plain',
                                    'ACL' => 'public-read',
                                    'StorageClass' => 'REDUCED_REDUNDANCY',
                                    'Metadata' => array()
                                ));


                                $file_data = array('type' => 4, 'note_id' => $notice_id, 'file_url' => $result['ObjectURL'], 'last_update' => date('Y-m-d H:i:s'));
                                $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                $successfulAttch++;
                            } catch (S3Exception $e) {

                                $errorAttch++;
                            }

                            $success_count++;
                        } else {
                            $errorAttch++;
                        }
                    } else {
                        //$error_count++;
                        //$error .= '.' . $ext . ',';
                    }

                    if (@file_exists($temp_file)) {
                        @unlink($temp_file);
                    }
                }



                if ($success_count > 0) {
                    $this->session->set_flashdata("s_message", 'Notice successfully added.');
                    //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                }
                if ($error_count > 0) {
                    $error_ext = rtrim($error, ',');
                    $this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                }
            }





            redirect("school/user/manageNotice");
        } else {
            $data['page_title'] = 'Edit notice';
            $notice_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

            $data['notice_detail'] = $this->my_custom_functions->get_details_from_id($notice_id, TBL_NOTICE);
            $data['notice_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and note_id = "' . $notice_id . '" and type = 4');
            $this->load->view('school/edit_notice', $data);
        }
    }

    function deleteNotice() {
        $notice_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $notice_file_list_data = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and note_id = "' . $notice_id . '" and type = 4');

        if (!empty($notice_file_list_data)) {
            foreach ($notice_file_list_data as $notice_file_list) {
                $file_url = $notice_file_list['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => NOTICE . '/' . $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data(TBL_NOTE_FILES, array("id" => $notice_file_list['id']));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
        }
        $condition = array('id' => $notice_id);

        $delete = $this->my_custom_functions->delete_data(TBL_NOTICE, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Notice successfully deleted.');
            redirect("school/user/manageNotice");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/manageNotice");
        }
    }

    function deleteIndividualFile() {
        $indv_file_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $notice_file_list = $this->my_custom_functions->get_details_from_id($indv_file_id, TBL_NOTE_FILES);

        if (!empty($notice_file_list)) {
            //foreach ($notice_file_data as $notice_file_list) {echo $notice_file_list['id'];die;
            $file_url = $notice_file_list['file_url'];
            $fileUrlArray = explode("/", $file_url);
            $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

            if ($aws_key != "") {
                $s3 = new S3Client(array(
                    'version' => 'latest',
                    'region' => 'ap-south-1',
                    'credentials' => array(
                        'key' => AWS_KEY,
                        'secret' => AWS_SECRET,
                    ),
                ));
                $bucket = AMAZON_BUCKET;
                try {
                    if ($aws_key != '') {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => NOTICE . '/' . $aws_key
                        ));
                    }
                    $delete = $this->my_custom_functions->delete_data(TBL_NOTE_FILES, array("id" => $notice_file_list['id']));
                } catch (S3Exception $e) {

                    $encode[] = array(
                        "msg" => "Operation Failed",
                        "status" => "true"
                    );
                }
            }
            //}
        }

        if ($delete) {
            $this->session->set_flashdata("s_message", 'file successfully deleted.');
            redirect("school/user/editNotice/" . $this->my_custom_functions->ablEncrypt($notice_file_list['note_id']));
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/editNotice/" . $this->my_custom_functions->ablEncrypt($notice_file_list['note_id']));
        }
    }

    function manageHolidays() {
        $data['page_title'] = 'Manage holidays';
        $data['holiday_list'] = $this->my_custom_functions->get_multiple_data(TBL_HOLIDAY, 'and school_id = "' . $this->session->userdata('school_id') . '" and session_id = "' . $this->session->userdata('session_id') . '" ');

        $this->load->view('school/manage_holiday', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Add holiday
    ////////////////////////////////////////////////////////////////////////////
    function addHolidays() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("office_close", "Office Open", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addHolidays");
            } else {

                $teaching_staff_present = 0;
                $non_teaching_staff_present = 0;

                if ($this->input->post('office_close') == 1) { // Office open
                    if ($this->input->post('teaching_staff_present')) {
                        $teaching_staff_present = 1;
                    }
                    if ($this->input->post('non_teaching_staff_present')) {
                        $non_teaching_staff_present = 1;
                    }
                }

                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'session_id' => $this->session->userdata('session_id'),
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'start_date' => $this->my_custom_functions->database_date_dash($this->input->post('from_date')),
                    'office_open' => $this->input->post('office_close'),
                    'teaching_staff_present' => $teaching_staff_present,
                    'non_teaching_staff_present' => $non_teaching_staff_present,
                    'status' => 1,
                );
                if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
                    $basic_data['end_date'] = $this->my_custom_functions->database_date_dash($this->input->post('to_date'));
                }
                $holiday = $this->my_custom_functions->insert_data($basic_data, TBL_HOLIDAY);

                if ($holiday) {
                    $this->session->set_flashdata("s_message", 'Holiday successfully created.');
                    redirect("school/user/manageHolidays");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageHolidays");
                }
            }
        } else {

            $data['page_title'] = 'Add holiday';

            $data['session_details'] = $this->my_custom_functions->get_details_from_id($this->session->userdata('session_id'), TBL_SESSION);

            $this->load->view('school/add_holiday', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit holiday
    ////////////////////////////////////////////////////////////////////////////
    function editHolidays() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $holiday_id = $this->input->post('holiday_id');

            $this->load->library("form_validation");

            $this->form_validation->set_rules("title", "Title", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("office_close", "Office Open", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editHolidays/" . $this->my_custom_functions->ablEncrypt($holiday_id));
            } else {

                $teaching_staff_present = 0;
                $non_teaching_staff_present = 0;

                if ($this->input->post('office_close') == 1) { // Office open
                    if ($this->input->post('teaching_staff_present')) {
                        $teaching_staff_present = 1;
                    }
                    if ($this->input->post('non_teaching_staff_present')) {
                        $non_teaching_staff_present = 1;
                    }
                }

                $basic_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'start_date' => $this->my_custom_functions->database_date_dash($this->input->post('from_date')),
                    'office_open' => $this->input->post('office_close'),
                    'teaching_staff_present' => $teaching_staff_present,
                    'non_teaching_staff_present' => $non_teaching_staff_present,
                    'status' => $this->input->post('status'),
                );
                if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
                    $basic_data['end_date'] = $this->my_custom_functions->database_date_dash($this->input->post('to_date'));
                }
                $condition = array('id' => $holiday_id);
                $holiday = $this->my_custom_functions->update_data($basic_data, TBL_HOLIDAY, $condition);

                if ($holiday) {
                    $this->session->set_flashdata("s_message", 'Holiday successfully updated.');
                    redirect("school/user/manageHolidays");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("school/user/manageHolidays");
                }
            }
        } else {

            $data['page_title'] = 'Edit holiday';

            $holiday_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['holiday_detail'] = $this->my_custom_functions->get_details_from_id($holiday_id, TBL_HOLIDAY);
            $data['session_details'] = $this->my_custom_functions->get_details_from_id($this->session->userdata('session_id'), TBL_SESSION);

            $this->load->view('school/edit_holiday', $data);
        }
    }

    function deleteHoliday() {
        $id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $condition = array('id' => $id);
        $deleteHoliday = $this->my_custom_functions->delete_data(TBL_HOLIDAY, $condition);
        //echo $this->db->last_query();die;
        if ($deleteHoliday) {
            $this->session->set_flashdata("s_message", 'Data successfully deleted.');
            redirect("school/user/manageHolidays");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/manageHolidays");
        }
    }

    //////////////////////////////////////////////////////////
    ////// Syllabus Part
    //////////////////////////////////////////////////////////


    function manageSyllabus() {
        $data['page_title'] = 'Manage syllabus';
        $data['syllabus'] = $this->my_custom_functions->get_multiple_data(TBL_SYLLABUS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $this->load->view('school/manage_syllabus', $data);
    }

    function addSyllabus() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("class_id", "Class", "trim|required");




            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addSyllabus");
            } else { /// Update admin
                $syllabus = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'class_id' => $this->input->post('class_id'),
                    'syllabus_text' => $this->input->post('syllabus_text'),
                );
                $syllabus_id = $this->my_custom_functions->insert_data_last_id($syllabus, TBL_SYLLABUS);


                if ($syllabus_id) {
                    if (isset($_FILES['doc_upload'])) {
                        $file_type_list = $this->config->item('file_type');
                        $file_count = count($_FILES['doc_upload']['name']);
                        $success_count = 0;
                        $error_count = 0;
                        $error = '';
                        $errorAttch = 0;
                        $successfulAttch = 0;
                        for ($i = 0; $i < $file_count; $i++) {

                            $filename = $_FILES['doc_upload']['name'][$i];
                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            if ((in_array($ext, $file_type_list))) {

                                $uploaddir = 'syllabus_upload/';
                                $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                                move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                                $temp_file = $uploadfile;


                                $fileSize = $_FILES['doc_upload']['size'][$i];
                                if ($fileSize <= ALLOWED_FILE_SIZE) {

                                    // Instantiate an Amazon S3 client.
                                    $s3 = new S3Client(array(
                                        'version' => 'latest',
                                        'region' => 'ap-south-1',
                                        'credentials' => array(
                                            'key' => AWS_KEY,
                                            'secret' => AWS_SECRET,
                                        ),
                                    ));

                                    $bucket = AMAZON_BUCKET;
                                    $key = '';
                                    if (
                                            $ext == "jpg" ||
                                            $ext == "jpeg" ||
                                            $ext == "png") {
                                        $key .= 'image_';
                                    } else if (
                                            $ext == "pdf" ||
                                            $ext == "doc" ||
                                            $ext == "docx" ||
                                            $ext == "xls" ||
                                            $ext == "xlsx" ||
                                            $ext == "pptx" ||
                                            $ext == "ppt"
                                    ) {
                                        
                                    }
                                    $timestamp = time();
                                    $key = 'syllabus_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                    //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                    try {
                                        $result = $s3->putObject(array(
                                            'Bucket' => $bucket,
                                            'Key' => SYLLABUS . '/' . $key,
                                            'SourceFile' => $temp_file,
                                            'ContentType' => 'text/plain',
                                            'ACL' => 'public-read',
                                            'StorageClass' => 'REDUCED_REDUNDANCY',
                                            'Metadata' => array()
                                        ));


                                        $file_data = array('type' => 5, 'note_id' => $syllabus_id, 'file_url' => $result['ObjectURL'], 'last_update' => date('Y-m-d H:i:s'));
                                        $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                        $successfulAttch++;
                                    } catch (S3Exception $e) {

                                        $errorAttch++;
                                    }

                                    $success_count++;
                                } else {
                                    $errorAttch++;
                                }
                            } else {
                                //$error_count++;
                                //$error .= '.' . $ext . ',';
                            }

                            if (@file_exists($temp_file)) {
                                @unlink($temp_file);
                            }
                        }



                        if ($success_count > 0) {
                            $this->session->set_flashdata("s_message", 'Syllabus successfully added.');
                            //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                        }
                        if ($error_count > 0) {
                            //$error_ext = rtrim($error, ',');
                            //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                        }
                    }
                }


                redirect('school/user/manageSyllabus');
            }
        } else {
            $data['page_title'] = 'Add syllabus';
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/add_syllabus', $data);
        }
    }

    function editSyllabus() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editSyllabus/" . $this->my_custom_functions->ablEncrypt($this->input->post('syllabus_id')));
            } else { /// Update admin
                $syllabus_id = $this->input->post('syllabus_id');
                $syllabus = array(
                    'class_id' => $this->input->post('class_id'),
                    'syllabus_text' => $this->input->post('syllabus_text'),
                );
                $condition = array(
                    'id' => $this->input->post('syllabus_id')
                );
                $this->my_custom_functions->update_data($syllabus, TBL_SYLLABUS, $condition);

                if (isset($_FILES['doc_upload'])) {
                    $file_type_list = $this->config->item('file_type');
                    $file_count = count($_FILES['doc_upload']['name']);
                    $success_count = 0;
                    $error_count = 0;
                    $error = '';
                    $errorAttch = 0;
                    $successfulAttch = 0;
                    for ($i = 0; $i < $file_count; $i++) {

                        $filename = $_FILES['doc_upload']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if ((in_array($ext, $file_type_list))) {

                            $uploaddir = 'syllabus_upload/';
                            $uploadfile = $uploaddir . basename($_FILES['doc_upload']['name'][$i]);

                            move_uploaded_file($_FILES['doc_upload']['tmp_name'][$i], $uploadfile);

                            $temp_file = $uploadfile;


                            $fileSize = $_FILES['doc_upload']['size'][$i];
                            if ($fileSize <= ALLOWED_FILE_SIZE) {

                                // Instantiate an Amazon S3 client.
                                $s3 = new S3Client(array(
                                    'version' => 'latest',
                                    'region' => 'ap-south-1',
                                    'credentials' => array(
                                        'key' => AWS_KEY,
                                        'secret' => AWS_SECRET,
                                    ),
                                ));

                                $bucket = AMAZON_BUCKET;
                                $key = '';
                                if (
                                        $ext == "jpg" ||
                                        $ext == "jpeg" ||
                                        $ext == "png") {
                                    $key .= 'image_';
                                } else if (
                                        $ext == "pdf" ||
                                        $ext == "doc" ||
                                        $ext == "docx" ||
                                        $ext == "xls" ||
                                        $ext == "xlsx" ||
                                        $ext == "pptx" ||
                                        $ext == "ppt"
                                ) {
                                    
                                }
                                $timestamp = time();
                                $key = 'syllabus_' . $timestamp . '_' . $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));
                                //$key .= $this->my_custom_functions->clean_alise(basename($_FILES['doc_upload']['name'][$i]));

                                try {
                                    $result = $s3->putObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => SYLLABUS . '/' . $key,
                                        'SourceFile' => $temp_file,
                                        'ContentType' => 'text/plain',
                                        'ACL' => 'public-read',
                                        'StorageClass' => 'REDUCED_REDUNDANCY',
                                        'Metadata' => array()
                                    ));


                                    $file_data = array('type' => 5, 'note_id' => $syllabus_id, 'file_url' => $result['ObjectURL'], 'last_update' => date('Y-m-d H:i:s'));
                                    $this->my_custom_functions->insert_data($file_data, TBL_NOTE_FILES);

                                    $successfulAttch++;
                                } catch (S3Exception $e) {

                                    $errorAttch++;
                                }

                                $success_count++;
                            } else {
                                $errorAttch++;
                            }
                        } else {
                            //$error_count++;
                            //$error .= '.' . $ext . ',';
                        }

                        if (@file_exists($temp_file)) {
                            @unlink($temp_file);
                        }
                    }



                    if ($success_count > 0) {
                        $this->session->set_flashdata("s_message", 'Syllabus successfully added.');
                        //$this->session->set_flashdata("s_message", $success_count.' files successfully uploaded.');
                    }
                    if ($error_count > 0) {
                        //$error_ext = rtrim($error, ',');
                        //$this->session->set_flashdata("e_message", $error_ext . ' files not allowed.');
                    }
                }
                redirect('school/user/manageSyllabus');
            }
        } else {
            $data['page_title'] = 'Edit syllabus';
            $syllabus_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['syllabus_detail'] = $this->my_custom_functions->get_details_from_id($syllabus_id, TBL_SYLLABUS);
            $data['syllabus_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and note_id = "' . $syllabus_id . '" and type = 5');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
            $this->load->view('school/edit_syllabus', $data);
        }
    }

    function deleteSyllabus() {
        $syllabus_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $syllabus_file_list_data = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and note_id = "' . $syllabus_id . '" and type = 5');

        if (!empty($syllabus_file_list_data)) {
            foreach ($syllabus_file_list_data as $syllabus_file_list) {
                $file_url = $syllabus_file_list['file_url'];
                $fileUrlArray = explode("/", $file_url);
                $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

                //echo $aws_key; die;
                if ($aws_key != "") {
                    $s3 = new S3Client(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
                    ));
                    $bucket = AMAZON_BUCKET;
                    try {
                        if ($aws_key != '') {
                            $result = $s3->deleteObject(array(
                                'Bucket' => $bucket,
                                'Key' => SYLLABUS . '/' . $aws_key
                            ));
                        }
                        $this->my_custom_functions->delete_data(TBL_NOTE_FILES, array("id" => $syllabus_file_list['id']));
                    } catch (S3Exception $e) {

                        $encode[] = array(
                            "msg" => "Operation Failed",
                            "status" => "true"
                        );
                    }
                }
            }
        }
        $condition = array('id' => $syllabus_id);

        $delete = $this->my_custom_functions->delete_data(TBL_SYLLABUS, $condition);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Syllabus successfully deleted.');
            redirect("school/user/manageSyllabus");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/manageSyllabus");
        }
    }

    function deleteIndividualSyllabusFile() {
        $indv_file_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $syllabus_file_list = $this->my_custom_functions->get_details_from_id($indv_file_id, TBL_NOTE_FILES);

        if (!empty($syllabus_file_list)) {

            //foreach ($notice_file_data as $notice_file_list) {echo $notice_file_list['id'];die;
            $file_url = $syllabus_file_list['file_url'];

            $fileUrlArray = explode("/", $file_url);
            $aws_key = $fileUrlArray[count($fileUrlArray) - 1];

            if ($aws_key != "") {
                $s3 = new S3Client(array(
                    'version' => 'latest',
                    'region' => 'ap-south-1',
                    'credentials' => array(
                        'key' => AWS_KEY,
                        'secret' => AWS_SECRET,
                    ),
                ));
                $bucket = AMAZON_BUCKET;
                try {
                    if ($aws_key != '') {
                        $result = $s3->deleteObject(array(
                            'Bucket' => $bucket,
                            'Key' => SYLLABUS . '/' . $aws_key
                        ));
                    }
                    $delete = $this->my_custom_functions->delete_data(TBL_NOTE_FILES, array("id" => $syllabus_file_list['id']));
                } catch (S3Exception $e) {

                    $encode[] = array(
                        "msg" => "Operation Failed",
                        "status" => "true"
                    );
                }
            }
            //}
        }

        if ($delete) {
            $this->session->set_flashdata("s_message", 'file successfully deleted.');
            redirect("school/user/editSyllabus/" . $this->my_custom_functions->ablEncrypt($syllabus_file_list['note_id']));
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
            redirect("school/user/editSyllabus/" . $this->my_custom_functions->ablEncrypt($syllabus_file_list['note_id']));
        }
    }

    function generalInformation() {
        $data['page_title'] = 'Manage general information';
        $data['general_info'] = $this->my_custom_functions->get_multiple_data(TBL_GENERAL_INFO, 'and school_id = "' . $this->session->userdata('school_id') . '"and status = 1');
        $this->load->view('school/manage_general_info', $data);
    }

    function addGeneralInformation() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $info_text = array(
                'school_id' => $this->session->userdata('school_id'),
                'info_text' => $this->input->post('info_text'),
                'status' => 1
            );

            $update_info = $this->my_custom_functions->insert_data($info_text, TBL_GENERAL_INFO);

            if ($update_info) {
                $this->session->set_flashdata("s_message", 'Information successfully Added.');
                redirect("school/user/generalInformation");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/generalInformation");
            }
        } else {
            $data['page_title'] = 'Add general information';
            $this->load->view('school/add_info', $data);
        }
    }

    function editGeneralInformation() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $info_id = $this->input->post('info_id');
            $info_text = array(
                'info_text' => $this->input->post('info_text')
            );
            $condition = array('id' => $info_id);
            $update_info = $this->my_custom_functions->update_data($info_text, TBL_GENERAL_INFO, $condition);

            if ($update_info) {
                $this->session->set_flashdata("s_message", 'Information successfully updated.');
                redirect("school/user/generalInformation");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/generalInformation");
            }
        } else {
            $data['page_title'] = 'Edit general information';
            $data['general_info'] = $this->my_custom_functions->get_multiple_data(TBL_GENERAL_INFO, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $this->load->view('school/edit_general_info', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Manage main fees structure records
    ////////////////////////////////////////////////////////////////////////////
    function manageFeesStructure() {

        $data['page_title'] = 'Manage fees structure';

        $data['fees_structure'] = $this->School_user_model->get_fees_structures_for_session();

        $this->load->view('school/manage_fees_structure', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Add main fees structure record for a class
    ////////////////////////////////////////////////////////////////////////////
    function addFeesStructure() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $semester_id = $this->input->post('semester_id');
            $cycle_id = $this->input->post('cycle_id');
            $general_comment = $this->input->post('comment');

            $existing_fees_for_this_semester = $this->my_custom_functions->get_perticular_count(TBL_FEES_STRUCTURE, 'and school_id="' . $this->session->userdata('school_id') . '" and semester_id="' . $semester_id . '"');

            if ($existing_fees_for_this_semester > 0) {

                $this->session->set_flashdata("e_message", 'There already exists a fees structure for this semester.');
                redirect("school/user/manageFeesStructure");
            } else {

                $add_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'semester_id' => $semester_id,
                    'cycle_type' => $cycle_id,
                    'general_comment' => $general_comment
                );
                $fees_structure_id = $this->my_custom_functions->insert_data_last_id($add_data, TBL_FEES_STRUCTURE);

                redirect('school/user/feesStructureBreakup/' . $this->my_custom_functions->ablEncrypt($fees_structure_id));
            }
        } else {

            $data['page_title'] = 'Add fees structure';

            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

            $this->load->view('school/add_fees_structure', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit main fees structure record for a class
    ////////////////////////////////////////////////////////////////////////////
    function editFeesStructure() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $update = $this->School_user_model->update_fees_structure();

            if ($update) {

                $this->session->set_flashdata("s_message", 'Fees structure successfully updated.');
                redirect("school/user/manageFeesStructure");
            } else {

                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/manageFeesStructure");
            }
        } else {

            $data['page_title'] = 'Edit fees structure';

            $fees_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure_detail'] = $this->my_custom_functions->get_details_from_id($fees_id, TBL_FEES_STRUCTURE);

            $this->load->view('school/edit_fees_structure', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Delete main fees structure record as well as the breakups
    ////////////////////////////////////////////////////////////////////////////
    function deleteFeesStructure() {

        $fees_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));

        $delete = $this->School_user_model->delete_fees_structure($fees_id);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Fees Structure successfully Deleted.');
            redirect("school/user/manageFeesStructure");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageFeesStructure");
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Add break ups to a main fees structure
    ////////////////////////////////////////////////////////////////////////////
    function feesStructureBreakup() {

        $data['page_title'] = 'Fees structure break up';

        if ($this->input->post('submit')) {

            $this->School_user_model->insert_fees_breakup();

            $this->session->set_flashdata("s_message", "Successfully added fees structure breakups.");
            redirect('school/user/editFeesStructureBreakup/' . $this->my_custom_functions->ablEncrypt($this->input->post('fees_id')));
        } else {

            $fees_structure_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure'] = $this->my_custom_functions->get_details_from_id($fees_structure_id, TBL_FEES_STRUCTURE);

            $this->load->view('school/add_fees_structure_breakup', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit break ups of a main fees structure
    ////////////////////////////////////////////////////////////////////////////
    function editFeesStructureBreakup() {

        $data['page_title'] = 'Fees structure break up';

        if ($this->input->post('submit')) {

            $this->School_user_model->update_fees_breakup();

            $this->session->set_flashdata("s_message", "Successfully updated fees structure breakups.");
            redirect('school/user/editFeesStructureBreakup/' . $this->my_custom_functions->ablEncrypt($this->input->post('fees_id')));
        } else {

            $fees_structure_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure'] = $this->my_custom_functions->get_details_from_id($fees_structure_id, TBL_FEES_STRUCTURE);
            $data['breakups'] = $this->my_custom_functions->get_multiple_data(TBL_FEES_STRUCTURE_BREAKUPS, ' and fees_id="' . $fees_structure_id . '"');

            $this->load->view('school/edit_fees_structure_breakup', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit break ups of a main fees structure
    ////////////////////////////////////////////////////////////////////////////
    function manualFeesBreakups() {

        $data['page_title'] = 'Set Fees Manually';

        // Update the fees
        if ($this->input->post('submit')) {

            $this->School_user_model->manuallly_update_fees_breakup();

            $this->session->set_flashdata("s_message", "Fees structure breakup updated manually.");
            redirect('school/user/manualFeesBreakups/' . $this->my_custom_functions->ablEncrypt($this->input->post('student_id')));
        }
        // Update the manual fees setting
        else if ($this->input->post('setting_submit')) {

            $this->School_user_model->update_manual_fees_setting();

            $this->session->set_flashdata("s_message", "Manual fees setting has been updated successfully.");
            redirect('school/user/manualFeesBreakups/' . $this->my_custom_functions->ablEncrypt($this->input->post('setting_student_id')));
        } else {

            if ($this->uri->segment(4)) {

                $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
                $data['student_details'] = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                $data['student_semesters'] = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));
                                
                if($this->input->post('manual_fees_semester')) {
                    
                    $data['fees_structure'] = $this->my_custom_functions->get_details_from_id("", TBL_FEES_STRUCTURE, array("school_id" => $this->session->userdata('school_id'), "semester_id" => $this->input->post('manual_fees_semester')));

                    if (!empty($data['fees_structure'])) {
                        $data['breakups'] = $this->School_user_model->get_fees_breakups_for_manual_fees($data['fees_structure']['id'], $student_id);
                    }                    
                }

                $this->load->view('school/manually_set_fees_breakups', $data);
            } else {
                redirect('school/user/manageStudent');
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Insert fees from edit student panel
    ////////////////////////////////////////////////////////////////////////////
    function receiveStudentFees() {

        $data['page_title'] = 'Receive Student Fees';

        // Insert fees
        if ($this->input->post('submit')) {

            $payment = $this->School_user_model->insert_fees_for_student();

            if ($payment) {
                $this->session->unset_userdata("receiveFeesBreakupId");
                $this->session->set_flashdata("s_message", 'Successfully received student fees.');
                redirect("school/user/receiveStudentFees/" . $this->input->post('student_id'));
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/receiveStudentFees/" . $this->input->post('student_id'));
            }
        } else {

            if ($this->uri->segment(4)) {

                $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
                $data['student_details'] = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                $student_semesters = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));

                if (!empty($student_semesters)) {

                    $data['fees_data'] = $this->School_user_model->get_fees_breakups_for_receiving_fees($student_id);

                    if (!empty($data['fees_data'])) {
                        if ($this->input->post('fees_breakup')) {
                            $this->session->set_userdata("receiveFeesBreakupId", $this->input->post('fees_breakup'));
                        }

                        if ($this->uri->segment(5)) {
                            $due_fees_breakup_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
                            $this->session->set_userdata("receiveFeesBreakupId", $due_fees_breakup_id);
                        }

                        if ($this->session->userdata("receiveFeesBreakupId")) {
                            $data['breakup_details'] = $this->School_user_model->get_fees_breakup_details($this->session->userdata("receiveFeesBreakupId"), $student_id);
                        }
                    }
                }

                $data['recent_payments'] = $this->School_user_model->get_recent_payments_of_student($student_id);

                $this->load->view('school/receive_student_fees', $data);
            } else {
                redirect('school/user/manageStudent');
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get calculated fees depending on different scenario
    ////////////////////////////////////////////////////////////////////////////
    function calculate_fees_amount() {

        $fees_amounts = $this->my_custom_functions->calculate_fees_amounts($this->input->post('fees_amount'), $this->input->post('fees_id'), $this->input->post('breakup_id'));
        $json = json_encode($fees_amounts);

        echo $json;
    }

    function downloadFilenotice() {
        $file_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }

    function downloadFilesyllabus() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');


        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;
        //}
    }

    function permission() {
        $staff_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $data["permissions"] = $this->School_user_model->get_permissions();
        $data["login"] = $this->School_user_model->get_login_details($staff_id);
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = 'Manage Permission';
        $this->load->view('school/permission', $data);
    }

    public function check_existing_permission($uid, $pid) {

        $response["exper"] = $this->School_user_model->get_existing_permission($uid, $pid);

        return $response;
    }

    public function update_permission() {

        if ($this->input->post('submit')) {

            $data = $this->input->post('type');

            $user = $this->input->post('login_id');

            //print_r($_POST);die;

            $this->School_user_model->remove_permission($user);

            $rs = $this->School_user_model->create_permission($data, $user);

            //redirect('setting/manage_login');
            $staff_id = $this->my_custom_functions->ablEncrypt($user);
            if ($rs) {
                $this->session->set_flashdata("s_message", 'permission successfully updated');
                redirect("school/user/permission/" . $staff_id);
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("school/user/permission/" . $staff_id);
            }
        }
    }

    function accessDenied() {
        $data['page_title'] = 'access denied';
        $this->load->view('school/permission_denied', $data);
    }

//    function tempTeacherAssign() {
//        $data['post_data'] = array();
//        if ($this->input->post('submit') && $this->input->post('submit') != '') {
//
//            $data['post_data'] = array('teacher_id' => $this->input->post('teacher_id'), 'post_date' => $this->input->post('class_date'));
//
//            $day_num = date('N', strtotime($this->my_custom_functions->database_date($this->input->post('class_date'))));
//
//            $data['classes_list'] = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE, 'and school_id = "' . $this->session->userdata('school_id') . '" and day_id = "' . $day_num . '" and teacher_id = "' . $this->input->post('teacher_id') . '" order by period_start_time ASC');
//        }
//
//        $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '" and staff_type = 1');
//        $data['page_title'] = 'Temporary teacher assign';
//        $this->load->view('school/manage_temp_teacher', $data);
//    }

    function tempTeacherAssign() {
        $data['post_data'] = array();
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $data['post_data'] = array('teacher_id' => $this->input->post('teacher_id'), 'post_date' => $this->input->post('class_date'), 'available_teacher' => $this->input->post('available_teacher'));

            $day_num = date('N', strtotime($this->my_custom_functions->database_date($this->input->post('class_date'))));

            $data['classes_list'] = $this->School_user_model->get_teacher_list_temp_assign($day_num);
        }

        $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '" and staff_type = 1');
        $data['page_title'] = 'Temporary teacher assign';
        $this->load->view('school/manage_temp_teacher', $data);
    }

    function assignTempTeacher() {
        //echo "<pre>";print_r($_POST);die;
        $teacher_list = $this->input->post('teacher_id');
        $period_id_list = $this->input->post('period_id');
        $class_assign_date = $this->input->post('class_assign_date');
        //echo "<pre>";print_r($teacher_list);
        $day_num = date('N', strtotime($this->my_custom_functions->database_date($class_assign_date)));


        foreach ($teacher_list as $key => $teacher_id) {

            if ($teacher_id != '') {

                $period_detail = $this->my_custom_functions->get_details_from_id($key, TBL_TIMETABLE);
                $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $period_detail['class_id'] . '" and session_id = "' . $this->session->userdata('session_id') . '"');
                $check_classes = $this->my_custom_functions->get_perticular_count(TBL_TEMP_TIMETABLE, 'and day_id = "' . $day_num . '" and class_date = "' . $this->my_custom_functions->database_date($class_assign_date) . '" and class_id = "' . $period_detail['class_id'] . '" and semester_id = "' . $semester_id . '" and section_id = "' . $period_detail['section_id'] . '" and subject_id = "' . $period_detail['subject_id'] . '"');
                if ($check_classes > 0) {
                    $update_data = array(
                        'teacher_id' => $teacher_id,
                    );

                    $condition = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'day_id' => $day_num,
                        'class_id' => $period_detail['class_id'],
                        'semester_id' => $semester_id,
                        'section_id' => $period_detail['section_id'],
                        'subject_id' => $period_detail['subject_id'],
                        'class_date' => $this->my_custom_functions->database_date($class_assign_date)
                    );
                    $update = $this->my_custom_functions->update_data($update_data, TBL_TEMP_TIMETABLE, $condition);

                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN   /////////////////////////////////////

                    $class = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $period_detail['class_id'] . '"');
                    $section = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $period_detail['section_id'] . '"');
                    $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $period_detail['subject_id'] . '"');

                    $email_data = $this->config->item("temporary_class");
                    $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='temporary_class'");
                    $from_email = $this->config->item($from_email_variable);
                    $subject = $email_data['subject'];
                    $addressing_user = $email_data['addressing_user'];
                    $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                        'Class' => $class,
                        'Section' => $section,
                        'Subject' => $subject_name,
                        'date' => $class_assign_date
                            )
                    );

                    $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                        'unsubscribe' => '<a href="javascript:;" target="_blank">unsubscribe</a>'
                            )
                    );
                    $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                        'institute_name' => 'Team Bidyaaly'
                            )
                    );
                    $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                        'systemtext' => SYSTEM_TEXT
                            )
                    );
                    $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                    //echo $full_mail;die ;

                    $this->my_custom_functions->SendNotification($teacher_id, array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_TEMP_CLASS_ASSIGN); // done notification
                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN END   /////////////////////////////////////
                } else {
                    $class_data = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'day_id' => $day_num,
                        'semester_id' => $semester_id,
                        'class_id' => $period_detail['class_id'],
                        'section_id' => $period_detail['section_id'],
                        'period_start_time' => $period_detail['period_start_time'],
                        'period_end_time' => $period_detail['period_end_time'],
                        'period_name' => $period_detail['period_name'],
                        'subject_id' => $period_detail['subject_id'],
                        'teacher_id' => $teacher_id,
                        'attendance_class' => $period_detail['attendance_class'],
                        'status' => $period_detail['status'],
                        'class_date' => $this->my_custom_functions->database_date($class_assign_date)
                    );

                    $insert_data = $this->my_custom_functions->insert_data($class_data, TBL_TEMP_TIMETABLE);

                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN   /////////////////////////////////////

                    $class = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $period_detail['class_id'] . '"');
                    $section = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $period_detail['section_id'] . '"');
                    $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $period_detail['subject_id'] . '"');
                    $email_data = $this->config->item("temporary_class");
                    $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='temporary_class'");
                    $from_email = $this->config->item($from_email_variable);
                    $subject = $email_data['subject'];
                    $addressing_user = $email_data['addressing_user'];
                    $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                        'Class' => $class,
                        'Section' => $section,
                        'Subject' => $subject_name,
                        'date' => $class_assign_date
                            )
                    );

                    $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                        'unsubscribe' => '<a href="javascript:;" target="_blank">unsubscribe</a>'
                            )
                    );
                    $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe);


                    $this->my_custom_functions->SendNotification($teacher_id, array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_TEMP_CLASS_ASSIGN); // done notification
                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN END   /////////////////////////////////////
                }
            }
        }
        $this->session->set_flashdata("s_message", 'Teacher successfully assigned');
        redirect("school/user/tempTeacherAssign/");
    }

    /*function attendanceReport() {
        $data['page_title'] = 'Manage atendance report';
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $this->load->view('school/attendance_report', $data);
    }

    function generateAttendanceReport() {

        $data['page_title'] = 'Atendance report';

        if ($this->input->post('report_type') == 1) {

            $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
            $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
            $data['classSection'] = $classname . '' . $sectionname;
            $data['report_classwise'] = $this->School_user_model->get_attendance_report_classwise();
            $this->load->view('school/report_classwise', $data);
        } else {
            $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
            $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
            $data['classSection'] = $classname . '' . $sectionname;
            $data['report_studentwise'] = $this->School_user_model->get_attendance_report_studentwise();

            $this->load->view('school/report_studentwise', $data);
        }
    }*/
    
    /*function activityReport() {
        $data['page_title'] = 'Manage activity report';
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, ' and school_id = "' . $this->session->userdata('school_id') . '" and is_deleted = 0');

        $this->load->view('school/activity_report', $data);
    }

    function generateActivityReport() {
        $data['page_title'] = 'Activity report';
        if ($this->input->post('report_type') == 1) {
            $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
            $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
            $data['classSection'] = $classname . '' . $sectionname;
            $data['teacher_activity'] = $this->School_user_model->get_teacher_activity_report();
            $this->load->view('school/teacher_activity', $data);
        } else if ($this->input->post('report_type') == 2) {
            //echo "<pre>";print_r($_POST);die;
            $data['disry_count_list'] = $this->School_user_model->diary_report_by_teacher();
            $this->load->view('school/student_activity', $data);
        }
    }*/

//    function searchMoveStudent() {
//        $data['page_title'] = 'Move Student';
//        $data['students'] = $this->School_user_model->get_student_list();
//        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
//        $this->load->view('school/search_move_student', $data);
//    }
//
//    function moveStudents() {
//        //echo "<pre>";print_r($_POST);
//        $student_list = $this->input->post('student');
//        foreach ($student_list as $student_id) {
//            $update_data = array(
//                'class_id' => $this->input->post('class'),
//                'semester_id' => $this->input->post('semester'),
//                'section_id' => $this->input->post('section')
//            );
//            $condition = array('id' => $student_id);
//            $this->my_custom_functions->update_data($update_data, TBL_STUDENT, $condition);
//        }
//        $this->session->set_flashdata("s_message", 'Students successfully moved');
//        redirect("school/user/manageStudent");
//    }

    function get_student_list() {

        $class_id = $this->input->post('class_id');
        $sec_id = $this->input->post('section_id');

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class_id') . '" and session_id = "' . $this->session->userdata('session_id') . '"');

        $get_student_list = $this->my_custom_functions->get_student_list(array('semester_id' => $semester_id, 'section_id' => $sec_id));


        echo '<option value="">Select Student</option>';
        foreach ($get_student_list as $student_list) {

            echo '<option value="' . $student_list['id'] . '">' . $student_list['name'] . '</option>';
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Bulk student upload
    ////////////////////////////////////////////////////////////////////////////
    function bulkUploadStudent() {
        
        if ($this->input->post('submit') AND ( $this->input->post('submit') == "Submit")) {
            
            $CI = & get_instance();
            $CI->load->database();
            $hostname = $CI->db->hostname;
            $database = $CI->db->database;
            $username = $CI->db->username;
            $password = $CI->db->password;
            $mysqli = new mysqli($hostname, $username, $password, $database);

            $up_path = 'csv/student_output.csv';
            $this->convertXLStoCSV($_FILES, $up_path);
            $down_path = 'csv/student_output.csv';
            $handle = fopen($down_path, "r");

            $total_rows = 0;
            $row_inserted = 0;
            $counter = 0;
            $err_msg = '';
            $error = '';
            $student_deleted = array();
            
            $allowed_license = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'no_of_license', 'and id = "' . $this->session->userdata('school_id') . '"');

            while ($line = fgetcsv($handle, 1000, ",")) {
                $flag = 0;

                if ($counter > 1) {
                    $lineno = $counter + 1;

                    $count_total_students = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '"');

                    if ($allowed_license > $count_total_students) {
                        
                        $registration_no = $line[0];
                        if ($registration_no == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Registration No. should not be blank. <br>';
                        }
                        
                        $name = $line[1];
                        if ($name == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Name should not be blank. <br>';
                        } else if (!preg_match("/^[a-zA-Z\.\-\_ ]*$/", $name)) {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Client Name is incorrect. First Name was written as ' . $name . '. <br>';
                        }
                        
                        $date_of_birth = $line[2];
                        if ($date_of_birth == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Date of birth should not be blank. <br>';
                        }

                        $gender = $line[3];
                        if ($gender == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Gender should not be blank. <br>';
                        } else if ($gender == 'M') {
                            $gender_name = 1;
                        } else if ($gender == 'F') {
                            $gender_name = 2;
                        } else {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . '. Invalid gender format.<br>';
                        }

                        $father_name = $line[4];
                        $mother_name = $line[5];

//                        $class_name = $line[6];
//                        $class_id = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and  class_name = "' . $class_name . '"');
//                        if ($class_id == '') {
//                            $flag = 1;
//                            $error .= 'Line No. ' . $lineno . ': Class should not be blank. <br>';
//                        }
                        //if ($class_id != '') {

//                        $semester_name = $line[6];
//                        $semester = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and semester_name = "' . $semester_name . '"');
//                        if ($semester_name == '') {
//                            $flag = 1;
//                            $error .= 'Line No. ' . $lineno . ': Section should not be blank. <br>';
//                        }
//                        if ($semester == '') {
//                            $flag = 1;
//                            $error .= 'Line No. ' . $lineno . ': Incorrect semester name. <br>';
//                        } else {
//                            $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id="' . $semester . '" ');
//                        }
                        
                        $semester = $this->input->post('semester');
                        $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id="' . $semester . '" ');

                        $section_name = $line[6];
                        $section = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_name = "' . $section_name . '"');
                        if ($section_name == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Section should not be blank. <br>';
                        }
                        if ($section == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Incorrect section name. <br>';
                        }
//                        } else {
//                            $flag = 1;
//                            $error .= 'Line No. ' . $lineno . ': Incorrect class name. <br>';
//                        }
                        
                        $roll_no = $line[7];
                        if ($roll_no == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Roll no. should not be blank. <br>';
                        }

                        $address = $line[8];
                        $aadhar_no = $line[9];
                        $emergency_contact_name = $line[10];
                        $emergency_contact_no = $line[11];
                        $blood_group = $line[12];
                        $registered_contact_no = $line[13];
                        $status = 1;

                        /////////////////////// Converting the date of birth to password //////////////////////
                        $dob = explode('-', $date_of_birth);
                        $combine_dob = $dob[0] . '' . $dob[1] . '' . $dob[2];
                        $dob_password = password_hash($combine_dob, PASSWORD_DEFAULT);
                        
                        if ($registered_contact_no == '') {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': Registered contact no should not be blank. <br>';
                        }

                        $registration_no_chk = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and registration_no = "' . $registration_no . '"');
                        if ($registration_no_chk > 0) {
                            $flag = 1;
                            $error .= 'Line No. ' . $lineno . ': another student already registered with this registration number.<br>';
                        }
                                                
                        if ($flag == 0) {

                            $basic_data = array(
                                'school_id' => $this->session->userdata('school_id'),
                                'registration_no' => $registration_no,
                                'name' => $name,
                                'dob' => $this->my_custom_functions->database_date_dash($date_of_birth),
                                'gender' => $gender_name,
                                'father_name' => $father_name,
                                'mother_name' => $mother_name,
                                //'class_id' => $class_id,
                                //'semester_id' => $semester,
                                //'section_id' => $section,
                                //'roll_no' => $roll_no,
                                'status' => $status,
                            );

                            $student_id = $this->my_custom_functions->insert_data_last_id($basic_data, TBL_STUDENT);

//                            $query1 = "INSERT IGNORE INTO ".TBL_STUDENT."  SET school_id='" . $this->session->userdata('school_id') . "',registration_no = '" . $registration_no . "', name='" . mysqli_real_escape_string($mysqli, $name) . "',dob='" . $date_of_birth . "',gender='" . $gender . "',father_name='" . mysqli_real_escape_string($mysqli, $father_name) . "',mother_name='" . mysqli_real_escape_string($mysqli, $mother_name) . "',class_id='" . $class_name . "',section_id='" . $section . "',roll_no='" . $roll_no . "',status='" . $status . "'";
//                            //  echo $query1;die;
//                            $result = $mysqli->query($query1) or die(mysqli_error($mysqli));
                            $last_id = $student_id;
                            $row_inserted +=1;

                            $student_detail = array(
                                'student_id' => $student_id,
                                'address' => $address,
                                'aadhar_no' => $aadhar_no,
                                'emergency_contact_person' => $emergency_contact_name,
                                'emergency_contact_no' => $emergency_contact_no,
                                'blood_group' => $blood_group,
                            );


                            $student_details = $this->my_custom_functions->insert_data($student_detail, TBL_STUDENT_DETAIL);

                            $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id="' . $semester . '" ');

                            $student_semester_link_data = array(
                                'school_id' => $this->session->userdata('school_id'),
                                'student_id' => $student_id,
                                'class_id' => $class_id,
                                'semester_id' => $semester,
                                'section_id' => $section,
                                'roll_no' => $roll_no,
                                'enrollment_time' => time()
                            );
                            $this->my_custom_functions->insert_data($student_semester_link_data, TBL_STUDENT_ENROLLMENT);

//                        $query2 = "INSERT IGNORE INTO ".TBL_STUDENT_DETAIL."  SET student_id='" . $last_id . "',address='" . $address . "',aadhar_no='" . $aadhar_no . "',emergency_contact_person='" . $emergency_contact_name . "',emergency_contact_no='" . $emergency_contact_no . "',"
//                                    . "blood_group='".$blood_group."'";
//                            $results = $mysqli->query($query2) or die(mysqli_error($mysqli));


                            $parent_username = explode(',', $registered_contact_no);
                            foreach ($parent_username as $username) {
                                
                                $number = trim($username);
                                $username_exist_for_teacher = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $number . '" and type = "' . TEACHER . '"');
                                
                                if ($username_exist_for_teacher == 0) {
                                    
                                    $username_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $number . '" and type = "' . PARENTS . '"');
                                    
                                    if ($username_exist > 0) {
                                        
                                        $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'id', 'and username = "' . $number . '" and type = "' . PARENTS . '"');
                                    } else {
                                        
                                        $parent_username_data = array(
                                            'type' => PARENTS,
                                            'username' => $number,
                                            'password' => $dob_password,
                                            'status' => 1
                                        );
                                        $parent_id = $this->my_custom_functions->insert_data_last_id($parent_username_data, TBL_COMMON_LOGIN);
                                    }

                                    $parent_detail_data = array(
                                        'id' => $parent_id,
                                        'school_id' => $this->session->userdata('school_id'),
                                    );
                                    $parent_student_link_data = $this->my_custom_functions->insert_data($parent_detail_data, TBL_PARENT);

                                    $parent_student_link = array(
                                        'school_id' => $this->session->userdata('school_id'),
                                        'parent_id' => $parent_id,
                                        'student_id' => $last_id
                                    );
                                    $parent_student_link_data = $this->my_custom_functions->insert_data($parent_student_link, TBL_PARENT_KIDS_LINK);
                                } else {
                                    
                                    $del_student_array = array('id' => $last_id);
                                    $delete_student = $this->my_custom_functions->delete_data(TBL_STUDENT, $del_student_array);

                                    $del_student_detail_array = array('student_id' => $last_id);
                                    $delete_student_det = $this->my_custom_functions->delete_data(TBL_STUDENT_DETAIL, $del_student_detail_array);                                    
                                    $delete_student_enr = $this->my_custom_functions->delete_data(TBL_STUDENT_ENROLLMENT, $del_student_detail_array);
                                    
                                    $err_msg = 'Error(s) identified in excel file. Please click on  <a href="javascript:" style="font-size:18px;" onclick="toggle_div()">this link</a> to view error(s).<br>';
                                    $error .= 'Line No. ' . $lineno . ': Phone number is already in use. Please try with another number.<br>';

                                    $student_deleted[$last_id] = $last_id;
                                }
                            }
                        } else {
                            
                            $err_msg = 'Error(s) identified in excel file. Please click on  <a href="javascript:" style="font-size:18px;" onclick="toggle_div()">this link</a> to view error(s).<br>';
                        }
                    } else {

                        $err_msg = 'Error(s) identified in excel file. Please click on  <a href="javascript:" style="font-size:18px;" onclick="toggle_div()">this link</a> to view error(s).<br>';
                        $error .= 'Line No. ' . $lineno . ': No of license reached. Adding student not allowed.<br>';
                    }
                    $total_rows++;
                }
                $counter++;
            }
            
            fclose($handle);
            $mysqli->close();
            
            $row_inserted = $row_inserted - count($student_deleted);
            
            if ($row_inserted == '' || $row_inserted == 0) {
                $count_insert = 0;
            } else {
                $count_insert = $row_inserted;
            }
                        
            $data['error'] = $error;
            $data['err_msg'] = $err_msg;
            
            if($count_insert == $total_rows) {
                $data['counts'] = $count_insert . ' out of ' . $total_rows . ' records added.';
            } else {
                $data['err_msg'] = $count_insert . ' out of ' . $total_rows . ' records added.<br>'.$err_msg;
            }
            
            $data['page_title'] = 'Student bulk upload';
            $data['semester_list'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            
            $this->load->view('school/student_bulk_upload', $data);
        } else {
            
            $data['page_title'] = 'Student bulk upload';
            $data['semester_list'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            
            $this->load->view('school/student_bulk_upload', $data);
        }
    }

    function convertXLStoCSV($infiles, $outfile) {
        $infile = $infiles['adminphoto']['tmp_name'];
        $fileType = PHPExcel_IOFactory::identify($infile);
        $objReader = PHPExcel_IOFactory::createReader($fileType);

        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($infile);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save($outfile);
    }

    function notificationSettings() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $notification_type = $this->config->item('notification_type');
            $push_option = array();
            $email_option = array();
            $sms_option = array();

            $push_option = $this->input->post('push_notif');
            $email_option = $this->input->post('email_notif');
            $sms_option = $this->input->post('sms_notif');

            $check_for_settings = $this->my_custom_functions->get_perticular_count(TBL_NOTIFICATION_SETTINGS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            if ($check_for_settings > 0) {
                $where = array(
                    'school_id' => $this->session->userdata('school_id')
                );
                $this->my_custom_functions->delete_data(TBL_NOTIFICATION_SETTINGS, $where);
            }

            foreach ($notification_type as $key => $val) {
                if (!empty($push_option)) {
                    if (in_array($key, $push_option)) {
                        $push_notification = 1;
                    } else {
                        $push_notification = 0;
                    }
                } else {
                    $push_notification = 0;
                }

                if (!empty($email_option)) {
                    if (in_array($key, $email_option)) {
                        $email_notification = 1;
                    } else {
                        $email_notification = 0;
                    }
                } else {
                    $email_notification = 0;
                }

                if (!empty($sms_option)) {
                    if (in_array($key, $sms_option)) {
                        $sms_notification = 1;
                    } else {
                        $sms_notification = 0;
                    }
                } else {
                    $sms_notification = 0;
                }

                $data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'notification_type' => $key,
                    'push_notification' => $push_notification,
                    'email_notification' => $email_notification,
                    'sms_notification' => $sms_notification
                );
                $insert = $this->my_custom_functions->insert_data($data, TBL_NOTIFICATION_SETTINGS);
            }
            $this->session->set_flashdata("s_message", 'Settings saved successfully');
            redirect("school/user/notificationSettings");
        } else {
            $data['page_title'] = 'Notification setting';
            $data['settings'] = $this->my_custom_functions->get_multiple_data(TBL_NOTIFICATION_SETTINGS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/notification_settings', $data);
        }
    }

    function manageTerms() {
        $data['page_title'] = 'Manage Term';
        $data['term'] = $this->School_user_model->get_term_data();
        $this->load->view('school/manage_term', $data);
    }

    function addTerm() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            //echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("semester", "Semester", "trim|required");
            $this->form_validation->set_rules("term_name", "Term Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTerm");
            } else {
                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }


                $semester_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'semester_id' => $this->input->post('semester'),
                    'term_name' => $this->input->post('term_name'),
                    'combined_exam_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('combine_result_name')
                );
                //echo "<pre>";print_r($session_data);die;
                $add_term_data = $this->my_custom_functions->insert_data($semester_data, TBL_TERMS);
                if ($add_term_data) {
                    $this->session->set_flashdata("s_message", 'Term added successfully.');
                    redirect("school/user/manageTerms");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTerms");
                }
            }
        } else {
            $data['page_title'] = 'Add Term';
            $data['semester_list'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            $this->load->view('school/add_term', $data);
        }
    }

    function editTerm() {
        //echo "<pre>";print_r($_POST);die;
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("semester", "Semester", "trim|required");
            $this->form_validation->set_rules("term_name", "Term Name", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editTerm/" . $this->input->post('term_id'));
            } else {
                $term_id = $this->my_custom_functions->ablDecrypt($this->input->post('term_id'));
                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }


                $term_data = array(
                    'semester_id' => $this->input->post('semester'),
                    'term_name' => $this->input->post('term_name'),
                    'combined_exam_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('combine_result_name')
                );
                $condition = array('id' => $term_id);
                $edit_term_data = $this->my_custom_functions->update_data($term_data, TBL_TERMS, $condition);
                if ($edit_term_data) {
                    $this->session->set_flashdata("s_message", 'Term updated successfully.');
                    redirect("school/user/manageTerms");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("school/user/manageTerms");
                }
            }
        } else {

            $data['page_title'] = 'Edit Term';
            $term_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['term'] = $this->my_custom_functions->get_details_from_id($term_id, TBL_TERMS);
            $data['semester_list'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            $this->load->view('school/edit_term', $data);
        }
    }

    public function deleteTerm() {
        $term_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $delete_term = array('id' => $term_id);
        $delete = $this->my_custom_functions->delete_data(TBL_TERMS, $delete_term);
        if ($delete) {
            $this->session->set_flashdata("s_message", 'Term deleted successfully.');
            redirect("school/user/manageTerms");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("school/user/manageTerms");
        }
    }

    function manageExam() {
        $data['page_title'] = 'Manage Exam';
        $data['exam'] = $this->School_user_model->get_exam_data();
        $this->load->view('school/manage_exam', $data);
    }

    function addExam() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("term", "Term", "trim|required");
            $this->form_validation->set_rules("exam_name", "Exam name", "trim|required");
            $this->form_validation->set_rules("exam_disp_name", "Exam display name", "trim|required");
            $this->form_validation->set_rules("exam_start_date", "Exam start date", "trim|required");
            $this->form_validation->set_rules("exam_end_date", "Exam end date", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addExam");
            } else {

                $publish_date = $this->my_custom_functions->database_date($this->input->post('publish_date'));
                $publish_hour_post = $this->input->post('start_hour');
                $publish_minute_post = $this->input->post('start_minute');
                $publish_meridian = $this->input->post('start_meridian');
                if ($publish_hour_post < 10) {
                    $publish_hour = '0' . $publish_hour_post;
                } else {
                    $publish_hour = $publish_hour_post;
                }
                if ($publish_minute_post < 10) {
                    $publish_minute = '0' . $publish_minute_post;
                } else {
                    $publish_minute = $publish_minute_post;
                }

                $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);


                $exam_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'term_id' => $this->input->post('term'),
                    'exam_name' => $this->input->post('exam_name'),
                    'exam_display_name' => $this->input->post('exam_disp_name'),
                    'exam_start_date' => $this->my_custom_functions->database_date($this->input->post('exam_start_date')),
                    'exam_end_date' => $this->my_custom_functions->database_date($this->input->post('exam_end_date')),
                    'publish_date' => $publish_date_time,
                );
                $exam_id = $this->my_custom_functions->insert_data_last_id($exam_data, TBL_EXAM);
                //$exam_id = 2;

                $subject_list = $this->input->post('subject');
                $fullmarks = $this->input->post('total_marks');
                $passmarks = $this->input->post('pass_marks');

                foreach ($subject_list as $key => $subject_id) {
                    $marks_data = array(
                        'school_id' => $this->session->userdata('school_id'),
                        'exam_id' => $exam_id,
                        'subject_id' => $subject_id,
                        'full_marks' => $fullmarks[$key],
                        'pass_marks' => $passmarks[$key]
                    );
                    //echo "<pre>";print_r($marks_data);
                    $marks = $this->my_custom_functions->insert_data($marks_data, TBL_EXAM_SCORES);
                }
                $this->session->set_flashdata("s_message", 'Exam record added successfully.');
                redirect("school/user/manageExam");
            }
        } else {

            $data['page_title'] = 'Add Exam';
            $data['term_list'] = $this->my_custom_functions->get_multiple_data(TBL_TERMS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/add_exam', $data);
        }
    }

    function editExam() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
//            echo "<pre>";
//            print_r($_POST);
            $exam_id = $this->my_custom_functions->ablDecrypt($this->input->post('exam_id'));
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("term", "Term", "trim|required");
            $this->form_validation->set_rules("exam_name", "Exam name", "trim|required");
            $this->form_validation->set_rules("exam_disp_name", "Exam display name", "trim|required");
            $this->form_validation->set_rules("exam_start_date", "Exam start date", "trim|required");
            $this->form_validation->set_rules("exam_end_date", "Exam end date", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/editExam/" . $this->input->post('exam_id'));
            } else {
                $publish_date = $this->my_custom_functions->database_date($this->input->post('publish_date'));
                $publish_hour_post = $this->input->post('start_hour');
                $publish_minute_post = $this->input->post('start_minute');
                $publish_meridian = $this->input->post('start_meridian');
                if ($publish_hour_post < 10) {
                    $publish_hour = '0' . $publish_hour_post;
                } else {
                    $publish_hour = $publish_hour_post;
                }
                if ($publish_minute_post < 10) {
                    $publish_minute = '0' . $publish_minute_post;
                } else {
                    $publish_minute = $publish_minute_post;
                }

                $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);


                $exam_data = array(
                    'term_id' => $this->input->post('term'),
                    'exam_name' => $this->input->post('exam_name'),
                    'exam_display_name' => $this->input->post('exam_disp_name'),
                    'exam_start_date' => $this->my_custom_functions->database_date($this->input->post('exam_start_date')),
                    'exam_end_date' => $this->my_custom_functions->database_date($this->input->post('exam_end_date')),
                    'publish_date' => $publish_date_time,
                );
                $condition = array('id' => $exam_id);

                $update_exam_data = $this->my_custom_functions->update_data($exam_data, TBL_EXAM, $condition);


                $update_score_id = $this->input->post('update_id');
                $update_total_marks = $this->input->post('update_total_marks');
                $update_pass_marks = $this->input->post('update_pass_marks');

                if (!empty($update_score_id)) {
                    foreach ($update_score_id as $key => $score_id) {
                        $update_score = array(
                            'full_marks' => $update_total_marks[$key],
                            'pass_marks' => $update_pass_marks[$key]
                        );
                        $condition_score = array('id' => $score_id);
                        $update_scores = $this->my_custom_functions->update_data($update_score, TBL_EXAM_SCORES, $condition_score);
                    }
                }
                $subject_list = $this->input->post('subject');
                $fullmarks = $this->input->post('total_marks');
                $passmarks = $this->input->post('pass_marks');
                if (!empty($subject_list)) {
                    foreach ($subject_list as $key => $subject_id) {
                        $marks_data = array(
                            'school_id' => $this->session->userdata('school_id'),
                            'exam_id' => $exam_id,
                            'subject_id' => $subject_id,
                            'full_marks' => $fullmarks[$key],
                            'pass_marks' => $passmarks[$key]
                        );
                        //echo "<pre>";print_r($marks_data);
                        $marks = $this->my_custom_functions->insert_data($marks_data, TBL_EXAM_SCORES);
                    }
                }
                $this->session->set_flashdata("s_message", 'Exam record added successfully.');
                redirect("school/user/manageExam");
            }
        } else {
            $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $data['page_title'] = 'Add Exam';
            $data['exam_data'] = $this->School_user_model->get_exam_data_list($exam_id);
            $data['term_list'] = $this->my_custom_functions->get_multiple_data(TBL_TERMS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
            $this->load->view('school/edit_exam', $data);
        }
    }

    function copyExam() {
        $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $data['page_title'] = 'Add Exam';
        $data['exam_data'] = $this->School_user_model->get_exam_data_list($exam_id);
        $data['term_list'] = $this->my_custom_functions->get_multiple_data(TBL_TERMS, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '"');
        $this->load->view('school/copy_exam', $data);
    }

    function manageScore() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $semester_id = $this->input->post('semester_id');
            $term_id = $this->input->post('term_id');
            $exam_id = $this->input->post('exam_id');
            $session_id = $this->session->userdata('session_id');
            $data['student_list'] = $this->School_user_model->get_student_data($semester_id);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['session_list'] = $this->my_custom_functions->get_multiple_data(TBL_SESSION, ' and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['post_data'] = array('class_id' => $class_id, 'section_id' => $section_id, 'semester_id' => $semester_id, 'term_id' => $term_id, 'exam_id' => $exam_id);

            $exam_student_list = array();
            foreach ($data['student_list'] as $row) {
                $exam_student_list[] = $row['id'];
            }
            $this->session->set_userdata("exam_student_list", $exam_student_list);
        } else {

            $data['period'] = array();
            $data['post_data'] = array('semester_id' => '', 'class_id' => '', 'section_id' => '', 'term_id' => '', 'exam_id' => '');
            $data['session_list'] = $this->my_custom_functions->get_multiple_data(TBL_SESSION, ' and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        }

        $data['page_title'] = 'Manage score';
        $this->load->view('school/manage_score', $data);
    }

    function get_exam_list() {

        $term_id = $this->input->post('term_id');
        $post_exam_id = $this->input->post('exam_id');
        $exam_list = $this->School_user_model->get_exam_list($term_id);
        echo '<option value="">Select Section</option>';
        $selected = '';
        foreach ($exam_list as $exam) {
            if ($post_exam_id == $exam['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $exam['id'] . '" ' . $selected . '>' . $exam['exam_name'] . '</option>';
        }
    }

    function get_term_list() {
        $semester_id = $this->input->post('semester_id');
        $term_list = $this->my_custom_functions->get_multiple_data(TBL_TERMS, 'and semester_id = "' . $semester_id . '"');
        $post_term_id = $this->input->post('term_id');
        echo '<option value="">Select term</option>';
        $selected = '';
        foreach ($term_list as $term) {
            if ($post_term_id == $term['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $term['id'] . '" ' . $selected . '>' . $term['term_name'] . '</option>';
        }
    }

    function get_semester_list_for_score() {
        $class_id = $this->input->post('class_id');
        $session_id = $this->input->post('session_id');
        $post_sem_id = $this->input->post('semester_id');

        $semester_list = $this->my_custom_functions->get_multiple_data(TBL_SEMESTER, 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '"');

        echo '<option value="">Select Section</option>';
        $selected = '';
        foreach ($semester_list as $semester) {
            if ($post_sem_id == $semester['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $semester['id'] . '" ' . $selected . '>' . $semester['semester_name'] . '</option>';
        }
    }

    function addScore() {

        if ($this->input->post('submit') == 'Save' || $this->input->post('submit') == 'Save & Next') {

            $student_id = $this->my_custom_functions->ablDecrypt($this->input->post('student_id'));
            $exam_id = $this->my_custom_functions->ablDecrypt($this->input->post('exam_id'));
            $score_data_list = $this->input->post('marks_obtain');
            if (!empty($score_data_list)) {
                foreach ($score_data_list as $score_id => $score) {
                    $check_score_id_exist = $this->my_custom_functions->get_perticular_count(TBL_SCORE, 'and school_id = "' . $this->session->userdata('school_id') . '" and student_id = "' . $student_id . '" and score_id = "' . $score_id . '"');
                    if ($check_score_id_exist > 0) {
                        $update_data = array('score' => $score);
                        $condition = array(
                            'school_id' => $this->session->userdata('school_id'),
                            'score_id' => $score_id,
                            'student_id' => $student_id
                        );
                        $update_score = $this->my_custom_functions->update_data($update_data, TBL_SCORE, $condition);
                    } else {
                        $score_data = array(
                            'school_id' => $this->session->userdata('school_id'),
                            'exam_id' => $exam_id,
                            'score_id' => $score_id,
                            'student_id' => $student_id,
                            'score' => $score
                        );
                        $insert_score_data = $this->my_custom_functions->insert_data($score_data, TBL_SCORE);
                    }
                }
            }

            $this->session->set_flashdata("s_message", 'Exam record added successfully.');
            if ($this->input->post('submit') == 'Save') {
                redirect("school/user/addScore/" . $this->input->post('student_id') . '/' . $this->input->post('exam_id'));
            } else if ($this->input->post('submit') == 'Save & Next') {
                $exam_student_list = $this->session->userdata("exam_student_list");
                $student_id = $this->my_custom_functions->ablEncrypt($exam_student_list[$this->input->post('next_student_index')]);

                if (count($exam_student_list) == $this->input->post('next_student_index')) {
                    redirect("school/user/manageScore");
                } else {
                    redirect("school/user/addScore/" . $student_id . '/' . $this->input->post('exam_id'));
                }
            }
        } else {

            $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
            $data['student_detail'] = $this->School_user_model->student_detail($student_id);
            $data['score_card'] = $this->School_user_model->score_card_detail($exam_id);
            $data['page_title'] = 'Add score';

            $exam_student_list = $this->session->userdata("exam_student_list");
            $student_index = array_search($student_id, $exam_student_list);
            $data['next_student_index'] = $student_index + 1;

            $this->load->view('school/score_detail', $data);
        }
    }

    function viewScore() {
        $data['page_title'] = 'View score';
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
        $data['student_detail'] = $this->School_user_model->student_detail($student_id);
        $data['score_card'] = $this->School_user_model->score_card_detail($exam_id);
        $this->load->view('school/view_score_detail', $data);
    }

    function delete_exam_subject() {
        $ecam_score_id = $this->my_custom_functions->ablDecrypt($this->input->post('exam_score_id'));
        $delete_score_condition_one = array('score_id' => $ecam_score_id);
        $delete_score = $this->my_custom_functions->delete_data(TBL_SCORE, $delete_score_condition_one);
        $delete_score_condition_two = array('id' => $ecam_score_id);
        $delete_score = $this->my_custom_functions->delete_data(TBL_EXAM_SCORES, $delete_score_condition_two);
        echo "success";
    }

    function customAds() {
        $data['page_title'] = '';
        $this->load->view('adv', $data);
    }

    function updateParentPhoneNumber() {
        //echo "<pre>";print_r($_POST);die;
        $parent_phone_no = $this->input->post('parent_pho_num');
        $parent_id = $this->input->post('parent_id_no');
        $student_id = $this->input->post('student_id_no');
        $check_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $parent_phone_no . '"');

        if ($check_exist > 0) {

            $flashdata = 'Phone number already associated with another student.';
            $msg_flag = "e_message";
        } else {
            $update_data = array(
                'username' => $parent_phone_no
            );
            $condition = array(
                'id' => $parent_id
            );
            $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
            $flashdata = 'Phone number successfully updated';
            $msg_flag = "s_message";
        }
        $this->session->set_flashdata("$msg_flag", "$flashdata");
        redirect('school/user/editStudent/' . $student_id);
    }                 
}
