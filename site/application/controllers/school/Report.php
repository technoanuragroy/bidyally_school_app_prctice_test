<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_is_logged_in')) {
            $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

            if ($chek_maintainance_mode['maintenance_mode'] == 1) {
                $this->session->set_flashdata("maintainance_message", $chek_maintainance_mode['maintenance_message']);
                redirect("school/user/logout");
            }
        }
        $this->my_custom_functions->check_school_security();
        $this->load->model("school/School_report_model");
        $this->load->library('Classes/PHPExcel');
        $user_type = $this->session->userdata('usertype');
        if ($user_type == 2) {
            $this->my_custom_functions->check_permission();
        }
        
        $prefs = array(
            'start_day' => 'monday',
            'month_type' => 'long',
            'day_type' => 'short',
            'show_next_prev' => true,
            //'next_prev_url' => base_url() . "admin/calendar/event",
            'template' => '
					   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender" >{/table_open}
                                           
                                            {heading_row_start}<tr>{/heading_row_start}
                                           
					   {heading_previous_cell}<th id="pre_link"><a href="{previous_url}" title="previous">&laquo;</a></th>{/heading_previous_cell}
					   
                                           {heading_title_cell}<th colspan="{colspan}" id="month_heading">{heading}</th>{/heading_title_cell}
					   {heading_next_cell}<th id="next_link"><a href="{next_url}" title="next">&raquo;</a></th>{/heading_next_cell}
					
					   {heading_row_end}</tr>{/heading_row_end}
                                           
					   {week_row_start}<tr>{/week_row_start}
					   {week_day_cell}<th>{week_day}</th>{/week_day_cell}
					   {week_row_end}</tr>{/week_row_end}
					
					   {cal_row_start}<tr>{/cal_row_start}
					   {cal_cell_start}<td width="5">{/cal_cell_start}
					
					   {cal_cell_content}<div class="date {custom_class}">{day}</div><div class="content_box sort_box_{day}"></div> {/cal_cell_content}
					   {cal_cell_content_today}<div class="highlight"><div class="date">{day}</div><div class="content_box sort_box_{day}"></div></div>{/cal_cell_content_today}
					
					   {cal_cell_no_content}<div class="date">{day}</div><div class="content_box"></div>{/cal_cell_no_content}
					   {cal_cell_no_content_today}<div class="highlight"><div class="highlight">{day}</div></div>{/cal_cell_no_content_today}
					
					   {cal_cell_blank}&nbsp;{/cal_cell_blank}
					
					   {cal_cell_end}</td>{/cal_cell_end}
					   {cal_row_end}</tr>{/cal_row_end}
					
					   {table_close}</table>{/table_close}'
        );
        $this->load->library('calendar', $prefs);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Fees payment report
    ///////////////////////////////////////////////////////////////////////////////
    public function feesPaymentReport() {

        $data['page_title'] = 'Fees payment report';

        if ($this->uri->segment(4) == "re") {
            $this->session->unset_userdata(array('fpr_start_date', 'fpr_end_date', 'fpr_semester_id', 'fpr_section_id', 'fpr_roll_no', 'fpr_name'));
            $this->session->set_userdata(array('fpr_start_date' => date("d-m-Y"), 'fpr_end_date' => date("d-m-Y")));
            redirect('school/report/feesPaymentReport');
        }

        if ($this->input->post("submit")) {

            $this->session->set_userdata(array(
                "fpr_start_date" => $this->input->post("start_date"),
                "fpr_end_date" => $this->input->post("end_date"),
                "fpr_semester_id" => $this->input->post("semester_id"),
                "fpr_section_id" => $this->input->post("section_id"),
                "fpr_name" => $this->input->post("name")
                    )
            );
            redirect('school/report/feesPaymentReport');
        } else {

            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            $data['report'] = $this->School_report_model->get_fees_payment_data();
            $this->load->view('school/report/fees_payment_report', $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Fees payment details
    ///////////////////////////////////////////////////////////////////////////////
    public function feesPaymentDetails() {

        $data['page_title'] = 'Fees payment details';

        $payment_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $data['payment_details'] = $this->my_custom_functions->get_details_from_id($payment_id, TBL_FEES_PAYMENT);
        $data['payment_settings'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $this->session->userdata('school_id')));

        $this->load->view('school/report/fees_payment_details', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Fees due report
    ///////////////////////////////////////////////////////////////////////////////
    public function feesDueReport() {

        $data['page_title'] = 'Fees due report';

        if ($this->uri->segment(4) == "re") {
            $this->session->unset_userdata(array('fdr_class_id', 'fdr_section_id', 'fdr_name'));
            redirect('school/report/feesDueReport');
        }

        if ($this->input->post("submit")) {

            $this->session->set_userdata(array(
                    "fdr_semester_id" => $this->input->post("semester_id"),
                    "fdr_section_id" => $this->input->post("section_id"),
                    "fdr_name" => $this->input->post("name")
                )
            );
            redirect('school/report/feesDueReport');
        } else {

            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            $data['report'] = $this->School_report_model->get_fees_due_data();
            //echo '<pre>'; print_r($data['report']); echo '</pre>'; die;
            $this->load->view('school/report/fees_due_report', $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Get student list from student name or roll no
    ///////////////////////////////////////////////////////////////////////////////
    function ajax_get_student_name() {

        $student_record = $this->School_report_model->ajax_get_student_name();
        echo json_encode($student_record);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Advance Attendance Report
    /// Based on the attendance of the teachers a report will be displayed 
    /// for a date range.
    ///////////////////////////////////////////////////////////////////////////////
    /*function teacherAttendanceReport() {

        $data['page_title'] = "Teacher Attendance Report";

        if ($this->input->post("submit")) {

            // Alert user if more than 90 days are selected from datepicker
            $numDays = (strtotime($this->input->post("to_date")) - strtotime($this->input->post("from_date"))) / (60 * 60 * 24);
            if ($numDays > 90) {
                $this->session->set_flashdata("e_message", "Sorry..!! Please select date range within 3months.");
                redirect("school/report/teacherAttendanceReport");
            }

            $session_data = array(
                "from_date" => $this->input->post("from_date"),
                "to_date" => $this->input->post("to_date"),
                "multiple_teacher" => $this->input->post("multiple_teacher") ? $this->input->post("multiple_teacher") : array(),
            );
            $this->session->set_userdata($session_data);

            $data['result'] = $this->School_report_model->get_teacher_attendance_report();
            $data['teachers'] = $this->School_report_model->get_teacher_list();
            //echo '<pre>'; print_r($data['result']); echo '</pre>'; die;
            $this->load->view("school/report/teacher_attendance_report", $data);
        } else {

            $data['teachers'] = $this->School_report_model->get_teacher_list();
            $this->load->view("school/report/teacher_attendance_report", $data);
        }
    }*/

    ///////////////////////////////////////////////////////////////////////////////
    /// Download teacher attendance data
    ///////////////////////////////////////////////////////////////////////////////
    function downloadTeacherAttendances() {

        $this->load->library('Classes/PHPExcel');
        $result = $this->School_report_model->get_teacher_attendance_report($this->uri->segment(4));

        $multiple_teacher = array();
        $fromDate = "";
        $toDate = "";

        if ($this->uri->segment(4) != "") {
            $encrArray = explode("_", $this->my_custom_functions->ablDecrypt($this->uri->segment(4)));

            if (count($encrArray) == 3) {
                $multiple_teacher = ($encrArray[0] != "") ? json_decode($encrArray[0], true) : array();
                $fromDate = $encrArray[1];
                $toDate = $encrArray[2];
            }
        }

        if ($fromDate == "" OR $toDate == "") {
            $multiple_teacher = $this->session->userdata("tcrrpt_multiple_teacher");
            $fromDate = $this->session->userdata("tcrrpt_start_date");
            $toDate = $this->session->userdata("tcrrpt_end_date");
        }

        $objPHPExcel = new PHPExcel();

        $report_title = 'Teacher Attendance Report : From ' . date('d M, Y', strtotime($fromDate)) . ' To ' . date('d M, Y', strtotime($toDate));

        // Add tab label to the sheet
        $objPHPExcel->getActiveSheet()->setTitle(mb_substr('Teacher Attendance Report', 0, 30, 'utf-8'));

        // Sheet heading in the first row
        $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', $report_title);

        // Column headings in the forth row
        $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Name');
        $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Date');
        $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Start Time');
        $objPHPExcel->getActiveSheet()->setCellValue('D3', 'End Time');
        $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Duration');

        // For single teacher, show stats of attendance
        if (count($multiple_teacher) == 1) {
            $total_days = count($result);
            $total_presents = 0;
            $total_absents = 0;
            $holidays = 0;
            $sundays = 0;
        }

        $row = 4;
        foreach ($result as $key => $attendances) {
            foreach ($attendances as $value) {

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $value['name']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, date("D - d M, Y", strtotime($key)));
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $value['start_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $value['end_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $value['duration']);

                if (count($multiple_teacher) == 1) {
                    if ($value['start_time'] == "-" AND $value['end_time'] == "-") {
                        $total_absents++;
                    } else {
                        $total_presents++;
                    }

                    if ($value['holiday'] == 1) {
                        $holidays++;
                    } else if ($value['holiday'] == 2) {
                        $sundays++;
                    }
                }

                $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);

                $row++;
            }
        }

        if (count($multiple_teacher) == 1) {
            $row++;
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Total Days : ' . $total_days);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'Total Presents : ' . $total_presents);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, 'Total Absents : ' . $total_absents . ' (Including Weekends)');
            $objPHPExcel->getActiveSheet()->mergeCells('D' . $row . ':E' . $row);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, 'Holidays : ' . $holidays . ' Sundays : ' . $sundays);
        }

        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $report_title . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    function studentAcademicReport() {
        
        $data['page_title'] = 'Student Academic Report';
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            
            $semester_id = $this->input->post('semester_id');
            $section_id = $this->input->post('section_id');
            
            $data['post_data'] = array('semester_id' => $semester_id, 'section_id' => $section_id);
            $data['student_list'] = $this->my_custom_functions->get_student_list(array('semester_id' => $semester_id, 'section_id' => $section_id));            
            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            
            if(!empty($data['student_list'])) {
                foreach($data['student_list'] as $key => $student) {
                    $data['student_list'][$key]['result'] = $this->School_report_model->get_academic_report($student['id'], $student['class_id'], $student['semester_id'], $this->session->userdata('session_id'));
                }
            }
            
            //echo '<pre>'; print_r($data['student_list']); echo '</pre>'; die;
            $this->load->view('school/report/academic_report', $data);
        } else {
                        
            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            
            $this->load->view('school/report/academic_report', $data);
        }
    }
    
    function resultDetails() {
        
        $data['page_title'] = 'Student Academic Report';
        
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $class_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
        $semester_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(6));
        $session_id = $this->session->userdata('session_id');
                
        $data['result'] = $this->School_report_model->get_academic_report($student_id,$class_id,$semester_id,$session_id);
                
        $this->load->view('school/report/school_academic_report',$data);        
    }

    ////////////////////////////////////////////////////////////////////////////
    // Teacher report
    ////////////////////////////////////////////////////////////////////////////
    function teacherReport() {
        
        $data['page_title'] = 'Manage teacher report';
        
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, ' and school_id = "' . $this->session->userdata('school_id') . '" and is_deleted = 0');
        
        $this->load->view('school/report/teacher_report', $data);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Generate teacher report
    ////////////////////////////////////////////////////////////////////////////
    function generateTeacherReport() {
        
        $data['page_title'] = 'Teacher report';
        
        $session_data = array(
            "tcrrpt_start_date" => $this->input->post("start_date"),
            "tcrrpt_end_date" => $this->input->post("end_date"),
            "tcrrpt_class_id" => $this->input->post("class"),
            "tcrrpt_section_id" => $this->input->post("section"), 
            "tcrrpt_teacher_id" => $this->input->post("teacher_id"), 
            "tcrrpt_multiple_teacher" => $this->input->post("multiple_teacher") ? $this->input->post("multiple_teacher") : array(),
        );
        $this->session->set_userdata($session_data);
        
        // Attendance report
        if($this->input->post('see_report') == 1) {
                    
            // Alert user if more than 90 days are selected from datepicker
            $numDays = (strtotime($this->input->post("end_date")) - strtotime($this->input->post("start_date"))) / (60 * 60 * 24);
            if ($numDays > 90) {
                $this->session->set_flashdata("e_message", "Sorry..!! Please select date range within 3months.");
                redirect("school/report/teacherReport");
            }            

            $data['result'] = $this->School_report_model->get_teacher_attendance_report();
            
            $this->load->view("school/report/teacher_attendance_report", $data);
        }
        // Activity report
        else {
            
            $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
            $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
            $data['classSection'] = $classname . '' . $sectionname;
            
            // For class/home/assignment notes
            if($this->input->post('note_type') != 4) { 
                $data['teacher_activity'] = $this->School_report_model->get_teacher_activity_report();
                $this->load->view('school/report/teacher_activity', $data);
            } 
            // For diary
            else {
                $data['teacher_activity'] = $this->School_report_model->get_teacher_activity_report_diary();
                $this->load->view('school/report/teacher_activity_diary', $data);
            }            
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Teacher activity details
    ////////////////////////////////////////////////////////////////////////////
    function teacherActivityDiaryDetails() {
        
        $data['page_title'] = 'Teacher activity details';
        
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        
        $teacher_id = "";
        if($this->session->userdata("tcrrpt_teacher_id")) {
            if($this->session->userdata("tcrrpt_teacher_id") != "") {
                $teacher_id = $this->session->userdata("tcrrpt_teacher_id");
            }
        }
        if($this->input->post('teacher_id')) {
            if($this->input->post('teacher_id') != "") {
                $teacher_id = $this->input->post('teacher_id');
            }
        }
                               
        $data['details'] = $this->School_report_model->teacher_diary_report_details_of_student($student_id, $teacher_id);
       
        $this->load->view('school/report/teacher_activity_diary_details', $data);
    }  
    
    ////////////////////////////////////////////////////////////////////////////
    // Student report 
    ////////////////////////////////////////////////////////////////////////////
    function studentReport() {
        
        $data['page_title'] = 'Manage student report';
        
        $data['class_list'] = $this->my_custom_functions->get_multiple_data(TBL_CLASSES, ' and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        
        $this->load->view('school/report/student_report', $data);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Generate student report 
    ////////////////////////////////////////////////////////////////////////////
    function generateStudentReport() {
        
        $data['page_title'] = 'Student report';

        $session_data = array(
            'stdrpt_start_date' => $this->input->post('start_date'),
            'stdrpt_end_date' => $this->input->post('end_date'),
            'stdrpt_class_id' => $this->input->post('class'),
            'stdrpt_section_id' => $this->input->post('section')                
        );
        $this->session->set_userdata($session_data);
        
        // Attendance report
        if($this->input->post('see_report') == 1) {
                    
            // Class wise attendacne report
            if($this->input->post('report_type') == 1) {

                $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
                $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
                $data['classSection'] = $classname . '' . $sectionname;
                $data['report_classwise'] = $this->School_report_model->get_attendance_report_classwise();

                $this->load->view('school/report/report_classwise', $data);
            } 
            // Student wise attendacne report
            else {

                $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
                $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
                $data['classSection'] = $classname . '' . $sectionname;
                $data['report_studentwise'] = $this->School_report_model->get_attendance_report_studentwise();

                $this->load->view('school/report/report_studentwise', $data);
            }
        }
        // Activity report
        else {
                   
            $classname = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = "' . $this->input->post('class') . '"');
            $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section') . '"');
            $data['classSection'] = $classname . '' . $sectionname;
            $data['diary_count_list'] = $this->School_report_model->diary_report_by_teacher();
            
            $this->load->view('school/report/student_activity', $data);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Generate student report 
    ////////////////////////////////////////////////////////////////////////////
    function attendance_detail() {
        
        $data = array();
        $student_id = $this->uri->segment(4);

        $month = $this->uri->segment(4);
        $year = date('Y');

        $date = $this->School_report_model->get_attendance_detail($month, $year);
        foreach ($date as $year => $atten_data) {
            foreach ($atten_data as $month => $attenda_data) {
                $data['atten_data_list'][] = array(
                    'year' => $year,
                    'month' => $month,
                    'event' => $attenda_data
                );
            }
        }
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = 'Atendance detail report';
        $this->load->view('school/report/report_studentwise_detail', $data);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Student activity details
    ////////////////////////////////////////////////////////////////////////////
    function studentActivityDetails() {
        
        $data['page_title'] = 'Student activity details';
        
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $teacher_id = $this->input->post('teacher_id') ? $this->input->post('teacher_id') : '';
                       
        $data['details'] = $this->School_report_model->diary_report_details_of_student($student_id, $teacher_id);
       
        $this->load->view('school/report/student_activity_details', $data);
    }   
    
    ////////////////////////////////////////////////////////////////////////////
    // Collect fees
    ////////////////////////////////////////////////////////////////////////////
    function collectFees() { 
        
        $data['page_title'] = 'Collect fees';

        if ($this->uri->segment(4) == "re") {
            $this->session->unset_userdata(array('fdr_class_id', 'fdr_section_id', 'fdr_name'));
            redirect('school/report/collectFees');
        }

        if ($this->input->post("submit")) {

            $this->session->set_userdata(array(
                    "fdr_semester_id" => $this->input->post("semester_id"),
                    "fdr_section_id" => $this->input->post("section_id"),
                    "fdr_name" => $this->input->post("name")
                )
            );
            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            $data['report'] = $this->School_report_model->get_fees_due_data();
            
            $semestername = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'semester_name', ' and id = "' . $this->input->post('semester_id') . '"');
            $sectionname = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', ' and id = "' . $this->input->post('section_id') . '"');
            $semestername = ($semestername != '') ? 'Semester: '.$semestername : '';
            $sectionname = ($sectionname != '') ? ', Section: '.$sectionname : '';
            
            $data['semesterSection'] = 'for ' . $semestername . $sectionname;
            
            $this->load->view('school/report/collect_fees', $data);
        } else {

            $data['semesters'] = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
            $data['semesterSection'] = '';
                        
            $this->load->view('school/report/collect_fees', $data);
        }
    }
}
