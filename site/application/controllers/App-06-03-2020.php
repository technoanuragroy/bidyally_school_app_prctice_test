<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model("Main_model");
    }

    public function index() {
        
        if($this->session->userdata('multiple_login_redirect')) {
            $this->session->unset_userdata('multiple_login_redirect');
        }
        
        if ($this->uri->segment(2) == WEB_KEY) {
            if ($this->input->cookie('student_id', true)) {

                $student_id = $this->input->cookie('student_id');
                $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                $session_student_data = array(
                    "student_id" => $student_id,
                    "class_id" => $student_detail['class_id'],
                    "section_id" => $student_detail['section_id'],
                    "school_id" => $student_detail['school_id'],
                    "student_flag" => 1
                );
                $this->session->set_userdata($session_student_data);
                redirect('parent/user/dashBoard');
            } else if ($this->input->cookie('teacher_id', true)) {

                $teacher_id = $this->input->cookie('teacher_id');
                $teacher_detail = $this->my_custom_functions->get_details_from_id($teacher_id, TBL_TEACHER);
                $session_data = array(
                    "teacher_id" => $teacher_id,
                    "school_id" => $teacher_detail['school_id'],
                    "school_is_logged_in" => 1
                );
                $this->session->set_userdata($session_data);
                redirect('teacher/user/dashBoard');
            } else {

                //echo "<pre>";print_r($this->session->all_userdata()); 
                $this->load->view('login');
            }
        } else {
            die();
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // App login page webview
    ////////////////////////////////////////////////////////////////////////////
    public function login() {
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "New Password", "trim|required");

            if ($this->form_validation->run() == false) { 
                
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("app");
                
            } else {
                
                $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

                if ($chek_maintainance_mode['maintenance_mode'] == 0) {

                    $response = $this->Main_model->login_check();

                    if ($response == TEACHER) { /// If credentials match with system  
                        
                        $cookie = array(
                            'name' => 'teacher_id',
                            'value' => $this->session->userdata('teacher_id'),
                            'expire' => '8650000',
                        );
                        $this->input->set_cookie($cookie);
                        
                        
                        $running_session_ids = $this->my_custom_functions->get_running_session();
                        if($running_session_ids == ''){
                           $running_session_ids = 0; 
                        }
                        
                        $running_sessions = array('running_sessions' => $running_session_ids);
                        $this->session->set_userdata($running_sessions);


                        redirect("teacher/user/dashBoard");
                    } elseif ($response == PARENTS) {

                        $children_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $this->session->userdata('parent_id') . '"');


                        if ($children_count == 1) {
                            $student_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'student_id', 'and parent_id = "' . $this->session->userdata('parent_id') . '"');
                            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and parent_id = "' . $this->session->userdata('parent_id') . '"');
                            $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                            $enrollment_detail = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));
                            //echo $this->db->last_query();
                            //echo "<pre>";print_r($enrollment_detail);die;
                            //$this->load->helper('cookie');
                            $cookie = array(
                                'name' => 'student_id',
                                'value' => $student_id,
                                'expire' => '8650000',
                            );
                            $this->input->set_cookie($cookie);
                            $session_student_data = array(
                                "student_id" => $student_id,
                                "class_id" => $enrollment_detail[0]['class_id'],
                                "section_id" => $enrollment_detail[0]['section_id'],
                                "school_id" => $school_id,
                            );
                            $this->session->set_userdata($session_student_data);
                            $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_student();
                            $session_list = array("session_list_by_student" => $school_sessions);
                            $this->session->set_userdata($session_list);
                        }
                        redirect("parent/user/dashBoard");
                    } else {

                        $this->session->set_flashdata("e_message", "Invalid username/password.");
                        redirect("app/" . WEB_KEY);
                    }
                } else {

                    $this->session->set_flashdata("e_message", $chek_maintainance_mode['maintenance_message']);
                    redirect("app/" . WEB_KEY);
                }
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Signup intermediate screen
    ////////////////////////////////////////////////////////////////////////////
    public function signUp() {
        
        if ($this->uri->segment(2) == WEB_KEY) {
            
            $this->load->view('signup');        
        } else {
            
            die();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Parent signup screen
    ////////////////////////////////////////////////////////////////////////////
    public function parentSignUp() {
        
        if ($this->uri->segment(2) == WEB_KEY) {
            
            if ($this->input->post('submit') && $this->input->post('submit') != '') {
                
                $this->load->library("form_validation");

                /// tbl_admins contents
                $this->form_validation->set_rules("materialMobileno", "Mobile Number", "trim|required|min_length[10]|max_length[10]");

                if ($this->form_validation->run() == false) { 
                    
                    $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                    redirect("parentSignUp/" . WEB_KEY);
                } else {
                    
                    $mobile_no = $this->input->post('materialMobileno');
                    $parent_id = $this->Main_model->check_parent_existance($mobile_no);
                    
                    if ($parent_id != "0" && $parent_id != "exist") {
                        
                        $otp = $this->my_custom_functions->generateOTP();
                        $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");

                        while ($check_pin > 0) {
                            $otp = $this->my_custom_functions->generateOTP();
                            $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");
                        }
                        $update_data = array('otp' => $otp);
                        $condition = array('id' => $parent_id);
                        $update_otp = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);

                        ///////// CODE FOR SENDING OTP THROUGH MESSAGE
                        $subject = 'otp for sign up.';
                        $message = $otp . ' is your one time password for sign up.';
                        //$msgen = urlencode($message);
                        $msgen = $message;
                        $sms_sent = $this->my_custom_functions->sendSMS($mobile_no, $msgen);
                        
                        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id="' . $parent_id . '"');
                        
                        if($sms_sent) {
                            // Insert into SMS log
                            $this->my_custom_functions->insert_data(array(
                                "user_id" => $parent_id,
                                "school_id" => $school_id,
                                "mobile_no" => $mobile_no,
                                "message" => $msgen,
                                "request_time" => time()
                            ), TBL_SMS_LOG);
                        }

                        //$this->my_custom_functions->SendNotification($parent_id, array("subject" => $subject, "message" => $message), NOTIFICATION_TYPE_OTP);// done notification
                        //$this->session->set_flashdata("s_message", "Your otp is " . $otp);
                        $this->session->set_flashdata("s_message", "Please enter otp.");
                        redirect("enterOTP");
                    } else if ($parent_id == "exist") {
                        
                        $this->session->set_flashdata("e_message", "You are already registered with Bidyaaly. Please login.");
                        redirect("parentSignUp/" . WEB_KEY);
                    } else if ($parent_id == "0") {
                        
                        $this->session->set_flashdata("e_message", "Invalid mobile number/not registered with bidyaaly.");
                        redirect("parentSignUp/" . WEB_KEY);
                    }
                }
            } else {
                
                //$this->load->view('register');
                $this->load->view('getmobileno');
            }
        } else {
            
            die();
        }
    }   

    function enterOTP() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("enter_otp", "Enter OTP", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("enterOTP");
            } else {
                $otp = $this->input->post('enter_otp');
                $check_correct_otp = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and id="' . $this->session->userdata('parent_id') . '" and otp = "' . $otp . '"');

                if ($check_correct_otp > 0) {
                    redirect('enterDetail');
                } else {
                    $this->session->set_flashdata("e_message", "Invalid otp");
                    redirect("enterOTP");
                }
            }
        } else {

            $this->load->view('enterOTP');
        }
    }

    function enterDetail() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("parent_name", "Enter Name", "trim|required");
            //$this->form_validation->set_rules("parent_email", "Enter Email", "trim|required");
            $this->form_validation->set_rules("parent_password", "Enter Password", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("enterOTP");
            } else {

                $parent_password = array(
                    'password' => password_hash($this->input->post('parent_password'), PASSWORD_DEFAULT),
                );

                $condition = array('id' => $this->session->userdata('parent_id'));
                $password = $this->my_custom_functions->update_data($parent_password, TBL_COMMON_LOGIN, $condition);




                $parent_detail = array(
                    'name' => $this->input->post('parent_name'),
                    'email' => $this->input->post('parent_email'),
                );

                $condition = array('id' => $this->session->userdata('parent_id'));

                $detail = $this->my_custom_functions->update_data($parent_detail, TBL_PARENT, $condition);
                if ($detail) {

                    $children_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $this->session->userdata('parent_id') . '"');
                    if ($children_count == 1) {
                        $student_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'student_id', 'and parent_id = "' . $this->session->userdata('parent_id') . '"');

                        $cookie = array(
                            'name' => 'student_id',
                            'value' => $student_id,
                            'expire' => '8650000',
                        );
                        $this->input->set_cookie($cookie);
                        $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
                        $session_student_data = array(
                            "student_id" => $student_id,
                            "class_id" => $student_detail['class_id'],
                            "section_id" => $student_detail['section_id'],
                            "school_id" => $student_detail['school_id'],
                            "student_flag" => 1
                        );
                        $this->session->set_userdata($session_student_data);

                        redirect('parent/user/dashBoard');
                    }
                }
            }
        } else {
            $this->load->view('enter_detail');
        }
    }

    function forgetPassword() {
        if ($this->uri->segment(2) == WEB_KEY) {
            if ($this->input->post('submit') && $this->input->post('submit') != '') {
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $return = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, " AND username LIKE '" . $username . "' AND status=1 ");

                if ($return) {

                    $user_details = $this->my_custom_functions->get_details_from_id("", TBL_COMMON_LOGIN, array("username" => $username));


                    if (!is_numeric($username)) {
                        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $user_details['id'] . '"');
                        $mobile_no = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'phone_no', 'and id = "' . $user_details['id'] . '"');
                    } else {
                        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'school_id', 'and id = "' . $user_details['id'] . '"');


                        $mobile_no = $username;
                    }

                    ///////////////////////////   SEND OTP TO USER   ////////////////////////////////

                    $otp = $this->my_custom_functions->generateOTP();
                    $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");

                    while ($check_pin > 0) {
                        $otp = $this->my_custom_functions->generateOTP();
                        $check_pin = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, "and otp='" . $otp . "'");
                    }
                    $update_data = array('otp' => $otp);
                    $condition = array('id' => $user_details['id']);
                    $update_otp = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
                    //echo $this->db->last_query();die;
                    ///////////////////  SET SESSION FOR USER REQUESTING FOR NEW PASSWORD   ////////////////
                    $session_data = array(
                        "user_id" => $user_details["id"],
                    );
                    $this->session->set_userdata($session_data);

                    ///////// CODE FOR SENDING OTP THROUGH MESSAGE


                    $message = $otp . ' is your one time password for create new password.';

                    $school_id = $this->Main_model->get_parent_list($user_details['id']);

                    if ($school_id != '') {
                        $sms_credits = $this->my_custom_functions->get_available_sms_credits_of_school($school_id);

                        if ($sms_credits > 0) {
                            $sms_sent = $this->my_custom_functions->sendSMS($mobile_no, $message);
                            
                            if($sms_sent) {
                                // Insert into SMS log
                                $this->my_custom_functions->insert_data(array(
                                    "user_id" => $user_details['id'],
                                    "school_id" => $school_id,
                                    "mobile_no" => $mobile_no,
                                    "message" => $message,
                                    "request_time" => time()
                                ), TBL_SMS_LOG);
                            }
                            
                            $this->session->set_flashdata("s_message", "Please enter otp.");
                            redirect("app/enterOTPforgotPassword");
                        } else {
                            $this->session->set_flashdata("e_message", "SMS limit exceeded at the organisation end you are registered with. Contact the organisation for assistance.");
                            redirect("forgetPassword/" . WEB_KEY);
                        }
                    } else {
                        $this->session->set_flashdata("e_message", "SMS limit exceeded at the organisation end you are registered with. Contact the organisation for assistance.");
                        redirect("forgetPassword/" . WEB_KEY);
                    }


                    //$this->session->set_flashdata("s_message", "Your otp is " . $otp);
                } else {

                    $this->session->set_flashdata("e_message", "You are not a registered user!");
                    redirect("forgetPassword/" . WEB_KEY);
                }
            } else {
                $this->load->view('forget_password');
            }
        } else {
            die();
        }
    }

    function enterOTPforgotPassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("enter_otp", "Enter OTP", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("app/enterNewPassword");
            } else {
                $otp = $this->input->post('enter_otp');
                $check_correct_otp = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and id="' . $this->session->userdata('user_id') . '" and otp = "' . $otp . '"');

                if ($check_correct_otp > 0) {
                    redirect('app/enterNewPassword');
                } else {
                    $this->session->set_flashdata("e_message", "Invalid otp");
                    redirect("app/enterOTPforgotPassword");
                }
            }
        } else {
            $this->load->view('enterFPWOTP');
        }
    }

    function enterNewPassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $user_id = $this->session->userdata('user_id');

            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $update_data = array('password' => $password);
            $condition = array('id' => $user_id);
            $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);

            $this->session->set_flashdata("s_message", "Password successfully updated. Please login here!");
            redirect("app/" . WEB_KEY);
        } else {
            // echo "<pre>";print_r($this->session->all_userdata());
            $this->load->view('reset_password');
        }
    }

////////////////////////////////////////////////////////////////////////////
//////////////  NOT USED 
//////////////////////////////////////////////////////////////////////////
    function userResetPassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $user_id = $this->my_custom_functions->ablDecrypt($this->input->post('user_id'));

            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $update_data = array('password' => $password);
            $condition = array('id' => $user_id);
            $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
            $this->session->set_flashdata("s_message", "Password successfully updated. Please login here!");
            redirect("app/" . WEB_KEY);
        } else {
            $user_type = $this->my_custom_functions->ablDecrypt($this->uri->segment(2));
            $user_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(3));
            $check_exist = $this->my_custom_functions->get_perticular_count(TBL_COMMON_LOGIN, 'and type = "' . $user_type . '" and id = "' . $user_id . '"');
            if ($check_exist == 0) {
                $this->session->set_flashdata("e_message", "Invalid link!");
                redirect("forgetPassword");
            }

            $this->load->view('reset_password');
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Redirected from api login
    ////////////////////////////////////////////////////////////////////////////
    function app_login() {
        
        $user_id_encr = $this->uri->segment(3);
        $user_id = $this->my_custom_functions->ablDecrypt($user_id_encr);
        $user_details = $this->my_custom_functions->get_details_from_id($user_id, TBL_COMMON_LOGIN);
                     
        // Update api log if redirected from multiple login screen in app
        if($this->input->get('log')) {
            
            $this->session->set_userdata('multiple_login_redirect', $this->my_custom_functions->ablEncrypt('app/app_login/' . $user_id_encr));
            
            $encrypted_apilog_id = $this->input->get('log');   
            $apilog_id = $this->my_custom_functions->ablDecrypt($encrypted_apilog_id);

            $data = array('user_id' => $user_id);
            $table = TBL_API_LOG;
            $where = array('id' => $apilog_id);
            $this->my_custom_functions->update_data($data, $table, $where);
        }
        
        if ($user_details['type'] == TEACHER) { 
            
            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $user_id . '"');
            $session_data = array(
                "teacher_id" => $user_id,
                "school_id" => $school_id,
                "school_is_logged_in" => 1
            );
            $this->session->set_userdata($session_data);
            $cookie = array(
                'name' => 'teacher_id',
                'value' => $user_id,
                'expire' => '8650000',
            );
            $this->input->set_cookie($cookie);
            
            $running_session_ids = 0;
            $running_session_ids = $this->my_custom_functions->get_running_session();
            
            $running_sessions = array('running_sessions' => $running_session_ids);
            $this->session->set_userdata($running_sessions);

            redirect("teacher/user/dashBoard");
            
        } elseif ($user_details['type'] == PARENTS) {

            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id = "' . $user_id . '"');
            $session_data = array(
                "parent_id" => $user_id,
                "school_id" => $school_id,
                "parent_is_logged_in" => 1
            );
            $this->session->set_userdata($session_data);
            $children_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $user_id . '"');


            if ($children_count == 1) {
                $student_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'student_id', 'and parent_id = "' . $user_id . '"');
                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and parent_id = "' . $user_id . '"');
                $student_detail = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));
                ;

                //$this->load->helper('cookie');
                $cookie = array(
                    'name' => 'student_id',
                    'value' => $student_id,
                    'expire' => '8650000',
                );
                $this->input->set_cookie($cookie);
                $session_student_data = array(
                    "student_id" => $student_id,
                    "class_id" => $student_detail[0]['class_id'],
                    "section_id" => $student_detail[0]['section_id'],
                    "school_id" => $school_id,
                );
                $this->session->set_userdata($session_student_data);
                $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_student();
                $session_list = array("session_list_by_student" => $school_sessions);
                $this->session->set_userdata($session_list);
            }
            
            redirect("parent/user/dashBoard");
            
        } 
//        else {
////                    $this->session->set_flashdata("e_message", "Invalid username/password.");
////                    redirect("app");
//        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // If multiple logins found against credentials
    ////////////////////////////////////////////////////////////////////////////
    function app_login_multiple() {
            
        // Redirect to respective panel if user press back button from thei panel after login                    
        if($this->session->userdata('multiple_login_redirect')) {
            redirect($this->my_custom_functions->ablDecrypt($this->session->userdata('multiple_login_redirect')));
        }        
        
        $encrypted_apilog_id = $this->uri->segment(3);   
        $encrypted_login_ids = $this->uri->segment(4);
                
        $login_ids = $this->my_custom_functions->ablDecrypt($encrypted_login_ids);
        
        $login_ids_array = explode(",", $login_ids);
        $record = array();
        
        if(!empty($login_ids_array)) {
            foreach($login_ids_array as $id) {
                
                if($id != "") {                    
                    $record[] = $this->my_custom_functions->get_details_from_id($id, TBL_COMMON_LOGIN);
                }
            } 
        }
        
        if(!empty($record) AND $encrypted_apilog_id != "") {
            
            $data['logins'] = $record;
            $data['apilog_id'] = $encrypted_apilog_id;

            $this->load->view('app_login_multiple', $data);
            
        } else {  
            
            redirect("app/" . WEB_KEY);            
        }
    }

}
