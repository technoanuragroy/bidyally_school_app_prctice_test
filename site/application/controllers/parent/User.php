<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once (APPPATH . 'libraries/razorpay/Razorpay.php');

use \Razorpay\Api\Api;

class User extends CI_Controller {

    function __construct() {
        parent::__construct();

        $chek_maintainance_mode = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);

        if ($chek_maintainance_mode['maintenance_mode'] == 1) {
            redirect('parent/user/logout_user');
        }

        if ($this->uri->segment(5) && $this->uri->segment(5) == DOWNLOAD_KEY) {
            
        } else {

            $this->my_custom_functions->check_parent_security();
        }
        $this->load->model("parent/Parent_user_model");
        $prefs = array(
            'start_day' => 'monday',
            'month_type' => 'long',
            'day_type' => 'short',
            'show_next_prev' => true,
            //'next_prev_url' => base_url() . "admin/calendar/event",
            'template' => '
					   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calender" >{/table_open}
                                           
                                            {heading_row_start}<tr>{/heading_row_start}
                                           
					   {heading_previous_cell}<th id="pre_link"><a href="{previous_url}" title="previous">&laquo;</a></th>{/heading_previous_cell}
					   
                                           {heading_title_cell}<th colspan="{colspan}" id="month_heading">{heading}</th>{/heading_title_cell}
					   {heading_next_cell}<th id="next_link"><a href="{next_url}" title="next">&raquo;</a></th>{/heading_next_cell}
					
					   {heading_row_end}</tr>{/heading_row_end}
                                           
					   {week_row_start}<tr>{/week_row_start}
					   {week_day_cell}<th>{week_day}</th>{/week_day_cell}
					   {week_row_end}</tr>{/week_row_end}
					
					   {cal_row_start}<tr>{/cal_row_start}
					   {cal_cell_start}<td width="5">{/cal_cell_start}
					
					   {cal_cell_content}<div class="date {custom_class}">{day}</div><div class="content_box sort_box_{day}"></div> {/cal_cell_content}
					   {cal_cell_content_today}<div class="highlight"><div class="date">{day}</div><div class="content_box sort_box_{day}"></div></div>{/cal_cell_content_today}
					
					   {cal_cell_no_content}<div class="date">{day}</div><div class="content_box"></div>{/cal_cell_no_content}
					   {cal_cell_no_content_today}<div class="highlight"><div class="highlight">{day}</div></div>{/cal_cell_no_content_today}
					
					   {cal_cell_blank}&nbsp;{/cal_cell_blank}
					
					   {cal_cell_end}</td>{/cal_cell_end}
					   {cal_row_end}</tr>{/cal_row_end}
					
					   {table_close}</table>{/table_close}'
        );
        $this->load->library('calendar', $prefs);

        // Delete temporary files of parent
        $this->deleteTemporaryParentFiles();
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Teacher area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {

        //echo "<pre>";print_r($this->session->all_userdata());
        //echo $this->input->cookie('student_id', true);
        if ($this->input->cookie('student_id', true)) {
            //echo $_COOKIE['student_id'];die;
            //$data['student_detail'] = $this->Parent_user_model->get_student_details_from_id($_COOKIE['student_id']);
            $data['student_detail'] = $this->my_custom_functions->get_semesters_of_a_student($_COOKIE['student_id'], $this->session->userdata('session_id'));
            $data['student'] = $this->my_custom_functions->get_details_from_id($_COOKIE['student_id'], TBL_STUDENT);
            //echo '<pre>';print_r($data);die;
            if (!empty($data['student_detail'])) {
                $student_flag = array("student_flag" => 1, 'class_id' => $data['student_detail'][0]['class_id'], 'section_id' => $data['student_detail'][0]['section_id']);
            } else {
                $student_flag = array("student_flag" => 1, 'class_id' => '', 'section_id' => '');
            }
            $this->session->set_userdata($student_flag);
            $data['page_title'] = 'Dashboard';
            $data['back_url'] = '';
            $this->load->view("parent/dashboard", $data);
        } else {
            //redirect('parent/user/select_student');
            $data['child_list'] = $this->Parent_user_model->get_child_list();
            $data['page_title'] = 'Select Student';
            $data['back_url'] = '';
            $this->load->view('parent/select_student', $data);
        }
    }

    function select_student() {
        //print_r($_COOKIE);die;
        $data['child_list'] = $this->Parent_user_model->get_child_list();
        $data['page_title'] = 'Select Student';
        $data['back_url'] = '';
        $this->load->view('parent/select_student', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Logout functions
    ////////////////////////////////////////////////////////////////////////////
    public function initial_logout() {
        
        $session_data = array('parent_id', 'parent_is_logged_in', 'student_flag', 'student_id');
        $this->session->unset_userdata($session_data);
        
        if($this->session->userdata('multiple_login_redirect')) {
            $this->session->unset_userdata('multiple_login_redirect');
        }
        
        unset($_COOKIE['student_id']);
        setcookie('student_id', '', time() - 3600, '/');
        delete_cookie('student_id');
        
        redirect("parent/user/logout_user");        
    }

    public function logout_user() {
        
        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("app/" . WEB_KEY);
    }

//    public function logout() {
//                
//        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
//        redirect("app/" . WEB_KEY);
//    }

//    public function logout() {
//
//        $session_data = array('parent_id', 'parent_is_logged_in','student_flag');
//        $this->session->unset_userdata($session_data);
//        delete_cookie('student_id');
//        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
//        redirect("app/".WEB_KEY);
//    }

    function changePassword() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $old_pass = $this->input->post('oldpass');
            $new_pass = $this->input->post('newpass');
            $confirm_pass = $this->input->post('confirmpass');

            $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE id = '" . $this->session->userdata('parent_id') . "' AND status=1";
            $query = $this->db->query($sql);

            $record = array();
            if ($query->num_rows() > 0) {
                $record = $query->row_array();

                if (password_verify($old_pass, $record['password'])) {
                    $update_data = array(
                        'password' => password_hash($new_pass, PASSWORD_DEFAULT)
                    );
                    $condition = array('id' => $this->session->userdata('parent_id'));
                    $update = $this->my_custom_functions->update_data($update_data, TBL_COMMON_LOGIN, $condition);
                } else {
                    $this->session->set_flashdata("e_message", 'Old password is wrong.');
                    redirect("parent/user/changePassword");
                }
            }
            if ($update) {
                $this->session->set_flashdata("s_message", 'Password successfully updated.');
                redirect("parent/user/changePassword");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong. Please try again later');
                redirect("parent/user/changePassword");
            }
        } else {
            $data['page_title'] = 'Change Password';
            $data['back_url'] = '';
            $this->load->view('parent/change_password', $data);
        }
    }

    function switch_student() {

        $student_id = $this->uri->segment(4);
        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and student_id = "' . $student_id . '"');
        //$parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'parent_id', 'and student_id = "' . $student_id . '"');
        $student_detail = $this->my_custom_functions->get_details_from_id($student_id, TBL_STUDENT);
        $enrollment_detail = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));

        $cookie = array(
            'name' => 'student_id',
            'value' => $student_id,
            'expire' => '86500000000',
        );
        $this->input->set_cookie($cookie);
        $session_student_data = array(
            "student_id" => $student_id,
            "class_id" => $enrollment_detail[0]['class_id'],
            "section_id" => $enrollment_detail[0]['section_id'],
            "school_id" => $school_id,
            //"parent_id" => $parent_id, 
            "student_flag" => 1
        );

        $this->session->set_userdata($session_student_data);

        $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_student();


        $session_list = array("session_list_by_student" => $school_sessions);
        $this->session->set_userdata($session_list);
        redirect('parent/user/dashBoard');
    }

    /////////////////////////////////////////////////////////////////////
    /////////// SCHOOL SYLLABUS
    ////////////////////////////////////////////////////////////////////

    function syllabus() {
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $syllabus_detail = $this->my_custom_functions->get_details_from_id('', TBL_SYLLABUS, array('school_id' => $school_id, 'class_id' => $class_id));
        if (!empty($syllabus_detail)) {
            $syllabus_id = $syllabus_detail['id'];
            $data['syllabus_file_list'] = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and type = 5 and note_id = ' . $syllabus_id . '');
            $data['syllabus_detail'] = $syllabus_detail;
        }
        $data['page_title'] = 'Syllabus';
        $data['back_url'] = '';
        $this->load->view('parent/syllabus', $data);
    }

    /////////////////////////////////////////////////////////////////////
    /////////// SCHOOL HOLIDAY
    ////////////////////////////////////////////////////////////////////
    function schoolCalander() {
        $school_id = $this->session->userdata('school_id');
        $data['school_calender'] = $this->Parent_user_model->get_school_calender($school_id);
        $data['page_title'] = 'Institute Calender';
        $data['back_url'] = '';
        //echo "<pre>";print_r($data);die;
        $this->load->view('parent/school_calender', $data);
    }

    /////////////////////////////////////////////////////////////////////
    /////////// DAILY CLASS WORKS
    ////////////////////////////////////////////////////////////////////

    function dailyWorks() {

        $data['class_list_daywise'] = $this->Parent_user_model->get_class_list_daywise();
        $data['page_title'] = 'Daily works';
        $data['back_url'] = '';
        if ($this->input->post('class_date')) {
            $data['display_class_date'] = $this->input->post('class_date');
        } else {
            $data['display_class_date'] = date('Y-m-d');
        }

        $this->load->view('parent/daily_works', $data);
    }

    function getClassNoteDetail() {
        $subject_id = $this->uri->segment(4);
        $data['note_detail'] = $this->Parent_user_model->get_class_note_detail($subject_id);
        $data['note_detail_home'] = $this->Parent_user_model->get_home_note_detail($subject_id);
        $data['note_detail_assignment'] = $this->Parent_user_model->get_assignment_note_detail($subject_id);
        if ($this->session->userdata('class_date') == date('Y-m-d')) {
            $data['page_title'] = "Today's class";
        } else {
            $data['page_title'] = date('D d M', strtotime($this->session->userdata('class_date')));
        }
        $data['back_url'] = base_url() . "parent/user/dailyWorks";
        $this->load->view('parent/works', $data);
    }

    function attendance() {
        $data['attendance_data'] = $this->Parent_user_model->get_attendance_data();
        $data['page_title'] = "Attendance Report";
        $data['back_url'] = '';
        $this->load->view('parent/attendance_list', $data);
    }

    function timeTable() {
        $data['schedule'] = $this->Parent_user_model->get_teacher_weekly_schedule();
        $data['page_title'] = "Timetable";
        $data['back_url'] = '';
        $this->load->view('parent/weekly_schedule', $data);
    }

    function generalInformation() {
        $data['general_info'] = $this->Parent_user_model->get_general_info();
        $data['page_title'] = "Institute Info";
        $data['back_url'] = '';
        $this->load->view('parent/general_info', $data);
    }

    function notice() {
        $school_id = $this->session->userdata('school_id');
        $data['school_notice'] = $this->Parent_user_model->get_school_notice($school_id);
        $data['page_title'] = "Notice";
        $data['back_url'] = '';
        $this->load->view('parent/school_notice', $data);
    }

    function noticeDetail() {
        $notice_id = $this->uri->segment(4);
        $data['school_notice_detail'] = $this->Parent_user_model->get_school_notice_detail($notice_id);
        $data['page_title'] = "Notice Details";
        $data['back_url'] = '';
        $this->load->view('parent/notice_detail', $data);
    }

    function downloadAllNotice() {

        $notice_id = $this->uri->segment(4);
        $get_all_notice = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and type = 4 and note_id = ' . $notice_id . '');
        mkdir('archive/' . $notice_id);
        mkdir('archive_zip/' . $notice_id);
        foreach ($get_all_notice as $notice_file) {
            $fileUrlArray = explode("/", $notice_file['file_url']);
            $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
            //echo $aws_key; echo '<br>';

            file_put_contents('archive/' . $notice_id . '/' . $aws_key, $notice_file['file_url']);
        }

        $filepath = 'archive_zip/' . $notice_id . '/zip-file-name.zip';
        shell_exec('zip -r ' . APPPATH . '../archive_zip/' . $notice_id . '/zip-file-name.zip archive/' . $notice_id . '/');

        header('Content-Disposition: attachment;filename="zip-file-name.zip"');
        header('Content-Description: File Transfer');
        header('Expires: 0');
        header('Content-Type: application/zip');
        header('Cache-Control: must-revalidate');

        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer

        readfile($filepath);
        foreach ($get_all_notice as $notice_file) {
            $fileUrlArray = explode("/", $notice_file['file_url']);
            $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
            //echo $aws_key; echo '<br>';
            //unlink('archive/'.$notice_id.'/'.$aws_key);
        }

//        unlink('archive_zip/'.$notice_id.'/zip-file-name.zip');
//        rmdir('archive/'.$notice_id);
//        rmdir('archive_zip/'.$notice_id);

        die;
    }

    function downloadFile() {
        $file_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_NOTE_FILES, 'file_url', 'and id = "' . $file_id . '" ');
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        if ($ext == 'jpg' || $ext == 'jpeg' || $ext = 'png') {
            header('Content-Type: image/jpg');
        }
        header('Content-Disposition: attachment;filename="' . $filepath . '"');
        header('Cache-Control: max-age=0');

        readfile($filepath);
        die;

//        header('Content-Description: File Transfer');
//        header('Content-Type: application/octet-stream');
//        header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
//        header('Expires: 0');
//        header('Cache-Control: must-revalidate');
//        header('Pragma: public');
//        header('Content-Length: ' . filesize($filepath));
//        flush(); // Flush system output buffer
//        readfile($filepath);
//        exit;
        //}
    }

    function attendance_detail() {
        $data = array();
        $month = $this->uri->segment(4);
        $year = date('Y');

        $date = $this->Parent_user_model->get_attendance_detail($month, $year);

        $data = array(
            'year' => $year,
            'month' => $month,
            'event' => $date
        );
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = "Attendance Details";
        $data['back_url'] = '';
        $this->load->view('parent/attendance_detail', $data);
    }

    function studentDiary() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $school_id = $this->session->userdata('school_id');
            $first_day_this_month = date('Y-m-01');
            $last_dayo_of_the_month = date('Y-m-t', strtotime(date('Y-m-d')));
            $start = strtotime($first_day_this_month . '00:00:00');
            $end = strtotime($last_dayo_of_the_month . '23:59:59');
            $diary_count_per_day = $this->my_custom_functions->get_perticular_count(TBL_DIARY, 'and school_id = "' . $this->session->userdata('school_id') . '" and student_id = "' . $this->session->userdata('student_id') . '" and status = 2 and issue_date >="' . $start . '" and issue_date <="' . $end . '" ');


            if ($diary_count_per_day < PARENT_DIARY_COUNT) {

                $class_id = $this->session->userdata('class_id');
                $session_id = $this->session->userdata('session_id');
                $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');


                $roll_no = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_ENROLLMENT, 'roll_no', 'and id = "' . $this->session->userdata('student_id') . '"');
                $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $this->session->userdata('student_id') . '"');
                $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $this->session->userdata('class_id') . '"');
                $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $this->session->userdata('section_id') . '"');
                $school_name = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"');
                $post_data = array(
                    'school_id' => $this->session->userdata('school_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    'subject_id' => $this->input->post('subject_id'),
                    'student_id' => $this->session->userdata('student_id'),
                    'semester_id' => $semester_id,
                    'class_id' => $this->session->userdata('class_id'),
                    'section_id' => $this->session->userdata('section_id'),
                    'roll_no' => $roll_no,
                    'heading' => $this->input->post('heading'),
                    'diary_note' => $this->input->post('diary_note'),
                    'issue_date' => time(),
                    'parent_id' => $this->session->userdata('parent_id'),
                    'status' => 2,
                    'parent_read_msg' => 2,
                    'teacher_read_msg' => 1
                );
                $this->my_custom_functions->insert_data($post_data, TBL_DIARY);

                // Email template
                $email_data = $this->config->item("send_diary_from_parent_to_teacher");
                $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='send_diary_from_parent_to_teacher'");
                $from_email = $this->config->item($from_email_variable);

                //$subject = $email_data['subject'];
                $subject = $this->my_custom_functions->sprintf_email($email_data['subject'], array(
                    'ActualDiaryHeading' => $this->input->post('heading'),
                        )
                );
                $addressing_user = $email_data['addressing_user'];

                $message_post = $this->input->post('diary_note');
                $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                    'studentname' => $student_name,
                    'Class' => $class_name,
                    'Section' => $section_name,
                    'SchoolName' => $school_name,
                    'ActualDiary' => $message_post,
                        )
                );
                $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                    'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                        )
                );
                $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                    'institute_name' => $school_name
                        )
                );
                $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                    'systemtext' => 'This is a system generated message, please do not reply to this email.'
                        )
                );
                $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                $this->my_custom_functions->SendNotification($this->input->post('teacher_id'), array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_DIARY); // done notification



                $this->session->set_flashdata("s_message", 'record successfully saved.');
                redirect('parent/user/studentDiary');
            } else {

                $this->session->set_flashdata("e_message", 'You can write upto 10 diary per day. Diary limit exceeds.');
                redirect('parent/user/studentDiary');
            }
        } else {

            $school_id = $this->session->userdata('school_id');
            $data['diarylist'] = $this->Parent_user_model->getDiaryList($school_id);
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, ' and school_id = "' . $this->session->userdata('school_id') . '"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data(TBL_SUBJECT, 'and school_id = "' . $this->session->userdata('school_id') . '" order by id ASC');
            $data['page_title'] = "Dairy";
            $data['back_url'] = '';
            $this->load->view('parent/diary', $data);
        }
    }

    function diaryDetail() {
        $diary_id = $this->uri->segment(4);
        $update_data = array('parent_read_msg' => 2);
        $condition = array('id' => $diary_id);
        $this->my_custom_functions->update_data($update_data, TBL_DIARY, $condition);

        $data['diary_detail'] = $this->Parent_user_model->get_diary_detail($diary_id);
        $data['diary_attachment'] = $this->Parent_user_model->get_diary_attachment($diary_id);
        //echo "<pre>";print_r($data);die;
        $data['page_title'] = "Diary Details";
        $data['back_url'] = '';

        $this->load->view('parent/diary_detail', $data);
    }

    function updateProfile() {
        $data['page_title'] = "Update Profile";
        $data['back_url'] = '';
        $this->load->view('parent/update_profile', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Fees structure
    ////////////////////////////////////////////////////////////////////////////
    function feeStructure() {

        $data['page_title'] = "Fees structure";
        $data['back_url'] = '';

        $data['payment_gateways'] = $this->Parent_user_model->get_active_payment_gateways();
        $data['payment_settings'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $this->session->userdata('school_id')));
        $data['fees_data'] = $this->Parent_user_model->get_fees_detail();

        if ($this->input->post('break_ups')) {
            $data['breakup_details'] = $this->Parent_user_model->get_fees_breakup_details($this->input->post('break_ups'));
        }

        $this->load->view('parent/fees_structure', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Common payment summary function
    ////////////////////////////////////////////////////////////////////////////
    function paymentSummary() {

        $data['page_title'] = "Payment Summary";
        $data['back_url'] = '';

        if ($this->input->post('payment_gateway_id') AND
                $this->input->post('breakup_id') AND
                $this->input->post('fees_id') AND
                $this->input->post('total_fees') AND
                $this->input->post('label') AND
                $this->input->post('amount') AND
                $this->input->post('option')) {

            $fees_json = $this->Parent_user_model->get_fees_json();

            // If a valid fees json returned
            if ($fees_json) {

                $data['fees_json'] = $fees_json;
                $data['fees_amounts'] = $this->my_custom_functions->calculate_fees_amounts($this->input->post('total_fees'), $this->input->post('fees_id'), $this->input->post('breakup_id'), $this->input->post('payment_gateway_id'));
                $data['payment_settings'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $this->session->userdata('school_id')));

                $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE, "semester_id", " and id='" . $this->input->post('fees_id') . "'");
                $data['semester_details'] = $this->my_custom_functions->get_details_from_id($semester_id, TBL_SEMESTER);

                // Setting the session variable to put a checking on next page
                $this->session->set_userdata("payment_total_amount", $data['fees_amounts']['total_amount']);

                $this->load->view("parent/payment_summary", $data);
            } else {

                redirect('parent/user/paymentCompleted/failure');
            }
        } else {

            redirect('parent/user/paymentCompleted/failure');
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Common payment gateway function
    ////////////////////////////////////////////////////////////////////////////
    function paymentGateway() {

        $data['page_title'] = "Payment Gateway";
        $data['back_url'] = '';

        if ($this->input->post('payment_gateway_id') AND $this->input->post('fees_id') AND $this->input->post('breakup_id') AND $this->input->post('total_amount')) {

            // Fees settings
            $payment_gateway_id = $this->my_custom_functions->ablDecrypt($this->input->post('payment_gateway_id'));
            $fees_id = $this->my_custom_functions->ablDecrypt($this->input->post('fees_id'));
            $breakup_id = $this->my_custom_functions->ablDecrypt($this->input->post('breakup_id'));
            $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE, "semester_id", " and id='" . $fees_id . "'");
            if ($this->input->post('manual_breakup_id')) {
                $manual_breakup_id = $this->my_custom_functions->ablDecrypt($this->input->post('manual_breakup_id'));
            } else {
                $manual_breakup_id = 0;
            }

            // Fees details
            $fees_amount = $this->input->post('fees_amount');
            $late_fine = $this->input->post('late_fine');
            $service_charges = $this->input->post('service_charges');
            $total_amount = $this->input->post('total_amount');
            $fees_json = $this->my_custom_functions->ablDecrypt($this->input->post('fees_json'));

            // Parent details
            $parent_details = $this->my_custom_functions->get_details_from_id("", TBL_PARENT, array("id" => $this->session->userdata('parent_id')));
            $parent_login_details = $this->my_custom_functions->get_details_from_id("", TBL_COMMON_LOGIN, array("id" => $this->session->userdata('parent_id'), "type" => PARENTS));
            $father_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, "father_name", " and id='" . $this->session->userdata('student_id') . "'");
            
            // Class info
            $class_info = $this->my_custom_functions->get_class_info_json_for_student($this->session->userdata('student_id'), $semester_id);

            // Checking to make sure there is no manipulation on fees
            if ($this->session->userdata("payment_total_amount") == $total_amount) {

                // Insert payment data
                $payment_data = array(
                    "fees_id" => $fees_id,
                    "fees_breakup_id" => $breakup_id,
                    "manual_fees_breakup_id" => $manual_breakup_id,
                    "fees" => $fees_json,
                    "school_id" => $this->session->userdata('school_id'),
                    "class_info" => $class_info,
                    "student_id" => $this->session->userdata('student_id'),
                    "parent_id" => $this->session->userdata('parent_id'),
                    "fees_amount" => $fees_amount,
                    "late_fine" => $late_fine,
                    "service_charges" => $service_charges,
                    "total_amount" => $total_amount,
                    "payment_datetime" => date("Y-m-d H:i:s"),
                    "payment_status" => PAYMENT_STATUS_PENDING,
                    "payment_gateway_id" => $payment_gateway_id,
                    "created_by" => PARENTS // Parent
                );
                $payment_id = $this->Parent_user_model->insert_payment($payment_data);

                // If any error occurs
                if ($payment_id == 0) {
                    redirect('parent/user/paymentCompleted/failure');
                }

                // If Razorpay is selected
                if ($payment_gateway_id == RAZORPAY_DB_ID) {

                    // Set the razorpay related sessions for completing the order
                    $razorpay_amount = $payment_data['total_amount'] * 100; // Need to convert the Rupees to Paise before sending it to Razorpay
                    $this->session->set_userdata(array(
                            "onetimeRazorPay" => array(
                                "description" => SITE_NAME . " Fees Payment",
                                "name" => $father_name,
                                "email" => $parent_details['email'],
                                "phone" => $parent_login_details['username'],
                                "payment_id" => $payment_id,
                                "display_currency" => DISPLAY_CURRENCY,
                                "display_amount" => $payment_data['total_amount'],
                                "amount" => $razorpay_amount
                            )
                        )
                    );

                    redirect("parent/user/rzp_PaymentProcess");
                } else {

                    redirect('parent/user/paymentCompleted/failure');
                }
            } else {

                redirect('parent/user/paymentCompleted/failure');
            }
        } else {

            redirect('parent/user/paymentCompleted/failure');
        }
    }

    function rzp_PaymentProcess() {

        $data['page_title'] = "Payment Process";
        $data['back_url'] = '';
        
        $data['razorpay_credentials'] = $this->my_custom_functions->get_razorpay_credentials($this->session->userdata('school_id'));
        
        $this->load->view("parent/razorpay/payment_process", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Razorpay payment specific function    
    /// For checkout from wrapper app using callback_url of razorpay
    ///////////////////////////////////////////////////////////////////////////////
    function rzp_OnetimePaidCallback() {

        $razorpay_credentials = $this->my_custom_functions->get_razorpay_credentials($this->session->userdata('school_id'));
        
        // If response from razorpay contains a payment id
        if ($this->input->post('razorpay_payment_id')) {

            // Fetch the payment on razorpay
            $api = new Api($razorpay_credentials['RAZORPAY_KEY_ID'], $razorpay_credentials['RAZORPAY_KEY_SECRET']);
            $payment = $api->payment->fetch($this->input->post('razorpay_payment_id'));

            if (isset($payment['notes']['payment_id'])) {

                $api = new Api($razorpay_credentials['RAZORPAY_KEY_ID'], $razorpay_credentials['RAZORPAY_KEY_SECRET']);
                $captured = $api->payment->fetch($this->input->post('razorpay_payment_id'))->capture(array('amount' => $payment['amount']));

                // If captured successful
                if ($captured->amount == $payment['amount']) {

                    $payment_details = $this->my_custom_functions->get_details_from_id("", TBL_FEES_PAYMENT, array("id" => $payment['notes']['payment_id']));
                    $payment_settings = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $payment_details['school_id']));

                    // Update main fees payment data
                    $actual_amount = $payment['amount'] / 100; // Need to convert Razorpay returned amount from Paise to Rupees
                    $payment_data = array(
                        "paid_amount" => $actual_amount,
                        "payment_datetime" => date("Y-m-d H:i:s"),
                        "payment_status" => PAYMENT_STATUS_PAID,
                        "payment_ref_id" => $payment['id']
                    );
                    $this->Parent_user_model->update_payment($payment_data, $payment['notes']['payment_id']);

                    // Transfer school fees from captured amount  
                    $transfer_amount = $payment_data['paid_amount'] - $payment_details['service_charges'];
                    $razorpay_amount = $transfer_amount * 100; // Need to convert the Rupees to Paise before sending it to Razorpay

                    if ($payment_settings['payment_gateway_reference_id'] != "") {
                        $transfer = $api->payment->fetch($this->input->post('razorpay_payment_id'))->transfer(array(
                                'transfers' => array(
                                    array(
                                        'account' => $payment_settings['payment_gateway_reference_id'],
                                        'amount' => $razorpay_amount,
                                        'currency' => DISPLAY_CURRENCY
                                    )
                                )
                            )
                        );

                        // Add transfer records to database
                        if (isset($transfer['items'])) {
                            foreach ($transfer['items'] as $item) {
                                $actual_amount = $item['amount'] / 100; // Need to convert Razorpay returned amount from Paise to Rupees
                                $transfer_data = array(
                                    "school_id" => $payment_details['school_id'],
                                    "payment_id" => $payment_details['id'],
                                    "razorpay_account_id" => $item['recipient'],
                                    "payment_ref_id" => $item['id'],
                                    "amount" => $actual_amount,
                                    "payment_datetime" => date("Y-m-d H:i:s")
                                );
                                $this->Parent_user_model->insert_transfer($transfer_data);
                            }
                        }
                    }

                    // Send email, push notification to parent
                    $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, "name", " and id='" . $payment_details['student_id'] . "'");

                    // Email template
                    $email_data = $this->config->item("fees_payment_success_mail_to_parent");
                    $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='fees_payment_success_mail_to_parent'");
                    $from_email = $this->config->item($from_email_variable);

                    // Email variables
                    $subject = $email_data['subject'];

                    $addressing_user = $email_data['addressing_user'];

                    $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                        'amount' => INDIAN_CURRENCY_CODE . $payment_data['paid_amount'],
                        'student_name' => $student_name,
                        'payment_date' => date("g:iA, d-m-Y")
                            )
                    );

                    $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                        'unsubscribe' => '<a href="javascript:;" target="_blank">unsubscribe</a>'
                            )
                    );
                    $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                        'institute_name' => 'Team Bidyaaly'
                            )
                    );
                    $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                        'systemtext' => 'This is a system generated message, please do not reply to this email.'
                            )
                    );

                    $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                    $this->my_custom_functions->SendNotification($payment_details['parent_id'], array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_FEES_PAYMENT);

                    redirect('parent/user/paymentCompleted/success');
                } else {

                    redirect('parent/user/paymentCompleted/failure');
                }
            } else {

                redirect('parent/user/paymentCompleted/failure');
            }
        } else {

            redirect('parent/user/paymentCompleted/failure');
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Common payment gateway success/failure function
    ////////////////////////////////////////////////////////////////////////////
    function paymentCompleted() {

        if ($this->uri->segment(4) == "success") {
            $data['page_title'] = "Payment Success";
        } else {
            $data['page_title'] = "Payment Failure";
        }
        $data['back_url'] = '';

        $this->load->view("parent/payment_completed", $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Fees payment history
    ////////////////////////////////////////////////////////////////////////////
    function feesPaymentHistory() {

        $data['page_title'] = "Fees Payment History";
        $data['back_url'] = '';

        $data['history'] = $this->Parent_user_model->get_parent_payment_history();

        $this->load->view('parent/fees_payment_history', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Individual payment details
    ////////////////////////////////////////////////////////////////////////////
    function feesPaymentDetails() {

        $data['page_title'] = "Fees Payment Details";
        $data['back_url'] = '';

        $payment_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $data['payment_details'] = $this->Parent_user_model->get_parent_payment_details($payment_id);
        $data['payment_settings'] = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $data['payment_details']['school_id']));

        $this->load->view('parent/fees_payment_details', $data);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Download payment receipt
    ////////////////////////////////////////////////////////////////////////////
    function downloadPaymentReceipt() {

        $student_payment_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $student_payment_id_array = explode("_", $student_payment_id);

        if (is_array($student_payment_id_array) AND count($student_payment_id_array) == 2) {

            $student_id = $student_payment_id_array[0];
            $payment_id = $student_payment_id_array[1];

            $payment_details = $this->Parent_user_model->get_parent_payment_details($payment_id);

            if (!empty($payment_details)) {
                if ($payment_details['student_id'] == $student_id) {

                    $payment_settings = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $payment_details['school_id']));
                    $school_details = $this->my_custom_functions->get_details_from_id($payment_details['school_id'], TBL_SCHOOL);
                    $student_details = $this->my_custom_functions->get_details_from_id($payment_details['student_id'], TBL_STUDENT);
                    $fees_details = $this->my_custom_functions->get_details_from_id($payment_details['fees_breakup_id'], TBL_FEES_STRUCTURE_BREAKUPS);

                    $paid_amount = $payment_details['paid_amount'];
                    $payment_handling_fees = $payment_details['service_charges'];
                    $display_amount = $paid_amount - $payment_handling_fees;
                    
                    $school_logo = ($school_details['file_url'] != "") ? $school_details['file_url'] : base_url() . '_images/notification_icon.png';
                    $semester = 'N/A';
                    $section = 'N/A';
                    $roll_no = 'N/A';
                    if ($payment_details['class_info'] != "") {
                        $class_info_array = json_decode($payment_details['class_info'], true);
                        if (is_array($class_info_array)) {
                            if (array_key_exists('semester', $class_info_array)) {
                                $semester = $class_info_array['semester'];
                            }
                            if (array_key_exists('section', $class_info_array)) {
                                $section = $class_info_array['section'];
                            }
                            if (array_key_exists('roll_no', $class_info_array)) {
                                $roll_no = $class_info_array['roll_no'];
                            }
                        }
                    }
                    $payment_mode = ($payment_details['created_by'] == 1) ? "Offline" : "Online";

                    include(APPPATH . "libraries/mpdf/mpdf.php");

                    $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');
                    $mpdf->SetTitle('Payment Receipt');
                    $mpdf->SetDisplayMode('fullpage');

                    $html =    '<htmlpagefooter name="MyFooter1">
                                    <table width="100%" style="vertical-align: bottom;">
                                        <tr>
                                            <td width="80%">
                                                <span style="font-size: 14px;">Powered by bidyaaly.com</span>
                                            </td>                                
                                        </tr>
                                    </table>
                                </htmlpagefooter>
                                <sethtmlpagefooter name="MyFooter1" value="on" />
                                <htmlpageheader name="MyHeader1"><div style="color: #525252;font-size: 14px;width: 100%;text-align: center;"></div></htmlpageheader>
                                <sethtmlpageheader name="MyHeader1" value="on" />';


                    $html .=   '<div style="width:100%;text-align:center;">            
                                    <div style="width: 100%; float: left; display: block; overflow: auto;">

                                        <div style="width: 100%; float: left; display: block; padding-bottom: 15px; border-bottom: 4px solid #333;">
                                            <div style="width: 20%; float: left; display: inline-block; text-align: left;">
                                                <span>
                                                    <img src="' . $school_logo . '" width="96" height="96">
                                                </span>    
                                            </div>
                                            <div style="width: 80%; float: left; display: inline-block; text-align: left; padding-top: 15px;">                                 
                                                <span style="font-weight: bold;">
                                                    ' . $school_details['name'] . '
                                                </span>  
                                                <br>
                                                <span style="font-weight: bold;">
                                                    ' . $school_details['address'] . '
                                                </span>  
                                                <br>
                                                <span style="font-weight: bold;">
                                                    Mobile: ' . $school_details['mobile_no'] . ', 
                                                    Email: ' . $school_details['email_address'] . '    
                                                </span>                                                             
                                            </div>
                                        </div>    

                                        <div style="width: 100%; float: left; display: block; margin-top: 25px; text-align: center;">                        
                                            <span style="font-weight: bold;">
                                                Payment Receipt
                                            </span>                                                       
                                        </div>

                                        <div style="width: 100%; float: left; display: block; margin-top: 25px;">     
                                            <div style="width: 50%; float: left; display: inline-block; text-align: left;">
                                                <span style="font-weight: bold;">
                                                    Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $student_details['name'] . '
                                                </span>   
                                                <br>
                                                <span style="font-weight: bold;">
                                                    Semester: &nbsp;&nbsp;' . $semester . '
                                                </span>
                                                <br>
                                                <span style="font-weight: bold;">
                                                    Section: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $section . '
                                                </span>
                                                <br>
                                                <span style="font-weight: bold;">
                                                    Roll No: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $roll_no . '
                                                </span>
                                            </div>   
                                            <div style="width: 50%; float: right; display: inline-block; text-align: left;">
                                                <span style="font-weight: bold;">
                                                    Payment Date: &nbsp;' . date("d-m-Y, g:ia", strtotime($payment_details['payment_datetime'])) . '
                                                </span>   
                                                <br>
                                                <span style="font-weight: bold;">
                                                    Receipt No: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $payment_details['id'] . '
                                                </span>
                                                <br>
                                                <span style="font-weight: bold;">                                
                                                </span>
                                            </div> 
                                        </div>

                                        <div style="width: 100%; float: left; display: block; line-height: 40px; margin-top: 25px; border: 2px solid #333; border-left: none; border-right: none;"> 
                                            <div style="width: 50%; float: left; display: inline-block; text-align: left;">
                                                <span style="font-weight: bold;">
                                                    Fees Term: ' . $fees_details['breakup_label'] . '
                                                </span> 
                                            </div>  
                                            <div style="width: 50%; float: right; display: inline-block; text-align: left;">
                                                <span style="font-weight: bold;">
                                                    Payment Mode: ' . $payment_mode . '
                                                </span>   
                                            </div> 
                                        </div>

                                        <div style="width: 100%; float: left; display: block; margin-top: 25px;">                             
                                            <table style="width: 100%; line-height: 40px;" border="0" cellspacing="0">
                                                <thead>
                                                    <tr style="background-color: #eee;">                                               
                                                        <th scope="col" style="text-align:left; padding: 6px 10px;">Fees Name</th>  
                                                        <th scope="col" style="text-align:right; padding: 6px 10px;">Amount</th>                                                
                                                    </tr>
                                                </thead>
                                                <tbody>';
                    
                                                    $fees_array = json_decode($payment_details['fees'], true);
                                                    foreach ($fees_array as $fees) {

                                                        $html .=   '<tr>
                                                                        <td style="text-align:left"><span style="font-weight: bold;">' . $fees['label'] . '</span></td>
                                                                        <td style="text-align:right"><span style="font-weight: bold;">' . INDIAN_CURRENCY_CODE . ' ' . number_format($fees['amount'], 2) . '</span></td>
                                                                    </tr>';
                                                    }

                                                    $html .=    '<tr>
                                                                    <td style="text-align:left; border-top: 2px solid #333;">
                                                                        <span style="font-weight: bold;">Fees Amount</span> 
                                                                    </td>  
                                                                    <td style="text-align:right; border-top: 2px solid #333;">
                                                                        <span style="font-weight: bold;">
                                                                            ' . INDIAN_CURRENCY_CODE . number_format($payment_details['fees_amount'], 2) . '
                                                                        </span>    
                                                                    </td>                                                    
                                                                </tr>

                                                                <tr>
                                                                    <td style="text-align:left; border-top: 2px solid #333;">
                                                                        <span style="font-weight: bold;">Late Fine</span> 
                                                                    </td>  
                                                                    <td style="text-align:right; border-top: 2px solid #333;">
                                                                        <span style="font-weight: bold;">
                                                                            ' . INDIAN_CURRENCY_CODE . number_format($payment_details['late_fine'], 2) . '
                                                                        </span>    
                                                                    </td>                                                    
                                                                </tr>';

                                                    /*if ($payment_settings['add_on_school_fees'] > 0 AND $payment_details['service_charges'] > 0) {
                                                        $html .=    '<tr>
                                                                        <td style="text-align:left; border-top: 2px solid #333;">
                                                                            <span style="font-weight: bold;">Payment Handling Fees</span> 
                                                                        </td>  
                                                                        <td style="text-align:right; border-top: 2px solid #333;">
                                                                            <span style="font-weight: bold;">
                                                                                ' . INDIAN_CURRENCY_CODE . number_format($payment_details['service_charges'], 2) . '
                                                                            </span>    
                                                                        </td>                                                    
                                                                    </tr>';
                                                    }*/

                                                    $html .=   '<tr>
                                                                    <td style="text-align:left; border-top: 2px solid #333;">
                                                                        <span style="font-weight: bold;">Paid Amount</span>                                                                 
                                                                    </td>  
                                                                    <td style="text-align:right; border-top: 2px solid #333;">
                                                                        <span style="font-weight: bold;">
                                                                            ' . INDIAN_CURRENCY_CODE . number_format($display_amount, 2) . '
                                                                            <br>
                                                                            ' . ucwords($this->my_custom_functions->convert_number_to_words(abs($display_amount))) . '
                                                                        </span>    
                                                                    </td>                                                    
                                                                </tr>                                                                                                                                  
                                                </tbody>
                                            </table>                                
                                        </div>                                                
                                    </div>                      
                                </div>';

                    $mpdf->WriteHTML($html);

                    $file_name = 'temp/Payment-Receipt-' . $this->my_custom_functions->ablEncrypt($student_id) . '.pdf';
                    $mpdf->Output($file_name, 'F');

                    $file_url = base_url() . $file_name;
                    header("Location: $file_url");

                    exit();
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Delete temporary files of parent(payment receipts)
    ////////////////////////////////////////////////////////////////////////////
    function deleteTemporaryParentFiles() {

        $paymentReceipt = 'temp/Payment-Receipt-' . $this->my_custom_functions->ablEncrypt($this->session->userdata("student_id")) . '.pdf';
        if (file_exists($paymentReceipt)) {
            unlink($paymentReceipt);
        }
    }

    function parentFeedback() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $parent_id = $this->session->userdata('parent_id');
            $from_email = DONOT_REPLY_EMAIL;
            $to = SITE_EMAIL;
            $subject = 'Parent Feedback';
            $message = $this->my_custom_functions->get_feedback_user_details($parent_id);
            $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $to, $subject, $message);
            $this->session->set_flashdata("s_message", "Feedback form successfully submitted. Thank you.");
            redirect('parent/user/parentFeedback');
        } else {
            //echo "<pre>";print_r($this->session->all_userdata());
            $data['page_title'] = "Parent Feedback";
            $data['back_url'] = '';
            $this->load->view('parent/parent_feedback', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Change main session from menu dropdown
    //////////////////////////////////////////////////////////////////////////// 
    function change_session() {

        if ($this->input->post("main_session_dropdown")) {
            $session_data = array("session_id" => $this->input->post("main_session_dropdown"));
            $this->session->set_userdata($session_data);
        }

        redirect("parent/user/dashboard");
        //redirect($_SERVER['HTTP_REFERER']);
    }

    function customAds() {
        $data['page_title'] = "";
        $data['back_url'] = '';
        $this->load->view('parent_adv', $data);
    }

    ///////////////////////////////// VIEW RESULT ///////////////////////////////////////

    function viewResult() {
        
        $data['result'] = $this->Parent_user_model->get_student_result();
        $data['school_details'] = $this->my_custom_functions->get_details_from_id($this->session->userdata("school_id"), TBL_SCHOOL);
        $data['student_details'] = $this->my_custom_functions->get_semesters_of_a_student($this->session->userdata("student_id"), $this->session->userdata('session_id'));
        //$enrollment_detail = $this->my_custom_functions->get_semesters_of_a_student($student_id,$this->session->userdata('session_id'));
        $data['page_title'] = "View Result";
        $data['back_url'] = '';
        $this->load->view('parent/view_result', $data);
    }

    ////////////////////////////////// DOWNLOAD RESULT//////////////////////////////////////////

    function result() {

        $student_id = $this->session->userdata("student_id");
        $result = $this->Parent_user_model->get_student_result();
        $school_details = $this->my_custom_functions->get_details_from_id($this->session->userdata("school_id"), TBL_SCHOOL);
        $student_details = $this->my_custom_functions->get_semesters_of_a_student($this->session->userdata("student_id"), $this->session->userdata('session_id'));
        $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $this->session->userdata("student_id") . '"');
        $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_details[0]['class_id'] . '"');
        $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_details[0]['section_id'] . '"');
        $roll_no = $student_details[0]['roll_no'];
        //echo "<pre>";print_r($student_details);die;
        $school_logo = ($school_details['file_url'] != "") ? $school_details['file_url'] : base_url() . '_images/notification_icon.png';

        include(APPPATH . "libraries/mpdf/mpdf.php");

        $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');
        $mpdf->SetTitle('Result');
        $mpdf->SetDisplayMode('fullpage');

        $html = '<div style="width:100%;text-align:center;">            
                        <div style="width: 100%; float: left; display: block; overflow: auto;">

                            <div style="width: 100%; float: left; display: block; padding-bottom: 15px; border-bottom: 3px solid #333;">
                                <div style="width: 20%; float: left; display: inline-block; text-align: left;">
                                    <span>
                                        <img src="' . $school_logo . '" width="96" height="96">
                                    </span>    
                                </div>
                                <div style="width: 80%; float: left; display: inline-block; text-align: left; padding-top: 15px;">                                 
                                    <span style="font-weight: bold;font-size:28px;">
                                        ' . $school_details['name'] . '
                                    </span>  
                                    <br>
                                    <span style="font-weight: bold;">
                                        ' . $school_details['address'] . '
                                    </span>  
                                    <br>
                                    <span style="font-weight: bold;">
                                        Mobile: ' . $school_details['mobile_no'] . ', 
                                        Email: ' . $school_details['email_address'] . '    
                                    </span>                                                             
                                </div>
                                <div class="width: 100%;float: left;text-align: left;padding: 15px 0px;">
                                <ul style="width: 100%;float: left;text-align: left;padding: 0px;margin: 0px;list-style:none;">
                                <li>
                                        <b>name:</b>
                                        <b>'.$student_name.'</b>
                                </li>
                                <li>
                                        <b>class:</b>
                                        <b>'.$class_name.', '.$section_name.'</b>
                                </li>
                                <li>
                                        <b>roll:</b>
                                        <b>'.$roll_no.'</b>
                                </li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div style="width: 100%; float: left; display: block; overflow: auto;">
                            <table border="1" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Subjects</th>';
        foreach ($result['terms'] as $term_id => $term_detail) {

            if ($term_detail['combined_exam_results'] == 1) {
                $colspan = count($term_detail['exams']) + 1;
            } else {
                $colspan = count($term_detail['exams']);
            }

            $html .= '<th colspan="' . $colspan . '">' . $term_detail['term_name'] . '</th>';
        }

        if ($result['semester_detail']['combined_term_results'] == 1) {
            $html .= '<th rowspan="2">' . $result['semester_detail']['combined_result_name'] . '</th>';
        }
        $html .= '</tr>
                                    <tr>';
        foreach ($result['terms'] as $term_id => $term_detail) {

            foreach ($term_detail['exams'] as $exam_id => $exam_detail) {
                $html .= '<th>' . $exam_detail['exam_name'] . '</th>';
            }

            if ($term_detail['combined_exam_results'] == 1) {
                $html .= '<th>' . $term_detail['combined_result_name'] . '</th>';
            }
        }
        $html .= '</tr>
                                </thead>
                                <tbody>';
        foreach ($result['subjects'] as $subject_id => $subject) {
            $html .= '<tr>
                                                        <td>' . $subject . '</td>';

            $semester_combined_score = 0;
            foreach ($result['terms'] as $term_id => $term_detail) {

                $term_combined_score = 0;
                foreach ($term_detail['exams'] as $exam_id => $exam_detail) {

                    if (array_key_exists($subject_id, $exam_detail['subjects'])) {

                        $score = $exam_detail['subjects'][$subject_id]['score'];
                        $term_combined_score += $score;
                        $semester_combined_score += $score;

                        $html .= '<td>' . $score . '</td>';
                    } else {

                        $html .= '<td>-</td>';
                    }
                }

                if ($term_detail['combined_exam_results'] == 1) {
                    $html .= '<td>' . $term_combined_score . '</td>';
                }
            }

            if ($result['semester_detail']['combined_term_results'] == 1) {
                $html .= '<td>' . $semester_combined_score . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</tbody>  
                            </table>
                        </div>';


        //echo $html;
        //die;
        $mpdf->WriteHTML($html);

        $file_name = 'temp/Result-' . $this->my_custom_functions->ablEncrypt($student_id) . '.pdf';
        //$mpdf->Output($file_name, 'F');
        $mpdf->Output($file_name, 'I');
        exit();
        $file_url = base_url() . $file_name;
        header("Location: $file_url");

        exit();
    }

}
