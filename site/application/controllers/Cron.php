<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


$file_name = "UserDelete";
$file = APPPATH . '/controllers/school/' . $file_name . '.php';
include_once($file);

class Cron extends UserDelete {

    function __construct() {
        parent::__construct();
        if ($this->uri->segment(3) != CRON_KEY) {
            die();
        }

        //parent::__construct();
        //$this->my_custom_functions->check_school_security();
        $this->load->model("Cron_model");
    }

    function index() {
        echo"here";
    }

    ///////////////////////////////////////////////////////////////////////////
    //////   CRON WILL RUN IN EVERY DAY 
    // http://server1/webdev/Bidyaaly/site/cron/improper_account_setup/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////

    function improper_account_setup() {
        $now = time();
        $school_list = $this->Cron_model->get_all_school_list();
        if (!empty($school_list)) {
            foreach ($school_list as $schools) {
                $school_id = $schools['id'];
                $count_teacher = $this->my_custom_functions->get_perticular_count(TBL_TEACHER, 'and school_id = "' . $school_id . '"');
                $count_student = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $school_id . '"');
                $count_parent = $this->my_custom_functions->get_perticular_count(TBL_PARENT, 'and school_id = "' . $school_id . '"');
                $count_class = $this->my_custom_functions->get_perticular_count(TBL_CLASSES, 'and school_id = "' . $school_id . '"');

                if ($count_teacher < 1 OR $count_student < 1 OR $count_parent < 1 OR $count_class < 1) {

                    $datediff = $now - $schools['created_at'];
                    $days = round($datediff / (60 * 60 * 24));
                    //$days = 21;
                    if ($days == 3 OR $days == 7 OR $days == 15 OR $days == 25) {

                        ////////////////////// TRIGGER EMAIL //////////////////////
                        $email_data = $this->config->item("improper_school_account");
                        $from_email_variable = $this->my_custom_functions->get_particular_field_value("tbl_system_emails", "from_email_variable", " and variable_name='improper_school_account'");
                        $from_email = $this->config->item($from_email_variable);
                        //$subject = $email_data['subject'];
                        $contact_person = $schools['contact_person_name'];
                        $subject = $this->my_custom_functions->sprintf_email($email_data['subject'], array(
                            'ContactPerson' => $contact_person
                                )
                        );
                        //$addressing_user = $email_data['addressing_user'];
                        $addressing_user = $this->my_custom_functions->sprintf_email($email_data['addressing_user'], array(
                            'ContactPerson' => $contact_person
                                )
                        );
                        $organization_name = $schools['name'];
                        $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                            'organization_name' => $organization_name
                                )
                        );
                        $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                            'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                                )
                        );
                        $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                            'institute_name' => 'Team Bidyaaly'
                                )
                        );
                        $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                            'systemtext' => SYSTEM_TEXT
                                )
                        );
                        $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                        //echo $full_mail;

                        $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $schools['email_address'], $subject, $full_mail);
                    }
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //////   CRON WILL RUN IN EVERY DAY 
    // http://server1/webdev/Bidyaaly/site/cron/account_expiring/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////

    function account_expiring() {
        $now = time();
        $school_list = $this->Cron_model->get_all_school_list_data();
        if (!empty($school_list)) {
            foreach ($school_list as $schools) {
                $school_id = $schools['id'];
                $datediff = strtotime($schools['license_expiry_date']) - $now;
                $days = round($datediff / (60 * 60 * 24));

                if ($days == 1) {
                    ///////////////////// TRIGGER EMAIL FOR ACCOUNT EXPIRY //////////////////////////
                    $email_data = $this->config->item("account_about_to_expire");
                    $from_email_variable = $this->my_custom_functions->get_particular_field_value("tbl_system_emails", "from_email_variable", " and variable_name='account_about_to_expire'");
                    $from_email = $this->config->item($from_email_variable);
                    $subject = $email_data['subject'];
                    $contact_person = $schools['contact_person_name'];
                    $addressing_user = $this->my_custom_functions->sprintf_email($email_data['addressing_user'], array(
                        'ContactPerson' => $contact_person
                            )
                    );
                    $organization_name = $schools['name'];
                    $license_expiry_date = date('d/m/Y', strtotime($schools['license_expiry_date']));
                    $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                        'company_name' => $organization_name,
                        'date' => $license_expiry_date
                            )
                    );
                    $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                        'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                            )
                    );
                    $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                        'institute_name' => 'Team Bidyaaly'
                            )
                    );
                    $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                        'systemtext' => SYSTEM_TEXT
                            )
                    );
                    $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                    //echo $full_mail;

                    $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $schools['email_address'], $subject, $full_mail);
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //////   CRON WILL RUN IN EVERY DAY 
    // http://server1/webdev/Bidyaaly/site/cron/proper_account_setup/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////

    function proper_account_setup() {
        $now = time();
        $school_list = $this->Cron_model->get_all_school_list_data();
        if (!empty($school_list)) {
            foreach ($school_list as $schools) {
                $school_id = $schools['id'];
                $count_teacher = $this->my_custom_functions->get_perticular_count(TBL_TEACHER, 'and school_id = "' . $school_id . '"');
                $count_student = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $school_id . '"');
                $count_parent = $this->my_custom_functions->get_perticular_count(TBL_PARENT, 'and school_id = "' . $school_id . '"');
                $count_class = $this->my_custom_functions->get_perticular_count(TBL_CLASSES, 'and school_id = "' . $school_id . '"');

                if ($count_teacher > 0 AND $count_student > 0 AND $count_parent > 0 AND $count_class > 0) {
                    $datediff = $now - $schools['created_at'];
                    $days = round($datediff / (60 * 60 * 24));
                    //$days = 8;
                    if ($days == 8) {
                        ///////////////////// TRIGGER EMAIL FOR ACCOUNT SET UP PROPERLY //////////////////////////
                        $email_data = $this->config->item("proper_school_account");
                        $from_email_variable = $this->my_custom_functions->get_particular_field_value("tbl_system_emails", "from_email_variable", " and variable_name='proper_school_account'");
                        $from_email = $this->config->item($from_email_variable);
                        $contact_person = $schools['contact_person_name'];
                        $school_name = $schools['name'];
                        $subject = $this->my_custom_functions->sprintf_email($email_data['subject'], array(
                            'contact_person' => $contact_person,
                            'institute_name' => $school_name
                                )
                        );
                        $addressing_user = $this->my_custom_functions->sprintf_email($email_data['addressing_user'], array(
                            'contact_person' => $contact_person
                                )
                        );
                        $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                            'institute_name' => $school_name
                                )
                        );
                        $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                            'unsubscribe' => '<a href="' . base_url() . 'unsubscribe/' . $this->my_custom_functions->ablEncrypt($school_id) . '" target="_blank">unsubscribe</a>'
                                )
                        );
                        $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                            'institute_name' => 'Team Bidyaaly'
                                )
                        );
                        $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                            'systemtext' => SYSTEM_TEXT
                                )
                        );
                        $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                        //echo $full_mail;

                        $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $schools['email_address'], $subject, $full_mail);
                    }
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Fees due reminder
    ////////////////////////////////////////////////////////////////////////////
    function send_fees_due_reminder_to_parents() {

        $data = $this->Cron_model->get_records_for_due_reminder();

        // Email template
        $email_data = $this->config->item("fees_due_reminder");
        $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='fees_due_reminder'");
        $from_email = $this->config->item($from_email_variable);

        if (!empty($data)) {
            foreach ($data as $reminder_details) {

                $school_id = $reminder_details['student']['school_id'];
                $school_name = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, "name", " and id='" . $school_id . "'");
                $student_id = $reminder_details['student']['id'];
                $student_name = $reminder_details['student']['name'];
                $class_info = "";
                $fees_amount = $reminder_details['fees_amount'];
                $due_date = date("d-m-Y", strtotime($reminder_details['due_date']));

                if ($reminder_details['class_info'] != "") {
                    $class_info_array = json_decode($reminder_details['class_info'], true);
                    if (is_array($class_info_array)) {
                        if (array_key_exists('semester', $class_info_array)) {
                            $class_info .= 'Semester: ' . $class_info_array['semester'] . ', ';
                        }
                        if (array_key_exists('section', $class_info_array)) {
                            $class_info .= 'Section: ' . $class_info_array['section'] . ', ';
                        }
                        if (array_key_exists('roll_no', $class_info_array)) {
                            $class_info .= 'Roll No: ' . $class_info_array['roll_no'];
                        }
                    }
                }
                $class_info = ($class_info == "") ? 'N/A' : $class_info;

                $subject = $email_data['subject'];
                $addressing_user = $email_data['addressing_user'];

                $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                    'student_name' => $student_name,
                    'class_info' => $class_info,
                    'due_amount' => INDIAN_CURRENCY_CODE . $fees_amount,
                    'due_date' => $due_date
                        )
                );

                $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                    'unsubscribe' => '<a href="javascript:;" target="_blank">unsubscribe</a>'
                        )
                );

                $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                    'institute_name' => $school_name
                        )
                );

                $system_text = $email_data['systemtext'];

                $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);

                // Getting the parent emails, mobile numbers, device ids
                $communication = $this->Cron_model->get_parents_communication_details($student_id);

                // Send push notifications if enabled
                if ($reminder_details['reminder']['reminder_push'] == 1) {

                    if (!empty($communication['devices'])) {
                        foreach ($communication['devices'] as $device_table_id) {

                            $this->my_custom_functions->sendSingleUserNotification($device_table_id, strip_tags(trim($message)), "", "", $subject);
                        }
                    }
                }

                // Send sms notifications if enabled
                if ($reminder_details['reminder']['reminder_sms'] == 1) {

                    if (!empty($communication['mobile_numbers'])) {

                        // Send sms if there is sufficient credit available
                        $sms_credits = $this->my_custom_functions->get_available_sms_credits_of_school($school_id);

                        $sms_text = strip_tags($message);
                        $count_sms_length = strlen($sms_text);
                        $count_no_of_sms = ceil($count_sms_length / 160);
                        $count_total_sms = $count_no_of_sms * count($communication['mobile_numbers']);

                        if ($sms_credits >= $count_total_sms) {

                            foreach ($communication['mobile_numbers'] as $mobile) {

                                $sms_sent = $this->my_custom_functions->sendSMS($mobile['mobile_no'], strip_tags($message));

                                // Add credit out after sms is sent
                                if ($sms_sent) {
                                    $this->my_custom_functions->insert_data(array(
                                        "school_id" => $school_id,
                                        "credit_in" => 0,
                                        "credit_out" => $count_no_of_sms,
                                        "transaction_time" => date("Y-m-d H:i:s")
                                            ), TBL_SMS_CREDITS);

                                    // Insert into SMS log
                                    $this->my_custom_functions->insert_data(array(
                                        "user_id" => $mobile['parent_id'],
                                        "school_id" => $school_id,
                                        "mobile_no" => $mobile['mobile_no'],
                                        "message" => strip_tags($message),
                                        "request_time" => time()
                                            ), TBL_SMS_LOG);
                                }
                            }
                        }
                    }
                }

                // Send email notifications if enabled
                if ($reminder_details['reminder']['reminder_email'] == 1) {

                    if (!empty($communication['emails'])) {

                        foreach ($communication['emails'] as $email) {

                            $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $email, $subject, $full_mail);
                        }
                    }
                }
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //////   CRON WILL RUN IN EVERY DAY 
    // http://server1/webdev/Bidyaaly/site/cron/dailyRegistrationLog/19BidAbl20
    ////////////////////////////////////////////////////////////////////////////

    function dailyRegistrationLog() {
        $school_list_data = '';
        $school_list = $this->Cron_model->get_school_list_registered_today();
        if (!empty($school_list)) {
            $admin_email_address = ADMIN_EMAIL;
            $from_email = DONOT_REPLY_EMAIL;
            $email_data = $this->config->item("registration_detail");
            $email_structure = $this->config->item("reg_format");
            $admin_subject = "Daily Registration List";
            $admin_addressing_user = $email_data['addressing_user'];
            $registration_count = 0;
            foreach ($school_list as $school) {
                $admin_message = $this->my_custom_functions->sprintf_email($email_structure['mail_body'], array(
                    
                    'institute_name' => $school['name'],
                    'contact_person_name' => $school['contact_person_name'],
                    'mobile_number' => $school['mobile_no'],
                    'email_address' => $school['email_address'],
                    'ip_address' => $school['ip_address'],
                    'address' => $school['address']
                        )
                );
               $school_list_data.=$admin_message."<br>";  
               $registration_count++;
            }
            $total_new_reg = "Total New Registration : ".$registration_count."<br><br>";
            $admin_message_final = $total_new_reg.$school_list_data;
            
            $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                'institute_name' => 'Team Bidyaaly'
                    )
            );

            $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                'systemtext' => SYSTEM_TEXT
                    )
            );
            $full_mail = $this->my_custom_functions->CreateSystemEmail($admin_addressing_user, $admin_message_final, '', $institute_name, $system_text);
            $this->my_custom_functions->SendEmail($from_email . ',' . SITE_NAME, $admin_email_address, $admin_subject, $full_mail);
        }
    }
    
    

}
