<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_admin_security();
        $this->load->model("admin/Setting_model");
    }

    function smsGatewayConfigure() {
        $data['page_title'] = 'SMS Gateway Configuration';
        $data['sms_settings'] = $this->Setting_model->get_sms_settings_data();
        $this->load->view('admin/sms_configuration', $data);
    }

    function addSMSGatewaySettings() {
        $key_value = array();

        $sms_variables = $this->input->post('sms_variable');
        $sms_values = $this->input->post('sms_value');

        foreach ($sms_variables as $key => $val) {
            if ($val != '') {
                $key_value[$val] = $sms_values[$key];
            }
        }

        $final_key_value = json_encode($key_value);
        $update_data = $this->Setting_model->update_sms_settings($final_key_value);
        if ($update_data) {
            $this->session->set_flashdata("s_message", 'SMS Gateway record updated successfully.');
            redirect("admin/setting/smsGatewayConfigure");
        }
    }

    function emailGatewayConfigure() {
        $data['page_title'] = 'SMS Gateway Configuration';
        $data['email_settings'] = $this->Setting_model->get_email_settings_data();
        $this->load->view('admin/email_configuration', $data);
    }
    
    function addEmailGatewaySettings() {
        //echo "<pre>";print_r($_POST);die;
        $update_data = $this->Setting_model->update_email_settings();
        if ($update_data) {
            $this->session->set_flashdata("s_message", 'Email Gateway record updated successfully.');
            redirect("admin/setting/emailGatewayConfigure");
        }
    }

}
