<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_admin_security();
        $this->load->model("admin/Admin_user_model");
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function dashBoard() {

        $data['page_title'] = 'Dashboard';
        $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $this->session->userdata('admin_id')));
        $this->load->view("admin/dashboard", $data);
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Change password from admin area
    ///////////////////////////////////////////////////////////////////////////////
    public function change_password() {

        if ($this->input->post("change_password") && $this->input->post("change_password") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("o_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("n_password", "New Password", "trim|required|min_length[6]");
            $this->form_validation->set_rules("c_password", "Confirm Password", "trim|required|min_length[6]|matches[n_password]");

            if ($this->form_validation->run() == false) { /// Return to change password page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/change_password");
            } else {

                $o_password = $this->input->post('o_password');
                $password = $this->my_custom_functions->get_particular_field_value(TBL_ADMIN, "password", " and admin_id='" . $this->session->userdata('admin_id') . "'");

                if (password_verify($o_password, $password)) {

                    $data = array(
                        "password" => password_hash($this->input->post("n_password"), PASSWORD_DEFAULT)
                    );

                    $table = TBL_ADMIN;

                    $where = array(
                        "admin_id" => $this->session->userdata('admin_id')
                    );
                    $password_updated = $this->my_custom_functions->update_data($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("admin/user/change_password");
                } else {
                    $this->session->set_flashdata("e_message", "Old password is incorrect.");
                    redirect("admin/user/change_password");
                }
            }
        } else {
            $data['page_title'] = 'Change Password';
            $data['active_menu'] = '';
            $this->load->view("admin/change_password", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Edit admin details
    ///////////////////////////////////////////////////////////////////////////////
    function edit_profile() {

        if ($this->input->post("save") && $this->input->post("save") != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("email", "Account Email", "trim|required|valid_email");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/edit_profile");
            } else { /// Update admin
                $admin_id = $this->session->userdata('admin_id');
                $update = $this->Admin_user_model->update_admin($admin_id);
                $this->load->library('image_lib');
                $this->load->library('upload');

                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    $filepath = $this->my_custom_functions->get_particular_field_value(TBL_ADMIN_FILES, 'file_url', 'and admin_id = "' . $admin_id . '" ');
                    if ($filepath != '') {
                        $file_url = $filepath;
                        $fileUrlArray = explode("/", $file_url);
                        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
                        if ($aws_key != "") {
                            $s3 = new S3Client(array(
                                'version' => 'latest',
                                'region' => 'ap-south-1',
                                'credentials' => array(
                                    'key' => AWS_KEY,
                                    'secret' => AWS_SECRET,
                                ),
                            ));
                            $bucket = AMAZON_BUCKET;
                            try {
                                if ($aws_key != '') {
                                    $result = $s3->deleteObject(array(
                                        'Bucket' => $bucket,
                                        'Key' => ADMIN_FOLD . '/' . $aws_key
                                    ));
                                }
                                $this->my_custom_functions->delete_data(TBL_ADMIN_FILES, array("admin_id" => $admin_id));
                            } catch (S3Exception $e) {

                                $encode[] = array(
                                    "msg" => "Operation Failed",
                                    "status" => "true"
                                );
                            }
                        }
                    }








                    $timestamp = strtotime(date('Y-m-d H:i:s'));
                    $emp_name = str_replace(' ', '', $this->input->post("name"));

                    $mime_type = $_FILES['adminphoto']['type'];
                    $split = explode('/', $mime_type);
                    $type = $split[1];

                    $temp_file = 'file_upload/' . $emp_name . '_' . $timestamp . '.jpg';
                    $img_width = IMAGE_WIDTH;
                    $img_height = IMAGE_HEIGHT;

                    if ($type == "jpg" || $type == "jpeg" || $type == "png" || $type == "gif") {

                        $success = move_uploaded_file($_FILES['adminphoto']['tmp_name'], $temp_file);

                        $ImageSize = filesize($temp_file); /* get the image size */

                        //if ($ImageSize < ALLOWED_FILE_SIZE) {

                        $this->my_custom_functions->CreateFixedSizedImage($temp_file, FCPATH . $temp_file, $img_width, $img_height);

                        // Instantiate an Amazon S3 client.
                        $s3 = new S3Client(array(
                            'version' => 'latest',
                            'region' => 'ap-south-1',
                            'credentials' => array(
                                'key' => AWS_KEY,
                                'secret' => AWS_SECRET,
                            ),
                        ));

                        $bucket = AMAZON_BUCKET;

                        try {
                            $result = $s3->putObject(array(
                                'Bucket' => $bucket,
                                'Key' => ADMIN_FOLD . '/' . $admin_id . '_' . $timestamp,
                                'SourceFile' => $temp_file,
                                'ContentType' => 'text/plain',
                                'ACL' => 'public-read',
                                'StorageClass' => 'REDUCED_REDUNDANCY',
                                'Metadata' => array()
                            ));

                            if (@file_exists($temp_file)) {
                                @unlink($temp_file);
                            }

                            //$this->Admin_model->update_employee_photo($emp_id,$result['ObjectURL']);
//                                            $this->Company_model->update_employee_photo($emp_id,$result['ObjectURL']);
                            $file_data = array('admin_id' => $admin_id, 'file_url' => $result['ObjectURL']);
                            $this->my_custom_functions->insert_data($file_data, TBL_ADMIN_FILES);
                        } catch (S3Exception $e) {
                            //echo $e->getMessage() . "\n";
                        }
//                                    } else {
//                                        $msg = "Uploaded Photo Size Should Be Less Than 5MB. ";
//                                    }
                    } else {
                        $msg = "Only JPEG, JPG, PNG File Types Are Allowed. ";
                    }
                }

                if ($update) {
                    $this->session->set_flashdata('s_message', "Successfully updated admin details.");
                    redirect("admin/user/edit_profile");
                } else {
                    $this->session->set_flashdata('e_message', "Error updating admin details.");
                    redirect("admin/user/edit_profile");
                }
            }
        } else {

            $data['active_menu'] = '';
            $data['page_title'] = 'Edit admin';
            $data['admin_details'] = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $this->session->userdata('admin_id')));
            $data['admin_details']['image'] = "<img src='" . base_url() . "_images/superadmin_icon.png'>";
            $this->load->view("admin/edit_profile", $data);
        }
    }

    function delete_profilePic() {
        $admin_id = $this->uri->segment(4);
        $filepath = $this->my_custom_functions->get_particular_field_value(TBL_ADMIN_FILES, 'file_url', 'and admin_id = "' . $admin_id . '" ');
        $file_url = $filepath;
        $fileUrlArray = explode("/", $file_url);
        $aws_key = $fileUrlArray[count($fileUrlArray) - 1];
        if ($aws_key != "") {
            $s3 = new S3Client(array(
                'version' => 'latest',
                'region' => 'ap-south-1',
                'credentials' => array(
                    'key' => AWS_KEY,
                    'secret' => AWS_SECRET,
                ),
            ));
            $bucket = AMAZON_BUCKET;
            try {
                if ($aws_key != '') {
                    $result = $s3->deleteObject(array(
                        'Bucket' => $bucket,
                        'Key' => ADMIN_FOLD . '/' . $aws_key
                    ));
                }
                $this->my_custom_functions->delete_data(TBL_ADMIN_FILES, array("admin_id" => $admin_id));
            } catch (S3Exception $e) {

                $encode[] = array(
                    "msg" => "Operation Failed",
                    "status" => "true"
                );
            }
        }
        redirect('admin/user/edit_profile');
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// Reset password(linked with forgot password)
    ///////////////////////////////////////////////////////////////////////////////
    public function reset_password() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
            $this->form_validation->set_rules('re_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/main/forgot_password");
            } else {
                $data = array(
                    "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                );

                $table = TBL_ADMIN;

                $where = array(
                    "admin_id" => $this->session->userdata("reset_admin_id")
                );
                $password_updated = $this->my_custom_functions->update_data($data, $table, $where);
                $this->session->unset_userdata("reset_admin_id");

                if ($password_updated) {
                    $this->session->set_flashdata("s_message", 'Password has been changed successfully.<a href="' . base_url() . 'admin">Login here</a>.');
                    redirect("admin/main/forgot_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            }
        } else {

            $admin_id = $this->uri->segment(4);
            $code = $this->uri->segment(5);

            if (isset($admin_id) && isset($code)) {
                $admin_details = $this->my_custom_functions->get_details_from_id("", TBL_ADMIN, array("admin_id" => $admin_id));

                if ($code == $this->my_custom_functions->encrypt_string($admin_details['email'])) {
                    $this->session->set_userdata("reset_admin_id", $admin_id);
                    $this->load->view("admin/reset_password");
                } else {
                    $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                    redirect("admin/main/forgot_password");
                }
            } else {
                $this->session->set_flashdata("e_message", "Some error occurred. Click on the link again.");
                redirect("admin/main/forgot_password");
            }
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   TEACHER PART
    //////////////////////////////////////////////////////////////////////

    public function manageAdmin() {

        $data['page_title'] = 'Manage Admins';
        $data['admins'] = $this->Admin_user_model->get_admin_data();
        $this->load->view('admin/manage_admins', $data);
    }

    public function addAdmin() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            // echo "<pre>";print_r($_POST);die;
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/addAdmin");
            } else { /// Update admin
                $teacher_data = array(
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'status' => $this->input->post('status'),
                );

                $admin_id = $this->my_custom_functions->insert_data_last_id($teacher_data, TBL_ADMIN);

                if ($admin_id) {

                    $this->load->library('image_lib');
                    $this->load->library('upload');

                    /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                    if ($_FILES AND $_FILES['adminphoto']['name']) {

                        if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                                ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                                ($_FILES['adminphoto']['type'] == 'image/png') ||
                                ($_FILES['adminphoto']['type'] == 'image/gif')) {

                            list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                            $new_width = "";
                            $new_height = "";

                            $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                            $new_width = 185;
                            $new_height = $new_width / $ratio;

                            $config['image_library'] = 'gd2';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg';
                            $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                            $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                            $config['maintain_ratio'] = FALSE;
                            $config['width'] = $new_width;
                            $config['height'] = $new_height;

                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                        }
                    }

                    $this->session->set_flashdata("s_message", 'Admin added successfully.');
                    redirect("admin/user/manageAdmin");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/user/manageAdmin");
                }
            }
        } else {
            $data['page_title'] = 'Add Admins';
            $this->load->view('admin/add_admin', $data);
        }
    }

    public function editAdmin() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $admin_id = $this->input->post('admin_id');
            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("phone", "Phone Number", "required|trim");
            $this->form_validation->set_rules("email", "Email", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/editAdmin/" . $admin_id);
            } else { /// Update class
                $admin_data = array(
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                    'phone' => $this->input->post('phone'),
                    'status' => $this->input->post('status'),
                );
                if ($this->input->post('password') && $this->input->post('password') != '') {
                    $admin_data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                }

                $condition = array(
                    'admin_id' => $admin_id
                );

                $edit_admin_data = $this->my_custom_functions->update_data($admin_data, TBL_ADMIN, $condition);

                $this->load->library('image_lib');
                $this->load->library('upload');

                /////////////////////// UPLOAD IMAGE FOR QUESTIONS///////////////////////////////

                if ($_FILES AND $_FILES['adminphoto']['name']) {

                    if (($_FILES['adminphoto']['type'] == 'image/jpeg') ||
                            ($_FILES['adminphoto']['type'] == 'image/jpg') ||
                            ($_FILES['adminphoto']['type'] == 'image/png') ||
                            ($_FILES['adminphoto']['type'] == 'image/gif')) {

                        list($width, $height) = getimagesize($_FILES['adminphoto']['tmp_name']);
                        $new_width = "";
                        $new_height = "";

                        $ratio = $width / $height;

//                        $new_width = 800;
//                        $new_height = $new_width / $ratio;
                        $new_width = 185;
                        //$new_height = $new_width / $ratio;
                        $new_height = 185;

                        $config['image_library'] = 'gd2';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['source_image'] = $_FILES['adminphoto']['tmp_name'];
                        $config['new_image'] = "uploads/admin/" . $admin_id . ".jpg";
                        $config['maintain_ratio'] = FALSE;
                        $config['width'] = $new_width;
                        $config['height'] = $new_height;

                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                }
                if ($edit_admin_data) {
                    $this->session->set_flashdata("s_message", 'Admin edited successfully.');
                    redirect("admin/user/manageAdmin");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/user/manageAdmin");
                }
            }
        } else {
            $data['page_title'] = 'Edit Admin';
            $subject_id = $this->uri->segment(4);
            $data['admin'] = $this->my_custom_functions->get_details_from_id('', TBL_ADMIN, array('admin_id' => $subject_id));

            $this->load->view('admin/edit_admin', $data);
        }
    }

    public function deleteSchool() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            
            if ($this->input->post('immediate_delete') && $this->input->post('immediate_delete') == 1) {
                $school_id = $this->input->post('school_id');
                $delete_date_pre = date('Y-m-d', time());
                $delete_date = $delete_date_pre . ' 00:00:00';
                $data = array(
                    'is_deleted' => DELETE,
                    'delete_time' => $delete_date
                );
                $condition = array(
                    'id' => $school_id
                );
                $delete = $this->my_custom_functions->update_data($data, TBL_SCHOOL, $condition);
                $this->session->set_flashdata("s_message", 'Account delete is in progress');
                redirect("school/user/edit_profile");
            } else {
                $school_id = $this->input->post('school_id');
                $delete_date_pre = date('Y-m-d', strtotime("+" . DELETE_DAY . " days"));
                $delete_date = $delete_date_pre . ' 00:00:00';
                $data = array(
                    'is_deleted' => DELETE,
                    'delete_time' => $delete_date
                );
                $condition = array(
                    'id' => $school_id
                );
                $delete = $this->my_custom_functions->update_data($data, TBL_SCHOOL, $condition);
                $this->session->set_flashdata("s_message", 'Account delete is in progress');
                redirect("school/user/edit_profile");
            }
        } else {
            $school_id = $this->uri->segment(4);
            $delete_date_pre = date('Y-m-d', strtotime("+" . DELETE_DAY . " days"));
            $delete_date = $delete_date_pre . ' 00:00:00';
            $data = array(
                'is_deleted' => DELETE,
                'delete_time' => $delete_date
            );
            $condition = array(
                'id' => $school_id
            );
            $delete = $this->my_custom_functions->update_data($data, TBL_SCHOOL, $condition);
            if ($delete) {
                $this->session->set_flashdata("s_message", 'School deleted successfully.');
                redirect("admin/user/manageSchool");
            } else {
                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("admin/user/manageSchool");
            }
        }
    }

    public function logout() {


        $session_data = array('admin_id', 'admin_username', 'admin_email', 'admin_is_logged_in');
        $this->session->unset_userdata($session_data);

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("admin");
    }

    ////////////////////////////////////////////////////////////////////////////
    // Manage schools
    ////////////////////////////////////////////////////////////////////////////
    function manageSchool() {

        $data['page_title'] = 'Manage Schools';

        $schools = $this->Admin_user_model->get_school_data();
        $data['schools'] = array();

        if (!empty($schools)) {
            foreach ($schools as $key => $school) {

                $address = '';
                if ($school['address'] != '') {
                    $address = $school['address'] . ', ';
                }

                if ($school['country_id'] != 0) {
                    $address .= $this->my_custom_functions->get_particular_field_value(TBL_COUNTRY, "name", " and country_id='" . $school['country_id'] . "'");
                }

                // Address
                $address = rtrim($address, ', ');
                $school['address'] = $address;

                // Username, last login time
                $login_details = $this->my_custom_functions->get_details_from_id("", TBL_COMMON_LOGIN, array("id" => $school['id'], "type" => SCHOOL));
                $school['username'] = $login_details['username'];
                $school['last_login_time'] = $login_details['last_login_time'];

                // Mobile, email address verified status
                $verified = '<span><i class="fas fa-check-circle"></i></span>';
                $school['mobile_no_verified_status'] = '';
                $school['email_verified_status'] = '';
                
                if ($school['otp_verification'] == 1) {
                    $school['mobile_no_verified_status'] = $verified;
                }

                if ($school['email_verification'] == 1) {
                    $school['email_verified_status'] = $verified;
                }

                // SMS credits
                $school['available_sms_credits'] = $this->my_custom_functions->get_available_sms_credits_of_school($school['id']);

                // Last login day difference
                $school['last_login_day_difference'] = '';
                if ($school['last_login_time'] != 0) {
                    $last_login_day_difference = $this->my_custom_functions->dateDiff(date('Y-m-d', time()), date('Y-m-d', $school['last_login_time']));

                    if ($last_login_day_difference == "") {
                        $school['last_login_day_difference'] = '<span style="color:#09672c;"> (Today) </span>';
                    } else {
                        $school['last_login_day_difference'] = '<span style="color:#9a4d0a;"> (' . $last_login_day_difference . ' ago) </span>';
                    }
                }

                // Account progress
                $account_progress = $this->my_custom_functions->get_school_account_progress($school['id']);
                $school['progress_percentage'] = $account_progress['percentage'];
                
                $data['schools'][] = $school;
            }
        }

        //echo "<pre>";print_r($data['schools']); die;
        $this->load->view('admin/manage_school', $data);
    }

    ////////////////////////////////////////////////////////////////////////////

    function loginAsSchool() {

        $school_id = $this->uri->segment(4);
        $school_details = $this->my_custom_functions->get_details_from_id($school_id, TBL_SCHOOL);

        $today = strtotime(date("Y-m-d"));
        $session_selected = "";
        $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_school($school_id);

        if (!empty($school_sessions)) {
            foreach ($school_sessions as $school_ses) {
                if (strtotime($school_ses['from_date']) <= $today AND strtotime($school_ses['to_date']) >= $today) {
                    $session_selected = $school_ses['id'];
                }
            }
            if ($session_selected == "") {
                $session_selected = $school_sessions[0]['id'];
            }
        }

        $session_data = array(
            "school_id" => $school_details["id"],
            "session_id" => $session_selected,
            //"school_username" => $school_details["username"],
            "school_email" => $school_details["email_address"],
            "school_is_logged_in" => 1,
            "usertype" => SCHOOL,
        );
        $this->session->set_userdata($session_data);
        redirect("school/user/dashBoard");
    }

    function checkAdminUsername() {

        $adminUsername = $this->input->post('reg_val');

        $check_registratin_no = $this->my_custom_functions->get_perticular_count(TBL_ADMIN, ' and username = "' . $adminUsername . '"');
        if ($check_registratin_no > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // SMS Credit Report (Moved to report controller)
    ////////////////////////////////////////////////////////////////////////////
    /* function smsCreditReport() {

      $data['page_title'] = 'SMS Credit Report';

      if ($this->input->post('submit') AND $this->input->post('submit') != "") {

      $search_sms_credit = array(
      'smscrrp_school_id' => $this->input->post('school'),
      'smscrrp_from_date' => $this->input->post('from_date'),
      'smscrrp_to_date' => $this->input->post('to_date')
      );
      $this->session->set_userdata($search_sms_credit);

      redirect('admin/user/smsCreditReport');
      } else {

      $data['schools'] = $this->Admin_user_model->get_school_data();
      $data['credits'] = $this->Admin_user_model->get_sms_credit_report();
      $this->load->view('admin/sms_credit_report', $data);
      }
      }

      ////////////////////////////////////////////////////////////////////////////
      // Add SMS Credit
      ////////////////////////////////////////////////////////////////////////////
      function addSMSCredit() {

      $data['page_title'] = 'Add SMS Credit';

      if ($this->input->post('submit') AND $this->input->post('submit') != "") {

      $this->load->library("form_validation");

      /// tbl_admins contents
      $this->form_validation->set_rules("school", "School", "trim|required");
      $this->form_validation->set_rules("credit_amount", "Credit Amount", "required|trim");
      $this->form_validation->set_rules("credit_type", "Credit Type", "trim|required");

      if ($this->form_validation->run() == false) { /// Return to SMS Credit Report page
      $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
      redirect("admin/user/addSMSCredit");
      } else {

      $credit_in = 0;
      $credit_out = 0;
      if ($this->input->post('credit_type') == 1) {

      $credit_in = $this->input->post('credit_amount');
      } else if ($this->input->post('credit_type') == 2) {

      $credit_out = $this->input->post('credit_amount');
      }

      $credit_data = array(
      'school_id' => $this->input->post('school'),
      'credit_in' => $credit_in,
      'credit_out' => $credit_out,
      'transaction_time' => date('Y-m-d H:i:s')
      );

      $ret = $this->my_custom_functions->insert_data($credit_data, TBL_SMS_CREDITS);

      if ($ret) {
      $this->session->set_flashdata("s_message", 'Transaction added successfully.');
      redirect("admin/user/smsCreditReport");
      } else {
      $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
      redirect("admin/user/smsCreditReport");
      }
      }
      } else {

      $data['schools'] = $this->Admin_user_model->get_school_data();
      //echo '<pre>'; print_r($data['schools']); echo '</pre>'; die;
      $this->load->view('admin/add_sms_credit', $data);
      }
      } */

    function restoreAdmin() {
        $school_id = $this->uri->segment(4);

        $restore_date = '0000-00-00 00:00:00';
        $data = array(
            'is_deleted' => NOT_DELETE,
            'delete_time' => $restore_date
        );
        $condition = array(
            'id' => $school_id
        );
        $restore = $this->my_custom_functions->update_data($data, TBL_SCHOOL, $condition);

        if ($restore) {
            $this->session->set_flashdata("s_message", 'Student restored successfully.');
            redirect("admin/user/manageSchool");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("admin/user/manageSchool");
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit school
    ////////////////////////////////////////////////////////////////////////////
    public function editSchool() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $encrypted_school_id = $this->input->post('school_id');
            $school_id = $this->my_custom_functions->ablDecrypt($encrypted_school_id);

            $this->load->library("form_validation");

            /// tbl_schools contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("contact_person_name", "Contact Person Name", "required|trim");
            $this->form_validation->set_rules("mobile_no", "Mobile Number", "trim|required");
            $this->form_validation->set_rules("email_address", "Email", "trim|required");
            $this->form_validation->set_rules("address", "Address", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/editSchool/" . $encrypted_school_id);
            } else {

                // Update school details
                $school_data = array(
                    'name' => $this->input->post('name'),
                    'contact_person_name' => $this->input->post('contact_person_name'),
                    'mobile_no' => $this->input->post('mobile_no'),
                    'email_address' => $this->input->post('email_address'),
                    'address' => $this->input->post('address'),
                    'no_of_license' => $this->input->post('no_of_license'),
                    'license_expiry_date' => $this->my_custom_functions->database_date_dash($this->input->post('license_expiry_date')),
                    'expiry_module' => $this->input->post('exp_module'),
                    'display_ads' => $this->input->post('display_ads'),
                    'otp_verification' => $this->input->post("otp_verification"),
                    'email_verification' => $this->input->post("email_verification")
                );

                $condition = array(
                    'id' => $school_id
                );

                $update_school_data = $this->my_custom_functions->update_data($school_data, TBL_SCHOOL, $condition);

                // Update school login details
                $school_login_data['status'] = $this->input->post('status');
                if ($this->input->post('password') && $this->input->post('password') != '') {
                    $school_login_data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                }

                $condition = array(
                    'id' => $school_id,
                    'type' => SCHOOL
                );

                $update_school_login_data = $this->my_custom_functions->update_data($school_login_data, TBL_COMMON_LOGIN, $condition);

                // Update school payment setting
                $this->Admin_user_model->update_school_payment_setting($school_id);

                if ($update_school_data) {
                    $this->session->set_flashdata("s_message", 'School updated successfully.');
                    redirect("admin/user/manageSchool");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/user/manageSchool");
                }
            }
        } else {

            $data['page_title'] = 'Edit School';

            $encrypted_school_id = $this->uri->segment(4);
            $school_id = $this->my_custom_functions->ablDecrypt($encrypted_school_id);

            $data['school'] = $this->my_custom_functions->get_details_from_id('', TBL_SCHOOL, array('id' => $school_id));
            $data['status'] = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, "status", " and id='" . $school_id . "' and type='" . SCHOOL . "'");
            $data['payment_settings'] = $this->my_custom_functions->get_details_from_id('', TBL_SCHOOL_PAYMENT_SETTINGS, array('school_id' => $school_id));

            $this->load->view('admin/edit_school', $data);
        }
    }

    //////////////////////////////////////////////////////////// 
    // System email templates
    //////////////////////////////////////////////////////////// 
    function systemEmailTemplates() {

        $data['page_title'] = 'System Email Templates';

        $data['templates'] = $this->Admin_user_model->get_system_email_templates();
        $this->load->view("admin/mail_template/email_templates", $data);
    }

    //////////////////////////////////////////////////////////// 
    // View a system email template
    //////////////////////////////////////////////////////////// 
    function viewEmailTemplate() {

        $data['page_title'] = 'View Email Template';

        $template_id = $this->uri->segment(4);
        $template_details = $this->my_custom_functions->get_details_from_id("", "tbl_system_emails", array("id" => $template_id));

        $email_config_data = $this->config->item($template_details['variable_name']);
        //echo "<pre>";print_r($email_config_data);die;

        $data['template'] = $this->my_custom_functions->CreateSystemEmail($email_config_data['addressing_user'], $email_config_data['mail_body'], $email_config_data['unsubscribe'], $email_config_data['institute_name'], $email_config_data['systemtext']);

        $this->load->view("admin/mail_template/view_template", $data);
    }

    //////////////////////////////////////////////////////////// 
    // Add new system email template information to database
    //////////////////////////////////////////////////////////// 
    function addEmailTemplate() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->load->library("form_validation");

            /// tbl_system_emails contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("variable_name", "Variable Name", "trim|required");
            $this->form_validation->set_rules("from_email_variable", "From Email Variable", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to manage templates page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/addEmailTemplate");
            } else {

                $res = $this->Admin_user_model->add_email_template();

                if ($res) {
                    $this->session->set_flashdata('s_message', "Record added successfully.");
                    redirect("admin/user/systemEmailTemplates");
                } else {
                    $this->session->set_flashdata('e_message', "Unable to insert record.");
                    redirect("admin/user/addEmailTemplate");
                }
            }
        } else {

            $data['page_title'] = 'Add Email Template';

            $this->load->view("admin/mail_template/add_template", $data);
        }
    }

    //////////////////////////////////////////////////////////// 
    // Edit an existing system email template record 
    //////////////////////////////////////////////////////////// 
    function editEmailTemplate() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->load->library("form_validation");

            /// tbl_system_emails contents
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("variable_name", "Variable Name", "trim|required");
            $this->form_validation->set_rules("from_email_variable", "From Email Variable", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to manage templates page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/user/editEmailTemplate/" . $this->input->post("template_id"));
            } else {

                $res = $this->Admin_user_model->update_email_template();

                if ($res) {
                    $this->session->set_flashdata('s_message', "Record updated successfully.");
                    redirect("admin/user/systemEmailTemplates");
                } else {
                    $this->session->set_flashdata('e_message', "Unable to update record.");
                    redirect("admin/user/editEmailTemplate/" . $this->input->post("template_id"));
                }
            }
        } else {

            $data['page_title'] = 'Edit Email Template';

            $template_id = $this->uri->segment(4);
            $data['details'] = $this->my_custom_functions->get_details_from_id($template_id, "tbl_system_emails");

            $this->load->view("admin/mail_template/edit_template", $data);
        }
    }

    function maintainanceMode() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $update_data = array(
                'maintenance_mode' => $this->input->post('mode'),
                'maintenance_message' => $this->input->post('mode_text'),
            );
            $condition = array(
                'id' => 1
            );
            $update_data = $this->my_custom_functions->update_data($update_data, TBL_APP_MAINTAINANCE, $condition);
            $this->session->set_flashdata('s_message', "Record updated successfully.");
            redirect("admin/user/maintainanceMode");
        } else {

            $data['maintainance_data'] = $this->my_custom_functions->get_details_from_id('1', TBL_APP_MAINTAINANCE);
            $data['page_title'] = 'App Maintaince Mode';
            $this->load->view('admin/maintainance_mode', $data);
        }
    }

}
