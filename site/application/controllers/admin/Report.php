<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->check_admin_security();
        $this->load->model("admin/Admin_report_model");
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // SMS Credit Report
    ////////////////////////////////////////////////////////////////////////////
    function smsCreditReport() {

        $data['page_title'] = 'SMS Credit Report';

        if ($this->input->post('submit') AND $this->input->post('submit') != "") {

            $search_sms_credit = array(
                'smscrrp_school_id' => $this->input->post('school'),
                'smscrrp_from_date' => $this->input->post('from_date'),
                'smscrrp_to_date' => $this->input->post('to_date')
            );
            $this->session->set_userdata($search_sms_credit);

            redirect('admin/report/smsCreditReport');
        } else {

            $data['schools'] = $this->Admin_report_model->get_school_data();
            $data['credits'] = $this->Admin_report_model->get_sms_credit_report();
            $this->load->view('admin/report/sms_credit_report', $data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Add SMS Credit
    ////////////////////////////////////////////////////////////////////////////
    function addSMSCredit() {

        $data['page_title'] = 'Add SMS Credit';

        if ($this->input->post('submit') AND $this->input->post('submit') != "") {

            $this->load->library("form_validation");

            /// tbl_admins contents
            $this->form_validation->set_rules("school", "School", "trim|required");
            $this->form_validation->set_rules("credit_amount", "Credit Amount", "required|trim");
            $this->form_validation->set_rules("credit_type", "Credit Type", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to SMS Credit Report page
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/report/addSMSCredit");
            } else {

                $credit_in = 0;
                $credit_out = 0;
                if ($this->input->post('credit_type') == 1) {

                    $credit_in = $this->input->post('credit_amount');
                } else if ($this->input->post('credit_type') == 2) {

                    $credit_out = $this->input->post('credit_amount');
                }

                $credit_data = array(
                    'school_id' => $this->input->post('school'),
                    'credit_in' => $credit_in,
                    'credit_out' => $credit_out,
                    'transaction_time' => date('Y-m-d H:i:s')
                );

                $ret = $this->my_custom_functions->insert_data($credit_data, TBL_SMS_CREDITS);

                if ($ret) {
                    $this->session->set_flashdata("s_message", 'Transaction added successfully.');
                    redirect("admin/report/smsCreditReport");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/report/smsCreditReport");
                }
            }
        } else {

            $data['schools'] = $this->Admin_report_model->get_school_data();
            
            $this->load->view('admin/report/add_sms_credit', $data);
        }
    }    

    ////////////////////////////////////////////////////////////////////////////
    // SMS log report
    ////////////////////////////////////////////////////////////////////////////
    function smsLog() {
        
        $data['page_title'] = 'SMS Log';

        if ($this->input->post('submit') AND $this->input->post('submit') != "") {

            $search_sms_log = array(
                'smslog_school_id' => $this->input->post('school'),
                'smslog_from_date' => $this->input->post('from_date'),
                'smslog_to_date' => $this->input->post('to_date')
            );
            $this->session->set_userdata($search_sms_log);

            redirect('admin/report/smsLog');
        } else {

            $data['schools'] = $this->Admin_report_model->get_school_data();
            $data['smslog'] = $this->Admin_report_model->get_sms_log_report();
            $this->load->view('admin/report/sms_log_report', $data);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Email log report
    ////////////////////////////////////////////////////////////////////////////
    function emailLog() {
        
        
    }
}
