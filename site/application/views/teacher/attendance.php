<?php $this->load->view('teacher/include/header'); ?>
<script type="text/javascript">
    $(document).ready(function () {

        $('.presentbtn').click(function () {

            $(".check-control-input").attr("checked", true);
            $(".check-control-label").removeClass("absentbtn_clk");
            $(".check-control-label").addClass("presentbtn_clk");
        });

        $('.absentbtn').click(function () {
            $(".check-control-input").attr("checked", false);
            $(".check-control-label").removeClass("presentbtn_clk");
            $(".check-control-label").addClass("absentbtn_clk");
        });
        $('.clearbtn').click(function () {
            $(".check-control-input").attr("checked", false);
            $(".check-control-label").removeClass("presentbtn_clk");
            $(".check-control-label").removeClass("absentbtn_clk");
        });
    });

    function attendance(id) {
        //alert($('#attend_'+id).prop("checked"));
        if ($('#attend_' + id).prop("checked") == true) {
            $('#attend_' + id).parent().find('.check-control-label').removeClass("absentbtn_clk").addClass("presentbtn_clk");
        } else {
            $('#attend_' + id).parent().find('.check-control-label').removeClass("presentbtn_clk").addClass("absentbtn_clk");
        }
    }
</script> 

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="innerbodycontent studentbdContent">

                    <div class="sectionrowBox">
                        <div class="togglebtnContainer">
                            <a href="javascript:" class="buttontab presentbtn">All present</a>
                            <a href="javascript:" class="buttontab absentbtn">All absent</a>
                            <!--                            <a href="javascript:" class="buttontab clearbtn">clear</a>-->
                        </div>
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>
                        <form action="<?php echo current_url(); ?>" method="post" class="was-validated">
                            <?php if (!empty($student_list)) { ?>

                                <div class="studentGrid" >
                                    <ul class="profileStudent">
                                        <?php
                                        //echo "<pre>";print_r($student_list);die;
                                        foreach ($student_list as $row) { //echo "<pre>";print_r($row);
                                            $checked = '';
                                            $attendance_class = '';
                                            if (!empty($attendance_list)) {

                                                foreach ($attendance_list as $attendance_data) {//echo "<pre>";print_r($attendance_data);
                                                    if ($row['id'] == $attendance_data['student_id'] && $attendance_data['attendance_status'] == PRESENT) {
                                                        $checked = 'checked="checked"';
                                                        $attendance_class = 'presentbtn_clk';
                                                    }
                                                }
                                            } else {
                                                
                                            }
                                            ?>
                                            <li>
                                                <div class="check-control">
                                                    <input type="checkbox" class="check-control-input" name="student_id[]" value="<?php echo $row['id']; ?>" <?php echo $checked; ?> id="attend_<?php echo $row['id']; ?>" onclick="attendance(<?php echo $row['id']; ?>);">
                                                    <label class="check-control-label <?php echo $attendance_class; ?>" for="attend_<?php echo $row['id']; ?>">
                                                        <span class="rollname"><?php echo $row['roll_no']; ?></span>
                                                        <span class="studentname"><?php echo $row['name']; ?></span>
                                                    </label>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>

                            <?php } ?>
                            <input type="hidden" name="semester_id" value="<?php echo $this->uri->segment(4); ?>">
                            <input type="hidden" name="section_id" value="<?php echo $this->uri->segment(5); ?>">
                            <input type="hidden" name="period_id" value="<?php echo $this->uri->segment(6); ?>">
                            <?php
                            if ($this->session->userdata('query_date') == date('Y-m-d')) {
                                $semester_id = $this->uri->segment(4);
                                $section_id = $this->uri->segment(5);
                                $period_id = $this->uri->segment(6);
                                $check_publish = $this->my_custom_functions->get_perticular_count(TBL_ATTENDANCE, 'and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" and period_id = "' . $period_id . '" and attendance_time = "' . strtotime(date('Y-m-d')) . '" and publish_data = 1');
                                //echo $this->db->last_query();

                                if ($check_publish == 0) {
                                    ?>
                                    <div class="col-lg-12 buttonContainer">  <input type="submit" name="submit" value="Save" class="btn002"></div>
                                    <?php //if(!empty($attendance_list)){ ?>
                                    <div class="col-lg-12 buttonContainer">  <input type="submit" name="submit" value="Publish" class="btn002"></div>
                                <?php
                                //}
                            }else{
                                echo '<div class="col-lg-12 buttonContainer">Attendance already published</div>';
                            }

                            }
                            ?>
                        </form>
                    </div>
<?php $this->load->view('teacher/include/footer'); ?>
