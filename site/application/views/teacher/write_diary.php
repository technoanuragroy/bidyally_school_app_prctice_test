<?php $this->load->view('teacher/include/header'); ?>
<style>
    .onOff {
        display:none;
    }
    .totop {
        /*        position: fixed;*/
        bottom: 10px;
        right: 20px;
        padding: 10px;
        text-align: center;
        background-color: #333;
        color: #fff;
        display: block;
        width: 70%;
        margin: 17px auto;
        border-radius: 4px;
    }
    .totop{
        display: none;
    }

    a:hover {
        text-decoration: none;
    }

</style>
<script type="text/javascript">

    function CustomConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

    $(function () {
        $(".onOff").slice(0, 4).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $(".onOff:hidden").slice(0, 4).slideDown();
            if ($(".onOff:hidden").length == 0) {
                $("#load").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
        });
    });
    
     function callBrowser() {
        $('.upload_ico').click();
    }
    function callBrowserFile() {
        $('.upload_ico_file').click();
    }
    function previewImage(obj) {

        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_' + i + '"> ' +
                            '<img src="' + e.target.result + '" alt="" style="width:300px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.img_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }
        $('.proceed_text').show();

    }
    function del_prev_image_file(file_index) {

        $(".preview_image_" + file_index).remove();

        var deleted_image = $(".deleted_images").val();
        deleted_image += file_index + ',';

        //$(".upload_ico")[0].files[file_index].name

        $(".deleted_images").val(deleted_image);
    }
    function previewImageFile(obj) {
        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {
                var fileSource = '<?php echo base_url(); ?>_images/file.png';
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_file' + i + '"> ' +
                            '<img src="' + fileSource + '" alt="" style="width:35px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file_a(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.file_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }
        $('.proceed_text').show();
    }

    function del_prev_image_file_a(file_index) {

        $(".preview_image_file" + file_index).remove();

        var deleted_image = $(".deleted_images_files").val();
        deleted_image += file_index + ',';

        //$(".upload_ico")[0].files[file_index].name

        $(".deleted_images_files").val(deleted_image);
    }
    
    
     function fileChange(e) {

        document.getElementById('inp_img').value = '';

        for (var i = 0; i < e.target.files.length; i++) {

            var file = e.target.files[i];

            if (file.type == "image/jpeg" || file.type == "image/png") {

                var reader = new FileReader();
                reader.onload = function (readerEvent) {
                    var image = new Image();
                    image.onload = function (imageEvent) {
                        var max_size = 550;
                        var w = image.width;
                        var h = image.height;
                        if (w > h) {
                            if (w > max_size) {
                                h *= max_size / w;
                                w = max_size;
                            }
                        } else {
                            if (h > max_size) {
                                w *= max_size / h;
                                h = max_size;
                            }
                        }
                        var canvas = document.createElement('canvas');
                        canvas.width = w;
                        canvas.height = h;
                        canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                        if (file.type == "image/jpeg") {
                            var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                        } else {
                            var dataURL = canvas.toDataURL("image/png");
                        }
                        document.getElementById('inp_img').value += dataURL + '|';
                    }
                    image.src = readerEvent.target.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }
    
    
</script>

<!-- Sidebar  -->
<?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">

    <?php $this->load->view('teacher/include/header_nav'); ?>

    <div class="bodycontent">
        <h2 class="page_head"><?php echo $page_title; ?></h2>

        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <ul class="nav nav-tabs diaryTab highlightTab" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="buttontab viewClass nav-link textpos" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="view" aria-selected="true">View</a>
            </li>
            <li class="nav-item">
                <a class="buttontab createClass nav-link textpos active" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">Create</a>
            </li>
        </ul>
        <div class="rowBox">
            <div class="tab-content gapContent highlightContent compose_diary_form_container" id="myTabContent">
                <div class="tab-pane fade show" id="view" role="tabpanel" aria-labelledby="view-tab">
                    <?php
                    if (!empty($diary_detail)) {
                        $count = 1;
                        foreach ($diary_detail as $roww) {
                            $subject_id = '';
                            if ($this->uri->segment(5) && $this->uri->segment(5) != '') {
                                $diary_id = $this->uri->segment(5);

                                $subject_id = $this->my_custom_functions->get_particular_field_value(TBL_DIARY, 'subject_id', 'and id = "' . $diary_id . '"');
                                if ($subject_id != 0) {
                                    $subject_id = $subject_id;
                                } else {
                                    $subject_id = 0;
                                }
                            }


                            $break_start_date = explode('-', date('Y-m-d', $roww['issue_date']));
                            $year = $break_start_date[0];
                            $monthe = $break_start_date[1];
                            $day = $break_start_date[2];

                            $monthNum = $monthe;
                            $dateObj = DateTime::createFromFormat('!m', $monthNum);
                            $monthName = $dateObj->format('M'); // March
                            ?>
                            <a href="<?php echo base_url(); ?>teacher/user/diaryDetail/<?php echo $roww['id']; ?>" class="onOff">
                                <?php
                                $read_cls = '';
                                if ($roww['teacher_read_msg'] == 2) {
                                    $read_cls = 'read_cls';
                                    ?>

                                <?php } ?>
                                <div class="noticeGrid <?php echo $read_cls; ?>">
                                    <div class="grid_row">
                                        <div class="grid_row_left">
                                            <h2><?php echo $day; ?></h2>
                                            <span class="dayText"><?php echo $monthName; ?></span>
                                        </div>
                                        <div class="grid_row_right">
                                            <h2 class="grid_row_sub"><?php echo $roww['heading']; ?></h2>
                                            <span class="arrow_ind">
                                                <?php if ($roww['status'] == 1) { ?>
                                                    <i class="fas fa-arrow-right outgoing"></i>
                                                <?php } else { ?>
                                                    <i class="fas fa-arrow-left incoming"></i>

                                                <?php } ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <?php
                            $count++;
                        }
                        if ($count > 4) {
                            ?>
                            <a href="#" id="loadMore">Load More</a>

                            <p class="totop">
                                <a href="#top">Back to top</a>
                            </p>
                            <?php
                        }
                    }
                    ?>


                </div>
                <div class="tab-pane fade show active" id="create" role="tabpanel" aria-labelledby="create-tab">
                    <div class="formContent">

                        <?php echo form_open_multipart('teacher/user/writeDiary', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                        <?php
                        if (!empty($student_list)) {

                            foreach ($student_list as $student) {
                                ?>
                                <h5>Name : <?php echo $student['name']; ?></h5>
                                <h5>Class : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student['class_id'] . '"'); ?></h5>
                                <h5>Section : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student['section_id'] . '"'); ?></h5>
                                <h5>Roll No : <?php echo $student['roll_no']; ?></h5>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="inputFirstname">Select subject</label>
                                        <select name="subject_id" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            foreach ($subject_list as $subject) {
                                                if ($subject['id'] == $subject_id) {
                                                    $selected = "selected='selected'";
                                                } else {
                                                    $selected = "";
                                                }
                                                ?>
                                                <option value="<?php echo $subject['id']; ?>" <?php echo $selected; ?>><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                            <option value="0">other</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputFirstname">Diary heading</label>
                                        <input type="text" name="heading" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="inputFirstname">Diary note</label>
                                        <textarea name="diary_note" class="form-control" rows="20"></textarea>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <div class="uploadContainer">
                                            <label for="inputFirstname">Image</label>
                                            <ul class="photoGrid img_add_icon">
                                                <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                    <input type="file" name="doc_img_upload[]" id="inp_files" class="upload_ico" style="display:none;" multiple="" onchange="previewImage(this);fileChange(event);">
                                                    <input type="hidden" name="deleted_images" class="deleted_images" value="">
                                                    <input id="inp_img" name="img" type="hidden" value="">
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="uploadContainer">
                                            <label for="inputFirstname">File</label>
                                            <ul class="gridPdf file_add_icon">
                                                <li><a href="javascript:" onclick="callBrowserFile()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                            <!--                    <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>-->
                                                    <input type="file" name="doc_upload[]" class="upload_ico_file" style="display:none;" multiple="" onchange="previewImageFile(this);">
                                                    <input type="hidden" name="deleted_images_files" class="deleted_images_files" value="">

                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="form-group buttonContainer">
                                        <input type="hidden" name="semester_id" value="<?php echo $this->uri->segment(5); ?>">
                                        <input type="hidden" name="student_id" value='<?php echo json_encode(array($student['id'])); ?>'>
                                        <input type="submit" name="submit" class="btn002" value="Send">

                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <?php echo form_close(); ?>


                        </div>  
                    </div>
                </div>
            </div>
        </div>


        <?php $this->load->view('teacher/include/footer'); ?>
        <script type="text/javascript">
<?php if ($this->uri->segment(5) && $this->uri->segment(5) != '') { ?>
                $('[href="#create"]').tab('show');
<?php } ?>
        </script>