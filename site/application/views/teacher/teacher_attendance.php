<?php $this->load->view("teacher/include/header"); ?>

<script type="text/javascript" >
    $(document).ready(function() {
        get_attendance_status();
        
        setInterval(function() { 
            if($("#type").val() == "end") {
                get_attendance_status();     
            }
        }, 20000);
    });
    
    function add_attendance() {
        
        $.ajax({
            type:"POST",
            url:"<?php echo base_url();?>teacher/user/addAttendance",
            data:$("#attendanceForm").serialize(),
            success:function(data) {                
                get_attendance_status();
            }
        });
    }
    
    function get_attendance_status() {
        
        $.ajax({
            type:"POST",
            url:"<?php echo base_url();?>teacher/user/getTeacherAttendanceStatus",
            data:"",
            success:function(data) { 
                $(".response").html(data);
                startTime();
            }
        });
    }
    
    function startTime() {
    
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        var meridiem = h >= 12 ? "PM" : "AM";
        h = h % 12;
        h = h ? h : 12; // the hour '0' should be '12'
        h = checkTime(h);
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('current_time').innerHTML = h + ":" + m + "<span>" + meridiem + "<small>" + s + "</small></span>";
        var t = setTimeout(startTime, 500);
    }
    
    function checkTime(i) {
        
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
</script>

<!-- Sidebar  -->
<?php $this->load->view('teacher/include/side_bar'); ?>

    <div id="content">

        <?php $this->load->view('teacher/include/header_nav'); ?>

        <div class="innerbodycontent studentbdContent">

            <div class="sectionrowBox">                                             

                <div class="container_attendance">

                    <div class="header_container003">
                        <h2>Attendance</h2>
                        <span class="date_attendance"><?php echo date("F j, Y"); ?></span>
                    </div>

                    <div class="response">

                    </div>                                                                                                                                                                                                                                                    
                </div>                                                                                                               

            </div>                                    
        </div>                                        
    </div>            

<script src="//maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>

<script type="text/javascript">         
    var geocoder;
            
    $(document).ready(function() {           
        refresh_location();
        
        setInterval(function() {             
            refresh_location();                 
        }, 4000);
        
        geocoder = new google.maps.Geocoder();                                           
    });    
    
    function refresh_location() {
        
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $("#latitude").val("");
            $("#longitude").val("");
            
            $(".addrAvailable").hide();    
            $(".address_container_right").html("");
            $(".notAvailable").show();  
        }
    }
    
    function showPosition(position) {        

        geocoder.geocode({
            latLng: {lat:position.coords.latitude, lng:position.coords.longitude},
        }, function(responses) {
            if (responses && responses.length > 0) {    
                $("#latitude").val(position.coords.latitude);
                $("#longitude").val(position.coords.longitude);
                
                $(".notAvailable").hide();
                $(".addrAvailable").show();
                $(".address_container_right").html(responses[0].formatted_address);                                
            } else {
                $("#latitude").val("");
                $("#longitude").val("");
                            
                $(".addrAvailable").hide();    
                $(".address_container_right").html("");
                $(".notAvailable").show();                                
            }
        });    
    }        
</script>  

<?php $this->load->view("teacher/include/footer"); ?>
