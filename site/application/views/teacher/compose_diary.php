<?php $this->load->view('teacher/include/header'); ?>
<style>
    .onOff {
        display:none;
    }
    .totop {
        /*        position: fixed;*/
        bottom: 10px;
        right: 20px;
        padding: 10px;
        text-align: center;
        background-color: #333;
        color: #fff;
        display: block;
        width: 70%;
        margin: 17px auto;
        border-radius: 4px;
    }
    .totop{
        display: none;
    }

    a:hover {
        text-decoration: none;
    }
    /*    #loadMore {
            padding: 10px;
            text-align: center;
            background-color: #33739E;
            color: #fff;
            border-width: 0 1px 1px 0;
            border-style: solid;
            border-color: #fff;
            box-shadow: 0 1px 1px #ccc;
            transition: all 600ms ease-in-out;
            -webkit-transition: all 600ms ease-in-out;
            -moz-transition: all 600ms ease-in-out;
            -o-transition: all 600ms ease-in-out;
        }
        #loadMore:hover {
            background-color: #fff;
            color: #33739E;
        }*/

</style>
<script type="text/javascript">

    function CustomConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
    function get_section_list() {
        $('.loadd').show();
        var class_id = $('.class').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>teacher/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loadd').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
                get_semester_list(class_id);

            }
        });


    }

    function get_semester_list(class_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>teacher/user/get_semester_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                ///alert(msg);
                $('.loadd').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    if(msg == "singleSemester"){
                        $('.semm').hide();
                    }else{
                        $('.semm').show();
                        $('.semester').prop('required',true);
                    $(".semester").html(msg);
                }
                } else {
                    $(".semester").html("");
                }


            }
        });
    }

    $(document).ready(function () {
<?php if ($this->uri->segment(4) == 'search') { ?>
            $('[href="#create"]').tab('show');
<?php } ?>
    });

    $(function () {
        $(".onOff").slice(0, 4).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $(".onOff:hidden").slice(0, 4).slideDown();
            if ($(".onOff:hidden").length == 0) {
                $("#load").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
        });
    });

//    $('a[href=#top]').click(function () {
//        $('body,html').animate({
//            scrollTop: 0
//        }, 600);
//        return false;
//    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.totop a').fadeIn();
        } else {
            $('.totop a').fadeOut();
        }
    });


</script>

<!-- Sidebar  -->
<?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">

    <?php $this->load->view('teacher/include/header_nav'); ?>

    <div class="bodycontent">
        <div class="filter_select">
            <span><a href="#" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-filter"></i></a></span>
            <span><a href="<?php echo base_url(); ?>teacher/user/composeDiary">
                    <i class="fas fa-filter"></i>
                    <span class="slash_icon"><i class="fas fa-slash"></i></span>
                </a></span>

        </div>
        <h2 class="page_head"><?php echo $page_title; ?></h2>

        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="highlightTabContainer">
            <ul class="nav nav-tabs diaryTab highlightTab" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="buttontab viewClass nav-link textpos active" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="view" aria-selected="true">View</a>
                </li>
                <li class="nav-item">
                    <a class="buttontab createClass nav-link textpos" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">Create</a>
                </li>
            </ul>
            <div class="rowBox">
                <div class="tab-content gapContent highlightContent compose_diary_form_container" id="myTabContent">
                    <div class="tab-pane fade show active" id="view" role="tabpanel" aria-labelledby="view-tab">
                        <?php
                        if (!empty($diary_detail)) {
                            $count = 1;
                            foreach ($diary_detail as $roww) {

                                $break_start_date = explode('-', date('Y-m-d', $roww['issue_date']));
                                $year = $break_start_date[0];
                                $monthe = $break_start_date[1];
                                $day = $break_start_date[2];

                                $monthNum = $monthe;
                                $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                $monthName = $dateObj->format('M'); // March
                                ?>
                                <a href="<?php echo base_url(); ?>teacher/user/diaryDetail/<?php echo $roww['id']; ?>" class="onOff ads">
                                    <?php
                                    $read_cls = '';
                                    if ($roww['teacher_read_msg'] == 2) {
                                        $read_cls = 'read_cls';
                                        ?>

                                    <?php } ?>
                                    <div class="noticeGrid <?php echo $read_cls; ?>">
                                        <div class="grid_row">
                                            <div class="grid_row_left">
                                                <h2><?php echo $day; ?></h2>
                                                <span class="dayText"><?php echo $monthName; ?></span>
                                            </div>
                                            <div class="grid_row_right">
                                                <h2 class="grid_row_sub"><?php echo $roww['heading']; ?></h2>
                                                <span class="arrow_ind">
                                                    <?php if ($roww['status'] == 1) { ?>
                                                        <i class="fas fa-arrow-right outgoing"></i>
                                                    <?php } else { ?>
                                                        <i class="fas fa-arrow-left incoming"></i>

                                                    <?php } ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <?php
                                $count++;
                            }
                            if ($count > 4) {
                                ?>
                                <a href="#" id="loadMore">Load More</a>

                                <p class="totop">
                                    <a href="#top">Back to top</a>
                                </p>
                                <?php
                            }
                        }
                        ?>


                    </div>
                    <div class="tab-pane fade show" id="create" role="tabpanel" aria-labelledby="create-tab">
                        <div class="formContent">
                            <?php
                            $flag = '';
                            if ($this->uri->segment(3) == 'search') {
                                $flag = 'src';
                            }
                            if ($running_class['running_class'] > 0) {
                                ?>
                                <div class="current_student">
                                    <a href="<?php echo base_url(); ?>teacher/user/getStudentList/<?php echo $running_class['period_id']; ?>/<?php echo $flag; ?>" onclick="history.back();" class="btn002">Current Class</a>
                                </div>
                            <?php } ?>
                            <div class="rowBox" style="position: relative;">
                                <div class="loadd" style="display:none;">
                                    <div class="loader_parent">
                                        <span> <img src="<?php echo base_url(); ?>app_images/app_loader.gif"></span>
                                    </div>
                                </div>

                                <div class="formContent">
                                    <?php echo form_open_multipart('teacher/user/searchStudentList/' . $flag, array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">


                                            <label for="inputFirstname">Select Class</label>
                                            <select required="" name="class_id" class="form-control class" id="class_id" onchange="get_section_list()">
                                                <option value="">Select</option>
                                                <?php foreach ($class_list as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['class_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Select Section</label>
                                            <select  name="section_id" class="form-control section">
                                                <option value="">Select Section</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Roll Number</label>
                                            <input type="text" name="roll_no" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Enter Name</label>
                                            <input type="text" name="student_name" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6 semm" style="display:none;">

                                            <label for="inputFirstname">Select Semester</label>
                                            <select  name="semester_id" class="form-control semester">
                                                <option value="">Select Semester</option>
                                            </select>
                                        </div>


                                        <div class="form-group buttonContainer">
                                            <input type="submit" name="submit" class="btn002" value="Search">
                                        </div>

                                        <?php echo form_close(); ?>
                                        <!--                            <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <?php $this->load->view('teacher/include/footer'); ?>

        <!-- Modal -->
        <div class="modal fade filterPopup" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <!--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>-->

                    <div class="modal-body">
                        <form action="<?php echo current_url(); ?>" method="post">
                            <div class="form-group">
                                <label for="formGroupExampleInput">From date</label>
                                <input type="date" class="form-control" id="formGroupExampleInput" placeholder="From date" name="start_date">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">To date</label>
                                <input type="date" class="form-control" id="formGroupExampleInput2" placeholder="To date" name="end_date">
                            </div>
                            <div class="custom-control custom-checkbox selectall">
                                <input type="checkbox" class="custom-control-input" id="incommingmessage" name="in_message">
                                <label class="custom-control-label" for="incommingmessage">incomming message</label>
                            </div>

                            <div class="custom-control custom-checkbox selectall">
                                <input type="checkbox" class="custom-control-input" id="outgoingmessage" name="out_message">
                                <label class="custom-control-label" for="outgoingmessage">Outgoing message</label>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn closebutton" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">close</span>
                                </button>
                                <!--       <input type="submit" name="submit" value="close" class="btn">-->
                                <input type="submit" name="submit" value="apply" class="btn">
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
