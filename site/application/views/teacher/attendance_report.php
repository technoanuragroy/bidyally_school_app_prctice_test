<?php $this->load->view('teacher/include/header'); ?>

<script type="text/javascript">
    function get_section_list() {
        $('.loadd').show();
        var class_id = $('.class').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>teacher/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loadd').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
                get_semester_list(class_id);
            }
        });


    }
      function get_semester_list(class_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>teacher/user/get_semester_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                ///alert(msg);
                $('.loadd').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    if(msg == "singleSemester"){
                        $('.semm').hide();
                    }else{
                        $('.semm').show();
                        $('.semester').prop('required',true);
                    $(".semester").html(msg);
                }
                } else {
                    $(".semester").html("");
                }


            }
        });
    }
    
</script>

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="formContent">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                                <?php } ?>
                                <?php if ($this->session->flashdata("e_message")) { ?>
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                <?php } ?>
                            </div>
                            <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                          
                            <div class="rowBox">
                                <div class="formContent">
                                    <?php echo form_open_multipart('teacher/user/generateAttendanceData', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-row">
                                        <div class="loadd" style="display:none;">
                                            <div class="loader_parent_att">
                                                <span> <img src="<?php echo base_url(); ?>app_images/app_loader.gif"></span>
                                            </div>
                                            </div>
                                        <div class="form-group col-md-6">


                                            <label for="inputFirstname">Select Class</label>
                                            <select required="" name="class_id" class="form-control class" id="class_id" onchange="get_section_list()">
                                                <option value="">Select</option>
                                                <?php foreach ($class_list as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['class_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Select Section</label>
                                            <select  name="section_id" class="form-control section">
                                                <option value="">Select Section</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Roll Number</label>
                                            <input type="text" name="roll_no" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Enter Name</label>
                                            <input type="text" name="student_name" class="form-control">
                                        </div>
                                        
                                        
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">Start Date</label>
                                            <input required type="date" name="start_date" class="form-control" value="<?php echo date("Y-m-01");?>">
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="inputFirstname">End Date</label>
                                            <input required type="date" name="end_date" class="form-control" value="<?php echo date("Y-m-d");?>">
                                        </div>
                                         <div class="form-group col-md-6 semm" style="display:none;">

                                            <label for="inputFirstname">Select Semester</label>
                                            <select  name="semester_id" class="form-control semester">
                                                <option value="">Select Semester</option>
                                            </select>
                                        </div>



                                        <div class="form-group buttonContainer">
                                            <input type="submit" name="submit" class="btn002" value="Search">
                                        </div>

                                        <?php echo form_close(); ?>
                                        <!--                            <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->

                                    </div>
                                </div>
                            </div>
                             </div>
                    </div>
                            <?php $this->load->view('teacher/include/footer'); ?>
                        