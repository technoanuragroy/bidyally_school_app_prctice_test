
<?php $this->load->view('teacher/include/header'); ?>
<script type="text/javascript">
    $('#checkboxall').on('click', function () {

        if (this.checked) {
            $('.checkboxTeacher').each(function () {
                this.checked = true;
            });
        } else {
            $('.checkboxTeacher').each(function () {
                this.checked = false;
            });
        }
    });
</script>

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="innerbodycontent">
                    <div class="sectionrowBox">
                        <div class="innerbodycontent3">
                            <?php //echo form_open_multipart('teacher/user/showStudentDetail', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                           
                            
                            <ul class="studentlist profiledetailscontent">
                                
                                <?php
                                if (!empty($students)) {
                                    foreach($students as $student){
                                        ?>
                                        <li>
                                            
                                                <span class="profileimg">
                                                    <?php
                                                    $file_path = 'uploads/student/' . $student['id'] . '.jpg';
                                                    
                                                    $display_path = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL,'file_url','and student_id = "'.$student['id'].'"');
                                                    if ($display_path) {
                                                        ?>
                                                        <img src="<?php echo $display_path; ?>"> 
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>_images/NoImageFound.png"> 
                                                    <?php } ?>
                                                </span>
                                                <?php
                                                $sem_id = $this->uri->segment(5);
                                                $student_detail = $this->my_custom_functions->get_student_details_from_id($student['id']);

                                                ?>
                                                
                                                <h3><?php echo $student_detail[0]['name']; ?></h3>
                                                <span class="tag_section"><?php
                                                    $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_detail[0]['class_id'] . '"');
                                                    $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_detail[0]['section_id'] . '"');
                                                    echo 'Class - ' . $class_name . '' . $section_name;
                                                    ?></span>
                                                <span class="tag_section"><?php echo 'Roll No - ' . $student_detail[0]['roll_no']; ?></span>
                                                <div class="buttonContainer">
                            
                            <a href="<?php echo base_url(); ?>teacher/user/writeDiary/<?php echo $student['id']; ?>/<?php echo $sem_id; ?>" class="btn002">Write Diary</a>


                            
                            <!--                            <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->
                        </div>
                                                <div class="bottomprofiledetails">
                                                <span class="tag_section"><?php echo 'Date of birth - ' . date('d/m/Y',strtotime($student_detail[0]['dob'])); ?></span>
                                                <span class="tag_section"><?php echo 'Father Name - ' . $student_detail[0]['father_name']; ?></span>
                                                <span class="tag_section"><?php echo 'Mother Name - ' . $student_detail[0]['mother_name']; ?></span>
<!--                                                <span class="tag_section"><?php echo 'Mother Name - ' . $student_detail[0]['mother_name']; ?></span>
                                                <span class="tag_section"><?php echo 'Mother Name - ' . $student_detail[0]['mother_name']; ?></span>
                                                <span class="tag_section"><?php echo 'Mother Name - ' . $student_detail[0]['mother_name']; ?></span>-->
                                                <span class="custom-control custom-checkbox">
                                                </span>
                                                </div>
                                        </li>
                                        <?php
                                    
                                }}
                                ?>

                            </ul>

                            
                            
                            
                            
                            
                            
                        </div>
                        
                        
                        
                        
                        
                        
                        

                        
                        <?php //echo form_close(); ?>
                        <?php $this->load->view('teacher/include/footer'); ?>
