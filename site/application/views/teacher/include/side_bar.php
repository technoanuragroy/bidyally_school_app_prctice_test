<body>
        <?php if (ENVIRONMENT == 'production') { ?>
    <!-- Google Tag Manager (noscript) -->
    
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWL9Z9Q"

                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

        <!-- End Google Tag Manager (noscript) -->
        <?php } ?>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">
            <!-- Sidebar  -->

<nav id="sidebar">
  <div class="scrollContainer">
    <div id="dismiss">
        <!-- <i class="fas fa-arrow-left"></i> -->
        <i class="fas fa-times"></i>
    </div>
    <div class="sidebar-header">
        <span class="profileimg">
            <?php
            $teacher_id = $this->session->userdata('teacher_id');
            $name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $teacher_id . '"');
            $phone_no = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'phone_no', 'and id = "' . $teacher_id . '"');
            $file = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $teacher_id . '"');

            if ($file != '') {
                ?>
                <img src="<?php echo $file; ?>" alt="" class="">
            <?php } else { ?>
                <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt="" class="">
<?php } ?>
        </span>
        <h3><?php echo $name; ?></h3>
        <span class="tag_location"><?php echo $phone_no; ?></span>
    </div>
    <ul class="list-unstyled components">
        <!--                    <li class="">
                                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-home"></i>Home</a>
                                <ul class="collapse list-unstyled" id="homeSubmenu">
                                    <li>
                                        <a href="#">Home 1</a>
                                    </li>
                                    <li>
                                        <a href="#secondlabelhomeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home 2</a>
                                        <ul class="collapse list-unstyled secondlabel" id="secondlabelhomeSubmenu">
                                            <li>
                                                <a href="#">Home 11</a>
                                            </li>
                                            <li>
                                                <a href="#">Home 12</a>
                                            </li>
                                            <li>
                                                <a href="#">Home 13</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Home 3</a>
                                    </li>
                                </ul>
                            </li>-->
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/dashboard"><i class="fas fa-home"></i>Dashboard</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/todaysClasses"><i class="fas fa-chalkboard-teacher"></i>Classes</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>/teacher/user/getWeeklyRoutine"><i class="fas fa-calendar-week"></i>Timetable</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/composeDiary" class="ads"><i class="fas fa-book-open"></i>Diary</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/teacherAttendance"><i class="fas fa-user-check"></i>Teacher Attendance</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/attendanceReport"><i class="fas fa-user-check"></i>Attendance</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/syllabus"><i class="fas fa-book-reader"></i>Syllabus</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/schoolCalander" class="ads"><i class="far fa-calendar-alt"></i>Institute Calendar</a>
        </li>

        <li>
            <a href="<?php echo base_url(); ?>teacher/user/search"><i class="fas fa-user-graduate"></i>Students</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/getNotice"><i class="fas fa-bell"></i>Notice</a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/updateProfile"><i class="far fa-address-card"></i>Update Profile</a>
        </li>
        
        <!--                    <li>
                                <a href="<?php echo base_url(); ?>teacher/user/attendance"><i class="far fa-image"></i>Attendance</a>
                            </li>-->
        <!--                    <li>
                                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="far fa-image"></i>Pages</a>
                                <ul class="collapse list-unstyled" id="pageSubmenu">
                                    <li>
                                        <a href="#">Page 1</a>
                                    </li>
                                    <li>
                                        <a href="#">Page 2</a>
                                    </li>
                                    <li>
                                        <a href="#">Page 3</a>
                                    </li>
                                </ul>
                            </li>-->
        <!--                    <li>
                                <a href="<?php echo base_url(); ?>teacher/user/items"><i class="far fa-image"></i>Items</a>
                            </li>-->
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/changePassword"><i class="fas fa-unlock-alt"></i>Change Password</a>
        </li>
<!--        <li>
            <a href="<?php echo base_url(); ?>teacher/user/location"><i class="fas fa-unlock-alt"></i>Location</a>
        </li>-->
<!--        <li>
            <a href="<?php echo base_url(); ?>teacher/user/location_two"><i class="fas fa-unlock-alt"></i>Location Two</a>
        </li>-->
        <li>
            <a href="<?php echo base_url(); ?>teacher/user/initial_logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
        </li>
    </ul>

</div>
</nav>
 <?php
        $check_adv_flag = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'display_ads', 'and id = "' . $this->session->userdata('school_id') . '"');
        if ($check_adv_flag == 0) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('.ads').click(function (e) {
                        
                        e.preventDefault();
                        //alert($(this).attr('href'));
                        //var value = $(this).data('encrypted');
                        var value = $(this).attr('href');

                        if ($(this).hasClass('ads')) {
                            
                            
                            var new_url = '<?php echo base_url(); ?>'+'teacher/user/customAds/?V='+value;//current_url.split('/customAds/?V')[0]+'/customAds/?V='+value;
                            window.location.href = new_url;
                            
                            //var loc = 'customAds/?V=' + value;
                            //window.location = loc;
                        }


                    });
                });

            </script>
<?php } ?>           
            
