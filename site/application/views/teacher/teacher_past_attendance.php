<?php $this->load->view("teacher/include/header"); ?>

<!-- Sidebar  -->
<?php $this->load->view('teacher/include/side_bar'); ?>

    <div id="content">

        <?php $this->load->view('teacher/include/header_nav'); ?>

        <div class="innerbodycontent studentbdContent">

            <div class="sectionrowBox">                                             

                <div class="container_attendance">
                            
                    <div class="header_container003">
                        <h2>Past Attendance</h2>                        
                    </div>
                    
                    <?php 
                        if(!empty($attendance_list)) {
                            foreach($attendance_list as $attendance) {
                    ?>
                                <div class="wrapper_attendence">

                                    <span class="date_attendence"><i class="fa fa-calendar-o" aria-hidden="true"></i><?php echo $attendance['date']; ?></span>

                                    <div class="wrapperColumnAttdnc">
                                        <div class="ColumnAttdnc">
                                            <h3>Start Time</h3>
                                            <span class="time_display">
                                                <?php 
                                                    if($attendance['start_time'] != "") {
                                                        echo $attendance['start_time']; 
                                                    } else {
                                                        echo 'Not Available';
                                                    }
                                                ?>
                                            </span>
                                        </div>
                                        <span class="separator"></span>
                                        <div class="ColumnAttdnc">
                                            <h3>End Time</h3>
                                            <span class="time_display">
                                                <?php                                                            
                                                    if($attendance['end_time'] != "") {
                                                        echo $attendance['end_time']; 
                                                    } else {
                                                        echo 'Not Available';
                                                    }
                                                ?>
                                            </span>
                                        </div>
                                    </div>

                                    <span class="date_attendence">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <?php                                                     
                                            if($attendance['duration'] != "") {
                                                echo $attendance['duration']; 
                                            } else {
                                                echo 'Not Available';
                                            }
                                        ?>
                                    </span>
                                </div>
                    <?php            
                            }
                        } else {
                    ?>
                            <div>No Past Attendance Found</div>
                    <?php                                     
                        }
                    ?>                                                      
                                                                                                                                                                                                          
                </div> 
                
            </div>                                        
        </div>
    </div>        

<?php $this->load->view("teacher/include/footer"); ?>
