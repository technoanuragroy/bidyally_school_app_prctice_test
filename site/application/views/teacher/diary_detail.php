<?php $this->load->view('teacher/include/header'); ?>


            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">

                    <div class="rowBox">
                        <h2 class="page_head"><?php echo $page_title; ?></h2>
                        <div class="togglebtnContainer">
                            <!--                            <ul class="nav nav-tabs diaryTab" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                        <a class="buttontab presentbtn nav-link active" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="view" aria-selected="true">View</a>
                                                            </li>
                                                            <li class="nav-item">
                                                        <a class="buttontab absentbtn nav-link" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">Create</a>
                                                            </li>
                                                        </ul>
                                                        <a href="javascript:" class="buttontab clearbtn">clear</a>-->
                        </div>
                        <div class="noticedetails">
                            <!--                            <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="view" role="tabpanel" aria-labelledby="view-tab">-->
                            <?php
                            if (!empty($diary_detail)) {
                                foreach ($diary_detail as $diary) {
                                    $parent_name = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'name', 'and id = "' . $diary['parent_id'] . '"');
                                    $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $diary['subject_id'] . '"');
                                    ?>
                                    <h2><?php echo $diary['heading']; ?></h2>
                                    <div class="content_noticeDetails">
                                    <span class="date__notice"><?php echo date('M d Y', ($diary['issue_date'])); ?></span>
                                    <p>
                                        <?php echo $diary['diary_note']; ?>
                                    </p>
                                    
                            <!--                                ////////////////////////////////// If attachment is present for diary /////////////////////////////////-->

                            <?php
                            if (!empty($diary_attachment)) {
                                $image_count = 0;
                                $style = '';
                                $count = '';
                                $cc = 0;
                                $ddisp = '';
                                $class = '';

                                foreach ($diary_attachment as $index => $filess) {
                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                    $path = $filess['file_url'];
                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                        $image_count++;
                                        $display_path = $path;
                                        $download_path = $path;
                                        $width = '50px';
                                        ?>
                                        <li style="list-style: none;">
                                            <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery"><img src="<?php echo $display_path; ?>" alt="" style="width:<?php echo $width; ?>"/></a>
                                            <span><?php echo $ddisp; ?></span>
                                            <small>
                                                <a class="downloadFile" target="_blank" href="<?php echo $download_path; ?>" download="">
                                                    <i class="fas fa-download"></i></a>
                                            </small>

                                        </li>
                                        <?php
                                    }

                                    if ($ext == 'xlsx') {

                                        $display_paths = base_url() . 'app_images/XLSX.png';
                                        $download_paths = $path;
                                        $width = '48px';
                                        ?>
                                        <li style="list-style:none;">
                                            <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                            <span><?php //echo $file_ddisp; ?></span>

                                            <small>
                                                <a class="downloadFile"  href="<?php echo $download_paths; ?>" download="hello">
                                                    <i class="fas fa-download"></i></a>

                                            </small>

                                        </li>
                                        <?php
                                    } if ($ext == 'xls') {

                                        $display_paths = base_url() . 'app_images/XLS.png';
                                        $download_paths = $path;
                                        $width = '48px';
                                        ?>
                                        <li style="list-style: none;">
                                            <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                            <span><?php //echo $file_ddisp; ?></span>

                                            <small>
                                                <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                    <i class="fas fa-download"></i> </a>

                                            </small>

                                        </li>
                                        <?php
                                    } if ($ext == 'docx') {

                                        $display_paths = base_url() . 'app_images/DOCX.png';
                                        $download_paths = $path;
                                        $width = '48px';
                                        ?>
                                        <li style="list-style: none;">
                                            <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                            <span><?php //echo $file_ddisp; ?></span>

                                            <small>
                                                <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                    <i class="fas fa-download"></i></a>

                                            </small>

                                        </li>
                                        <?php
                                    } if ($ext == 'doc') {

                                        $display_paths = base_url() . 'app_images/DOC.png';
                                        $download_paths = $path;
                                        $width = '48px';
                                        ?>
                                        <li style="list-style: none;">
                                            <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                            <span><?php //echo $file_ddisp; ?></span>

                                            <small>
                                                <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                    <i class="fas fa-download"></i>
                                                </a>

                                            </small>

                                        </li>
                                        <?php
                                    } if ($ext == 'pdf') {

                                        $display_paths = base_url() . 'app_images/PDF.png';
                                        $download_paths = $path;
                                        $width = '48px';
                                        ?>
                                        <li style="list-style: none;">
                                            <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                            <span><?php //echo $file_ddisp; ?></span>

                                            <small>
                                                <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                    <i class="fas fa-download"></i>
                                                </a>

                                            </small>

                                        </li>
                                        <?php
                                    } if ($ext == 'pptx') {

                                        $display_paths = base_url() . 'app_images/PPTX.png';
                                        $download_paths = $path;
                                        $width = '48px';
                                        ?>
                                        <li style="list-style: none;">
                                            <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                            <span><?php //echo $file_ddisp; ?></span>

                                            <small>
                                                <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                    <i class="fas fa-download"></i>
                                                </a>

                                            </small>

                                        </li>
                                    <?php
                                    }
                                }
                            }
                        
                        ?>
                        <!--                               ////////////////////////////////////  ATTACHMENT END //////////////////////////////////////-->

                                    <div class="teacher_detail_container">
                                        <div class="teacher_detail_left content_noticeDetailsLeft">
                                            By - <?php echo $parent_name; ?><br>
                                            Subject - <?php echo $subject_name; ?>
                                            
                                        </div>
                                        <div class="teacher_detail_left content_noticeDetailsLeft">
                                           
                                            
                                        </div>
                                        <div class="teacher_detail_right content_noticeDetailsRight">
                                            <a href="<?php echo base_url(); ?>teacher/user/writeDiary/<?php echo $diary['student_id']; ?>/<?php echo $diary['id']; ?>" class="btn002"><i class="fas fa-arrow-left"></i> Reply</a>
                                        </div>
                                    </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            
                        </div>
                        <?php $diary_id = $this->uri->segment(4); 
                        $diar_row_detail = $this->my_custom_functions->get_details_from_id($diary_id,TBL_DIARY);
                        if(!empty($diar_row_detail)){
                          $school_id = $diar_row_detail['school_id'];
                          $student_id = $diar_row_detail['student_id'];
                          $teachher_id = $diar_row_detail['teacher_id'];
                          $parent_id = $diar_row_detail['parent_id'];
                        $diary_conv = $this->my_custom_functions->get_multiple_data(TBL_DIARY,'and school_id = "'.$school_id.'" and student_id = "'.$student_id.'" and teacher_id = "'.$teachher_id.'" and parent_id = "'.$parent_id.'" order by id desc limit 0,5');
                        if(!empty($diary_conv)){
                        ?>
                        <div class="accordion" id="accordionExample">
                            <?php 
                            $show = '';
                            $count = 1;
                            foreach($diary_conv as $diary_convertation){ 
                                if($count == 1){
                                    $show = 'show';
                                }else{
                                    $show = '';
                                }
                                ?>
                            <div class="card">
                                <div class="card-header" id="heading<?php echo $diary_convertation['id']; ?>">
                                    <h2 class="mb-0">
                                        <button class="headingdetails" type="button" data-toggle="collapse" data-target="#collapse<?php echo $diary_convertation['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $diary_convertation['id']; ?>">
                                            <?php echo $diary_convertation['heading']; ?>
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse<?php echo $diary_convertation['id']; ?>" class="collapse <?php echo $show; ?>" aria-labelledby="heading<?php echo $diary_convertation['id']; ?>" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <?php echo $diary_convertation['diary_note']; ?>
                                    </div>
                                </div>
                            </div>
                            
                       <?php $count++;} ?>
                        </div>
                        <?php }} ?>
                    </div>
                </div>
                <?php $this->load->view('teacher/include/footer'); ?>
