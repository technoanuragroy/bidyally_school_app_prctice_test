<?php $this->load->view('teacher/include/header'); ?>
<style>
    .gridPdf li{
        height:0px !important;
    }
    .down{
        width:100% !important;
        float: left;
        text-align: center;
        
    }
    .img_add_icon li:first-child a {
    padding-top: 0px;
    display: block;
</style>
<script type="text/javascript">
     function callBrowser() {
        $('.upload_ico').click();
    }
    function previewImage(obj) {

        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_' + i + '"> ' +
                            '<img src="' + e.target.result + '" alt="" style="width:300px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.img_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }
    }
    </script>

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="innerbodycontent container_teacherdashboard">


                    <div class="rowBox">
                        <button id="find_btn">Find Me</button>
                        <div id="result"></div> 
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="gridPdf">
                                    <li>
                                        <label>Browse file</label>
                                        <input type="file" name="doc_upload[]" class="upload_ico_file" multiple="">

                                    </li>
                                </ul>
                            </div>
                            <div class="form-group col-md-12">
                                    <label for="inputFirstname">Image</label>
                                    <ul class="photoGrid img_add_icon">
                                        <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                            <input type="file" name="doc_img_upload[]" class="upload_ico" style="display:none;" multiple="" onchange="previewImage(this);">
                                            <input type="hidden" name="deleted_images" class="deleted_images" value="">
                                        </li>
                                    </ul>
                            </div>
<!--                            <div class="col-md-6">
                                <ul class="gridPdf">
                                    <li>
                                        <label>Download file</label>
                                        <p class="down"><a class="downloadFile down" href="<?php echo base_url() . 'teacher/user/downloadFile/171' ?>/<?php echo DOWNLOAD_KEY; ?>" download="hello">
                                            <i class="fas fa-download"></i>
                                        </a></p>
                                    </li>
                                </ul>
                            </div>-->
                            <div class="form-group col-md-12">
                                    
                                    <ul class="photoGrid img_add_icon">
                                        <li>
                                            <a href="https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563865686_sample.jpeg"><img src="https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563865686_sample.jpeg" style="width:100px;height:100px;"></a>
                                        </li>
                                        <li>
                                            <a href="https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563865685_ADMISSION-FORM1.pdf"><img src="<?php echo base_url(); ?>app_images/PDF.png" style="width:100px;height:100px;"></a>
                                        </li>
                                        <li>
                                            <a href="https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563866184_demo1.docx"><img src="<?php echo base_url(); ?>app_images/DOCX.png" style="width:100px;height:100px;"></a>
                                        </li>
                                        <li>
                                            <a href="https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563866185_excelfile1.xlsx"><img src="<?php echo base_url(); ?>app_images/XLSX.png" style="width:100px;height:100px;"></a>
                                        </li>
                                    </ul>
                            </div>
                            
                            
                            
<a href="https://web.bidyaaly.com" target="_blank" style="font-size: 20px; font-weight: bold; width: 100%; display: block; float: left;">Target Blank&nbsp;</a>

<!-- AddToAny BEGIN -->
<a class="a2a_dd" href="https://web.bidyaaly.com" style="font-size: 20px; font-weight: bold; width: 100%; display: block; float: left;">&nbsp;Add to Any Share&nbsp;</a>

<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
<a href="https://ablion.in/share" target="share" style="font-size: 20px; font-weight: bold; width: 100%; display: block; float: left;">&nbsp;Custom Share&nbsp;</a>                            

                        </div>
                    </div>

                    <?php $this->load->view('teacher/include/footer'); ?>
                    <script type="text/javascript">
                        $("#find_btn").click(function () { //user clicks button
                            if ("geolocation" in navigator) { //check geolocation available
                                //try to get user current location using getCurrentPosition() method
                                navigator.geolocation.getCurrentPosition(function (position) {
                                    $("#result").html("Found your location <br />Lat : " + position.coords.latitude + " </br>Lang :" + position.coords.longitude);
                                });
                            } else {
                                console.log("Browser doesn't support geolocation!");
                            }
                        });
                    </script>

