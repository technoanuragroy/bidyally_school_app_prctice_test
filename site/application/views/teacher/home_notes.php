<?php $this->load->view('teacher/include/header'); ?>
<script type="text/javascript">
    function callBrowser() {
        $('.upload_ico').click();
    }
    function callBrowserFile() {
        $('.upload_ico_file').click();
    }

    function disp_cls() {
        $('.img_add_icon > li').each(function () {
            $('.img_add_icon > li > span').text('');
            $('.overlay_ccl').find('span').remove();
            $('.img_add_icon > li > span').removeClass('overlay_note_img');
            $(this).fadeIn(1000);
        });
    }

    function disp_file() {
        $('.file_add_icon > li').each(function () {
            $('.file_add_icon > li > span').text('');
            $('.overlay_ccl').find('span').remove();
            $('.file_add_icon > li > span').removeClass('overlay_note_img');
            $(this).fadeIn(1000);
        });

    }
    function deleteImgFile(fileID) {
        $('.message_block').html('<p>Are you sure that you want to delete this image?</p>');
        $('.btn-yes').attr('onclick', "confirm_delete('" + fileID + "')");
    }
    function confirm_delete(fileID) {
        window.location.href = "<?php echo base_url(); ?>teacher/user/deleteFileHomenote/" + fileID;
    }
    function previewImage(obj) {

        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_' + i + '"> ' +
                            '<img src="' + e.target.result + '" alt="" style="width:300px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.img_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }
        $('.proceed_text').show();
    }
    function del_prev_image_file(file_index) {

        $(".preview_image_" + file_index).remove();

        var deleted_image = $(".deleted_images").val();
        deleted_image += file_index + ',';

        //$(".upload_ico")[0].files[file_index].name

        $(".deleted_images").val(deleted_image);
    }

    function previewImageFile(obj) {
        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {
                var fileSource = '<?php echo base_url(); ?>_images/file.png';
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_file' + i + '"> ' +
                            '<img src="' + fileSource + '" alt="" style="width:33px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file_a(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.file_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }

        $('.proceed_text').show();
    }

    function del_prev_image_file_a(file_index) {

        $(".preview_image_file" + file_index).remove();

        var deleted_image = $(".deleted_images_files").val();
        deleted_image += file_index + ',';

        //$(".upload_ico")[0].files[file_index].name

        $(".deleted_images_files").val(deleted_image);
    }
</script>
<script type="text/javascript">

    function fileChange(e) {

        document.getElementById('inp_img').value = '';

        for (var i = 0; i < e.target.files.length; i++) {

            var file = e.target.files[i];

            if (file.type == "image/jpeg" || file.type == "image/png") {

                var reader = new FileReader();
                reader.onload = function (readerEvent) {
                    var image = new Image();
                    image.onload = function (imageEvent) {
                        var max_size = 550;
                        var w = image.width;
                        var h = image.height;
                        if (w > h) {
                            if (w > max_size) {
                                h *= max_size / w;
                                w = max_size;
                            }
                        } else {
                            if (h > max_size) {
                                w *= max_size / h;
                                h = max_size;
                            }
                        }
                        var canvas = document.createElement('canvas');
                        canvas.width = w;
                        canvas.height = h;
                        canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                        if (file.type == "image/jpeg") {
                            var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                        } else {
                            var dataURL = canvas.toDataURL("image/png");
                        }
                        document.getElementById('inp_img').value += dataURL + '|';
                    }
                    image.src = readerEvent.target.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }
    //document.getElementById('inp_files').addEventListener('change', fileChange, false);

</script>

<!-- Sidebar  -->
<?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">

    <?php $this->load->view('teacher/include/header_nav'); ?>
    <div class="bodycontent">
        <h2 class="page_head"><?php echo $page_title; ?></h2>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="rowBox">
            <div class="formContent notesDetails">
                <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <!-- <label for="inputFirstname">Topic</label> -->
                        <input type="text" required="" placeholder="Topic" name="topic_name" class="form-control" value="<?php
                        if (!empty($note_detail)) {
                            echo $note_detail['notes']['topic_name'];
                        }
                        ?>">
                    </div>
                    <div class="form-group col-md-12">
                        <!-- <label for="inputFirstname">Notes</label> -->
                        <textarea class="form-control" placeholder="Notes" name="classnote_text" id="exampleFormControlTextarea1" rows="3" cols="10"><?php
                            if (!empty($note_detail)) {
                                echo $note_detail['notes']['classnote_text'];
                            }
                            ?></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="uploadContainer">
                            <label for="inputFirstname">Image</label>
                            <ul class="photoGrid img_add_icon">
                                <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                    <input type="file" name="doc_img_upload[]" id="inp_files" class="upload_ico" style="display:none;" multiple="" onchange="previewImage(this);
                                                    fileChange(event);">
                                    <input type="hidden" name="deleted_images" class="deleted_images" value="">
                                    <input id="inp_img" name="img" type="hidden" value="">
                                </li>
                                <?php
                                //echo "<pre>";print_r($note_detail);
                                if (!empty($note_detail['note_file'])) {
                                    $image_count = 0;
                                    $style = '';
                                    $count = '';
                                    $cc = 0;
                                    $ddisp = '';
                                    $class = '';
                                    foreach ($note_detail['note_file'] as $index => $filess) {
                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                        $path = $filess['file_url'];

                                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                            $cc++;
                                        }
                                    }
                                    $cou = $cc - 3;
                                    if ($cou == 0) {
                                        $disp_count = '';
                                        $manage_class = '';
                                    } else {
                                        $disp_count = '+' . $cou;
                                        $manage_class = '<span class="overlay_note_img"></span>';
                                    }
                                    foreach ($note_detail['note_file'] as $index => $filess) {
                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                        $path = $filess['file_url'];
                                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                            $image_count++;
                                            $display_path = $path;
                                            $download_path = $path;
                                            $width = '300px';
                                            if ($image_count > 3) {
                                                $style = "style='display:none;'";
                                            }
                                            if ($image_count == 3) {
                                                $class = 'onclick="disp_cls()" class="overlay_ccl"';
                                                $ddisp = $disp_count;
                                            } else {
                                                $class = '';
                                            }
                                            ?>
                                            <li <?php echo $style; ?> <?php echo $class; ?>><?php echo $manage_class; ?>
                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery"><img src="<?php echo $display_path; ?>" alt="" style="width:<?php echo $width; ?>"/></a>
                                                <span><?php echo $ddisp; ?></span>
                                                <?php
                                                $check_editing = $this->my_custom_functions->check_date_range();
                                                if ($check_editing == 1) {
                                                    ?>
                                                    <small>
                                                        <a class="downloadFile" href="<?php echo $download_path; ?>" download="">
                                                            <i class="fas fa-download"></i></a>
                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess['id']; ?>);">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </small>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="uploadContainer">
                            <label for="inputFirstname">File</label>
                            <ul class="gridPdf file_add_icon">
                                <li><a href="javascript:" onclick="callBrowserFile()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
            <!--                    <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>-->
                                    <input type="file" name="doc_upload[]" class="upload_ico_file" style="display:none;" multiple="" onchange="previewImageFile(this);">
                                    <input type="hidden" name="deleted_images_files" class="deleted_images_files" value="">
                                </li>
                                <?php
                                $file_count = 0;
                                $flag = 0;
                                if (!empty($note_detail['note_file'])) {
                                    $file_image_count = 0;
                                    $file_style = '';
                                    $file_count = '';
                                    $file_cc = 0;
                                    $file_ddisp = '';
                                    $file_class = '';
                                    $display_paths = '';
                                    $download_paths = '';
                                    $ext = '';
                                    $path = '';
                                    $disp_count = 0;
                                    $cou = 0;
                                    //$file_count = 0;
                                    foreach ($note_detail['note_file'] as $index => $filess) {
                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                        $path = $filess['file_url'];
                                        if ($ext == 'xlsx' || $ext == 'XLSX' || $ext == 'xls' || $ext == 'XLS' || $ext == 'docx' || $ext == 'DOCX' || $ext == 'doc' || $ext == 'DOC' || $ext == 'pdf' || $ext == 'PDF') {
                                            $file_cc++;
                                        }
                                    }
                                    $cou = $file_cc - 3;
                                    $disp_count = '+' . $cou;
                                    foreach ($note_detail['note_file'] as $index => $filess_fl) {
                                        $flag = 0;
                                        $extt = trim(pathinfo($filess_fl['file_url'], PATHINFO_EXTENSION));
                                        $file_count++;
                                        $path = $filess_fl['file_url'];


                                        if ($file_image_count > 2) {
                                            $file_style = "style='display:none;'";
                                        }
                                        if ($file_image_count == 2) {
                                            $file_class = 'onclick="disp_file()" class="overlay_ccl"';
                                            $file_ddisp = $disp_count;
                                        } else {
                                            $file_class = '';
                                        }
                                        if ($extt == 'xlsx') {
                                            $file_image_count++;

                                            $display_paths = base_url() . 'app_images/XLSX.png';
                                            $download_paths = $path;
                                            $width = '100px';
                                            ?>
                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                <span class="overlay_note_img"></span>
                                                <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                                <span><?php echo $file_ddisp; ?></span>
                                                <?php
                                                $check_editing = $this->my_custom_functions->check_date_range();
                                                if ($check_editing == 1) {
                                                    ?>
                                                    <small>
                                                        <a class="downloadFile"  href="<?php echo $download_paths; ?>" download="hello">
                                                            <i class="fas fa-download"></i></a>
                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </small>
                                                <?php } ?>
                                            </li>
                                            <?php
                                        } if ($extt == 'xls') {
                                            $file_image_count++;

                                            $display_paths = base_url() . 'app_images/XLS.png';
                                            $download_paths = $path;
                                            $width = '100px';
                                            ?>
                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                <span class="overlay_note_img"></span>
                                                <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                                <span><?php echo $file_ddisp; ?></span>
                                                <?php
                                                $check_editing = $this->my_custom_functions->check_date_range();
                                                if ($check_editing == 1) {
                                                    ?>
                                                    <small>
                                                        <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                            <i class="fas fa-download"></i> </a>
                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </small>
                                                <?php } ?>
                                            </li>
                                            <?php
                                        } if ($extt == 'docx') {
                                            $file_image_count++;

                                            $display_paths = base_url() . 'app_images/DOCX.png';
                                            $download_paths = $path;
                                            $width = '48px';
                                            ?>
                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                <span class="overlay_note_img"></span>
                                                <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                                <span><?php echo $file_ddisp; ?></span>
                                                <?php
                                                $check_editing = $this->my_custom_functions->check_date_range();
                                                if ($check_editing == 1) {
                                                    ?>
                                                    <small>
                                                        <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                            <i class="fas fa-download"></i></a>
                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </small>
                                                <?php } ?>
                                            </li>
                                            <?php
                                        } if ($extt == 'doc') {
                                            $file_image_count++;

                                            $display_paths = base_url() . 'app_images/DOC.png';
                                            $download_paths = $path;
                                            $width = '48px';
                                            ?>
                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                <span class="overlay_note_img"></span>
                                                <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                                <span><?php echo $file_ddisp; ?></span>
                                                <?php
                                                $check_editing = $this->my_custom_functions->check_date_range();
                                                if ($check_editing == 1) {
                                                    ?>
                                                    <small>
                                                        <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                            <i class="fas fa-download"></i>
                                                        </a>
                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </small>
                                                <?php } ?>
                                            </li>
                                            <?php
                                        } if ($extt == 'pdf') {
                                            $file_image_count++;

                                            $display_paths = base_url() . 'app_images/PDF.png';
                                            $download_paths = $path;
                                            $width = '48px';
                                            ?>
                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                <span class="overlay_note_img"></span>
                                                <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                                <span><?php echo $file_ddisp; ?></span>
                                                <?php
                                                $check_editing = $this->my_custom_functions->check_date_range();
                                                if ($check_editing == 1) {
                                                    ?>
                                                    <small>
                                                        <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                            <i class="fas fa-download"></i>
                                                        </a>
                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                            <i class="fas fa-trash-alt"></i>
                                                        </a>
                                                    </small>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                                ?>

                            </ul>
                        </div>
                    </div>              
                </div>
                <input type="hidden" name="semester_id" value="<?php echo $this->uri->segment(4); ?>">
                <input type="hidden" name="section_id" value="<?php echo $this->uri->segment(5); ?>">
                <input type="hidden" name="period_id" value="<?php echo $this->uri->segment(6); ?>">
                <div class="col-lg-12 buttonContainer" style="float:left;">
                    <div class="proceed_text" style="display:none;">Please save the file before you proceed</div>
                    <?php
                    $update_button = 0;
                    if (!empty($note_detail)) {
                        if ($note_detail['notes']['publish_data'] == 1) {
                            $update_button = 1;
                        }
                    }
                    $check_editing = $this->my_custom_functions->check_date_range();
                    if ($check_editing == 1) {
                        if ($update_button == 0) {
                            ?>
                            <input type="submit" name="submit" class="btn002" value="Save">
                            <input type="submit" name="submit" class="btn002" value="Publish">
                        <?php } else {
                            ?>
                            <input type="submit" name="submit" class="btn002" value="Update">    
                            <?php
                        }
                    }
                    ?>
                    <!--                            <a href="javascript:" onclick="history.back();" class="btn002">Publish</a>-->
                    <?php echo form_close(); ?>
                    <!--                            <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->
                </div>
            </div>

        </div>
        <?php $this->load->view('teacher/include/footer'); ?>
