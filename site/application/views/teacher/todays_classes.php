<?php $this->load->view('teacher/include/header'); ?>
<script>
    
    
    
    $(document).ready(function () {
//        $('#date_cal').datepicker({
//            dateFormat: 'yy-mm-dd',
//        });
        
        $('#date_cal_val').click(function(){
            $('#date_cal').focus();
            
        });  
        
        $('#date_cal').change(function() {
        $('#pastClasses').submit();
    });
    });
    
    

                    </script>

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="innerbodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="routine_container">
                        <a href="<?php echo base_url(); ?>teacher/user/getWeeklyRoutine"><img src="<?php echo base_url(); ?>app_images/routine_icon.png"> Routine</a>
                    </div>
                    <div class="routine_calender">
                        
                        <form method="post" action="<?php echo base_url(); ?>teacher/user/todaysClasses" id="pastClasses">
                            <div class="past_work">
                            <span>Past work</span>
                            <label class="openCalender">
                             
<!--                        <input required type="date" style="opacity: 1;" class="form-control" min="<?php //echo date("Y-m-d", strtotime('monday this week')) ?>" max="<?php //echo date("Y-m-d", strtotime('sunday this week')) ?>" id="date_cal" name="class_date" >-->
                                
                                <input required type="date" style="opacity: 1;" class="form-control" id="date_cal" name="class_date" >
                        <a href="javascript:" id="date_cal_val" ><i class="far fa-calendar-alt"></i></i></a>
                            </label>
                            </div>
                        </form>
                    </div>

                    <div class="sectionrowBox">

                        <?php
                                               
                        if (!empty($get_todays_classes)) {
                            foreach ($get_todays_classes as $row) {//echo $this->session->userdata('query_date');
                                if($this->session->userdata('query_date') == date('Y-m-d')){
                                if (date('H:i:s') > $row['period_end_time']) {
                                    $inactive_class = 'inactive_class';
                                }else {
                                    $inactive_class = '';
                                }
                                } else {
                                    $inactive_class = 'inactive_class';
                                }
                                ?>   
                        <a href="<?php echo base_url(); ?>teacher/user/dailyWorks/<?php echo $row['semester_id']; ?>/<?php echo $row['section_id']; ?>/<?php echo $row['id']; ?>" class="ads">
                                    <div class="section_Grid <?php echo $inactive_class; ?>">
                                        <h3>
                                            <?php
                                            if ($row['subject_id'] == 0) {
                                                echo $row['period_name'];
                                            } else {
                                                echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $row['subject_id'] . '"');
                                            }
                                            ?></h3>

                                        <span class="section_selection">
                                            <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $row['class_id'] . '"'); ?>, <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $row['section_id'] . '"'); ?>
                                        </span>
                                        <span class="time_tag"><i class="far fa-clock"></i>
                                            <?php echo date('h:i a', strtotime($row['period_start_time'])) . ' - ' . date('h:i a', strtotime($row['period_end_time'])); ?></span>
                                    </div>
                                </a>
                                <?php
                            }
                        }else{
                        ?>
                        No class found
                        <?php } ?>
<!--                        <div class="col-lg-12 buttonContainer">  <a href="#" class="btn002">dashbord</a></div>-->

                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
                    