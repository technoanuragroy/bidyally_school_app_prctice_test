<?php $this->load->view('teacher/include/header'); ?>

        <!-- Sidebar  -->
        <?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">
        <!-- <div class="innerHeadingContent">
            <h1>Today's class</h1>
        </div> -->

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <!-- <i class="fas fa-align-left"></i> -->
                    <i class="fas fa-bars"></i>
                    <!-- <span>Menu</span> -->
                </button>
                <div class="headingDiv"><h2>Today's class</h2></div>
                <?php $this->load->view('teacher/include/notification_bell'); ?>
                <a href="javascript:" onclick="history.back();"> <span class="iconsDiv"><i class="fas fa-arrow-left"></i></a>
            </div>
          </nav>

        <div class="innerbodycontent">
          <div class="sectionrowBox">
            <div class="innerbodycontent3">
              <h2>notes</h2>
              <div class="form-group">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" cols="10"></textarea>
              </div>

              <h2>Images</h2>
              <ul class="photoGrid">
                <li><img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/></li>
                <li><img src="<?php echo base_url(); ?>app_images/profileimg.jpg" alt=""/></li>
                <li><img src="<?php echo base_url(); ?>app_images/profileimg.jpg" alt=""/></li>
                <li><img src="<?php echo base_url(); ?>app_images/profileimg.jpg" alt=""/><a>+5</a></li>
              </ul>


              <div class="pdfFilescontainer">
                <h2>files</h2>
                <ul class="gridPdf">
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/PDF.png" alt="" class="pdfimg"/>
                    <h2>Debnath Bhattacharyya</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/PPT.png" alt="" class="pdfimg"/>
                    <h2>Apudi</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/PDF.png" alt="" class="pdfimg"/>
                    <h2>Kuntal Mukerjee</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/XLSX.png" alt="" class="pdfimg"/>
                    <h2>Debnath Bhattacharyya</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/PDF.png" alt="" class="pdfimg"/>
                    <h2>Apudi</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/DOCX.png" alt="" class="pdfimg"/>
                    <h2>Kuntal Mukerjee</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/PDF.png" alt="" class="pdfimg"/>
                    <h2>Kuntal Mukerjee</h2>
                  </li>
                  <li>
                    <img src="<?php echo base_url(); ?>app_images/DOC.png" alt="" class="pdfimg"/>
                    <h2>+3</h2>
                  </li>
              </div>

            </div>

              <div class="col-lg-12 buttonContainer" style="float:left;">
              <a href="javascript:" onclick="history.back();" class="btn002">save</a>
              <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>
        </div>
         <?php $this->load->view('teacher/include/footer'); ?>
