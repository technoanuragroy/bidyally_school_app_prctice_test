<?php $this->load->view('teacher/include/header'); ?>

        <!-- Sidebar  -->
        <?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">
        <!-- <div class="innerHeadingContent">
            <h1>Today's class</h1>
        </div> -->

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <!-- <i class="fas fa-align-left"></i> -->
                    <i class="fas fa-bars"></i>
                    <!-- <span>Menu</span> -->
                </button>
                <div class="headingDiv"><h2>Today's class</h2></div>
                <?php $this->load->view('teacher/include/notification_bell'); ?>
                <a href="javascript:" onclick="history.back();"> <span class="iconsDiv"><i class="fas fa-arrow-left"></i></span></a>
            </div>
          </nav>

        <div class="innerbodycontent">

          <div class="sectionrowBox">
          <div class="section_Grid">
            <h3>maths</h3>
            <span class="section_selection">7A</span>
            <span class="time_tag"><i class="far fa-clock"></i>11:00 am-12:00pm</span>
            </div>

          <div class="section_Grid">
            <h3>maths</h3>
            <span class="section_selection">9b</span>
            <span class="time_tag"><i class="far fa-clock"></i>12:00pm - 12:45pm</span>
          </div>

          <div class="section_Grid">
            <h3>science</h3>
            <span class="section_selection">5b</span>
            <span class="time_tag"><i class="far fa-clock"></i>1:00pm-1:45pm</span>
          </div>

          <div class="section_Grid">
            <h3>science</h3>
            <span class="section_selection">3c</span>
            <span class="time_tag"><i class="far fa-clock"></i>2:30pm-3:15pm</span>
          </div>

          <div class="section_Grid">
            <h3>geometry</h3>
            <span class="section_selection">vi b</span>
            <span class="time_tag"><i class="far fa-clock"></i>3:15pm-4:00pm</span>
          </div>

<div class="col-lg-12 buttonContainer">  <a href="#" class="btn002">dashbord</a></div>

        </div>
         <?php $this->load->view('teacher/include/footer'); ?>
