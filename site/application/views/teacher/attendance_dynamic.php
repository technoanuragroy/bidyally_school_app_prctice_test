<?php $this->load->view('teacher/include/header'); ?>

        <!-- Sidebar  -->
        <?php $this->load->view('teacher/include/side_bar'); ?>
<div id="content">
        <!-- <div class="innerHeadingContent">
            <h1>Today's class</h1>
        </div> -->

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <!-- <i class="fas fa-align-left"></i> -->
                    <i class="fas fa-bars"></i>
                    <!-- <span>Menu</span> -->
                </button>

                <div class="headingDiv"><h2>Class name:7A </h2></div>
                <a href="javascript:" onclick="history.back();"> <span class="iconsDiv"><i class="fas fa-arrow-left"></i></a>
            </div>
          </nav>

        <div class="innerbodycontent studentbdContent">

        <div class="sectionrowBox">
          <div class="togglebtnContainer">
          <a href="#" class="btn btn-dark-green btn-lg select-all checkall">All present</a>
          <a href="#" class="btn btn-danger btn-lg deselect-all uncheckall">All absent</a>
        </div>

            <?php if(!empty($student_list)){ ?>
            <form class="was-validated">
              <div class="studentGrid" >

                <ul class="profileStudent">
                    <?php foreach($student_list as $row){ ?>
                      <li>
                        <div class="check-control">
                        <input type="checkbox" class="check-control-input" name="student_id" value="<?php echo $row['id']; ?>" id="customControlValidation<?php echo $row['id']; ?>" required>
                        <label class="check-control-label" for="customControlValidation<?php echo $row['id']; ?>">
                        <span class="profileimg">
                        <span class="rollname"><?php echo $row['roll_no']; ?></span>
                        <?php
                            $file = 'uploads/student/'.$row['id'].'.jpg';
                            $file_path = base_url().'uploads/student/'.$row['id'].'.jpg';
                            if(file_exists($file)){
                        ?>
                        <img src="<?php echo $file_path; ?>" alt="" class="">
                            <?php }else{ ?>

                            <?php } ?>
                        </span>
                        </label>
                      </div>
                      </li>
                  <li>
                  <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" name="student_id" value="<?php echo $row['id']; ?>" id="customControlValidation<?php echo $row['id']; ?>" required>
                    <label class="custom-control-label" for="customControlValidation<?php echo $row['id']; ?>">
                    <span class="profileimg">
                    <span class="rollname"><?php echo $row['roll_no']; ?></span>
                    <?php
                        $file = 'uploads/student/'.$row['id'].'.jpg';
                        $file_path = base_url().'uploads/student/'.$row['id'].'.jpg';
                        if(file_exists($file)){
                    ?>
                    <img src="<?php echo $file_path; ?>" alt="" class="">
                        <?php }else{ ?>

                        <?php } ?>
                    </span>
                    </label>
                    <span class="studentname"><?php echo $row['name']; ?></span>
                  </div>
                </li>
                    <?php } ?>
        </ul>

        </div>
        </form>
                    <?php } ?>
       <div class="col-lg-12 buttonContainer">  <a href="#" class="btn002">back</a></div>
        </div>
         <?php $this->load->view('teacher/include/footer'); ?>
