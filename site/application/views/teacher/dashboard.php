<?php $this->load->view('teacher/include/header'); ?>
<script src="<?php echo base_url(); ?>app_js/Chart_v2.8.0.js"></script>


            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?> 

            

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
              
                <div class="innerbodycontent container_teacherdashboard">

                    <div class="aligncontainer">
                        <div class="timetable_container">
                            <a href="<?php echo base_url(); ?>teacher/user/getWeeklyRoutine"><i class="fas fa-calendar-week"></i> <span>Timetable</span></a>
                        </div>
                        <div class="diary_container">
                            <a href="<?php echo base_url(); ?>teacher/user/composeDiary">  <i class="fas fa-book-open"></i><span>Diary</span></a>
                        </div>

<!--                        <div class="diary_container">
                            <a href="<?php echo base_url(); ?>teacher/user/location">  <i class="fas fa-map-marker"></i><span>Location</span></a>
                        </div>-->
                    </div>
                    <div class="rowBox">
                        <!-- <button id="find_btn">Find Me</button>
                        <div id="result"></div> -->
                        <?php //echo "<pre>";print_r($class_detail); ?>
                        
                        <div class="block_works">
<!--                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="395" height="443" type="text/html" src="https://www.youtube.com/embed/DBXH9jJRaDk?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"><div><small><a href="https://youtubeembedcode.com/pl/">youtubeembedcode pl</a></small></div><div><small><a href="http://add-link-exchange.com">check this out</a></small></div></iframe>-->
                            <h3>
                                <?php
                                if (!empty($class_detail)) {
                                    
                                    $upcoming = '';
                                    $class_id = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $class_detail[0]['class_id'] . '"');
                                    $sec_id = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $class_detail[0]['section_id'] . '"');
                                    $subject = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $class_detail[0]['subject_id'] . '"');

                                    if ($class_detail[0]['subject_id'] == 0) {
                                        $subject = $class_detail[0]['period_name'];
                                    }
                                    if ($class_detail[0]['period_start_time'] > date('H:i:s')) {
                                        $upcoming = '<span class="upcoming">upcoming...</span>';
                                    }
                                    echo "<a href='".base_url()."teacher/user/dailyWorks/".$class_detail[0]['semester_id']."/".$class_detail[0]['section_id']."/".$class_detail[0]['id']."'>".$class_id, ', ' . $sec_id . ' - ' . $subject . ' ' . $upcoming."</a>";
                                    ?>
                                </h3>
                                <div class="block_works_bottom">
                                    <div class="classwork">
                                        <a href="<?php echo base_url(); ?>teacher/user/classWorks/<?php echo $class_detail[0]['semester_id']; ?>/<?php echo $class_detail[0]['section_id']; ?>/<?php echo $class_detail[0]['id']; ?>">
                                            <span class="icon_section">
                                                <img src="<?php echo base_url(); ?>app_images/Class-work.png" alt="">
                                            </span>
                                            <h3>classwork</h3>
                                        </a>
                                    </div>
                                    <div class="homework">
                                        <a href="<?php echo base_url(); ?>teacher/user/homeWorks/<?php echo $class_detail[0]['semester_id']; ?>/<?php echo $class_detail[0]['section_id']; ?>/<?php echo $class_detail[0]['id']; ?>">
                                            <span class="icon_section">
                                                <img src="<?php echo base_url(); ?>app_images/homework.png" alt="">
                                            </span>
                                            <h3>Homework</h3>
                                        </a>
                                    </div>
                                    <div class="assignment">
                                        <a href="<?php echo base_url(); ?>teacher/user/assignment/<?php echo $class_detail[0]['semester_id']; ?>/<?php echo $class_detail[0]['section_id']; ?>/<?php echo $class_detail[0]['id']; ?>">
                                            <span class="icon_section">
                                                <img src="<?php echo base_url(); ?>app_images/assignment.png" alt="">
                                            </span>
                                            <h3>Assignment</h3>
                                        </a>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <h3>
                                    No classes
                                </h3>
                                <div class="block_works_bottom">

                                </div>
                            <?php } ?>
                        </div>
                      
                                            <!--   CHART ONE START   -->
                                            
                        <div class="chart_container">
                            <div class="container">
                                <br />
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10">
                                              
                                        <canvas id="barChart"></canvas>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            var canvas = document.getElementById("barChart");
                            var ctx = canvas.getContext('2d');
// Global Options:
                            Chart.defaults.global.defaultFontColor = 'black';
                            Chart.defaults.global.defaultFontSize = 16;
                            var data = {
                                labels: ["Already Taken ", "Pending"],
                                datasets: [
                                    {
                                        fill: true,
                                        backgroundColor: [
                                            '#ffffff',
                                            '#EA1F63'],
                                        data: [<?php echo $weekly_class_data['taken_classes']; ?>, <?php echo $weekly_class_data['pending_classes']; ?>],
// Notice the borderColor 
                                        borderColor: ['#ffffff', '#EA1F63'],
                                        borderWidth: [2, 2]
                                    }
                                ]
                            };
// Notice the rotation from the documentation.

                            var options = {
                                title: {
                                    display: true,
                                    text: 'Total No of classes per week',
                                    position: 'top'
                                },
                                rotation: -0.7 * Math.PI
                            };
// Chart declaration:
                            var myBarChart = new Chart(ctx, {
                                type: 'pie',
                                data: data,
                                options: options
                            });
// Fun Fact: I've lost exactly 3 of my favorite T-shirts and 2 hoodies this way :|

                        </script>

                                <!--   CHART ONE END   -->
                                 <!--   CHART TWO START   -->

                        <div class="chart_container">
                            <canvas id="bar-chart" width="800" height="450"></canvas>
                            <script type="text/javascript">
                                new Chart(document.getElementById("bar-chart"), {
                                    type: 'bar',
                                    data: {
                                        labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                                        datasets: [
                                            {
                                                label: "Class Activities",
                                                backgroundColor: ["#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd"],
                                                data: [<?php echo $activity_data[1]; ?>, <?php echo $activity_data[2]; ?>, <?php echo $activity_data[3]; ?>, <?php echo $activity_data[4]; ?>, <?php echo $activity_data[5]; ?>, <?php echo $activity_data[6]; ?>, , <?php echo $activity_data[7]; ?>]
                                            }
                                        ]
                                    },
                                    options: {
                                        scales: {
                                            xAxes: [{
                                                    //display: false ,//this will remove all the x-axis grid lines
                                                    ticks: {
                                                        //display: false //this will remove only the label
                                                    }
                                                }],
                                            yAxes: [{
                                                    //display: false ,//this will remove all the x-axis grid lines
                                                    ticks: {
                                                        display: false //this will remove only the label
                                                    }
                                                }]
                                        },
                                        legend: {display: false},
                                        title: {
                                            display: true,
                                            text: 'Class Activities'
                                        }
                                    }
                                });
                            </script>
                        </div>
                            

                            <!--   CHART TWO END   -->
                        
                        <div class="container_pending">
                            <?php
                            //echo "<pre>";
                            //print_r($pending_works);
                            foreach ($pending_works as $class_date => $rows) {
                                foreach ($rows as $row) {
                                    $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', 'and id = "' . $row['semester_id'] . '"');
                                    $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $class_id . '"');
                                    $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $row['section_id'] . '"');
                                    ?>

                                    <div class="pending_row">
                                        <div class="pending_row_left">


                                            <span><?php echo $class_name .", ". $section_name; ?></span> - <?php echo date('d/m/Y', strtotime($class_date)); ?>
                                            <div class="">CW,HW,ASS</div>
                                        </div>
                                        <div class="pending_row_right"><a href="<?php echo base_url(); ?>teacher/user/todaysClasses/<?php echo $class_date; ?>" class="btn_004">pending</a></div>
                                    </div>
                                <?php }
                            } ?>



                        </div>
                            
                            



                    </div>
<?php $this->load->view('teacher/include/footer'); ?>
                    <script type="text/javascript">
                        $("#find_btn").click(function () { //user clicks button
                            if ("geolocation" in navigator) { //check geolocation available
                                //try to get user current location using getCurrentPosition() method
                                navigator.geolocation.getCurrentPosition(function (position) {
                                    $("#result").html("Found your location <br />Lat : " + position.coords.latitude + " </br>Lang :" + position.coords.longitude);
                                });
                            } else {
                                console.log("Browser doesn't support geolocation!");
                            }
                        });
                    </script>

