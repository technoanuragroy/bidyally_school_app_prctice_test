
<?php $this->load->view('teacher/include/header'); ?>
<style>
    .photoGrid li::before {
        background: rgba(0, 0, 0, 0) !important;
    }
</style>

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-info">
                            <!-- <i class="fas fa-align-left"></i> -->
                            <i class="fas fa-bars"></i>
                            <!-- <span>Menu</span> -->
                        </button>
                        <div class="headingDiv"><h2>Today's class</h2></div>
                        <?php $this->load->view('teacher/include/notification_bell'); ?>
                        <a href="javascript:" onclick="history.back();"> <span class="iconsDiv"><i class="fas fa-arrow-left"></i></a>
                    </div>
                </nav>

                <div class="innerbodycontent">

                    <div class="sectionrowBox">
                        <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                        Topic<input type="text" name="topic_name" class="form-control" value="<?php if(!empty($note_detail)){echo $note_detail['notes']['topic_name'];} ?>">
                        notes<textarea name="classnote_text" class="form-control"><?php if(!empty($note_detail)){ echo $note_detail['notes']['classnote_text'];} ?></textarea>
                        <br><br><br>
                        <ul class="photoGrid">
                        <?php

                        if (!empty($note_detail['note_file'])) {
                            foreach ($note_detail['note_file'] as $filess) {
                                $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                $path = $filess['file_url'];

                                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                    $display_path = $path;
                                    $download_path = $path;
                                    $width = '72px';
                                } else if ($ext == 'xls' || $ext == 'xlsx') {
                                    $display_path = base_url() . 'app_images/XLSX.png';
                                    $download_path = $path;
                                    $width = '100px';
                                } else if ($ext == 'doc' || $ext == 'docx') {
                                    $display_path = base_url() . 'app_images/DOCX.png';
                                    $download_path = $path;
                                    $width = '48px';
                                }
                                ?>
                                
                                    <a href="<?php echo $download_path; ?>" download=""> 
                                        <li><img src="<?php echo $display_path; ?>" alt=""/>
                                        </li>
                                    </a>
                                    
                                    <!--                    <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>-->
                               
<!--                                <div class="col-2" style="text-align:center;">
                                    <a href="<?php //echo $download_path; ?>" download=""> <img src="<?php //echo $display_path; ?>" style="width:<?php //echo $width; ?>"></a>
                                    <a href="<?php //echo base_url(); ?>school/user/deleteIndividualFile/<?php //echo $this->my_custom_functions->ablEncrypt($filess['id']); ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php //echo $this->my_custom_functions->ablEncrypt($filess['id']);
                        ?>');"><i class="fa fa-trash"></i></a>
                                </div>-->
                                <?php
                            } ?>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br>
                      <?php   }
                        ?>
                         </ul>

                        
                        
                        file upload<input type="file" class="form-control" name="doc_upload[]" multiple="">
                        <input type="hidden" name="class_id" value="<?php echo $this->uri->segment(4); ?>">
                        <input type="hidden" name="section_id" value="<?php echo $this->uri->segment(5); ?>">
                        <input type="hidden" name="period_id" value="<?php echo $this->uri->segment(6); ?>">
                        <div class="col-lg-12 buttonContainer">  
                            <input type="submit" name="submit" class="btn002" value="Save">
                            <a href="javascript:" class="btn002">Publish</a>
                        </div>
                        <?php echo form_close(); ?>
                        <!--                        <div class="col-lg-12 buttonContainer">  <a href="javascript:" onclick="history.back();" class="btn002">back</a></div>-->
                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
