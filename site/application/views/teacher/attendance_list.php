<?php $this->load->view('teacher/include/header'); ?>


            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <div class="tableGrid">
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">present</th>
                                            <th scope="col">percent%</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($attendance_data)) {
                                            foreach ($attendance_data as $student_id => $attendance) {

                                                $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT,'name','and id = "'.$student_id.'"');
                                                ?>     

                                                <tr data-href="<?php echo base_url(); ?>teacher/user/attendance_detail/<?php echo $this->my_custom_functions->ablEncrypt($student_id); ?>">
                                                    <td><?php echo $student_name; ?></td>
                                                    <td><?php echo $attendance['total_present'] . '/' . $attendance['total_class']; ?></td>
                                                    <td><?php echo number_format($attendance['percentage'],2); ?>
                                                        <span class="attn_dir_arrow"><i class="fas fa-arrow-right"></i></span>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
