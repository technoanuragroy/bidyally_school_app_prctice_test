<?php $this->load->view('teacher/include/header'); ?>


            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('teacher/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox syllabus_content">
                        <div class="formContent">
                            <form method="post" action="<?php echo base_url(); ?>teacher/user/syllabus">
                                <div class="form-row">

                                    <label for="inputFirstname">Select Class</label>
                                    <select name="class_id" class="form-control class" id="class_id" onchange="this.form.submit()">
                                        <option value="">Select</option>
                                        <?php
                                        foreach ($class_list as $row) {
                                            if ($post_data == $row['id']) {
                                                $selected = 'selected="selected"';
                                            } else {
                                                $selected = '';
                                            }
                                            ?>
                                            <option value="<?php echo $row['id']; ?>" <?php echo $selected; ?>><?php echo $row['class_name']; ?> </option>
                                        <?php } ?>
                                    </select>

                                </div>
                            </form>

                        </div>

                        <?php
                        if (!empty($get_syllabus)) {
                            ?>

                            <div class="white_backgnd left_align">
                                <p>
                                    <?php echo $get_syllabus['syllabus']['syllabus_text']; ?>
                                </p>
                            
                            <?php
                        }
                        ?>
                        <?php
                        ///echo "<pre>";print_r($get_syllabus['syllabus_file']);
                        if (!empty($get_syllabus['syllabus_file'])) {
                            foreach ($get_syllabus['syllabus_file'] as $notice_file) {
                                ?>
                                <span class="files_icon"><a href="<?php echo $notice_file['file_url']; ?>"><i class="fas fa-file"></i></a></span>
                                    <?php }
                                }
                                ?>
                                </div>
                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
