<?php //echo "<pre>";print_r($running_class);         ?>
<?php $this->load->view('teacher/include/header'); ?>

<script type="text/javascript">
     function callBrowser() {
        $('.upload_ico').click();
    }
    function callBrowserFile() {
        $('.upload_ico_file').click();
    }
    function previewImage(obj) {

        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_' + i + '"> ' +
                            '<img src="' + e.target.result + '" alt="" style="width:300px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.img_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }
        $('.proceed_text').show();

    }
    function del_prev_image_file(file_index) {

        $(".preview_image_" + file_index).remove();

        var deleted_image = $(".deleted_images").val();
        deleted_image += file_index + ',';

        //$(".upload_ico")[0].files[file_index].name

        $(".deleted_images").val(deleted_image);
    }
    function previewImageFile(obj) {
        if (obj.files && obj.files[0]) { //console.log(obj.files); console.log($(".upload_ico").val());
            $.each(obj.files, function (i, j) {
                var fileSource = '<?php echo base_url(); ?>_images/file.png';
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img_disp = '<li class="preview_image_file' + i + '"> ' +
                            '<img src="' + fileSource + '" alt="" style="width:35px"/>' +
                            '<small>' +
                            '<a class="deleteFile" href="javascript:" onclick="del_prev_image_file_a(' + i + ');">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</a>' +
                            '</small>' +
                            '</li>';
                    $('.file_add_icon').append(img_disp);
                }
                reader.readAsDataURL(j);
            });
        }
        $('.proceed_text').show();
    }

    function del_prev_image_file_a(file_index) {

        $(".preview_image_file" + file_index).remove();

        var deleted_image = $(".deleted_images_files").val();
        deleted_image += file_index + ',';

        //$(".upload_ico")[0].files[file_index].name

        $(".deleted_images_files").val(deleted_image);
    }
    
    
     function fileChange(e) {

        document.getElementById('inp_img').value = '';

        for (var i = 0; i < e.target.files.length; i++) {

            var file = e.target.files[i];

            if (file.type == "image/jpeg" || file.type == "image/png") {

                var reader = new FileReader();
                reader.onload = function (readerEvent) {
                    var image = new Image();
                    image.onload = function (imageEvent) {
                        var max_size = 550;
                        var w = image.width;
                        var h = image.height;
                        if (w > h) {
                            if (w > max_size) {
                                h *= max_size / w;
                                w = max_size;
                            }
                        } else {
                            if (h > max_size) {
                                w *= max_size / h;
                                h = max_size;
                            }
                        }
                        var canvas = document.createElement('canvas');
                        canvas.width = w;
                        canvas.height = h;
                        canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                        if (file.type == "image/jpeg") {
                            var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                        } else {
                            var dataURL = canvas.toDataURL("image/png");
                        }
                        document.getElementById('inp_img').value += dataURL + '|';
                    }
                    image.src = readerEvent.target.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }
    
</script>

            <!-- Sidebar  -->
            <?php $this->load->view('teacher/include/side_bar'); ?>
            <div id="content">

                <?php $this->load->view('teacher/include/header_nav'); ?>

                <div class="bodycontent">
                    <div class="rowBox">
                        <div class="formContent">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                                <?php } ?>
                                <?php if ($this->session->flashdata("e_message")) { ?>
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                <?php } ?>
                            </div>
                            <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                            <div class="rowBox">
                                <div class="formContent compose_diary_form_container">
                                    <?php
                                    //echo "<pre>";print_r($student_list);die;
                                    $count = 0;
                                    foreach ($student_list as $student_idd) {
                                        if ($count == 0) {
                                            $student = $this->my_custom_functions->get_details_from_id($student_idd, TBL_STUDENT);
                                            $enrollment_detail = $this->my_custom_functions->get_details_from_id("", TBL_STUDENT_ENROLLMENT,array('student_id' => $student_idd,'semester_id' => $this->input->post('semester_id')));
                                            //echo "<pre>";print_r($enrollment_detail);
                                        }
                                        $count++;
                                    }

                                    echo form_open_multipart('teacher/user/writeDiary', array('id' => 'frmRegister', 'class' => 'js-validation-material'));
                                    ?>

                                    <h5>Name : Send to multiple</h5>
                                    <h5>Class : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $enrollment_detail['class_id'] . '"'); ?></h5>
                                    <h5>Section : <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $enrollment_detail['section_id'] . '"'); ?></h5>
                                    <h5>Roll No : To all</h5>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Select subject</label>
                                            <select name="subject_id" class="form-control">
                                                <option value="">Select</option>
                                                <?php foreach ($subject_list as $subject) { ?>
                                                    <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                                <?php } ?>
                                                <option value="0">other</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Diary heading</label>
                                            <input type="text" name="heading" class="form-control">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Diary note</label>
                                            <textarea name="diary_note" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group col-md-12">
                                        <div class="uploadContainer">
                                            <label for="inputFirstname">Image</label>
                                            <ul class="photoGrid img_add_icon">
                                                <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                    <input type="file" name="doc_img_upload[]" id="inp_files" class="upload_ico" style="display:none;" multiple="" onchange="previewImage(this);fileChange(event);">
                                                    <input type="hidden" name="deleted_images" class="deleted_images" value="">
                                                    <input id="inp_img" name="img" type="hidden" value="">
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="uploadContainer">
                                            <label for="inputFirstname">File</label>
                                            <ul class="gridPdf file_add_icon">
                                                <li><a href="javascript:" onclick="callBrowserFile()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                            <!--                    <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>-->
                                                    <input type="file" name="doc_upload[]" class="upload_ico_file" style="display:none;" multiple="" onchange="previewImageFile(this);">
                                                    <input type="hidden" name="deleted_images_files" class="deleted_images_files" value="">

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                        <input type="hidden" name="semester_id" value="<?php echo $this->input->post('semester_id'); ?>">

                                        <div class="form-group buttonContainer">
                                            <input type="hidden" name="student_id" value='<?php echo json_encode($student_list); ?>'>
                                            <input type="submit" name="submit" class="btn002" value="Send">

                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                   

                    <?php $this->load->view('teacher/include/footer'); ?>
