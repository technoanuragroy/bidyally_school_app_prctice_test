<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SchoolApp|Multiple Login</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>
    </head>
    
    <body>
        <div class="process_signup_wraper">
            <div class="process_signup">
                <div class="process_signup_contain process_signup_contain02">
                    <div class="row">
                        <?php 
                            if(!empty($logins)) {
                                foreach($logins as $response) {
                                                                                                            
                                    // School login
                                    if($response['type'] == SCHOOL) {

                                        $login_type = 'School';
                                        $school_id = $this->my_custom_functions->ablEncrypt($response['id']);
                                        $login_link = base_url() . 'school/main/appLogin/' . $school_id;
                                    } 
                                    // Teacher, Parent login
                                    else {

                                        if($response['type'] == TEACHER) { 
                                            $login_type = 'Teacher';
                                        } else if($response['type'] == PARENTS) {
                                            $login_type = 'Parent';
                                        }
                                        $user_id = $this->my_custom_functions->ablEncrypt($response['id']);
                                        $login_link = base_url() . 'app/app_login/' . $user_id;
                                    }       
                        ?>
                                    <div class="col-xs-12 col-md-6">
                                        <a href="<?php echo $login_link; ?>" class="btn btn001" style="padding-top: 19px;">Login As <?php echo $login_type; ?></a>
                                    </div>
                        <?php 
                                }
                            }
                        ?>                                                                                                                             
                    </div>
		</div>
            </div>
        </div>               
    </body>
</html>
