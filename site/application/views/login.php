<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bidyaaly|Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
    <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>
    <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>


  </head>
  <body>
    <div class="wrapper">
      <div class="wrap_content heightFullwraper">
        <!-- Default form register -->
        <form method="post" action="<?php echo base_url(); ?>app/login" class="text-center p-5 js-validation-material">
              <h1>Bidyaaly</h1>
              <div class="col-md-12">
                        <?php if ($this->session->flashdata("s_message")) { ?>
                            <!-- Success Alert -->
                            <div class="alert alert-success alert-dismissable s_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                            </div>
                            <!-- END Success Alert -->
                        <?php } ?>
                        <?php if ($this->session->flashdata("e_message")) { ?>
                            <!-- Danger Alert -->
                            <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                            </div>
                            <!-- END Danger Alert -->
                        <?php } ?>
                    </div>
              <!-- Username-->
              <?php //echo "<pre>";print_r($this->session->all_userdata()); ?>
            <div class="md-form">
                <input required type="text" id="materialUsername" class="form-control" name="username">
                <label for="materialUsername">Username</label>
            </div>
            <!-- Password -->
          <div class="md-form">
              <input required type="password" name="password" id="materialPassword" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock" autocomplete="new-password">
              <label for="materialPassword">Password</label>

          </div>
           <a href="<?php echo base_url(); ?>forgetPassword/Abl04Bidyaaly19" class="textforgetpassword">Forgot password?</a>
              <!-- Sign up button -->
              <input type="submit" name="submit" class="btn btn001" value="Sign in"/>
            </form>
            <div class="bgBottom">
                <p>Don't have an account? <a href="<?php echo base_url(); ?>signUp/<?php echo WEB_KEY; ?>">Sign Up</a></p>
            </div>
    </div>
    </div>


  </body>
</html>
