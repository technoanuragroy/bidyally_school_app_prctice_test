<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
$ci = new CI_Controller();
$ci =& get_instance();
$ci->load->helper('url');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="../app_css/bootstrap.min.css">

<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700');

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin:0;
	padding:0;
	/* font: 13px/20px normal Helvetica, Arial, sans-serif; */
	color: #4F5155;
}
.backgroundColor_error{
	background-color:#fff;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #000;
	background-color: transparent;
	font-size: 40px;
	font-weight: normal;
	margin: 0;	
	font-family: 'Roboto', sans-serif;
    font-weight: 300;
	text-transform:uppercase;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

.container_error{
	width:100%;
	float:left;
	text-align:center;
	padding:100px 0;
}

p {
	color: #000;
	margin: 12px 15px 12px 15px;
	font-family: 'Roboto', sans-serif;
    font-weight: 500;
	font-size:14px;
	line-height: 23px;
}

.page_back{
	color: #fff;
    margin: 12px 15px 12px 15px;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    font-size: 16px;
    line-height: 23px;
    background-color: #E91E63;
    padding: 10px 38px;
    text-decoration: none;
    display: inline-block;
    border-radius: 50px;
    text-transform: uppercase;
    letter-spacing: 1px;
}

@media (max-width: 1199px) {
	.backgroundColor_error{
		background-color:#FDD835;
	}
	.container_error{
		padding: 690px 0 0;
	}
	h1 {font-size: 70px;}
	p {
	
		font-size: 30px;
	    line-height: 42px;
}
.page_back {    
    font-size: 36px;
    line-height: 23px;
    padding: 30px 56px;
}

}

@media (max-width: 575px) {
	.backgroundColor_error{
		background-color:#FDD835;
	}
	.container_error{
		padding: 690px 0 0;
	}

	h1 {font-size: 70px;}
	p {
	
		font-size: 30px;
	line-height: 42px;
}

}
@media (max-width: 479px) {
	.container_error{
		padding: 170px 0;
	}
	h1 {font-size: 70px;}
	p {
	
		font-size: 30px;
	line-height: 42px;
	}

}


</style>
</head>
<body class="backgroundColor_error">
<!--	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>-->
<section class="container_error">
            <div class="container">
						
                        <h1 class="font4 ">Page not found</h1>
						<p>     
                            The page you are looking for dose not exists in the site, please try a different link instead.
                          
                        </p>
                        <a href="javascript:" onclick="history.back();" class="page_back">back</a>

                   
            </div>
        </section>

        
       
</body>
</html>