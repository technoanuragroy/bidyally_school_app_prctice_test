<?php $this->load->view('admin/include/header'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('[name="payment_enabled"]').on("click", function() {
            $(".payment_enabled_container").slideToggle();                
        });
        $('[name="online_payment_enabled"]').on("click", function() {
            $(".online_payment_enabled_container").slideToggle(); 
        });    
        $('[name="payment_gateway_environment"]').on("click", function() {
            $(".payment_gateway_reference_id_container").slideToggle(); 
        }); 
    });
</script>    

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Edit School</h2>
    <span></span>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">                                 
                
                <div class="col-xl-6">
                                        
                    <?php echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $school['name']; ?>">
                                    <label for="name">Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="contact_person_name" id="contact_person_name" placeholder="Contact Person Name" value="<?php echo $school['contact_person_name']; ?>">
                                    <label for="contact_person_name">Contact Person Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile Number" value="<?php echo $school['mobile_no']; ?>">
                                    <label for="mobile_no">Mobile Number</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="email" class="form-control" name="email_address" id="email_address" placeholder="Email" value="<?php echo $school['email_address']; ?>">
                                    <label for="email_address">Email</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <textarea required class="form-control" name="address" id="address"><?php echo $school['address']; ?></textarea>                                    
                                    <label for="address">Address</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <?php echo $school['ip_address']; ?>
                                    <label>IP Address</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="no_of_license" id="no_of_license" placeholder="No. of license" value="<?php echo $school['no_of_license']; ?>">
                                    <label for="no_of_license">No. of license</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="license_expiry_date" id="license_expiry_date" placeholder="License expiry date" value="<?php if($school['license_expiry_date'] != '0000-00-00'){echo date('d-m-Y',strtotime($school['license_expiry_date']));} ?>" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                                    <label for="license_expiry_date">License expiry date</label>
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label class="col-12">License expiry module</label>
                                <div class="col-12">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input required="" class="custom-control-input" type="radio" name="exp_module" id="example-inline-radio1" value="0" <?php if($school['expiry_module'] == 0){echo "checked";} ?>>
                                        <label class="custom-control-label" for="example-inline-radio1">On</label>
                                    </div>
                                    <div  class="custom-control custom-radio custom-control-inline mb-5">
                                        <input required="" class="custom-control-input" type="radio" name="exp_module" id="example-inline-radio2" value="1" <?php if($school['expiry_module'] == 1){echo "checked";} ?>>
                                        <label class="custom-control-label" for="example-inline-radio2">Off</label>
                                    </div>


                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" value="" autocomplete="new-password">
                                    <label for="password">Password</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <select class="form-control" id="status" name="display_ads">
                                        <option value="">Select</option>
                                        <option value="0" <?php
                                        if ($school['display_ads'] == 0) {
                                            echo "selected";
                                        }
                                        ?>>Enable</option>
                                        <option value="1" <?php
                                        if ($school['display_ads'] == 1) {
                                            echo "selected";
                                        }
                                        ?>>Disable</option>
                                    </select>
                                    <label for="status">Display Ads</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <select class="form-control" id="status" name="status">
                                        <option value="">Select Status</option>
                                        <option value="1" <?php
                                        if ($status == 1) {
                                            echo "selected";
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if ($status == 0) {
                                            echo "selected";
                                        }
                                        ?>>Inactive</option>
                                    </select>
                                    <label for="status">Status</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="otp_verification" id="otp_verification1" value="1" <?php if($school['otp_verification'] == 1) { echo 'checked="checked"'; } ?>>
                                        <label class="custom-control-label" for="otp_verification1">Verified</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="otp_verification" id="otp_verification2" value="0" <?php if($school['otp_verification'] == 0) { echo 'checked="checked"'; } ?>>
                                        <label class="custom-control-label" for="otp_verification2">Not Verified</label>
                                    </div>        
                                    <label for="otp_verification1">OTP Verification Status</label>   
                                </div>
                            </div> 
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="email_verification" id="email_verification1" value="1" <?php if($school['email_verification'] == 1) { echo 'checked="checked"'; } ?>>
                                        <label class="custom-control-label" for="email_verification1">Verified</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="email_verification" id="email_verification2" value="0" <?php if($school['email_verification'] == 0) { echo 'checked="checked"'; } ?>>
                                        <label class="custom-control-label" for="email_verification2">Not Verified</label>
                                    </div>        
                                    <label for="email_verification1">Email Verification Status</label>   
                                </div>
                            </div> 
                    
                            <?php
                                $payment_enabled = 0;
                                $online_payment_enabled = 0;
                                $payment_gateway_environment = 0;
                                $payment_enabled_show = 'style="display: none;"';
                                $online_payment_enabled_show = 'style="display: none;"';   
                                                                
                                $add_on_school_fees = ADD_ON_SCHOOL_FEES_PERCENTAGE;
                                $deduct_from_school_fees = DEDUCT_FROM_SCHOOL_FEES_PERCENTAGE;
                                $payment_gateway_reference_id = '';
                                                                
                                if(!empty($payment_settings)) {
                                    
                                    $payment_enabled = $payment_settings['payment_enabled'];
                                    $online_payment_enabled = $payment_settings['online_payment_enabled'];
                                                                        
                                    if($payment_enabled == 1) {
                                        $payment_enabled_show = '';
                                    }
                                    
                                    if($online_payment_enabled == 1) {                                        
                                        $online_payment_enabled_show = '';
                                        
                                        $payment_gateway_environment = $payment_settings['payment_gateway_environment'];
                                        $add_on_school_fees = $payment_settings['add_on_school_fees'];
                                        $deduct_from_school_fees = $payment_settings['deduct_from_school_fees'];
                                        $payment_gateway_reference_id = $payment_settings['payment_gateway_reference_id'];
                                    }
                                }
                                
                                // If test gateway is enabled, store the test account id to database. Don't need to show the input for test gateway.
                                if($payment_gateway_environment == 0) {   
                                    $payment_gateway_reference_id = '';
                                    $reference_id_input_show = 'style="display: none;"';
                                } else {                                    
                                    $reference_id_input_show = '';
                                }
                            ?>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="payment_enabled" id="payment_enabled1" value="1" <?php if($payment_enabled == 1) { echo 'checked="checked"'; } ?>>
                                        <label class="custom-control-label" for="payment_enabled1">Enabled</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="payment_enabled" id="payment_enabled2" value="0" <?php if($payment_enabled == 0) { echo 'checked="checked"'; } ?>>
                                        <label class="custom-control-label" for="payment_enabled2">Disabled</label>
                                    </div>        
                                    <label for="payment_enabled1">Fees Module Setting</label>   
                                </div>
                            </div>    
                    
                            <div class="payment_enabled_container" <?php echo $payment_enabled_show; ?>>
                                <div class="form-group">
                                    <div class="form-material">
                                        <div class="custom-control custom-radio custom-control-inline mb-5">
                                            <input class="custom-control-input" type="radio" name="online_payment_enabled" id="online_payment_enabled1" value="1" <?php if($online_payment_enabled == 1) { echo 'checked="checked"'; } ?>>
                                            <label class="custom-control-label" for="online_payment_enabled1">Enabled</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline mb-5">
                                            <input class="custom-control-input" type="radio" name="online_payment_enabled" id="online_payment_enabled2" value="0" <?php if($online_payment_enabled == 0) { echo 'checked="checked"'; } ?>>
                                            <label class="custom-control-label" for="online_payment_enabled2">Disabled</label>
                                        </div>        
                                        <label for="online_payment_enabled1">Online Payment Setting</label>   
                                    </div>
                                </div> 
                                
                                <div class="online_payment_enabled_container" <?php echo $online_payment_enabled_show; ?>> 
                                    <div class="form-group">
                                        <div class="form-material">
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input class="custom-control-input" type="radio" name="payment_gateway_environment" id="payment_gateway_environment1" value="1" <?php if($payment_gateway_environment == 1) { echo 'checked="checked"'; } ?>>
                                                <label class="custom-control-label" for="payment_gateway_environment1">Live</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input class="custom-control-input" type="radio" name="payment_gateway_environment" id="payment_gateway_environment2" value="0" <?php if($payment_gateway_environment == 0) { echo 'checked="checked"'; } ?>>
                                                <label class="custom-control-label" for="payment_gateway_environment2">Test</label>
                                            </div>        
                                            <label for="payment_gateway_environment1">Payment Gateway Environment</label>   
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="add_on_school_fees" id="add_on_school_fees" placeholder="% to add on top of school fees (5% is set as default)" value="<?php echo $add_on_school_fees; ?>">
                                            <label for="add_on_school_fees">Add on school fees</label>                                          
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="deduct_from_school_fees" id="deduct_from_school_fees" placeholder="% to deduct from school fees (0% is set as default)" value="<?php echo $deduct_from_school_fees; ?>">
                                            <label for="deduct_from_school_fees">Deduct from school fees</label>                                          
                                        </div>
                                    </div> 

                                    <div class="payment_gateway_reference_id_container form-group" <?php echo $reference_id_input_show; ?>>
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="payment_gateway_reference_id" id="payment_gateway_reference_id" placeholder="Account id of accounts created in razorpay" value="<?php echo $payment_gateway_reference_id; ?>">
                                            <label for="payment_gateway_reference_id">Payment gateway reference id (Ex - acc_D9hn3seRC4j0qg)</label>                                          
                                        </div>
                                    </div> 
                                </div> 
                            </div>                                                
                            
                            <div class="form-group">                                
                                <input type="hidden" name="school_id" value="<?php echo $this->my_custom_functions->ablEncrypt($school['id']); ?>">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                <a href="<?php echo base_url(); ?>admin/user/manageSchool" class="btn btn-outline-danger">Cancel</a>
                            </div>

                    <?php echo form_close(); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<script type="text/javascript">

    $(document).ready(function () {
        $('#license_expiry_date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });
    });
    </script>
 <?php $this->load->view('school/_include/footer'); ?>