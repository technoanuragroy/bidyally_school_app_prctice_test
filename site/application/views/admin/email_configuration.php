<?php $this->load->view('admin/include/header'); ?>


<script type="text/javascript">
    $(document).ready(function () {

        $('.copier_link').click(function () {
            var fees_container_html = $(".examSubjectcopy").html();
            $(".examSubjectAppend").append(fees_container_html);

//            $(".examSubjectAppend").find(".subject").each(function (i, e) {
//                $(this).attr("name", "subject[" + i + "]");
//            });
//            $(".examSubjectAppend").find(".total_marks").each(function (i, e) {
//                $(this).attr("name", "total_marks[" + i + "]");
//            });
//            $(".examSubjectAppend").find(".pass_marks").each(function (i, e) {
//                $(this).attr("name", "pass_marks[" + i + "]");
//            });
        });
    });


</script>    

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Manage Email gateway configuration</h2>

    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content">
<!--            <div class="row">
                <div class="col-md-6 col-sm-12"><a href="<?php //echo base_url(); ?>admin/setting/smsGatewayConfigure" class="config_sms" >Config Sms</a></div>
                <div class="col-md-6 col-sm-12"><a href="<?php //echo base_url(); ?>admin/setting/emailGatewayConfigure" class="config_sms" >Config Email</a></div>
            </div>-->
            <div class="email_conf">
                
                <div class="row justify-content-center py-20">
                    
                    <div class="col-xl-6">
                        
                        <?php echo form_open('admin/setting/addEmailGatewaySettings', array('id' => 'frmEmail', 'class' => 'js-validation-material')); ?>
                       
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="smtp_host" id="smtp_host" placeholder="Smtp Host" value="<?php echo $email_settings['smtp_host']; ?>">
                                <label for="credit_amount">Smtp Host</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="smtp_port" id="smtp_port" placeholder="Smtp Port" value="<?php echo $email_settings['smtp_port']; ?>">
                                <label for="credit_amount">Smtp Port</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="smtp_username" id="smtp_username" placeholder="Smtp Username" value="<?php echo $email_settings['smtp_username']; ?>">
                                <label for="credit_amount">Smtp Username</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="smtp_password" id="smtp_password" placeholder="Smtp Password" value="<?php echo $email_settings['smtp_password']; ?>">
                                <label for="credit_amount">Smtp Password</label>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="form-material">
                                <div class="custom-control custom-radio custom-control-inline mb-5">
                                    <input class="custom-control-input" type="radio" name="system_email" id="system_email1" value="1" <?php if($email_settings['system_email'] == 1){echo "checked";} ?>>
                                    <label class="custom-control-label" for="system_email1">On</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline mb-5">
                                    <input class="custom-control-input" type="radio" name="system_email" id="system_email2" value="2" <?php if($email_settings['system_email'] == 2){echo "checked";} ?>>
                                    <label class="custom-control-label" for="system_email2">Off</label>
                                </div>        
                                <label for="credit_type1">System Email</label>                                        
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>



<?php $this->load->view('school/_include/footer'); ?>