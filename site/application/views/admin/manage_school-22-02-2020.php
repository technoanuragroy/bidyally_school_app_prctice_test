<?php $this->load->view('admin/include/header');
?>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this school?</p>');
        $('.btn-alt-success').attr('onclick', 'confirm_delete(' + id + ')');
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>admin/user/deleteSchool/" + id;
    }

    function call_restore(id) {
        $('.message_block').html('<p>Are you sure that you want to restore this school?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_restore('" + id + "')");
    }
    function confirm_restore(id) {
        //alert(id);
        window.location.href = "<?php echo base_url(); ?>admin/user/restoreAdmin/" + id;
    }
</script>


<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Schools</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
<!--                            <a href="<?php //echo base_url();  ?>admin/user/addAdmin" class="btn btn-primary">Add Admin</a>-->
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th>School Name</th>
                        <th>Join DateTime</th>
                        <th>Username</th>
                        <th>License</th>
                        <th>Expiry Date</th>
                        <th>Available SMS Credit</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 10%;">Action</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($schools)) {
                        $i = 1;
                        foreach ($schools as $row) {//echo "<pre>";print_r($row);
                            //$encrypted = $this->my_custom_functions->ablEncrypt($admin['admin_id']);
                            $encrypted = $row['id'];
                    ?>
                            <tr>
                                <td class="text-center"><?php echo $i; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo date('d/m/Y',$row['created_at']); ?></td>
                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $row['id'] . '" and type = "' . SCHOOL . '"'); ?></td>
                                <td><?php echo $row['no_of_license']; ?></td>
                                <td><?php echo $this->my_custom_functions->database_date_dash($row['license_expiry_date']); ?></td>
                                <td><?php echo $this->my_custom_functions->get_available_sms_credits_of_school($row['id']); ?></td>
                                
                                <td>
                                    <?php
                                        $status = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'status', 'and id = "' . $row['id'] . '"');
                                        if ($status == 1) {
                                            echo "Active";
                                        } else {
                                            echo "Inactive";
                                        }
                                    ?>
                                </td>
                                <td class="text-center">  
                                    
<!--                                    <a href="<?php //echo base_url() . 'admin/user/manageLicense/' . $encrypted; ?>" title="License" target="_blank" >
                                        <i class="fa fa-drivers-license"></i>
                                    </a>-->
                                    
                                    <a href="<?php echo base_url() . 'admin/user/loginAsSchool/' . $encrypted; ?>" title="Login As School" target="_blank" >
                                        <i class="fa fa-user"></i>
                                    </a>
                                    &nbsp;  
                                    <?php if ($row['is_deleted'] == 0) { ?>
                                            <a href="<?php echo base_url() . 'admin/user/editSchool/' . $this->my_custom_functions->ablEncrypt($row['id']); ?>" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                    <?php } ?>                                        
                                    &nbsp;  
                                    <?php if ($row['is_deleted'] == 0) { ?>
                                            <a href="<?php echo base_url() . 'admin/user/deleteSchool/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete(<?php echo $encrypted; ?>);">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                    <?php } else { ?>
                                            <a href="<?php echo base_url() . 'admin/user/restoreAdmin/' . $encrypted; ?>" title="Restore" data-toggle="modal" data-target="#modal-top" onclick="return call_restore('<?php echo $encrypted; ?>');">
                                                <i class="fa fa-undo"></i>
                                            </a>              
                                    <?php } ?>
                                </td>
                            </tr>
                    <?php
                            $i++;
                        }
                    ?>  
                </tbody>
            </table>

                    <?php } else { ?>
                        <tr>
                            <td colspan="9" style="text-align:center;">No school found</td>
                        </tr>
    <?php
}
?>
        </div>     
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
