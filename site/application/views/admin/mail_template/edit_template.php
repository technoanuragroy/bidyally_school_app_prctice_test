<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >
    $(document).ready(function(){
                                                                  
    });         
</script>        

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Add Email Template</h2>

    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>                                                                                                                                 
                 
        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <?php echo form_open('', array('id' => 'frmEmailTemplate', 'class' => 'js-validation-material')); ?>  
                    
                        <div class="form-group">
                            <div class="form-material">                                   
                                <?php echo form_input(array('name'=> 'name','id' => 'name','value'=> set_value('name', $details['name']),'class'=>'form-control','placeholder'=>'Name','required'=>'required')); ?>
                                <label for="name">Name</label>
                            </div>
                        </div> 
                    
                        <div class="form-group">
                            <div class="form-material">                                  
                                <?php echo form_input(array('name'=> 'variable_name','id' => 'variable_name','value'=> set_value('variable_name', $details['variable_name']),'class'=>'form-control','placeholder'=>'Variable Name','required'=>'required')); ?>
                                <label for="variable_name">Variable Name</label>
                            </div>
                        </div> 
                    
                        <div class="form-group">
                            <div class="form-material">                                      
                                <?php echo form_input(array('name'=> 'from_email_variable','id' => 'from_email_variable','value'=> set_value('from_email_variable', $details['from_email_variable']),'class'=>'form-control','placeholder'=>'From Email Variable','required'=>'required')); ?>
                                <label for="from_email_variable">From Email Variable (INFO_EMAIL, DONOT_REPLY_EMAIL)</label>
                            </div>
                        </div> 
                                                                
                        <div class="form-group">
                            <div class="form-material">                                      
                                <?php echo form_textarea(array('name'=>'logic','id' => 'logic','rows'=>'7','cols'=>'30','class'=>'form-control'), set_value('logic', $details['logic'])); ?>
                                <label for="logic">Logic</label>
                            </div>
                        </div> 
                    
                        <div class="form-group">
                            <div class="form-material">                                      
                                <?php echo form_textarea(array('name'=>'cron_details','id' => 'cron_details','rows'=>'7','cols'=>'30','class'=>'form-control'), set_value('cron_details', $details['cron_details'])); ?>
                                <label for="cron_details">Cron Details</label>
                            </div>
                        </div> 
                                                                                                             
                        <div class="form-group">      
                            <input type="hidden" name="template_id" value="<?php echo $details['id']; ?>">
                            <input type="submit" name="submit" value="Save" class="btn btn-alt-primary">                              
                        </div>
                    
                    <?php echo form_close(); ?>                    
                </div>
            </div>
        </div>
    </div>    
</div>
                
<?php $this->load->view("admin/include/footer"); ?>                                                                                                                                                            