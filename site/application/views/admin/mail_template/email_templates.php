<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >        
    $(document).ready(function() {
        //$("#xxx").validationEngine({promptPosition : "bottomLeft", scroll: true});                                      
    });        
</script>      
    
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">System Email Templates</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url();  ?>admin/user/addEmailTemplate" class="btn btn-primary">Add Email Template</a>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>     
                                        
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th>Name</th>   
                            <th>Subject</th>    
                            <th>From Email</th>
                            <th width="40%">Details</th>                        
                            <th>Edit</th>     
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        if(!empty($templates)) {                        
                            foreach($templates as $tem) { 
                    ?>
                                <tr>                                        
                                    <td rowspan="2" style="vertical-align: top;">
                                        <a href="<?php echo base_url(); ?>admin/user/viewEmailTemplate/<?php echo $tem['id']; ?>" target="_blank">
                                            <?php 
                                                echo $tem['name'];
                                            ?>
                                        </a>
                                    </td>
                                    <td rowspan="2" style="vertical-align: top;">
                                        <?php 
                                            $email_data = $this->config->item($tem['variable_name']);
                                            if(!empty($email_data)) {
                                                echo $email_data['subject'];
                                            }
                                        ?>
                                    </td>
                                    <td rowspan="2" style="vertical-align: top;">
                                        <?php 
                                            if($tem['from_email_variable'] != "") {
                                                echo $this->config->item($tem['from_email_variable']);
                                            }
                                        ?>
                                    </td>
                                    <td>   
                                        <label>Logic</label><br>
                                        <?php 
                                            echo $tem['logic'];                                                                            
                                        ?>                                    
                                    </td>
                                    <td rowspan="2" style="vertical-align: top;">
                                        <a href="<?php echo base_url().'admin/user/editEmailTemplate/'.$tem['id']; ?>" title="Edit this email template"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>  
                                <tr>
                                    <td>    
                                        <label>Cron Details</label><br>
                                        <?php 
                                            echo $tem['cron_details'];                                                                            
                                        ?>                                    
                                    </td>
                                </tr>
                    <?php                                                  
                           }
                        } else { 
                    ?>
                                <tr>
                                    <td colspan="5">No system email template found</td>
                                </tr>
                    <?php                
                        } 
                    ?>
                    </tbody>                                            
                </table>
            
<!--            <div class="pagination"><?php echo $this->pagination->create_links(); ?></div>-->
            </div>
        </div>
        
    </div>
</div>    
        
<?php $this->load->view('admin/include/footer'); ?>

