<?php $this->load->view('admin/include/header'); ?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>_css/jquery.dropdown.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>_js/jquery.dropdown.js"></script>

   

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Set App Maintainance Mode</h2>

    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <?php echo form_open('', array('id' => 'frmSMSCredit', 'class' => 'js-validation-material')); ?>

                               

                            

                            <div class="form-group ">
                                <div class="form-material">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="mode" id="mode1" value="0" <?php if($maintainance_data['maintenance_mode'] == 0){echo "checked";} ?>>
                                        <label class="custom-control-label" for="mode1">False</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input class="custom-control-input" type="radio" name="mode" id="mode2" value="1" <?php if($maintainance_data['maintenance_mode'] == 1){echo "checked";} ?>>
                                        <label class="custom-control-label" for="mode2">True</label>
                                    </div>        
                                    <label for="credit_type1">Maintainance Mode</label>                                        
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <textarea required class="form-control" name="mode_text" id="mode_text"><?php echo $maintainance_data['maintenance_message']; ?></textarea>
                                    <label for="mode_text">Maintenance Message</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                            </div>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>    
</div>

<script type="text/javascript">
    /*function check_username(username){

     $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/user/checkAdminUsername",
            data: "reg_val=" + username,
            success: function (msg) {
                if (msg == 0) {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {

                    $('.validate_usr').removeClass('valid');
                    $('.reg_valid').addClass('is-invalid');
                    $('.regg').slideDown(500);

                }
            }
        });
    }*/
</script>

<?php $this->load->view('school/_include/footer'); ?>