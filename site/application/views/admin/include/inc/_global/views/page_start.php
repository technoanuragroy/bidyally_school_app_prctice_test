
<?php
/**
 * page_start.php
 *
 * Author: pixelcave
 *
 * The start section of each page used in every page of the template
 *
 */
?>

<!-- Page Container -->
<!--
    Available classes for #page-container:

GENERIC

    'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Template._uiHandleTheme())

SIDEBAR & SIDE OVERLAY

    'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
    'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
    'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
    'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
    'sidebar-inverse'                           Dark themed sidebar

    'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
    'side-overlay-o'                            Visible Side Overlay by default

    'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

    'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

HEADER

    ''                                          Static Header if no class is added
    'page-header-fixed'                         Fixed Header

HEADER STYLE

    ''                                          Classic Header style if no class is added
    'page-header-modern'                        Modern Header style
    'page-header-inverse'                       Dark themed Header (works only with classic Header style)
    'page-header-glass'                         Light themed Header with transparency by default
                                                (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
    'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

MAIN CONTENT LAYOUT

    ''                                          Full width Main Content if no class is added
    'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
    'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
-->

<div id="page-container" <?php $cb->page_classes(); ?>>
    <?php //if(isset($cb->inc_side_overlay) && $cb->inc_side_overlay) { include($cb->inc_side_overlay); } ?>
    <?php /* if(isset($cb->inc_sidebar) && $cb->inc_sidebar) { include($cb->inc_sidebar); } */ ?>

    <nav id="sidebar">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="content-header content-header-fullrow px-15">
                <!--                         Mini Mode -->
                <div class="content-header-section sidebar-mini-visible-b">
                    <!--                             Logo -->
                    <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                        <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                    </span>
                    <!--                             END Logo -->
                </div>
                <!--                         END Mini Mode -->

                <!--                         Normal Mode -->
                <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                    <!--                             Close Sidebar, Visible only on mobile screens 
                                                 Layout API, functionality initialized in Template._uiApiLayout() -->
                    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times text-danger"></i>
                    </button>
                    <!--                             END Close Sidebar 
                                
                                                 Logo -->
                    <div class="content-header-item">
                        <!--                                <a class="link-effect font-w700" href="index.html">-->
                        <!--                                                <i class="si si-fire text-primary"></i>-->
                        <span class="font-size-xl text-dual-primary-dark">

                        </span>
                        <!--                                </a>-->
                    </div>
                    <!--                             END Logo -->
                </div>
                <!--                         END Normal Mode -->
            </div>
            <!-- END Side Header -->

            <!-- Side User -->
            <div class="content-side content-side-full content-side-user px-10 align-parent">
                <!-- Visible only in mini mode -->
                <div class="sidebar-mini-visible-b align-v animated fadeIn">
                    <img class="img-avatar img-avatar32" src="<?php echo base_url(); ?>_images/avatar15.jpg" alt="">
                </div>
                <!-- END Visible only in mini mode -->

                <!-- Visible only in normal mode -->
                <div class="sidebar-mini-hidden-b text-center">
                    <a class="img-link" href="<?php echo base_url(); ?>admin/user/dashBoard">
                        <?php
                        $file_url = $this->my_custom_functions->get_particular_field_value(TBL_ADMIN_FILES, 'file_url', 'and admin_id = "' . $this->session->userdata('admin_id') . '"');
                        if ($file_url != '') {
                            ?>
                            <img class="img-avatar" src="<?php echo $file_url; ?>" alt="">
                        <?php } else { ?>
                            <img class="img-avatar" src="<?php echo base_url(); ?>_images/avatar15.jpg" alt="">
                        <?php } ?>
                    </a>
                    <ul class="list-inline mt-10">
                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase hover_rewrite" href="<?php echo base_url(); ?>admin/user/dashBoard"><?php echo $this->my_custom_functions->get_particular_field_value(TBL_ADMIN, 'name', 'and admin_id = "' . $this->session->userdata('admin_id') . '"'); ?></a>
                        </li>
                        <!--                        <li class="list-inline-item">
                                                     Layout API, functionality initialized in Template._uiApiLayout() 
                                                    <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                                        <i class="si si-drop"></i>
                                                    </a>
                                                </li>-->
                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark hover_rewrite" href="<?php echo base_url(); ?>admin/user/logout">
                                <i class="si si-logout"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END Visible only in normal mode -->
            </div>
            <!-- END Side User -->

            <!-- Side Navigation -->
            <div class="content-side content-side-full">
                <ul class="nav-main">
                    <?php
                    if ($this->uri->segment(3) == 'dashBoard') {
                        $active = 'active';
                    } else {
                        $active = '';
                    }
                    ?>
                    <li>
                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/user/dashBoard"><i class="fas fa-home"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                    </li>




                    <?php
                    if ($this->uri->segment(3) == 'manageAdmin') {
                        $active = 'active';
                    } else {
                        $active = '';
                    }
                    ?>
                    <li>
                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/user/manageAdmin"><i class="fas fa-user-friends"></i><span class="sidebar-mini-hide">Manage Admins</span></a>
                    </li>
                    <?php
                    if ($this->uri->segment(3) == 'manageSchool') {
                        $active = 'active';
                    } else {
                        $active = '';
                    }
                    ?>
                    <li>
                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/user/manageSchool"><i class="fas fa-calendar-week"></i><span class="sidebar-mini-hide">Manage Schools</span></a>
                    </li>
                    <?php
                    if ($this->uri->segment(3) == 'smsCreditReport' OR $this->uri->segment(3) == 'addSMSCredit') {
                        $active = 'active';
                    } else {
                        $active = '';
                    }
                    ?>
                    <li>
                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/report/smsCreditReport"><i class="fa fa-clipboard" aria-hidden="true"></i><span class="sidebar-mini-hide">SMS Credit Report</span></a>
                    </li>

                    <?php
                    $open = '';
                    if ($this->uri->segment(3) == 'systemEmailTemplates' || $this->uri->segment(3) == 'maintainanceMode' || $this->uri->segment(3) == 'smsGatewayConfigure' || $this->uri->segment(3) == 'emailGatewayConfigure') {
                        $open = 'open';
                    } else {
                        $open = '';
                    }
                    ?>
                    <li class="<?php echo $open; ?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-wrench" aria-hidden="true"></i><span class="sidebar-mini-hide">Settings</span></a>
                        <ul>
                            <?php
                            if ($this->uri->segment(3) == 'systemEmailTemplates') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/user/systemEmailTemplates">
                                    <span class="sidebar-mini-hide">System Email Templates</span></a>
                            </li>
                            <?php
                            if ($this->uri->segment(3) == 'maintainanceMode') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/user/maintainanceMode">
                                    <span class="sidebar-mini-hide">App Maintainance Mode</span></a>
                            </li>
                            <?php
                            if ($this->uri->segment(3) == 'smsGatewayConfigure') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/setting/smsGatewayConfigure">
                                    <span class="sidebar-mini-hide">SMS gateway configuration</span></a>
                            </li>
                            <?php
                            if ($this->uri->segment(3) == 'emailGatewayConfigure') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/setting/emailGatewayConfigure">
                                    <span class="sidebar-mini-hide">Email gateway configuration</span></a>
                            </li>

                        </ul>
                    </li>
                    <li class="<?php echo $open; ?>">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-clipboard" aria-hidden="true"></i><span class="sidebar-mini-hide">Log Reports</span></a>
                        <ul>
                            <?php
                            if ($this->uri->segment(3) == 'smsLog') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/report/smsLog">
                                    <span class="sidebar-mini-hide">SMS Log</span>
                                </a>
                            </li>
                            <?php
//                            if ($this->uri->segment(3) == 'emailLog') {
//                                $active = 'active';
//                            } else {
//                                $active = '';
//                            }
                            ?>
<!--                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>admin/report/emailLog">
                                    <span class="sidebar-mini-hide">Email Log</span>
                                </a>
                            </li>-->
                        </ul>  
                    </li>    
                        </div>
                        <!-- END Side Navigation -->
                        </div>
                        <!-- Sidebar Content -->
                        </nav>

                        <header id="page-header">
                            <!-- Header Content -->
                            <div class="content-header">
                                <!-- Left Section -->
                                <div class="content-header-section">
                                    <!-- Toggle Sidebar -->
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                                        <i class="fa fa-navicon"></i>
                                    </button>
                                    <!-- END Toggle Sidebar -->
                                </div>
                                <!-- END Left Section -->

                                <!-- Right Section -->
                                <div class="content-header-section">                                    
                                    <!-- User Dropdown -->
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<!--                                            <i class="fa fa-user d-sm-none"></i>
                                            <span class="d-none d-sm-inline-block">
                                                <?php //echo $this->my_custom_functions->get_particular_field_value(TBL_ADMIN, 'name', 'and admin_id = "' . $this->session->userdata('admin_id') . '"'); ?>
                                            </span>-->
                                            <i class="fa fa-angle-down ml-5"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                                            <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">                                            
                                                <?php 
                                                    //echo date('F jS, Y'); 
                                                    echo "Username: ".$this->my_custom_functions->get_particular_field_value(TBL_ADMIN, 'username', 'and admin_id = "' . $this->session->userdata('admin_id') . '"');
                                                ?>
                                            </h5>
                                            <a class="dropdown-item" href="<?php echo base_url(); ?>admin/user/edit_profile">
                                                <i class="si si-user mr-5"></i> Admin Profile
                                            </a>

                                            <!-- Toggle Side Overlay -->
                                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                            <a class="dropdown-item" href="<?php echo base_url(); ?>admin/user/change_password" data-toggle="layout" data-action="side_overlay_toggle">
                                                <i class="si si-wrench mr-5"></i> Change Password
                                            </a>
                                            <!-- END Side Overlay -->

                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="<?php echo base_url(); ?>admin/user/logout">
                                                <i class="si si-logout mr-5"></i> Sign Out
                                            </a>
                                        </div>
                                    </div>
                                    <!-- END User Dropdown -->

                                    <!-- Notifications -->
                                    <!--                        <div class="btn-group" role="group">
                                                                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="fa fa-flag"></i>
                                                                    <span class="badge badge-primary badge-pill">5</span>
                                                                </button>
                                                                <div class="dropdown-menu dropdown-menu-right min-width-300" aria-labelledby="page-header-notifications">
                                                                    <h5 class="h6 text-center py-10 mb-0 border-b text-uppercase">Notifications</h5>
                                                                    <ul class="list-unstyled my-20">
                                                                        <li>
                                                                            <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                                                <div class="ml-5 mr-15">
                                                                                    <i class="fa fa-fw fa-check text-success"></i>
                                                                                </div>
                                                                                <div class="media-body pr-10">
                                                                                    <p class="mb-0">You’ve upgraded to a VIP account successfully!</p>
                                                                                    <div class="text-muted font-size-sm font-italic">15 min ago</div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                                                <div class="ml-5 mr-15">
                                                                                    <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                                                                </div>
                                                                                <div class="media-body pr-10">
                                                                                    <p class="mb-0">Please check your payment info since we can’t validate them!</p>
                                                                                    <div class="text-muted font-size-sm font-italic">50 min ago</div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                                                <div class="ml-5 mr-15">
                                                                                    <i class="fa fa-fw fa-times text-danger"></i>
                                                                                </div>
                                                                                <div class="media-body pr-10">
                                                                                    <p class="mb-0">Web server stopped responding and it was automatically restarted!</p>
                                                                                    <div class="text-muted font-size-sm font-italic">4 hours ago</div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                                                <div class="ml-5 mr-15">
                                                                                    <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                                                                </div>
                                                                                <div class="media-body pr-10">
                                                                                    <p class="mb-0">Please consider upgrading your plan. You are running out of space.</p>
                                                                                    <div class="text-muted font-size-sm font-italic">16 hours ago</div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                                                <div class="ml-5 mr-15">
                                                                                    <i class="fa fa-fw fa-plus text-primary"></i>
                                                                                </div>
                                                                                <div class="media-body pr-10">
                                                                                    <p class="mb-0">New purchases! +$250</p>
                                                                                    <div class="text-muted font-size-sm font-italic">1 day ago</div>
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item text-center mb-0" href="javascript:void(0)">
                                                                        <i class="fa fa-flag mr-5"></i> View All
                                                                    </a>
                                                                </div>
                                                            </div>-->
                                    <!-- END Notifications -->

                                    <!-- Toggle Side Overlay -->
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <!--                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="side_overlay_toggle">
                                                                <i class="fa fa-tasks"></i>
                                                            </button>-->
                                    <!-- END Toggle Side Overlay -->
                                </div>
                                <!-- END Right Section -->
                            </div>
                            <!-- END Header Content -->

                            <!-- Header Search -->
                            <div id="page-header-search" class="overlay-header">
                                <div class="content-header content-header-fullrow">
                                    <form action="be_pages_generic_search.html" method="post">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <!-- Close Search Section -->
                                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                                <button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                <!-- END Close Search Section -->
                                            </div>
                                            <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-secondary">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END Header Search -->

                            <!-- Header Loader -->
                            <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
                            <div id="page-header-loader" class="overlay-header bg-primary">
                                <div class="content-header content-header-fullrow text-center">
                                    <div class="content-header-item">
                                        <i class="fa fa-sun-o fa-spin text-white"></i>
                                    </div>
                                </div>
                            </div>
                            <!-- END Header Loader -->


                            <!-- END Icons -->

                            <link rel="stylesheet" href="<?php echo base_url(); ?>_css/dataTables.bootstrap4.css">
                            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
                            <!-- Page JS Plugins CSS -->
                    <!--        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/slick.css">
                            <link rel="stylesheet" href="<?php echo base_url(); ?>_css/slick-theme.css">-->

                            <!-- Fonts and Codebase framework -->
                            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
                            <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/codebase.min.css">
                            <link rel="stylesheet" href="<?php echo base_url(); ?>_css/school_custom.css">

                            <link rel="stylesheet" href="<?php echo base_url(); ?>_css/summernote-bs4.css">
                            <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">
                            <link rel="stylesheet" href="<?php echo base_url(); ?>_css/jquery-ui.css">
                            <link rel="stylesheet" href="<?php echo base_url(); ?>_css/jquery.timepicker_1.3.5.min.css">


<!--<script src="http://192.168.1.10/webdev/Bidyaaly/site/assets/js/pages/be_pages_dashboard.min.js"></script>-->
                            <script src="<?php echo base_url(); ?>assets/js/codebase.core.min.js"></script>


                            <!--
                                Codebase JS
                            
                                Custom functionality including Blocks/Layout API as well as other vital and optional helpers
                                webpack is putting everything together at assets/_es6/main/app.js
                            -->
                            <script src="<?php echo base_url(); ?>assets/js/codebase.app.min.js"></script>
                            <script src="<?php echo base_url(); ?>_js/jquery.dataTables.min.js"></script>
                            <script src="<?php echo base_url(); ?>_js/dataTables.bootstrap4.min.js"></script>
                            <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
                            <script src="<?php echo base_url(); ?>_js/additional-methods.js"></script>
                            <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>
                            <script src="<?php echo base_url(); ?>_js/be_tables_datatables.min.js"></script>
                            <script src="<?php echo base_url(); ?>_js/jquery-ui.js"></script>
                            <script src="<?php echo base_url(); ?>_js/jquery.timepicker_1.3.5.min.js"></script>
                            <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
                    <!--        <script src="<?php echo base_url(); ?>_js/bootstrap-datepicker.min.js"></script>-->
                            <script src="<?php echo base_url(); ?>_js/summernote-bs4.min.js"></script>
                            <script src="https://cdn.ckeditor.com/4.11.4/basic/ckeditor.js"></script>
                            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
                            <script src="<?php echo base_url(); ?>_js/chart.js"></script>
                            <script src="<?php echo base_url(); ?>assets/js/plugins/chartjs/Chart.bundle.min.js"></script>
                        </header>

                        <!-- Main Container -->
                        <main id="main-container">
