<?php $this->load->view('admin/include/header'); ?>
<style type="text/css">
    .email_container {
        width: 110px;
        height: 20px;
        line-height: 18px;
        display: inline-block;
        overflow: hidden;
        vertical-align: bottom;               
    }
    .expand_container {
        cursor: pointer;
    }
</style>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this school?</p>');
        $('.btn-alt-success').attr('onclick', 'confirm_delete(' + id + ')');
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>admin/user/deleteSchool/" + id;
    }

    function call_restore(id) {
        $('.message_block').html('<p>Are you sure that you want to restore this school?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_restore('" + id + "')");
    }
    function confirm_restore(id) {
        //alert(id);
        window.location.href = "<?php echo base_url(); ?>admin/user/restoreAdmin/" + id;
    }
    
    $(document).ready(function() {
        $(".expand_container").click(function() {
            $(this).toggleClass("email_container");
        });
    });
</script>


<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Schools</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
<!--                            <a href="<?php //echo base_url();  ?>admin/user/addAdmin" class="btn btn-primary">Add Admin</a>-->
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th style="width: 20%;">Name</th>
                            <th>Username</th>
                            <th>Person</th>
                            <th style="width: 20%;">Dates</th>
                            <th style="width: 20%;">Pack</th>

    <!--                        <th>Join DateTime</th>                        
                            <th>License</th>
                            <th>Expiry Date</th>
                            <th>Available SMS Credit</th>
                            <th>Status</th>-->
                            <th class="text-center" style="width: 80px;">Action</th>                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!empty($schools)) {
                                $i = 1;
                                foreach ($schools as $row) {
                                
                                    //$encrypted = $this->my_custom_functions->ablEncrypt($admin['admin_id']);
                                    $encrypted = $row['id'];
                        ?>
                                    <tr>
                                        <td class="text-center">
                                            <?php 
                                                echo $i; 
                                            ?>
                                        </td>
                                        <td>
                                            <i class="fa fa-university" aria-hidden="true"></i>&nbsp;
                                            <?php 
                                                echo $row['name']; 
                                            ?>    
                                            <br>
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
                                            <?php    
                                                echo $row['address'];
                                            ?>                                            
                                        </td>
                                        <td>
                                            <?php 
                                                echo $row['username']; 
                                            ?>
                                            <br>
                                            <i class="fas fa-tasks"></i>&nbsp;
                                            <?php
                                                echo $row['progress_percentage'].'%';
                                            ?>
                                        </td>
                                        <td style="word-break: break-all;">
                                            <i class="fa fa-user" aria-hidden="true"></i>&nbsp;
                                            <?php 
                                                echo $row['contact_person_name']; 
                                            ?>
                                            <br>
                                            <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
                                            <?php 
                                                echo $row['mobile_no'].' '.$row['mobile_no_verified_status']; 
                                            ?>
                                            <br>
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="expand_container email_container" title="<?php echo $row['email_address']; ?>">
                                                <?php 
                                                    echo $row['email_address']; 
                                                ?>
                                            </span>
                                            <?php 
                                                echo $row['email_verified_status']; 
                                            ?>
                                        </td>                                                                                
                                        <td>
                                            <span title="Joined">
                                                <i class="fas fa-user-plus"></i>&nbsp;                                            
                                                <?php 
                                                    if($row['created_at'] != 0) {
                                                        echo date('d-m-Y h:i A', $row['created_at']); 
                                                    } else {
                                                        echo 'N/A';
                                                    }                                                
                                                ?>
                                            </span>
                                            <br>
                                            <span title="Last Logged-in">
                                                <i class="fas fa-sign-in-alt"></i>&nbsp;                                            
                                                <?php 
                                                    if($row['last_login_time'] != 0) {
                                                        echo date('d-m-Y h:i A', $row['last_login_time']); 
                                                    } else {
                                                        echo 'N/A';
                                                    }
                                                    echo $row['last_login_day_difference'];
                                                ?>
                                            </span>    
                                        </td>                                       
                                        <td>
                                            License / SMS:&nbsp;                                            
                                            <?php 
                                                echo $row['no_of_license']; 
                                            ?>
                                            /
                                            <?php 
                                                echo $row['available_sms_credits']; 
                                            ?>
                                            <br>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Expiry:&nbsp;                                          
                                            <?php     
                                                if($row['license_expiry_date'] != '0000-00-00') {
                                                    echo $this->my_custom_functions->database_date_dash($row['license_expiry_date']);
                                                } else {
                                                    echo 'N/A';
                                                }                                                
                                            ?>
                                        </td>                                                                                
                                        <td class="text-center">  

        <!--                                    <a href="<?php //echo base_url() . 'admin/user/manageLicense/' . $encrypted; ?>" title="License" target="_blank" >
                                                <i class="fa fa-drivers-license"></i>
                                            </a>-->

                                            <a href="<?php echo base_url() . 'admin/user/loginAsSchool/' . $encrypted; ?>" title="Login As School" target="_blank" >
                                                <i class="fa fa-user"></i>
                                            </a>
                                            &nbsp;  
                                            <?php if ($row['is_deleted'] == 0) { ?>
                                                    <a href="<?php echo base_url() . 'admin/user/editSchool/' . $this->my_custom_functions->ablEncrypt($row['id']); ?>" title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                            <?php } ?>                                        
                                            &nbsp;  
                                            <?php if ($row['is_deleted'] == 0) { ?>
                                                    <a href="<?php echo base_url() . 'admin/user/deleteSchool/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete(<?php echo $encrypted; ?>);">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                            <?php } else { ?>
                                                    <a href="<?php echo base_url() . 'admin/user/restoreAdmin/' . $encrypted; ?>" title="Restore" data-toggle="modal" data-target="#modal-top" onclick="return call_restore('<?php echo $encrypted; ?>');">
                                                        <i class="fa fa-undo"></i>
                                                    </a>              
                                            <?php } ?>
                                        </td>
                                    </tr>
                        <?php
                                    $i++;
                                }                         
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="7" style="text-align:center;">No school found</td>
                                </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
       
            </div>     
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
