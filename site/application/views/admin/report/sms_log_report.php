<?php $this->load->view('admin/include/header'); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>_css/jquery.dropdown.css"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>_js/jquery.dropdown.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#clickbtn").click(function () {
                $("#div_slideDown").slideToggle('fast');
            });
            
            // Dropdown search 
            $('.dropdown-sin-1').dropdown({
                readOnly: true,
                input: '<input type="text" maxLength="20" placeholder="Search">'
            });
            
            // Datepicker
            $('.date_range').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
            });
        });
        
        function reset_form() {  
        
            $("#from_date").val("");
            $("#to_date").val("");
            $("#school").val("");
            
            $("#search").trigger("click");
        }                
    </script>

    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">SMS Log Report</h2>
        
        <div class="block">
            <div class="block-header block-header-default">                
                <a href="javascript:" class="btn btn-primary" id="clickbtn">Search</a>
            </div>
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>
            
            <div class="block-content block-content-full">
                <?php
                    if($this->session->userdata('smslog_from_date') OR $this->session->userdata('smslog_to_date') OR $this->session->userdata('smslog_school_id')) {
                        $search_box = 'style="display: block;"';
                    } else {
                        $search_box = '';
                    }
                ?>
                <div id="div_slideDown" <?php echo $search_box; ?>>
                    <form method="post" id="searchSMSCredit" action="<?php echo current_url(); ?>" autocomplete="off">
                        <div class="form-row">
                            <div class="form-material col-md-6">
                                <input type="text" class="form-control date_range" name="from_date" id="from_date" placeholder="From Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php if($this->session->userdata('smslog_from_date')) { echo $this->session->userdata('smslog_from_date'); } ?>">
                                <label for="from_date">From Date</label>
                            </div>
                            <div class="form-material col-md-6">
                                <label for="to_date">To Date</label>
                                <input type="text" class="form-control date_range" name="to_date" id="to_date" placeholder="To Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php if($this->session->userdata('smslog_to_date')) { echo $this->session->userdata('smslog_to_date'); } ?>">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-material col-md-6">
                                <div class="dropdown-sin-1">
                                    <select class="form-control" id="school" name="school" placeholder="">
                                        <option value="">Select School</option>
                                        <?php 
                                            if(!empty($schools)) {
                                                foreach($schools as $school) {
                                                    $selected = '';
                                                    if($this->session->userdata('smslog_school_id')) { 
                                                        if($school['id'] == $this->session->userdata('smslog_school_id')) {
                                                            $selected = 'selected="selected"';
                                                        }
                                                    }
                                        ?>
                                                    <option value="<?php echo $school['id']; ?>" <?php echo $selected; ?>><?php echo $school['name']; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </select>                                    
                                </div>
                                <label for="school">School</label>
                            </div>                        
                        </div>
                        <input type="button" name="reset" value="Reset" class="btn btn-primary" onclick="reset_form();">
                        <input type="submit" name="submit" value="Search" id="search" class="btn btn-primary">
                    </form>
                </div>
                
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th>School</th>
                            <th>Mobile No</th>
                            <th>SMS Text</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if (!empty($smslog)) {
                                $i = 1;
                                foreach ($smslog as $row) {                               
                        ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i; ?></td>
                                        <td><?php echo $row['school_name']; ?></td>
                                        <td><?php echo $row['mobile_no']; ?></td>
                                        <td><?php echo $row['message']; ?></td>                                        
                                        <td><?php echo date("d/m/Y h:i A", $row['request_time']); ?></td> 
                                    </tr>
                        <?php
                                    $i++;
                                }                        
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="5" align="center">No record to show</td>
                                </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>                
    </div>    

<?php $this->load->view('admin/include/footer'); ?>