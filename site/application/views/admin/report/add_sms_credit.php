<?php $this->load->view('admin/include/header'); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>_css/jquery.dropdown.css"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>_js/jquery.dropdown.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // Dropdown search 
            $('.dropdown-sin-1').dropdown({
                readOnly: true,
                input: '<input type="text" maxLength="20" placeholder="Search">'
            });
        });
    </script>    

    <!-- Page Content -->
    <div class="content">

        <!-- Material Forms Validation -->
        <h2 class="content-heading">Add SMS Credit</h2>

        <div class="block">
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-6">
                        <?php echo form_open('', array('id' => 'frmSMSCredit', 'class' => 'js-validation-material')); ?>

                                <div class="form-group">
                                    <div class="form-material">
                                        <div class="dropdown-sin-1">
                                            <select class="form-control" id="school" name="school" placeholder="">
                                                <option value="">Select School</option>
                                                <?php 
                                                    if(!empty($schools)) {
                                                        foreach($schools as $school) {
                                                ?>
                                                            <option value="<?php echo $school['id']; ?>"><?php echo $school['name']; ?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>    
                                        <label for="school">School</label>
                                    </div>
                                </div>     

                                <div class="form-group">
                                    <div class="form-material">
                                        <input required type="number" class="form-control" name="credit_amount" id="credit_amount" placeholder="Credit Amount" value="">
                                        <label for="credit_amount">Credit Amount</label>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="form-material">
                                        <div class="custom-control custom-radio custom-control-inline mb-5">
                                            <input class="custom-control-input" type="radio" name="credit_type" id="credit_type1" value="1" checked="">
                                            <label class="custom-control-label" for="credit_type1">Credit In</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline mb-5">
                                            <input class="custom-control-input" type="radio" name="credit_type" id="credit_type2" value="2">
                                            <label class="custom-control-label" for="credit_type2">Credit Out</label>
                                        </div>        
                                        <label for="credit_type1">Credit Type</label>                                        
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>    
    </div>

<?php $this->load->view('school/_include/footer'); ?>