<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript" >
   
    function deleteImg(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this image?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");

           }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>admin/user/delete_profilePic/" + id;
    }
</script>

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Edit Admin</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    <?php
//echo "<pre>";print_r($admin_details);

                    echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material'));
                    ?>


                    <div class="form-group">
                        <div class="form-material">
                            <?php
                            $file_url = $this->my_custom_functions->get_particular_field_value(TBL_ADMIN_FILES,'file_url','and admin_id = "'.$admin_details['admin_id'].'"');
                            
                            if ($file_url != '') {
                                ?>
                                <div class="edit_photo_container">
                                    <div class="icon_delete_container">

                                        <div class="icon_link">
                                            <label for="file-input" >

                                                <input id="file-input"  type="file" name="adminphoto" id="adminphoto">
                                                </div>
                                                </div>
                                    <img src="<?php echo $file_url; ?>" style="width:100px;height:100px;">
                                                <a href="javascript:" title="Delete" data-toggle="modal" data-target="#modal-top"  class="icon_link" onclick="return deleteImg(<?php echo $admin_details['admin_id']; ?>);" >
                                                    <i class="fa fa-trash" aria-hidden="true"> Delete Image</i>
                                                </a>
                                        </div>
                                    <?php } else { ?>

                                        <div class="edit_photo_container">
                                            <div class="icon_delete_container">
                                                <div class="icon_link">
                                                    <label for="file-input" >

                                                        <input id="file-input"  type="file" name="adminphoto" id="adminphoto">
                                                        </div>
                                                        </div>



                                                        </div>
                                                    <?php } ?>
                                                    <label for="name">Profile Picture</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-material">
                                                <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $admin_details['name']; ?>">
                                                <label for="name">Name</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material">
                                                <input type="number" class="form-control" name="phone" id="phone" placeholder="Phone number" value="<?php echo $admin_details['phone']; ?>">
                                                <label for="phone">Phone Number</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material">
                                                <input type="email" class="form-control validate[required,custom[email]]" name="email" id="email" placeholder="Account email" value="<?php echo $admin_details['email']; ?>">
                                                <label for="phone">Email Address</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-material">
                                                <input readonly="" type="text" class="form-control" name="username" id="username" placeholder="Enter username" value="<?php echo $admin_details['username']; ?>" readonly="true">
                                                <label for="username">Username</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select name="status" class="form-control" id="status">
                                                        <option value="1" <?php if ($admin_details['status'] == 1) { ?>selected="selected" <?php } ?>>Active</option>
                                                        <option value="0" <?php if ($admin_details['status'] == 0) { ?>selected="selected" <?php } ?>>Inactive</option>
                                                    </select>
                                                    <label for="status">Status</label>
                                                </div>
                                            </div>
                                            <br>

                                            <div class="form-group">
                                                <input type="submit" name="save" value="Update" class="btn btn-alt-primary">
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                    <?php $this->load->view("admin/include/footer"); ?>
