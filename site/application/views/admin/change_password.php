<?php $this->load->view("admin/include/header"); ?>


<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Change Password</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <?php echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>               
                    <div class="form-group">
                        <div class="form-material">
                            <input type="password" name="o_password" class="form-control" id="o_password" placeholder="Old password" required="">
                            <?php //echo form_password(array("name" => "o_password", "id" => "o_password", "class" => "form-control", "placeholder" => "Old password","required" => "")); ?>                                   
                        <label for="o_password">Old Password</label>
                        </div>
                    </div>

                        <div class="form-group">
                        <div class="form-material">
                            <?php echo form_password(array("name" => "n_password", "id" => "n_password", "class" => "form-control", "placeholder" => "New password", "required" => "")); ?>                                    
                        <label for="name">New Password</label>
                        </div>
                        </div>

                        <div class="form-group">
                        <div class="form-material">
                            <?php echo form_password(array("name" => "c_password", "id" => "c_password", "class" => "form-control", "placeholder" => "Confirm new password", "required" => "")); ?>
                        <label for="name">Confirm Password</label>
                        </div>
                        </div>

                        <div class="form-group">                                   
                            <input type="submit" name="change_password" id="change_password" value="Update" class="btn btn-alt-primary">                        
                        </span>                                                              
                    </div>   
                </div>
                <?php echo form_close(); ?>          

                <?php $this->load->view("admin/include/footer"); ?>