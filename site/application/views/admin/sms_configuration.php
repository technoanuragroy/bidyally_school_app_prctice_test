<?php $this->load->view('admin/include/header'); ?>


<script type="text/javascript">
    $(document).ready(function () {

        $('.copier_link').click(function () {
            var fees_container_html = $(".examSubjectcopy").html();
            $(".examSubjectAppend").append(fees_container_html);
        });
    });
    
     function remove_fees(e) {

        $(e).closest(".row_abc").remove();
    }
    
   
</script>    

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Manage SMS gateway configuration</h2>

    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        
        <div class="block-content">
<!--            <div class="row">
                <div class="col-md-6 col-sm-12"><a href="<?php //echo base_url(); ?>admin/setting/smsGatewayConfigure" class="config_sms" >Config Sms</a></div>
                <div class="col-md-6 col-sm-12"><a href="<?php //echo base_url(); ?>admin/setting/emailGatewayConfigure" class="config_sms" >Config Email</a></div>
        </div>-->
            <div class="sms_conf">
                
                <div class="row justify-content-center py-20">
                    <div class="col-xl-6">
                        <?php echo form_open('admin/setting/addSMSGatewaySettings', array('id' => 'frmSMS', 'class' => 'js-validation-material')); ?>
                        


                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="sms_url" id="sms_url" placeholder="Sms Url" value="<?php echo $sms_settings['sms_url']; ?>">
                                <label for="credit_amount">Sms Url</label>
                            </div>
                        </div>

                        <div class="examSubjectAppend">
                            <?php foreach($sms_settings['key_values'] as $value){ //echo "<pre>";print_r($value);
                                    foreach($value as $key => $val){ ?>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="sms_variable[]" id="sms_variable" placeholder="Sms variable" value="<?php echo $key; ?>">
                                    <label for="credit_amount">Sms Variable Name</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="sms_value[]" id="sms_value" placeholder="Sms Value" value="<?php echo $val; ?>">
                                    <label for="credit_amount">Value</label>
                                </div>
                            </div>
                            <?php }} ?>
                        </div>
                        <div class="form-group">
                            <div class="form-material">

                                <label for="copy_existing_exam"><a href="javascript:" class="copier_link"><i class="fa fa-plus"></i> Add More</a></label>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="form-material">
                                <div class="custom-control custom-radio custom-control-inline mb-5">
                                    <input class="custom-control-input" type="radio" name="system_sms" id="system_sms1" value="1" <?php if($sms_settings['system_sms'] == 1){echo "checked";} ?>>
                                    <label class="custom-control-label" for="system_sms1">On</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline mb-5">
                                    <input class="custom-control-input" type="radio" name="system_sms" id="system_sms2" value="2" <?php if($sms_settings['system_sms'] == 2){echo "checked";} ?>>
                                    <label class="custom-control-label" for="system_sms2">Off</label>
                                </div>        
                                <label for="credit_type1">System Sms</label>                                        
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                    <div class="examSubjectcopy" style="display: none;">
                        <div class="row_abc">
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="sms_variable[]" id="sms_variable" placeholder="Sms variable" value="">
                                <label for="credit_amount">Sms Variable Name</label>
                            </div>
                           
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" class="form-control" name="sms_value[]" id="sms_value" placeholder="Sms Value" value="">
                                <label for="credit_amount">Value</label>
                            </div>
                        </div>
                         <a href="javascript:" class="" onclick="remove_fees(this);"><i class="fa fa-minus-circle"></i>&nbsp;Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
            
            
            
            
        </div>
    </div>    
</div>



<?php $this->load->view('school/_include/footer'); ?>