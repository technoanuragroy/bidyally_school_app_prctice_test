<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Twib</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" >
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109694029-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109694029-2');
</script>
        <!-- recaptcha-->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
            function validate(event) {
                    event.preventDefault();
                    if (!document.getElementById('email').value) {
                      alert("Please enter your email address.");
                    } else {
                      grecaptcha.execute();
                    }
                  }

            function onload() {
              var element = document.getElementById('submit');
              element.onclick = validate;
            }
        </script>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/front_style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
</head>
    <body class="home_body">
        <div>
            <div class="header">
                <div class="header_bg">
                    <div class="container">
                        <div class="row">
                            <div class="top_content"> <a href="#" class="navbar-brand"><img src="<?php echo base_url(); ?>images/logo.png" alt=""/></a>
                                <div class="Social_media">
                                    <a class="media_link" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a class="media_link facebook_icon" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a class="media_link " href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="text_container"> 
                                <h1 class="heading1">Your Sales process,Simplified</h1>
                                <h2 class="heading2">Gps tracking, work assign, expenses, Attendance</h2>
                                            <div class="heading2" style="font-size: 18px;color:red;">
                                                <?php if($this->session->flashdata("e_message")) { echo '<p class="e_message">'.$this->session->flashdata("e_message").'</p>'; } ?>                            
                                            </div>
                                            <div class="heading2" style="font-size: 18px;">
                                                <?php if($this->session->flashdata("s_message")) { echo '<p class="s_message">'.$this->session->flashdata("s_message").'</p>'; } ?>
                                            </div>
                                <div class="form_container">
                                    <?php echo form_open('', array('id' => 'i-recaptcha')); ?>
                                            <div class="input_container">
                                                <input type="email" name="email" id="email" class="form-control validate[required[custom[email]]]" placeholder="Enter your email... " required/>
                                            </div>
                                               
                                            <input type="submit" value="Join waiting list" class="button1" name="submit" />
                                            <div id='recaptcha' class="g-recaptcha"
                                                    data-sitekey="6LebgjgUAAAAADRBQBT0T05tooS3LzVxEKaBxgmQ"
                                                    data-callback="onSubmit"
                                                    data-size="invisible"></div>
                                        <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>
        <script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>          
        <script>onload();</script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>   
</body>
</html>
