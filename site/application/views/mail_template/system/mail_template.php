<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=windows-1252">
</head>
<body style="margin:0;">
       <div style="background-color:#f4f4f4;margin:0!important;padding:0!important">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#ECB101">
                        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr bgcolor="#ECB101">
                                    <td style="padding:30px 10px 30px 10px" valign="top" align="center" bgcolor="#ECB101">
                                      <img alt="Bidyaaly" src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/main_logo.png" style="display:block;width:200px;max-width:200px;min-width:40px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#ffffff;font-size:18px" class="CToWUd" width="140" height="" border="0">
<!--                                        <span style="color:#000;font-size:34px;">Bidyaaly</span>-->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                
                <!-- Header ends -->
                <!-- Body starts -->
                <tr>
                    <td style="padding:0px 10px 0px 10px;background: #fff;" align="center" bgcolor="">
                        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#f4f4f4">

                            <tr bgcolor="#ffffff">
                                <td style="padding:20px 30px 5px 30px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:25px" valign="top" align="left" bgcolor="#ffffff">
                                    <span style="text-align:center;color:#595959; text-transform:capitalize;">
<!--                                        <h1>mail template</h1>-->
                                        %addressing_user% <!-- Addressing user -->

                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:20px 30px 30px 30px;border-radius:0px 0px 8px 8px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:25px" valign="top" align="center" bgcolor="#ffffff">
                                    <p style="margin:0;text-align:center">
                                        %mail_body% <!-- Mail body -->
                                    </p>
                                    <p style="text-align:left">
                                        Regards,<br>%institute_name%
                                    </p>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="padding:0px 30px 0px 30px;border-radius:0px 0px 8px 8px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:25px" valign="top" align="center" bgcolor="#ffffff">
                                    
                                    <p style="text-align:left">
                                        %systemtext%
                                    </p>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding:30px 10px 0px 10px" align="center" bgcolor="#fff">
                                    <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding:30px 30px 0px 30px;border-radius:4px 4px 4px 4px;color:#666666;font-family:'Lato',Helvetica,Arial,sans-serif;font-size:18px;font-weight:100!important;line-height:25px" align="center" bgcolor="transparent">
                                                    <p style="margin:0;color:#606060">Let's connect!</p>
                                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                              <tr>
                                                                  <td style="padding:10px 0 8px 0" valign="top" align="center">
                                                                    <a style="display:inline-block" href="https://www.facebook.com/bidyaaly" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/small_fb.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>&nbsp;&nbsp;&nbsp;
                                                                    <a style="display:inline-block" href="https://twitter.com/bidyaaly" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/small_twitter.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>&nbsp;&nbsp;&nbsp;
                                                                    <a style="display:inline-block" href="https://bidyaaly.com/" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/globe.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>&nbsp;&nbsp;&nbsp;
<!--                                                                    <a style="display:inline-block" href="https://www.youtube.com/channel/UC_5Sv4zjYabCvzTmhp9ku-Q" target="_blank" >
                                                                      <img style="display:block;border-width:0" src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/small_youtube.png" alt="" class="CToWUd" width="35" height="35" border="0">
                                                                    </a>-->
                                                                  </td>
                                                              </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table style="max-width:600px; margin: 5px 0 10px;" width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="center" bgcolor="transparent">
                                                    <a style="text-decoration:none">
                                                        <span style="text-align:center;color:#aaa!important;text-decoration:none;display:block;">
                                                            <a href="https://play.google.com/store/apps/details?id=com.ablion.bidyaaly&hl=en">
                                                                <img src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/google_play_icon.png" style="vertical-align: top;">
                                                            </a>
                                                        </span>
                                                    </a>
                                                </td>
<!--                                                <td align="left" bgcolor="transparent">
                                                    <a style="text-decoration:none">
                                                        <span style="text-align:center;color:#aaa!important;text-decoration:none;display:block;">
                                                            <a href="https://itunes.apple.com/in/app/twib/id1453389043">
                                                                <img src="https://twib.online/global_assets/apple_app_store.png?t=01042019" style="padding-left: 18px;vertical-align: top;">
                                                            </a>
                                                        </span>
                                                    </a>
                                                </td>-->
                                            </tr>
                                        </tbody>
                                    </table>    

                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
       </div>
</body>
</html>