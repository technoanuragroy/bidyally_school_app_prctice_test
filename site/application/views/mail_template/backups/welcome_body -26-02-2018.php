
                  <span style="text-align:left;color:#666666"> Thank you for signing up.</span>
                  <br><br>
                I'm absolutely thrilled that you've decided to give Twib a try and just want you to know that if you want to contact me or the team, feel free to do so anytime at hello@twib.online.<br>
                <br> Download the twib mobile app now.
<!--                <a href="https://mailtrack.io/trace/link/641e9a0144667c0622bcbcfc0c59aaf6d6324568?url=https%3A%2F%2Ftwib.online&userId=1249411&signature=bf5b33fa7fd1d59d">-->
                    <a href="https://play.google.com/store/apps/details?id=com.twib.online">
                    <img src="http://twib.online/global_assets/google_play_icon.png" style="padding-left: 9px;vertical-align: top;">
                </a><br><br>
                <span style="color:#333333;font-weight:700">Please follow the steps to maximize your Twib journey.</span>
                <br><br>
                <span style="color:#333333;font-weight:700">Verify your email</span>
                <br> You need to verify your email account in order to use twib. Check your inbox for verification email and click the verification link to activate your account to use all the features.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/user/profile">
                    Verify Your Account
                </a>
                <br>
                <br><span style="color:#333333;font-weight:700">Select your package</span>
                <br>Whether you want to just try twib or is serious about maximizing your business growth and simplify sales process, just select your free or paid package or can try our full featured trial options.
                <br>
                <a href="https://web.twib.online/company/user/selectPackage" bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px">
                    Select Your Package
                </a>
                <br>
                <br><span style="color:#333333;font-weight:700">Add users</span>
                <br>Add your sales personals in the twib company panel and start tracking your users sales activity on the field.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/user/add_user">
                    Add users
                </a>
                <br>
                <br><span style="color:#333333;font-weight:700">Assign work remotely</span>
                <br>Assign work to your users so that they can view and work accordingly.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/user/add_work">
                  Assign work now
              </a>
                <br>
                <br><span style="color:#333333;font-weight:700">View reports</span>
                <br>Once your users start using twib on the field then you can just sit back and view their activity and statistics, detail charts and reports.
                <br>
                <a bgcolor="#52B7A5" align="center" style="color:#ffffff;background-color:#52B7A5;display:inline-block;font-size:0.8rem;font-family:'Lato',Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="https://web.twib.online/company/report/checkin_report">
                    Check Activity
                </a>
                <br><br>
                <br> Let me know if you have any questions.
                <br>
                <br> Have a great day,
                <br> <span style="color:#333;font-weight: 700;">Team Twib</span>
              