<tr>
    <td style="padding:0px 0px 0px 0px" align="center" bgcolor="#f4f4f4">
        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#0395a5">
            <tbody>
                <tr bgcolor="#ffffff">
                    <td style="padding:20px 30px 5px 30px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:1.1rem;font-weight:400;line-height:25px" valign="top" align="left" bgcolor="#ffffff">
                        <span style="text-align:left;color:#666666">
                            %s
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td style="padding:0px 0px 0px 0px;background:#f4f4f4;" align="center" bgcolor="#f4f4f4">
        <table style="max-width:600px" width="100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td style="padding:20px 30px 30px 30px;border-radius:0px 0px 8px 8px;color:#666666;font-family:Lato,Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:25px" valign="top" align="center" bgcolor="#ffffff">
                        <p style="margin:0;text-align:left">
                            %s
                        </p>
                        <p style="text-align:left">
                            <br> Thank you,
                            <br> Team Twib
                        </p>