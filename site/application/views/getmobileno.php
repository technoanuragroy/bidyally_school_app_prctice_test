<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SchoolApp|Register</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>


    </head>
    <body>
        <div class="wrapper">
            <div class="wrap_content heightFullwraper">
                <!-- Default form register -->
                <form class="text-center p-5 js-validation-material" action="<?php echo current_url(); ?>" method="post">
                    <h1>Enter mobile no</h1>
                    <div class="col-md-12">
                        <?php if ($this->session->flashdata("s_message")) { ?>
                            <!-- Success Alert -->
                            <div class="alert alert-success alert-dismissable s_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                            </div>
                            <!-- END Success Alert -->
                        <?php } ?>
                        <?php if ($this->session->flashdata("e_message")) { ?>
                            <!-- Danger Alert -->
                            <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                            </div>
                            <!-- END Danger Alert -->
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <div class="md-form">
                                <input required type="number" name="materialMobileno" maxlength="10" minlength="10" id="materialMobileno" class="form-control">
                                <label for="materialMobileno">Mobile no</label>
                            </div>
                        </div>
                    </div>


                    <input name="submit" type="submit" class="btn btn001" value="Get OTP"/>
                    <div class="pre_msg">Only pre-registered parents of <?php echo SITE_NAME; ?> associated organization will be able to signup</div>
                </form>
                
                <?php /*
                <div class="msg_container">               
                    <div class="seperator"></div>
                    <div class="sign_upp">Are you a School/College/Institute? <a target="_blank" href="<?php echo base_url(); ?>school/signUp">Signup Here.</a></div>
                </div>
                */ ?>
                
                <div class="bgBottom"></div>
            </div>
        </div>


    </body>
</html>
