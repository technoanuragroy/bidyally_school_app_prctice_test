
<?php $this->load->view('school/_include/header'); 
//require_once '_include/header.php';
?>

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Edit Profile</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <?php //echo "<pre>";print_r($teacher_details);?>
                                    
                                    <div class="form-group row">
                                        <label class="col-12">Staff Type</label>
                                        <div class="col-12">
                                            <?php if($teacher_details['staff_type'] == 1){?>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="staff_type" id="example-inline-radio1" value="1" <?php if($teacher_details['staff_type'] == 1){echo "checked";} ?>>
                                                <label class="custom-control-label" for="example-inline-radio1">Teaching Staff</label>
                                            </div>
                                            <?php } ?>
                                            <?php if($teacher_details['staff_type'] == 2){ ?>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required="" class="custom-control-input" type="radio" name="staff_type" id="example-inline-radio2" value="2" <?php if($teacher_details['staff_type'] == 2){echo "checked";} ?>>
                                                <label class="custom-control-label" for="example-inline-radio2">Non-teaching Staff</label>
                                            </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                           <?php $username = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN,'username','and id = "'.$this->session->userdata('teacher_id').'"');?> 
                                            <input required type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" readonly="">
                                            <label for="name">Username</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $teacher_details['name']; ?>">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="mobile_no" id="mobile_no" type="text" value="<?php echo $teacher_details['phone_no']; ?>">
                                                <label for="mobile_no">Phone Number</label>
                                            </div></div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="email_address" id="email_address" type="text" value="<?php echo $teacher_details['email']; ?>">
                                                <label for="email_address">Email Address</label>
                                            </div>
                                        </div>
                                    </div>

                                    



                                    <div class="form-group row">
                                        <?php
                                        $imgfile = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $this->session->userdata('teacher_id') . '"');
                                        if ($imgfile != '') {
                                            
                                            ?>
                                            <img src="<?php echo $imgfile; ?>" style="width:100px;height:100px;" />
                                            <span class="manu_chk"><input type="checkbox" name="del_img" class="form-group" value="1">Remove Image</span>

                                            <?php
                                        } else {
                                            $imgfile = base_url() . '_images/avatar15.jpg';
                                            ?>

                                            <img src="<?php echo $imgfile; ?>" />
                                        <?php } ?>


                                        <div class="form-group row">
                                            <label class="col-12" for="userphoto">Upload Photo</label>
                                            <div class="col-12">
                                                <input type="file" id="adminphoto" name="adminphoto"><br>
                                                (Image size should be 200px*200px )
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <input type="hidden" name="teacher_id" value="<?php echo $teacher_details['id']; ?>">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Save">
                                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

            <?php $this->load->view('school/_include/footer'); ?>



