
<?php $this->load->view('school/_include/header');?>
        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this syllabus?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
            }
            function confirm_delete(id) {
                window.location.href = "<?php echo base_url(); ?>school/user/deleteSyllabus/" + id;
            }
        </script>
    

                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Manage Syllabus</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <a href="<?php echo base_url(); ?>school/user/addSyllabus"class="btn btn-primary">Add Syllabus</a>
                        </div>
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content block-content-full">
                           
                            <?php
                            if (!empty($syllabus)) {
                                $string = '';
                                $i = 1;
                                ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                        <tr>
                                            <th class="text-center"></th>
                                            <th>Class</th>
    <!--                                        <th>syllabus</th>-->

                                            <th class="text-center">Edit</th>
                                            <th class="text-center">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($syllabus as $row) {


                                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                            //$encrypted = $row['id'];
                                            ?>
                                            <tr>
                                                <td class="text-center"><?php echo $i; ?></td>
                                                <td><?php
                                                    echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = ' . $row['class_id'] . '');
                                                    ?>
                                                </td>
       
                                                <td class="text-center">                                    
                                                    <a href="<?php echo base_url() . 'school/user/editSyllabus/' . $encrypted; ?>" title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td class="text-center">         

                                                    <a href="<?php echo base_url() . 'school/user/deleteSyllabus/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>  
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="9">No syllabus found</td>
                                </tr>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->



                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->

           <?php $this->load->view('school/_include/footer'); ?>

