<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this student?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        //alert(id);
        window.location.href = "<?php echo base_url(); ?>school/user/deleteStudent/" + id;
    }
    
    function call_restore(id) {
        $('.message_block').html('<p>Are you sure that you want to restore this student?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_restore('" + id + "')");
    }
    function confirm_restore(id) {
        //alert(id);
        window.location.href = "<?php echo base_url(); ?>school/user/restoreStudent/" + id;
    }




</script>

<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Student</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <div class="movvsStd col-lg-8 col-sm-5 col-xs-12" style="padding-left: 0;">
                <a href="<?php echo base_url(); ?>school/user/addStudent" class="btn btn-primary shift ads" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url().'school/user/addStudent')?>">Add Student</a>
                <a href="<?php echo base_url(); ?>school/enrollment/enrollStudent" class="btn btn-primary shift" id="clickbtnmove">Enroll Students</a>
            </div>
            <div class="movvStd col-lg-4 col-sm-7 col-xs-12" style="padding-right: 0;">                
<!--                <a href="javascript:" class="btn btn-primary show_hide shift" id="clickbtnmove" data-target=".slidingDiv">Move Students</a>-->
                <a href="javascript:" class="btn btn-primary show_hide shift" id="clickbtn" data-target=".slidingDiv2">Search Student</a>
            </div>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content block-content-full movedown">
            
            <div id="div_slideDown" class="slidingDiv2 slider load">
                <div class="loader" style="display: none;">
                    <i class="fa fa-3x fa-cog fa-spin"></i>
                </div>
                
                <form method="post" action="">                                        

                    <div class="form-row">
                        <div class="form-material col-md-6">
                            <select class="form-control semester_id" name="semester_id" onchange="get_section_list();">
                                <option value="">Select Semester</option>
                                <?php foreach ($semesters as $semester) { ?>
                                        <option value="<?php echo $semester['id']; ?>"><?php echo $semester['semester_name']; ?></option>
                                <?php } ?>
                            </select>
                            <label for="semester_id">Select Semester</label>
                        </div>
                        
                        <div class="form-material col-md-6">
                            <select class="form-control section_id" name="section_id">
                                <option value="">Select Section</option>
                            </select>
                            <label for="section_id">Select Section</label>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-material col-md-6">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="">
                            <label for="name">Name</label>
                        </div>
                        <div class="form-material col-md-6">                            
                            <input type="text" class="form-control" id="roll_no" name="roll_no" placeholder="Enter Roll Number">
                            <label for="roll_no">Roll Number</label>
                        </div>
                    </div>

                    <input type="submit" name="submit" value="Search" class="btn btn-primary">
                    
                </form>
            </div>
            
            <?php
                if (!empty($students)) {
                    $i = 1;
            ?>
                    <div class="table_responsive">
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>Student name</th>
                                    <th>Current Enrollments</th>        
                                    <th class="d-none d-sm-table-cell">Father Name</th>
<!--                                    <th class="d-none d-sm-table-cell">Mother Name</th>-->
                                    <th class="d-none d-sm-table-cell">Registered Numbers</th>
                                    <th class="d-none d-sm-table-cell">Status</th>
                                    <th class="text-center" style="width: 15%;">Action</th>                            
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($students as $row) {

                                        $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                        $active_semesters = $this->my_custom_functions->get_semesters_of_a_student($row['id'], $this->session->userdata("session_id"));                                            
                                ?>
                                        <tr>

                                            <td><?php echo $row['name']; ?></td>
                                            <td>
                                                <?php 
                                                    $current_enrollments = '';
                                                    if(!empty($active_semesters)) {
                                                        foreach($active_semesters as $a_s) {
                                                            $current_enrollments .= 'Semester: '.$a_s['semester_name'].', ';
                                                            $current_enrollments .= 'Section: '.$this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $a_s['section_id'] . '"').', ';
                                                            $current_enrollments .= 'Roll No: '.$a_s['roll_no'].',';
                                                            $current_enrollments .= '<br>';
                                                        }
                                                    }
                                                    echo rtrim($current_enrollments, ',<br>');
                                                ?>
                                            </td>       
                                            <td class="d-none d-sm-table-cell"><?php echo $row['father_name']; ?></td>
<!--                                            <td class="d-none d-sm-table-cell"><?php echo $row['mother_name']; ?></td>-->
                                            <td class="d-none d-sm-table-cell">
                                                <?php
                                                    $phone_number = '';
                                                    $parent_ids = $this->my_custom_functions->get_multiple_data(TBL_PARENT_KIDS_LINK, 'and student_id = "' . $row['id'] . '"');

                                                    if (!empty($parent_ids)) {
                                                        foreach ($parent_ids as $rows) {
                                                            $ph_nn = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $rows['parent_id'] . '"');
                                                            if ($ph_nn != '') {
                                                                $phone_number .= $ph_nn . ',';
                                                            }
                                                        }
                                                        $phone_numbers = rtrim($phone_number, ',  ');
                                                        $phone_numbers = ltrim($phone_numbers, ',');
                                                        $phone_numbers = trim($phone_numbers, ',');
                                                        echo $phone_numbers;
                                                    }
                                                ?>
                                            </td>
                                            <td class="d-none d-sm-table-cell">
                                                <?php
                                                    if ($row['status'] == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                ?>
                                            </td>
                                            <td class="text-center"> 
                                                <?php if($row['is_deleted'] == 0) { ?>
                                                <a class="ads" href="<?php echo base_url() . 'school/user/editStudent/' . $encrypted; ?>" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url().'school/user/editStudent/'.$encrypted)?>" title="Edit">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                <?php } ?>

                                                <?php if($row['is_deleted'] == 0) { ?>
                                                        <a href="<?php echo base_url() . 'school/user/deleteStudent/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                <?php } else { ?>
                                                        <a href="<?php echo base_url() . 'school/user/deleteStudent/' . $encrypted; ?>" title="Restore" data-toggle="modal" data-target="#modal-top" onclick="return call_restore('<?php echo $encrypted; ?>');">
                                                            <i class="fa fa-undo"></i>
                                                        </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                <?php
                                        $i++;
                                    }
                                ?>  
                            </tbody>
                        </table>
                    </div>
            <?php                         
                } else { 
            ?>
                    No student found                                
            <?php
                }
            ?>
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>

<script type="text/javascript">

    $(document).ready(function () {
        //all target div's which has to be toggled has the class slider
        var $sliders = $(".slider").hide();
        //all anchors which has to trigger the slide has the class show_hide and has a data-* property data-target whose value is the selector to find the target div
        $(".show_hide").show();

        $('.show_hide').click(function () {
            var $target = $($(this).data('target'));
            $sliders.not($target).stop(true, true).slideUp();
            $target.stop(true, true).slideToggle();
        });                
    });
    
    function get_section_list() {
        
        var semester_id = $('.semester_id').val();
       
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader2').hide();
                $(".section_id").html("");
                if (msg != "") {                    
                    $(".section_id").html(msg);
                }
            }
        });
    }
</script>