<?php $this->load->view('school/_include/header'); ?>

    <script type="text/javascript">
        function call_delete(id) {
            $('.message_block').html('<p>Are you sure that you want to delete this fees structure?</p>');
            $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
        }
        function confirm_delete(id) {
            window.location.href = "<?php echo base_url(); ?>school/user/deleteFeesStructure/" + id;
        }
    </script>
    
    <!-- Page Content -->
    <div class="content">
        <h2 class="content-heading">Fees Structure</h2>
        
        <div class="block">
            <div class="block-header block-header-default">
                <a href="<?php echo base_url(); ?>school/user/addFeesStructure" class="btn btn-primary">Add Fees Structure</a>
            </div>
                        
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                            
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>
            
            <div class="block-content block-content-full">
                <?php
                    if (!empty($fees_structure)) {
                        $i = 1;
                ?>
                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>Semester Name</th>
                                    <th class="text-center" style="width: 25%;">Fees Structure Breakup</th>
                                    <th class="text-center" style="width: 15%;">Edit</th>
                                    <th class="text-center" style="width: 15%;">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($fees_structure as $row) {
                                        $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);    
                                        $breakups_exist = $this->my_custom_functions->get_perticular_count(TBL_FEES_STRUCTURE_BREAKUPS, 'and fees_id="' . $row['id'] . '"');
                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'semester_name', 'and id = "' . $row['semester_id'] . '"'); ?>
                                            </td>

                                            <td class="text-center">
                                                <?php if($breakups_exist > 0) { ?>
                                                        <a href="<?php echo base_url() . 'school/user/editFeesStructureBreakup/' . $encrypted; ?>" class="btn btn-primary">Fees Structure Breakup</a>                                                        
                                                <?php } else { ?>
                                                        <a href="<?php echo base_url() . 'school/user/feesStructureBreakup/' . $encrypted; ?>" class="btn btn-primary">Fees Structure Breakup</a>
                                                <?php } ?>         
                                            </td>
                                            
                                            <td class="text-center">                                    
                                                <a href="<?php echo base_url() . 'school/user/editFeesStructure/' . $encrypted; ?>" title="Edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            
                                            <td class="text-center">         
                                                <a href="<?php echo base_url() . 'school/user/deleteFeesStructure/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                <?php
                                        $i++;
                                    }
                                ?>  
                            </tbody>
                        </table>

                <?php                 
                    } else { 
                ?>
                        <tr>
                            <td colspan="4">No fees structure found</td>
                        </tr>
                <?php
                    }
                ?>

            </div>
        </div>        
    </div>   

<?php $this->load->view('school/_include/footer'); ?>
