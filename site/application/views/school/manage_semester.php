
<?php $this->load->view('school/_include/header');?>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this semester?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteSemester/" + id;
    }
</script>
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Semester</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url(); ?>school/user/addSemester" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url().'school/user/addSemester')?>" class="btn btn-primary ads">Add Semester</a>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">
           
            <?php

            if (!empty($semester)) {
                $i = 1;
                ?>
                 <!--                <table class="table table-striped table-vcenter js-dataTable-full">-->
                    <table class="table table-striped table-vcenter dataTable no-footer ">
                    <thead>
                        <tr>
                            
                            <th>Semester</th>
<!--                            <th class="d-none d-sm-table-cell">Class</th>-->
                            <th class="">Class</th>
<!--                            <th class="d-none d-sm-table-cell">Session</th>-->
                             <th class="">Session</th>
                            <th class="text-center" style="width: 15%;">Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($semester as $row) {


                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                            //$encrypted = $row['id'];
                            ?>
                            <tr>
                                
                                <td class="font-w600"><?php echo $row['semester_name']; ?></td>
<!--                                <td class="d-none d-sm-table-cell">-->
                                    <td class="">
                                    <?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES,'class_name','and id = '.$row['class_id'].''); 
                                    
                                ?></td>
<!--                                <td class="d-none d-sm-table-cell">-->
                                <td class="">
                                    <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SESSION,'session_name','and id = '.$row['session_id'].''); ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url() . 'school/user/editSemester/' . $encrypted; ?>" title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url() . 'school/user/deleteSemester/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                                
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>  
                    </tbody>
                </table>       

<?php } else { ?>
                <tr>
                    <td colspan="9">No classes found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>

<?php $this->load->view('school/_include/footer'); ?>
