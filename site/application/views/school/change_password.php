<?php $this->load->view('school/_include/header');  ?>
<script type="text/javascript">
    function checkpassword(i) {
        
        var new_password = $('#n_password').val();
        var conf_pass = $(i).val();
        
        if(new_password == conf_pass){
            $('.vall').removeClass('is-invalid');
             $('.fullanim').hide();
            $('.sub').show();
            
        }else{
            $('.vall').addClass('is-invalid');
            $('.fullanim').show();
            $('.sub').hide();
        }
    }
    </script>
                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Change Password</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-6">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="password" class="form-control" name="o_password" id="o_password" placeholder="Old password" value="">
                                            <label for="o_password">Old password</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="password" class="form-control" name="n_password" id="n_password" placeholder="New Password" value="">
                                            <label for="n_password">New password</label>
                                        </div>
                                    </div>
                                    <div class="form-group vall">
                                        <div class="form-material">
                                            <input required type="password" class="form-control" name="c_n_password" id="c_n_password" placeholder="Confirm New Password" value="" onkeyup="checkpassword(this);">
                                            <label for="c_n_password">Confirm New password</label>
                                        </div>
                                        <div id="n_password-error" class="invalid-feedback animated fadeInDown fullanim" style="display: none;">Not matched with new password.</div>
                                    </div>
                                    

                                    <div class="form-group sub" style="display:none;">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

           <?php $this->load->view('school/_include/footer'); ?>

