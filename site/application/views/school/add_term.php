
<?php $this->load->view('school/_include/header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#combine_rslt').click(function (){
            if($(this).prop("checked") == true){
                $('#combine_result_name').attr('required','true');
                $('.final_label').slideDown();
            }
            else if($(this).prop("checked") == false){
                $('#combine_result_name').removeAttr('required');
                $('.final_label').slideUp();
            }
        });
    });

//    function get_semester_name() {
//        var class_name = $('#class option:selected').text();
//        var session_name = $('#session option:selected').text();
//
//        var semester_name = class_name + ' (' + session_name + ')';
//
//        if ($('#class').val() == '') {
//            semester_name = '';
//        }
//        if ($('#session').val() == '') {
//            semester_name = '';
//        }
//
//        $('#semester_name').val(semester_name);
//    }
</script> 
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Term</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                    <div class="form-group">
                        <div class="form-material">
                            <select required class="form-control" id="semester" name="semester">
                                <option value="">Select Semester</option>
                                <?php foreach ($semester_list as $sem) { ?>
                                    <option value="<?php echo $sem['id']; ?>"><?php echo $sem['semester_name']; ?></option>
                                <?php } ?>
                            </select>
                            <label for="semester">Semester</label>
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" id="term_name" name="term_name" placeholder="Enter term name">
                            <label for="term_name">Term Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-control-inline mb-5">
                            <input class="custom-control-input" type="checkbox" name="combine_rslt" id="combine_rslt" value="1">
                            <label class="custom-control-label" for="combine_rslt">Combine Exam Result</label>
                        </div>
                    </div>
                    
                    <div class="form-group final_label" style="display:none;">
                        <div class="form-material">
                            <input type="text" class="form-control" id="combine_result_name" name="combine_result_name" placeholder="Enter final term label">
                            <label for="combine_result_name">Combine result name</label>
                        </div>
                    </div>




                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->


<?php $this->load->view('school/_include/footer'); ?>


