
<?php $this->load->view('school/_include/header');?>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this term?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteTerm/" + id;
    }
</script>
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Term</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url(); ?>school/user/addTerm" class="btn btn-primary">Add Term</a>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">
           
            <?php
            
            if (!empty($term)) {
                $i = 1;
                ?>
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th>Term Name</th>
                            <th>Semester Name</th>
                            <th class="text-center" style="width: 15%;">Edit</th>
                            <th class="text-center" style="width: 15%;">Delete</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($term as $row) {


                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                            //$encrypted = $row['id'];
                            ?>
                            <tr>
                                <td class="font-w600"><?php echo $row['term_name']; ?></td>
                                <td class="font-w600"><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'semester_name','and id = '.$row['semester_id'].''); ?></td>
                                <td class="text-center">
                                    <a href="<?php echo base_url() . 'school/user/editTerm/' . $encrypted; ?>" title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <td class="text-center">         

                                    <a href="<?php echo base_url() . 'school/user/deleteTerm/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>  
                    </tbody>
                </table>       

<?php } else { ?>
                <tr>
                    <td colspan="9">No classes found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>

<?php $this->load->view('school/_include/footer'); ?>
