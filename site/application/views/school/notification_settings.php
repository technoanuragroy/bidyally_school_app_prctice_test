<?php $this->load->view('school/_include/header'); ?>
<script type="text/javascript" >


</script>  
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Notification setting</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="ttloader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>



                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">

                                    <label for="period_start_time">Notification Type</label>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">

                                    <label for="period_start_time">Push notification</label>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">

                                    <label for="period_start_time">Email</label>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">

                                    <label for="period_start_time">SMS</label>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <?php $notification_type = $this->config->item('notification_type'); 
                    $count = 0;
                    $psh_checked = '';
                    $mail_checked = '';
                    $sms_checked = '';
                    foreach($notification_type as $key => $val){ 
                        
                        if(!empty($settings)){
                            foreach($settings as $row){
                                if($key == $row['notification_type']){
                                    if($row['push_notification'] == 1){
                                        $psh_checked = "checked='checked'";
                                    }else{
                                        $psh_checked = '';
                                    }
                                    
                                    if($row['email_notification'] == 1){
                                        $mail_checked = "checked='checked'";
                                    }else{
                                        $mail_checked = '';
                                    }
                                    
                                    if($row['sms_notification'] == 1){
                                        $sms_checked = "checked='checked'";
                                    }else{
                                        $sms_checked = '';
                                    }
                                    
                                }
                                
                            }
                        }
                        ?>
                    
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <?php echo $val; ?>

                                    <label for="period_start_time"></label>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                        <input class="custom-control-input checkboxTeacher" type="checkbox" name="push_notif[]" id="push<?php echo $count; ?>" value="<?php echo $key; ?>" <?php echo $psh_checked; ?> >
                                        <label class="custom-control-label" for="push<?php echo $count; ?>"></label>
                                    </div>
                                    <label for="period_start_time"></label>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                        <input class="custom-control-input checkboxTeacher" type="checkbox" name="email_notif[]" id="mail<?php echo $count; ?>" value="<?php echo $key; ?>" <?php echo $mail_checked; ?>>
                                        <label class="custom-control-label" for="mail<?php echo $count; ?>"></label>
                                    </div>
                                    <label for="period_start_time"></label>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                        <input class="custom-control-input checkboxTeacher" type="checkbox" name="sms_notif[]" id="sms<?php echo $count; ?>" value="<?php echo $key; ?>" <?php echo $sms_checked; ?>>
                                        <label class="custom-control-label" for="sms<?php echo $count; ?>"></label>
                                    </div>
                                    <label for="period_start_time"></label>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <?php $count++;} ?>
                    

                    
                    
                    

                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Save">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->


<!-- END Footer -->
<?php $this->load->view('school/_include/footer'); ?>
              


