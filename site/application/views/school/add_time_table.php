<?php $this->load->view('school/_include/header'); ?>
<script type="text/javascript" >
    $(document).ready(function () {


        $('.timepicker').timepicker({
            timeFormat: 'H:mm',
            //interval: 5,
            //minTime: '6',
            //maxTime: '7:00pm',
            defaultTime: '10',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

    });

    function get_section_list() {

        $('.ttloader').show();
        var class_id = $('.class').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.ttloader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }

                get_semester_list(class_id);
            }
        });


    }

    function get_semester_list(class_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_list_tt",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.ttloader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".semester").html(msg);
                } else {
                    $(".semester").html("");
                }
            }
        });
    }

    function check_existing_class() {
        //$('#frmRegister').submit(function(e){
        var day = document.forms["myForm"]["day_id"].value;
        if (day == "") {
            alert("Day field should not be blank");
            return false;
        }
        var class_val = document.forms["myForm"]["class_id"].value;
        if (class_val == "") {
            alert("Class field should not be blank");
            return false;
        }
        var semester_id = document.forms["myForm"]["semester_id"].value;
        if (semester_id == "") {
            alert("Semester field should not be blank");
            return false;
        }
        var section_id = document.forms["myForm"]["section_id"].value;
        if (section_id == "") {
            alert("Section field should not be blank");
            return false;
        }
        var start_hour_val = document.forms["myForm"]["start_hour"].value;
        if (start_hour_val == "") {
            alert("Start Hour field should not be blank");
            return false;
        }
        var start_minute_val = document.forms["myForm"]["start_minute"].value;
        if (start_minute_val == "") {
            alert("Start Minute field should not be blank");
            return false;
        }
        var start_meridian_val = document.forms["myForm"]["start_meridian"].value;
        if (start_meridian_val == "") {
            alert("Start Meridian field should not be blank");
            return false;
        }
        var end_hour_val = document.forms["myForm"]["end_hour"].value;
        if (end_hour_val == "") {
            alert("End Hour field should not be blank");
            return false;
        }
        var end_minuter_val = document.forms["myForm"]["end_minute"].value;
        if (end_minuter_val == "") {
            alert("End Minute field should not be blank");
            return false;
        }
        var end_meridian_val = document.forms["myForm"]["end_meridian"].value;
        if (end_meridian_val == "") {
            alert("End Meridian field should not be blank");
            return false;
        }
        var subject_id_val = document.forms["myForm"]["subject_id"].value;
        if (subject_id_val == "") {
            alert("Subject field should not be blank");
            return false;
        }
        var teacher_id_val = document.forms["myForm"]["teacher_id"].value;
        if (teacher_id_val == "") {
            alert("Teacher field should not be blank");
            return false;
        }

        var start_hour = $('#start_hour').val();
        var start_minute = $('#start_minute').val();
        var start_meridian = $('#start_meridian').val();
        if (start_minute < 10) {
            var start_min = "0" + start_minute;
        } else {
            var start_min = start_minute;
        }
        var start_time = start_hour + ":" + start_min + " " + start_meridian;

        var end_hour = $('#end_hour').val();
        var end_minute = $('#end_minute').val();
        var end_meridian = $('#end_meridian').val();
        if (end_minute < 10) {
            var end_min = "0" + end_minute;
        } else {
            var end_min = end_minute;
        }
        var end_time = end_hour + ":" + end_min + " " + end_meridian;

        var time_table_data = {
            'day_id': $("#day_id").val(),
            'class_id': $("#class_id").val(),
            'semester_id': $("#semester_id").val(),
            'section_id': $("#section_id").val(),
            'start_time': start_time,
            'end_time': end_time,
            'subject_id': $("#subject_id").val(),
            'teacher_id': $("#teacher_id").val(),
        };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/check_duplicate_period",
            data: time_table_data,
            success: function (msg) {
                console.log(msg);
//                if (msg != '') {
//                    if (confirm(msg)) {
//                        
//                        $('#submit').trigger('click');
//
//                    } 
//                }
                $('.message_block').html(msg);
                $('.btn-alt-success').attr('onclick', "confirm_submit()");

            }
        });
        //});
    }
    
    function confirm_submit(){
    $('#submit').trigger('click');
    }

</script>  
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Time Table</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="ttloader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php //echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material', 'name' => 'myForm')); ?>
                    <form method="post" name="myForm" action="" id="frmRegister" class="js-validation-material">

                        <div class="form-group">
                            <div class="form-material">
                                <?php $day_list = $this->config->item('days_list'); ?>
                                <select required name="day_id" id="day_id" class="form-control">
                                    <option value="">Select Day</option>
                                    <?php foreach ($day_list as $kkey => $daylist) { ?>
                                        <option value="<?php echo $kkey; ?>"><?php echo $daylist; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="day_id">Select Day</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select required name="class_id" id="class_id" class="form-control class" onchange="get_section_list()">
                                    <option value="">Select Class</option>
                                    <?php foreach ($class_list as $class) { ?>
                                        <option value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="day_id">Select Class</label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="form-material">
                                <select required name="semester_id" id="semester_id" class="form-control semester">
                                    <option value="">Select Semester</option>
                                </select>

                                <label for="day_id">Select Semester</label>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="form-material">
                                <select required name="section_id" id="section_id" class="form-control section">
                                    <option value="">Select Section</option>
                                </select>

                                <label for="day_id">Select Section</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <input required type="text" name="period_name" id="period_name" class="form-control" placeholder="Period Name">
                                <label for="period_name">Enter Period Name</label>
                            </div>
                        </div>

                        <!--                                    <div class="form-group">
                                                                <div class="form-material">
                                                                    <input required type="text" class="form-control timepicker" name="period_start_time" id="period_start_time" placeholder="Period Start Time" value="">
                                                                    <label for="period_start_time">Period Start Time</label>
                                                                </div>
                                                                (Enter time in 24 hour format. E.g - 11:00, 13:30)
                                                            </div>-->
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <?php $hour_list = $this->config->item('hour_list'); ?>
                                        <select required="" name="start_hour" id="start_hour" class="form-control">
                                            <option value="">Select Hour</option>
                                            <?php foreach ($hour_list as $key => $val) { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="period_start_time">Select Start Hour</label>
                                    </div> 
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <select required="" name="start_minute" id="start_minute" class="form-control">
                                            <option value="">Select Minute</option>
                                            <?php for ($i = 0; $i < 60; $i+=1) { ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="period_start_time">Select Start Minute</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <select required="" name="start_meridian" id="start_meridian" class="form-control">
                                            <option value="">Select</option>
                                            <option value="AM">A.M</option>
                                            <option value="PM">P.M</option>

                                        </select>
                                        <label for="period_start_time">Select Start Meridian</label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <!--                    <div class="form-group">
                                                <div class="form-material">
                                                    <input required type="text" class="form-control timepicker" name="period_end_time" id="period_end_time" placeholder="Period End Time" value="">
                                                    <label for="period_end_time">Period End Time</label>
                                                </div>
                                                (Enter time in 24 hour format. E.g - 11:00, 13:30)
                                            </div>-->

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <?php $hour_list = $this->config->item('hour_list'); ?>
                                        <select required="" name="end_hour" id="end_hour" class="form-control">
                                            <option value="">Select Hour</option>
                                            <?php foreach ($hour_list as $key => $val) { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="period_start_time">Select End Hour</label>
                                    </div> 
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <select required="" name="end_minute" id="end_minute" class="form-control">
                                            <option value="">Select Minute</option>
                                            <?php for ($i = 0; $i < 60; $i+=1) { ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="period_start_time">Select End Minute</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <select required="" name="end_meridian" id="end_meridian" class="form-control">
                                            <option value="">Select</option>
                                            <option value="AM">A.M</option>
                                            <option value="PM">P.M</option>

                                        </select>
                                        <label for="period_start_time">Select End Meridian</label>
                                    </div> 
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">
                                <select name="subject_id" id="subject_id" class="form-control subject">
                                    <option value="">Select Subject</option> 
                                    <?php foreach ($subject_list as $subject) { ?>
                                        <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                    <?php } ?>

                                </select>
                                <label for="subject_id">Select Subject</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material">
                                <select  name="teacher_id" id="teacher_id" class="form-control">
                                    <option value="">Select Teacher</option>
                                    <?php foreach ($teacher_list as $teacher) { ?>
                                        <option value="<?php echo $teacher['id']; ?>"><?php echo $teacher['name']; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="teacher_id">Select Teacher</label>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline mb-5">
                            <input class="custom-control-input checkboxTeacher" type="checkbox" name="attendance_class" id="attendance_class" value="1">
                            <label class="custom-control-label" for="attendance_class">Make as attendance period</label>
                        </div>

                        <div class="form-group">

                            <!--                        <button class="btn btn-alt-primary" name="submit" id="button_submit" onclick="check_existing_class();">Save</button>-->
                            <input type="button" class="btn btn-alt-primary" name="submit" id="button_submit" onclick="check_existing_class();" value="Save" data-toggle='modal' data-target='#modal-top'>
                            <input type="submit" class="btn btn-alt-primary" id="submit" name="submit" value="Save" style="opacity: 0;">
                        </div>
                    </form>
                    <?php //echo form_close(); ?>
                    <!-- </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->


<!-- END Footer -->
<?php $this->load->view('school/_include/footer'); ?>
              


