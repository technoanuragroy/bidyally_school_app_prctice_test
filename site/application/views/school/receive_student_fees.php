<?php $this->load->view('school/_include/header');?>

    <!-- Page Content -->
    <div class="content">                    
        <h2 class="content-heading">Receive Student Fees</h2>
        
        <div class="block">
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-12">
                        <h4>Student: <?php echo $student_details['name']; ?></h4>      
                        
                        <?php if(!empty($fees_structure)) { ?>
                                <div class="form-group">
                                    <div class="form-material">
                                        <?php $semester_name = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'semester_name','and id = "'.$fees_structure['semester_id'].'"'); ?>
                                        <label>Semester - <?php echo $semester_name; ?></label>
                                    </div>
                                </div>
                        <?php } ?>
                        
                        <hr>
                        
                        <h4>Recent Payment Records</h4>                        
                        <div class="table_responsive">
                            <table class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th>Fees Cycle</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Semester Info</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(!empty($recent_payments)) {
                                            foreach($recent_payments as $rp) {
                                    ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE_BREAKUPS,'breakup_label','and id = "'.$rp['fees_breakup_id'].'"'); ?>    
                                                    </td>
                                                    <td>
                                                        <?php echo date("d-m-Y, g:ia", strtotime($rp['payment_datetime'])); ?>    
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $paid_amount = $rp['paid_amount'];
                                                            $payment_handling_fees = $rp['service_charges'];
                                                            $display_amount = $paid_amount - $payment_handling_fees;
                                                            echo INDIAN_CURRENCY_CODE.' '.number_format($display_amount, 2); 
                                                        ?>    
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            if($rp['class_info'] != "") {
                                                                $class_info_array = json_decode($rp['class_info'], true);
                                                                if(is_array($class_info_array)) {                                                                    
                                                                    if(array_key_exists('semester', $class_info_array)) {
                                                                        echo 'Semester: '.$class_info_array['semester'].', ';
                                                                    }                                                                    
                                                                    if(array_key_exists('section', $class_info_array)) {
                                                                        echo 'Section: '.$class_info_array['section'].', ';
                                                                    }
                                                                    if(array_key_exists('roll_no', $class_info_array)) {
                                                                        echo 'Roll No: '.$class_info_array['roll_no'];
                                                                    }
                                                                } else {
                                                                    echo 'N/A';
                                                                }
                                                            }
                                                        ?>    
                                                    </td>
                                                </tr>
                                    <?php
                                            }
                                        } else {
                                    ?>
                                            <tr>
                                                <td colspan="4" style="text-align: left">
                                                    No data available
                                                </td>
                                            <tr>                                                        
                                    <?php         
                                        }
                                    ?>
                                </tbody> 
                            </table>
                        </div>
                        
                        <hr>
                        
                        <!-- Form to select fees breakup -->
                        <h4>Receive Payment Offline</h4>
                        <?php
                            if(!empty($fees_data)) {                                  

                                echo form_open('school/user/receiveStudentFees/'.$this->uri->segment(4), array('id' => 'frmSelectFeesBreakup', 'class' => 'js-validation-material')); 

                                    $selected = '';
                                    if(isset($breakup_details)) {  
                                        $selected = $breakup_details['id'];
                                    }
                        ?>                        
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select name="fees_breakup" class="form-control" id="class_id" required onchange="this.form.submit()">
                                                <option value="">Select</option>
                                                <?php 
                                                    foreach($fees_data as $semesters) {                                                        
                                                ?>      
                                                        <optgroup label="Semester: <?php echo $semesters['semester_name']; ?>">

                                                            <?php             
                                                                if(!empty($semesters['break_ups'])) {
                                                                    foreach($semesters['break_ups'] as $breakup) { 
                                                            ?>
                                                                        <option value="<?php echo $breakup['id']; ?>" <?php if($breakup['id'] == $selected) { echo 'selected="selected"'; } ?>><?php echo $breakup['breakup_label']; ?></option>
                                                            <?php                                         
                                                                    }  
                                                                } else {
                                                                    $payment_exist = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and student_id="' . $student_details['id'] . '" and school_id="' . $this->session->userdata('school_id') . '" and fees_id="' . $semesters['structure']['id'] . '" and payment_status=1');
                                                                    if($payment_exist > 0) {
                                                                        echo '<option disabled="disabled">All fees have been paid already</option>';
                                                                    } else {
                                                                        echo '<option disabled="disabled">No fees have been set so far</option>';
                                                                    }
                                                                }
                                                            ?>

                                                        </optgroup>
                                                <?php                                         
                                                    }                                                                                                       
                                                ?>
                                            </select>
                                            <label for="fees_breakup">Select Fees Breakup</label>
                                        </div>
                                    </div>                                                     
                        <?php 
                                echo form_close();  
                                                                    
                            } else {
                                
                                echo 'No fees have been set so far';
                            } 
                        ?>                                                
                        
                        <!-- Form to receive fees -->
                        <?php                             
                            if(isset($breakup_details)) {
                                                            
                                echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); 
                        ?> 
                                    <div class="table_responsive">
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: left; width: 70%">Fees Name</th>
                                                    <th style="text-align: right; width: 30%">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $fees = json_decode($breakup_details['fees'], true);
                                                $index = 0;
                                                foreach ($fees as $fees_list) {
                                            ?> 
                                                    <tr>
                                                        <td style="text-align:left">
                                                            <?php echo $fees_list['label']; ?>                                            
                                                            <input type="hidden" name="label[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]" value="<?php echo $fees_list['label']; ?>">
                                                            <input type="hidden" name="option[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]" value="<?php echo $fees_list['option']; ?>">                                                
                                                        </td>
                                                        <td style="text-align:right">
                                                            <?php 
                                                                // Optional fees will have an extra option set to 0. For both single and multiple values
                                                                if($fees_list['option'] == 1) {
                                                                    if( strpos($fees_list['amount'], ",") !== false ) {
                                                                        $fees_array = explode(",", $fees_list['amount']);
                                                                        array_unshift($fees_array, 0);
                                                                    } else {
                                                                        $fees_array = array(0, $fees_list['amount']);
                                                                    }                                                                       
                                                            ?>
                                                                    <select class="amount form-control" name="amount[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]">
                                                                        <?php 
                                                                            foreach($fees_array as $fa) {
                                                                                if($fa == 0 OR (int)$fa > 0) {
                                                                        ?>
                                                                                    <option value="<?php echo $fa; ?>"><?php echo number_format($fa, 2); ?></option>
                                                                        <?php
                                                                                }
                                                                            }
                                                                        ?>    
                                                                    </select>
                                                            <?php 
                                                                }                                                                                                                                                                                                                                                                            
                                                                // Mandatory single fees will show as text, multiple fees will show as dropdown
                                                                else {
                                                                    if( strpos($fees_list['amount'], ",") !== false ) {
                                                                        $fees_array = explode(",", $fees_list['amount']);
                                                            ?>
                                                                        <select class="amount form-control" name="amount[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]">
                                                                            <?php 
                                                                                foreach($fees_array as $fa) {
                                                                                    if($fa == 0 OR (int)$fa > 0) {
                                                                            ?>
                                                                                        <option value="<?php echo $fa; ?>"><?php echo number_format($fa, 2); ?></option>
                                                                            <?php
                                                                                    }
                                                                                }
                                                                            ?>    
                                                                        </select>
                                                            <?php
                                                                    } else {                                                                        
                                                            ?>     
                                                                        <input type="text" class="amount form-control" name="amount[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]" value="<?php echo $fees_list['amount']; ?>">
                                                            <?php 
                                                                    }
                                                                }
                                                            ?>  
                                                        </td>
                                                    </tr>                                         
                                            <?php   
                                                    $index++;
                                                }
                                            ?>
                                                <!-- Fees amount -->        
                                                <tr style="border-top: 3px solid #333">
                                                    <td style="text-align:left">
                                                        <b>Fees Amount</b> 
                                                    </td>  
                                                    <td style="text-align:right">
                                                        <span class="fees_amount_show">

                                                        </span>    
                                                    </td>                                                    
                                                </tr>    
                                                <!-- Late fine -->        
                                                <tr style="border-top: 3px solid #333">
                                                    <td style="text-align:left">
                                                        <b>Late Fine</b> 
                                                    </td>  
                                                    <td style="text-align:right">
                                                        <input type="text" class="form-control late_fine" name="late_fine" value="0">  
                                                    </td>                                                    
                                                </tr>  
                                                <!-- Total amount -->        
                                                <tr style="border-top: 3px solid #333">
                                                    <td style="text-align:left">
                                                        <b>Total Amount</b> 
                                                    </td>  
                                                    <td style="text-align:right">
                                                        <span class="total_amount_show">
                                                            
                                                        </span>    
                                                    </td>                                                    
                                                </tr> 
                                            </tbody>
                                        </table>      
                                        
                                        <div class="form-group">     
                                            <?php 
                                                if(isset($breakup_details['manual_breakup_id'])) {
                                            ?>
                                                    <input type="hidden" name="manual_breakup_id" value="<?php echo $this->my_custom_functions->ablEncrypt($breakup_details['manual_breakup_id']); ?>">                                        
                                            <?php
                                                }
                                            ?>
                                            <input type="hidden" name="parent_id" value="<?php echo $this->my_custom_functions->ablEncrypt($this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'parent_id', 'and student_id="' . $student_details['id'] . '" ')); ?>">
                                            <input type="hidden" name="student_id" value="<?php echo $this->my_custom_functions->ablEncrypt($student_details['id']); ?>">                                            
                                            <input type="hidden" name="breakup_id" class="breakup_id" value="<?php echo $this->my_custom_functions->ablEncrypt($breakup_details['id']); ?>">                                        
                                            <input type="hidden" name="fees_id" class="fees_id" value="<?php echo $this->my_custom_functions->ablEncrypt($breakup_details['fees_id']); ?>">
                                            <input type="hidden" name="fees_amount" class="fees_amount" value="0">                                            
                                            <input type="hidden" name="total_amount" class="total_amount" value="0">
                                            <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                        </div>
                                    </div>
                        <?php 
                                echo form_close(); 
                            }
                        ?>

                    </div>
                </div>
            </div>

        </div>                    
    </div>
                        
    <script type="text/javascript">
        $(document).ready(function() {
            calculate_price();

            $(".amount").on("click change keyup", function() {
                calculate_price();
            }); 
            
            $(".late_fine").on("click change keyup", function() {
                calculate_late_fine();
            }); 
        });

        function calculate_late_fine() {
            
            var fees_amount = $(".fees_amount").val();  
            var late_fine = $(".late_fine").val();
            var total_amount = Number(fees_amount) + Number(late_fine);
                                    
            $(".total_amount").val(total_amount);            
            $(".total_amount_show").text('<?php echo INDIAN_CURRENCY_CODE; ?> '+total_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
        }
        
        function calculate_price() { 

            var fees_amount = 0;  
            var late_fine = $(".late_fine").val();
            $(".amount").each(function() {                                                                                                                                   
                fees_amount += Number($(this).val());                                                                                    
            });     
            
            var fees_data = {
                'fees_amount': fees_amount,                
                'fees_id': $(".fees_id").val(),
                'breakup_id': $(".breakup_id").val(),
            };

            $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>school/user/calculate_fees_amount",
                data:fees_data,
                success:function(data) { console.log(data);
                    if(data != "") {
                        var response = JSON.parse(data);
                        var total_amount = Number(fees_amount) + Number(response.late_fine);
                        
                        $(".fees_amount").val(fees_amount);
                        $(".late_fine").val(response.late_fine);
                        $(".total_amount").val(total_amount);
                        
                        $(".fees_amount_show").text('<?php echo INDIAN_CURRENCY_CODE; ?> '+fees_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));                        
                        $(".total_amount_show").text('<?php echo INDIAN_CURRENCY_CODE; ?> '+total_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    } else {
                        $(".fees_amount").val(fees_amount);
                        $(".late_fine").val(late_fine);
                        $(".total_amount").val(fees_amount);
                        
                        $(".fees_amount_show").text('<?php echo INDIAN_CURRENCY_CODE; ?> '+fees_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));                        
                        $(".total_amount_show").text('<?php echo INDIAN_CURRENCY_CODE; ?> '+fees_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    }
                }
            });                                                                      
        }
    </script>   
          
<?php $this->load->view('school/_include/footer'); ?>