<?php $this->load->view('school/_include/header'); ?>
<!-- END Footer -->
<style>
/*    .invalid-feedback {
        width: 50% !important;
    }*/
</style>
<script type="text/javascript">

    function get_section_list() {
        $('.loader').show();
        var class_id = $('.class').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });


    }

    $(document).ready(function () {
        $('#start_date').datepicker({
            dateFormat: 'dd-mm-yy',
        });
        $('#end_date').datepicker({
            dateFormat: 'dd-mm-yy',
        });
    });
    
    function cap_the_section(val) {
        
        if(val == 1){
            $('.section').prop('required',false);
        }
        if(val == 2){
            
            $('.section').prop('required',true);
        }
    }
</script>
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Attendance Report</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('school/user/generateAttendanceReport', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    <div class="form-group row">
                        <label class="col-12">Type</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="report_type" id="example-inline-radio1" value="1" onclick="cap_the_section('1');">
                                <label class="custom-control-label" for="example-inline-radio1">Class Wise</label>
                            </div>
                            <div  class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="report_type" id="example-inline-radio2" value="2" onclick="cap_the_section('2');"> 
                                <label class="custom-control-label" for="example-inline-radio2">Student Wise</label>
                            </div>


                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material">
                                <select required="" class="form-control class" id="class" name="class" onchange="get_section_list();">

                                    <option value="">Select Class</option>
                                    <?php
                                    $selected = '';
                                    foreach ($class_list as $class) {
//                                        if ($post_data['class_id'] != '') {
//                                            if ($post_data['class_id'] == $class['id']) {
//
//                                                $selected = 'selected="selected"';
//                                            } else {
//
//                                                $selected = '';
//                                            }
//                                        }
                                        ?>
                                        <option value="<?php echo $class['id']; ?>" <?php //echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="material-gridf">Select Class</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material">
                                <select class="form-control section" id="section" name="section">
                                    <option value="">Select Section</option>
                                </select>
                                <label for="section">Select Section</label>
                            </div>
                        </div>
                    </div>

                   


                    <div class="form-group row">
                        <div class="col-6">
                            <div class="form-material">
                                <input required="" type="text" class="form-control" id="start_date" name="start_date" placeholder="Start Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d-m-Y'); ?>">
                                <label for="start_date">Start Date</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-material">
                                <input required="" type="text" class="form-control" id="end_date" name="end_date" placeholder="End Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d-m-Y'); ?>">
                                <label for="end_date">End Date</label>
                            </div>
                        </div>
                    </div>



                   

                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<?php $this->load->view('school/_include/footer'); ?>
           



