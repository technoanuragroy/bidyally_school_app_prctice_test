<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this file?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteIndividualFile/" + id;
    }
</script>


<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Notice</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="noticeloader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php
                    echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material'));
                    //echo "<pre>";print_r($notice_detail);
                    $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', 'and id = "' . $notice_detail['semester_id'] . '"');
                    ?>




                    <div class="form-group row">
                        <label class="col-12">Type</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio1" value="1" onclick="close_all();" <?php
                                if ($notice_detail['type'] == 1) {
                                    echo "checked";
                                }
                                ?> >
                                <label class="custom-control-label" for="example-inline-radio1">All Teachers</label>
                            </div>
                            <div  class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio2" value="2" onclick="close_all();" <?php
                                if ($notice_detail['type'] == 2) {
                                    echo "checked";
                                }
                                ?>>
                                <label class="custom-control-label" for="example-inline-radio2">All parents</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input type_3" type="radio" name="type" id="example-inline-radio3" value="3" <?php
                                if ($notice_detail['type'] == 3) {
                                    echo "checked";
                                }
                                ?> onclick="all_class();">
                                <label class="custom-control-label" for="example-inline-radio3">All parents by class</label>
                            </div>

                        </div>
                    </div>
                    <input type="hidden" name="class_list" class="class_list" value="<?php echo $class_id; ?>">

                    <div class="form-group row full_block">
                        <label class="col-12 sub_type"></label>
                        <div class="col-12 check_lists">


                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="notice_heading" name="notice_heading" placeholder="Notice heading" value="<?php echo $notice_detail['notice_heading']; ?>">
                            <label for="notice_heading">Notice Heading</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="example-textarea-input">Notice</label>
                        <div class="col-12">
                            <textarea class="form-control" id="editor1" name="notice_text" rows="6" placeholder="Content.."><?php echo $notice_detail['notice_text']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="date_of_birth" name="date_of_publish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php
                            //if ($notice_detail['publish_date'] != '0000-00-00') {
                                echo date('d-m-Y',$notice_detail['publish_date']);
                            //}
                            ?>">
                            <label for="date_of_birth">Date of Publish</label>
                        </div>
                    </div>


                    <div class="form-group row">
                        <?php
//                                        echo "<pre>";
//                                        print_r($notice_file_list);

                        if (!empty($notice_file_list)) {
                            foreach ($notice_file_list as $filess) {
                                $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                $path = $filess['file_url'];

                                if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                    $display_path = $path;
                                    $download_path = $path;
                                    $width = '72px';
                                } else if ($ext == 'xls' || $ext == 'xlsx') {
                                    $display_path = base_url() . '_images/xls.jpg';
                                    $download_path = $path;
                                    $width = '100px';
                                } else if ($ext == 'doc' || $ext == 'docx') {
                                    $display_path = base_url() . '_images/doc.png';
                                    $download_path = $path;
                                    $width = '48px';
                                } else if ($ext == 'pdf' || $ext == 'PDF') {
                                    $display_path = base_url() . '_images/pdf.png';
                                    $download_path = $path;
                                    $width = '48px';
                                }else{
                                    $display_path = '';
                                    $download_path = '';
                                    $width = '';
                                }
                                //echo $download_path;die;
                                if($display_path != ''){
                                ?>
                                
                                <div class="col-2" style="text-align:center;">
                                        <!--<a href="<?php //echo $download_path;  ?>" download=""> <img src="<?php //echo $display_path;  ?>" style="width:<?php //echo $width;  ?>"></a>-->
                                    <a href="<?php echo base_url(); ?>school/user/downloadFilenotice/<?php echo $this->my_custom_functions->ablEncrypt($filess['id']); ?>" download=""> <img src="<?php echo $display_path; ?>" style="width:<?php echo $width; ?>"></a>    
                                    <a href="<?php echo base_url(); ?>school/user/deleteIndividualFile/<?php echo $this->my_custom_functions->ablEncrypt($filess['id']); ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $this->my_custom_functions->ablEncrypt($filess['id']);
                        ; ?>');"><i class="fa fa-trash"></i></a>
                                </div>
                                <?php
                                }
                            }
                        }
                        ?>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="example-file-multiple-input">Upload Docs (Multiple)</label>
                        <div class="col-12">
                            <input type="file" id="example-file-multiple-input" name="doc_upload[]" multiple="">
                        </div>
                    </div>


                    <input type="hidden" name="notice_id" value="<?php echo $notice_detail['id']; ?>">
                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                    </div>
<?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
<!-- END Footer -->
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
</script>
<script type="text/javascript">
    function all_class() {
        $('.noticeloader').show();
        $('.sub_type').text('All Classes');

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_class_list_semesterwise",
            //data: "class_id=" + class_id,
            success: function (msg) {
                $('.noticeloader').hide();
                if (msg != "") {
                    $('.check_lists').html(msg);
                    $('.full_block').slideDown('slow');
                    
                    $('#checkboxall').on('click', function () {

                        if (this.checked) {
                            $('.checkboxTeacher').each(function () {
                                
                                this.checked = true;
                            });
                        } else {
                            $('.checkboxTeacher').each(function () {
                                
                                this.checked = false;
                            });
                        }
                    });

                    $('.checkboxTeacher').on('click', function () {
                        if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
                            $('#checkboxall').prop('checked', true);
                        } else {
                            $('#checkboxall').prop('checked', false);
                        }
                    });

                }
            }
        });

    }
    $(document).ready(function () {
        $('#date_of_birth').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#date_of_issue').datepicker({
            dateFormat: 'dd/mm/yy',
        });

        if ($('.type_3').prop('checked') == true) {
            var class_id = $('.class_list').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>school/user/get_class_list_semesterwise",
                data: "class_id=" + class_id,
                success: function (msg) {
                    //alert(msg);
                    if (msg != "") {
                        $('.check_lists').html(msg);
                        $('.checkboxTeacher').each(function () {
                                
                                $(this).attr('disabled','true');
                            });
                            $('#checkboxall').attr('disabled','true');
                            
                        $('#checkboxall').on('click', function () {

                            if (this.checked) {
                                $('.checkboxTeacher').each(function () {
                                    this.checked = true;
                                });
                            } else {
                                $('.checkboxTeacher').each(function () {
                                    this.checked = false;
                                });
                            }
                        });

                        $('.checkboxTeacher').on('click', function () {
                            if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
                                $('#checkboxall').prop('checked', true);
                            } else {
                                $('#checkboxall').prop('checked', false);
                            }
                        });

                    }
                }
            });
        }


    });
    function close_all() {
        $('#checkboxall').prop('checked', false);
        $('.checkboxTeacher').prop('checked', false);
        $('.full_block').slideUp('slow');
    }
</script>



