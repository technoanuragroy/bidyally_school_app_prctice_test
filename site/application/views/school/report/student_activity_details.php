<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">
    .table-vcenter thead tr th {        
        font-weight: bold;
        text-align: center;
    }
</style>
<script type="text/javascript">
    
</script>

<!-- Page Content -->
<div class="content">
    <?php 
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4)); 
        $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $student_id . '" '); 
    ?>
    <h2 class="content-heading">Student Activity Details for <?php echo $student_name; ?></h2>

    <!-- Dynamic Table Full -->
    <div class="block">        
        <div class="block-content">
            
            <div class="form-group">                
                <a href="<?php echo base_url(); ?>school/report/studentReport" class="btn btn-outline-danger">Back</a>                
            </div>
            
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                                                
                    <?php echo form_open('school/report/studentActivityDetails/'.$this->uri->segment(4), array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>                                   

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material">
                                        <select class="form-control teacher_id" id="teacher_id" name="teacher_id">
                                            <option value="">Select Teacher</option>
                                            <?php 
                                                foreach ($details['teachers'] as $teacher) { 
                                                    
                                                    $selected = '';
                                                    if($this->input->post('teacher_id')) {
                                                        if($teacher['id'] == $this->input->post('teacher_id')) {
                                                            $selected = 'selected="selected"';
                                                        }
                                                    }
                                            ?>
                                                    <option value="<?php echo $teacher['id']; ?>" <?php echo $selected; ?>><?php echo $teacher['name']; ?></option>
                                            <?php                                             
                                                } 
                                            ?>
                                        </select>
                                        <label for="teacher_id">Select Teacher</label>
                                    </div>                                    
                                </div> 
                            </div>     
                    
                            <input type="submit" name="submit" value="Search" class="btn btn-primary">

                    <?php echo form_close(); ?>
                    
                </div>
            </div>
            
            <div class="table_responsive">
                                   
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>      
                            <th>Date</th>
                            <th>Subject</th>        
                            <th>Teacher Name</th>
                            <th>Diary Heading</th>
                            <th>Diary Note</th>                                                                                                 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(!empty($details['diary_notes'])) {   
                                foreach($details['diary_notes'] as $note) {                                                                                   
                        ?>
                                    <tr>
                                        <td width="170"><?php echo date("d-m-Y, g:ia", $note['issue_date']); ?></td> 
                                        <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, "subject_name", " and id='" . $note['subject_id'] . "'"); ?></td> 
                                        <td><?php echo $note['teacher_name']; ?></td>                                                 
                                        <td><?php echo $note['heading']; ?></td> 
                                        <td><?php echo $note['diary_note']; ?></td> 
                                    </tr>
                        <?php                                        
                                }
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="5">
                                        No record found
                                    </td>    
                                </tr>
                        <?php      
                            }
                        ?> 
                    </tbody>
                </table>
                
            </div>      
                    
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>