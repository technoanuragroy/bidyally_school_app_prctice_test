<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">
    .table-vcenter thead tr th {        
        font-weight: bold;
        text-align: center;
    }
</style>    
<script type="text/javascript">
    $(document).ready(function($) {
        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>

<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Student Activity Report for <?php echo $classSection; ?></h2>

    <!-- Dynamic Table Full -->
    <div class="block">        
        <div class="block-content block-content-full">

            <div class="form-group">                
                <a href="<?php echo base_url(); ?>school/report/studentReport" class="btn btn-outline-danger">Back</a>                
            </div>
            
            <div class="table_responsive">
                                   
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th>Student name</th>
                            <th>Diary Count</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(!empty($diary_count_list)) {
                                foreach($diary_count_list as $student_id => $diary_count) {
                        ?>
                                    <tr style="cursor:pointer;" class="clickable-row" data-href="<?php echo base_url(); ?>school/report/studentActivityDetails/<?php echo $this->my_custom_functions->ablEncrypt($student_id); ?>">
                                        <td>
                                            <?php echo $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $student_id . '"') ?>
                                        </td>
                                        <td>
                                            <?php echo $diary_count; ?>                                            
                                        </td>
                                        <td>                                            
                                            <span class="attn_dir_arrow"><i class="fas fa-arrow-right"></i></span>
                                        </td>
                                    </tr>
                        <?php
                                }
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="3">
                                        No record found
                                    </td>    
                                </tr>
                        <?php      
                            }
                        ?>
                    </tbody>
                </table>
            
            </div>
            
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
        

