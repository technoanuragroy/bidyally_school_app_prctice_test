<?php $this->load->view('school/_include/header'); ?>
            
    <link href="<?php echo base_url(); ?>_css/new_version/jquery-ui.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>_css/jquery.fancybox.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>_css/jquery.dropdown.css" rel="stylesheet" type="text/css"/>

<!--    <script src="<?php echo base_url(); ?>js/jquery.tablesorter.js" type="text/javascript"></script>-->
    <script src="<?php echo base_url(); ?>_js/new_version/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>_js/jquery.fancybox.js" type="text/javascript"></script>      
    <script src="<?php echo base_url(); ?>_js/jquery.dropdown.js" type="text/javascript"></script>
    
    <style type="text/css">
        .maps {
            height: 90%;
            width: 90%;
        }
        .map_canvas {
            height: 100%;
            width: 100%;
        }
        a.fancybox:focus {
            outline: none !important;
        }    
        .header {
            width: auto;
            float: none;
        }        
        .plus_class {
            background: url("../../_images/plus-icon.png") no-repeat center center;
            display: block;
            width: 32px;
            height: 32px;
        }
        .minus_class {
            background: url("../../_images/minus-icon.png") no-repeat center center !important;
        }
        .breakup_location {
            display: block; width: 100%; float: left; margin-top: 10px;
        }    
        table.breakup-table tbody tr td {
            vertical-align: top;
        } 
    </style>
    
    <script type="text/javascript">
        $(document).ready(function() {            
            $(".datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd"
            });

            //$(".attendance-table").tablesorter(); 
            
            $(".fancybox").fancybox({
                //touch: false
            });

            // Dropdown search 
            $('.dropdown-sin-1').dropdown({
                readOnly: true,
                input: '<input type="text" maxLength="20" placeholder="Search">'
            });

            $(".breakup_attendance").hide();       

            var background_color = "";
            $(".table-striped02>tbody>tr").each(function() {
                if(!$(this).hasClass("breakup_attendance")) {

                    if($(this).data("bgcolor") != "") {
                        $(this).css("background-color", $(this).data("bgcolor"));
                    } else {
                        $(this).css("background-color", background_color);
                    }

                    if(background_color == "") {
                        background_color = "#f9f9f9";
                    } else {
                        background_color = "";
                    }
                }
            })
        });   

        function show_attendace_breakup(row_id, e) {

            $(".breakup_attendance_"+row_id).slideToggle();
            $(e).toggleClass("minus_class");
        }
    </script>

    <!-- Page Content -->
    <div class="content">

        <!-- Material Forms Validation -->
        <h2 class="content-heading">Teacher Attendance Report</h2>
        
        <div class="block">                        
            <div class="block-content">
                                    
                <div class="form-group">                
                    <a href="<?php echo base_url(); ?>school/report/teacherReport" class="btn btn-outline-danger">Back</a>                
                </div>
                
                <div class="table_responsive">  
                    <?php                                                                    
                        if($this->input->post('submit')) { 
                            if(isset($result) && count($result) > 0) {
                    ?>
                                <div style="display: block; width: 100%;">
                                    <div style="width: 20px; height: 20px; display: inline-block; background-color: #d4d4fc;"></div> <span style="vertical-align: top;">Holiday</span> &nbsp;
                                    <div style="width: 20px; height: 20px; display: inline-block; background-color: #fcd4d4;"></div> <span style="vertical-align: top;">Sunday</span>
                                </div>
                    <?php   
                            }

                            // For multiple teachers
                            if(count($this->session->userdata('tcrrpt_multiple_teacher')) != 1) { 
                    ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full table-striped02 attendance-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th>Duration</th>
                                            <th>Break Up</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php                                                                        
                                            if(isset($result) && count($result) > 0) {                                                        

                                                foreach($result as $key => $attendances) {

                                                    foreach($attendances as $teacherId => $att) {

                                                        $mapKey = $teacherId.'_'.strtotime($key);

                                                        $bg_color = '';
                                                        if($att['holiday'] == 1) {
                                                            $bg_color = ATTENDANCE_REPORT_HOLIDAY_BGCOLOR;
                                                        } else if($att['holiday'] == 2) {
                                                            $bg_color = ATTENDANCE_REPORT_SUNDAY_BGCOLOR;
                                                        }
                                        ?>
                                                        <tr data-bgcolor="<?php echo $bg_color; ?>">
                                                            <td width="30%"><?php echo $att['name']; ?></td>
                                                            <td width="15%"><?php echo date("D - d M, Y", strtotime($key)); ?></td>
                                                            <td width="15%">
                                                                <?php
                                                                    echo $att['start_time'];
                                                                    $slat = "";
                                                                    $slon = "";
                                                                    if(array_key_exists('start_lat', $att) AND array_key_exists('start_lon', $att)) {
                                                                        $slat = $att['start_lat'];
                                                                        $slon = $att['start_lon'];

                                                                        echo '&nbsp;<a href="#map_start_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                        echo '<div class="maps" id="map_start_'.$mapKey.'" data-id="map_canvas_start_'.$mapKey.'" data-lat="'.$slat.'" data-lon="'.$slon.'" style="display:none;"><div class="map_canvas" id="map_canvas_start_'.$mapKey.'"></div></div>';
                                                                    }                                                                            
                                                                ?>
                                                            </td>
                                                            <td width="15%">
                                                                <?php
                                                                    echo $att['end_time'];
                                                                    $elat = "";
                                                                    $elon = "";
                                                                    if(array_key_exists('end_lat', $att) AND array_key_exists('end_lon', $att)) {
                                                                        $elat = $att['end_lat'];
                                                                        $elon = $att['end_lon'];

                                                                        echo '&nbsp;<a href="#map_end_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                        echo '<div class="maps" id="map_end_'.$mapKey.'" data-id="map_canvas_end_'.$mapKey.'" data-lat="'.$elat.'" data-lon="'.$elon.'" style="display:none;"><div class="map_canvas" id="map_canvas_end_'.$mapKey.'"></div></div>';
                                                                    }                                                                            
                                                                ?>
                                                            </td>
                                                            <td width="15%"><?php echo $att['duration']; ?></td>
                                                            <td width="10%">
                                                                <?php 
                                                                    if(array_key_exists("break_up", $att)) {
                                                                ?>
                                                                        <a href="javascript:;" onclick="show_attendace_breakup('<?php echo $mapKey; ?>', this);" class="plus_class"></a>
                                                                <?php
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>

                                                        <!-- Break up attendance table -->
                                                        <?php 
                                                            if(array_key_exists("break_up", $att)) {
                                                        ?>
                                                                <tr class="breakup_attendance breakup_attendance_<?php echo $mapKey; ?>">
                                                                    <td colspan="6">
                                                                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full attendance-table" border="1">
                                                                            <thead>
                                                                                <tr>                                                                                                                                                                                                
                                                                                    <th>Start Time</th>
                                                                                    <th>End Time</th>
                                                                                    <th>Duration</th>                                                                                                
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php 
                                                                                    foreach($att['break_up'] as $mapKey => $brkp) { 
                                                                                ?>
                                                                                        <tr>
                                                                                            <td width="35%">
                                                                                                <?php
                                                                                                    echo $brkp['start_time'];
                                                                                                    $slat = "";
                                                                                                    $slon = "";
                                                                                                    if(array_key_exists('start_lat', $brkp) AND array_key_exists('start_lon', $brkp)) {
                                                                                                        $slat = $brkp['start_lat'];
                                                                                                        $slon = $brkp['start_lon'];

                                                                                                        echo '&nbsp;<a href="#map_start_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                                                        echo '<div class="maps" id="map_start_'.$mapKey.'" data-id="map_canvas_start_'.$mapKey.'" data-lat="'.$slat.'" data-lon="'.$slon.'" style="display:none;"><div class="map_canvas" id="map_canvas_start_'.$mapKey.'"></div></div>';
                                                                                                    }                                                                                                            
                                                                                                ?>
                                                                                            </td>
                                                                                            <td width="35%">
                                                                                                <?php
                                                                                                    echo $brkp['end_time'];
                                                                                                    $elat = "";
                                                                                                    $elon = "";
                                                                                                    if(array_key_exists('end_lat', $brkp) AND array_key_exists('end_lon', $brkp)) {
                                                                                                        $elat = $brkp['end_lat'];
                                                                                                        $elon = $brkp['end_lon'];

                                                                                                        echo '&nbsp;<a href="#map_end_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                                                        echo '<div class="maps" id="map_end_'.$mapKey.'" data-id="map_canvas_end_'.$mapKey.'" data-lat="'.$elat.'" data-lon="'.$elon.'" style="display:none;"><div class="map_canvas" id="map_canvas_end_'.$mapKey.'"></div></div>';
                                                                                                    }                                                                                                            
                                                                                                ?>
                                                                                            </td>
                                                                                            <td width="30%"><?php echo $brkp['duration']; ?></td>
                                                                                        </tr>    
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </tbody>   
                                                                        </table>    
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        ?>
                                        <?php
                                                    }
                                                }                                                      
                                            } else {
                                        ?>
                                                <tr><td colspan="6" align="center">No record found</td></tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>    
                    <?php         
                            // For single teacher
                            } else if(count($this->session->userdata('tcrrpt_multiple_teacher')) == 1) { 
                    ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full table-striped02 attendance-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th>Duration</th>
                                            <th>Break Up</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php                                                                        
                                            if(isset($result) && count($result) > 0) {

                                                $total_days = count($result);
                                                $total_presents = 0;
                                                $total_absents = 0;                                                        
                                                $holidays = 0;
                                                $sundays = 0;

                                                foreach($result as $key => $attendances) {

                                                    foreach($attendances as $teacherId => $att) {

                                                        $mapKey = $teacherId.'_'.strtotime($key);

                                                        $bg_color = '';
                                                        if($att['holiday'] == 1) {
                                                            $bg_color = ATTENDANCE_REPORT_HOLIDAY_BGCOLOR;
                                                        } else if($att['holiday'] == 2) {
                                                            $bg_color = ATTENDANCE_REPORT_SUNDAY_BGCOLOR;
                                                        }
                                        ?>
                                                        <tr data-bgcolor="<?php echo $bg_color; ?>">
                                                            <td width="30%"><?php echo $att['name']; ?></td>
                                                            <td width="15%"><?php echo date("D - d M, Y", strtotime($key)); ?></td>
                                                            <td width="15%">
                                                                <?php
                                                                    echo $att['start_time'];
                                                                    $slat = "";
                                                                    $slon = "";
                                                                    if(array_key_exists('start_lat', $att) AND array_key_exists('start_lon', $att)) {
                                                                        $slat = $att['start_lat'];
                                                                        $slon = $att['start_lon'];

                                                                        echo '&nbsp;<a href="#map_start_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                        echo '<div class="maps" id="map_start_'.$mapKey.'" data-id="map_canvas_start_'.$mapKey.'" data-lat="'.$slat.'" data-lon="'.$slon.'" style="display:none;"><div class="map_canvas" id="map_canvas_start_'.$mapKey.'"></div></div>';
                                                                    }                                                                            
                                                                ?>
                                                            </td>
                                                            <td width="15%">
                                                                <?php
                                                                    echo $att['end_time'];
                                                                    $elat = "";
                                                                    $elon = "";
                                                                    if(array_key_exists('end_lat', $att) AND array_key_exists('end_lon', $att)) {
                                                                        $elat = $att['end_lat'];
                                                                        $elon = $att['end_lon'];

                                                                        echo '&nbsp;<a href="#map_end_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                        echo '<div class="maps" id="map_end_'.$mapKey.'" data-id="map_canvas_end_'.$mapKey.'" data-lat="'.$elat.'" data-lon="'.$elon.'" style="display:none;"><div class="map_canvas" id="map_canvas_end_'.$mapKey.'"></div></div>';
                                                                    }                                                                            
                                                                ?>
                                                            </td>
                                                            <td width="15%"><?php echo $att['duration']; ?></td>
                                                            <td width="10%">
                                                                <?php 
                                                                    if(array_key_exists("break_up", $att)) {
                                                                ?>
                                                                        <a href="javascript:;" onclick="show_attendace_breakup('<?php echo $mapKey; ?>', this);" class="plus_class"></a>
                                                                <?php
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>

                                                        <!-- Break up attendance table -->
                                                        <?php 
                                                            if(array_key_exists("break_up", $att)) {
                                                        ?>
                                                                <tr class="breakup_attendance breakup_attendance_<?php echo $mapKey; ?>">
                                                                    <td colspan="6">
                                                                        <table class="table table-bordered table-striped table-vcenter js-dataTable-full attendance-table breakup-table" border="1">
                                                                            <thead>
                                                                                <tr>                                                                                                                                                                                                
                                                                                    <th>Start Time</th>
                                                                                    <th>End Time</th>
                                                                                    <th>Duration</th>                                                                                                
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php 
                                                                                    foreach($att['break_up'] as $mapKey => $brkp) { 
                                                                                ?>
                                                                                        <tr>
                                                                                            <td width="35%">
                                                                                                <?php
                                                                                                    echo '<div class="breakup_top">'.$brkp['start_time'];
                                                                                                            $slat = "";
                                                                                                            $slon = "";
                                                                                                            if(array_key_exists('start_lat', $brkp) AND array_key_exists('start_lon', $brkp)) {
                                                                                                                $slat = $brkp['start_lat'];
                                                                                                                $slon = $brkp['start_lon'];

                                                                                                                echo '&nbsp;<a href="#map_start_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                                                                echo '<div class="maps breakup_maps" id="map_start_'.$mapKey.'" data-id="map_canvas_start_'.$mapKey.'" data-lat="'.$slat.'" data-lon="'.$slon.'" data-locationkey="start_breakup_location_'.$mapKey.'" style="display:none;"><div class="map_canvas" id="map_canvas_start_'.$mapKey.'"></div></div>';
                                                                                                            }                                                                                                                    
                                                                                                    echo '</div>';        
                                                                                                ?>
                                                                                                <span class="breakup_location">Place: <span class="start_breakup_location_<?php echo $mapKey; ?>">N/A</span></span>
                                                                                            </td>
                                                                                            <td width="35%">
                                                                                                <?php
                                                                                                    echo '<div class="breakup_top">'.$brkp['end_time'];
                                                                                                            $elat = "";
                                                                                                            $elon = "";
                                                                                                            if(array_key_exists('end_lat', $brkp) AND array_key_exists('end_lon', $brkp)) {
                                                                                                                $elat = $brkp['end_lat'];
                                                                                                                $elon = $brkp['end_lon'];

                                                                                                                echo '&nbsp;<a href="#map_end_'.$mapKey.'" class="fancybox" title="View on map"><img src="'.base_url().'_images/marker.png" alt="Marker" width="22px;"></a>';
                                                                                                                echo '<div class="maps breakup_maps" id="map_end_'.$mapKey.'" data-id="map_canvas_end_'.$mapKey.'" data-lat="'.$elat.'" data-lon="'.$elon.'" data-locationkey="end_breakup_location_'.$mapKey.'" style="display:none;"><div class="map_canvas" id="map_canvas_end_'.$mapKey.'"></div></div>';
                                                                                                            }                                                                                                                    
                                                                                                    echo '</div>';             
                                                                                                ?>
                                                                                                <span class="breakup_location">Place: <span class="end_breakup_location_<?php echo $mapKey; ?>">N/A</span></span>
                                                                                            </td>
                                                                                            <td width="30%"><?php echo $brkp['duration']; ?></td>
                                                                                        </tr>    
                                                                                <?php
                                                                                    }
                                                                                ?>
                                                                            </tbody>   
                                                                        </table>    
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        ?>
                                        <?php
                                                        if($att['start_time'] == "-" AND $att['end_time'] == "-") {
                                                            $total_absents++;
                                                        } else {
                                                            $total_presents++;
                                                        }   

                                                        if($att['holiday'] == 1) { 
                                                            $holidays++;
                                                        } else if($att['holiday'] == 2) { 
                                                            $sundays++;
                                                        }
                                                    }
                                                }
                                        ?>        
                                                <tr>
                                                    <td><b>Total Days : <?php echo $total_days; ?></b></td>
                                                    <td colspan="2"><b>Total Presents : <?php echo $total_presents; ?></b></td>
                                                    <td colspan="2"><b>Total Absents : <?php echo $total_absents; ?> (Including Holidays)</b></td>
                                                    <td>
                                                        <b>Holidays : <?php echo $holidays; ?></b><br>
                                                        <b>Sundays : <?php echo $sundays; ?></b>
                                                    </td>
                                                </tr>    
                                        <?php        
                                            } else {
                                        ?>
                                                <tr><td colspan="6" align="center">No record found</td></tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                    <?php            
                            }
                        } 
                    ?>
                </div>  
                            
                <?php                                 
                    if(isset($result) && count($result) > 0) {
                ?>
                        <hr style="height:1px; background-color: #cad7de;">
                        <div class="form-group full-col"><br>
                            <h4 class="Heading03 MBheadng03">Download Attendance(s) in Excel:
                                <?php 
                                    if($this->session->userdata('tcrrpt_multiple_teacher')) {
                                        $multiple_teacher = json_encode($this->session->userdata('tcrrpt_multiple_teacher'), true);
                                    } else {
                                        $multiple_teacher = "";
                                    }

                                    $encrypted_data = $this->my_custom_functions->ablEncrypt($multiple_teacher.'_'.$this->session->userdata("tcrrpt_start_date").'_'.$this->session->userdata("tcrrpt_end_date"));
                                ?>
                                <a href="<?php echo base_url()."school/report/downloadTeacherAttendances/".$encrypted_data; ?>" title="Download">
                                    <img src="<?php echo base_url() ?>_images/download-excel.png" alt="Download" class="csv_img">
                                </a>
                            </h4>
                        </div>
                <?php 
                    }                               
                ?>                        
            </div>
        </div>
    </div>        
            
    <script src="//maps.googleapis.com/maps/api/js?v=3.exp&key=<?php echo GOOGLE_MAP_API_KEY; ?>"></script>

    <script type="text/javascript">
        var geocoder;

        $(document).on('afterShow.fb', function( e, instance, slide ) {

            var image = new google.maps.MarkerImage('<?php echo base_url(); ?>_images/checkin-marker.png');
            var myLatLng = {lat: $(slide.src).data("lat"), lng: $(slide.src).data("lon")};

            var mappop = new google.maps.Map(document.getElementById($(slide.src).data("id")), {
                zoom: 14,
                center: myLatLng
            });

            var markerpop = new google.maps.Marker({
                position: myLatLng,
                map: mappop,
                icon: image
            });

            $(".fancybox-close-small").css("outline", "none");
        });

        $(document).ready(function() {
            
            geocoder = new google.maps.Geocoder();   

            // Generate location on break up attendance section
            $(".breakup_maps").each(function() {
                var lat = $(this).data("lat");
                var lon = $(this).data("lon");
                var location_key = $(this).data("locationkey");

                geocoder.geocode({
                    latLng: {lat:lat, lng:lon},
                }, function(responses) {                
                    //console.log(responses);
                    //console.log("."+location_key);
                    if (responses && responses.length > 0) {                       
                        $("."+location_key).text(responses[0].formatted_address);                       
                    } else {
                        $("."+location_key).text("Location can not be recognized.");
                    }
                });
            });
        });
    </script>

<?php $this->load->view('school/_include/footer'); ?>
