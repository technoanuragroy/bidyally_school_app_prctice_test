<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">    
    .table-vcenter thead tr th {        
        font-weight: bold;
        text-align: center;
    }
</style>

<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Class-wise Attendance Report for <?php echo $classSection; ?></h2>

    <!-- Dynamic Table Full -->
    <div class="block">                        
        <div class="block-content block-content-full">
            
            <div class="form-group">                
                <a href="<?php echo base_url(); ?>school/report/studentReport" class="btn btn-outline-danger">Back</a>                
            </div>
            
            <div class="table_responsive">
            
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <?php 
                                if(!empty($report_classwise['section_list'])) {
                                    foreach($report_classwise['section_list'] as $class_section) {    

                                        $class_sec_split = explode('_',$class_section);
                                        $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $class_sec_split[0] . '"');
                                        $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $class_sec_split[1] . '"');
                            ?>
                                        <th style="text-align:center;font-weight: bold"><?php echo $class_name.$section_name; ?></th>
                            <?php                         
                                    }                            
                                } 
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                    
                            if (!empty($report_classwise['attendance'])) {
                                foreach ($report_classwise['attendance'] as $attendance_date => $row) {                            
                        ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d-m-Y, g:ia',strtotime($attendance_date)); ?>
                                        </td>
                                        <?php                                 
                                            foreach($report_classwise['section_list'] as $class_section) {
                                        ?>
                                                <td style="text-align:center;">
                                                    <?php
                                                        if(array_key_exists($class_section, $row)) {
                                                            echo number_format($row[$class_section],2).'%';
                                                        } else {
                                                            echo 0.00.'%';
                                                        }
                                                    ?>
                                                </td>
                                        <?php
                                            } 
                                        ?>                                                                
                                    </tr>
                        <?php
                                }
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="99">
                                        No record found
                                    </td>    
                                </tr>
                        <?php      
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>    
            
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
        

