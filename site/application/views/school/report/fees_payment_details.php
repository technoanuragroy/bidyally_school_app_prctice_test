<?php $this->load->view('school/_include/header'); ?>    
    
    <!-- Page Content -->
    <div class="content">

        <!-- Material Forms Validation -->
        <h2 class="content-heading">Fees Breakup</h2>
        
        <div class="block">  
            <div class="block-header block-header-default">
                <a href="<?php echo base_url(); ?>school/report/feesPaymentReport"class="btn btn-primary">Back</a>
            </div>
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-12 load">
                                                                                               
                        <h4>Payment Details</h4>    
                        
                        <div class="table_responsive">
                            <table class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>                            
                                        <th>Student name</th>
                                        <th>Semester Info</th>    
                                        <th>Fees For</th>
                                        <th>Payment Mode</th>
                                        <th>Date Paid</th>
                                        <th style="text-align:right">Amount</th>                                                                                                       
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(!empty($payment_details)) {
                                                                                    
                                        $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id="' . $payment_details['student_id'] . '" ');
                                        $fees_breakup_label = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE_BREAKUPS, 'breakup_label', 'and id="' . $payment_details['fees_breakup_id'] . '" ');

                                        $class_info = "";
                                        if($payment_details['class_info'] != "") {
                                            $class_info_array = json_decode($payment_details['class_info'], true);
                                            if(is_array($class_info_array)) {                                                
                                                if(array_key_exists('semester', $class_info_array)) {
                                                    $class_info .= 'Semester: '.$class_info_array['semester'].', ';
                                                }                                                
                                                if(array_key_exists('section', $class_info_array)) {
                                                    $class_info .= 'Section: '.$class_info_array['section'].', ';
                                                }
                                                if(array_key_exists('roll_no', $class_info_array)) {
                                                    $class_info .= 'Roll No: '.$class_info_array['roll_no'];
                                                }
                                            }
                                        }
                                        $class_info = ($class_info == "") ? 'N/A' : $class_info;

                                        $paid_by = 'N/A';
                                        if($payment_details['created_by'] == SCHOOL) {
                                            $paid_by = 'Offline';
                                        } else if($payment_details['created_by'] == PARENTS) {
                                            $paid_by = 'Online';
                                        }                                                                                        
                            ?>
                                        <tr>
                                            <td><?php echo $student_name; ?></td>
                                            <td><?php echo $class_info; ?></td>
                                            <td><?php echo $fees_breakup_label; ?></td>
                                            <td><?php echo $paid_by; ?></td>
                                            <td><?php echo date("d-m-Y, g:ia", strtotime($payment_details['payment_datetime'])); ?></td>
                                            <td style="text-align:right">
                                                <?php 
                                                    $paid_amount = $payment_details['paid_amount'];
                                                    $payment_handling_fees = $payment_details['service_charges'];
                                                    $display_amount = $paid_amount - $payment_handling_fees;
                                                    echo INDIAN_CURRENCY_CODE.' '.number_format($display_amount, 2); 
                                                ?>
                                            </td>                                            
                                        </tr>
                                <?php                                                 
                                    } else { 
                                ?>
                                        <tr>
                                            <td colspan="7">No payment records</td>
                                        </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        
                        <h4>Fees Breakup</h4>                                                          
                
                        <div class="table_responsive">
                            <table class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>                            
                                        <th>Fees Name</th>
                                        <th style="text-align:right">Amount</th>                                                                
                                    </tr>
                                </thead>
                                <tbody>
                                <?php          
                                    $fees_array = json_decode($payment_details['fees'], true);
                                    if(is_array($fees_array)) {
                                        foreach($fees_array as $fees) {
                                ?>
                                            <tr>
                                                <td><?php echo $fees['label']; ?></td>
                                                <td style="text-align:right"><?php echo INDIAN_CURRENCY_CODE.' '.number_format($fees['amount'], 2); ?></td>
                                            </tr>
                                <?php
                                        }
                                ?>
                                        <!-- Actual fees amount -->   
                                        <tr style="border-top: 3px solid #333">
                                            <td style="text-align:left">
                                                <b>Fees Amount</b> 
                                            </td>  
                                            <td style="text-align:right">
                                                <span class="fees_amount">
                                                    <?php echo INDIAN_CURRENCY_CODE . number_format($payment_details['fees_amount'], 2); ?>
                                                </span>    
                                            </td>                                                    
                                        </tr>

                                        <!-- Late fine amount if any -->
                                        <tr style="border-top: 3px solid #333">
                                            <td style="text-align:left">
                                                <b>Late Fine</b> 
                                            </td>  
                                            <td style="text-align:right">
                                                <span class="fees_amount">
                                                    <?php echo INDIAN_CURRENCY_CODE . number_format($payment_details['late_fine'], 2); ?>
                                                </span>    
                                            </td>                                                    
                                        </tr>

                                        <?php /*
                                        <!-- Payment handling fees(if charged from customer) --> 
                                        <?php if($payment_settings['add_on_school_fees'] > 0 AND $payment_details['service_charges'] > 0) { ?>                                                                                                                                                               
                                                <tr style="border-top: 3px solid #333">
                                                    <td style="text-align:left">
                                                        <b>Payment Handling Fees</b> 
                                                    </td>  
                                                    <td style="text-align:right">
                                                        <span class="fees_amount">
                                                            <?php echo INDIAN_CURRENCY_CODE . number_format($payment_details['service_charges'], 2); ?>
                                                        </span>    
                                                    </td>                                                    
                                                </tr>                                                      
                                        <?php } ?>  
                                        */ ?>

                                        <!-- Total payable amount -->   
                                        <tr style="border-top: 3px solid #333">
                                            <td style="text-align:left">
                                                <b>Paid Amount</b> 
                                            </td>  
                                            <td style="text-align:right">
                                                <span class="fees_amount">
                                                    <?php echo INDIAN_CURRENCY_CODE . number_format($display_amount, 2); ?>
                                                </span>    
                                            </td>                                                    
                                        </tr>       
                                <?php            
                                    } else { 
                                ?>
                                        <tr>
                                            <td colspan="2">No payment details</td>
                                        </tr>
                                <?php
                                    }
                                ?>                                
                                </tbody>
                            </table>    
                                                                                
                        </div>
           
                    </div>
                </div>
            </div>
            
        </div>        
    </div>
    
<?php $this->load->view('school/_include/footer'); ?>
           



