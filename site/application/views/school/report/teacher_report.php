<?php $this->load->view('school/_include/header'); ?>

<link href="<?php echo base_url(); ?>_css/jquery.dropdown.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>_js/jquery.dropdown.js" type="text/javascript"></script>

<script type="text/javascript">
    function get_section_list() {
        
        $('.loader').show();
        var class_id = $('.class').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loader').hide();
                if (msg != "") {                    
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });
    }        

    $(document).ready(function () {
        $('#start_date').datepicker({
            dateFormat: 'dd-mm-yy',
        });
        $('#end_date').datepicker({
            dateFormat: 'dd-mm-yy',
        });
        
        // Dropdown search 
        $('.dropdown-sin-1').dropdown({
            readOnly: true,
            input: '<input type="text" maxLength="20" placeholder="Search">'
        });
        
        $('.class').prop('required', false);
    });
    
    function cap_the_report(val) {
        
        // Attendance report
        if(val == 1) {            
            $('.attendance_report_container').slideDown();
            $('.activity_report_container').slideUp();
            $('.class').prop('required', false);
        }
        // Activity report
        if(val == 2) {   
            $('.activity_report_container').slideDown();
            $('.attendance_report_container').slideUp();
            $('.class').prop('required', true);
        }
    }        
</script>

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Teacher Report</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    
                    <?php echo form_open('school/report/generateTeacherReport', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                            
                            <div class="form-group row">
                                <label class="col-12">See Report</label>
                                <div class="col-12">
                                    <div class="custom-control custom-radio custom-control-inline mb-5">
                                        <input required="" class="custom-control-input" type="radio" name="see_report" id="example-inline-radio3" value="1" onclick="cap_the_report('1');" checked="checked">
                                        <label class="custom-control-label" for="example-inline-radio3">Attendance Report</label>
                                    </div>
                                    <div  class="custom-control custom-radio custom-control-inline mb-5">
                                        <input required="" class="custom-control-input" type="radio" name="see_report" id="example-inline-radio4" value="2" onclick="cap_the_report('2');"> 
                                        <label class="custom-control-label" for="example-inline-radio4">Activity Report</label>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="attendance_report_container">
                                <div class="multiple_container">                                        
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <label for="multiple_teacher">Select Teachers</label>  
                                            <div class="dropdown-sin-1">
                                                <select name="multiple_teacher[]" id="multiple_teacher" class="form-control" multiple="multiple" style="display: none;" placeholder="--Select Teachers--">     
                                                    <?php
                                                        foreach($teacher_list as $teacher) {
                                                            if($this->input->post('multiple_teacher') AND in_array($teacher['id'], $this->input->post('multiple_teacher'))) {
                                                                echo '<option value="'. $teacher['id'] .'" selected="selected">'. $teacher['name']. '</option>';
                                                            } else {
                                                                echo '<option value="'. $teacher['id'] .'">'. $teacher['name']. '</option>';
                                                            }
                                                        }
                                                    ?>
                                                </select>                                                          
                                            </div>   
                                        </div>    
                                    </div>                                                                                                                        
                                </div>
                            </div>
                                 
                            <div class="activity_report_container" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="form-material">
                                            <select required="" class="form-control class" id="class" name="class" onchange="get_section_list();">
                                                <option value="">Select Class</option>
                                                <?php                                                
                                                    foreach ($class_list as $class) {        
                                                ?>
                                                        <option value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?></option>
                                                <?php                                             
                                                    } 
                                                ?>
                                            </select>
                                            <label for="class">Select Class</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <div class="form-material">
                                            <select class="form-control section" id="section" name="section">
                                                <option value="">Select Section</option>
                                            </select>
                                            <label for="section">Select Section</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-6">
                                        <div class="form-material">
                                            <select class="form-control teacher_id" id="teacher_id" name="teacher_id">
                                                <option value="">Select Teacher</option>
                                                <?php
                                                    foreach ($teacher_list as $teacher) {
                                                ?>
                                                        <option value="<?php echo $teacher['id']; ?>"><?php echo $teacher['name']; ?></option>
                                                <?php                                             
                                                    } 
                                                ?>
                                            </select>
                                            <label for="teacher_id">Select Teacher</label>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-material">
                                            <select class="form-control note_type" id="note_type" name="note_type">
<!--                                                <option value="">Select Note Type</option>-->
                                                <option value="1">Class note</option>
                                                <option value="2">Home note</option>
                                                <option value="3">Assignment note</option>
                                                <option value="4">Diary</option>
                                            </select>
                                            <label for="note_type">Select Note Type</label>
                                        </div>
                                    </div>
                                </div>                               
                            </div>                                               
                    
                            <div class="form-group row">
                                <div class="col-6">
                                    <div class="form-material">
                                        <input required="" type="text" class="form-control" id="start_date" name="start_date" placeholder="Start Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d-m-Y'); ?>">
                                        <label for="start_date">Start Date</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-material">
                                        <input required="" type="text" class="form-control" id="end_date" name="end_date" placeholder="End Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d-m-Y'); ?>">
                                        <label for="end_date">End Date</label>
                                    </div>
                                </div>
                            </div>
                  
                            <div class="form-group">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                            </div>
                    
                    <?php echo form_close(); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
           



