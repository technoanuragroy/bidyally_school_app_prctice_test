<?php $this->load->view('school/_include/header'); ?>

<link href="<?php echo base_url(); ?>_css/jquery.fancybox.css" rel="stylesheet">  
<script src="<?php echo base_url(); ?>_js/jquery.fancybox.js" type="text/javascript"></script>  

<!-- END Footer -->
<style>
/*    .invalid-feedback {
        width: 50% !important;
    }*/
</style>
<script type="text/javascript">

    function get_section_list() {
        
        var semester_id = $('.semester_id').val();
       
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader').hide();
                $(".section_id").html("");
                if (msg != "") {                    
                    $(".section_id").html(msg);
                }
            }
        });
    }

    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Student Academic Report</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material">
                                <select required="" class="form-control semester_id" id="semester_id" name="semester_id" onchange="get_section_list();">

                                    <option value="">Select Semester</option>
                                    <?php
                                    $selected = '';
                                    foreach ($semesters as $semester) {
//                                        if ($post_data['class_id'] != '') {
//                                            if ($post_data['class_id'] == $class['id']) {
//
//                                                $selected = 'selected="selected"';
//                                            } else {
//
//                                                $selected = '';
//                                            }
//                                        }
                                        ?>
                                        <option value="<?php echo $semester['id']; ?>" <?php //echo $selected; ?>><?php echo $semester['semester_name']; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="material-gridf">Select Semester</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material">
                                <select class="form-control section_id" id="section_id" name="section_id">
                                    <option value="">Select Section</option>
                                </select>
                                <label for="section">Select Section</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
            
            <div class="table_responsive">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                    <tr>                            
                                        <th>Student name</th>
                                        <th>Class</th>    
                                        <th>Section</th>                                  
                                        <th>Details</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(!empty($student_list)) {
                                        foreach($student_list as $student) {
                                            
                                            $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id="' . $student['id'] . '" ');
                                            $class_name =  $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id="' . $student['class_id'] . '" ');
                                            $section_name =  $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id="' . $student['section_id'] . '" ');                                                                                   
                                ?>
                                            <tr>
                                                <td><?php echo $student_name; ?></td>
                                                <td><?php echo $class_name; ?></td>
                                                <td><?php echo $section_name; ?></td>
                                                
<!--                                                <td style="text-align:center"><a target="_blank" href="<?php echo base_url(); ?>school/report/resultDetails/<?php echo $this->my_custom_functions->ablEncrypt($student['id']); ?>/<?php echo $this->my_custom_functions->ablEncrypt($student['class_id']); ?>/<?php echo $this->my_custom_functions->ablEncrypt($student['semester_id']); ?>"><i class="fa fa-search-plus" aria-hidden="true"></i></a></td>-->
                                                <td style="text-align:center"><a href="#fancy_<?php echo $student['id']; ?>" class="fancybox" data-fancybox="gallery"><i class="fa fa-search-plus" aria-hidden="true"></i></a></td>
                                            </tr>
                                            
                                            
                                            
                                            
                                            
                                                                                      
                                            
                                            
                                            
                                            
                                <?php         
                                        }
                                    } else { 
                                ?>
                                        <tr>
                                            <td colspan="7">No student records</td>
                                        </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
            
            
            
            
                        <?php 
                        
                        if(!empty($student_list)) {
                                        foreach($student_list as $student) {
                        $result = $student['result']; ?>
                                            <div class="table_responsive" id="fancy_<?php echo $student['id']; ?>" style="display: none;">
                                                <table class="table table-bordered table-striped table-vcenter">
                                                    <?php if(!empty($result['terms'])){ ?>
                                                    <thead>
                                                        <tr>
                                                            <th rowspan="2">Subjects</th>
                                                            <?php
                                                            foreach ($result['terms'] as $term_id => $term_detail) {

                                                                if ($term_detail['combined_exam_results'] == 1) {
                                                                    $colspan = count($term_detail['exams']) + 1;
                                                                } else {
                                                                    $colspan = count($term_detail['exams']);
                                                                }
                                                                ?>
                                                                <th colspan="<?php echo $colspan; ?>"><?php echo $term_detail['term_name']; ?></th>
                                                            <?php } ?>
                                                            <?php
                                                            if ($result['semester_detail']['combined_term_results'] == 1) {
                                                                ?>
                                                                <th rowspan="2"><?php echo $result['semester_detail']['combined_result_name']; ?></th>
                                                            <?php } ?>
                                                        </tr>
                                                        <tr>
                                                            <?php
                                                            foreach ($result['terms'] as $term_id => $term_detail) {
                                                                foreach ($term_detail['exams'] as $exam_id => $exam_detail) {
                                                                    ?>
                                                                    <th><?php echo $exam_detail['exam_name']; ?></th>
                                                                    <?php
                                                                }
                                                                if ($term_detail['combined_exam_results'] == 1) {
                                                                    ?>
                                                                    <th><?php echo $term_detail['combined_result_name']; ?></th>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($result['subjects'] as $subject_id => $subject) { ?>
                                                            </tr>
                                                        <td><?php echo $subject; ?></td>
                                                        <?php
                                                        $semester_combined_score = 0;
                                                        foreach ($result['terms'] as $term_id => $term_detail) {

                                                            $term_combined_score = 0;
                                                            foreach ($term_detail['exams'] as $exam_id => $exam_detail) {//echo "<pre>";print_r($exam_detail);
                                                                if (array_key_exists($subject_id, $exam_detail['subjects'])) {

                                                                    $score = $exam_detail['subjects'][$subject_id]['score'];
                                                                    if (is_numeric($score)) {
                                                                        $score = $score;
                                                                    } else {
                                                                        $score = 0;
                                                                    }
                                                                    $term_combined_score += $score;
                                                                    $semester_combined_score += $score;
                                                                    ?>
                                                                    <td><?php echo $score; ?></td>
                                                                <?php } else { ?>
                                                                    <td>-</td>
                                                                    <?php
                                                                }
                                                            }
                                                            if ($term_detail['combined_exam_results'] == 1) {
                                                                ?>
                                                                <td><?php echo $term_combined_score; ?></td>
                                                                <?php
                                                            }
                                                        }
                                                        if ($result['semester_detail']['combined_term_results'] == 1) {
                                                            ?>
                                                            <td><?php echo $semester_combined_score; ?></td> 
                                                        <?php } ?>
                                                        <tr>
                                                    <?php } ?>
                                                        </tbody>  
                                                    <?php }else{ ?>
                                                        <tr>
                                                            <td colspan="4">No result found</td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </div>
                        <?php }
                        
                                                    } ?>
            
            
            
            
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<?php $this->load->view('school/_include/footer'); ?>
           



