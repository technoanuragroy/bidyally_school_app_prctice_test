<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">
    .table-vcenter tbody tr:nth-of-type(odd) {
        background-color: rgba(0, 0, 0, .1) !important;
    }
    .table-vcenter thead tr th {        
        font-weight: bold;
        text-align: center;
    }
</style>
<script type="text/javascript">
    $(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>

<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Student-wise Attendance Report for <?php echo $classSection; ?></h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-content block-content-full">
                   
            <div class="form-group">                
                <a href="<?php echo base_url(); ?>school/report/studentReport" class="btn btn-outline-danger">Back</a>                
            </div>
            
            <div class="table_responsive">
                                   
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Present</th>
                            <th>%age</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (!empty($report_studentwise)) {
                                foreach ($report_studentwise as $student_id => $row) {
                        ?>
                                    <tr style="cursor:pointer;" class="clickable-row" data-href="<?php echo base_url(); ?>school/report/attendance_detail/<?php echo $student_id; ?>">
                                        <td>
                                            <?php echo $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $student_id . '"'); ?>
                                        </td>
                                        <td style="text-align:center;">
                                            <?php echo $row['total_present'] . '/' . $row['total_class']; ?>
                                        </td>
                                        <td style="text-align:center;">
                                            <?php echo number_format($row['percentage'],2).' %'; ?>
                                            <span class="attn_dir_arrow"><i class="fas fa-arrow-right"></i></span>
                                        </td>
                                    </tr>  
                        <?php
                                }
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="3">
                                        No record found
                                    </td>    
                                </tr>
                        <?php      
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>    
            
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
        

