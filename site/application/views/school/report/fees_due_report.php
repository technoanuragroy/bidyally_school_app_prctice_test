<?php $this->load->view('school/_include/header'); ?>

    <link href="<?php echo base_url(); ?>_css/new_version/jquery-ui.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>_js/new_version/jquery-ui.js" type="text/javascript"></script>
    
    <style type="text/css">
        .search_container {
            display: none;
        }       
        .name_container {
            display: none;
        }
        .hidden_due {
            display: none;
        }
    </style>
    
    <script type="text/javascript">
        function get_section_list() {
                        
            var semester_id = $('.semester_id').val();
            
            $('.loader').show();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
                data: "semester_id=" + semester_id,
                success: function (msg) {
                    $('.loader').hide();
                    $(".section_id").html("");
                    if (msg != "") {                        
                        $(".section_id").html(msg);                       
                        $(".name").val("");
                        $(".name_container").hide();                        
                    } else {
                        $(".section_id").html("");
                    }
                }
            });
        }

        $(document).ready(function () {
            $("#search").on("click", function() { 
                $(".search_container").slideToggle();
            });             
            
            $(".semester_id,.section_id").on("change", function() {
                if($('.semester_id').val() != "" && $(".section_id").val() != "") {
                    $(".name_container").show();
                } else {
                    $(".name").val("");
                    $(".name_container").hide();
                }
            });
            
            // For autocomplete student name 
            var srcStudent = '<?php echo base_url(); ?>school/report/ajax_get_student_name';
            $(".name").autocomplete({ 
                source: function(request, response) {
                    $.ajax({
                        url: srcStudent,
                        dataType: "json",
                        data: {
                            name_or_roll: $('.name').val(),
                            semester_id: $('.semester_id').val(),
                            section_id: $('.section_id').val(),
                        },
                        success: function(data) { 
                            response(data);
                        }
                    });
                },
                minLength: 2,
            });
            
            $(".show_hidden_due").on("click", function() {
                
                var student_id = $(this).data("student");                
                $(".hidden_due_"+student_id).slideToggle();
                
                $(this).find("i").toggleClass("fa-chevron-circle-down");
                $(this).find("i").toggleClass("fa-chevron-circle-up");
            });
        });        
    </script>
    
    <!-- Page Content -->
    <div class="content">

        <!-- Material Forms Validation -->
        <h2 class="content-heading">Fees Due Report</h2>
        
        <div class="block">   
            <div class="block-header block-header-default">
                <a href="javascript:;" class="btn btn-primary" id="search">Search</a>
            </div>
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content block-content-full movedown">
<!--                <div class="row justify-content-center py-20">-->
<!--                    <div class="col-xl-12 load">-->
                        
                        <div class="loader" style="display: none;">
                            <i class="fa fa-3x fa-cog fa-spin"></i>
                        </div>
                        
                        <div class="search_container">
                            <?php echo form_open_multipart('school/report/feesDueReport', array('id' => 'frmFeesDueReport', 'class' => 'js-validation-material')); ?>                                                                

                                    <div class="form-group row">
                                        <div class="col-6">
                                            <div class="form-material">
                                                <select class="form-control semester_id" id="semester_id" name="semester_id" onchange="get_section_list();">
                                                    <option value="">Select Semester</option>
                                                    <?php
                                                        $selected = '';
                                                        foreach ($semesters as $semester) {                                                            
                                                    ?>
                                                            <option value="<?php echo $semester['id']; ?>"><?php echo $semester['semester_name']; ?></option>
                                                    <?php                                                 
                                                        } 
                                                    ?>
                                                </select>
                                                <label for="semester_id">Select Semester</label>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-material">
                                                <select class="form-control section_id" id="section_id" name="section_id">
                                                    <option value="">Select Section</option>
                                                </select>
                                                <label for="section_id">Select Section</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group name_container">                                                                      
                                        <div class="form-material">
                                            <input type="text" class="form-control name" id="name" name="name" placeholder="Student Name/Roll No " value="" style="position: relative;">
                                            <label for="name">Student Name/Roll No</label>
                                        </div>                                    
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">                                        
                                    </div>

                            <?php echo form_close(); ?>      
                        </div>    
                
                        <div class="table_responsive">
                            <table class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>                            
                                        <th>Student name</th>      
                                        <th>Due Information</th>   
                                        <th style="text-align:right" width="150px">Total Due Of Student</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $total_due = 0;
                                    if(!empty($report)) {
                                        foreach($report as $student) {     
                                            
                                            $student_id = $this->my_custom_functions->ablEncrypt($student['id']);  
                                            $total_due_of_student = 0;
                                ?>
                                            <tr>
                                                <td><?php echo $student['name']; ?></td>                                                
                                                <td>
                                                    <table class="table table-bordered table-striped table-vcenter check_align">
                                                        <thead>
                                                            <tr>                                                                                                                                                                         
                                                                <th width="30%">Semester Info</th>    
                                                                <th width="50%">Due On</th>  
                                                                <th width="20%">Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php                                                                 
                                                                foreach($student['semester_wise_dues'] as $semester_id => $dues) {                                                                     
                                                            ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php 
                                                                                $class_info = "";                                           
                                                                                $class_info_array = json_decode($dues['class_info'], true);
                                                                                if(is_array($class_info_array)) {
                                                                                    if(array_key_exists('semester', $class_info_array)) {
                                                                                        $class_info .= 'Semester: '.$class_info_array['semester'].', ';
                                                                                    }                                                                                    
                                                                                    if(array_key_exists('section', $class_info_array)) {
                                                                                        $class_info .= 'Section: '.$class_info_array['section'].', ';
                                                                                    }
                                                                                    if(array_key_exists('roll_no', $class_info_array)) {
                                                                                        $class_info .= 'Roll No: '.$class_info_array['roll_no'];
                                                                                    }
                                                                                }
                                                                                echo $class_info;
                                                                            ?> 
                                                                        </td>
                                                                        <td>
                                                                            <?php 
                                                                                foreach($dues['dues']['details'] as $key => $due) {
                                                                                    $breakup_id = $this->my_custom_functions->ablEncrypt($dues['dues']['breakup_id'][$key]);

                                                                                    if($key == 1) {
                                                                                        echo '<span class="hidden_due hidden_due_'.$student['id'].'_'.$semester_id.'">';
                                                                                    }
                                                                                    echo '<a href="'.base_url().'school/user/receiveStudentFees/'.$student_id.'/'.$breakup_id.'" target="_blank">'.$due.'</a>';
                                                                                    if($key == 0 AND count($dues['dues']['details']) > 1) {
                                                                                        echo '<a href="javascript:;" style="float: right;" class="show_hidden_due" data-student="'.$student['id'].'_'.$semester_id.'"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>';
                                                                                    }
                                                                                    echo '<br>';
                                                                                }
                                                                                if(count($dues['dues']['details']) > 1) {
                                                                                    echo '</span>';                                                            
                                                                                }
                                                                            ?>                                                                          
                                                                        </td> 
                                                                        <td>
                                                                            <?php 
                                                                                foreach($dues['dues']['amount'] as $key => $due) {
                                                                                    if($key == 1) {
                                                                                        echo '<span class="hidden_due hidden_due_'.$student['id'].'_'.$semester_id.'">';
                                                                                    }
                                                                                    echo INDIAN_CURRENCY_CODE.' '.number_format($due, 2);
                                                                                    echo '<br>';
                                                                                    $total_due_of_student += $due;
                                                                                    $total_due += $due;
                                                                                }
                                                                                if(count($dues['dues']['amount']) > 1) {
                                                                                    echo '</span>';
                                                                                }
                                                                            ?>  
                                                                        </td>
                                                                    </tr>
                                                            <?php

                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>                                                    
                                                </td>
                                                <td style="text-align:right">
                                                    <?php echo INDIAN_CURRENCY_CODE.' '.number_format($total_due_of_student, 2); ?>                                                                                                      
                                                </td>
                                            </tr>
                                <?php         
                                        }
                                ?>
                                        <tr>
                                            <td colspan="2" style="text-align:right">TOTAL DUE</td>
                                            <td style="text-align:right"><?php echo INDIAN_CURRENCY_CODE.' '.number_format($total_due, 2); ?></td>
                                        </tr>    
                                <?php            
                                    } else { 
                                ?>
                                        <tr>
                                            <td colspan="3">No due records</td>
                                        </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
           
<!--                    </div>-->
<!--                </div>-->
            </div>
            
        </div>        
    </div>
    
<?php $this->load->view('school/_include/footer'); ?>
           



