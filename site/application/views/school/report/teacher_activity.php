<?php $this->load->view('school/_include/header'); ?>

<link href="<?php echo base_url(); ?>_css/new_version/jquery-ui.css" rel="stylesheet">    
<link href="<?php echo base_url(); ?>_css/jquery.fancybox.css" rel="stylesheet">  
<script src="<?php echo base_url(); ?>_js/new_version/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>_js/jquery.fancybox.js" type="text/javascript"></script>  

<style type="text/css">
    .table-vcenter thead tr th {        
        font-weight: bold;
        text-align: center;
    }
</style>  
<script type="text/javascript">
    $(document).ready(function () {
        $(".fancybox").fancybox({
            //touch: false
        });
    });
</script>

<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Teacher Activity Report for <?php echo $classSection; ?></h2>

    <!-- Dynamic Table Full -->
    <div class="block">        
        <div class="block-content block-content-full">

            <div class="form-group">                
                <a href="<?php echo base_url(); ?>school/report/teacherReport" class="btn btn-outline-danger">Back</a>                
            </div>
            
            <div class="table_responsive">
            
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Class/Section</th>
                            <th>Subject/type</th>
                            <th>Teacher</th>
                            <th>View</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                            
                            if (!empty($teacher_activity)) {
                                foreach ($teacher_activity as $row) {
                                    
                                    $note = '';
                        ?>
                                    <tr>
                                        <td>
                                            <?php echo date("d-m-Y, g:ia", $row['note_issuetime']); ?>                                            
                                        </td>
                                        <td>
                                            <?php
                                                $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', 'and id = "' . $row['semester_id'] . '"');
                                                $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $class_id . '"');
                                                $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $row['section_id'] . '"');
                                               
                                                echo $class_name . ', ' . $section_name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $row['subject_id'] . '"');
                                                if ($row['type'] == 1) {
                                                    $note = 'Class note';
                                                } else if ($row['type'] == 2) {
                                                    $note = 'Home note';
                                                } else if ($row['type'] == 3) {
                                                    $note = 'Assignment note';
                                                }
                                                
                                                echo $subject_name . ', ' . $note;
                                            ?>
                                        </td>
                                        <td style="text-align:center;">
                                            <?php echo $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $row['teacher_id'] . '"'); ?>
                                        </td>
                                        <td style="text-align:center;">
                                            <a href="#view_detail<?php echo $row['id']; ?>" class="fancybox"> <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    
                                    <div style="display:none" id="view_detail<?php echo $row['id']; ?>">
                                        <div style="width:100%;float: left;"><h2>Details</h2></div>
                                        <div style="display: inline-block;width: 100%;float: left;font-size: 18px;font-weight: bold;"> Topic : <?php echo $row['topic_name']; ?></div>
                                        <div style="display: inline-block;width: 100%;float: left;font-size: 18px;font-weight: bold;"> Note : <?php echo $row['classnote_text']; ?></div>
                                        <div style="display: inline-block;width: 100%;float: left;">    
                                            <?php
                                                $get_file = $this->my_custom_functions->get_multiple_data(TBL_NOTE_FILES, 'and note_id = "' . $row['id'] . '" and type IN(1,2,3)');                                            
                                                foreach ($get_file as $rec) {
                                            ?>
                                                    <a target="_blank" href="<?php echo $rec['file_url']; ?>"><img src="<?php echo base_url(); ?>_images/file.png" width="50" height="50"></a>
                                            <?php                                             
                                                }
                                            ?>
                                        </div>
                                    </div>
                        <?php
                                }
                            } else { 
                        ?>
                                <tr>
                                    <td colspan="5">
                                        No record found
                                    </td>    
                                </tr>
                        <?php      
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>    
            
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
        

