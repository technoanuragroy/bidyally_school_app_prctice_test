<?php $this->load->view('school/_include/header'); ?>

    <link href="<?php echo base_url(); ?>_css/new_version/jquery-ui.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>_js/new_version/jquery-ui.js" type="text/javascript"></script>
    
    <style type="text/css">
        .advanced_search {
            display: none;
        }       
        .name_container {
            display: none;
        }
    </style>
    
    <script type="text/javascript">
        function get_section_list() {
                        
            var semester_id = $('.semester_id').val();
            
            $('.loader').show();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
                data: "semester_id=" + semester_id,
                success: function (msg) {
                    $('.loader').hide();
                    $(".section_id").html("");
                    if (msg != "") {                        
                        $(".section_id").html(msg);
                        $(".name").val("");
                        $(".name_container").hide();  
                    } else {
                        $(".section_id").html("");
                    }
                }
            });
        }

        $(document).ready(function () {
            $('#start_date').datepicker({
                dateFormat: 'dd-mm-yy',
            });
            $('#end_date').datepicker({
                dateFormat: 'dd-mm-yy',
            });
            
            $("#adv_search").on("click", function() { 
                $(".advanced_search").slideToggle();
            });   
            
            $(".semester_id,.section_id").on("change", function() {
                if($('.semester_id').val() != "" && $(".section_id").val() != "") {
                    $(".name_container").show();
                } else {
                    $(".name").val("");
                    $(".name_container").hide();
                }
            });
            
            // For autocomplete student name 
            var srcStudent = '<?php echo base_url(); ?>school/report/ajax_get_student_name';
            $(".name").autocomplete({ 
                source: function(request, response) {
                    $.ajax({
                        url: srcStudent,
                        dataType: "json",
                        data: {
                            name_or_roll: $('.name').val(),
                            semester_id: $('.semester_id').val(),
                            section_id: $('.section_id').val(),
                        },
                        success: function(data) { 
                            response(data);
                        }
                    });
                },
                minLength: 2,
            });
        });        
    </script>
    
    <!-- Page Content -->
    <div class="content">

        <!-- Material Forms Validation -->
        <h2 class="content-heading">Fees Payment Report</h2>
        
        <div class="block">            
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
<!--                <div class="row justify-content-center py-20">-->
<!--                    <div class="col-xl-12 load">-->
                        
                        <div class="loader" style="display: none;">
                            <i class="fa fa-3x fa-cog fa-spin"></i>
                        </div>
                        
                        <?php echo form_open_multipart('school/report/feesPaymentReport', array('id' => 'frmFeesPaymentReport', 'class' => 'js-validation-material')); ?>                                                                

                                <div class="form-group row">
                                    <div class="col-6">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" id="start_date" name="start_date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo $this->session->userdata('fpr_start_date'); ?>">
                                            <label for="start_date">Start Date</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" id="end_date" name="end_date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo $this->session->userdata('fpr_end_date'); ?>">
                                            <label for="end_date">End Date</label>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="advanced_search">
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <div class="form-material">
                                                <select class="form-control semester_id" id="semester_id" name="semester_id" onchange="get_section_list();">
                                                    <option value="">Select Semester</option>
                                                    <?php
                                                        $selected = '';
                                                        foreach ($semesters as $semester) {                                                            
                                                    ?>
                                                            <option value="<?php echo $semester['id']; ?>"><?php echo $semester['semester_name']; ?></option>
                                                    <?php                                                 
                                                        } 
                                                    ?>
                                                </select>
                                                <label for="semester_id">Select Semester</label>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-material">
                                                <select class="form-control section_id" id="section_id" name="section_id">
                                                    <option value="">Select Section</option>
                                                </select>
                                                <label for="section_id">Select Section</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group name_container">                                                                      
                                        <div class="form-material">
                                            <input type="text" class="form-control name" id="name" name="name" placeholder="Student Name/Roll No " value="" style="position: relative;">
                                            <label for="name">Student Name/Roll No</label>
                                        </div>                                    
                                    </div>
                                </div>    
                                
                                <div class="form-group">
                                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    <a href="javascript:" class="btn btn-primary show_hide shift" id="adv_search">Advanced Search</a>
                                </div>
                        
                        <?php echo form_close(); ?>                                                                     
                
                        <div class="table_responsive">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                    <tr>                            
                                        <th>Student name</th>
                                        <th>Semester Info</th>    
                                        <th>Fees For</th>
                                        <th>Payment Mode</th>
                                        <th>Date Of Payment</th>
                                        <th style="text-align:right">Amount</th>                                        
                                        <th>Details</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(!empty($report)) {
                                        foreach($report as $payment) {
                                            
                                            $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id="' . $payment['student_id'] . '" ');
                                            $fees_breakup_label = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE_BREAKUPS, 'breakup_label', 'and id="' . $payment['fees_breakup_id'] . '" ');
                                                 
                                            $class_info = "";
                                            if($payment['class_info'] != "") {
                                                $class_info_array = json_decode($payment['class_info'], true);
                                                if(is_array($class_info_array)) {
                                                    if(array_key_exists('semester', $class_info_array)) {
                                                        $class_info .= 'Semester: '.$class_info_array['semester'].', ';
                                                    }                                                                                    
                                                    if(array_key_exists('section', $class_info_array)) {
                                                        $class_info .= 'Section: '.$class_info_array['section'].', ';
                                                    }
                                                    if(array_key_exists('roll_no', $class_info_array)) {
                                                        $class_info .= 'Roll No: '.$class_info_array['roll_no'];
                                                    }
                                                }
                                            }
                                            $class_info = ($class_info == "") ? 'N/A' : $class_info;
                                            
                                            $paid_by = 'N/A';
                                            if($payment['created_by'] == SCHOOL) {
                                                $paid_by = 'Offline';
                                            } else if($payment['created_by'] == PARENTS) {
                                                $paid_by = 'Online';
                                            }                                                                                        
                                ?>
                                            <tr>
                                                <td><?php echo $student_name; ?></td>
                                                <td><?php echo $class_info; ?></td>
                                                <td><?php echo $fees_breakup_label; ?></td>
                                                <td><?php echo $paid_by; ?></td>
                                                <td><?php echo date("d-m-Y, g:ia", strtotime($payment['payment_datetime'])); ?></td>
                                                <td style="text-align:right">
                                                    <?php 
                                                        $paid_amount = $payment['paid_amount'];
                                                        $payment_handling_fees = $payment['service_charges'];
                                                        $display_amount = $paid_amount - $payment_handling_fees;
                                                        echo INDIAN_CURRENCY_CODE.' '.number_format($display_amount, 2); 
                                                    ?>
                                                </td>
                                                <td style="text-align:center"><a href="<?php echo base_url(); ?>school/report/feesPaymentDetails/<?php echo $this->my_custom_functions->ablEncrypt($payment['id']); ?>"><i class="fa fa-search-plus" aria-hidden="true"></i></a></td>
                                            </tr>
                                <?php         
                                        }
                                    } else { 
                                ?>
                                        <tr>
                                            <td colspan="7">No payment records</td>
                                        </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
           
<!--                    </div>-->
<!--                </div>-->
            </div>
            
        </div>        
    </div>
    
<?php $this->load->view('school/_include/footer'); ?>
           



