<?php $this->load->view('school/_include/header'); ?>
<!-- END Footer -->
<style>
/*    .invalid-feedback {
        width: 50% !important;
    }*/
</style>
<script type="text/javascript">

    function get_section_list() {
        
        var semester_id = $('.semester_id').val();
       
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader').hide();
                $(".section_id").html("");
                if (msg != "") {                    
                    $(".section_id").html(msg);
                }
            }
        });
    }

    
</script>
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Student Academic Report</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    
                    
                </div>
            </div>
            
            <div class="table_responsive">
                        <table class="table table-bordered table-striped table-vcenter">
                            <?php if(!empty($result['terms'])){ ?>
                            <thead>
                                <tr>
                                    <th rowspan="2">Subjects</th>
                                    <?php
                                    foreach ($result['terms'] as $term_id => $term_detail) {

                                        if ($term_detail['combined_exam_results'] == 1) {
                                            $colspan = count($term_detail['exams']) + 1;
                                        } else {
                                            $colspan = count($term_detail['exams']);
                                        }
                                        ?>
                                        <th colspan="<?php echo $colspan; ?>"><?php echo $term_detail['term_name']; ?></th>
                                    <?php } ?>
                                    <?php
                                    if ($result['semester_detail']['combined_term_results'] == 1) {
                                        ?>
                                        <th rowspan="2"><?php echo $result['semester_detail']['combined_result_name']; ?></th>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <?php
                                    foreach ($result['terms'] as $term_id => $term_detail) {
                                        foreach ($term_detail['exams'] as $exam_id => $exam_detail) {
                                            ?>
                                            <th><?php echo $exam_detail['exam_name']; ?></th>
                                            <?php
                                        }
                                        if ($term_detail['combined_exam_results'] == 1) {
                                            ?>
                                            <th><?php echo $term_detail['combined_result_name']; ?></th>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($result['subjects'] as $subject_id => $subject) { ?>
                                    </tr>
                                <td><?php echo $subject; ?></td>
                                <?php
                                $semester_combined_score = 0;
                                foreach ($result['terms'] as $term_id => $term_detail) {

                                    $term_combined_score = 0;
                                    foreach ($term_detail['exams'] as $exam_id => $exam_detail) {//echo "<pre>";print_r($exam_detail);
                                        if (array_key_exists($subject_id, $exam_detail['subjects'])) {

                                            $score = $exam_detail['subjects'][$subject_id]['score'];
                                            if (is_numeric($score)) {
                                                $score = $score;
                                            } else {
                                                $score = 0;
                                            }
                                            $term_combined_score += $score;
                                            $semester_combined_score += $score;
                                            ?>
                                            <td><?php echo $score; ?></td>
                                        <?php } else { ?>
                                            <td>-</td>
                                            <?php
                                        }
                                    }
                                    if ($term_detail['combined_exam_results'] == 1) {
                                        ?>
                                        <td><?php echo $term_combined_score; ?></td>
                                        <?php
                                    }
                                }
                                if ($result['semester_detail']['combined_term_results'] == 1) {
                                    ?>
                                    <td><?php echo $semester_combined_score; ?></td> 
                                <?php } ?>
                                <tr>
                            <?php } ?>
                                </tbody>  
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="4">No result found</td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<?php $this->load->view('school/_include/footer'); ?>
           



