<?php $this->load->view('school/_include/header'); ?>
<script type="text/javascript">
    $(document).ready(function ($) {

    });
</script>
<style>
    #calendar_box{
        max-width:700px;
        margin: auto;
    }
    .calender {
    background: transparent;
    border: 0;
    width: 100%;
    box-shadow: none;
    margin-bottom: 30px;
}

.calender td {
    border-bottom: 0;
    border-right: 0;
    vertical-align: top;
    padding: 2px;
    width: 100px;
    min-height: 50px;
    border-radius: 100px;
    height: 27px;
}
.date{
    text-align:center;
}
.calender .date{
    background: #fff;
    border-radius: 100px;
    width: 50px;
    height: 50px;
    line-height: 48px;
    border: 1px solid #333;
    color: #000;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
}
.calender th {
    font-size: 15px;
    text-decoration: none;
    background-color: transparent;
    color: #3A3939;
    font-weight: 700;
    line-height: 37px;
}
.green{
    background: green;
}

.present_class{
    /* background: #388E3C !important; */
    /* color: #fff!important; */
    background: #fff;
    border: 3px solid #388E3C!important;
}
.absent_class{
    /* background: #FF3547!important; */
    border: 3px solid #c30011!important;
    /* color: #fff!important; */
    background: #fff;
}
.pertial_present_class{
    /* background: #ec780b!important; */
    border: 3px solid #f57701!important;
    /* color: #fff!important; */
    background: #fff;
}
.calender tbody tr:first-child th{
    text-align: center;
    font-size: 20px;
}
</style>


<!-- Page Content -->
<div class="content">
    <?php 
        $student_id = $this->uri->segment(4); 
        $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $student_id . '" '); 
    ?>
    <h2 class="content-heading">Student Attendance Detail for <?php echo $student_name; ?></h2>

    <!-- Dynamic Table Full -->
    <div class="block">        
        <div class="block-content block-content-full">
            
            <div class="form-group">                
                <a href="<?php echo base_url(); ?>school/report/studentReport" class="btn btn-outline-danger">Back</a>                
            </div>
            
            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            
            <div id="calendar_box">
                <?php
                if (!empty($atten_data_list)) {
                    foreach ($atten_data_list as $att) {//echo "<pre>";print_r($att);
                        echo $this->calendar->generate($att['year'], $att['month'], $att['event']);
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->


<script type="text/javascript">
    $(document).ready(function () {
        $("div.highlight").eq(0).removeClass("highlight").addClass("date");
        $("div.highlight").eq(1).removeClass("highlight").addClass("content_box");
    });
</script>
</script>
<?php $this->load->view('school/_include/footer'); ?>
        

