<?php $this->load->view('school/_include/header'); ?>
<style>
    #DataTables_Table_0_length{
        display: none;
    }
</style>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this staff?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteTeacher/" + id;
    }

    function call_restore(id) {
        $('.message_block').html('<p>Are you sure that you want to restore this teacher?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_restore('" + id + "')");
    }
    function confirm_restore(id) {
        //alert(id);
        window.location.href = "<?php echo base_url(); ?>school/user/restoreTeacher/" + id;
    }
</script>


<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Staff</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url(); ?>school/user/addTeacher" class="btn btn-primary">Add Staff</a>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">
        <div class="table-responsive">
            <?php
            if (!empty($teacher)) {
                $i = 1;
                ?>
                <!--                <table class="table table-striped table-vcenter js-dataTable-full">-->
                    <table class="table table-striped table-vcenter js-dataTable-full no-footer ">
                    <thead>
                        <tr>
                            <th class="">Username</th>
                            <th>Staff name</th>
                            <th>Staff type</th>
                            <th>Phone Number</th>
                            <th>Status</th>
                            <th class="text-center" style="width: 15%;">Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($teacher as $row) {


                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                            //$encrypted = $row['id'];
                            ?>
                            <tr>
                                <td class="text-center"><?php 
                                echo $username = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $row['id'] . '"');
                                
                                ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php
                                    if ($row['staff_type'] == 1) {
                                        echo "Teaching Staff";
                                    } else {
                                        echo "Non-teaching staff";
                                    }
                                    ?></td>
                                <td><?php echo $row['phone_no']; ?></td>

                                <td><?php
                                    $status = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'status', 'and id = "' . $row['id'] . '"');
                                    if ($status == 1) {
                                        echo "Active";
                                    } else {
                                        echo "Inactive";
                                    }
                                    ?></td>
                                <td class="text-center">                                    
                                    <a href="<?php echo base_url() . 'school/user/permission/' . $encrypted; ?>" title="Permission">
                                        <i class="fa fa-lock"></i>
                                    </a>
                                
                                                                    
                                    <a href="<?php echo base_url() . 'school/user/editTeacher/' . $encrypted; ?>" title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                               
                                     
                                    <?php if($row['is_deleted'] == 0){ ?>
                                    <a href="<?php echo base_url() . 'school/user/deleteTeacher/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    <?php }else{ ?>
                                    <a href="<?php echo base_url() . 'school/user/deleteTeacher/' . $encrypted; ?>" title="Restore" data-toggle="modal" data-target="#modal-top" onclick="return call_restore('<?php echo $encrypted; ?>');">
                                        <i class="fa fa-undo"></i>
                                    </a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>  
                    </tbody>
                </table>
                </div>
            <?php } else { ?>
                <tr>
                    <td colspan="9">No staff added</td>
                </tr>
                <?php
            }
            ?>
            
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
