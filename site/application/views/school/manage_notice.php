<?php $this->load->view('school/_include/header');?>

        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this notice?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
            }
            function confirm_delete(id) {
                window.location.href = "<?php echo base_url(); ?>school/user/deleteNotice/" + id;
            }
        </script>
    

                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Manage Notice</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <a href="<?php echo base_url(); ?>school/user/issueNoice"class="btn btn-primary">Issue Notice</a>
                        </div>
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content block-content-full">
                        <div class="table-responsive">
                            <?php
                            if (!empty($notice)) {
                                $string = '';
                                $i = 1;
                                ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    <thead>
                                        <tr>
                                            
                                            <th>Notice for</th>
                                            <th>Notice Heading</th>
                                            <th>Publish Date</th>
                                            <th>Status</th>
                                            <th class="text-center">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($notice as $row) {


                                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                            //$encrypted = $row['id'];
                                            ?>
                                            <tr>
                                                
                                                <td><?php
                                                    if ($row['type'] == 1) {
                                                        echo 'All Teachers';
                                                    } else if ($row['type'] == 2) {
                                                        echo 'All Parents';
                                                    } else {
                                                        $string = '';
                                                        $semester_ids = explode(',', $row['semester_id']);
                                                        foreach ($semester_ids as $sem_id) {
                                                            $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id = ' . $sem_id . '');
                                                            $string .= $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = ' . $class_id . '') . ',';
                                                        }
                                                        $class_name_text = rtrim($string, ',');
                                                        echo 'Notice only for ' . $class_name_text;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $row['notice_heading']
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    
                                                        echo date('d/m/Y',$row['publish_date']);
                                                    
                                                    ?>
                                                </td>

                                                <td><?php
                                                    if ($row['status'] == 1) {
                                                        echo "Active";
                                                    } else {
                                                        echo "Inactive";
                                                    }
                                                    ?></td>
                                                <td class="text-center">                                    
                                                    <a href="<?php echo base_url() . 'school/user/editNotice/' . $encrypted; ?>" title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="<?php echo base_url() . 'school/user/deleteNotice/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                               
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>  
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="9">No notice found</td>
                                </tr>
                                <?php
                            }
                            ?>
                            </div>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->



                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->

                      <?php $this->load->view('school/_include/footer'); ?>

