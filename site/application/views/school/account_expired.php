
<?php $this->load->view('school/_include/header');?>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this class?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteClass/" + id;
    }
</script>
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Account expired</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        
        
        <div class="block-content block-content-full">
            <p>Your account has been expired. Please contact customer care.</p>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>

<?php $this->load->view('school/_include/footer'); ?>
