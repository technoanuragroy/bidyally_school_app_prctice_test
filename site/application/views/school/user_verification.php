<?php $this->load->view('school/_include/header');
?>
<style>
    #enter_otp-error{
        float: left;
    }
    .modal-backdrop{
        display: none;
    }
</style>
<script>
    $(document).ready(function () {        
        $('[data-toggle="tooltip"]').tooltip();       
    });

    function sendOTP() {
        
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/main/send_otp",            
            success: function (msg) {       
                
                $('.loader').hide();
                
                if (msg != "") {
                    $('.otp_message').show();
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    onTimer();
                } else {
                    $(".section").html("");
                }
            }
        });
    }
    
    function sendVerificationEmail() {
    
        $('.loader').show();
        $('#email_button').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/main/sendVerificationEmail",            
            success: function (msg) {    
                
                $('.loader').hide();

                if (msg != "") {
                    $('.email_message').css('display', 'block');
                    $('.email_message').show();
                } else {
                    $(".section").html("");
                }
            }
        });
    }
</script>

<script>
    i = 60;
    function onTimer() {
        $('#mycounter').html(i + ' seconds left');

        i--;
        if (i < 0) {
            $('#otp_button').prop('disabled', false);
            $('#otp_button').text('Resend OTP');
            $('#mycounter').html('');
            $('.otp_message').hide();
            i = 60;
        } else {

            setTimeout(onTimer, 1000);
            $('#otp_button').prop('disabled', true);
        }
    }

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>


<div class="content">    
    <div class="col-md-12">
        <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
        <?php } ?>
        <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
        <?php } ?>
    </div>
    
    <div class="row">
        <div class="loader cust loader-universal" style="display: none;">
            <i class="fa fa-3x fa-cog fa-spin"></i>
        </div>
                                    
        <div class="blockOtp">            
            <h4>OTP Verification for <?php echo $school_details['mobile_no']; ?>
                <?php 
                    // Show the section only if otp not verified already
                    if($school_details['otp_verification'] == 0) { 
                ?>
                        <a href="javascript:" data-toggle="modal" data-target="#modal-popin">
                            <span class="edit_icon">
                                <i class="fa fa-edit"></i>
                            </span>
                        </a>
                <?php         
                    } else {
                ?>
                        <span class="edit_icon">
                            <i class="fa fa-check-circle green" aria-hidden="true"></i>
                        </span>
                <?php
                    } 
                ?>
            </h4>

            <?php 
                // Show the section only if otp not verified already
                if($school_details['otp_verification'] == 0) { 
            ?>
                    <div class="otp_button_container">
                        <button class="btn btn-alt-primary otp_btn" id="otp_button" onclick="sendOTP();">Send OTP</button>
                        <div id="mycounter"></div>
                        <span class="otp_message" style="display:none;">An OTP have been sent to the registered mobile number</span>
                    </div>

                    <form method="post" action="<?php echo current_url(); ?>" class="js-validation-material" style="clear:both;">
                        <div class="form-group clear_class">
                            <div class="form-material clear_class">
                                <input type="text" required="" class="form-control" name="enter_otp" id="enter_otp" placeholder="Enter OTP" value="" style="border: 1px solid #ccc;padding: 5px;box-shadow: none;">
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" name="submit" value="submit" class="btn btn-alt-primary blockOtpSubmit">
                        </div>
                    </form>
            <?php         
                } 
            ?>
        </div>
                                        
        <div class="blockOtp">            
            <h4>Email Verification for <?php echo $school_details['email_address']; ?>
                <?php 
                    // Show the section only if email not verified already
                    if($school_details['email_verification'] == 0) { 
                ?>
                        <a href="javascript:" data-toggle="modal" data-target="#modal-popin_email">
                            <span class="edit_icon">
                                <i class="fa fa-edit"></i>
                            </span>
                        </a>
                <?php                         
                    } else { 
                ?>
                        <span class="edit_icon">
                           <i class="fa fa-check-circle green" aria-hidden="true"></i>
                        </span>
                <?php                         
                    } 
                ?>
            </h4>

            <?php 
                // Show the section only if email not verified already
                if($school_details['email_verification'] == 0) { 
            ?>
                    <div class="otp_button_container">
                        <button class="btn btn-alt-primary mail_btn" id="email_button" onclick="sendVerificationEmail();">Send Verification Email</button>
        <!--                    <input type="submit" class="btn btn-alt-primary mail_btn" id="email_button" name="submit" value="Send Verification Email" >-->
                        <span class="email_message" style="display:none;">An Email have been sent to the registered email address</span>
                    </div>
            <?php                         
                } 
            ?>
        </div>                
    </div>

    <!-- Pop In Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content popup_side_border">
                <form action="<?php echo base_url(); ?>school/main/changeMobileNumber" method="post" class="">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark mob-block-header">
                            <h3 class="block-title">Change Mobile Number</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobile Number</label>
                                <input type="text" required="" name="mobilenumber" class="form-control" id="mobilenumber" placeholder="Mobile number" onkeypress="return isNumber(event)" maxlength="10" minlength="10">

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-alt-success" value="Submit">

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->

    <!-- Pop In Modal -->
    <div class="modal fade" id="modal-popin_email" tabindex="-1" role="dialog" aria-labelledby="modal-popin_email" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content popup_side_border">
                <form action="<?php echo base_url(); ?>school/main/changeEmailAddress" method="post" class="">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark mob-block-header">
                            <h3 class="block-title">Change Mobile Number</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="text" required="" name="email" class="form-control" id="email" placeholder="Email address">

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-alt-success" value="Submit">

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->

    <?php $this->load->view('school/_include/footer'); ?>