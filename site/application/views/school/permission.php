<?php $this->load->view('school/_include/header');?>

        <script type="text/javascript">
            function call_delete(id) {
                $('.message_block').html('<p>Are you sure that you want to delete this notice?</p>');
                $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
            }
            function confirm_delete(id) {
                window.location.href = "<?php echo base_url(); ?>school/user/deleteNotice/" + id;
            }
        </script>
    </head>
    <body>
<?php $this->load->view('school/_include/loader'); ?>

                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Manage Permission</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>
                        <?php echo form_open('school/user/update_permission','id="set_permission"'); ?>
                        <div class="block-content block-content-full">
                           
                            <?php
                            //echo "<pre>";print_r($login);
                            if (!empty($permissions)) {
                                
                                $string = '';
                                $i = 1;
                                ?>
                                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                    
                                    <tbody>
                                        <?php
                                        foreach ($permissions as $row) {
                                            $CI =& get_instance(); 
                                            $exists = $CI->check_existing_permission($login['id'],$row['id']); 
						  //echo '<pre>'; print_r($exists); die();
						foreach($exists as $exist ){  }  

                                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                            //$encrypted = $row['id'];
                                            ?>
                                            <tr>
								<input type="hidden" name="login_id" value="<?php echo $login['id']; ?>"  />
								
								<td style="text-align:left;" title="<?php echo $row['page_urls'] ; ?>">
                                                                    <strong><?php echo $row['section_name']; ?></strong>
                                                                </td>
							
								<td>
                                                                    <label>
                                                                        <span class="spanwidth" style="float:left;font-weight:bold;">
                                                                            <input type="radio" name="type[<?php echo $row['id']; ?>]" value="1" <?php if($exist[0]["allowed_type"]==1) { echo ' checked="checked" ' ;} ?> />Allowed
                                                                        </span>
                                                                    </label>
                                                                </td>
						
								<td>
                                                                    <label>
                                                                        <span class="spanwidth" style="float:left;font-weight:bold;">
                                                                            <input type="radio" name="type[<?php echo $row['id']; ?>]" value="2" <?php if($exist[0]["allowed_type"]==2 || $exist[0]["allowed_type"]==0) { echo ' checked="checked" ' ;} ?> />Not Allowed
                                                                        </span>
                                                                    </label>
                                                                </td>
												
						</tr>
                                            <?php
                                            $i++;
                                        }
                                        ?>  
                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="9">No notice found</td>
                                </tr>
                                <?php
                            }
                            ?>
                                <center><?php echo form_submit('submit', 'Update', 'id="submit" class="btn btn-primary"'); ?></center>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- END Dynamic Table Full -->



                    <!-- END Dynamic Table Simple -->
                </div>
                <!-- END Page Content -->

           <?php $this->load->view('school/_include/footer'); ?>
