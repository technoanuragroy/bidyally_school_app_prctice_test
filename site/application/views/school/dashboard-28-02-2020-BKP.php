<?php $this->load->view('school/_include/header');
?>
<script src="<?php echo base_url(); ?>_js/jQuery-plugin-progressbar.js"></script>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        //$('.activeProgress').not('.activeProgress:last-child').append("<div class='progressBarLine'></div>")
    });

    function sendVerificationEmail() {
        $('.loader').show();
        $('#email_button').prop('disabled', true);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/main/sendVerificationEmail",
            //data: "class_id=" + class_id,
            success: function (msg) {
                //alert(msg);
                //console.log(msg);
                $('.loader').hide();

                if (msg != "") {
                    $('.email_message').css('display', 'block');
                    $('.email_message').show();

                } else {
                    $(".section").html("");
                }
            }
        });
    }
</script>

<div class="content">
    <?php //echo "<pre>";print_r($this->session->all_userdata()); ?>
    <div class="col-md-12">
        <?php if ($this->session->flashdata("s_message")) { ?>
            <!-- Success Alert -->
            <div class="alert alert-success alert-dismissable s_message" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
            </div>
            <!-- END Success Alert -->
        <?php } ?>
        <?php if ($this->session->flashdata("e_message")) { ?>
            <!-- Danger Alert -->
            <div class="alert alert-danger alert-dismissable e_message" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
            </div>
            <!-- END Danger Alert -->
        <?php } ?>
    </div >
    <div class="loader dashboard_loader" style="display: none;">
        <i class="fa fa-3x fa-cog fa-spin"></i>
    </div>
    <div class="col-md-12">
        <?php 
            $check_email_verification = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'email_verification', 'and id = "' . $this->session->userdata('school_id') . '"');
            if ($check_email_verification == 0) {
        ?>
                <p>Email verification is pending! To verify email please click <a href="javascript:" onclick="sendVerificationEmail();">HERE</a></p>
        <?php         
            } 
        ?>
    </div>
    
    <!------ Account Progress Box------->
    <?php 
        // Show the account progress until the progress is 100%
        if(isset($account_progress)) {
    ?>        
            <div class="row " data-toggle="appear">

                <div class="col-xs-12 progress_wrpper">
                    <div class="progress_contain">

                        <h1 class="progress_heading">Setup Progress.<small>You are on your way!</small></h1>

                        <div class="progress_main_block">
                            <div class="progress-bar1" data-percent="<?php echo $account_progress['percentage']; ?>" data-duration="1000" data-color="#fdd734,#363636"></div>
                        </div>

                        <div class="progress_content">
                            <ul class="progress_content_listing">

                                <li class="<?php echo $account_progress['account_created']; ?>">
                                    <a href="<?php echo base_url(); ?>school/user/dashBoard">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_01.png"></picture>
                                            <p>account created</p>
                                        </span>
                                    </a>
                                </li>

                                <li class="<?php echo $account_progress['account_verified']; ?>">
                                    <a href="<?php echo base_url(); ?>school/main/userVerification">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_02.png"></picture>
                                            <p>account verified</p>
                                        </span>
                                    </a>    
                                </li>

                                <li class="<?php echo $account_progress['create_session']; ?>" id="create_session">
                                    <a href="<?php echo base_url(); ?>school/user/addSession">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_03.png"></picture>
                                            <p>create session</p>
                                        </span>
                                    </a>    
                                </li>

                                <li class="<?php echo $account_progress['create_class_section']; ?>" id="create_class_section">
                                    <?php
                                        $class_section_link = 'school/user/addClass';
                                        if($account_progress['create_class'] == 'activeProgress' AND $account_progress['create_section'] == '') {
                                            $class_section_link = 'school/user/addSection';
                                        }
                                    ?>
                                    <a href="<?php echo base_url().$class_section_link; ?>">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_04.png"></picture>
                                            <p>create class, section</p>
                                        </span>
                                    </a>    
                                </li>

                                <li class="<?php echo $account_progress['create_semesters']; ?>" id="create_semesters">
                                    <a href="<?php echo base_url(); ?>school/user/addSemester">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_05.png"></picture>
                                            <p>create semesters</p>
                                        </span>
                                    </a>    
                                </li>

                                <li class="<?php echo $account_progress['add_staff']; ?>" id="add_staff">
                                    <a href="<?php echo base_url(); ?>school/user/addTeacher">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_06.png"></picture>
                                            <p>add staff</p>
                                        </span>
                                    </a>    
                                </li>

                                <li class="<?php echo $account_progress['create_students']; ?>" id="create_students">
                                    <a href="<?php echo base_url(); ?>school/user/addStudent">
                                        <span>
                                            <picture><img src="<?php echo base_url(); ?>_images/progress_icon_07.png"></picture>
                                            <p>create students</p>
                                        </span>
                                    </a>    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <script>		
                        $(".progress-bar1").loading();
                </script>
            </div>     
    <?php
        }
    ?>
    
    <!------ Students, Parents, SMS Credits, Teachers/Staffs Boxes ------->
    <div class="row " data-toggle="appear">
        <!-- Row #1 -->				
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">

                <div class="block-content block-content-full clearfix" data-toggle="tooltip" data-placement="top" title="Active Students/Total License">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-bag fa-3x text-body-bg-dark"></i>
                    </div>
                    <?php $no_of_license = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'no_of_license', 'and id = "' . $this->session->userdata('school_id') . '"'); //DEFAULT_LICENSE;  ?>
                    <div class="font-size-h3 font-w600 license" data-toggle="countTo" data-speed="1000" data-to="<?php echo $no_of_students; ?>"></div><span class="license_font">/<?php echo $no_of_license; ?></span>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Students</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)" >
                <div class="block-content block-content-full clearfix" data-toggle="tooltip" data-placement="top" title="Active Parents/Total Parents">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-wallet fa-3x text-body-bg-dark"></i>
                    </div>
                    <div class="font-size-h3 font-w600 license" data-toggle="countTo" data-speed="1000" data-to="<?php echo $no_of_parents_active; ?>"></span></div><span class="license_font">/<?php echo $no_of_parents; ?></span>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Parents</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-envelope-open fa-3x text-body-bg-dark"></i>
                    </div>
                    <?php $sms_credit = $this->my_custom_functions->get_available_sms_credits_of_school($this->session->userdata('school_id')); ?>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="<?php echo $sms_credit; ?>">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">SMS Credits</div>
                </div>
            </a>
        </div>        
        <div class="col-6 col-xl-3">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <i class="si si-users fa-3x text-body-bg-dark"></i>
                    </div>
                    <?php $teacher_count = $this->my_custom_functions->get_perticular_count(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '"'); ?>
                    <div class="font-size-h3 font-w600" data-toggle="countTo" data-speed="1000" data-to="<?php echo $teacher_count; ?>">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Teacher/Staff</div>
                </div>
            </a>
        </div>
        <!-- END Row #1 -->
    </div>

    <!------ Attendance, Class Activity Charts ------->
    <?php 
        // Show the charts when the account progress is 100%
        if(!isset($account_progress)) {
    ?>            
            <div class="row" data-toggle="appear">
                <!-- Row #2 -->
                <div class="col-md-6">
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">
                                Overall <small>attendance</small>
                            </h3>
                            <div class="block-options">

                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="pull-all">

                                <canvas id="speedChart"></canvas>
                            </div>
                        </div>                                  
                        <div class="block-content">
                            <div class="row items-push">
                                <div class="col-6 col-sm-4 text-center text-sm-left">
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">This Month</div>
                                    <div class="font-size-h4 font-w600"><?php echo $attendance_count['present_month_attendance']; ?></div>
                                    <!--                                            <div class="font-w600 text-success">
                                    <?php //if($attendance_count['month_flag'] == 1){ ?>
                                                                                    <i class="fa fa-caret-up"></i> +<?php //echo $attendance_count['avg_attendance'];  ?>%
                                    <?php //}else{ ?>
                                                                                    <i class="fa fa-caret-down"></i> -3%
                                    <?php //} ?>
                                                                                </div>-->
                                </div>
                                <div class="col-6 col-sm-4 text-center text-sm-left">
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">This Week</div>
                                    <div class="font-size-h4 font-w600"><?php echo $attendance_count['present_week_attendance']; ?></div>
                                    <!--                                            <div class="font-w600 text-danger">
                                                                                    <i class="fa fa-caret-down"></i> -3%
                                                                                </div>-->
                                </div>
                                <div class="col-12 col-sm-4 text-center text-sm-left">
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Today</div>
                                    <div class="font-size-h4 font-w600"><?php echo $attendance_count['todays_attendance']; ?></div>
                                    <!--                                            <div class="font-w600 text-success">
                                                                                    <i class="fa fa-caret-up"></i> +9%
                                                                                </div>-->
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">
                                Overall <small>class activities</small>
                            </h3>
                            <div class="block-options">
                                <!--                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                                            <i class="si si-refresh"></i>
                                                                        </button>
                                                                        <button type="button" class="btn-block-option">
                                                                            <i class="si si-wrench"></i>
                                                                        </button>-->
                            </div>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="pull-all">

                                <canvas id="speedChartClass"></canvas>
                            </div>
                        </div>
                        <div class="block-content bg-white">
                            <div class="row items-push">
                                <div class="col-6 col-sm-4 text-center text-sm-left">
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">This Month</div>
                                    <div class="font-size-h4 font-w600"><?php echo $activity_count['month_activity_list']; ?></div>
                                    <!--                                            <div class="font-w600 text-success">
                                                                                    <i class="fa fa-caret-up"></i> +4%
                                                                                </div>-->
                                </div>
                                <div class="col-6 col-sm-4 text-center text-sm-left">
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">This Week</div>
                                    <div class="font-size-h4 font-w600"><?php echo $activity_count['week_activity_list']; ?></div>
                                    <!--                                            <div class="font-w600 text-danger">
                                                                                    <i class="fa fa-caret-down"></i> -7%
                                                                                </div>-->
                                </div>
                                <div class="col-12 col-sm-4 text-center text-sm-left">
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Today</div>
                                    <div class="font-size-h4 font-w600"><?php echo $activity_count['day_activity_list']; ?></div>
                                    <!--                                            <div class="font-w600 text-success">
                                                                                    <i class="fa fa-caret-up"></i> +35%
                                                                                </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <!--                        <canvas id="myChart1" width="400" height="400"></canvas>-->


                <script type="text/javascript">
                    ///////////////////////////////////   PIE CHART START    ///////////////////////////////


                    var speedCanvas = document.getElementById("speedChart").getContext('2d');

                    Chart.defaults.global.defaultFontFamily = "Lato";
                    Chart.defaults.global.defaultFontSize = 18;

                    var dataFirst = {
                        label: "Last week",
                        data: [<?php echo $previous_week_data; ?>],
                        lineTension: .4,
                        fill: true,
                        borderColor: '#42A5F5',
                        backgroundColor: 'rgba(208, 232, 252, .4)'
                    };

                    var dataSecond = {
                        label: "This week",
                        data: [<?php echo $current_week_data; ?>],
                        lineTension: .4,
                        fill: true,
                        borderColor: '#42A5F5',
                        backgroundColor: 'rgba(99, 180, 245, .6)'
                    };

                    var speedData = {
                        labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
                        datasets: [dataFirst, dataSecond]
                    };

                    var chartOptions = {
                        legend: {
                            display: false,
                            position: 'top',
                            labels: {
                                boxWidth: 80,
                                fontColor: 'black',
                            }
                        },
                        scales: {
                            yAxes: [{
                                    display: false,
                                    ticks: {
                                        display: false
                                    },
                                    gridLines: {
                                        display: false,
                                    },
                                }],
                            xAxes: [{
                                    ticks: {
                                        display: false
                                    },
                                    gridLines: {
                                        display: false,
                                    },
                                }]
                        }

                    };

                    var lineChart = new Chart(speedCanvas, {
                        type: 'line',
                        data: speedData,
                        options: chartOptions,
                    });


                    ////////////////////////////////////////    BAR CHART START   ///////////////////////////////////// 


                    var speedCanvas = document.getElementById("speedChartClass").getContext('2d');

                    Chart.defaults.global.defaultFontFamily = "Lato";
                    Chart.defaults.global.defaultFontSize = 18;

                    var dataFirst = {
                        label: "Last week",
                        data: [<?php echo $prev_week_acivity_data; ?>],
                        lineTension: .4,
                        fill: true,
                        borderColor: '#9CCC65',
                        backgroundColor: 'rgba(190,232,143, .4)'
                    };

                    var dataSecond = {
                        label: "This week",
                        data: [<?php echo $current_week_acivity_data; ?>],
                        lineTension: .4,
                        fill: true,
                        borderColor: '#9CCC65',
                        backgroundColor: 'rgba(210,232,186, .6)'
                    };

                    var speedData = {
                        labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
                        datasets: [dataFirst, dataSecond]
                    };

                    var chartOptions = {
                        legend: {
                            display: false,
                            position: 'top',
                            labels: {
                                boxWidth: 80,
                                fontColor: 'black',
                            }
                        },
                        scales: {
                            yAxes: [{
                                    display: false,
                                    ticks: {
                                        display: false
                                    },
                                    gridLines: {
                                        display: false,
                                    },
                                }],
                            xAxes: [{
                                    ticks: {
                                        display: false
                                    },
                                    gridLines: {
                                        display: false,
                                    },
                                }]
                        }

                    };

                    var lineChart = new Chart(speedCanvas, {
                        type: 'line',
                        data: speedData,
                        options: chartOptions,
                    });
                </script>

                <!-- END Row #2 -->                        
            </div>
    <?php
        }
    ?>


    <div class="row" data-toggle="appear">
        <?php
        if (!empty($events)) {
            foreach ($events as $row) {
                ?>

                <!-- Row #3 -->
                <div class="col-md-4">
                    <div class="block">
                        <div class="block-content block-content-full">
                            <div class="py-20 text-center">
                                <div class="mb-20">
                                    <i class="fa fa-tree fa-4x text-primary"></i>
                                </div>
                                <div class="font-size-h4 font-w600"><?php echo $row['title']; ?></div>
                                <div class="text-muted"><?php echo date('d M Y', strtotime($row['start_date'])); ?></div>
                                <div class="pt-20">
                                    <!--                                            <a class="btn btn-rounded btn-alt-primary" href="javascript:void(0)">
                                                                                    <i class="fa fa-cog mr-5"></i> Manage list
                                                                                </a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        }
        ?>

        <!-- END Row #3 -->
    </div>
    <div class="row" data-toggle="appear">

    </div>
    <div class="row" data-toggle="appear">
        <!-- Row #5 -->
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/manageTimeTable">
                <div class="block-content ribbon ribbon-bookmark ribbon-success ribbon-left">
                    <!--                                    <div class="ribbon-box">15</div>-->
                    <p class="mt-5">
                        <i class="si si-calendar fa-3x"></i>
                    </p>
                    <p class="font-w600">Timetable</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/manageNotice">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-notebook fa-3x"></i>
                    </p>
                    <p class="font-w600">Notice</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/addStudent">
                <div class="block-content ribbon ribbon-bookmark ribbon-primary ribbon-left">
                    <!--                                    <div class="ribbon-box">3</div>-->
                    <p class="mt-5">
                        <i class="si si-users fa-3x"></i>
                    </p>
                    <p class="font-w600">Student Add</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/attendanceReport">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-bar-chart fa-3x"></i>
                    </p>
                    <p class="font-w600">Report</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/changePassword">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-settings fa-3x"></i>
                    </p>
                    <p class="font-w600">Setting</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-link-shadow text-center" href="<?php echo base_url(); ?>school/user/logout">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-logout fa-3x"></i>
                    </p>
                    <p class="font-w600">Logout</p>
                </div>
            </a>
        </div>
        <!-- END Row #5 -->
    </div>
</div>

<?php $this->load->view('school/_include/footer'); ?>