
<?php $this->load->view('school/_include/header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.copier_link').click(function () {
            var fees_container_html = $(".examSubjectcopy").html();
            $(".examSubjectAppend").append(fees_container_html);

            $(".examSubjectAppend").find(".subject").each(function (i, e) {
                $(this).attr("name", "subject[" + i + "]");
            });
            $(".examSubjectAppend").find(".total_marks").each(function (i, e) {
                $(this).attr("name", "total_marks[" + i + "]");
            });
            $(".examSubjectAppend").find(".pass_marks").each(function (i, e) {
                $(this).attr("name", "pass_marks[" + i + "]");
            });
        });



    });
    function remove_fees(e) {

        $(e).closest(".row").remove();
    }

    $(document).ready(function () {
        $('#exam_start_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#exam_end_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#publish_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
    });


</script> 
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Exam</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    <?php echo form_open_multipart('school/user/addExam', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    <?php
                    //echo "<pre>";print_r($exam_data); 
                    foreach ($exam_data['exam'] as $row_exam) {
                        ?>
                        <div class="form-group">
                            <div class="form-material">
                                <select required class="form-control" id="term" name="term">
                                    <option value="">Select Term</option>
                                    <?php
                                    foreach ($term_list as $term) {
                                        if ($term['id'] == $row_exam['term_id']) {
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = "";
                                        }
                                    
                                    ?>
                                    <option value="<?php echo $term['id']; ?>" <?php echo $selected; ?>><?php echo $term['term_name']; ?></option>
                                    <?php } ?>
                            </select>
                            <label for="term">Term</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" id="exam_name" name="exam_name" placeholder="Enter exam name" value="<?php echo $row_exam['exam_name']; ?>">
                            <label for="exam_name">Exam Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" id="exam_disp_name" name="exam_disp_name" placeholder="Enter exam display name" value="<?php echo $row_exam['exam_display_name']; ?>">
                            <label for="exam_disp_name">Exam Display Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control e_s_d" id="exam_start_date" name="exam_start_date" placeholder="Enter exam start date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($row_exam['exam_start_date'])) ?>">
                            <label for="exam_start_date">Exam Start Date</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control e_n_d" id="exam_end_date" name="exam_end_date" placeholder="Enter exam end date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($row_exam['exam_end_date'])) ?>">
                            <label for="exam_end_date">Exam End Date</label>
                        </div>
                    </div>
                    <?php 
                    $publish_date_time = date('d/m/Y h:i A',$row_exam['publish_date']);
                    $publish_date = explode(" ",$publish_date_time);
                    
                    $publish_time = explode(":",$publish_date[1]);
                    $hour = (int)$publish_time[0];
                    $minute = (int)$publish_time[1];
                    
                    ?>


                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control e_p_d" id="publish_date" name="publish_date" placeholder="Enter exam publish date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php echo $publish_date[0]; ?>">
                                    <label for="period_start_time">Exam Publish Date</label>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <?php $hour_list = $this->config->item('hour_list'); ?>
                                    <select required="" name="start_hour" class="form-control">
                                        <option value="">Select Hour</option>
                                        <?php foreach ($hour_list as $key => $val) { 
                                            if($hour == $key){
                                            $selected = "selected='slected'";
                                        }else{
                                            $selected = "";
                                        }
                                            ?>
                                            <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="period_start_time">Select Start Hour</label>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <select required="" name="start_minute" class="form-control">
                                        <option value="">Select Minute</option>
                                        <?php for ($i = 0; $i < 60; $i+=1) { 
                                            if($i == $minute){
                                        $selected = "selected='slected'";
                                        }else{
                                            $selected = "";
                                        }
                                            ?>
                                            <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="period_start_time">Select Start Minute</label>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="form-material">
                                    <select required="" name="start_meridian" class="form-control">
                                        <option value="">Select</option>
                                    <option value="AM" <?php if($publish_date[2] == 'AM'){echo "selected";} ?>>A.M</option>
                                    <option value="PM" <?php if($publish_date[2] == 'PM'){echo "selected";} ?>>P.M</option>

                                    </select>
                                    <label for="period_start_time">Select Start Meridian</label>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <!--                    <div style="border:1px solid gray"></div>-->


<?php } ?>



                    <div class="examSubjectAppend">
                        <?php foreach($exam_data['score_data'] as $row_score){ //echo "<pre>";print_r($row_score);?>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <select required="" name="subject[]" class="form-control subject">
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subject_list as $subject) {
                                                if($subject['id'] == $row_score['subject_id']){
                                                    $selected = "selected='selected'";
                                                }else{
                                                    $selected = "";
                                                }
                                                ?>
                                                <option value="<?php echo $subject['id']; ?>" <?php echo $selected; ?>><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="period_start_time">Select Subject</label>
                                    </div> 
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <input required type="text" class="form-control total_marks" name="total_marks[]" placeholder="Enter total marks" value="<?php echo $row_score['full_marks'] ?>">
                                        <label for="total_marks">Total Marks</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <input required type="text" class="form-control pass_marks" name="pass_marks[]" placeholder="Enter pass marks" value="<?php echo $row_score['pass_marks'] ?>">
                                        <label for="pass_marks">Pass Marks</label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>


                    <div class="form-group">
                        <div class="form-material">

                            <label for="copy_existing_exam"><a href="javascript:" class="copier_link"><i class="fa fa-plus"></i> Copy from existing exam</a></label>
                        </div>
                    </div>



                    <!--                    <div class="form-group">
                                            <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                                <input class="custom-control-input" type="checkbox" name="combine_rslt" id="combine_rslt" value="1">
                                                <label class="custom-control-label" for="combine_rslt">Copy from existing exam</label>
                                            </div>
                                        </div>-->






                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>

                    <div class="examSubjectcopy" style="display: none;">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <select required="" name="subject[]" class="form-control subject">
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subject_list as $subject) { ?>
                                                <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="period_start_time">Select Subject</label>
                                    </div> 
                                </div>
                                <a href="javascript:" class="" onclick="remove_fees(this);"><i class="fa fa-minus-circle"></i>&nbsp;Remove</a>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <input required type="text" class="form-control total_marks" name="total_marks[]" placeholder="Enter total marks">
                                        <label for="total_marks">Total Marks</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <input required type="text" class="form-control pass_marks" name="pass_marks[]" placeholder="Enter pass marks">
                                        <label for="pass_marks">Pass Marks</label>
                                    </div> 
                                </div>

                            </div>

                        </div>
                    </div>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".e_s_d").keydown(function (event) {
            return false;
        });
        $(".e_n_d").keydown(function (event) {
            return false;
        });
        $(".e_p_d").keydown(function (event) {
            return false;
        });
    });

</script>

<?php $this->load->view('school/_include/footer'); ?>


