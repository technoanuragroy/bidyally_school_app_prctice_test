<?php $this->load->view('school/_include/header'); ?>

    <div class="content">   
        <h2 class="content-heading">Charts</h2>
        
        <div class="block">
            <div class="block-content block-content-full">
                <div class="row" data-toggle="appear">
                    <!-- Row #2 -->
                    <div class="col-md-6">
                        <div class="block">
                            <div class="block-header">
                                <h3 class="block-title">
                                    Overall <small>attendance</small>
                                </h3>
                                <div class="block-options">

                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <div class="pull-all">

                                    <canvas id="speedChart"></canvas>
                                </div>
                            </div>  

                            <div class="block-content">
                                <div class="row items-push">
                                    <div class="col-6 col-sm-4 text-center text-sm-left">
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">This Month</div>
                                        <div class="font-size-h4 font-w600"><?php echo $attendance_count['present_month_attendance']; ?></div>
            <!--                                            <div class="font-w600 text-success">
                                            <?php //if($attendance_count['month_flag'] == 1){ ?>
                                            <i class="fa fa-caret-up"></i> +<?php //echo $attendance_count['avg_attendance']; ?>%
                                            <?php //}else{ ?>
                                            <i class="fa fa-caret-down"></i> -3%
                                            <?php //} ?>
                                        </div>-->
                                    </div>
                                    <div class="col-6 col-sm-4 text-center text-sm-left">
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">This Week</div>
                                        <div class="font-size-h4 font-w600"><?php echo $attendance_count['present_week_attendance']; ?></div>
            <!--                                            <div class="font-w600 text-danger">
                                            <i class="fa fa-caret-down"></i> -3%
                                        </div>-->
                                    </div>
                                    <div class="col-12 col-sm-4 text-center text-sm-left">
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Today</div>
                                        <div class="font-size-h4 font-w600"><?php echo $attendance_count['todays_attendance']; ?></div>
            <!--                                            <div class="font-w600 text-success">
                                            <i class="fa fa-caret-up"></i> +9%
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="block">
                            <div class="block-header">
                                <h3 class="block-title">
                                    Overall <small>class activities</small>
                                </h3>
                                <div class="block-options">
            <!--                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                        <i class="si si-refresh"></i>
                                    </button>
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-wrench"></i>
                                    </button>-->
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <div class="pull-all">

                                        <canvas id="speedChartClass"></canvas>
                                </div>
                            </div>
                            <div class="block-content bg-white">
                                <div class="row items-push">
                                    <div class="col-6 col-sm-4 text-center text-sm-left">
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">This Month</div>
                                        <div class="font-size-h4 font-w600"><?php echo $activity_count['month_activity_list']; ?></div>
            <!--                                            <div class="font-w600 text-success">
                                            <i class="fa fa-caret-up"></i> +4%
                                        </div>-->
                                    </div>
                                    <div class="col-6 col-sm-4 text-center text-sm-left">
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">This Week</div>
                                        <div class="font-size-h4 font-w600"><?php echo $activity_count['week_activity_list']; ?></div>
            <!--                                            <div class="font-w600 text-danger">
                                            <i class="fa fa-caret-down"></i> -7%
                                        </div>-->
                                    </div>
                                    <div class="col-12 col-sm-4 text-center text-sm-left">
                                        <div class="font-size-sm font-w600 text-uppercase text-muted">Today</div>
                                        <div class="font-size-h4 font-w600"><?php echo $activity_count['day_activity_list']; ?></div>
            <!--                                            <div class="font-w600 text-success">
                                            <i class="fa fa-caret-up"></i> +35%
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            <!--                        <canvas id="myChart1" width="400" height="400"></canvas>-->

                    <script type="text/javascript">
                        ///////////////////////////////////   PIE CHART START    ///////////////////////////////

                        var speedCanvas = document.getElementById("speedChart").getContext('2d');

                        Chart.defaults.global.defaultFontFamily = "Lato";
                        Chart.defaults.global.defaultFontSize = 18;

                        var dataFirst = {
                            label: "Last week",
                            data: [<?php echo $previous_week_data; ?>],
                            lineTension: .4,
                            fill: true,
                            borderColor: '#42A5F5',
                            backgroundColor: 'rgba(208, 232, 252, .4)'
                        };

                        var dataSecond = {
                            label: "This week",
                            data: [<?php echo $current_week_data; ?>],
                            lineTension: .4,
                            fill: true,
                            borderColor: '#42A5F5',
                            backgroundColor: 'rgba(99, 180, 245, .6)'
                        };

                        var speedData = {
                            labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
                            datasets: [dataFirst, dataSecond]
                        };

                        var chartOptions = {
                            legend: {
                                display: false,
                                position: 'top',
                                labels: {
                                    boxWidth: 80,
                                    fontColor: 'black',
                                }
                            },
                            scales: {
                                yAxes: [{
                                        display: false,
                                        ticks: {
                                            display: false
                                        },
                                        gridLines: {
                                            display: false,
                                        },
                                    }],
                                xAxes: [{
                                        ticks: {
                                            display: false
                                        },
                                        gridLines: {
                                            display: false,
                                        },
                                    }]
                            }

                        };

                        var lineChart = new Chart(speedCanvas, {
                            type: 'line',
                            data: speedData,
                            options: chartOptions,
                        });

                        ////////////////////////////////////////    BAR CHART START   ///////////////////////////////////// 

                        var speedCanvas = document.getElementById("speedChartClass").getContext('2d');

                        Chart.defaults.global.defaultFontFamily = "Lato";
                        Chart.defaults.global.defaultFontSize = 18;

                        var dataFirst = {
                            label: "Last week",
                            data: [<?php echo $prev_week_acivity_data; ?>],
                            lineTension: .4,
                            fill: true,
                            borderColor: '#9CCC65',
                            backgroundColor: 'rgba(190,232,143, .4)'
                        };

                        var dataSecond = {
                            label: "This week",
                            data: [<?php echo $current_week_acivity_data; ?>],
                            lineTension: .4,
                            fill: true,
                            borderColor: '#9CCC65',
                            backgroundColor: 'rgba(210,232,186, .6)'
                        };

                        var speedData = {
                            labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
                            datasets: [dataFirst, dataSecond]
                        };

                        var chartOptions = {
                            legend: {
                                display: false,
                                position: 'top',
                                labels: {
                                    boxWidth: 80,
                                    fontColor: 'black',
                                }
                            },
                            scales: {
                                yAxes: [{
                                        display: false,
                                        ticks: {
                                            display: false
                                        },
                                        gridLines: {
                                            display: false,
                                        },
                                    }],
                                xAxes: [{
                                        ticks: {
                                            display: false
                                        },
                                        gridLines: {
                                            display: false,
                                        },
                                    }]
                            }

                        };

                        var lineChart = new Chart(speedCanvas, {
                            type: 'line',
                            data: speedData,
                            options: chartOptions,
                        });                                                                                                                                                         
                    </script>

                    <!-- END Row #2 -->                        
                </div>
            </div>    
        </div>        
    </div>

<?php $this->load->view('school/_include/footer'); ?>