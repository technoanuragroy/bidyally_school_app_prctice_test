<?php $this->load->view('school/_include/header'); ?>
<!-- END Footer -->
<style>
/*    .invalid-feedback {
        width: 50% !important;
    }*/
</style>
<script type="text/javascript">

    function get_section_list() {
         $('.loader').show();
        var class_id = $('.class').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });


    }
    
    function getStudentList() {
    $('.loader').show();
        var class_id = $('.class').val();
        var section_id = $('.section').val();
        
         $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_student_list",
            data: 'class_id='+class_id+'&section_id='+section_id,
            success: function (msg) {
                
                $('.loader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".student").html(msg);
                } else {
                    $(".student").html("");
                }
            }
        });
    }
    

    $(document).ready(function () {
        $('#start_date').datepicker({
            dateFormat: 'dd-mm-yy',
        });
        $('#end_date').datepicker({
            dateFormat: 'dd-mm-yy',
        });
    });
    
    function cap_the_section(val) {
        
        if(val == 1){
            $('.row_std').slideUp('slow');
            $('.teacher_type').slideDown('slow');
            $('.section').prop('required',false);
        }
        if(val == 2){
            $('.teacher_type').slideUp('slow');
            
            
            $('.row_std').slideDown('slow');
            $('.section').prop('required',true);
        }
    }
</script>
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Activity report</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('school/user/generateActivityReport', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    <div class="form-group row">
                        <label class="col-12">Type</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="report_type" id="example-inline-radio1" value="1" onclick="cap_the_section('1');">
                                <label class="custom-control-label" for="example-inline-radio1">Teacher activity report </label>
                            </div>
                            <div  class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="report_type" id="example-inline-radio2" value="2" onclick="cap_the_section('2');"> 
                                <label class="custom-control-label" for="example-inline-radio2">Student activity report</label>
                            </div>


                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material">
                                <select required="" class="form-control class" id="class" name="class" onchange="get_section_list();">

                                    <option value="">Select Class</option>
                                    <?php
                                    $selected = '';
                                    foreach ($class_list as $class) {

                                        ?>
                                        <option value="<?php echo $class['id']; ?>" <?php //echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="material-gridf">Select Class</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material">
                                <select required="" class="form-control section" id="section" name="section" onchange="getStudentList();">
                                    <option value="">Select Section</option>
                                </select>
                                <label for="section">Select Section</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row row_std" style="display:none;">
                        <div class="col-12">
                            <div class="form-material">
                                <select class="form-control student" id="student" name="student">
                                    <option value="">Select Student</option>
                                </select>
                                <label for="section">Select Student</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row teacher_type">
                        <div class="col-6">
                            
                            <div class="form-material">
                                <select class="form-control teacher_id" id="teacher_id" name="teacher_id">

                                    <option value="">Select Teacher</option>
                                    <?php
                                    
                                    foreach ($teacher_list as $teacher) {

                                        ?>
                                        <option value="<?php echo $teacher['id']; ?>" <?php //echo $selected; ?>><?php echo $teacher['name']; ?></option>
                                    <?php } ?>
                                </select>
                                <label for="material-gridf">Select Teacher</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-material">
                                <select class="form-control note_type" id="note_type" name="note_type">

                                    <option value="">Select Note Type</option>
                                    
                                        <option value="1">Class note</option>
                                        <option value="2">Home note</option>
                                        <option value="3">Assignment note</option>
                                   
                                </select>
                                <label for="material-gridf">Select Note Type</label>
                            </div>
                        </div>
                    </div>

                   


                    <div class="form-group row">
                        <div class="col-6">
                            
                            <div class="form-material">
                                <input required="" type="text" class="form-control" id="start_date" name="start_date" placeholder="Start Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d-m-Y'); ?>">
                                <label for="start_date">Start Date</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-material">
                                <input required="" type="text" class="form-control" id="end_date" name="end_date" placeholder="End Date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d-m-Y'); ?>">
                                <label for="end_date">End Date</label>
                            </div>
                        </div>
                    </div>



                   

                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<?php $this->load->view('school/_include/footer'); ?>
           



