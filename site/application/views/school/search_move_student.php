<?php $this->load->view('school/_include/header'); ?>





<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Select Student For Move</h2>

    <!-- Dynamic Table Full -->
    <div class="block">

        <div class="block-content block-content-full">


<form method="post" action="<?php echo base_url(); ?>school/user/moveStudents" class="js-validation-material" id="move_form">
            <?php
            if (!empty($students)) {
                $i = 1;
                ?>
                <table class="table table-bordered table-striped table-vcenter check_align" id="example">
                    <thead>
                        <tr>
                            <th>
                                <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                    <input class="custom-control-input" type="checkbox" name="example-inline-checkboxall" id="checkboxall" value="0">
                                    <label class="custom-control-label" for="checkboxall">Select All</label>
                                </div>
                            </th>
                            <th class="text-center">Sl No.</th>
                            <th>Student name</th>
                            <th>Class</th>
                            <th>Section</th>
                            <th>Roll No</th>
                            <th>Father Name</th>
                            <th>Mother Name</th>
                            <th>Registered Numbers</th>

                        </tr>
                    </thead>
                    <tbody>
                        
                        
                        <?php
                        foreach ($students as $row) {


                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                            //$encrypted = $row['id'];
                            ?>
                            <tr>
                                <td style="text-align:center;">

                                    <div class="custom-control custom-checkbox custom-control-inline mb-5" style="margin-right:0;">
                                        <input required="" class="custom-control-input checkboxTeacher" type="checkbox" name="student[]" id="example-inline-checkbox<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>">
                                        <label class="custom-control-label" for="example-inline-checkbox<?php echo $row['id']; ?>"></label>
                                    </div>



                                </td>
                                <td class="text-center"><?php echo $i; ?></td>
                                <td><?php echo $row['name']; ?></td>
                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $row['class_id'] . '"'); ?></td>
                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $row['section_id'] . '"'); ?></td>
                                <td><?php echo $row['roll_no']; ?></td>
                                <td><?php echo $row['father_name']; ?></td>
                                <td><?php echo $row['mother_name']; ?></td>
                                <td>
                                    <?php
                                    $phone_number = '';
                                    $parent_ids = $this->my_custom_functions->get_multiple_data(TBL_PARENT_KIDS_LINK, 'and student_id = "' . $row['id'] . '"');
                                    //echo "<pre>";print_r($parent_ids);
                                    if (!empty($parent_ids)) {
                                        foreach ($parent_ids as $rows) {
                                            $ph_nn = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $rows['parent_id'] . '"');
                                            if ($ph_nn != '') {
                                                $phone_number .= $ph_nn . ',';
                                            }
                                        }
                                        $phone_numbers = rtrim($phone_number, ',  ');
                                        $phone_numbers = ltrim($phone_numbers, ',');
                                        $phone_numbers = trim($phone_numbers, ',');
                                        echo $phone_numbers;
                                    }
                                    ?>
                                </td>

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>  
                    </tbody>
                </table>

                <h2 class="content-heading">Move to</h2>

                <div class="load">
                <div id="div_slideDown_move">
                    <div class="loader loader3" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                        <div class="form-row">
                            <div class="form-group form-material col-md-4">
                                <select required="" class="form-control class_move" id="class" name="class" onchange="get_section_list_move();">
                                    <option value="">Select Class</option>
                                    <?php
                                    $selected = '';
                                    foreach ($class_list as $class) {
                                        if ($post_data['class_id'] != '') {
                                            if ($post_data['class_id'] == $class['id']) {

                                                $selected = 'selected="selected"';
                                            } else {

                                                $selected = '';
                                            }
                                        }
                                        ?>
                                        <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                    <?php } ?>

                                </select>
                                <label for="status">Select Class</label>
                            </div>
                            <div class="form-group form-material col-md-4">
                                <select required="" class="form-control section_move" id="section" name="section">
                                    <option value="">Select Section</option>
                                </select>
                                <label for="status">Select Section</label>
                            </div>
                            <div class="form-group form-material col-md-4">
                                <select required="" class="form-control semester_move" id="semester_move" name="semester">
                                    <option value="">Select Semester</option>
                                </select>
                                <label for="status">Select Semester</label>
                            </div>
                            <div class="form-material col-md-6">
                                <a href="javascript:" class="btn btn-primary" title="Delete" data-toggle="modal" data-target="#modal-top"  onclick="return confirmation();">Move</a>
                            </div>
                        </div>


                   

                    <br>
                </div>
                    
                </div>

            <?php } else { ?>
                <tr>
                    <td colspan="9">No student found</td>
                </tr>
                <?php
            }
            ?>
 </form>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>

<script type="text/javascript">

    function get_section_list_move() {
        var class_id = $('.class_move').val();
        get_semeseter_list_move(class_id);
        $('.loader3').show();
        

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list_move",
            data: "class_id=" + class_id,
            success: function (msg) {
                //alert(msg);
                $('.loader3').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section_move").html(msg);
                } else {
                    $(".section_move").html("");
                }
            }
        });


    }
    
     function get_semeseter_list_move(class_id) {
        $('.loader3').show();
        //var class_id = $('.class_move').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_list_move",
            data: "class_id=" + class_id,
            success: function (msg) {
                //alert(msg);
                $('.loader3').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".semester_move").html(msg);
                } else {
                    $(".semester_move").html("");
                }
            }
        });


    }

    function confirmation() {
    
        $('.message_block').html('<p>Are you sure that you want to move selected students?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete()");
    }
    function confirm_delete() {
        $('#move_form').submit();
       
    }
</script>
<script type="text/javascript">

    $('#checkboxall').on('click', function () {

        if (this.checked) {
            $('.checkboxTeacher').each(function () {
                this.checked = true;
            });
        } else {
            $('.checkboxTeacher').each(function () {
                this.checked = false;
            });
        }
    });
    $('.checkboxTeacher').on('click', function () {
        if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
            $('#checkboxall').prop('checked', true);
        } else {
            $('#checkboxall').prop('checked', false);
        }
    });





</script>