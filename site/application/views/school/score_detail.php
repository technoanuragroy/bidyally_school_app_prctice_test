
<?php $this->load->view('school/_include/header'); ?>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this semester?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteSemester/" + id;
    }
</script>
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Score</h2>

    <!-- Dynamic Table Full -->
    <div class="block">

        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <?php
        //echo "<pre>";print_r($student_detail); 
        foreach ($student_detail as $row) {
            ?>
            <div class="form-group" style="margin-left:20px;">
                <div class="form-material">
                    <label for="session_name">Student Name</label>
                    <span><?php echo $row['name']; ?></span>
                </div>
                <div class="form-material">
                    <label for="session_name">Class</label>
                    <span>
                        <?php
                        $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = ' . $row['class_id'] . '');
                        $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = ' . $row['section_id'] . '');
                        echo $class_name . ', ' . $section_name;
                        ?>
                    </span>
                </div>
                <div class="form-material">
                    <label for="session_name">Semester</label>
                    <span>
                        <?php
                        $semester_name = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'semester_name', 'and id = ' . $row['semester_id'] . '');
                        echo $semester_name;
                        ?>
                    </span>
                </div>
                <div class="form-material">

                    <label for="session_name">Exam</label>
                    <span>
                        <?php
                        $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
                        $exam_name = $this->my_custom_functions->get_particular_field_value(TBL_EXAM, 'exam_name', 'and id = ' . $exam_id . '');
                        echo $exam_name;
                        ?>
                    </span>
                </div>
            </div>
        <?php } ?>
        <div class="block-content block-content-full">

            <?php
            if (!empty($score_card)) {
//                echo "<pre>";
//                print_r($score_card);
                $i = 1;
                ?>
            <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                        <tr>

                            <th>Subject</th>
                            <th class="d-none d-sm-table-cell">Full marks</th>
                            <th class="d-none d-sm-table-cell">Pass Marks</th>
                            <th class="d-none d-sm-table-cell">Marks Obtained</th>

                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $full_marks = 0;
                        $pass_marks = 0;
                        foreach ($score_card as $row) {
                            $full_marks += $row['full_marks'];
                            $pass_marks += $row['pass_marks'];
                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                            //$encrypted = $row['id'];
                            ?>
                            <tr>

                                <td class="font-w600"><?php
                                    echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = ' . $row['subject_id'] . '');
                                    ?></td>
                                <td class="d-none d-sm-table-cell">
                                    <?php echo $row['full_marks'];
                                    ?></td>
                                <td class="d-none d-sm-table-cell">
                                    <?php echo $row['pass_marks']; ?></td>
                                <td class="text-center">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <?php $score = $this->my_custom_functions->get_particular_field_value(TBL_SCORE, 'score', 'and school_id = ' . $this->session->userdata('school_id') . ' and score_id = "'.$row['id'].'" and student_id = "'.$this->my_custom_functions->ablDecrypt($this->uri->segment(4)).'"'); ?>
                                            <input required type="text" class="form-control" name="marks_obtain[<?php echo $row['id']; ?>]" value="<?php echo $score; ?>"> 

                                        </div>
                                    </div>
                                </td>

                            </tr>
                            <?php
                            $i++;
                        }
                        ?>  
                            <tr>
                                <td>Grand Total</td>
                                <td><?php echo $full_marks; ?></td>
                                <td><?php echo $pass_marks; ?></td>
                                <td></td>
                            </tr>
                    </tbody>
                </table> 
            <input type="hidden" name="student_id" value="<?php echo $this->uri->segment(4); ?>">
            <input type="hidden" name="exam_id" value="<?php echo $this->uri->segment(5); ?>">
            <input type="hidden" name="next_student_index" value="<?php echo $next_student_index; ?>">
            
            <div class="form-group">
                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Save">
                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Save & Next">
                </div>
             <?php echo form_close(); ?>
            

                
                

            <?php } else { ?>
                <tr>
                    <td colspan="9">No classes found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>

<?php $this->load->view('school/_include/footer'); ?>
