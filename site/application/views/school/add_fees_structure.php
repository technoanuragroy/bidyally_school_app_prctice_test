<?php $this->load->view('school/_include/header');?>

    <!-- Page Content -->
    <div class="content">        
        <h2 class="content-heading">Add Fees Structure</h2>
        
        <div class="block">
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-12">
                        
                        <?php echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                        
                                <div class="form-group">
                                    <div class="form-material">
                                        <select name="semester_id" class="form-control" id="semester_id" required>
                                            <option value="">Select</option>
                                            <?php foreach ($semesters as $row) { ?>
                                                <option value="<?php echo $row['id']; ?>"><?php echo $row['semester_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="semester_id">Select Semester</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-material">
                                        <?php $cycle = $this->config->item('cycle') ?>
                                        <select name="cycle_id" class="form-control" id="cycle_id" required>
                                            <option value="">Select</option>
                                            <?php foreach ($cycle as $key => $row) { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                                            <?php } ?>
                                        </select>
                                        <label for="cycle_id">Cycle Type</label>
                                    </div>
                                </div>
                                                        
                                <div class="form-group">
                                    <div class="form-material">
                                        <textarea class="form-control" name="comment" id="section_comment"></textarea>
                                        <label for="section_comment">Comment</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                </div>
                        
                        <?php echo form_close(); ?>
                        
                    </div>
                </div>
            </div>
            
        </div>        
    </div>    

    <script type="text/javascript">
        
    </script>
    
<?php $this->load->view('school/_include/footer'); ?>