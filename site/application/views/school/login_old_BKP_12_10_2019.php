<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title><?php echo SITE_NAME; ?>:Sign In</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<!--        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url(); ?>assets/media/favicons/android-icon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-icon-180x180.png">-->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/media/favicons/favicon-32x32.png">
        
        <link rel="manifest" href="<?php echo base_url(); ?>assets/media/favicons/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/media/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/codebase.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/school_custom.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->

    </head>
    <body>
        <?php if($this->session->userdata('maintainance_message') && $this->session->userdata('maintainance_message') != ''){ ?>
        <div class="overlay_screen">
            <img src="<?php echo base_url(); ?>_images/repair.png">
            <p><?php echo $this->session->userdata('maintainance_message'); ?></p>
        </div>
        <?php } ?>

        <div id="page-container" class="main-content-boxed">

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="bg-gd-dusk">
                    <div class="hero-static content content-full bg-white invisible" data-toggle="appear">
                        <!-- Header -->
                        <div class="py-30 px-5 text-center">
                            <a class="link-effect font-w700" href="index.html">

                                <span class="font-size-xl text-primary-dark"><img src="<?php echo base_url(); ?>_images/main_logo.png"></span>
                            </a>
<!--                            <h1 class="h2 font-w700 mt-50 mb-10">Welcome to Your Dashboard</h1>-->
                            <h2 class="h4 font-w400 text-muted mb-0">Please sign in</h2>
                        </div>
                        <!-- END Header -->

                        <div class="col-sm-8 col-md-6 col-xl-4" style="margin: 0 auto;">
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?>
                                </div>
                                <!-- END Success Alert -->
                            <?php }
                            ?>
                        
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?>
                                </div>
                                <!-- END Success Alert -->
                            <?php }
                            ?>
                        </div>
                        <!-- Sign In Form -->
                        <div class="row justify-content-center px-5">
                            <div class="col-sm-8 col-md-6 col-xl-4">
                               <form class="js-validation-signin" id="reg_form" method="post" action="<?php echo base_url(); ?>school/main/login">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input type="text" class="form-control" id="login-username" name="username">
                                                <label for="login-username">Username</label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input type="password" class="form-control" id="login-password" name="password">
                                                <label for="login-password">Password</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row gutters-tiny">
                                        <div class="col-12 mb-10">

                                            <input type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary" name="submit" value="Submit">

                                        </div>
                                        <div class="col-sm-6 mb-5">
                                            <a class="btn btn-block btn-noborder btn-rounded btn-alt-secondary" href="<?php echo base_url(); ?>school/signUp">
                                                <i class="fa fa-plus text-muted mr-5"></i> New Account
                                            </a>
                                        </div>
                                        <div class="col-sm-6 mb-5">
                                            <a class="btn btn-block btn-noborder btn-rounded btn-alt-secondary" href="<?php echo base_url(); ?>school/forgetPassword">
                                                <i class="fa fa-warning text-muted mr-5"></i> Forgot password
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Sign In Form -->
                    </div>
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>


        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?php echo base_url(); ?>_js/op_auth_signin.min.js"></script>
    </body>
</html>
