<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this period?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/deleteTimeTable",
            data: "timetable_id=" + id,
            success: function (msg) {

                $(".tt_" + id).hide();

            }
        });
    }
</script>


<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Time Table</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url(); ?>school/user/addTimeTable" class="btn btn-primary btn-md-01" style="margin-bottom:10px;">Add Time Table</a>
            <a href="<?php echo base_url(); ?>school/user/tempTeacherAssign" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url().'school/user/tempTeacherAssign')?>" class="btn btn-primary ads btn-md-01" style="margin-bottom:10px;">Temporary teacher assign</a>
        </div>
        <div id="searchSection" style="display: block;">
            <div class="col-md-12 col-lg-12 no_padding_left load">
                <div class="ttmloader" style="display: none;">
                                        <i class="fa fa-3x fa-cog fa-spin"></i>
                                    </div>
                <form action="<?php echo current_url(); ?>" id="searchForm" method="post" accept-charset="utf-8" class="js-validation-material">
                    <div class="row">
                        <div class="col-lg-5 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Class</label>
                            <select required name="class_id" class="form-control class" onchange="get_section_list();">
                                <option value="">Select Class</option>
                                <?php
                                $selected = '';
                                foreach ($class_list as $class) {
                                    if ($post_data['class_id'] != '') {
                                        if ($post_data['class_id'] == $class['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-5 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Section</label>
                            <select name="section_id" class="form-control validate[required] section">
                                <option value="">Select Section</option>
                            </select>
                            <input type="hidden" name="post_sec_id" class="post_sec_id" value="<?php
                            if ($post_data['section_id'] != '') {
                                echo $post_data['section_id'];
                            }
                            ?>">
                        </div>
                        <div class="col-lg-2 sbtnWrap buttonSbmit" style="margin-top:10px; float:left;">
                            <input id="search" name="submit" value="Go" class="btn btn-primary btn-submit-01" type="submit">
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <?php //echo "<pre>";print_r($period);   ?>
            <div class="col-lg-6 no_padding_left form-group">

            </div>
        </div>





        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">
        <div class="table-responsive">
             <table class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr>
                        <th colspan="20" style="text-align:center;font-weight: bold">Time Table</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $day_list = $this->config->item('days_list');
                    if (!empty($period)) {
                        foreach ($period as $day => $row) {
                            ?>
                            <tr>
                                <td><?php echo $day_list[$day]; ?></td>
                                <?php foreach ($row as $subject_id => $p_list) { ?>
                                    <td class="tt_<?php echo $p_list['id'] ?>"><?php
                                        echo "<b>" . $p_list['period_name'] . "</b><br>";
                                        if ($p_list['subject_id'] != 0) {
                                            echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $p_list['subject_id'] . '"');
                                            echo "<br>";
                                        }
//                            echo "T - ".$this->my_custom_functions->get_particular_field_value(TBL_TEACHER,'name','and id = "'.$period_data['teacher_id'].'"');
                                        if ($p_list['teacher_id'] != 0) {
                                            $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $p_list['teacher_id'] . '"');
                                            $name = $this->my_custom_functions->get_short_name($teacher_name);
                                            
                                            echo '(' . $name . ')';
                                            
                                            
                                        } else {
                                            echo "";
                                        }
                                        echo "<br>";
                                        echo '(' . date('h:i A', strtotime($p_list['period_start_time'])) . ' - ' . date('h:i A', strtotime($p_list['period_end_time'])) . ')';
                                        echo "<br>";
                                        ?>
                                        <a href="<?php echo base_url(); ?>school/user/editTimeTable/<?php echo $this->my_custom_functions->ablEncrypt($p_list['id']); ?>"><i class="fa fa-edit"></i></a>
                                        <a href='javascript:' title='Delete' data-toggle='modal' data-target='#modal-top' onclick="return call_delete('<?php echo $p_list['id']; ?>')"><i class='fa fa-trash'></i></a>
                                        

                                    </td>
                                <?php } ?>


                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="9">No timetable found</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->


<script type="text/javascript" >
    $(document).ready(function () {
        $("#showsearch").click(function () {
            $("#searchSection").slideToggle();
        });



        var class_id = $('.class').val();
        var sec_id = $('.post_sec_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: {
                'class_id': class_id,
                'sec_id': sec_id
            },
            success: function (msg) {
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });


    });


    function get_section_list() {
    $('.ttmloader').show();
        var class_id = $('.class').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.ttmloader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });


    }
</script>
<?php $this->load->view('school/_include/footer'); ?>
        

