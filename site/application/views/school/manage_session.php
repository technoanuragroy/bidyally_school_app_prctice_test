
<?php $this->load->view('school/_include/header'); ?>
<style>
    #DataTables_Table_0_length{
        display: none;
    }
</style>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this session?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteSession/" + id;
    }
</script>
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Session</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url(); ?>school/user/addSession" class="btn btn-primary">Add Session</a>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">

            <?php
            if (!empty($session)) {
                $i = 1;
                ?>
                <div class="table_responsive">
    <!--                <table class="table table-striped table-vcenter js-dataTable-full">-->
                    <table class="table table-striped table-vcenter dataTable no-footer">
                        <thead>
                            <tr>

                                <th>Name</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th class="text-center" style="width: 15%;">Action</th>
    <!--                            <th class="text-center" style="width: 15%;">Delete</th>-->
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            foreach ($session as $row) {


                                $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                                //$encrypted = $row['id'];
                                ?>
                                <tr>

                                    <td class="font-w600"><?php echo $row['session_name']; ?></td>
                                    <td><?php echo date('d/m/Y', strtotime($row['from_date'])); ?> - <?php echo date('d/m/Y', strtotime($row['to_date'])); ?></td>
                                    <td>
                                        <?php if (date('Y-m-d') > $row['to_date']) { ?>
                                            <span class="badge badge-danger">Past</span>
                                        <?php } ?>
                                        <?php if (date('Y-m-d') > $row['from_date'] && date('Y-m-d') < $row['to_date']) { ?>
                                            <span class="badge badge-success">Ongoing</span>
                                        <?php } ?>
                                        <?php if ($row['from_date']> date('Y-m-d')) { ?>
                                            <span class="badge badge-primary">Future</span>
                                        <?php } ?>

                                    </td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url() . 'school/user/editSession/' . $encrypted; ?>" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo base_url() . 'school/user/deleteSession/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>
                                <?php
                                $i++;
                            }
                            ?>  
                        </tbody>
                    </table>   
                </div>

            <?php } else { ?>
                <tr>
                    <td colspan="9">No session found</td>
                </tr>
                <?php
            }
            ?>
            </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>

<?php $this->load->view('school/_include/footer'); ?>
