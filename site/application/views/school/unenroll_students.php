<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">
    .show_section {
        cursor: pointer;
    }
    .show_rollno {
        cursor: pointer;
    }
</style>

<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Unenroll Students</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="javascript:" class="btn btn-primary show_hide shift" id="clickbtnmove" data-target=".slidingDiv">Search Students</a>  
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>                  
       
        <div class="block-content block-content-full">
            
            <div id="div_slideDown_move" class="slidingDiv slider">                
                <form method="post" action="">

                    <div class="form-row load">
                        <div class="loader loader2" style="display: none;">
                            <i class="fa fa-3x fa-cog fa-spin"></i>
                        </div>
                        
                        <div class="form-material col-md-6">
                            <select class="form-control semester_id" name="semester_id" onchange="get_section_list();">
                                <option value="">Select Semester</option>
                                <?php foreach ($semesters as $semester) { ?>
                                        <option value="<?php echo $semester['id']; ?>"><?php echo $semester['semester_name']; ?></option>
                                <?php } ?>
                                        <option value="unenroll">Unenrolled</option>
                            </select>
                            <label for="semester_id">Select Semester</label>
                        </div>
                        
                        <div class="form-material col-md-6">
                            <select class="form-control section_id" name="section_id">
                                <option value="">Select Section</option>
                            </select>
                            <label for="section_id">Select Section</label>
                        </div>
                        
                        <div class="form-material col-md-6">
                            <input type="submit" name="submit" value="Search" class="btn btn-primary">
                        </div>
                    </div>
                    
                </form>  
                <br>
            </div>
            
            <h2 class="content-heading">Select Students To Enroll</h2>

            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    
                    <form method="post" action="" class="js-validation-material" id="enrollmentForm">
                        <?php
                            if (!empty($students)) {                                
                        ?>
                                <table class="table table-bordered table-striped table-vcenter check_align" id="example" style="margin-top: -20px;">
                                    <thead>
                                        <tr>
                                            <th width="80px">
                                                <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                                    <input class="custom-control-input" type="checkbox" name="example-inline-checkboxall" id="checkboxall" value="0">
                                                    <label class="custom-control-label" for="checkboxall">All</label>
                                                </div>
                                            </th>                                            
                                            <th>Student</th>
                                            <th>Current Enrollments</th>                                            
                                            <th>Father Name</th>                                            
                                            <th>Enter New Roll No</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php                                        
                                        foreach ($students as $row) {

                                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);  
                                            $active_semesters = $this->my_custom_functions->get_semesters_of_a_student($row['id'], $this->session->userdata("session_id"));                                            
                                    ?>
                                            <tr>
                                                <td>
                                                    <div class="custom-control custom-checkbox custom-control-inline mb-5" style="margin-right:0;">
                                                        <input class="custom-control-input checkboxStudent" type="checkbox" name="student[<?php echo $row['id']; ?>]" id="example-inline-checkbox<?php echo $row['id']; ?>" value="<?php echo $row['id']; ?>">
                                                        <label class="custom-control-label" for="example-inline-checkbox<?php echo $row['id']; ?>"></label>
                                                    </div>
                                                </td>                                                
                                                <td><?php echo $row['name']; ?></td>
                                                <td>
                                                    <table class="table table-bordered table-striped table-vcenter check_align">
                                                        <thead>
                                                            <tr>                                                                                                                                                                         
                                                                <th>Semester</th>                                            
                                                                <th width="100px">Section</th>                                            
                                                                <th width="100px">Roll No</th>
                                                                <th width="50px">Unenroll</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                foreach($active_semesters as $a_s) {
                                                                    $key = $a_s['id']."_".$row['id'];
                                                            ?>
                                                                    <tr class="link_<?php echo $key; ?>">
                                                                        <td>
                                                                            <?php echo $a_s['semester_name']; ?> 
                                                                        </td>
                                                                        <td>
                                                                            <div class="show_section_<?php echo $key; ?> show_section" data-key="<?php echo $key; ?>">
                                                                                <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $a_s['section_id'] . '"'); ?>
                                                                            </div>
                                                                            <div class="edit_section_<?php echo $key; ?> edit_section">
                                                                                <?php 
                                                                                    $section_list = $this->my_custom_functions->get_section_list("", $a_s['id']);                                                                               
                                                                                ?>        
                                                                                <select name="section_drop_<?php echo $key; ?>" class="form-control section_drop_<?php echo $key; ?> section_drop" data-key="<?php echo $key; ?>" data-enrollment="<?php echo $a_s['enrollment_id']; ?>">                                                                                    
                                                                                    <?php      
                                                                                        $selected = '';
                                                                                        foreach ($section_list as $section) {
                                                                                            if($a_s['section_id'] == $section['id']) {
                                                                                                $selected = 'selected="selected"';
                                                                                            } else {
                                                                                                $selected = '';
                                                                                            }
                                                                                            echo '<option value="' . $section['id'] . '" '.$selected.'>' . $section['section_name'] . '</option>';
                                                                                        }
                                                                                    ?>                                                                                
                                                                                </select>    
                                                                            </div>                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <div class="show_rollno_<?php echo $key; ?> show_rollno" data-key="<?php echo $key; ?>">
                                                                                <?php echo $a_s['roll_no']; ?>
                                                                            </div>
                                                                            <div class="edit_rollno_<?php echo $key; ?> edit_rollno">
                                                                                <input type="text" name="rollno_text_<?php echo $key; ?>" class="form-control rollno_text_<?php echo $key; ?> rollno_text" data-key="<?php echo $key; ?>" data-enrollment="<?php echo $a_s['enrollment_id']; ?>" value="<?php echo $a_s['roll_no']; ?>">
                                                                            </div>    
                                                                        </td>
                                                                        <td style="text-align: center;">
                                                                            <a href="javascript:;" class="btn" title="Confirm" data-toggle="modal" data-target="#modal-top"  onclick="return confirm_unenroll('<?php echo $a_s['enrollment_id']; ?>', '<?php echo $key; ?>', '<?php echo str_replace("'", "", $a_s['semester_name']); ?>');">
                                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                            <?php

                                                                }
                                                            ?>
                                                        </tbody>
                                                    </table>        
                                                </td>
                                                <td><?php echo $row['father_name']; ?></td>                                                  
                                                <td>
                                                    <div class="form-group form-material">
                                                        <input required="" class="form-control" type="text" name="roll_no[<?php echo $row['id']; ?>]" value="<?php echo $a_s['roll_no']; ?>">
                                                    </div>    
                                                </td> 
                                            </tr>
                                    <?php                                            
                                        }
                                    ?>  
                                    </tbody>
                                </table>

                                <h2 class="content-heading">Enroll To</h2>

                                <div class="load">
                                    <div id="div_slideDown_move">
                                        <div class="loader loader3" style="display: none;">
                                            <i class="fa fa-3x fa-cog fa-spin"></i>
                                        </div>
                                    
                                        <div class="form-row">
                                            <div class="form-group form-material col-md-4">
                                                <select required="" class="form-control session_id_move" name="session_id_move" onchange="get_semester_list_move();">
                                                    <option value="">Select Session</option>
                                                    <?php foreach ($sessions as $session) { ?>
                                                            <option value="<?php echo $session['id']; ?>"><?php echo $session['session_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <label for="session_id_move">Select Session</label>
                                            </div>
                                            
                                            <div class="form-group form-material col-md-4">
                                                <select required="" class="form-control semester_id_move" name="semester_id_move" onchange="get_section_list_move();">
                                                    <option value="">Select Semester</option>
                                                </select>
                                                <label for="semester_id_move">Select Semester</label>                                                
                                            </div>
                                            
                                            <div class="form-group form-material col-md-4">
                                                <select required="" class="form-control section_id_move" name="section_id_move">
                                                    <option value="">Select Section</option>
                                                </select>
                                                <label for="section_id_move">Select Section</label>
                                            </div>
                                                                                        
                                            <div class="form-group form-material col-md-4">
                                                <a href="javascript:" class="btn btn-primary" title="Confirm" data-toggle="modal" data-target="#modal-top"  onclick="return confirmation();">Enroll</a>
                                            </div>
                                        </div>                                    
                                    </div>
                                </div>
                        <?php                         
                            } else { 
                        ?>
                                No student found                                
                        <?php
                            }
                        ?>
                    </form>                   
                </div>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table Full -->

    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>

<script type="text/javascript">

    $(document).ready(function() {
        //all target div's which has to be toggled has the class slider
        var $sliders = $(".slider").hide();
        //all anchors which has to trigger the slide has the class show_hide and has a data-* property data-target whose value is the selector to find the target div
        $(".show_hide").show();

        $('.show_hide').click(function () {
            var $target = $($(this).data('target'));
            $sliders.not($target).stop(true, true).slideUp();
            $target.stop(true, true).slideToggle();
        });
        
        $('#checkboxall').on('click', function () {
            if (this.checked) {
                $('.checkboxStudent').each(function () {
                    this.checked = true;
                });
            } else {
                $('.checkboxStudent').each(function () {
                    this.checked = false;
                });
            }
        });
        
        $('.checkboxStudent').on('click', function () {
            if ($('.checkboxStudent:checked').length == $('.checkboxStudent').length) {
                $('#checkboxall').prop('checked', true);
            } else {
                $('#checkboxall').prop('checked', false);
            }
        });
        
        $(".edit_section").hide();
        $(".edit_rollno").hide();
        
        $(".show_section").on("click", function() {
            var key = $(this).data("key");
            $(this).hide();            
            $(".edit_section_"+key).show();
        });
        
        $(".show_rollno").on("click", function() {
            var key = $(this).data("key");
            $(this).hide();            
            $(".edit_rollno_"+key).show();
        });
        
        $(".section_drop").on("blur", function() {
            
            var key = $(this).data("key");
            var enrollment_data = {
                'enrollment': $(this).data("enrollment"),
                'section': $(this).val(),
                'key': key
            };                      
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>school/enrollment/update_enrollment_data_ajax",
                data: enrollment_data,
                success: function (msg) {                   
                    $(".edit_section_"+key).hide();
                    $(".show_section_"+key).text(msg);
                    $(".show_section_"+key).show();
                }
            });
        });
        
        $(".rollno_text").on("blur", function() {
            
            var key = $(this).data("key");
            var enrollment_data = {
                'enrollment': $(this).data("enrollment"),
                'rollno': $(this).val(),
                'key': key
            };                      
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>school/enrollment/update_enrollment_data_ajax",
                data: enrollment_data,
                success: function (msg) {                   
                    $(".edit_rollno_"+key).hide();
                    $(".show_rollno_"+key).text(msg);
                    $(".show_rollno_"+key).show();
                }
            });
        });
    });
    
    function get_section_list() {
        
        var semester_id = $('.semester_id').val();
       
        $('.loader2').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader2').hide();
                $(".section_id").html("");
                if (msg != "") {                    
                    $(".section_id").html(msg);
                }
            }
        });
    }
    
    function get_semester_list_move() {
    
        var session_id = $('.session_id_move').val();
       
        $('.loader3').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_dropdown_from_session",
            data: "session_id=" + session_id,
            success: function (msg) {
                $('.loader3').hide();
                $(".semester_id_move").html("");
                $(".section_id_move").find('option').not(':first').remove();
                if (msg != "") {                    
                    $(".semester_id_move").html(msg);
                }
            }
        });
    }

    function get_section_list_move() {
    
        var semester_id = $('.semester_id_move').val();
       
        $('.loader3').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader3').hide();
                $(".section_id_move").html("");
                if (msg != "") {                    
                    $(".section_id_move").html(msg);
                }
            }
        });
    }       
    
    function confirm_unenroll(enrollment, key, semester) {
               
        $('.message_block').html('<p>Are you sure that you want to unenroll the student from "'+semester+'" semester?</p>');
        $('.btn-alt-success').attr('onclick', "unenroll('"+enrollment+"','"+key+"')");
    }
    
    function unenroll(enrollment, key) {
     
        var unenrollment_data = {
            'enrollment': enrollment,
            'key': key
        }
        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/enrollment/unenroll_ajax",
            data: unenrollment_data,
            success: function (msg) {               
                if(msg != "") {
                    $(".link_"+key).remove();
                }
            }
        });
    }    

    function confirmation() {
    
        $('.message_block').html('<p>Are you sure that you want to move selected students?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_enroll()");
    }
    
    function confirm_enroll() {
        
        $('#enrollmentForm').submit();       
    }
</script>
