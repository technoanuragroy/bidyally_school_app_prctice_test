<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#combine_rslt').click(function() {
            if($(this).prop("checked") == true) {
                $('#term_label').attr('required','true');
                $('.final_label').slideDown();
            } else if($(this).prop("checked") == false) {
                $('#term_label').removeAttr('required');
                $('.final_label').slideUp();
            }
        });
    });

    function get_semester_name() {
        
        var class_name = $('#class option:selected').text();
        var session_name = $('.session_name').text();
        var semester_name = class_name + ' (' + session_name + ')';

        if ($('#class').val() == '') {
            semester_name = '';
        }
        if ($('#session').val() == '') {
            semester_name = '';
        }

        $('#semester_name').val(semester_name);
    }
</script> 
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Semester</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                            <div class="form-group">
                                <div class="form-material">
                                    <select required class="form-control" id="class" name="class" onchange="get_semester_name();">
                                        <option value="">Select Class</option>
                                        <?php foreach ($classes as $class) { ?>
                                                <option value="<?php echo $class['id']; ?>"><?php echo $class['class_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="class">Class</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="form-material">
                                    <span class="session_name" id="session_name"><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SESSION, 'session_name', 'and id="' . $this->session->userdata('session_id') . '"'); ?></span>                                    
                                    <label for="session_name">Session</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" id="semester_name" name="semester_name" placeholder="Enter a semester name">
                                    <label for="semester_name">Semester Name</label>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                    <input class="custom-control-input" type="checkbox" name="combine_rslt" id="combine_rslt" value="1">
                                    <label class="custom-control-label" for="combine_rslt">Combine Result</label>
                                </div>
                            </div>

                            <div class="form-group final_label" style="display:none;">
                                <div class="form-material">
                                    <input type="text" class="form-control" id="term_label" name="term_label" placeholder="Enter final term label">
                                    <label for="term_label">Final Term Label</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                            </div>
                    
                    <?php echo form_close(); ?>                    
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->


<?php $this->load->view('school/_include/footer'); ?>


