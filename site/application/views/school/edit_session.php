
<?php $this->load->view('school/_include/header');
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#session_start').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });
         $('#session_end').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });
    });
</script> 
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Edit Session</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="session_name" name="session_name" placeholder="Enter a session name" value="<?php echo $session['session_name']; ?>">
                            <label for="session_name">Session Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="session_start" name="session_start" value="<?php echo date('d/m/Y',strtotime($session['from_date'])); ?>" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                            <label for="session_start">Session Start Date</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="session_end" name="session_end" value="<?php echo date('d/m/Y',strtotime($session['to_date'])); ?>" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                            <label for="session_end">Session End Date</label>
                        </div>
                    </div>

<!--                    <div class="form-group">
                        <div class="form-material">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" selected>Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            <label for="status">Status</label>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <input type="hidden" name="session_id" value="<?php echo $this->uri->segment(4); ?>">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->


<?php $this->load->view('school/_include/footer'); ?>


