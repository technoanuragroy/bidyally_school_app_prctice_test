<?php $this->load->view('school/_include/header');?>

    <!-- Page Content -->
    <div class="content">                    
        <h2 class="content-heading">Edit Fees Structure</h2>

        <div class="block">
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-12">
                        
                        <div class="form-group">
                            <div class="form-material">
                                <?php $semester_name = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'semester_name','and id = "'.$fees_structure_detail['semester_id'].'"'); ?>
                                <label>Semester - <?php echo $semester_name; ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-material">                                            
                                <label>Cycle Type - <?php echo ucfirst($this->config->item('cycle')[$fees_structure_detail['cycle_type']]); ?></label>
                            </div>
                        </div>
                                                                        
                        <hr/>
                        
                        <h4>Edit Fees Structure</h4>     
                        <?php echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                                                                                          
                                <div class="form-group">
                                    <div class="form-material">
                                        <textarea class="form-control" name="comment" id="section_comment"><?php echo $fees_structure_detail['general_comment']; ?></textarea>
                                        <label for="section_comment">Comment</label>
                                    </div>
                                </div>
                        
                                <!-- Late payment related settings -->
                                <div class="form-group">                                    
                                    <div class="form-material">
                                        <label>
                                            <input type="checkbox" name="late_payment_setting" class="late_payment_setting" value="1" <?php if($fees_structure_detail['late_payment_setting'] == 1) { echo 'checked="checked"'; } ?>>&nbsp;Enable Late Fine
                                        </label>                                
                                    </div>
                                </div>
                                
                                <?php 
                                    // If late payment setting is enabled
                                    $display = '';
                                    if($fees_structure_detail['late_payment_setting'] == 0) {         
                                        $display = 'style="display: none;"';
                                    }                               
                                ?>
                                
                                <div class="late_payment_info" <?php echo $display; ?>>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="late_payment_day_limit" value="<?php echo $fees_structure_detail['late_payment_day_limit']; ?>" placeholder="Late Payment Day Duration">
                                            <label>Late Fine Charged After: {Number of days} <a href="#" data-toggle="tooltip" data-placement="right" title="After how many days it will be considered as late, for example: if set to 20, then after 20 days of fees payment cycle that payment will be treated as late."><i class="fa fa-question-circle"></i></a></label>
                                        </div>
                                    </div> 

                                    <div class="form-group ">
                                        <div class="form-material">
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input class="custom-control-input" type="radio" name="late_payment_type" id="late_payment_type1" value="1" <?php if($fees_structure_detail['late_payment_type'] == 1) { echo 'checked="checked"'; } ?>>
                                                <label class="custom-control-label" for="late_payment_type1">Fixed</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input class="custom-control-input" type="radio" name="late_payment_type" id="late_payment_type2" value="2" <?php if($fees_structure_detail['late_payment_type'] == 2) { echo 'checked="checked"'; } ?>>
                                                <label class="custom-control-label" for="late_payment_type2">Percentage</label>
                                            </div>        
                                            <label for="late_payment_type1">Fine Type</label>                                        
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="text" class="form-control" name="late_payment_amount" value="<?php echo $fees_structure_detail['late_payment_amount']; ?>" placeholder="Late Payment Amount">
                                            <label>Fine Amount</label>
                                        </div>
                                    </div> 
                                </div>     

                                <div class="form-group">
                                    <input type="hidden" name="fees_id" value="<?php echo $this->my_custom_functions->ablEncrypt($fees_structure_detail['id']); ?>">
                                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                                </div>
                        
                        <?php echo form_close(); ?>
                                   
                    </div>
                </div>
            </div>
            
        </div>                   
    </div>         
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('.late_payment_setting').on("click", function() {
                if($(this).prop("checked")) { 
                    $(".late_payment_info").slideDown();
                } else {
                    $(".late_payment_info").slideUp();
                }
            });
        });
    </script>    
        
<?php $this->load->view('school/_include/footer'); ?>