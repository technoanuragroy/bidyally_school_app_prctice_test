<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">
    #inputEmail-error {
        bottom: 5px;
        padding-left: 16px;
    }
    #inputMobile-error {
        bottom: 5px;
        padding-left: 16px;
    }
</style>
       
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Student</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                            <h2 class="content-heading">Basic Details</h2>
                            
                            <div class="form-group reg_valid">
                                <div class="form-material">
                                    <input required type="text" class="form-control validate_reg_no" name="reg_no" id="reg_no" placeholder="Enter Registration Number" value="">
                                    <label for="reg_no">Registration Number</label>
                                </div>             
                                <div class="exists_error animated fadeInDown" style="display: none;">Registration number not available</div>                                
                            </div>                            

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="">
                                    <label for="name">Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control dobb" id="date_of_birth" name="date_of_birth" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                                    <label for="date_of_birth">Date of Birth</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <select required class="form-control" id="status" name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Other</option>
                                    </select>
                                    <label for="status">Select Gender</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="father_name" id="father_name" placeholder="Enter Father Name" value="">
                                    <label for="father_name">Father Name</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Enter Mother Name" value="">
                                    <label for="mother_name">Mother Name</label>
                                </div>
                            </div>
                            
                            <h2 class="content-heading">Enrollment Detail</h2>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <select required name="semester_id" class="form-control semester_id" onchange="get_section_list()">
                                        <option value="">Select Semester</option>
                                        <?php foreach ($semesters as $semester) { ?>
                                                <option value="<?php echo $semester['id']; ?>"><?php echo $semester['semester_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="semester_id">Select Semester</label>
                                </div>
                            </div>
                                                        
                            <div class="form-group">
                                <div class="form-material">
                                    <select required name="section_id" class="form-control section_id">
                                        <option value="">Select Section</option>
                                    </select>
                                    <label for="section_id">Select Section</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="roll_no" id="roll_no" placeholder="Enter Roll Number" value="" >
                                    <label for="roll_no">Roll Number</label>
                                </div>
                            </div>

                            <h2 class="content-heading">Personal Detail</h2>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" value="" >
                                    <label for="address">Address</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="number" class="form-control" name="aadhar_no" id="aadhar_no" placeholder="Enter Aadhar Number" value="" >
                                    <label for="aadhar_no">Aadhar Number</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="emg_contact_person" id="emg_contact_person" placeholder="Enter Emergency Contact Name" value="" >
                                    <label for="emg_contact_person">Emergency Contact Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="number" class="form-control" name="emg_contact_no" id="emg_contact_no" placeholder="Enter Emergency Contact Number" value="" >
                                    <label for="emg_contact_no">Emergency Contact Number</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="blood_group" id="blood_group" placeholder="Enter Blood Group" value="" autocomplete="new-password">
                                    <label for="blood_group">Blood Group</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <select class="form-control" id="status" name="status">
                                        <option value="">Select Status</option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <label for="status">Status</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-12" for="userphoto">Upload Photo</label>
                                <div class="col-12">
                                    <input type="file" id="adminphoto" name="adminphoto"><br>
                                    (Image size should be 200px*200px )
                                </div>
                            </div>

                            <h2 class="content-heading">Login Details</h2>
                            
                            <div class="add-details-container">
                                <div class="add-details-content">
                                    <div class="add-details-content-top">
                                        <h2 class="">Username Connected to Parents</h2>
                                        <p>
                                            These details can be used by parents/Guardians to login to the
                                            app to check progress and get regular updates of the student.
                                        </p>
                                    </div>
                                    
                                    <div class="contacts_preview">
                                        
                                    </div>                                                                                                          
                                    
                                    <div class="add-details-content-bottom" style="display: block;">
                                        <div class="dtls-bottom-contnr">
                                            <h2 class="">Add your details</h2>
                                            <div class="wpr-from-container">                                                                                                
                                                <div class="form-group row form-row-wrappr">
                                                    <label for="inputMobile" class="col-sm-2 col-form-label">Mobile</label>
                                                    <div class="col-sm-10">
                                                        <input type="number" required="" minlength="10" maxlength="10" class="form-control" id="inputMobile" placeholder="Enter Mobile Number">
                                                    </div>
                                                </div>

                                                <div class="form-group row form-row-wrappr">
                                                    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputName" placeholder="Enter Name">
                                                    </div>
                                                </div>

                                                <div class="form-group row form-row-wrappr">
                                                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="email" class="form-control" id="inputEmail" placeholder="Enter Email" style="text-transform: none;">
                                                    </div>
                                                </div>

                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="passwordType1" name="passwordType" value="1" class="custom-control-input passwordType" checked="true">
                                                    <label class="custom-control-label" for="passwordType1">
                                                        <span class="d-block">Set Date of Birth as Password </span>
                                                        (NOTE: student's DOB will be set as the parent's login 
                                                        password in ddmmyyyy format, so if the students
                                                        DOB is 27/02/2010 then the password will be set to 27022010.)
                                                    </label>
                                                </div>

                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="passwordType2" name="passwordType" value="2" class="custom-control-input passwordType">
                                                    <label class="custom-control-label" for="passwordType2">Set Manual Password</label>
                                                </div>

                                                <div class="form-group row form-row-wrappr manual_password" style="display: none;">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                                    <div class="col-sm-10">
                                                        <input type="password" class="form-control" id="inputPassword" placeholder="Enter Password">
                                                    </div>
                                                </div>

                                                <div class="button-container-from add_contact_container">
                                                    <a href="javascript:;" class="btn-002 addNewContact disabled">Add</a>  
                                                </div>                                                
                                            </div> 
                                        </div>    
                                    </div>
                                    
                                </div>                                
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit" id="submit_student">
                                <a href="<?php echo base_url(); ?>school/user/manageStudent"  class="btn btn-outline-danger">Back</a>
                                <a href="<?php echo base_url(); ?>school/user/bulkUploadStudent"  class="btn btn-alt-primary" style="float: right;">Bulk Upload</a>
                            </div>
                            
                    <?php echo form_close(); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>

<script type="text/javascript">
    
    function delete_contact(contact_row) {
        
        $(".contacts_preview .contact_"+contact_row).remove();

        var contact_count = 1;
        $(".contacts_preview .new_contact").each(function() {

            $(this).attr("class", "add-details-content-middle new_contact contact_"+contact_count);            
            $(this).find("input.input_mobile").attr("name", "input_mobile["+contact_count+"]");
            $(this).find("input.input_name").attr("name", "input_name["+contact_count+"]");
            $(this).find("input.input_email").attr("name", "input_email["+contact_count+"]");
            $(this).find("input.password_type").attr("name", "password_type["+contact_count+"]");
            $(this).find("input.input_password").attr("name", "input_password["+contact_count+"]");

            $(this).find("a.delete_contact").attr("onclick", "delete_contact("+contact_count+");");

            contact_count = Number(contact_count) + 1;
        });

        if($(".contacts_preview .new_contact").length == 0) {
            $(".contacts_preview").hide();
            $("#inputMobile").attr("required", "");
        }
    }

    $(document).ready(function () {        
        
        $("#inputMobile").on("keyup change paste blur", function() {
            
            // Remove space if a string with space is pasted
            $("#inputMobile").val($("#inputMobile").val().replace(/ /g, ""));

            var mobile_number = $("#inputMobile").val();

            if(mobile_number != "") {
                if(mobile_number.length == 10) {
                    $(".addNewContact").removeClass("disabled");
                } else {
                    $(".addNewContact").addClass("disabled");
                }
            } else {
                $(".addNewContact").addClass("disabled");
            }
        });
        
        // When password type changed
        $(".passwordType").click(function() {
            
            if($(this).val() == "1") {
                $(".manual_password").slideUp();
                $("#inputPassword").val("");
            } else if($(this).val() == "2") {
                $(".manual_password").slideDown();
            }
        });
        
        // A new contact is added
        $(".addNewContact").click(function() {
            
            $(".contacts_preview").show();
            
            var contact_count = Number($(".contacts_preview .new_contact").length) + 1;
            var new_contact = '<div class="add-details-content-middle new_contact contact_'+contact_count+'">'+
                                    '<div class="wrp-dtls-contnr">'+
                                        '<div class="block-dtls">'+
                                            '<label>Mobile Number</label>'+
                                            '<span>'+$("#inputMobile").val()+'</span>'+
                                            '<input type="hidden" class="input_mobile" name="input_mobile['+contact_count+']" value="'+$("#inputMobile").val()+'">'+
                                        '</div>'+
                                        '<div class="block-dtls">'+
                                            '<label>Full Name</label>'+
                                            '<span>'+$("#inputName").val()+'</span>'+
                                            '<input type="hidden" class="input_name" name="input_name['+contact_count+']" value="'+$("#inputName").val()+'">'+
                                        '</div>'+
                                        '<div class="block-dtls">'+
                                            '<label>Email</label>'+
                                            '<span>'+$("#inputEmail").val()+'</span>'+
                                            '<input type="hidden" class="input_email" name="input_email['+contact_count+']" value="'+$("#inputEmail").val()+'">'+
                                            '<input type="hidden" class="password_type" name="password_type['+contact_count+']" value="'+$(".passwordType:checked").val()+'">'+
                                            '<input type="hidden" class="input_password" name="input_password['+contact_count+']" value="'+$("#inputPassword").val()+'">'+
                                        '</div>'+
                                        '<div class="block-dtls block-dtls-last">'+
                                            '<label>Action</label>'+                                        
                                            '<span><a href="javascript:;" onclick="delete_contact('+contact_count+');" class="delete_contact"><i class="fa fa-trash"></i></a></span>'+
                                        '</div>'+
                                    '</div>'+    
                              '</div>';
                      
            $(".contacts_preview").append(new_contact); 
            
            // Reset the inputs
            $("#inputMobile").val("");
            $("#inputName").val("");
            $("#inputEmail").val("");
            $("#passwordType1").trigger("click");
            $("#inputPassword").val("");
            $(".addNewContact").addClass("disabled");
            
            $('html, body').animate({
                scrollTop: $(".contact_"+contact_count).offset().top - 100
            }, 500);       
            
            $("#inputMobile").removeAttr("required");
        });                                                        
        
        $('#date_of_birth').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });
        
        get_section_list();
        
        // Check registration number uniqueness
        $("#reg_no").on("keyup change paste blur", function() {
            
            var reg_val = $('#reg_no').val();

            if(reg_val != '') {

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/checkRegistrationNumber",
                    data: "reg_val=" + reg_val,
                    success: function (msg) {
                        if (msg == 0) {                            

                            $('.validate_reg_no').addClass('valid');
                            $('.reg_valid').removeClass('is-invalid');
                            $('.exists_error').slideUp(500);
                            $('#submit_student').show();
                        } else {

                            $('.validate_reg_no').removeClass('valid');
                            $('.reg_valid').addClass('is-invalid');
                            $('.exists_error').slideDown(500);
                            $('#submit_student').hide();
                        }
                    }
                });            
            } else {

                $('.exists_error').slideUp(500);
                $('#submit_student').hide();
            }
        });
    });
        
    function get_section_list() {
        
        var semester_id = $('.semester_id').val();
       
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader').hide();
                $(".section_id").html("");
                if (msg != "") {                    
                    $(".section_id").html(msg);
                }
            }
        });
    }
        
    $(".dobb").keydown(function (event) {
        return false;
    });

</script>




