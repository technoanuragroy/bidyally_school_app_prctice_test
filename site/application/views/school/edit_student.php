<?php $this->load->view('school/_include/header'); ?>

<style type="text/css">
    #inputEmail-error {
        bottom: 5px;
        padding-left: 16px;
    }
    #inputMobile-error {
        bottom: 5px;
        padding-left: 16px;
    }
</style>

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Edit Student</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                    </div>
                    <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                    <!-- Danger Alert -->
                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                    </div>
                    <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <?php //echo "<pre>";print_r($get_student_detail); ?>
        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                    <h2 class="content-heading">Basic Details</h2>
                    <div class="form-group reg_valid">
                        <div class="form-material">
                            <input required type="text" class="form-control" name="reg_no" id="reg_no" placeholder="Enter Registration Number" value="<?php echo $get_student_detail['student_detail'][0]['registration_no']; ?>">
                            <label for="reg_no">Registration Number</label>
                        </div>
                        <div class="exists_error animated fadeInDown" style="display: none;">Registration number not available</div>                                
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="<?php echo $get_student_detail['student_detail'][0]['name']; ?>">
                            <label for="name">Name</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control dobb" id="date_of_birth" name="date_of_birth" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y', strtotime($get_student_detail['student_detail'][0]['dob'])); ?>">
                            <label for="date_of_birth">Date of Birth</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <select required class="form-control" id="gender" name="gender">
                                <option value="">Select Gender</option>
                                <option value="1" <?php
                                if ($get_student_detail['student_detail'][0]['gender'] == 1) {
                                    echo "selected";
                                };
                                ?>>Male</option>
                                <option value="2" <?php
                                if ($get_student_detail['student_detail'][0]['gender'] == 2) {
                                    echo "selected";
                                };
                                ?>>Female</option>
                                <option value="3" <?php
                                if ($get_student_detail['student_detail'][0]['gender'] == 3) {
                                    echo "selected";
                                };
                                ?>>Other</option>
                            </select>
                            <label for="status">Select Gender</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" name="father_name" id="father_name" placeholder="Enter Father Name" value="<?php echo $get_student_detail['student_detail'][0]['father_name']; ?>">
                            <label for="father_name">Father Name</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Enter Mother Name" value="<?php echo $get_student_detail['student_detail'][0]['mother_name']; ?>">
                            <label for="mother_name">Mother Name</label>
                        </div>
                    </div>
                    <!--                    <h2 class="content-heading">Class Detail</h2>
                                        <div class="form-group">
                                            <div class="form-material">
                                                <select required name="class_id" class="form-control class" onchange="get_section_list()">
                                                    <option value="">Select Class</option>
                    <?php
                    foreach ($class_list as $class) {
                        if ($get_student_detail['student_detail'][0]['class_id'] == $class['id']) {
                            $selected = 'selected="selected"';
                        } else {
                            $selected = '';
                        }
                        ?>
                                                                <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                    <?php } ?>
                                                </select>
                                                <label for="day_id">Select Class</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material">
                    
                                                <select required name="semester_id" class="form-control semester">
                                                    <option value="">Select Semester</option>
                                                </select>
                    
                                                <label for="semester_id">Select Semester</label>
                                            </div>
                                            <input type="hidden" name="sem_id_db" class="sem_id_db" value="<?php echo $get_student_detail['student_detail'][0]['semester_id']; ?>">
                                        </div>
                    
                                        <div class="form-group">
                                            <div class="form-material">
                                                <select required name="section_id" class="form-control section">
                                                    <option value="">Select Section</option>
                                                </select>
                    
                                                <label for="day_id">Select Section</label>
                                            </div>
                                            <input type="hidden" name="sec_id_db" class="sec_id_db" value="<?php echo $get_student_detail['student_detail'][0]['section_id']; ?>">
                                        </div>
                    
                                        <div class="form-group">
                                            <div class="form-material">
                                                <input type="text" class="form-control" name="roll_no" id="roll_no" placeholder="Enter Roll Number" value="<?php echo $get_student_detail['student_detail'][0]['roll_no']; ?>" >
                                                <label for="roll_no">Roll Number</label>
                                            </div>
                                        </div>-->

                    <h2 class="content-heading">Personal Detail</h2>

                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" value="<?php echo $get_student_detail['student_detail'][0]['address']; ?>" >
                            <label for="address">Address</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <input type="number" class="form-control" name="aadhar_no" id="aadhar_no" placeholder="Enter Aadhar Number" value="<?php
                            if ($get_student_detail['student_detail'][0]['aadhar_no'] != 0) {
                                echo $get_student_detail['student_detail'][0]['aadhar_no'];
                            }
                            ?>" >
                            <label for="aadhar_no">Aadhar Number</label>
                        </div>
                    </div>




                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" name="emg_contact_person" id="emg_contact_person" placeholder="Enter Emergency Contact Name" value="<?php echo $get_student_detail['student_detail'][0]['emergency_contact_person']; ?>" >
                            <label for="emg_contact_person">Emergency Contact Name</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <input type="number" class="form-control" name="emg_contact_no" id="emg_contact_no" placeholder="Enter Emergency Contact Number" value="<?php
                            if ($get_student_detail['student_detail'][0]['emergency_contact_no'] != 0) {
                                echo $get_student_detail['student_detail'][0]['emergency_contact_no'];
                            }
                            ?>" >
                            <label for="emg_contact_no">Emergency Contact Number</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" name="blood_group" id="blood_group" placeholder="Enter Blood Group" value="<?php echo $get_student_detail['student_detail'][0]['blood_group']; ?>" autocomplete="new-password" >
                            <label for="blood_group">Blood Group</label>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="form-material">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" <?php
                                if ($get_student_detail['student_detail'][0]['status'] == 1) {
                                    echo "selected";
                                };
                                ?>>Active</option>
                                <option value="0" <?php
                                if ($get_student_detail['student_detail'][0]['status'] == 0) {
                                    echo "selected";
                                };
                                ?>>Inactive</option>
                            </select>
                            <label for="status">Status</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php
                        if ($get_student_detail['student_detail'][0]['file_url'] != '') {

                            $imgfile = $get_student_detail['student_detail'][0]['file_url'];
                            ?>
                            <img src="<?php echo $imgfile; ?>" />
                            <span class="manu_chk"><input type="checkbox" name="del_img" class="form-group" value="1">Remove Image</span>

                            <?php
                        } else {
                            $imgfile = base_url() . '_images/avatar15.jpg';
                            ?>

                            <img src="<?php echo $imgfile; ?>" style="width:200px;height:200px;" />
                        <?php } ?>
                        <label class="col-12" for="userphoto">Upload Photo</label>
                        <div class="col-12">
                            <input type="file" id="adminphoto" name="adminphoto"><br>
                            (Image size should be 200px*200px )
                        </div>
                    </div>




                    <div class="form-group">
                        <input type="hidden" name="student_id" id="student_id" value="<?php echo $get_student_detail['student_detail'][0]['id']; ?>">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit" id="submit_student">
                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                        <a href="<?php echo base_url(); ?>school/user/manualFeesBreakups/<?php echo $this->my_custom_functions->ablEncrypt($get_student_detail['student_detail'][0]['id']); ?>" class="btn btn-success min-width-125">Set Manual Fees</a>

                        <!-- Show receive payment button if school has payment option enabled -->
                        <?php
                        $payment_enabled = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_PAYMENT_SETTINGS, 'payment_enabled', 'and school_id = "' . $this->session->userdata('school_id') . '"');
                        if ($payment_enabled == 1) {
                            ?>        
                            <a href="<?php echo base_url(); ?>school/user/receiveStudentFees/<?php echo $this->my_custom_functions->ablEncrypt($get_student_detail['student_detail'][0]['id']); ?>" class="btn btn-success min-width-125">Receive Student Fees</a>
                            <?php
                        }
                        ?>     

                        <!-- Show enrollment history of a student -->      
                        <?php
                        if (!empty($enrollment_history)) {
                            ?>
                            <a href="javascript:;" class="btn btn-success min-width-125" onclick="$('.enrollment-history').slideToggle();">Show Enrollment History</a>
                            <?php
                        }
                        ?>
                    </div>

                    <?php
                    if (!empty($enrollment_history)) {
                        ?>  
                        <div class="form-group enrollment-history">
                            <h2 class="content-heading">Enrollment History</h2>
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th>Semester</th>
                                        <th>Section</th> 
                                        <th>Roll No</th>
                                        <th>From Date</th>
                                        <th>To Date</th>                                                                                
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($enrollment_history as $eh) {
                                    ?>
                                            <tr>
                                                <td><?php echo $eh['semester_name']; ?></td>
                                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id="' . $eh['section_id'] . '"'); ?></td>
                                                <td><?php echo $eh['roll_no']; ?></td>
                                                <td><?php echo $eh['from_date']; ?></td>
                                                <td><?php echo $eh['to_date']; ?></td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody> 
                            </table>
                        </div>    
                        <?php
                    }
                    ?>

                    <?php echo form_close(); ?>

                    <?php /*
                    <h2 class="content-heading">Username Connected to Parents</h2>
                    <div id="user"></div>
                    
                    <?php
                    //echo "<pre>";print_r($get_student_detail);
                    foreach ($get_student_detail['username_detail'] as $users) {
                        $parent_detail = $this->my_custom_functions->get_parent_detail($users['id']);
                        //echo "<pre>";print_r($parent_detail);
                        ?>
                        <div class="form-group main_box del_section_<?php echo $users['id']; ?>">
                            <div class="form-material">
                                <div class="noDel_wrap">
                                    <form method="post" action="<?php echo base_url(); ?>school/user/updateParentPhoneNumber">
                                        <label for="contact_no" class="contactNo cont_<?php echo $users['id']; ?> ">Contact Number
                                            <span class="showNo"><?php echo $users['username']; ?></span>
                                        </label>
                                        <input type="text" name="parent_pho_num" class="edit-input_<?php echo $users['id']; ?>" style="display:none;" />

                                        <div class="controls_<?php echo $users['id']; ?>">
                                            <a href="javascript:" data-toggle="modal" data-target="#modal-top" onclick="deleteUsername('<?php echo $users['id']; ?>', '<?php echo $this->uri->segment(4); ?>');"><span class="del_icon"><i class="si si-trash"></i></span></a>
                                            <a href="javascript:" onclick="open_edit_field('<?php echo $users['id']; ?>', '<?php echo $this->uri->segment(4); ?>')"><span class="del_icon"><i class="fa fa-edit"></i></span></a>
                                        </div>
                                        <input type="hidden" name="parent_id_no" value="<?php echo $users['id']; ?>">
                                        <input type="hidden" name="student_id_no" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="update_phone" value="update" class="btn btn-alt-primary update_button_<?php echo $users['id']; ?>" style="display:none;">
                                    </form>
                                </div>
                                <div class="noDel_wrap">
                                    <label for="contact_no" class="contactNo">Name</label>
                                    <span class="showNo"><?php
                                        if ($parent_detail[0]['name'] != '') {
                                            echo $parent_detail[0]['name'];
                                        } else {
                                            echo "--";
                                        }
                                        ?></span>


                                </div>
                                <div class="noDel_wrap">
                                    <label for="contact_no" class="contactNo">Email</label>
                                    <span class="showNo"><?php
                                        if ($parent_detail[0]['email'] != '') {
                                            echo $parent_detail[0]['email'];
                                        } else {
                                            echo "--";
                                        }
                                        ?></span>


                                </div>
                            </div>
                        </div>
                        <div class="show_msg_<?php echo $users['id']; ?>"></div>
                    <?php } ?>
                    <div class="popup_btn_wrap">

                        <a href="javascript:" class="btn btn-alt-primary open_pop">Add Phone Number(s)</a>
                        <div class="popWrap">
                            <div class="popInn">
                                <form method="post" action="<?php echo base_url(); ?>school/user/addMoreUsernames">
                                    <span><label>phone no(username)</label><input type="text" name="user_phone_no" class="usr" placeholder="Enter Phone No"></span>
                                    <span><label>Parent name</label><input type="text" name="user_full_name" class="usr" placeholder="Enter Name"></span>
                                    <span><label>email</label><input type="text" name="user_email" class="usr" placeholder="Enter Email"></span>
                                    <span><input type="hidden" name="stdnt_id_for_usrname" value="<?php echo $this->uri->segment(4); ?>"></span>
                                    <div class="passwordType">
                                        <p>password</p>
                                        <label class="radio_wrp"><input type="radio" name="user_password" class="usr" value="1"> DOB</label>
                                        <label class="radio_wrp"><input type="radio" name="user_password" class="usr" value="2" checked=""> Blank</label>
                                    </div>
                                    <input type="submit" value="Add" class="btn btn-alt-primary">
                                </form>
                            </div>
                        </div>
                    </div>


                    <div id="username_append"></div>
                    */ ?>
                    
                    <h2 class="content-heading" id="login_details">Login Details</h2>
                     
                    <?php if ($this->session->flashdata("s_username")) { ?>
                            <!-- Success Alert -->
                            <div class="alert alert-success alert-dismissable s_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("s_username"); ?></p>
                            </div>
                            <!-- END Success Alert -->
                    <?php } ?>
                    <?php if ($this->session->flashdata("e_username")) { ?>
                            <!-- Danger Alert -->
                            <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                <p class="mb-0"><?php echo $this->session->flashdata("e_username"); ?></p>
                            </div>
                            <!-- END Danger Alert -->
                    <?php } ?>
                    
                    <div class="add-details-container">
                        <div class="add-details-content">
                            <div class="add-details-content-top">
                                <h2 class="">Username Connected to Parents</h2>
                                <p>
                                    These details can be used by parents/Guardians to login to the
                                    app to check progress and get regular updates of the student.
                                </p>
                            </div>

                            <?php
                                $usernames = $get_student_detail['username_detail'];
                                
                                if(!empty($usernames)) {
                                    foreach($usernames as $users) {
                                        
                                        $parent_details = $this->my_custom_functions->get_parent_detail($users['id']);                                        
                            ?>                                        
                                        <div class="add-details-content-middle username_<?php echo $users['id']; ?>">
                                            <div class="wrp-dtls-contnr">
                                                <div class="block-dtls">
                                                    <label>Mobile Number</label>
                                                    <span><?php echo $users['username']; ?></span>
                                                </div>
                                                <div class="block-dtls">
                                                    <label>Full Name</label>
                                                    <span><?php echo $parent_details['name']; ?></span>
                                                </div>
                                                <div class="block-dtls">
                                                    <label>Email</label>
                                                    <span><?php echo $parent_details['email']; ?></span>
                                                </div>
                                                <div class="block-dtls block-dtls-last">
                                                    <label>Action</label>
                                                    <span><a href="javascript:;" class="btn-edit-click" data-parent="<?php echo $this->my_custom_functions->ablEncrypt($users['id']); ?>"><i class="fa fa-edit"></i></a></span>
                                                    <span><a href="javascript:;" data-toggle="modal" data-target="#modal-top" onclick="deleteUsername('<?php echo $users['id']; ?>', '<?php echo $this->uri->segment(4); ?>');"><i class="fa fa-trash"></i></a></span>
                                                </div>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                            ?>
                            
                            <div class="edit-details-content" id="edit-details-container">
                                <div class="dtls-bottom-contnr">
                                    <h2 class="">Edit your details</h2>
                                    <div class="wpr-from-container">
                                        
                                        <?php echo form_open('school/user/updateParentUsername/'.$this->uri->segment(4), array('id' => 'editUsernameForm', 'class' => 'js-validation-material')); ?>
                                        
                                                <div class="form-group row form-row-wrappr">
                                                    <label for="editMobile" class="col-sm-5 col-form-label">Mobile</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" required="" minlength="10" maxlength="10" class="form-control" name="editMobile" id="editMobile" placeholder="Enter Mobile Number">
                                                    </div>
                                                </div>
                                        
                                                <div class="form-group row form-row-wrappr">
                                                    <label for="editName" class="col-sm-5 col-form-label">Name</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" name="editName" id="editName" placeholder="Enter Name">
                                                    </div>
                                                </div>
                                        
                                                <div class="form-group row form-row-wrappr">
                                                    <label for="editEmail" class="col-sm-5 col-form-label">Email</label>
                                                    <div class="col-sm-7">
                                                        <input type="email" class="form-control" name="editEmail" id="editEmail" placeholder="Enter Email" style="text-transform: none;">
                                                    </div>
                                                </div>

                                                <div class="form-group row form-row-wrappr">
                                                    <label for="editPassword" class="col-sm-5 col-form-label">Enter New Password</label>
                                                    <div class="col-sm-7">
                                                        <input type="password" class="form-control" name="editPassword" id="editPassword" placeholder="Enter Password">
                                                    </div>
                                                </div>

                                                <div class="button-container-from">
                                                    <input type="hidden" name="parent_id" id="parent_id" value="">
                                                    <input type="submit" name="update_contact" value="Update" class="btn-002" style="border: none;">
                                                    <a href="javascript:" class="btn-003" id="cancel-link">Cancel</a> 
                                                </div>
                                            
                                        <?php echo form_close(); ?>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <?php 
                                if(count($usernames) > 0) { 
                                    $add_style = 'style="display: none;"';
                            ?>
                                    <div class="button-container-more"><a href="javascript:" class="btn-001 btn-check-in-expand">Add More <i class="fa fa-chevron-down"></i> </a></div>
                            <?php                             
                                } else {
                                    $add_style = 'style="display: block;"';
                                }
                            ?>
                            
                            <div class="add-details-content-bottom" <?php echo $add_style; ?>>
                                <div class="dtls-bottom-contnr">
                                    <h2 class="">Add your details</h2>
                                    <div class="wpr-from-container">  
                                        
                                        <?php echo form_open('school/user/addParentUsername/'.$this->uri->segment(4), array('id' => 'addUsernameForm', 'class' => 'js-validation-material')); ?>
                                        
                                                <div class="form-group row form-row-wrappr">
                                                    <label for="inputMobile" class="col-sm-2 col-form-label">Mobile</label>
                                                    <div class="col-sm-10">
                                                        <input type="number" required="" minlength="10" maxlength="10" class="form-control" name="inputMobile" id="inputMobile" placeholder="Enter Mobile Number">
                                                    </div>
                                                </div>

                                                <div class="form-group row form-row-wrappr">
                                                    <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="inputName" id="inputName" placeholder="Enter Name">
                                                    </div>
                                                </div>

                                                <div class="form-group row form-row-wrappr">
                                                    <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Enter Email" style="text-transform: none;">
                                                    </div>
                                                </div>

                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="passwordType1" name="passwordType" value="1" class="custom-control-input passwordType" checked="true">
                                                    <label class="custom-control-label" for="passwordType1">
                                                        <span class="d-block">Set Date of Birth as Password </span>
                                                        (NOTE: student's DOB will be set as the parent's login 
                                                        password in ddmmyyyy format, so if the students
                                                        DOB is 27/02/2010 then the password will be set to 27022010.)
                                                    </label>
                                                </div>

                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="passwordType2" name="passwordType" value="2" class="custom-control-input passwordType">
                                                    <label class="custom-control-label" for="passwordType2">Set Manual Password</label>
                                                </div>

                                                <div class="form-group row form-row-wrappr manual_password" style="display: none;">
                                                    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                                    <div class="col-sm-10">
                                                        <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Enter Password">
                                                    </div>
                                                </div>

                                                <div class="button-container-from add_contact_container">
                                                    <input type="submit" name="add_contact" value="Add" class="btn-002 addNewContact disabled" style="border: none;">                                                    
                                                    <a href="javascript:" class="btn-003" id="cancel-link-02">Cancel</a> 
                                                </div>      
                                        
                                        <?php echo form_close(); ?>
                                        
                                    </div> 
                                </div>    
                            </div>
                            
                        </div>                                
                    </div>
                            
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>

<script type="text/javascript">
    
    $(document).ready(function () {
        
        $("#inputMobile").on("keyup change paste blur", function() {
            
            // Remove space if a string with space is pasted
            $("#inputMobile").val($("#inputMobile").val().replace(/ /g, ""));

            var mobile_number = $("#inputMobile").val();

            if(mobile_number != "") {
                if(mobile_number.length == 10) {
                    $(".addNewContact").removeClass("disabled");
                } else {
                    $(".addNewContact").addClass("disabled");
                }
            } else {
                $(".addNewContact").addClass("disabled");
            }
        });
        
        // Add student details script
        $(".btn-check-in-expand").click(function () { 
            $(this).find('i').toggleClass('fas fa-chevron-up');
            $(this).find('i').toggleClass('fas fa-chevron-down');
                        
            $(".add-details-content-bottom").slideToggle(500);           
        }); 
        
        $("#cancel-link-02").click(function(){
            $(".add-details-content-bottom").slideUp("slow");             
        });
        
        // When password type changed
        $(".passwordType").click(function() {
            
            if($(this).val() == "1") {
                $(".manual_password").slideUp();
                $("#inputPassword").val("");
            } else if($(this).val() == "2") {
                $(".manual_password").slideDown();
            }
        });
       
        // Edit student details script
        $(".btn-edit-click").click(function() {
            
            var parent_id = $(this).data("parent");
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>school/user/getParentDetails",
                data: "parent_id=" + parent_id,
                success: function (msg) {
                    if (msg != "") {
                        var data = JSON.parse(msg);
                        $("#editMobile").val(data.username);
                        $("#editName").val(data.name);
                        $("#editEmail").val(data.email);
                        $("#parent_id").val(parent_id);
                        
                        $("#edit-details-container").slideDown("slow");
                        $('html, body').animate({
                            scrollTop: $("#edit-details-container").offset().top - 100
                        }, 500);       
                    }
                }
            });            
        });
        
        $("#cancel-link").click(function(){
            $("#edit-details-container").slideUp("slow");
        });
        
        
        $('#date_of_birth').datepicker({
            dateFormat: 'dd/mm/yy',
        });

        $("#add_field").click(function () {
            $("#username_append").append('<div class="form-group"><div class="form-material"><input required type="number" class="form-control" name="contact_no[]" placeholder="Enter Contact Number" value="" ><label for="contact_no">Contact Number</label></div><a href="javascript:" onClick="remove_fiels();">Remove</a></div>');
        });

        $(".remove_fiels").click(function () {
            $(this).remove();
        });

        $(".enrollment-history").hide();

        var class_id = $('.class').val();
        var sec_id = $('.sec_id_db').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id + "&sec_id=" + sec_id,
            success: function (msg) {

                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });

        var class_id = $('.class').val();
        var sem_id = $('.sem_id_db').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_list",
            data: "class_id=" + class_id + "&sem_id=" + sem_id,
            success: function (msg) {

                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".semester").html(msg);
                } else {
                    $(".semester").html("");
                }
            }
        });

        // Check registration number uniqueness
        $("#reg_no").on("keyup change paste blur", function() {
            
            var reg_val = $('#reg_no').val();
            var student_id = $('#student_id').val();

            if(reg_val != '') {

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/checkRegistrationNumber",
                    data: "reg_val=" + reg_val + "&student_id=" + student_id,
                    success: function (msg) {
                        if (msg == 0) {                            

                            $('.validate_reg_no').addClass('valid');
                            $('.reg_valid').removeClass('is-invalid');
                            $('.exists_error').slideUp(500);
                            $('#submit_student').show();
                        } else {

                            $('.validate_reg_no').removeClass('valid');
                            $('.reg_valid').addClass('is-invalid');
                            $('.exists_error').slideDown(500);
                            $('#submit_student').hide();
                        }
                    }
                });            
            } else {

                $('.exists_error').slideUp(500);
                $('#submit_student').hide();
            }
        });
    });

    function get_section_list() {

        var class_id = $('.class').val();
        get_semester_list(class_id);
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });


    }

    function get_semester_list(class_id) {
    
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.loader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".semester").html(msg);
                } else {
                    $(".semester").html("");
                }
            }
        });

    }
   
    function deleteUsername(id, student_id) {
    
        $('.message_block').html('<p>Are you sure that you want to delete this username?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "','" + student_id + "')");
    }

    function confirm_delete(id, student_id) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/deleteUsername",
            data: "id=" + id + " & student_id=" + student_id,
            success: function (msg) {
                $('.username_' + id).slideUp(500);                
            }
        });
    }

    function open_edit_field(parent_id, student_id) {

        $('.cont_' + parent_id).hide();
        $('.controls_' + parent_id).hide();
        $('.edit-input_' + parent_id).show().focus();
        $('.update_button_' + parent_id).show();
    }
    $(document).ready(function () {
        $(".open_pop").click(function () {
            $(".popWrap").toggleClass("openPopWrap");
        });
    });
    $(".dobb").keydown(function (event) {
        return false;
    });
</script>




