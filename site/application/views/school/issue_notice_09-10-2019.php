<?php $this->load->view('school/_include/header'); ?>
<!-- END Footer -->

<script type="text/javascript">
    function all_class() {
        $('.noticeloader').show();
        $('.sub_type').text('All Classes');

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_class_list",
            //data: "class_id=" + class_id,
            success: function (msg) {
                if (msg != "") {
                    $('.noticeloader').hide();
                    $('.check_lists').html(msg);
                    $('.full_block').slideDown('slow');
                    $('#checkboxall').on('click', function () {

                        if (this.checked) {
                            $('.checkboxTeacher').each(function () {
                                this.checked = true;
                            });
                        } else {
                            $('.checkboxTeacher').each(function () {
                                this.checked = false;
                            });
                        }
                    });

                    $('.checkboxTeacher').on('click', function () {
                        if ($('.checkboxTeacher:checked').length == $('.checkboxTeacher').length) {
                            $('#checkboxall').prop('checked', true);
                        } else {
                            $('#checkboxall').prop('checked', false);
                        }
                    });

                }
            }
        });
    }
    $(document).ready(function () {
        $('#date_of_birth').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#date_of_issue').datepicker({
            dateFormat: 'dd/mm/yy',
        });
    });

    function close_all() {
        $('#checkboxall').prop('checked', false);
        $('.checkboxTeacher').prop('checked', false);
        $('.full_block').slideUp('slow');
    }


</script>

<!--<script type="text/javascript">
    var fileReader = new FileReader();
    var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

    fileReader.onload = function (event) {
        var image = new Image();

        image.onload = function () {
            //document.getElementById("original-Img").src = image.src;
            var canvas = document.createElement("canvas");
            var context = canvas.getContext("2d");
            canvas.width = image.width / 4;
            canvas.height = image.height / 4;
            context.drawImage(image,
                    0,
                    0,
                    image.width,
                    image.height,
                    0,
                    0,
                    canvas.width,
                    canvas.height
                    );

            //document.getElementById("upload-Preview").src = canvas.toDataURL();
            document.getElementById("img_compressed").value = canvas.toDataURL();
            
        }
        image.src = event.target.result;
    };

    var loadImageFile = function () {
        var uploadImage = document.getElementById("upload-Image");

        //check and retuns the length of uploded file.
        if (uploadImage.files.length === 0) {
            return;
        }

        //Is Used for validate a valid file.
        var uploadFile = document.getElementById("upload-Image").files[0];
        if (!filterType.test(uploadFile.type)) {
            alert("Please select a valid image.");
            return;
        }

        fileReader.readAsDataURL(uploadFile);
    }
</script>-->
<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Notice</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="noticeloader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    <div class="form-group row">
                        <label class="col-12">Type</label>
                        <div class="col-12">
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio1" value="1" onclick="close_all();" >
                                <label class="custom-control-label" for="example-inline-radio1">All Teachers</label>
                            </div>
                            <div  class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio2" value="2" onclick="close_all();" >
                                <label class="custom-control-label" for="example-inline-radio2">All parents</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                <input required="" class="custom-control-input" type="radio" name="type" id="example-inline-radio3" value="3" onclick="all_class();">
                                <label class="custom-control-label" for="example-inline-radio3">All parents by class</label>
                            </div>

                        </div>
                    </div>


                    <div class="form-group row full_block">
                        <label class="col-12 sub_type"></label>
                        <div class="col-12 check_lists">


                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <input required="" type="text" class="form-control" id="notice_heading" name="notice_heading" placeholder="Heading">
                            <label for="notice_heading">Notice Heading</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="example-textarea-input">Notice</label>
                        <div class="col-12">
                            <textarea class="form-control" id="editor1" name="notice_text" rows="6" placeholder="Content.."></textarea>
                        </div>
                        <script>

                            CKEDITOR.replace('editor1');
                        </script>
                    </div>
                    <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="date_of_birth" name="date_of_publish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                            <label for="date_of_birth">Date of Publish</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="upload-Image">Upload Docs (Multiple)</label>
                        <div class="col-12">
<!--                            <input type="file" id="upload-Image" name="doc_upload[]" multiple="" onchange="loadImageFile();">-->
                            <input type="file" id="upload-Image" name="doc_upload[]" multiple="">
                            <input type="hidden" id="img_compressed" name="img_compressed">
                        </div>
                    </div>
<!--                    <td>Origal Img - <img id="original-Img"/></td>
                    <td>Compress Img - <img id="upload-Preview"/></td>-->

                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->
<?php $this->load->view('school/_include/footer'); ?>
           



