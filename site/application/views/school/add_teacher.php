<?php $this->load->view('school/_include/header');?>

                <!-- Page Content -->
                <div class="content">

                    <!-- Material Forms Validation -->
                    <h2 class="content-heading">Create Staff</h2>
                    <div class="block">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                </div>
                                <!-- END Success Alert -->
                            <?php } ?>
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <!-- Danger Alert -->
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                </div>
                                <!-- END Danger Alert -->
                            <?php } ?>
                        </div>

                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-12">
                                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                    <div class="form-group row">
                                        <label class="col-12">Select Staff Type</label>
                                        <div class="col-12">
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required class="custom-control-input" type="radio" name="staff_type" id="example-inline-radio1" value="1" checked >
                                                <label class="custom-control-label" for="example-inline-radio1">Teaching Staff</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mb-5">
                                                <input required class="custom-control-input" type="radio" name="staff_type" id="example-inline-radio2" value="2">
                                                <label class="custom-control-label" for="example-inline-radio2">Non-teaching Staff</label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="">
                                            <label for="name">Name</label>
                                        </div>
                                    </div>


                                    <div class="form-group user">
                                        <div class="form-material">
                                            <input required type="text" class="form-control username" name="username" id="username" placeholder="Phone or Email" value=""  onBlur="return check_username(this.value);">
                                            <label for="username">Username</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="password" class="form-control" name="password" id="password" placeholder="Enter password" value="" autocomplete="new-password">
                                            <label for="password">Password</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="number" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="" >
                                            <label for="phone">Phone Number</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required type="email" class="form-control" name="email" id="Email" placeholder="Account email" value="">
                                            <label for="email">Email Address</label>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">Select Status</option>
                                                <option value="1" selected>Active</option>
                                                <option value="2">Inactive</option>
                                            </select>
                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="col-12" for="adminphoto">Upload Photo</label>
                                        <div class="col-12">
                                            <input type="file" id="adminphoto" name="adminphoto"><br>
                                            (Image size should be 200px*200px )
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!--                                    </form>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Material Forms Validation -->
                </div>
                <!-- END Page Content -->

           
            <!-- END Footer -->
            <script type="text/javascript">
                $(document).ready(function () {
//        $("#frmRegister").validationEngine({promptPosition: "bottomLeft", scroll: true});

                    $("#username").on("keyup change paste keydown", function (event) {

                        var key = event.keyCode || event.which;

                        // Validate the input if space is given
                        if (key == 32) { //console.log(key);               
                            return false;
                        }

                        // Remove space if a string with space is pasted 
                        $("#username").val($("#username").val().replace(/ /g, ""));

                        var username = $("#username").val();

                        if (username != "") {

                            if (validateEmail(username)) {
                                $('#Email').val(username);
                                $('#phone').val('');
                            } else if ($.isNumeric(username) && username.length == 10) {
                                $('#phone').val(username);
                                $('#Email').val('');
                            } else {
                                $('#phone').val('');
                                $('#Email').val('');
                            }

                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>school/user/check_username",
                                data: "username=" + username,
                                success: function (msg) {
                                    //alert(msg);
                                    if (msg == 1) {
                                        $('.user').removeClass('is-invalid').addClass('is-invalid');
                                        $("#username").attr("aria-describedby", "username-error");
                                        $("#username").attr("aria-invalid", "true");
                                        $("#username-error").remove();
                                        $(".user").append('<div id="username-error" class="invalid-feedback animated fadeInDown">Username already exists.</div>');
                                        $('.btn').hide();
                                        //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                                        //$(".section").html(msg);
                                    } else {
                                        $('.btn').show();
                                        //$(".section").html("");
                                    }
                                }
                            });
                        } else {

                            $(".label-username-msg").css({"display": "block", "transform": "translateY(30px)", "opacity": "0"});
                            $(".label-username").css({"display": "block", "transform": "translateY(25px)", "opacity": "1"});

                            $("#register").hide();
                        }
                    });
                });
                function validateEmail(email) {

                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return re.test(email);
                }
                function isNumber(evt) {

                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                }
            </script>
            <script type="text/javascript">
                function check_username(user_val) {
                    //alert(user_val);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>school/user/check_username",
                        data: "username=" + user_val,
                        success: function (msg) {
                            //alert(msg);
                            if (msg == 1) {
                                $('.user').addClass('is-invalid');

                                //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                                //$(".section").html(msg);
                            } else {
                                //$(".section").html("");
                            }
                        }
                    });
                }
            </script>
       <?php $this->load->view('school/_include/footer'); ?>


