<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <?php if (ENVIRONMENT == 'production') { ?>
            <!-- Google Tag Manager -->

            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);

                })(window, document, 'script', 'dataLayer', 'GTM-KWL9Z9Q');</script>

        <?php } ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Bidyaaly:Sign In</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<!--        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url(); ?>assets/media/favicons/android-icon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-icon-180x180.png">-->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/media/favicons/favicon-32x32.png">

        <link rel="manifest" href="<?php echo base_url(); ?>assets/media/favicons/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/media/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/codebase.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/school_custom.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->

        <script src="<?php echo base_url(); ?>_js/jquery-2.2.4.min.js"></script>

        <style type="text/css">
            .username_error {
                color: red;
            }

            .username_success {
                color: #49ca75;
            }
        </style>    
        <script type="text/javascript">
            function display_loader() {
                //$('.loader_holder').show();
                if ($("#name").val() != "" && $("#contact_person_name").val() != "" && $("#mobile_no").val() != "" && $("#email_address").val() != "" && $("#password").val() != "") {
                    $('.loader').show();
                }
            }

            $(document).ready(function () {

                $("#mobile_no").on("keyup change paste blur", function () {

                    // Remove space if a string with space is pasted
                    $("#mobile_no").val($("#mobile_no").val().replace(/ /g, ""));

                    var username = $("#mobile_no").val();

                    if (username != "") {

                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>school/Main/usernameValidation",
                            data: "username=" + username,
                            success: function (response) {
                                var data = JSON.parse(response);

                                $('.exists_error02').html(data.msg);
                                $('.exists_error02').slideDown(200);

                                if (data.error == 0) {

                                    $('.validate_mob_no').addClass('valid');
                                    $('.number_valid').removeClass('is-invalid');
                                    $('#login').removeAttr("disabled");
                                } else {

                                    $('.validate_mob_no').removeClass('valid');
                                    $('.number_valid').addClass('is-invalid');
                                    $('#login').attr("disabled", "disabled");
                                }
                            }
                        });
                    } else {

                        $('.exists_error02').slideUp(200);
                        $('#login').attr("disabled", "disabled");
                    }
                });
            });
        </script>
    </head>
    <body>
        <div id="page-container" class="main-content-boxed">

            <div class="loader cust loader-universal" style="display: none;">
                <i class="fa fa-3x fa-cog fa-spin"></i>
            </div>

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="registration_content">
                    <div class="registration_content_left">
                        <div class="field_container">
                            <div class="field_container_content">

                                <form class="forms js-validation-signin" id="reg_form" method="post" action="<?php echo base_url(); ?>school/main/createSchool" enctype="multipart/form-data">

                                    <!-- First step with username and password only -->
                                    <div class="steps" id="step3">

                                        <img src="<?php echo base_url(); ?>_images/main_logo.png" class="moblOGO" alt=""/>

                                        <h2>Registration</h2>

                                        <br>     

                                        <div class="form-group number_valid">
                                            <!-- <label for="username" class="control-label col-xs-2 label_field"><span class="iconfield"><i class="fa fa-user"></i></span></label> -->
                                            <div class="col-xs-10">                                            
                                                <input name="mobile_no" id="mobile_no" type="number" class="form-control validate_mob_no" placeholder="Mobile Number" value="">                                        
                                                <div class="exists_error02 animated fadeInDown" ></div>                                        
                                            </div>                                    
                                        </div>

                                        <div class="form-group">
                                            <!-- <label for="password" class="control-label col-xs-2 label_field"><span class="icon_lock_field"><i class="fa fa-lock"></i></span></label> -->
                                            <div class="col-xs-10">
                                                <input type="password" name="password" id="password" required="" class="form-control" placeholder="Password" value="" autocomplete="new-password">                                        
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <!-- <label for="username" class="control-label col-xs-2 label_field"><span class="iconfield"><i class="fa fa-user"></i></span></label> -->
                                            <div class="col-xs-10">                                            
                                                <input type="text" name="name" id="name" required="" class="form-control" placeholder="Organization Name" value="">                                        
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <!-- <label for="username" class="control-label col-xs-2 label_field"><span class="iconfield"><i class="fa fa-user"></i></span></label> -->
                                            <div class="col-xs-10">                                            
                                                <input type="text" name="contact_person_name" id="contact_person_name" required="" class="form-control" placeholder="Contact person name" value="">                                       
                                            </div>
                                        </div>                                

                                        <div class="form-group">
                                            <!-- <label for="username" class="control-label col-xs-2 label_field"><span class="iconfield"><i class="fa fa-user"></i></span></label> -->
                                            <div class="col-xs-10">                                            
                                                <input type="email" name="email_address" id="email_address" required="" class="form-control" placeholder="Email Address" value="">                                        
                                            </div>
                                        </div>                                

                                        <span class="buttonSbmit" style="position: relative;">                                    
                                            <input name="submit" id="login" value="Submit" class="submitButton" type="submit" onclick="display_loader();" disabled="disabled">
                                        </span>

                                        <div class="loginfooter">
                                            <span class="loginfooterleft"><a href="<?php echo base_url(); ?>school">Already have an account?</a></span>
                                            <span class="loginfooterright"><a href="<?php echo base_url(); ?>school">sign in</a></span>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>                        
                    </div>

                    <div class="registration_content_right ">
                        <div class="text_container_registration">
                            <div class="text_container_registration_content">
                                <h1>Top Features</h1>
                                <p>
                                    Boost up your Organization's activity by giving the power of Bidyaaly's useful features.
                                    <!-- The Most Convenient App for Academic network with features useful for 
                                    educational organization, teacher and parents, all under one platform. -->
                                </p>

                                <ul class="pointsUl">
                                    <li>
                                        <span class="iconWrapper"><img src="<?php echo base_url(); ?>_images/icon_04.png"  alt=""/></span>
                                        <span class="textheading">Daily Activity</span>
                                        <span class="text_subtag">Daily Class activity can be added</span>
                                    </li>
                                    <li>
                                        <span class="iconWrapper"><img src="<?php echo base_url(); ?>_images/icon_02.png"  alt=""/></span>
                                        <span class="textheading">SMS Alert</span>
                                        <span class="text_subtag">SMS Alert for Persent and Absent</span>
                                    </li>
                                    <li>
                                        <span class="iconWrapper"><img src="<?php echo base_url(); ?>_images/icon_03.png"  alt=""/></span>
                                        <span class="textheading">Attendance Tracking</span>
                                        <span class="text_subtag">Attendance tracking has now become easy</span>
                                    </li>
                                    <li>
                                        <span class="iconWrapper"><img src="<?php echo base_url(); ?>_images/icon_01.png"  alt=""/></span>
                                        <span class="textheading">Time Table</span>
                                        <span class="text_subtag">Creating a Time Table is no more a headache</span>
                                    </li>
                                    <li>
                                        <span class="iconWrapper"><img src="<?php echo base_url(); ?>_images/icon_05.png"  alt=""/></span>
                                        <span class="textheading">Online Fees Payment</span>
                                        <span class="text_subtag">Online Fees payment through the App</span>
                                    </li>
                                    <li>
                                        <span class="iconWrapper"><img src="<?php echo base_url(); ?>_images/icon_06.png"  alt=""/></span>
                                        <span class="textheading">Online Diary Note</span>
                                        <span class="text_subtag">Teacher-Parent connections through digital Diary</span>
                                    </li>
                                </ul>                         
                            </div>
                        </div>
                    </div>                
                </div>

                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>

        <!--
            Codebase JS

            Custom functionality including Blocks/Layout API as well as other vital and optional helpers
            webpack is putting everything together at assets/_es6/main/app.js
        -->
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?php echo base_url(); ?>_js/op_auth_signin.min.js"></script>

    </body>
</html>
