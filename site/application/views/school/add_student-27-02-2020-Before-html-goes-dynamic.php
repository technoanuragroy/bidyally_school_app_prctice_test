<?php $this->load->view('school/_include/header'); ?>

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Create Student</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12 load">
                    <div class="loader" style="display: none;">
                        <i class="fa fa-3x fa-cog fa-spin"></i>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                            <h2 class="content-heading">Basic Details</h2>
                            
                            <div class="form-group reg_valid">
                                <div class="form-material">
                                    <input required type="text" class="form-control validate_reg_no" name="reg_no" id="reg_no" placeholder="Enter Registration Number" value="">
                                    <label for="reg_no">Registration Number</label>
                                </div>             
                                <div class="exists_error animated fadeInDown" style="display: none;">Registration number not available</div>                                
                            </div>                            

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="">
                                    <label for="name">Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control dobb" id="date_of_birth" name="date_of_birth" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                                    <label for="date_of_birth">Date of Birth</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <select required class="form-control" id="status" name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                    <label for="status">Select Gender</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="father_name" id="father_name" placeholder="Enter Father Name" value="">
                                    <label for="father_name">Father Name</label>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="mother_name" id="mother_name" placeholder="Enter Mother Name" value="">
                                    <label for="mother_name">Mother Name</label>
                                </div>
                            </div>
                            
                            <h2 class="content-heading">Enrollment Detail</h2>
                            
                            <div class="form-group">
                                <div class="form-material">
                                    <select required name="semester_id" class="form-control semester_id" onchange="get_section_list()">
                                        <option value="">Select Semester</option>
                                        <?php foreach ($semesters as $semester) { ?>
                                                <option value="<?php echo $semester['id']; ?>"><?php echo $semester['semester_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="semester_id">Select Semester</label>
                                </div>
                            </div>
                                                        
                            <div class="form-group">
                                <div class="form-material">
                                    <select required name="section_id" class="form-control section_id">
                                        <option value="">Select Section</option>
                                    </select>
                                    <label for="section_id">Select Section</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="roll_no" id="roll_no" placeholder="Enter Roll Number" value="" >
                                    <label for="roll_no">Roll Number</label>
                                </div>
                            </div>

                            <h2 class="content-heading">Personal Detail</h2>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Enter Address" value="" >
                                    <label for="address">Address</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="number" class="form-control" name="aadhar_no" id="aadhar_no" placeholder="Enter Aadhar Number" value="" >
                                    <label for="aadhar_no">Aadhar Number</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="emg_contact_person" id="emg_contact_person" placeholder="Enter Emergency Contact Name" value="" >
                                    <label for="emg_contact_person">Emergency Contact Name</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="number" class="form-control" name="emg_contact_no" id="emg_contact_no" placeholder="Enter Emergency Contact Number" value="" >
                                    <label for="emg_contact_no">Emergency Contact Number</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <input type="text" class="form-control" name="blood_group" id="blood_group" placeholder="Enter Blood Group" value="" autocomplete="new-password">
                                    <label for="blood_group">Blood Group</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-material">
                                    <select class="form-control" id="status" name="status">
                                        <option value="">Select Status</option>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    <label for="status">Status</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-12" for="userphoto">Upload Photo</label>
                                <div class="col-12">
                                    <input type="file" id="adminphoto" name="adminphoto"><br>
                                    (Image size should be 200px*200px )
                                </div>
                            </div>

                            <h2 class="content-heading">Login Details</h2>
                            <div class="variant_container_append_to">
                                <div class="container_del">
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input required=""type="number" class="form-control" name="contact_no[]" id="phone" placeholder="Enter Contact Number" value="" >
                                            <label for="contact_no">Contact Number</label>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            

                            <div class="add-details-container">
                                <div class="add-details-content">
                                    <div class="add-details-content-top">
                                        <h2 class="">Username Connected to Parents</h2>
                                        <p>These details can be used by parents/Guardians to login to the
                                           app to check progress and get regular updates of the student. (text)
                                        </p>
                                    </div>
                                    <div class="add-details-content-middle">
                                        <div class="wrp-dtls-contnr">
                                            <div class="block-dtls">
                                                <label>Mobile Number</label>
                                                <span>9332113346</span>
                                            </div>
                                            <div class="block-dtls">
                                                <label>Full Name</label>
                                                <span>Debnath Bhattacharyya</span>
                                            </div>
                                            <div class="block-dtls">
                                                <label>Email</label>
                                                <span>debnath@ablion.in</span>
                                            </div>
                                            <div class="block-dtls block-dtls-last">
                                                <label>Action</label>
                                                <span><a href="javascript:" id="btn-edit-click"><i class="fa fa-edit"></i></a></span>
                                                <span><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                      
                                    </div>

                                   
                                    <div class="edit-details-content" id="edit-details-container">
                                        <div class="dtls-bottom-contnr">
                                            <h2 class="">Edit your details</h2>
                                            <div class="wpr-from-container">
                                                <form>
                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="inputMobile" class="col-sm-5 col-form-label">mobile</label>
                                                        <div class="col-sm-7">
                                                        <input type="number" class="form-control" id="inputMobile" placeholder="54123456">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="inputName" class="col-sm-5 col-form-label">name</label>
                                                        <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="inputName" placeholder="jhone doe">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="staticEmail" class="col-sm-5 col-form-label">Email</label>
                                                        <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="staticEmail" placeholder="email@example.com">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="staticEmail" class="col-sm-5 col-form-label">Enter New Password</label>
                                                        <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="staticEmail" placeholder="*********">
                                                        </div>
                                                    </div>
                                                  
                                                    </div>
                                            
                                                    <div class="button-container-from">
                                                        <a href="#" class="btn-002">update</a>  
                                                        <a href="javascript:" class="btn-003" id="cancel-link">Cancel</a> 
                                                    </div>
                                                </form> 
                                           
                                        </div>
                                    </div>

                                    <div class="button-container-more"><a href="javascript:" class="btn-001 btn-check-in-expand" data-id="1">Add More <i class="fa fa-chevron-down"></i> </a></div>
                                    <div class="add-details-content-bottom" id="toggle1">
                                        <div class="dtls-bottom-contnr">
                                            <h2 class="">Add your details</h2>
                                            <div class="wpr-from-container">
                                                <!-- <div class="form-row-wrappr">
                                                    <label>Mobile</label>
                                                    <input type="text">
                                                </div> -->

                                                <form>
                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="inputMobile" class="col-sm-2 col-form-label">mobile</label>
                                                        <div class="col-sm-10">
                                                        <input type="number" class="form-control" id="inputMobile" placeholder="54123456">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="inputName" class="col-sm-2 col-form-label">name</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputName" placeholder="jhone doe">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row form-row-wrappr">
                                                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="staticEmail" placeholder="email@example.com">
                                                        </div>
                                                    </div>


                                                    <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio1">
                                                        <span class="d-block">Set Date of Birth as Password </span>
                                                        (NOTE: student's DOB will be set as the parent's login 
                                                        password in ddmmyyyy format, so if the students
                                                        DOB is 27/02/2010 then the password will be set to 27022010.)
                                                    </label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                    <label class="custom-control-label" for="customRadio2">Set Manual Password</label>
                                                    </div>
                                                    
                                                    <div class="container-submit-pswrd">
                                                        <input type="password" class="form-control" id="staticpassword" placeholder="Enter Password">
                                                    </div>
                                                  

                                                </div> 
                                                <div class="button-container-from">
                                                <a href="#" class="btn-002">submit</a>  
                                                <a href="javascript:" class="btn-003" id="cancel-link-02">Cancel</a> 
                                                </div>
                                                </form>
                                            </div>    
                                        </div>

                                    </div>
                                
                            </div>



                            <div class="variant_container_copier" style="display: none;">
                                <div class="container_del">       
                                    <div class="form-group">
                                        <div class="form-material">
                                            <input type="number" class="form-control" name="contact_no[]" id="phone" placeholder="Enter Contact Number" value="" >
                                            <label for="contact_no">Contact Number</label>
                                        </div>

                                    </div>
                                    <a href="javascript:" class="" onclick="remove_variant(this);"><i class="fa fa-minus-circle"></i>Remove</a>
                                </div>
                            </div>

                            <div class="append_data"></div>

                            <a href="javascript:" class="add_variant_link"><i class="fa fa-plus-circle"></i>Add Number(s)</a>

                            <div class="form-group">
                                <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit" id="submit_student">
                                <a href="<?php echo base_url(); ?>school/user/manageStudent"  class="btn btn-outline-danger">Back</a>
                                <a href="<?php echo base_url(); ?>school/user/bulkUploadStudent"  class="btn btn-alt-primary" style="float: right;">Bulk Upload</a>
                            </div>
                            
                    <?php echo form_close(); ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>
<script type="text/javascript">

    $(document).ready(function () {

// Add student details script
        $(".btn-check-in-expand").click(function () { 
            $(this).find('i').toggleClass('fas fa-chevron-up');
            $(this).find('i').toggleClass('fas fa-chevron-down');

            
             var id = $(this).data('id');
             $('#toggle'+id).slideToggle(500);
           
        }); 
        $("#cancel-link-02").click(function(){
            $(".add-details-content-bottom").slideUp("slow");             
       });
        

// Edit student details script
    $("#btn-edit-click").click(function() {
            $("#edit-details-container").slideDown("slow");
       });
         $("#cancel-link").click(function(){
             $("#edit-details-container").slideUp("slow");
       });

        // function("close-edit-details"){
        //     $("#edit-details-container").slideUp("fast");
        // }





         $('#date_of_birth').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });
        $(".add_variant_link").click(function () {

            var variant_container_html = $(".variant_container_copier").html();
            $(".variant_container_append_to").append(variant_container_html);

            $(".variant_container_append_to").find(".attribute_id").each(function () {
                $(this).attr("name", "attribute_id[]");
            });
        });
        
        get_section_list();
        
        // Check registration number uniqueness
        $("#reg_no").on("keyup change paste blur", function() {
            
            var reg_val = $('#reg_no').val();

            if(reg_val != '') {

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>school/user/checkRegistrationNumber",
                    data: "reg_val=" + reg_val,
                    success: function (msg) {
                        if (msg == 0) {

                            $(".section_id").html(msg);

                            $('.validate_reg_no').addClass('valid');
                            $('.reg_valid').removeClass('is-invalid');
                            $('.exists_error').slideUp(500);
                            $('#submit_student').show();
                        } else {

                            $('.validate_reg_no').removeClass('valid');
                            $('.reg_valid').addClass('is-invalid');
                            $('.exists_error').slideDown(500);
                            $('#submit_student').hide();
                        }
                    }
                });            
            } else {

                $('.exists_error').slideUp(500);
                $('#submit_student').hide();
            }
        });
    });
    
    function remove_variant(e) {

        $(e).closest(".container_del").remove();
    }

    function get_section_list() {
        
        var semester_id = $('.semester_id').val();
       
        $('.loader').show();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_dropdown_from_semester",
            data: "semester_id=" + semester_id,
            success: function (msg) {
                $('.loader').hide();
                $(".section_id").html("");
                if (msg != "") {                    
                    $(".section_id").html(msg);
                }
            }
        });
    }
        
    $(".dobb").keydown(function (event) {
        return false;
    });

</script>




