<?php $this->load->view('school/_include/header'); ?>

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Edit Subject</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-12">
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>
                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" name="subject_name" id="subject_name" placeholder="Subject Name" value="<?php echo $subject['subject_name']; ?>">
                            <label for="subject_name">Subject Name</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <select class="form-control" id="status" name="status">
                                <option value="">Select Status</option>
                                <option value="1" <?php
                                if ($subject['status'] == 1) {
                                    echo "selected";
                                }
                                ?>>Active</option>
                                <option value="0" <?php
                                if ($subject['status'] == 0) {
                                    echo "selected";
                                }
                                ?>>Inactive</option>
                            </select>
                            <label for="status">Status</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="subject_id" value="<?php echo $subject['id']; ?>">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                    </div>
                    <?php echo form_close(); ?>
                    <!--                                    </form>-->
                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>