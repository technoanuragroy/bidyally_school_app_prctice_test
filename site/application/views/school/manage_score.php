<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this period?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/deleteTimeTable",
            data: "timetable_id=" + id,
            success: function (msg) {

                $(".tt_" + id).hide();

            }
        });
    }

    function open_window(student_id, exam_id) {
        window.open("<?php echo base_url(); ?>school/user/class_email_notattendees_all/" + student_id + "/" + exam_id, "menubar=yes,status=no,scrollbars=yes,resizable=no,width=1110,height=600");
    }
</script>


<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Score</h2>

    <!-- Dynamic Table Full -->
    <div class="block">

        <div id="searchSection" style="display: block;">
            <div class="col-md-12 col-lg-12 no_padding_left load">
                <div class="ttmloader" style="display: none;">
                    <i class="fa fa-3x fa-cog fa-spin"></i>
                </div>
                <form action="<?php echo current_url(); ?>" id="searchForm" method="post" accept-charset="utf-8" class="js-validation-material">
                    <div class="row">
                        <!--                        <div class="col-lg-2 no_padding_left form-group">
                                                    <label class="label-form"><span class="symbolcolor">*</span>Select Session</label>
                                                    <select required name="session_id" class="form-control session">
                                                        <option value="">Select Session</option>
                        <?php
//                                $selected = '';
//                                foreach ($session_list as $session) {
//                                    if ($post_data['session_id'] != '') {
//                                        if ($post_data['session_id'] == $session['id']) {
//
//                                            $selected = 'selected="selected"';
//                                        } else {
//
//                                            $selected = '';
//                                        }
//                                    }
                        ?>
                                                            <option value="<?php //echo $session['id'];  ?>" <?php //echo $selected;  ?>><?php //echo $session['session_name'];  ?></option>
                        <?php //} ?>
                                                    </select>
                                                </div>-->
                        <div class="col-lg-2 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Class</label>
                            <select required name="class_id" class="form-control class" onchange="get_section_list();">
                                <option value="">Select Class</option>
                                <?php
                                $selected = '';
                                foreach ($class_list as $class) {
                                    if ($post_data['class_id'] != '') {
                                        if ($post_data['class_id'] == $class['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-2 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Section</label>
                            <select required name="section_id" class="form-control section">
                                <option value="">Select Section</option>
                            </select>
                            <input type="hidden" name="post_sec_id" class="post_sec_id" value="<?php
                            if ($post_data['section_id'] != '') {
                                echo $post_data['section_id'];
                            }
                            ?>">
                        </div>

                        <div class="col-lg-2 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Semester</label>
                            <select required name="semester_id" class="form-control semester" onchange="get_all_term();">
                                <option value="">Select Semester</option>
                            </select>
                            <input type="hidden" name="post_sem_id" class="post_sem_id" value="<?php
                            if ($post_data['semester_id'] != '') {
                                echo $post_data['semester_id'];
                            }
                            ?>">
                        </div>

                        <div class="col-lg-2 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Term</label>
                            <select required name="term_id" class="form-control term" onchange="get_exam_list();">
                                <option value="">Select Term</option>
                            </select>
                            <input type="hidden" name="post_term_id" class="post_term_id" value="<?php
                            if ($post_data['term_id'] != '') {
                                echo $post_data['term_id'];
                            }
                            ?>">
                        </div>

                        <div class="col-lg-2 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Exam</label>
                            <select required name="exam_id" class="form-control exam">
                                <option value="">Select Exam</option>
                            </select>
                            <input type="hidden" name="post_exam_id" class="post_exam_id" value="<?php
                            if ($post_data['exam_id'] != '') {
                                echo $post_data['exam_id'];
                            }
                            ?>">
                        </div>
                        <div class="col-lg-2 sbtnWrap buttonSbmit" style="margin-top:26px; float:left;">
                            <input id="search" name="submit" value="Go" class="btn btn-primary" type="submit">
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <?php //echo "<pre>";print_r($period);   ?>
            <div class="col-lg-6 no_padding_left form-group">

            </div>
        </div>





        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">
                <?php if (!empty($student_list)) { ?>
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th>Student Name</th>
                                <th style="text-align:center">View</th>
                                <th style="text-align:center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($student_list as $row) { ?>
                                <tr>
                                    <td>
                                        <?php echo $row['name']; ?>  
                                    </td>

                                    <td style="text-align:center">
                                        <?php
                                        $check_record_exist = $this->my_custom_functions->get_perticular_count(TBL_SCORE, "and school_id = '" . $this->session->userdata('school_id') . "' and student_id = '" . $row['id'] . "' and exam_id = '" . $post_data['exam_id'] . "'");
                                        if ($check_record_exist > 0) {
                                            ?>
                                        <a target="_blank" href="<?php echo base_url(); ?>school/user/viewScore/<?php echo $this->my_custom_functions->ablEncrypt($row['id']); ?>/<?php echo $this->my_custom_functions->ablEncrypt($post_data['exam_id']); ?>" ><i class="fa fa-eye"></i></a>
                                        <?php } ?>
                                    </td>
                                    <td style="text-align:center">
                                        <?php if ($check_record_exist > 0) { ?>
                                            <a href="<?php echo base_url(); ?>school/user/addScore/<?php echo $this->my_custom_functions->ablEncrypt($row['id']); ?>/<?php echo $this->my_custom_functions->ablEncrypt($post_data['exam_id']); ?>" class="btn btn-primary">Edit Score</a>
                                        <?php } else { ?>
                                            <a href="<?php echo base_url(); ?>school/user/addScore/<?php echo $this->my_custom_functions->ablEncrypt($row['id']); ?>/<?php echo $this->my_custom_functions->ablEncrypt($post_data['exam_id']); ?>" class="btn btn-primary">Add Score</a>
                                        <?php } ?>
                                    </td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } else { ?>
                    <table class="table table-bordered table-striped table-vcenter">
                        <tr><td>No student found</td></tr>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->


<script type="text/javascript" >
    $(document).ready(function () {
        $("#showsearch").click(function () {
            $("#searchSection").slideToggle();
        });



        var class_id = $('.class').val();
        var sec_id = $('.post_sec_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: {
                'class_id': class_id,
                'sec_id': sec_id
            },
            success: function (msg) {
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
            }
        });
        
        var class_id = $('.class').val();
        var session_id = <?php echo $this->session->userdata('session_id'); ?>;
        var semester_id = $('.post_sem_id').val();
        
         $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_list_for_score",
            data: {"class_id": class_id, "session_id": session_id,"semester_id" :semester_id},
            success: function (msg) {
                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".semester").html(msg);
                } else {
                    $(".semester").html("");
                }

            }
        });
        
         //var semester_id = $('.semester').val();
         var term_id = $('.post_term_id').val();
         
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_term_list",
            data: {"semester_id": semester_id,"term_id":term_id},
            success: function (msg) {
                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".term").html(msg);
                } else {
                    $(".term").html("");
                }

            }
        });

        var exam_id = $('.post_exam_id').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_exam_list",
            data: {"term_id": term_id,"exam_id":exam_id},
            success: function (msg) {
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".exam").html(msg);
                } else {
                    $(".exam").html("");
                }

            }
        });


    });


    function get_section_list() {
        $('.ttmloader').show();
        var class_id = $('.class').val();
        var session_id = <?php echo $this->session->userdata('session_id'); ?>

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_section_list",
            data: "class_id=" + class_id,
            success: function (msg) {
                $('.ttmloader').hide();
                if (msg != "") {
                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".section").html(msg);
                } else {
                    $(".section").html("");
                }
                get_semester_list(class_id, session_id);
                

            }
        });


    }

    function get_exam_list() {
        var term_id = $('.term').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_exam_list",
            data: {"term_id": term_id},
            success: function (msg) {
                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".exam").html(msg);
                } else {
                    $(".exam").html("");
                }

            }
        });


    }

    function get_semester_list(class_id, session_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_semester_list_for_score",
            data: {"class_id": class_id, "session_id": session_id},
            success: function (msg) {
                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".semester").html(msg);
                } else {
                    $(".semester").html("");
                }

            }
        });
    }

    function get_all_term() {
        var semester_id = $('.semester').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/get_term_list",
            data: {"semester_id": semester_id},
            success: function (msg) {
                if (msg != "") {

                    //$('#username').validationEngine('showPrompt', msg, 'red', 'bottomLeft', 1);
                    $(".term").html(msg);
                } else {
                    $(".term").html("");
                }

            }
        });
    }
</script>
<?php $this->load->view('school/_include/footer'); ?>
        

