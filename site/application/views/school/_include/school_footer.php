<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
<!--        <div class="float-right">
            Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>
        </div>
        <div class="float-left">
            <a class="font-w600" href="https://1.envato.market/95j" target="_blank">Codebase 3.1</a> &copy; <span class="js-year-copy"></span>
        </div>-->
    </div>
</footer>

<script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>
		
<!--
    Codebase JS

    Custom functionality including Blocks/Layout API as well as other vital and optional helpers
    webpack is putting everything together at assets/_es6/main/app.js
-->
<script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>

<!-- Page JS Plugins -->
<script src="<?php echo base_url(); ?>_js/Chart.bundle.min.js"></script>

<script src="<?php echo base_url(); ?>_js/slick.min.js"></script>

<!-- Page JS Code -->
<script src="<?php echo base_url(); ?>_js/be_pages_dashboard.min.js"></script>

<script src="<?php echo base_url(); ?>_js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>_js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>_js/additional-methods.js"></script>
<script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>


<script src="<?php echo base_url(); ?>_js/be_tables_datatables.min.js"></script>

<!--<script src="<?php echo base_url(); ?>_js/jquery-ui.js"></script>-->
<script src="<?php echo base_url(); ?>_js/jquery.timepicker_1.3.5.min.js"></script>
<script src="<?php echo base_url(); ?>_js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>_js/summernote-bs4.min.js"></script>
<script src="https://cdn.ckeditor.com/4.11.4/basic/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


<script type="text/javascript">

	jQuery(document).ready(function($) {
	// executes when HTML-Document is loaded and DOM is ready
	$("#clickbtn").click(function(){
		$("#div_slideDown").slideToggle('fast');
	});
        
//        $(".loader").delay(2000).fadeOut("slow");
//        $(".loader_img").delay(2000).fadeOut("slow");

        setTimeout(function(){
		$('body').addClass('loaded');
		
	}, 1000);

        

	});
        
        
       

	</script>

        