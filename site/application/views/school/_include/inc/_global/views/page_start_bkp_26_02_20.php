
<?php
/**
 * page_start.php
 *
 * Author: pixelcave
 *
 * The start section of each page used in every page of the template
 *
 */
?>

<!-- Page Container -->
<!--
    Available classes for #page-container:

GENERIC

    'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Template._uiHandleTheme())

SIDEBAR & SIDE OVERLAY

    'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
    'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
    'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
    'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
    'sidebar-inverse'                           Dark themed sidebar

    'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
    'side-overlay-o'                            Visible Side Overlay by default

    'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

    'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

HEADER

    ''                                          Static Header if no class is added
    'page-header-fixed'                         Fixed Header

HEADER STYLE

    ''                                          Classic Header style if no class is added
    'page-header-modern'                        Modern Header style
    'page-header-inverse'                       Dark themed Header (works only with classic Header style)
    'page-header-glass'                         Light themed Header with transparency by default
                                                (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
    'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

MAIN CONTENT LAYOUT

    ''                                          Full width Main Content if no class is added
    'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
    'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
-->
<div id="page-container" <?php $cb->page_classes(); ?>>
    <?php //if(isset($cb->inc_side_overlay) && $cb->inc_side_overlay) { include($cb->inc_side_overlay); } ?>
    <?php /* if(isset($cb->inc_sidebar) && $cb->inc_sidebar) { include($cb->inc_sidebar); } */ ?>

    <?php
    //echo "<pre>";print_r($this->session->all_userdata()); 
    ?>

    <nav id="sidebar">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="content-header content-header-fullrow px-15">
                <!--                         Mini Mode -->
                <div class="content-header-section sidebar-mini-visible-b">
                    <!--                             Logo -->
                    <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                        <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                    </span>
                    <!--                             END Logo -->
                </div>
                <!--                         END Mini Mode -->

                <!--                         Normal Mode -->
                <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                    <!--                             Close Sidebar, Visible only on mobile screens 
                                                 Layout API, functionality initialized in Template._uiApiLayout() -->
                    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times text-danger"></i>
                    </button>
                    <!--                             END Close Sidebar 
                                
                                                 Logo -->
                    <div class="content-header-item">
                        <!--                                <a class="link-effect font-w700" href="index.html">-->
                        <!--                                                <i class="si si-fire text-primary"></i>-->
                        <span class="font-size-xl text-dual-primary-dark">
                            <?php
                            //if ($this->session->userdata('usertype') == SCHOOL) {


                            echo substr($this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"'), 0, 20);
//            } else {
//                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $this->session->userdata('school_id') . '"');
//                echo substr($this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $school_id . '"'),0,20);
//            }
                            ?>
                        </span>
                        <!--                                </a>-->
                    </div>
                    <!--                             END Logo -->
                </div>
                <!--                         END Normal Mode -->
            </div>
            <!-- END Side Header -->

            <!-- Side User -->
            <div class="content-side content-side-full content-side-user px-10 align-parent">
                <!-- Visible only in mini mode -->
                <div class="sidebar-mini-visible-b align-v animated fadeIn">
                    <img class="img-avatar img-avatar32" src="<?php echo base_url(); ?>_images/avatar15.jpg" alt="">
                </div>
                <!-- END Visible only in mini mode -->

                <!-- Visible only in normal mode -->
                <div class="sidebar-mini-hidden-b text-center">
<!--                    <a class="img-link" href="<?php //echo base_url();      ?>school/user/dashBoard">-->
                    <a class="img-link" href="javascript:"> 
                        <?php
                        $url = '';
                        if ($this->session->userdata('usertype') == SCHOOL) {
                            $url = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'file_url', 'and id = "' . $this->session->userdata('school_id') . '"');
                            //echo $this->db->last_query();
                        } else if ($this->session->userdata('usertype') == TEACHER) {
                            $url = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'file_url', 'and id = "' . $this->session->userdata('teacher_id') . '"');
                        }
                        if ($url != '') {
                            ?>
                            <img class="img-avatar" src="<?php echo $url; ?>" alt="">
                        <?php } else { ?>
                            <img class="img-avatar" src="<?php echo base_url(); ?>_images/avatar15.jpg" alt="">
                        <?php } ?>
                    </a>
                    <ul class="list-inline mt-10">

                        <li class="list-inline-item">
                            <?php
                            if ($this->session->userdata('usertype') == SCHOOL) {
                                $school_id = $this->session->userdata('school_id');
                            } else if ($this->session->userdata('usertype') == TEACHER) {
                                $teacher_id = $this->session->userdata('teacher_id');
                                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $this->session->userdata('teacher_id') . '"');
                            }
                            $menuOn = 0;
                            if ($this->session->userdata('admin_is_logged_in') && $this->session->userdata('admin_is_logged_in') == 1) {
                                $menuOn = 1;
                            } else {
                                $mobile_verification = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $school_id . '"');
                                if ($mobile_verification == 1) {
                                    $menuOn = 1;
                                } else {
                                    $menuOn = 0;
                                }
                            }

                            if ($menuOn == 1) {
                                ?>

                                <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase hover_rewrite" href="<?php echo base_url(); ?>school/user/dashBoard">
                                <?php } else { ?>
                                    <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase hover_rewrite" href="javascript:">   
                                    <?php } ?>

                                    <?php
                                    if ($this->session->userdata('usertype') == SCHOOL) {
                                        echo substr($this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"'), 0, 20) . '..';
                                    } else if ($this->session->userdata('usertype') == TEACHER) {
                                        echo substr($this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $this->session->userdata('teacher_id') . '"'), 0, 20) . '..';
                                    }
                                    ?>

                                </a>
                        </li>

                        <li class="list-inline-item">
                            <a class="link-effect text-dual-primary-dark hover_rewrite" href="<?php echo base_url(); ?>school/user/logout">
                                <i class="si si-logout"></i>
                            </a>
                        </li>
                    </ul>                                        
                </div>
                <!-- END Visible only in normal mode -->
            </div>
            <!-- END Side User -->
            <?php
            //echo '<pre>'; print_r($this->session->userdata()); echo '</pre>';            
//            if ($this->session->userdata('usertype') == SCHOOL) {
//                $school_id = $this->session->userdata('school_id');
//            } else if ($this->session->userdata('usertype') == TEACHER) {
//                $teacher_id = $this->session->userdata('teacher_id');
//                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $this->session->userdata('teacher_id') . '"');
//            }
//
//            $menuOn = 0;
            if ($this->session->userdata('admin_is_logged_in') && $this->session->userdata('admin_is_logged_in') == 1) {
                $menuOn = 1;
            } else {
                $mobile_verification = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'otp_verification', 'and id = "' . $school_id . '"');
                if ($mobile_verification == 1) {
                    $menuOn = 1;
                } else {
                    $menuOn = 0;
                }
            }



            if ($menuOn == 1) {
                ?>
                <!-- Side Navigation -->
                <?php if ($this->uri->segment(3) != 'accountExpired') { ?>
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
                            <?php
                            if ($this->uri->segment(3) == 'dashBoard') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/dashBoard"><i class="fas fa-home"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                            </li>

                            <?php
                            $open = '';
                            if ($this->uri->segment(3) == 'manageClasses' || $this->uri->segment(3) == 'manageSections' || $this->uri->segment(3) == 'manageSubjects' || $this->uri->segment(3) == 'manageSession' || $this->uri->segment(3) == 'manageSemester' || $this->uri->segment(3) == 'manageTerms' || $this->uri->segment(3) == 'manageExam' || $this->uri->segment(3) == 'manageScore') {
                                $open = 'open';
                            } else {
                                $open = '';
                            }
                            ?>
                            <li class="<?php echo $open; ?>">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-info-circle"></i><span class="sidebar-mini-hide">Information</span></a>
                                <ul>
                                    <?php
                                    if ($this->uri->segment(3) == 'manageSession') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageSession">
            <!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                            <span class="sidebar-mini-hide">Manage Session</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageClasses') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/manageClasses" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/manageClasses') ?>">
            <!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                            <span class="sidebar-mini-hide">Manage Classes</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageSections') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageSections">
                    <!--                        <i class="fas fa-users"></i>-->
                                            <span class="sidebar-mini-hide">Manage Sections</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageSemester') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/manageSemester" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/manageSemester') ?>">
            <!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                            <span class="sidebar-mini-hide">Manage Semester</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageSubjects') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageSubjects">
            <!--                                <i class="fas fa-book-open"></i>-->
                                            <span class="sidebar-mini-hide">Manage Subjects</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageTerms') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageTerms">
            <!--                                <i class="fas fa-book-open"></i>-->
                                            <span class="sidebar-mini-hide">Manage Terms</span>
                                        </a>
                                    </li>

                                    <?php
                                    if ($this->uri->segment(3) == 'manageExam') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageExam">
            <!--                                <i class="fas fa-book-open"></i>-->
                                            <span class="sidebar-mini-hide">Manage Exam</span>
                                        </a>
                                    </li>

                                    <?php
                                    if ($this->uri->segment(3) == 'manageScore') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageScore">
            <!--                                <i class="fas fa-book-open"></i>-->
                                            <span class="sidebar-mini-hide">Manage Score</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            <?php
                            if ($this->uri->segment(3) == 'manageTeachers') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/manageTeachers" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/manageTeachers') ?>"><i class="fas fa-user-friends"></i><span class="sidebar-mini-hide">Manage Staff</span></a>
                            </li>
                            
                            <?php
                            if ($this->uri->segment(3) == 'manageTimeTable') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/manageTimeTable" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/manageTimeTable') ?>"><i class="fas fa-calendar-week"></i><span class="sidebar-mini-hide">Manage Time Table</span></a>
                            </li>
                            
                            <?php
                            if ($this->uri->segment(3) == 'manageStudent') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/manageStudent" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/manageStudent') ?>"><i class="fas fa-user-graduate"></i><span class="sidebar-mini-hide">Manage Student</span></a>
                            </li>
                            
                            <?php
                            if ($this->uri->segment(3) == 'manageNotice') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/manageNotice" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/manageNotice') ?>"><i class="fas fa-bell"></i><span class="sidebar-mini-hide">Manage Notice</span></a>
                            </li>
                            
                            <?php
                            $open = '';
                            if ($this->uri->segment(3) == 'manageHolidays' || $this->uri->segment(3) == 'manageSyllabus' || $this->uri->segment(3) == 'generalInformation' || $this->uri->segment(3) == 'manageFeesStructure') {
                                $open = 'open';
                            } else {
                                $open = '';
                            }
                            ?>
                            <li class="<?php echo $open; ?>">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-info-circle"></i><span class="sidebar-mini-hide">General Info</span></a>
                                <ul>
                                    <?php
                                    if ($this->uri->segment(3) == 'manageHolidays') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageHolidays">
                    <!--                        <i class="far fa-calendar-alt"></i>-->
                                            <span class="sidebar-mini-hide">Manage Calender</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageSyllabus') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageSyllabus">
                    <!--                        <i class="fas fa-book-reader"></i>-->
                                            <span class="sidebar-mini-hide">Manage Syllabus</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'generalInformation') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/generalInformation">
                    <!--                        <i class="fas fa-info-circle"></i>-->
                                            <span class="sidebar-mini-hide">General Information</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'manageFeesStructure') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/manageFeesStructure">
                    <!--                        <i class="fas fa-money-check"></i>-->
                                            <span class="sidebar-mini-hide">Manage Fees Structure</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            
                            <?php
                            if ($this->uri->segment(3) == 'collectFees') {
                                $active = 'active';
                            } else {
                                $active = '';
                            }
                            ?>
                            <li>
                                <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/report/collectFees" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/report/collectFees') ?>"><i class="fa fa-money" aria-hidden="true"></i><span class="sidebar-mini-hide">Collect Fees</span></a>
                            </li>                            
                            
                            <?php
                            if ($this->uri->segment(3) == 'attendanceReport' || $this->uri->segment(3) == 'activityReport' || $this->uri->segment(3) == 'charts') {
                                $open = 'open';
                            } else {
                                $open = '';
                            }
                            ?>
                            <li class="<?php echo $open; ?>">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fas fa-chart-bar"></i><span class="sidebar-mini-hide">Reports</span></a>
                                <ul>
                                    <?php
                                    if ($this->uri->segment(3) == 'studentReport') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/report/studentReport" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/report/studentReport') ?>">            
                                            <span class="sidebar-mini-hide">Student Report</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($this->uri->segment(3) == 'teacherReport') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/report/teacherReport" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/report/teacherReport') ?>">           
                                            <span class="sidebar-mini-hide">Teacher Report</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    /*if ($this->uri->segment(3) == 'attendanceReport') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/attendanceReport" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/attendanceReport') ?>">
            <!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                            <span class="sidebar-mini-hide">Attendance Report</span>
                                        </a>
                                    </li>
                                     * 
                                     */ ?>
                                    
                                    <?php
                                    /*if ($this->uri->segment(3) == 'activityReport') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/user/activityReport" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/user/activityReport') ?>">
                    <!--                        <i class="fas fa-users"></i>-->
                                            <span class="sidebar-mini-hide">Activity report</span>
                                        </a>
                                    </li>
                                     * 
                                     */ ?>

                                    <!-- Show payment reports if school has payment option enabled -->
                                    <?php
                                    $payment_enabled = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_PAYMENT_SETTINGS, 'payment_enabled', 'and school_id = "' . $this->session->userdata('school_id') . '"');
                                    if ($payment_enabled == 1) {

                                        if ($this->uri->segment(3) == 'feesPaymentReport') {
                                            $active = 'active';
                                        } else {
                                            $active = '';
                                        }
                                        ?>
                                        <li>
                                            <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/report/feesPaymentReport/re">
                        <!--                        <i class="fas fa-users"></i>-->
                                                <span class="sidebar-mini-hide">Fees payment report</span>
                                            </a>
                                        </li>

                                        <?php
                                        if ($this->uri->segment(3) == 'feesDueReport') {
                                            $active = 'active';
                                        } else {
                                            $active = '';
                                        }
                                        ?>
                                        <li>
                                            <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/report/feesDueReport/re">
                        <!--                        <i class="fas fa-users"></i>-->
                                                <span class="sidebar-mini-hide">Fees due report</span>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>        
                                        
                                    <?php
                                    /*if ($this->uri->segment(3) == 'teacherAttendanceReport') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/report/teacherAttendanceReport" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/report/teacherAttendanceReport') ?>">
                    <!--                        <i class="fas fa-users"></i>-->
                                            <span class="sidebar-mini-hide">Teacher attendance report</span>
                                        </a>
                                    </li> 
                                     * 
                                     */ ?>
                                        
                                    <?php
                                    if ($this->uri->segment(3) == 'studentAcademicReport') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?> ads" href="<?php echo base_url(); ?>school/report/studentAcademicReport" data-encrypted="<?php echo $this->my_custom_functions->ablEncrypt(base_url() . 'school/report/studentAcademicReport') ?>">
                    <!--                        <i class="fas fa-users"></i>-->
                                            <span class="sidebar-mini-hide">Student Academic report</span>
                                        </a>
                                    </li>

                                    <?php /* if ($this->uri->segment(3) == 'charts') {
                                      $active = 'active';
                                      } else {
                                      $active = '';
                                      }
                                      ?>
                                      <li>
                                      <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/charts">
                                      <!--                        <i class="fas fa-users"></i>-->
                                      <span class="sidebar-mini-hide">Charts</span>
                                      </a>
                                      </li>
                                     * 
                                     */ ?>
                                </ul>
                            </li>



                            <?php
                            if ($this->uri->segment(3) == 'changePassword' || $this->uri->segment(3) == 'edit_profile') {
                                $open = 'open';
                            } else {
                                $open = '';
                            }
                            ?>
                            <li class="<?php echo $open; ?>">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-cog"></i><span class="sidebar-mini-hide">Settings</span></a>
                                <ul>
                                    <?php
                                    if ($this->uri->segment(3) == 'changePassword') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/changePassword">
            <!--                                <i class="fas fa-chalkboard-teacher"></i>-->
                                            <span class="sidebar-mini-hide">Change Password</span>
                                        </a>
                                    </li>
                                    <?php
                                    if ($this->uri->segment(3) == 'edit_profile') {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li>
                                        <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/edit_profile">
                    <!--                        <i class="fas fa-users"></i>-->
                                            <span class="sidebar-mini-hide">Edit Institute Profile</span>
                                        </a>
                                    </li>
                                    <?php
                                    if ($this->session->userdata('admin_is_logged_in') && $this->session->userdata('admin_is_logged_in') == 1) {

                                        if ($this->uri->segment(3) == 'notificationSettings') {
                                            $active = 'active';
                                        } else {
                                            $active = '';
                                        }
                                        ?>

                                        <li>
                                            <a class="<?php echo $active; ?>" href="<?php echo base_url(); ?>school/user/notificationSettings">
                        <!--                        <i class="fas fa-users"></i>-->
                                                <span class="sidebar-mini-hide">Notification Settings</span>
                                            </a>
                                        </li>

                                    <?php } ?>





                                </ul>
                            </li>

                        </ul>
                    </div>
                <?php } ?>
                <!-- END Side Navigation -->
            <?php } ?>
        </div>
        <!-- Sidebar Content -->
    </nav>

    <header id="page-header">
        <!-- Header Content -->
        <div class="content-header">
            <!-- Left Section -->
            <div class="content-header-section">
                <!-- Toggle Sidebar -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                    <i class="fa fa-navicon"></i>
                </button>
                <!-- END Toggle Sidebar -->                                
            </div>
            <!-- END Left Section -->

            <!-- Main session drop-down section -->
            <div class="content-header-section main-session-dropdown">
                <?php
                    // Session dropdown
                    $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_school($this->session->userdata('school_id'));
                    
                    if (!empty($school_sessions)) {
                ?>    
                        <form action="<?php echo base_url(); ?>school/main/change_session" method="POST">                    
                            <select name="main_session_dropdown" class="main_session_dropdown form-control">
                                <option disabled="true">Select Session</option>
                                <?php foreach ($school_sessions as $school_ses) { ?>   
                                            <option value="<?php echo $school_ses['id']; ?>" <?php if ($school_ses['id'] == $this->session->userdata('session_id')) { echo 'selected="selected"'; } ?>><?php echo $school_ses['session_name']; ?></option>
                                <?php } ?>
                            </select>  
                        </form>
                <?php
                    } 
                    // Show username if there is no session
                    else {
                ?>                        
                        <span class="d-none d-sm-inline-block" style="color: #fff;">
                            <i class="fa fa-user"></i>
                            <?php
                                if ($this->session->userdata('usertype') == SCHOOL) {
                                    echo "Username: ". $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $this->session->userdata('school_id') . '" and type="'.SCHOOL.'"');
                                } else if ($this->session->userdata('usertype') == TEACHER) {
                                    echo "Username: ". $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $this->session->userdata('teacher_id') . '" and type="'.TEACHER.'"');
                                }    
                            ?>
                        </span>
                <?php 
                    }
                ?>
            </div>
            <!-- END Main session drop-down section -->    

            <!-- Right Section -->
            <div class="content-header-section">                
                <!-- User Dropdown -->
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<!--                        <i class="fa fa-user d-sm-none"></i>
                        <span class="d-none d-sm-inline-block">
                            <?php
                            /*if ($this->session->userdata('usertype') == SCHOOL) {
                                echo $this->my_custom_functions->get_particular_field_value(TBL_ADMIN, 'name', 'and admin_id = "' . $this->session->userdata('school_id') . '"');
                            } else {
                                echo $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $this->session->userdata('school_id') . '"');
                                //echo $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL,'name','and id = "'.$school_id.'"'); 
                            }*/
                            ?>
                        </span>-->
                        <i class="fa fa-angle-down ml-5"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                        <?php
                        if ($this->session->userdata('usertype') == SCHOOL) {
                            $school_id = $this->session->userdata('school_id');
                        } else if ($this->session->userdata('usertype') == TEACHER) {
                            $teacher_id = $this->session->userdata('teacher_id');
                            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $this->session->userdata('teacher_id') . '"');
                        }
                        ?>

                        <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">
                            <?php 
                                //echo date('F jS, Y'); 
                                if ($this->session->userdata('usertype') == SCHOOL) {
                                    echo "Username: ". $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $this->session->userdata('school_id') . '" and type="'.SCHOOL.'"');
                                } else if ($this->session->userdata('usertype') == TEACHER) {
                                    echo "Username: ". $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $this->session->userdata('teacher_id') . '" and type="'.TEACHER.'"');
                                }    
                            ?>
                        </h5>
                        <?php
                        if ($menuOn == 1) {
                            if ($this->uri->segment(3) != 'accountExpired') {
                                if ($this->session->userdata('usertype') == SCHOOL) {
                                    ?>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>school/user/edit_profile">
                                        <i class="si si-user mr-5"></i> Institute Profile
                                    </a>
                                <?php } else if ($this->session->userdata('usertype') == TEACHER) { ?>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>school/user/edit_teacher_profile">
                                        <i class="si si-user mr-5"></i> Edit Profile
                                    </a>
                                <?php } ?>
                                <!-- Toggle Side Overlay -->
                                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                <a class="dropdown-item" href="<?php echo base_url(); ?>school/user/changePassword" data-toggle="layout" data-action="side_overlay_toggle">
                                    <i class="si si-wrench mr-5"></i> Change Password
                                </a>

                                <!-- END Side Overlay -->
                                <?php
                            }
                        }
                        ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url(); ?>school/user/logout">
                            <i class="si si-logout mr-5"></i> Sign Out
                        </a>
                    </div>
                </div>
                <!-- END User Dropdown -->

                <!-- Notifications -->
                <!--                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-flag"></i>
                                                <span class="badge badge-primary badge-pill">5</span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right min-width-300" aria-labelledby="page-header-notifications">
                                                <h5 class="h6 text-center py-10 mb-0 border-b text-uppercase">Notifications</h5>
                                                <ul class="list-unstyled my-20">
                                                    <li>
                                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                            <div class="ml-5 mr-15">
                                                                <i class="fa fa-fw fa-check text-success"></i>
                                                            </div>
                                                            <div class="media-body pr-10">
                                                                <p class="mb-0">You’ve upgraded to a VIP account successfully!</p>
                                                                <div class="text-muted font-size-sm font-italic">15 min ago</div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                            <div class="ml-5 mr-15">
                                                                <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                                            </div>
                                                            <div class="media-body pr-10">
                                                                <p class="mb-0">Please check your payment info since we can’t validate them!</p>
                                                                <div class="text-muted font-size-sm font-italic">50 min ago</div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                            <div class="ml-5 mr-15">
                                                                <i class="fa fa-fw fa-times text-danger"></i>
                                                            </div>
                                                            <div class="media-body pr-10">
                                                                <p class="mb-0">Web server stopped responding and it was automatically restarted!</p>
                                                                <div class="text-muted font-size-sm font-italic">4 hours ago</div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                            <div class="ml-5 mr-15">
                                                                <i class="fa fa-fw fa-exclamation-triangle text-warning"></i>
                                                            </div>
                                                            <div class="media-body pr-10">
                                                                <p class="mb-0">Please consider upgrading your plan. You are running out of space.</p>
                                                                <div class="text-muted font-size-sm font-italic">16 hours ago</div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="text-body-color-dark media mb-15" href="javascript:void(0)">
                                                            <div class="ml-5 mr-15">
                                                                <i class="fa fa-fw fa-plus text-primary"></i>
                                                            </div>
                                                            <div class="media-body pr-10">
                                                                <p class="mb-0">New purchases! +$250</p>
                                                                <div class="text-muted font-size-sm font-italic">1 day ago</div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item text-center mb-0" href="javascript:void(0)">
                                                    <i class="fa fa-flag mr-5"></i> View All
                                                </a>
                                            </div>
                                        </div>-->
                <!-- END Notifications -->

                <!-- Toggle Side Overlay -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <!--                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="side_overlay_toggle">
                                            <i class="fa fa-tasks"></i>
                                        </button>-->
                <!-- END Toggle Side Overlay -->
            </div>
            <!-- END Right Section -->
        </div>
        <!-- END Header Content -->

        <!-- Header Search -->
        <div id="page-header-search" class="overlay-header">
            <div class="content-header content-header-fullrow">
                <form action="be_pages_generic_search.html" method="post">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <!-- Close Search Section -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
                                <i class="fa fa-times"></i>
                            </button>
                            <!-- END Close Search Section -->
                        </div>
                        <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Header Search -->

        <!-- Header Loader -->
        <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
        <div id="page-header-loader" class="overlay-header bg-primary">
            <div class="content-header content-header-fullrow text-center">
                <div class="content-header-item">
                    <i class="fa fa-sun-o fa-spin text-white"></i>
                </div>
            </div>
        </div>
        <!-- END Header Loader -->


        <!-- END Icons -->

        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/dataTables.bootstrap4.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
        <!-- Page JS Plugins CSS -->
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/slick.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/slick-theme.css">-->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/codebase.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/school_custom.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/summernote-bs4.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/jquery.timepicker_1.3.5.min.css">


        <!--<script src="http://192.168.1.10/webdev/Bidyaaly/site/assets/js/pages/be_pages_dashboard.min.js"></script>-->
        <script src="<?php echo base_url(); ?>assets/js/codebase.core.min.js"></script>


        <!--
            Codebase JS
        
            Custom functionality including Blocks/Layout API as well as other vital and optional helpers
            webpack is putting everything together at assets/_es6/main/app.js
        -->
        <script src="<?php echo base_url(); ?>assets/js/codebase.app.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/additional-methods.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_tables_datatables.min.js"></script>
        <?php if ($this->uri->segment(3) != 'editTeacher' && $this->uri->segment(3) != 'dashBoard' && $this->uri->segment(3) != 'editFeesStructure' && $this->uri->segment(3) != 'feesPaymentReport' && $this->uri->segment(3) != 'feesDueReport') { ?>
            <script src="<?php echo base_url(); ?>_js/jquery-ui.js"></script>
        <?php } ?>
        <script src="<?php echo base_url(); ?>_js/jquery.timepicker_1.3.5.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!--        <script src="<?php echo base_url(); ?>_js/bootstrap-datepicker.min.js"></script>-->
        <script src="<?php echo base_url(); ?>_js/summernote-bs4.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.11.4/basic/ckeditor.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <script src="<?php echo base_url(); ?>_js/chart.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".main_session_dropdown").change(function () {
                    $(this).closest("form").submit();
                });
            });

        </script>  
        <?php
        $check_adv_flag = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'display_ads', 'and id = "' . $this->session->userdata('school_id') . '"');
        if ($check_adv_flag == 0) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('.ads').click(function (e) {
                        e.preventDefault();
                        //alert($(this).attr('href'));
                        var value = $(this).data('encrypted');

                        if ($(this).hasClass('ads')) {
                        
                            var current_url = window.location.href;
                            var new_url = '<?php echo base_url(); ?>'+'school/user/customAds/?V='+value;//current_url.split('/customAds/?V')[0]+'/customAds/?V='+value;
                            window.location.href = new_url;

                            //var loc = 'customAds/?V=' + value;
                            //window.location = loc;
                        }


                    });
                });

            </script>
        <?php } ?>

    </header>

    <!-- Main Container -->
    <main id="main-container">
