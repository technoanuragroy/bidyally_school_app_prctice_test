<?php
    if($this->uri->segment(1) != 'admin') {
?>
        <script src="https://wchat.freshchat.com/js/widget.js"></script>
        <script type="text/javascript">       
            // Freshchat code for google tag manager            
            var chat_school = <?php echo $this->my_custom_functions->get_chat_school_details(); ?>;
                        
            window.fcWidget.init({ token: "86c9d9b7-fb16-4876-8ee2-d21da2de626d", host: "https://wchat.freshchat.com" }); 

            window.fcWidget.setExternalId(chat_school.externalId);                                                 
            window.fcWidget.user.setFirstName(chat_school.name);                                                 
            window.fcWidget.user.setEmail(chat_school.email);     
            window.fcWidget.user.setPhone(chat_school.mobile);     
            window.fcWidget.user.setProperties(                                                    
                chat_school.properties
            );                                
        </script>
        
<?php /*        <script>
            function initFreshChat() {
                window.fcWidget.init({
                    token: "86c9d9b7-fb16-4876-8ee2-d21da2de626d",
                    host: "https://wchat.freshchat.com"
                });
            }
            function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
        </script> */ ?>
<?php 
    } 
?>






