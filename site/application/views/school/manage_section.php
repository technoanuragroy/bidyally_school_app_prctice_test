<?php $this->load->view('school/_include/header'); ?>
<style>
    #DataTables_Table_0_length{
        display: none;
    }
</style>
<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this section?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        window.location.href = "<?php echo base_url(); ?>school/user/deleteSection/" + id;
    }
     jQuery(document).ready(function ($) {
// executes when HTML-Document is loaded and DOM is ready
        $("#clickbtn").click(function () {
            $("#div_slideDown").slideToggle('fast');
        });

//        $(".loader").delay(2000).fadeOut("slow");
//        $(".loader_img").delay(2000).fadeOut("slow");

        setTimeout(function () {
            $('body').addClass('loaded');

        }, 1000);


    });
    

</script>
<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Manage Section</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">
            <a href="<?php echo base_url(); ?>school/user/addSection" class="btn btn-primary">Add Section</a>
            <a href="javascript:" class="btn btn-primary" id="clickbtn">Search Section</a>
        </div>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content block-content-full">
        <div class="table-responsive">
            <div id="div_slideDown">
                <form method="post" id='frm1' action="<?php echo current_url(); ?>">
                    

                    <div class="form-row">
                        <div class="form-material col-md-6">
                            <select class="form-control class" id="class" name="class">
                                <option value="">Select Class</option>
                                <?php
                                $selected = '';
                                foreach ($class_list as $class) {
                                    if ($post_data['class_id'] != '') {
                                        if ($post_data['class_id'] == $class['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['class_name']; ?></option>
                                <?php } ?>

                            </select>

                        </div>
                        
                    </div>
                    <input type="submit" name="submit" value="Search" class="btn btn-primary">
                    
                </form>


            </div>
            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->

            <?php
            if (!empty($classes)) {
                $i = 1;
                ?>
                <!--                <table class="table table-striped table-vcenter js-dataTable-full">-->
                    <table class="table table-striped table-vcenter dataTable no-footer ">
                    <thead>
                        <tr>
<!--                            <th class="text-center"></th>-->
                            <th>Section</th>
                            <th>Class</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($classes as $row) {


                            $encrypted = $this->my_custom_functions->ablEncrypt($row['id']);
                            //$encrypted = $row['id'];
                            ?>
                            <tr>
<!--                                <td class="text-center"><?php echo $i; ?></td>-->
                                <td><?php echo $row['section_name']; ?></td>
                                <td><?php echo $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', ' and id = ' . $row['class_id'] . ''); ?></td>
                                <td><?php
                                    if ($row['status'] == 1) {
                                        echo "<span class='badge badge-success'>Active</span>";
                                    } else {
                                        echo "<span class='badge badge-danger'>Inactive</span>";
                                    }
                                    ?></td>
                                <td class="text-center">                                    
                                    <a href="<?php echo base_url() . 'school/user/editSection/' . $encrypted; ?>" title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo base_url() . 'school/user/deleteSection/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                                
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>  
                    </tbody>
                </table>
            <?php } else { ?>
                <tr>
                    <td colspan="9">No section found</td>
                </tr>
                <?php
            }
            ?>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<?php $this->load->view('school/_include/footer'); ?>

