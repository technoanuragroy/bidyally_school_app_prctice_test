<?php $this->load->view('school/_include/header');?>

    <!-- Page Content -->
    <div class="content feesContainer">                    
        <h2 class="content-heading">Add Fees Structure Break ups</h2>
        
        <div class="block">
            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>
                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

            <div class="block-content">
                <div class="row justify-content-center py-20">
                    <div class="col-xl-12">

                        <?php echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                <div class="form-group">
                                    <div class="form-material">
                                        <?php $semester_name = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'semester_name','and id = "'.$fees_structure['semester_id'].'"'); ?>
                                        <label>Semester - <?php echo $semester_name; ?></label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-material">                                            
                                        <label>Cycle Type - <?php echo ucfirst($this->config->item('cycle')[$fees_structure['cycle_type']]); ?></label>
                                    </div>
                                </div>

                                <?php                                                                         
                                    $break_up_structure = $this->my_custom_functions->get_general_breakup_of_months($fees_structure['id']);
                                                                                                                                                                            
                                    $index = 1;
                                    foreach($break_up_structure as $breakup) {        
                                ?>                        
                                            <div class="fees_structure_breakup_container">
                                                <h4 class="fees_for_header">Fees for - <?php echo $breakup['label']; ?></h4>
                                                
                                                <div class="fees_structure_breakup_content">
                                                    <div class="form-group">
                                                        <div class="form-material">
                                                            <input required type="text" class="form-control" name="breakup_label[<?php echo $index; ?>]" value="Fees For <?php echo $breakup['label']; ?>" placeholder="Ex: Fees For <?php echo $breakup['label']; ?>">
                                                            <label>Label For Parents</label>
                                                        </div>
                                                    </div> 

                                                    <input type="hidden" name="month[<?php echo $index; ?>]" value="<?php echo $breakup['start_month']; ?>">

                                                    <div class="fees_container_append fees_container_append_to_<?php echo $index; ?>" data-index="<?php echo $index; ?>">
                                                        <div class="container_del container_del_main">
                                                            <div class="form-group custom-block">
                                                                <div class="form-material">
                                                                    <input required type="text" class="form-control label" name="label[<?php echo $index; ?>][]" placeholder="Ex: Transport Fees">
                                                                    <label>Label</label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group custom-block">
                                                                <div class="form-material">
                                                                    <input required type="text" class="form-control amount" name="amount[<?php echo $index; ?>][]" placeholder="Ex: 100" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');">
                                                                    <label>Amount</label>
                                                                    <span>Enter comma(",") separated values(Ex: 100,200) to show the fees amounts as drop-down.</span>
                                                                </div>
                                                            </div> 

                                                            <div class="form-group custom-block">
                                                                <div class="form-material">
                                                                    <select required="" name="option[<?php echo $index; ?>][]" class="form-control option">
                                                                        <option value="1">Yes</option>
                                                                        <option selected="selected" value="2">No</option>
                                                                    </select>
                                                                    <label>Optional</label>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>

                                                    <a href="javascript:" class="add_fees_link" data-index="<?php echo $index; ?>"><i class="fa fa-plus-circle"></i>&nbsp;Add</a>                                                                                                                            
                                                
                                                </div>  
                                            </div>  
                                            
                                            <!-- Give option to copy first break up to rest -->
                                            <?php 
                                                if($index == 1 AND count($break_up_structure) > 1) {
                                            ?>
                                                    <label class="repeat_breakups_label">
                                                        <input type="checkbox" class="repeat_breakups" data-index="<?php echo $index; ?>">&nbsp; Click here to copy above fees to the rest of the breakups. 
                                                    </label>    
                                            <?php
                                                }
                                            ?>
                                <?php          
                                            $index++;
                                    }                                                                       
                                ?>

                                <div class="form-group">
                                    <input type="hidden" name="fees_id" value="<?php echo $fees_structure['id']; ?>">
                                    <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                                </div>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>

        </div>                    
    </div>
    
    <!-- Content for more fee records -->
    <div class="fees_container_copier" style="display: none;">
        <div class="container_del">
            <div class="form-group custom-block">
                <div class="form-material">
                    <input required type="text" class="form-control label" name="label[]" placeholder="">
                    <label>Label</label>
                </div>
            </div>

            <div class="form-group custom-block">
                <div class="form-material">
                    <input required type="text" class="form-control amount" name="amount[]" placeholder="" oninput="this.value = this.value.replace(/[^0-9.,]/g, '').replace(/(\..*)\./g, '$1');">
                    <label>Amount</label>
                </div>
            </div> 

            <div class="form-group custom-block">
                <div class="form-material">
                    <select required="" name="option[]" class="form-control option">
                        <option value="1">Yes</option>
                        <option selected="selected" value="2">No</option>
                    </select>
                    <label>Optional</label>
                </div>
            </div>

            <a href="javascript:" class="" onclick="remove_fees(this);"><i class="fa fa-minus-circle"></i>&nbsp;Remove</a>
        </div>                                        
    </div>
                
    <script type="text/javascript">
        $(".add_fees_link").click(function () {            
            var fees_index = $(this).data("index");

            var fees_container_html = $(".fees_container_copier").html();
            $(".fees_container_append_to_"+fees_index).append(fees_container_html);

            $(".fees_container_append_to_"+fees_index).find(".label").each(function () {
                $(this).attr("name", "label["+fees_index+"][]");
            });
            $(".fees_container_append_to_"+fees_index).find(".amount").each(function () {
                $(this).attr("name", "amount["+fees_index+"][]");
            });
            $(".fees_container_append_to_"+fees_index).find(".option").each(function () {
                $(this).attr("name", "option["+fees_index+"][]");
            });
        });
        
        $(".repeat_breakups").on("click", function () {    
            var fees_index = $(this).data("index");
            
            if($(this).prop("checked")) {
                var source_html = $(".fees_container_append_to_"+fees_index).html();
                $('.fees_container_append:not(.fees_container_append_to_'+fees_index+')').each(function() {      
                    
                    var corresponding_index = $(this).data("index");
                    $(this).html(source_html);
                    
                    $(this).find(".container_del").find(".label").each(function(i, e) {
                        $(this).val($(".fees_container_append_to_"+fees_index).find(".container_del").find(".label").eq(i).val());
                        $(this).attr("name", "label["+corresponding_index+"][]");
                    });   
                    $(this).find(".container_del").find(".amount").each(function(i, e) {
                        $(this).val($(".fees_container_append_to_"+fees_index).find(".container_del").find(".amount").eq(i).val());
                        $(this).attr("name", "amount["+corresponding_index+"][]");
                    });  
                    $(this).find(".container_del").find(".option").each(function(i, e) {
                        $(this).val($(".fees_container_append_to_"+fees_index).find(".container_del").find(".option").eq(i).val());
                        $(this).attr("name", "option["+corresponding_index+"][]");
                    });  
                });    
            } else {
                $('.fees_container_append:not(.fees_container_append_to_'+fees_index+')').each(function() {      
                    $(this).find(".container_del:not(.container_del_main)").remove();
                    
                    $(this).find(".container_del_main").find(".label").val("");
                    $(this).find(".container_del_main").find(".amount").val("");
                    $(this).find(".container_del_main").find(".option").val("2");
                });
            }
        });
        
        function remove_fees(e) {

            $(e).closest(".container_del").remove();
        }
        
        $(document).ready(function() {               
            // Sort fees
            $('.fees_container_append').sortable().disableSelection();
        });
    </script>
          
<?php $this->load->view('school/_include/footer'); ?>