<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <?php if (ENVIRONMENT == 'production') { ?>
            <!-- Google Tag Manager -->

            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);

                })(window, document, 'script', 'dataLayer', 'GTM-KWL9Z9Q');</script>

        <?php } ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title><?php echo SITE_NAME; ?>:Sign In</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<!--        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url(); ?>assets/media/favicons/android-icon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-icon-180x180.png">-->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/media/favicons/favicon-32x32.png">

        <link rel="manifest" href="<?php echo base_url(); ?>assets/media/favicons/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/media/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/codebase.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/school_custom.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->

    </head>
    <body>
        <script src="https://snippets.freshchat.com/js/fc-pre-chat-form-v2.js"></script> 
        <script>
    var preChatTemplate = {//Form header color and Submit button color.     
        mainbgColor: '#0aa4db', //Form Header Text and Submit button text color.     
        maintxColor: '#fff', //Chat Form Title     
        heading: 'GadgetGod', //Chat form Welcome Message     
        textBanner: 'We can\'t wait to talk to you. But first, please take a couple of moments to tell us a bit about yourself.',
        //Submit Button Label.     
        SubmitLabel: 'Start Chat', //Fields List - Maximum is 5     
        //All the values are mandatory and the script will not work if not available.     
        fields: {
            field1: {//Type for Name - Do not Change         
                type: "name", //Label for Field Name, can be in any language         
                label: "Name", //Default - Field ID for Name - Do Not Change         
                fieldId: "name", //Required "yes" or "no"         
                required: "yes", //Error text to be displayed         
                error: "Please Enter a valid name"
            },
            field2: {//Type for Email - Do Not Change         
                type: "email", //Label for Field Email, can be in any language         
                label: "Email", //Default - Field ID for Email - Do Not Change         
                fieldId: "email", //Required "yes" or "no"         
                required: "yes", //Error text to be displayed         
                error: "Please Enter a valid Email"
            },
            field3: {//Type for Phone - Do Not Change         
                type: "phone", //Label for Field Phone, can be in any language         
                label: "Phone", //Default - Field ID for Phone - Do Not Change         
                fieldId: "phone", //Required "yes" or "no"         
                required: "yes", //Error text to be displayed         
                error: "Please Enter a valid Phone Number"
            }
        }
    };

    window.fcSettings = {
        token: "86c9d9b7-fb16-4876-8ee2-d21da2de626d",
        host: "https://wchat.freshchat.com",
        config: {
            cssNames: {//The below element is mandatory. Please add any custom class or leave the default.         
                widget: 'custom_fc_frame', //The below element is mandatory. Please add any custom class or leave the default.         
                expanded: 'custom_fc_expanded'
            }
        },
        onInit: function () {
            console.log('widget init');
            fcPreChatform.fcWidgetInit(preChatTemplate);
        }
    };
        </script> 
        <script src="https://wchat.freshchat.com/js/widget.js" async></script>


        <?php if ($this->session->userdata('maintainance_message') && $this->session->userdata('maintainance_message') != '') { ?>
            <div class="overlay_screen">
                <img src="<?php echo base_url(); ?>_images/repair.png">
                <p><?php echo $this->session->userdata('maintainance_message'); ?></p>
            </div>
        <?php } ?>

        <div id="page-container" class="main-content-boxed">

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="registration_content">

                    <div class="registration_content_left login_left">
                        <div class="field_container">
                            <div class="field_container_content">

                                <form class="js-validation-signin" id="reg_form" method="post" action="<?php echo base_url(); ?>school/main/login">

                                    <!-- First step with username and password only -->
                                    <div class="steps" id="step3">

                                        <img src="<?php echo base_url(); ?>_images/main_logo.png" class="moblOGO" alt=""/>
                                        <h2>Please sign in</h2><br> 
                                        <div class="col-md-12">
                                            <?php if ($this->session->flashdata("s_message")) { ?>
                                                <!-- Success Alert -->
                                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                                </div>
                                                <!-- END Success Alert -->
                                            <?php } ?>
                                            <?php if ($this->session->flashdata("e_message")) { ?>
                                                <!-- Danger Alert -->
                                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                                </div>
                                                <!-- END Danger Alert -->
                                            <?php } ?>
                                        </div>

                                        <div class="form-group">
                                         <!-- <label for="username" class="control-label col-xs-2 label_field"><span class="iconfield"><i class="fa fa-user"></i></span></label> -->
                                            <div class="col-xs-10">                                            
                                                <input type="text" name="username" id="username" required="" class="form-control" placeholder="User Name" value="">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <!-- <label for="password" class="control-label col-xs-2 label_field"><span class="icon_lock_field"><i class="fa fa-lock"></i></span></label> -->
                                            <div class="col-xs-10">
                                                <input type="password" name="password" id="password" required="" class="form-control" placeholder="Password" value="" autocomplete="new-password">

                                            </div>
                                        </div>



                                        <span class="buttonSbmit">
                                            <input name="submit" id="login" value="Submit" class="submitButton" type="submit">
                                        </span>

                                        <div class="loginfooter">
                                            <span class="loginfooterleft"><a href="<?php echo base_url(); ?>school/signUp"> Don't have an account?</a></span>
                                            <span class="loginfooterright"><a href="<?php echo base_url(); ?>school/forgetPassword">Forget password?</a></span>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="registration_content_right login_right">
                        <div class="text_container_registration">
                            <div class="text_container_registration_content">
                                <h1>Get Started</h1>
                                <p>


                                    <!-- With the rapidly growing number of pre-schools, primary, secondary schools
                                     and also various coaching and training institutes all around us, we often
                                      get confused about which one to choose for our child. Some worries remain
                                       common to all parents and often students while choosing the most suitable institute.
                                        Among these what pricks the most is the communication gap between the institute,
                                         teachers and the parents or students themselves. Bidyaaly unites parents teachers
                                          and educational organizations all under one platform which bridges the communication
                                           gap and acts as a real-time activity notifier. Bidyaaly joins hands with Educational 
                                           Institute Parents and Teachers to make the learning environment better.
                                            Follow a few easy steps and get started. -->
                                    Bidyaaly unites parents teachers and educational organizations
                                    all under one platform which bridges the communication gap and 
                                    acts as a real-time activity notifier. Bidyaaly joins hands with
                                    Educational Institute Parents and Teachers to make the learning environment better.
                                    Follow a few easy steps and get started.


                                </p>

                            </div>
                        </div>
                    </div>



                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>


        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?php echo base_url(); ?>_js/op_auth_signin.min.js"></script>
    </body>
    <?php /*    <script>
      function initFreshChat() {
      window.fcWidget.init({
      token: "86c9d9b7-fb16-4876-8ee2-d21da2de626d",
      host: "https://wchat.freshchat.com"
      });
      }
      function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
      </script> */ ?>
</html>
