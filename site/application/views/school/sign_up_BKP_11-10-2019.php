<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Bidyaaly:Sign In</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<!--        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url(); ?>assets/media/favicons/android-icon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-icon-180x180.png">-->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/media/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/media/favicons/favicon-16x16.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/media/favicons/favicon-32x32.png">
        
        <link rel="manifest" href="<?php echo base_url(); ?>assets/media/favicons/site.webmanifest">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/media/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/codebase.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url(); ?>_css/school_custom.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->

    </head>
    <body>
        <div id="page-container" class="main-content-boxed">

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="bg-gd-dusk">
                    <div class="hero-static content content-full bg-white invisible" data-toggle="appear">
                        <!-- Header -->
                        <div class="py-30 px-5 text-center">
                            <a class="link-effect font-w700" href="#">
                                
                                <span class="font-size-xl text-primary-dark"><img src="<?php echo base_url(); ?>_images/main_logo.png"></span>
                            </a>
<!--                            <h1 class="h2 font-w700 mt-50 mb-10">Registration</h1>-->
                            <h2 class="h4 font-w400 text-muted mb-0">Registration</h2>
                        </div>
                        <!-- END Header -->
                        <div class="col-sm-8 col-md-6 col-xl-4" style="margin: 0 auto;">
                            <?php if ($this->session->flashdata("e_message")) { ?>
                                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?>
                                </div>
                                <!-- END Success Alert -->
                            <?php }
                            ?>

                            <?php if ($this->session->flashdata("s_message")) { ?>
                                <!-- Success Alert -->
                                <div class="alert alert-success alert-dismissable s_message" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?>
                                </div>
                                <!-- END Success Alert -->
                            <?php }
                            ?>
                        </div>

                        <div class="row justify-content-center px-5">
                            <div class="col-sm-8 col-md-6 col-xl-4">
                                <!-- jQuery Validation functionality is initialized with .js-validation-signin class in js/pages/op_auth_signin.min.js which was auto compiled from _es6/pages/op_auth_signin.js -->
                                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->

                                <form class="forms js-validation-signin" id="reg_form" method="post" action="<?php echo base_url(); ?>school/main/createSchool" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input required="" class="form-control" name="name" id="name"  type="text">
                                                <label for="name">Organization Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input required="" class="form-control" name="contact_person_name" id="contact_person_name"  type="text">
                                                <label for="contact_person_name">Contact person name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input required="" class="form-control" name="mobile_no" id="mobile_no" type="text" maxlength="10" minlength="10">
                                                <label for="mobile_no">Mobile Number</label>
                                            </div></div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input required="" class="form-control" name="email_address" id="email_address" type="text">
                                                <label for="email_address">Email Address</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input required="" class="form-control" name="password" id="password" type="password" autocomplete="new-password">
                                                <label for="password">Password</label>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input class="form-control" name="address" id="Address" type="text">
                                                <label for="Address">Address</label>
                                            </div>
                                        </div>
                                    </div>-->
<!--                                    <div class="form-group row">
                                        <label class="col-12" for="userphoto">Upload Photo</label>
                                        <div class="col-12">
                                            <input type="file" id="adminphoto" name="adminphoto"><br>
                                            (Image size should be 400px*400px )
                                        </div>
                                    </div>-->

                                    <div class="col-12 mb-10">
                                            <!-- <button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary">
                                                       <i class="si si-login mr-10"></i> Sign In
                                             </button>-->
                                            <input type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary" name="submit" value="Submit">
                                            <span class="already_acc">Already have an account?<a href="<?php echo base_url(); ?>school"> Sign in</a></span>
                                        </div>
                                    
                            </form>
                                
                            </div>
                        </div>
                        <!-- END Sign In Form -->
                    </div>
                   
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>

        <!--
            Codebase JS

            Custom functionality including Blocks/Layout API as well as other vital and optional helpers
            webpack is putting everything together at assets/_es6/main/app.js
        -->
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?php echo base_url(); ?>_js/op_auth_signin.min.js"></script>
    </body>
</html>
