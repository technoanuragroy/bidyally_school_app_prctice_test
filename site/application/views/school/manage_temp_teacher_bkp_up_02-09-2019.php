<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    function call_delete(id) {
        $('.message_block').html('<p>Are you sure that you want to delete this period?</p>');
        $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
    }
    function confirm_delete(id) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>school/user/deleteTimeTable",
            data: "timetable_id=" + id,
            success: function (msg) {

                $(".tt_" + id).hide();

            }
        });
    }

    $(document).ready(function () {
        
        $('#class_date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });

    });
</script>


<!-- Page Content -->
<div class="content">
    <h2 class="content-heading">Temporary Teacher Assign</h2>

    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header block-header-default">

        </div>
        <div id="searchSection" style="display: block;">
            <div class="col-md-12 col-lg-12 no_padding_left">
                <form action="<?php echo current_url(); ?>" id="searchForm" method="post" accept-charset="utf-8" class="js-validation-material">
                    <div class="row">
                        <div class="col-lg-5 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Teacher</label>
                            <select required name="teacher_id" class="form-control class">
                                <option value="">Select Teacher</option>
                                <?php
                                $selected = '';
                                foreach ($teacher_list as $teacher) {
                                    if ($post_data['teacher_id'] != '') {
                                        if ($post_data['teacher_id'] == $teacher['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $teacher['id']; ?>" <?php echo $selected; ?>><?php echo $teacher['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>



                        <div class="col-lg-5 no_padding_left form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Date</label>
                            <input type="text" name="class_date" id="class_date" class="form-control" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php
                            if (!empty($post_data)) {
                                if ($post_data['post_date'] != '') {
                                    echo $post_data['post_date'];
                                }
                            } else {
                                echo date('d/m/Y');
                            }
                            ?>">
                        </div>

                        <div class="col-lg-2 sbtnWrap buttonSbmit" style="margin-top:10px; float:left;">
                            <input id="search" name="submit" value="Go" class="btn btn-primary" type="submit">
                        </div>
                    </div>
                </form>
            </div>
            <br>
<?php //echo "<pre>";print_r($period);    ?>
            <div class="col-lg-6 no_padding_left form-group">

            </div>
        </div>





        <div class="col-md-12">
<?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
<?php } ?>
<?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
<?php } ?>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <form method="post" action="<?php echo base_url(); ?>school/user/assignTempTeacher" class="js-validation-material">
                <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th colspan="2" style="text-align:center;font-weight: bold">Time Table</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $day_list = $this->config->item('days_list');
                        if (!empty($classes_list)) {
                            
                            foreach ($classes_list as $row) {//echo "<pre>";print_r($row);
                                $background = '';
                                $disable = '';
                                $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $row['class_id'] . '"');
                                $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $row['section_id'] . '"');
                                
                                $post_date = $this->my_custom_functions->database_date($post_data['post_date']);
                                if (date('Y-m-d') >= $post_date) {
                                    if (date('H:i:s') > $row['period_start_time']) {
//                                        $background = 'style="background-color:#ccc"';
//                                        $disable = 'disabled';
                                    }
                                }
                                ?>
                                <tr <?php echo $background; ?>>

                                    <td class="tt_<?php echo $row['id'] ?>"><?php
                                        echo "<b>" . $row['period_name'] . "</b><br>";
                                        if ($row['subject_id'] != 0) {
                                            echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $row['subject_id'] . '"');
                                            echo "<br>";
                                        }

                                        echo "(" . $class_name . $section_name . ")<br>";


//                            echo "T - ".$this->my_custom_functions->get_particular_field_value(TBL_TEACHER,'name','and id = "'.$period_data['teacher_id'].'"');
                                        if ($row['teacher_id'] != 0) {
                                            $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $row['teacher_id'] . '"');
                                            $break_teacher_name = explode(' ', trim($teacher_name));

                                            $length = count($break_teacher_name);
                                            
                                            $join_teacher_name = '';
                                            for ($i = 0; $i < $length-1; $i++) {

                                                $join_teacher_name .= substr($break_teacher_name[$i], 0, 1) . '.';
                                            }
                                            $name = $join_teacher_name.$break_teacher_name[$length-1];
//
//                                            $final_name = rtrim($join_teacher_name, '.');
                                            echo '(' . $name . ')';
                                            
                                            
                                            
                                            
                                            
                                        } else {
                                            echo "";
                                        }
                                        echo "<br>";
                                        echo '(' . date('h:i A', strtotime($row['period_start_time'])) . ' - ' . date('h:i A', strtotime($row['period_end_time'])) . ')';
                                        echo "<br>";
                                        ?>
        <!--                                        <a href="<?php echo base_url(); ?>school/user/editTimeTable/<?php echo $this->my_custom_functions->ablEncrypt($row['id']); ?>"><i class="fa fa-edit"></i></a>
                                        <a href='javascript:' title='Delete' data-toggle='modal' data-target='#modal-top' onclick="return call_delete('<?php echo $row['id']; ?>')"><i class='fa fa-trash'></i></a>-->
                                        <input type="hidden" name="period_id[]" value="<?php echo $row['id']; ?>">
                                        <input type="hidden" name="class_assign_date" value="<?php echo $post_data['post_date']; ?>">
                                        

                                    </td>
                                    <td>
                                        <select required name="teacher_id[<?php echo $row['id']; ?>]" class="form-control" <?php echo $disable; ?>>
                                            <option value="">Select Teacher</option>
                                            <?php
                                            $selected = '';
                                            foreach ($teacher_list as $teacher) {
                                                ?>
                                                <option value="<?php echo $teacher['id']; ?>"><?php echo $teacher['name']; ?></option>
                                             <?php } ?>
                                        </select>
                                    </td>



                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="9">No timetable found</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <input type="submit" name="submit" value="Save" class="btn btn-primary">
            </form>
        </div>
    </div>
    <!-- END Dynamic Table Full -->



    <!-- END Dynamic Table Simple -->
</div>
<!-- END Page Content -->


<script type="text/javascript" >
    $(document).ready(function () {
        $("#showsearch").click(function () {
            $("#searchSection").slideToggle();
        });

    });


</script>
<?php $this->load->view('school/_include/footer'); ?>
        

