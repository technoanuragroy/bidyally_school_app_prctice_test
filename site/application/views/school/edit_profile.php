<?php $this->load->view('school/_include/header'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.due_reminder_settings').click(function () {
            if ($(this).prop("checked")) {
                $('.due_reminder_container').slideDown();
            } else {
                $('.due_reminder_container').slideUp();
            }
        });
        $('.after_schedule_copy_link').click(function () {
            var schedule_html = $(".schedule_copy_after").html();
            $(".after_schedule_append").append(schedule_html);

            $(".reminder_push_after").each(function (i, e) {
                $(this).attr("id", "reminder_push_after_" + i);
                $(this).attr("name", "reminder_push_after[" + i + "]");
                $(this).parent().find("label").attr("for", "reminder_push_after_" + i);
            });
            $(".reminder_email_after").each(function (i, e) {
                $(this).attr("id", "reminder_email_after_" + i);
                $(this).attr("name", "reminder_email_after[" + i + "]");
                $(this).parent().find("label").attr("for", "reminder_email_after_" + i);
            });
            $(".reminder_sms_after").each(function (i, e) {
                $(this).attr("id", "reminder_sms_after_" + i);
                $(this).attr("name", "reminder_sms_after[" + i + "]");
                $(this).parent().find("label").attr("for", "reminder_sms_after_" + i);
            });
        });
        $('.before_schedule_copy_link').click(function () {
            var schedule_html = $(".schedule_copy_before").html();
            $(".before_schedule_append").append(schedule_html);

            $(".reminder_push_before").each(function (i, e) {
                $(this).attr("id", "reminder_push_before_" + i);
                $(this).attr("name", "reminder_push_before[" + i + "]");
                $(this).parent().find("label").attr("for", "reminder_push_before_" + i);
            });
            $(".reminder_email_before").each(function (i, e) {
                $(this).attr("id", "reminder_email_before_" + i);
                $(this).attr("name", "reminder_email_before[" + i + "]");
                $(this).parent().find("label").attr("for", "reminder_email_before_" + i);
            });
            $(".reminder_sms_before").each(function (i, e) {
                $(this).attr("id", "reminder_sms_before_" + i);
                $(this).attr("name", "reminder_sms_before[" + i + "]");
                $(this).parent().find("label").attr("for", "reminder_sms_before_" + i);
            });
        });
    });

    function remove_schedule(e) {

        $(e).closest(".schedule").remove();
    }
</script>

<!-- Page Content -->
<div class="content">

    <!-- Material Forms Validation -->
    <h2 class="content-heading">Edit Institute Profile</h2>
    <div class="block">
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="block-content">
            <div class="row justify-content-center py-20">
                <div class="col-xl-6">
                    <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                    <div class="form-group">
                        <div class="form-material">
                            <?php $username = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id = "' . $this->session->userdata('school_id') . '"'); ?> 
                            <input required type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" readonly="">
                            <label for="name">Username</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material">
                            <input required type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $school_details['name']; ?>">
                            <label for="name">Institute Name</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input class="form-control" name="contact_person_name" id="contact_person_name"  type="text" value="<?php echo $school_details['contact_person_name']; ?>">
                                <label for="contact_person_name">Contact person name</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input class="form-control" name="mobile_no" id="mobile_no" type="text" value="<?php echo $school_details['mobile_no']; ?>">
                                <label for="mobile_no">Mobile Number</label>
                            </div></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input class="form-control" name="email_address" id="email_address" type="text" value="<?php echo $school_details['email_address']; ?>">
                                <label for="email_address">Email Address</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input class="form-control" name="address" id="Address" type="text" value="<?php echo $school_details['address']; ?>">
                                <label for="Address">Address</label>
                            </div>
                        </div>
                    </div>

                    <!--                                    <div class="form-group row">
                                                            <div class="col-12">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" name="city" id="city" type="text" value="<?php echo $school_details['city']; ?>">
                                                                    <label for="city">City</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-12">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" name="state" id="state" type="text" value="<?php echo $school_details['state']; ?>">
                                                                    <label for="state">State</label>
                                                                </div>
                                                            </div>
                                                        </div>-->

                    <div class="form-group">
                        <div class="form-material">
                            <select name="country" class="form-control" id="country" required>
                                <option value="">Select</option>
                                <?php
                                foreach ($country_list as $row) {
                                    if ($row['country_id'] == $school_details['country_id']) {
                                        $selected = "selected='selected'";
                                    } else {
                                        $selected = "";
                                    }
                                    ?>
                                    <option value="<?php echo $row['country_id']; ?>" <?php echo $selected; ?>><?php echo $row['name']; ?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label for="class_id">Select Country</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <?php
                        $imgfile = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'file_url', 'and id = "' . $this->session->userdata('school_id') . '"');
                        if ($imgfile != '') {
                            ?>
                            <img src="<?php echo $imgfile; ?>" style="width:100px;height:100px;" />
                            <span class="manu_chk"><input type="checkbox" name="del_img" class="form-group" value="1">Remove Image</span>

                            <?php
                        } else {
                            $imgfile = base_url() . '_images/avatar15.jpg';
                            ?>

                            <img src="<?php echo $imgfile; ?>"/>
                        <?php } ?>


                        <div class="form-group row">
                            <label class="col-12" for="userphoto">Upload Photo</label>
                            <div class="col-12">
                                <input type="file" id="adminphoto" name="adminphoto"><br>
                                (Image size should be 200px*200px )
                            </div>
                        </div>

                    </div>

                    <!-- Due reminder settings -->
                    <?php
                    $due_reminder_show = 'style="display: none;"';
                    if ($school_details['due_reminder_settings'] == 1) {
                        $due_reminder_show = '';
                    }
                    ?>
                    <div class="form-group">                                    
                        <div class="form-material">
                            <div style="margin-top: 8px;">
                                <div class="custom-control custom-checkbox custom-control-inline"> 
                                    <input class="custom-control-input due_reminder_settings" id="due_reminder_settings" type="checkbox" name="due_reminder_settings" value="1" <?php
                                    if ($school_details['due_reminder_settings'] == 1) {
                                        echo 'checked="checked"';
                                    }
                                    ?>>
                                    <label class="custom-control-label" for="due_reminder_settings">Enable</label>
                                </div>    
                            </div>  
                            <label>Due Reminder Settings</label>
                        </div>
                    </div>              

                    <div class="due_reminder_container" <?php echo $due_reminder_show; ?>>
                        <div class="after_schedule_append" style="border: 2px solid #eee; padding: 10px 8px; border-radius: 5px; margin-bottom: 5px;">
                            <label><b>Remind Parents After Due Date:</b></label>
                            <?php
                            if (!empty($after_schedules)) {
                                foreach ($after_schedules as $key => $schedule) {
                                    ?>
                                    <div class="schedule" style="border: 2px solid #eee; padding: 10px 8px; border-radius: 5px; margin-bottom: 5px;">
                                        <div class="form-group">
                                            <div class="form-material">
                                                <input required type="text" class="form-control" name="days_after[<?php echo $key; ?>]" placeholder="Number of days after due date" value="<?php echo $schedule['days']; ?>">
                                                <label>Days</label>
                                            </div>
                                        </div>

                                        <div class="form-group">                                                      
                                            <div class="form-material">
                                                <div style="margin-top: 8px;">
                                                    <div class="custom-control custom-checkbox custom-control-inline">                                                   
                                                        <input class="custom-control-input reminder_push_after" id="reminder_push_after_<?php echo $key; ?>" type="checkbox" name="reminder_push_after[<?php echo $key; ?>]" value="1" <?php
                                                        if ($schedule['reminder_push'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label class="custom-control-label" for="reminder_push_after_<?php echo $key; ?>">Push Notification</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline"> 
                                                        <input class="custom-control-input reminder_email_after" id="reminder_email_after_<?php echo $key; ?>" type="checkbox" name="reminder_email_after[<?php echo $key; ?>]" value="1" <?php
                                                        if ($schedule['reminder_email'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label class="custom-control-label" for="reminder_email_after_<?php echo $key; ?>">Email</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline"> 
                                                        <input class="custom-control-input reminder_sms_after" id="reminder_sms_after_<?php echo $key; ?>" type="checkbox" name="reminder_sms_after[<?php echo $key; ?>]" value="1" <?php
                                                        if ($schedule['reminder_sms'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label class="custom-control-label" for="reminder_sms_after_<?php echo $key; ?>">SMS</label>
                                                    </div>
                                                </div>    
                                                <label>Notification Type</label>
                                            </div>
                                        </div>

                                        <a href="javascript:" onclick="remove_schedule(this);"><i class="fa fa-minus-circle"></i> Remove Schedule</a>                                                
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <div class="form-group">                                                                                        
                            <a href="javascript:" class="after_schedule_copy_link"><i class="fa fa-plus"></i> Add Schedule</a>                                                    
                        </div>

                        <div class="before_schedule_append" style="border: 2px solid #eee; padding: 10px 8px; border-radius: 5px; margin-bottom: 5px;">
                            <label><b>Remind Parents Before Due Date:</b></label>   
                            <?php
                            if (!empty($before_schedules)) {
                                foreach ($before_schedules as $key => $schedule) {
                                    ?>
                                    <div class="schedule" style="border: 2px solid #eee; padding: 10px 8px; border-radius: 5px; margin-bottom: 5px;">
                                        <div class="form-group">
                                            <div class="form-material">
                                                <input required type="text" class="form-control" name="days_before[<?php echo $key; ?>]" placeholder="Number of days before due date" value="<?php echo $schedule['days']; ?>">
                                                <label>Days</label>
                                            </div>
                                        </div>

                                        <div class="form-group">                                                      
                                            <div class="form-material">
                                                <div style="margin-top: 8px;">
                                                    <div class="custom-control custom-checkbox custom-control-inline">                                                   
                                                        <input class="custom-control-input reminder_push_before" id="reminder_push_before_<?php echo $key; ?>" type="checkbox" name="reminder_push_before[<?php echo $key; ?>]" value="1" <?php
                                                        if ($schedule['reminder_push'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label class="custom-control-label" for="reminder_push_before_<?php echo $key; ?>">Push Notification</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline"> 
                                                        <input class="custom-control-input reminder_email_before" id="reminder_email_before_<?php echo $key; ?>" type="checkbox" name="reminder_email_before[<?php echo $key; ?>]" value="1" <?php
                                                        if ($schedule['reminder_email'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label class="custom-control-label" for="reminder_email_before_<?php echo $key; ?>">Email</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline"> 
                                                        <input class="custom-control-input reminder_sms_before" id="reminder_sms_before_<?php echo $key; ?>" type="checkbox" name="reminder_sms_before[<?php echo $key; ?>]" value="1" <?php
                                                        if ($schedule['reminder_sms'] == 1) {
                                                            echo 'checked="checked"';
                                                        }
                                                        ?>>
                                                        <label class="custom-control-label" for="reminder_sms_before_<?php echo $key; ?>">SMS</label>
                                                    </div>
                                                </div>    
                                                <label>Notification Type</label>
                                            </div>
                                        </div>

                                        <a href="javascript:" onclick="remove_schedule(this);"><i class="fa fa-minus-circle"></i> Remove Schedule</a>                                                
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div> 

                        <div class="form-group">                                                                                        
                            <a href="javascript:" class="before_schedule_copy_link"><i class="fa fa-plus"></i> Add Schedule</a>                                                   
                        </div>                                                    
                    </div>    

                    <div class="form-group">
                        <input type="hidden" name="school_id" value="<?php echo $school_details['id']; ?>">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Save">
                        <a href="javascript:" onclick="history.back();" class="btn btn-outline-danger">Cancel</a>
                    </div>

                    <?php echo form_close(); ?>
                    <?php if ($this->session->userdata('admin_id') && $this->session->userdata('admin_id') != '') { ?>
                        <hr style="border-top: 1px solid #000">     
                        <h1>Delete My Account</h1>
                        <?php echo form_open_multipart('admin/user/deleteSchool', array('id' => 'frmRegister1', 'class' => 'js-validation-material')); ?>
                        <div class="form-group">                                    
                            <div class="form-material">
                                <div style="margin-top: 8px;">
                                    <div class="custom-control custom-checkbox custom-control-inline"> 
                                        <input class="custom-control-input immediate_delete" id="immediate_delete" type="checkbox" name="immediate_delete" value="1" >
                                        <label class="custom-control-label" for="immediate_delete">Enable</label>
                                    </div>    
                                </div>  
                                <label>Delete my account immediately</label>
                            </div>
                        </div>
                        <input type="hidden" name="school_id" value="<?php echo $school_details['id']; ?>">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Delete">
                        <?php echo form_close(); ?>
                    <?php } ?>

                    <div class="schedule_copy_after" style="display: none;">     

                        <div class="schedule" style="border: 2px solid #eee; padding: 10px 8px; border-radius: 5px; margin-bottom: 5px;">
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="days_after[]" placeholder="Number of days after due date">
                                    <label>Days</label>
                                </div>
                            </div>

                            <div class="form-group">                                                      
                                <div class="form-material">
                                    <div style="margin-top: 8px;">
                                        <div class="custom-control custom-checkbox custom-control-inline">                                                   
                                            <input class="custom-control-input reminder_push_after" id="reminder_push_after" type="checkbox" name="reminder_push_after[]" value="1">
                                            <label class="custom-control-label" for="reminder_push_after">Push Notification</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline"> 
                                            <input class="custom-control-input reminder_email_after" id="reminder_email_after" type="checkbox" name="reminder_email_after[]" value="1">
                                            <label class="custom-control-label" for="reminder_email_after">Email</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline"> 
                                            <input class="custom-control-input reminder_sms_after" id="reminder_sms_after" type="checkbox" name="reminder_sms_after[]" value="1">
                                            <label class="custom-control-label" for="reminder_sms_after">SMS</label>
                                        </div>
                                    </div>    
                                    <label>Notification Type</label>
                                </div>
                            </div>

                            <a href="javascript:" onclick="remove_schedule(this);"><i class="fa fa-minus-circle"></i> Remove Schedule</a>                                                
                        </div>

                    </div>  

                    <div class="schedule_copy_before" style="display: none;">     

                        <div class="schedule" style="border: 2px solid #eee; padding: 10px 8px; border-radius: 5px; margin-bottom: 5px;">
                            <div class="form-group">
                                <div class="form-material">
                                    <input required type="text" class="form-control" name="days_before[]" placeholder="Number of days before due date">
                                    <label>Days</label>
                                </div>
                            </div>

                            <div class="form-group">                                                      
                                <div class="form-material">
                                    <div style="margin-top: 8px;">
                                        <div class="custom-control custom-checkbox custom-control-inline">                                                   
                                            <input class="custom-control-input reminder_push_before" id="reminder_push_before" type="checkbox" name="reminder_push_before[]" value="1">
                                            <label class="custom-control-label" for="reminder_push_before">Push Notification</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline"> 
                                            <input class="custom-control-input reminder_email_before" id="reminder_email_before" type="checkbox" name="reminder_email_before[]" value="1">
                                            <label class="custom-control-label" for="reminder_email_before">Email</label>
                                        </div>
                                        <div class="custom-control custom-checkbox custom-control-inline"> 
                                            <input class="custom-control-input reminder_sms_before" id="reminder_sms_before" type="checkbox" name="reminder_sms_before[]" value="1">
                                            <label class="custom-control-label" for="reminder_sms_before">SMS</label>
                                        </div>
                                    </div>    
                                    <label>Notification Type</label>
                                </div>
                            </div>

                            <a href="javascript:" onclick="remove_schedule(this);"><i class="fa fa-minus-circle"></i> Remove Schedule</a>                                                
                        </div>

                    </div>              

                </div>
            </div>
        </div>
    </div>
    <!-- END Material Forms Validation -->
</div>
<!-- END Page Content -->

<?php $this->load->view('school/_include/footer'); ?>



