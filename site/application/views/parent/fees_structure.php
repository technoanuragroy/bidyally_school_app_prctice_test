<?php $this->load->view('parent/include/header'); ?>


    <!-- Sidebar  -->
    <?php $this->load->view('parent/include/side_bar'); ?>

        <!-- Page Content  -->
        <div id="content">
            
            <?php $this->load->view('parent/include/header_nav'); ?>
            <div class="bodycontent">
                
                <h2 class="page_head"><?php echo $page_title; ?></h2>
                <div class="rowBox">
                    
                    <div class="formContent">
                        <?php
                            if(!empty($fees_data)) {  
                                                                                                                                       
                                echo form_open('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); 

                                    $selected = '';
                                    if(isset($breakup_details)) {  
                                        $selected = $breakup_details['id'];
                                    }                                                                        
                        ?>                 
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="break_ups">Select Fees</label>
                                            <select name="break_ups" class="form-control class" id="break_ups" onchange="this.form.submit();">
                                                <option value="">Select</option>
                                                <?php 
                                                    foreach($fees_data as $semesters) {                                                        
                                                ?>      
                                                        <optgroup label="Semester: <?php echo $semesters['semester_name']; ?>">
                                                            
                                                            <?php             
                                                                if(!empty($semesters['break_ups'])) {
                                                                    foreach($semesters['break_ups'] as $breakup_counter => $breakup) { 
                                                                        $disabled = '';
                                                                        if($breakup_counter > 0) {
                                                                            $disabled = 'disabled="disabled"';
                                                                        }
                                                            ?>
                                                                        <option value="<?php echo $breakup['id']; ?>" <?php if($breakup['id'] == $selected) { echo 'selected="selected"'; } ?> <?php echo $disabled; ?>><?php echo $breakup['breakup_label']; ?></option>
                                                            <?php                                         
                                                                    }  
                                                                } else {
                                                                    $payment_exist = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and student_id="' . $this->session->userdata('student_id') . '" and school_id="' . $this->session->userdata('school_id') . '" and fees_id="' . $semesters['structure']['id'] . '" and payment_status=1');
                                                                    if($payment_exist > 0) {
                                                                        echo '<option disabled="disabled">You have already paid the fees</option>';
                                                                    } else {
                                                                        echo '<option disabled="disabled">No data available</option>';
                                                                    }
                                                                }
                                                            ?>
                                                                    
                                                        </optgroup>
                                                <?php                                         
                                                    }                                                                                                       
                                                ?>
                                            </select>
                                            <span class="">Please pay the first due payment before proceeding towards the other payments.</span>
                                        </div>  
                                    </div>     
                        <?php 
                                echo form_close(); 
                                                                                                                                                                           
                            } else {
                                echo 'No data available';
                            }    
                        ?>  
                    </div>    
                                                                                                                        
                    <?php 
                        if(isset($breakup_details)) {                         
                            echo form_open('parent/user/paymentSummary', array('id' => 'frmRegister', 'class' => 'js-validation-material')); 
                    ?>                 
                                <div class="tableGrid">                                    
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col" style="text-align:left" width="80%">Fees Name</th>
                                                    <th scope="col" style="text-align:right" width="20%">Amount</th>                                                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php        
                                                    $fees = json_decode($breakup_details['fees'], true);
                                                    $index = 0;
                                                    foreach ($fees as $fees_list) {
                                                ?>     
                                                        <tr>
                                                            <td style="text-align:left">
                                                                <?php echo $fees_list['label']; ?>
                                                                <input type="hidden" name="label[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]" value="<?php echo $fees_list['label']; ?>">
                                                                <input type="hidden" name="option[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]" value="<?php echo $fees_list['option']; ?>">
                                                            </td>
                                                            <td style="text-align:right">
                                                                <?php 
                                                                    // Optional fees will have an extra option set to 0. For both single and multiple values
                                                                    if($fees_list['option'] == 1) {
                                                                        if( strpos($fees_list['amount'], ",") !== false ) {
                                                                            $fees_array = explode(",", $fees_list['amount']);
                                                                            array_unshift($fees_array, 0);
                                                                        } else {
                                                                            $fees_array = array(0, $fees_list['amount']);
                                                                        }                                                                       
                                                                ?>
                                                                        <select class="amount" name="amount[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]">
                                                                            <?php 
                                                                                foreach($fees_array as $fa) {
                                                                                    if($fa == 0 OR (int)$fa > 0) {
                                                                            ?>
                                                                                        <option value="<?php echo $fa; ?>"><?php echo number_format($fa, 2); ?></option>
                                                                            <?php
                                                                                    }
                                                                                }
                                                                            ?>    
                                                                        </select>
                                                                <?php 
                                                                    }                                                                                                                                                                                                                                                                            
                                                                    // Mandatory single fees will show as text, multiple fees will show as dropdown
                                                                    else {
                                                                        if( strpos($fees_list['amount'], ",") !== false ) {
                                                                            $fees_array = explode(",", $fees_list['amount']);
                                                                ?>
                                                                            <select class="amount" name="amount[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]">
                                                                                <?php 
                                                                                    foreach($fees_array as $fa) {
                                                                                        if($fa == 0 OR (int)$fa > 0) {
                                                                                ?>
                                                                                            <option value="<?php echo $fa; ?>"><?php echo number_format($fa, 2); ?></option>
                                                                                <?php
                                                                                        }
                                                                                    }
                                                                                ?>    
                                                                            </select>
                                                                <?php
                                                                        } else {
                                                                            echo number_format($fees_list['amount'], 2);
                                                                ?>     
                                                                            <input type="hidden" class="amount" name="amount[<?php echo $breakup_details['id']; ?>][<?php echo $index; ?>]" value="<?php echo $fees_list['amount']; ?>">
                                                                <?php 
                                                                        }
                                                                    }
                                                                ?>                                                                                                                                                                                                               
                                                            </td>                                                                 
                                                        </tr>
                                                <?php
                                                        $index++;
                                                    }
                                                ?>
                                                <!-- Total amount -->        
                                                <tr style="border-top: 3px solid #333">
                                                    <td style="text-align:left">
                                                        <b>Total Amount</b> 
                                                    </td>  
                                                    <td style="text-align:right">
                                                        <span class="fees_amount">
                                                            
                                                        </span>    
                                                    </td>                                                    
                                                </tr>                                                                                                  
                                            </tbody>
                                        </table>
                                    </div>
                                              
                                    <?php 
                                        // Enable the payment option only if the online payment settings is enabled
                                        if(!empty($payment_settings)) {
                                            if($payment_settings['online_payment_enabled'] == 1) {
                                    ?> 
                                                <div style="display: block; width: 100%; float: left; text-align: left;">
                                                    <h3>Pay Fees</h3>
                                                    <?php foreach($payment_gateways as $pg) { ?>                                                
                                                            <div style="display: block; width: 100%; float: left; text-align: left;">     
                                                                <label>
                                                                    <input type="radio" name="payment_gateway_id" value="<?php echo $this->my_custom_functions->ablEncrypt($pg['id']); ?>" <?php if($pg['default_gateway'] == 1) { echo 'checked="checked"'; } ?>>&nbsp;<?php echo $pg['label_for_ui']; ?>
                                                                </label>
                                                            </div>
                                                    <?php } ?>       
                                                </div>                                    
                                                                                           
                                                <div class="form-group buttonContainer">    
                                                    <?php 
                                                        if(isset($breakup_details['manual_breakup_id'])) {
                                                    ?>
                                                            <input type="hidden" name="manual_breakup_id" value="<?php echo $this->my_custom_functions->ablEncrypt($breakup_details['manual_breakup_id']); ?>">                                        
                                                    <?php
                                                        }
                                                    ?>
                                                    <input type="hidden" name="breakup_id" value="<?php echo $this->my_custom_functions->ablEncrypt($breakup_details['id']); ?>">                                        
                                                    <input type="hidden" name="fees_id" value="<?php echo $this->my_custom_functions->ablEncrypt($breakup_details['fees_id']); ?>">
                                                    <input type="hidden" name="total_fees" class="total_fees" value="0">
                                                    <input type="submit" name="submit" class="btn002" value="Continue to Payment">
                                                </div>
                                    <?php                                     
                                            }
                                        }    
                                    ?>    
                                </div>
                    <?php                     
                            echo form_close(); 
                        } 
                    ?>                                                                                         
                </div>
                
    <script type="text/javascript">
        $(document).ready(function() {
            calculate_price();

            $(".amount").on("click change", function() {
                calculate_price();
            }); 
        });

        function calculate_price() { 

            var total_fees = 0;    
            $(".amount").each(function() {                                                                                                                                   
                total_fees += Number($(this).val());                                                                                    
            });     
            
            $(".total_fees").val(total_fees);
            $(".fees_amount").text('<?php echo INDIAN_CURRENCY_CODE; ?> '+total_fees.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));                                              
        }
    </script>               
                
<?php $this->load->view('parent/include/footer'); ?>
                 
