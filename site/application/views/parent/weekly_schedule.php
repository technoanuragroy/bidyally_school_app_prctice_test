<?php $this->load->view('parent/include/header'); ?>


            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <div class="routine_timetable">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <?php
                                $weeklist = $this->config->item('days_list');
                                $active_flag = 0;
                                $tab_cnt = 0;
                                foreach ($weeklist as $day => $val) {
                                    if ($active_flag == 0) {
                                        $active = 'active';
                                    } else {
                                        $active = '';
                                    }
                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link <?php echo $active; ?>" id="<?php echo $weeklist[$day]; ?>-tab" data-toggle="tab" href="#<?php echo $weeklist[$day]; ?>" role="tab" aria-controls="<?php echo $weeklist[$day]; ?>" aria-selected="true"><?php echo substr($weeklist[$day], 0, 3); ?></a>
                                    </li>
                                    <?php
                                    $active_flag++;
                                }
                                ?>


                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <?php
                                if (!empty($schedule)) {

                                    foreach ($schedule as $day => $class_list_data) {
                                        if ($tab_cnt == 0) {
                                            $tab_active = 'active';
                                        } else {
                                            $tab_active = '';
                                        }
                                        ?>

                                        <div class="tab-pane fade show <?php echo $tab_active; ?>" id="<?php echo $weeklist[$day]; ?>" role="tabpanel" aria-labelledby="<?php echo $weeklist[$day]; ?>-tab">
                                            <ul class="routinelist">
                                                <?php
                                                foreach ($class_list_data as $class_list) {
                                                    ?>
                                                    <li>
                                                        <span class="subjectsDiv"><small>
                                                                <?php
                                                                $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $class_list['subject_id'] . '" ');
                                                                if ($subject_name != '') {
                                                                    echo $subject_name;
                                                                } else {
                                                                    echo $class_list['period_name'];
                                                                }
                                                                ?></small></span>
                                                        <h3>
                                                            <?php
                                                            if ($class_list['teacher_id'] != 0) {
                                                                $check_temp_teacher = $this->my_custom_functions->get_perticular_count(TBL_TEMP_TIMETABLE, 'and class_id = "' . $class_list['class_id'] . '" and section_id = "' . $class_list['section_id'] . '" and  subject_id = "' . $class_list['subject_id'] . '" and class_date = "' . date('Y-m-d') . '"');
                                                                ?>
                                                                <i class="far fa-user"></i> 
                                                                <?php
                                                                if ($check_temp_teacher > 0) {
                                                                    $teacher_id = $this->my_custom_functions->get_particular_field_value(TBL_TEMP_TIMETABLE, 'teacher_id', 'and class_id = "' . $class_list['class_id'] . '" and section_id = "' . $class_list['section_id'] . '" and subject_id = "' . $class_list['subject_id'] . '" and class_date = "' . date('Y-m-d') . '"');
                                                                    $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $teacher_id . '" ');
                                                                    $name = $this->my_custom_functions->get_short_name($teacher_name);
                                                                    echo $name;
                                                                } else {
                                                                    $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'name', 'and id = "' . $class_list['teacher_id'] . '" ');
                                                                    $name = $this->my_custom_functions->get_short_name($teacher_name);
                                                                    echo $name;
                                                                }
                                                                ?>

                                                            <?php } else { ?>
                                                                --
                                                            <?php } ?>
                                                        </h3>
                                                        <span class="tag_section">

                                                            <i class="far fa-clock"></i> 
                                                            <?php echo date('h:i a', strtotime($class_list['period_start_time'])) . ' - <i class="far fa-clock"></i> ' . date('h:i a', strtotime($class_list['period_end_time'])); ?>

                                                        </span>
                                                    </li>
                                                <?php } ?>

                                            </ul>


                                        </div>
                                        <?php
                                        $tab_cnt++;
                                    }
                                }
                                ?>


                            </div>
                        </div>


                    </div>
                    <?php $this->load->view('parent/include/footer'); ?>
