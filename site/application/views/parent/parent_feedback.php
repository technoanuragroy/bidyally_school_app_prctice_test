<?php $this->load->view('parent/include/header'); ?>


<!-- Sidebar  -->
<?php $this->load->view('parent/include/side_bar'); ?>

<!-- Page Content  -->
<div id="content">
    <?php $this->load->view('parent/include/header_nav'); ?>
    <div class="bodycontent">
        <h2 class="page_head"><?php echo $page_title; ?></h2>
        <div class="col-md-12">
            <?php if ($this->session->flashdata("s_message")) { ?>
                <!-- Success Alert -->
                <div class="alert alert-success alert-dismissable s_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                </div>
                <!-- END Success Alert -->
            <?php } ?>
            <?php if ($this->session->flashdata("e_message")) { ?>
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable e_message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                    <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                </div>
                <!-- END Danger Alert -->
            <?php } ?>
        </div>

        <div class="rowBox">
            

               
                    <div class="formContent">

                        <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>


                        <div class="form-row">

                            <div class="form-group col-md-12">
                                <label for="inputFirstname">Feedback Type</label>
                                <?php $feedback_type = $this->config->item('feedback_type'); ?>
                                <select name="feedback_type" class="form-control" required>
                                    <option value="">Select</option>
                                    <?php
                                    foreach ($feedback_type as $key=>$feedback) {
                                        
                                        ?>
                                        <option value="<?php echo $key; ?>" ><?php echo $feedback; ?></option>
                                    <?php } ?>

                                </select>
                            </div>



                            <div class="form-group col-md-12">
                                <label for="inputFirstname">Comments</label>
                                <textarea name="comments" class="form-control" rows="40"></textarea>
                            </div>


                            <div class="form-group buttonContainer">

                                <input type="submit" name="submit" class="btn002" value="Send">

                            </div>





                        </div> 

                        <?php echo form_close(); ?>

                    </div>
               
           
        </div>
        <?php $this->load->view('teacher/include/footer'); ?>
        <script type="text/javascript">
<?php if ($this->uri->segment(4) && $this->uri->segment(4) != '') { ?>
                $('[href="#create"]').tab('show');
<?php } ?>
        </script>