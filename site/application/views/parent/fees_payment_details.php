<?php $this->load->view('parent/include/header'); ?>


    <!-- Sidebar  -->
    <?php $this->load->view('parent/include/side_bar'); ?>

        <!-- Page Content  -->
        <div id="content">
            
            <?php $this->load->view('parent/include/header_nav'); ?>
            <div class="bodycontent">
                                
                <div class="rowBox">
                    
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                                                                                                                                                                
                    <?php 
                        if(!empty($payment_details)) {                                                     
                    ?>           
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="break_ups">Receipt No: <?php echo $payment_details['id']; ?></label>
                                    <div class="downloadFiles">
                                        <?php 
                                            $student_payment_id = $this->session->userdata("student_id").'_'.$payment_details['id'];
                                        ?>
                                        <span class="files_icon">
                                            <a href="<?php echo base_url(); ?>parent/user/downloadPaymentReceipt/<?php echo $this->my_custom_functions->ablEncrypt($student_payment_id); ?>/<?php echo DOWNLOAD_KEY; ?>">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>    
                    
                            <h2 class="page_head">Fees Details</h2>
                    
                            <div class="tableGrid">                                    
                                <div class="table-responsive">
                                    <table class="table table-striped">                                                                                       
                                        <tbody>                                                 
                                            <tr>
                                                <td style="text-align:left">
                                                    School
                                                </td>
                                                <td style="text-align:left">
                                                    <?php echo $payment_details['school_name']; ?>
                                                </td>
                                            </tr>    
                                            <tr>
                                                <td style="text-align:left">
                                                    Semester Info
                                                </td>
                                                <td style="text-align:left">
                                                    <?php 
                                                        if($payment_details['class_info'] != "") {
                                                            $class_info_array = json_decode($payment_details['class_info'], true);
                                                            if(is_array($class_info_array)) {                                                                
                                                                if(array_key_exists('semester', $class_info_array)) {
                                                                    echo 'Semester: '.$class_info_array['semester'].', ';
                                                                }
                                                                if(array_key_exists('class', $class_info_array)) {
                                                                    echo 'Class: '.$class_info_array['class'].', ';
                                                                }                                                                
                                                                if(array_key_exists('roll_no', $class_info_array)) {
                                                                    echo 'Roll No: '.$class_info_array['roll_no'];
                                                                }
                                                            } else {
                                                                echo 'N/A';
                                                            }
                                                        }
                                                    ?>                                                                                                                                                                                                           
                                                </td>  
                                            </tr> 
                                            <tr>
                                                <td style="text-align:left">
                                                    Fees
                                                </td>
                                                <td style="text-align:left">
                                                    <?php echo $payment_details['fees_name']; ?>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td style="text-align:left">
                                                    Mode
                                                </td>
                                                <td style="text-align:left">
                                                    <?php echo ($payment_details['created_by'] == 1) ? "Offline" : "Online"; ?>
                                                </td>
                                            </tr>  
                                            <tr>
                                                <td style="text-align:left">
                                                    Date
                                                </td>
                                                <td style="text-align:left">
                                                    <?php echo date("d-m-Y, g:ia", strtotime($payment_details['payment_datetime'])); ?>
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td style="text-align:left">
                                                    Amount
                                                </td>
                                                <td style="text-align:left">
                                                    <?php 
                                                        $paid_amount = $payment_details['paid_amount'];
                                                        $payment_handling_fees = $payment_details['service_charges'];
                                                        $display_amount = $paid_amount - $payment_handling_fees;
                                                        echo INDIAN_CURRENCY_CODE.' '.number_format($display_amount, 2); 
                                                    ?>                                                            
                                                </td>                                                    
                                            </tr>                                                                                        
                                        </tbody>
                                    </table>
                                </div>                                
                            </div>
                    <?php                                                 
                        } else {
                            echo 'No data available';
                        }  
                    ?>                                                                                         
                    
                    <br><br>
                    
                    <h2 class="page_head">Fees Breakup</h2>
                    
                    <?php 
                        if(!empty($payment_details)) {                                                     
                    ?>                 
                            <div class="tableGrid">                                    
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>                                               
                                                <th scope="col" style="text-align:left">Fees Name</th>  
                                                <th scope="col" style="text-align:right">Amount</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>                                                 
                                            <?php          
                                                $fees_array = json_decode($payment_details['fees'], true);
                                                if(is_array($fees_array)) {
                                                    foreach($fees_array as $fees) {
                                            ?>
                                                        <tr>
                                                            <td style="text-align:left"><?php echo $fees['label']; ?></td>
                                                            <td style="text-align:right"><?php echo INDIAN_CURRENCY_CODE.' '.number_format($fees['amount'], 2); ?></td>
                                                        </tr>
                                            <?php
                                                    }
                                            ?>
                                                    <!-- Actual fees amount -->   
                                                    <tr style="border-top: 3px solid #333">
                                                        <td style="text-align:left">
                                                            <b>Fees Amount</b> 
                                                        </td>  
                                                        <td style="text-align:right">
                                                            <span class="fees_amount">
                                                                <?php echo INDIAN_CURRENCY_CODE . number_format($payment_details['fees_amount'], 2); ?>
                                                            </span>    
                                                        </td>                                                    
                                                    </tr>

                                                    <!-- Late fine amount if any -->
                                                    <tr style="border-top: 3px solid #333">
                                                        <td style="text-align:left">
                                                            <b>Late Fine</b> 
                                                        </td>  
                                                        <td style="text-align:right">
                                                            <span class="fees_amount">
                                                                <?php echo INDIAN_CURRENCY_CODE . number_format($payment_details['late_fine'], 2); ?>
                                                            </span>    
                                                        </td>                                                    
                                                    </tr>

                                                    <?php /*
                                                    <!-- Payment handling fees(if charged from customer) --> 
                                                    <?php if($payment_settings['add_on_school_fees'] > 0 AND $payment_details['service_charges'] > 0) { ?>                                                                                                                                                               
                                                            <tr style="border-top: 3px solid #333">
                                                                <td style="text-align:left">
                                                                    <b>Payment Handling Fees</b> 
                                                                </td>  
                                                                <td style="text-align:right">
                                                                    <span class="fees_amount">
                                                                        <?php echo INDIAN_CURRENCY_CODE . number_format($payment_details['service_charges'], 2); ?>
                                                                    </span>    
                                                                </td>                                                    
                                                            </tr>                                                      
                                                    <?php } ?> 
                                                    */ ?>                                                  

                                                    <!-- Total payable amount -->   
                                                    <tr style="border-top: 3px solid #333">
                                                        <td style="text-align:left">
                                                            <b>Paid Amount</b> 
                                                        </td>  
                                                        <td style="text-align:right">
                                                            <span class="fees_amount">
                                                                <?php echo INDIAN_CURRENCY_CODE . number_format($display_amount, 2); ?>
                                                            </span>    
                                                        </td>                                                    
                                                    </tr>       
                                            <?php            
                                                } else { 
                                            ?>
                                                    <tr>
                                                        <td colspan="2">No data available</td>
                                                    </tr>
                                            <?php
                                                }
                                            ?>                                                                                        
                                        </tbody>
                                    </table>
                                </div>                                
                            </div>
                    <?php                                                 
                        } else {
                            echo 'No data available';
                        }  
                    ?>                                                                                         
                </div>
                
    <script type="text/javascript">
        $(document).ready(function() {
            
        });        
    </script>               
                
<?php $this->load->view('parent/include/footer'); ?>
                 
