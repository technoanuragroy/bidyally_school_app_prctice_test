<?php $this->load->view('parent/include/header'); ?>


    <!-- Sidebar  -->
    <?php $this->load->view('parent/include/side_bar'); ?>

        <!-- Page Content  -->
        <div id="content">
            
            <?php $this->load->view('parent/include/header_nav'); ?>
            <div class="bodycontent">
                
                <h2 class="page_head"><?php echo $page_title; ?></h2>
                <div class="rowBox">
                                  
                    <div class="form-group">                          
                        <span>
                            Semester: <?php echo $semester_details['semester_name']; ?>
                        </span>,      
                        <span>
                            Paying for: <?php echo $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE_BREAKUPS, 'breakup_label', 'and id="' . $this->my_custom_functions->ablDecrypt($this->input->post('breakup_id')) . '"'); ?>
                        </span>     
                    </div>
                    
                    <?php 
                        if($fees_json != "") {                              
                    ?>                 
                            <div class="tableGrid">                                    
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" style="text-align:left" width="80%">Fees Name</th>
                                                <th scope="col" style="text-align:right" width="20%">Amount</th>                                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php        
                                                $fees = json_decode($fees_json, true);
                                                foreach ($fees as $fees_list) {
                                            ?>     
                                                    <tr>
                                                        <td style="text-align:left">
                                                            <?php echo $fees_list['label']; ?>                                                                
                                                        </td>
                                                        <td style="text-align:right">
                                                            <?php echo number_format($fees_list['amount'], 2); ?>                                                                                                                                                                                                                                                                                 
                                                        </td>                                                                 
                                                    </tr>
                                            <?php                                                        
                                                }
                                            ?>
                                                                                                                                           
                                            <!-- Actual fees amount -->   
                                            <tr style="border-top: 3px solid #333">
                                                <td style="text-align:left">
                                                    <b>Fees Amount</b> 
                                                </td>  
                                                <td style="text-align:right">
                                                    <span class="fees_amount">
                                                        <?php echo INDIAN_CURRENCY_CODE . number_format($fees_amounts['fees_amount'], 2); ?>
                                                    </span>    
                                                </td>                                                    
                                            </tr>
                                            
                                            <!-- Late fine amount if any -->
                                            <tr style="border-top: 3px solid #333">
                                                <td style="text-align:left">
                                                    <b>Late Fine</b> 
                                                </td>  
                                                <td style="text-align:right">
                                                    <span class="fees_amount">
                                                        <?php echo INDIAN_CURRENCY_CODE . number_format($fees_amounts['late_fine'], 2); ?>
                                                    </span>    
                                                </td>                                                    
                                            </tr>
                                            
                                            <!-- Payment handling fees(if charged from customer) --> 
                                            <?php if($payment_settings['add_on_school_fees'] > 0 AND $fees_amounts['service_charges'] > 0) { ?>                                                                                                                                                               
                                                    <tr style="border-top: 3px solid #333">
                                                        <td style="text-align:left">
                                                            <b>Payment Handling Fees</b> 
                                                        </td>  
                                                        <td style="text-align:right">
                                                            <span class="fees_amount">
                                                                <?php echo INDIAN_CURRENCY_CODE . number_format($fees_amounts['service_charges'], 2); ?>
                                                            </span>    
                                                        </td>                                                    
                                                    </tr>                                                      
                                            <?php } ?>  
                                                    
                                            <!-- Total payable amount -->   
                                            <tr style="border-top: 3px solid #333">
                                                <td style="text-align:left">
                                                    <b>Total Payable Amount</b> 
                                                </td>  
                                                <td style="text-align:right">
                                                    <span class="fees_amount">
                                                        <?php echo INDIAN_CURRENCY_CODE . number_format($fees_amounts['total_amount'], 2); ?>
                                                    </span>    
                                                </td>                                                    
                                            </tr>        
                                        </tbody>
                                    </table>
                                </div>
                                
                                <?php                                     
                                    echo form_open('parent/user/paymentGateway', array('id' => 'frmRegister', 'class' => 'js-validation-material')); 
                                ?>    
                                        <div style="display: block; width: 100%; float: left; text-align: left; margin-bottom: 20px; margin-top: -20px;">
                                            <span>Payment mode selected: <?php echo $this->my_custom_functions->get_particular_field_value(TBL_PAYMENT_GATEWAY, 'label_for_ui', 'and id="' . $this->my_custom_functions->ablDecrypt($this->input->post('payment_gateway_id')) . '"'); ?>                                          
                                        </div>   

                                        <div class="form-group buttonContainer" style="text-align: left;">    
                                            <?php 
                                                if($this->input->post('manual_breakup_id')) {
                                            ?>
                                                    <input type="hidden" name="manual_breakup_id" value="<?php echo $this->input->post('manual_breakup_id'); ?>">                                        
                                            <?php
                                                }
                                            ?>
                                            <input type="hidden" name="fees_json" value="<?php echo $this->my_custom_functions->ablEncrypt($fees_json); ?>">                                            
                                            <input type="hidden" name="payment_gateway_id" value="<?php echo $this->input->post('payment_gateway_id'); ?>">           
                                            <input type="hidden" name="breakup_id" value="<?php echo $this->input->post('breakup_id'); ?>">                                        
                                            <input type="hidden" name="fees_id" value="<?php echo $this->input->post('fees_id'); ?>">
                                            <input type="hidden" name="fees_amount" value="<?php echo $fees_amounts['fees_amount']; ?>">
                                            <input type="hidden" name="late_fine" value="<?php echo $fees_amounts['late_fine']; ?>">
                                            <input type="hidden" name="service_charges" value="<?php echo $fees_amounts['service_charges']; ?>">
                                            <input type="hidden" name="total_amount" value="<?php echo $fees_amounts['total_amount']; ?>">
                                            <input type="submit" name="submit" class="btn002" value="Pay Now">
                                        </div>
                                <?php 
                                    echo form_close(); 
                                ?>    
                            </div>
                    <?php                                                 
                        } 
                    ?>                                                                                         
                </div>
                
    <script type="text/javascript">
        $(document).ready(function() {
            
        });        
    </script>               
                
<?php $this->load->view('parent/include/footer'); ?>
                 
