<?php $this->load->view('parent/include/header'); ?>
<style>
    .onOff {
        display:none;
    }
    .totop {
        /*        position: fixed;*/
        bottom: 10px;
        right: 20px;
        padding: 10px;
        text-align: center;
        background-color: #333;
        color: #fff;
        display: block;
        width: 70%;
        margin: 17px auto;
        border-radius: 4px;
    }
    .totop{
        display: none;
    }

    a:hover {
        text-decoration: none;
    }
  
</style>
<script type="text/javascript">
 $(function () {
        $(".onOff").slice(0, 4).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $(".onOff:hidden").slice(0, 4).slideDown();
            if ($(".onOff:hidden").length == 0) {
                $("#load").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
        });
    });
    </script>

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="col-md-12">
                            <?php if ($this->session->flashdata("s_message")) { ?>
                                    <!-- Success Alert -->
                                    <div class="alert alert-success alert-dismissable s_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Success Alert -->
                            <?php } ?>
                                    <?php if ($this->session->flashdata("e_message")) { ?>
                                    <!-- Danger Alert -->
                                    <div class="alert alert-danger alert-dismissable e_message" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                                    </div>
                                    <!-- END Danger Alert -->
                                    <?php } ?>
                                </div>
                    <ul class="nav nav-tabs diaryTab highlightTab" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="buttontab viewClass nav-link textpos active" id="view-tab" data-toggle="tab" href="#view" role="tab" aria-controls="view" aria-selected="true">View</a>
                        </li>
                        <li class="nav-item">
                            <a class="buttontab createClass nav-link textpos" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create" aria-selected="true">Create</a>
                        </li>
                    </ul>
                    <div class="rowBox">
                        <div class="tab-content gapContent highlightContent" id="myTabContent">
                            <div class="tab-pane fade show active" id="view" role="tabpanel" aria-labelledby="view-tab">
                                <?php
                                if (!empty($diarylist)) {//echo "<pre>";print_r($diarylist);
                                    $count = 1;
                                    foreach ($diarylist as $diary) {

                                        $break_start_date = explode('-', date('Y-m-d', $diary['issue_date']));
                                        $year = $break_start_date[0];
                                        $monthe = $break_start_date[1];
                                        $day = $break_start_date[2];

                                        $monthNum = $monthe;
                                        $dateObj = DateTime::createFromFormat('!m', $monthNum);
                                        $monthName = $dateObj->format('M'); // March
                                        ?>
                                        <a href="<?php echo base_url(); ?>parent/user/diaryDetail/<?php echo $diary['id']; ?>" class="onOff ads">
                                            <?php 
                                            $read_cls = '';
                                            if($diary['parent_read_msg'] == 2){
                                                $read_cls = 'read_cls';
                                                ?>
                                            
                                            <?php } ?>
                                            <div class="noticeGrid <?php echo $read_cls; ?>">
                                                <div class="grid_row">
                                                    <div class="grid_row_left">
                                                        <h2><?php echo $day; ?></h2>
                                                        <span class="dayText"><?php echo $monthName; ?></span>
                                                    </div>
                                                    <div class="grid_row_right">
                                                        <h2 class="grid_row_sub"><?php echo $diary['heading']; ?></h2>
                                                        <span class="arrow_ind">
                                                            <?php if($diary['status'] == 1){?>
                                                            <i class="fas fa-arrow-left incoming"></i>
                                                            <?php }else{ ?>
                                                            
                                                            <i class="fas fa-arrow-right outgoing"></i>
                                                                <?php } ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <?php
                                        $count++;
                                    }
                                     if ($count > 4) {
                                            ?>
                                            <a href="#" id="loadMore">Load More</a>

                                            <p class="totop">
                                                <a href="#top">Back to top</a>
                                            </p>
                                        <?php }
                                }
                                ?>


                            </div>
                            <div class="tab-pane fade show" id="create" role="tabpanel" aria-labelledby="create-tab">
                                <div class="formContent">
                                    <?php 
                                    $teacher_id = '';
                                    $subject_id = '';
                                    $heading = '';
                                    if($this->uri->segment(4) && $this->uri->segment(4) != ''){
                                        $diary_id = $this->uri->segment(4);
                                        $teacher_id = $this->my_custom_functions->get_particular_field_value(TBL_DIARY,'teacher_id','and id = "'.$diary_id.'"');
                                        $subject_id = $this->my_custom_functions->get_particular_field_value(TBL_DIARY,'subject_id','and id = "'.$diary_id.'"');
                                        $heading = $this->my_custom_functions->get_particular_field_value(TBL_DIARY,'heading','and id = "'.$diary_id.'"');
                                        if($heading != ''){
                                            $heading = 'RE:'.$heading;
                                        }else{
                                            $heading = '';
                                        }
                                        
                                        if($subject_id != 0){
                                            $subject_id = $subject_id;
                                        }else{
                                            $subject_id = 0;
                                        }
                                    }
                                    
                                    
                                    ?>
                                    
                                    
                                    
                                    
                                    <?php echo form_open_multipart('parent/user/studentDiary', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                                   
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Select Teacher</label>
                                            <select name="teacher_id" class="form-control" required>
                                                <option value="">Select</option>
                                                <?php foreach($teacher_list as $teacher){ 
                                                    if($teacher['id'] == $teacher_id){
                                                        $selected = "selected='selected'";
                                                    }else{
                                                        $selected = "";
                                                    }
                                                    ?>
                                                <option value="<?php echo $teacher['id']; ?>" <?php echo $selected; ?>><?php echo $teacher['name']; ?></option>
                                                <?php } ?>
                                                
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                             <label for="inputFirstname">Select Subject</label>
                                            <select name="subject_id" class="form-control">
                                                <option value="">Select</option>
                                                <?php foreach($subject_list as $subject){ 
                                                    if($subject['id'] == $subject_id){
                                                        $selected = "selected='selected'";
                                                    }else{
                                                        $selected = ""; 
                                                    }
                                                    ?>
                                                <option value="<?php echo $subject['id']; ?>" <?php echo $selected; ?>><?php echo $subject['subject_name']; ?></option>
                                                <?php } ?>
                                                <option value="0" <?php if($subject_id == 0){echo "selected";} ?>>other</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Heading</label>
                                            <input required type="text" name="heading" class="form-control" value="<?php echo $heading; ?>">
                                        </div>
                                    
                                        <div class="form-group col-md-12">
                                            <label for="inputFirstname">Note</label>
                                            <textarea name="diary_note" class="form-control" rows="20"></textarea>
                                        </div>


                                        <div class="form-group buttonContainer">
                                            
                                            <input type="submit" name="submit" class="btn002" value="Send">

                                        </div>

                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
                    <script type="text/javascript">
                       <?php if($this->uri->segment(4) && $this->uri->segment(4) != ''){ ?> 
                        $('[href="#create"]').tab('show');
                        <?php } ?>
                        </script>