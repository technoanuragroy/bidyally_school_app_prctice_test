<?php $this->load->view('parent/include/header'); ?>


<!-- Sidebar  -->
<?php $this->load->view('parent/include/side_bar'); ?>

<!-- Page Content  -->
<div id="content">
    <?php $this->load->view('parent/include/header_nav'); ?>
    <div class="bodycontent dashboard_parent">
<!--                    <h2 class="page_head"><?php //echo $page_title;  ?></h2>-->
        <?php //echo "<pre>";print_r($this->session->all_userdata()); ?>
        <div class="rowBox">
            <div class="dashbordprofileGrid">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <?php
                        $disp_pic_path = '';
                        $class_name = '';
                        $section_name = '';
                        if (!empty($student_detail)) {
                            //echo '<pre>';print_r($student_detail);
                            $child_url = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL, 'file_url', 'and student_id = "' . $student['id'] . '"');

                            $disp_pic_path = $child_url;
                            $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_detail[0]['class_id'] . '"');
                            $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_detail[0]['section_id'] . '"');
                            $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $student['id'] . '"');
                            //echo $this->db->last_query();
                            ?>
                            <a href="<?php echo base_url(); ?>parent/user/dashBoard">
                                <span class="profileimg">
                                    <?php if ($disp_pic_path != '') { ?>
                                        <img src="<?php echo $disp_pic_path ?>" alt="" class="">
                                    <?php } else { ?>
                                        <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                    <?php } ?>
                                </span>
                            </a>

                            <h3><?php echo $student_name; ?></h3>
                            <span class="tag_section">Class - <?php echo $class_name; ?>(<?php echo $section_name; ?>) </span>
                            <span class="tag_section">Roll No: <?php echo $student_detail[0]['roll_no']; ?></span>
                            <!-- <a href="#" class="btn_select">Select</a> -->
                        </div>
                    <?php } else {
                        ?>
                        <a href="<?php echo base_url(); ?>parent/user/dashBoard">
                            <span class="profileimg">
                                
                                    <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                
                            </span>
                        </a>

                        
                        <span class="tag_section">No enrollment for this session </span>
                        
                        <!-- <a href="#" class="btn_select">Select</a> -->
                    </div>
                <?php } ?>


                <div class="col-lg-4 col-md-4 col-sm-4 switch_cls">
                    <?php
                    //echo "<pre>";print_r($this->session->all_userdata()); 
                    //$parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK,'parent_id','and school_id = "'.$this->session->userdata('school_id').'" and student_id = "'.$this->session->userdata('student_id').'"');
                    $parent_id = $this->session->userdata('parent_id');
                    $child_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, ' and parent_id = "' . $parent_id . '"');
                    //echo $this->db->last_query();
                    if ($child_count > 1) {
                        ?>

                        <a href="<?php echo base_url(); ?>parent/user/select_student">
                            <div class="section_Grid ">
                                <span class="icon_tag">
                                    <i class="fas fa-exchange-alt"></i>
    <!--                                        <img src="<?php echo base_url(); ?>app_images/syllabus.png" alt=""/>-->
                                </span>
                                <h3>Switch Student</h3>
                            </div>
                        </a>
                    <?php } ?>

                </div>
            </div>
        </div>


        <div class="sectionrowBox boxdashbord">
            <a href="<?php echo base_url(); ?>parent/user/dailyWorks">
                <div class="section_Grid ">
                    <span class="icon_tag">
                        <i class="fas fa-book-reader"></i>
                    </span>
                    <h3>daily work</h3>
                </div>
            </a>


            <a href="<?php echo base_url(); ?>parent/user/studentDiary" class="ads">
                <div class="section_Grid ">
                    <span class="icon_tag">
                        <i class="fas fa-book"></i>
                    </span>
                    <h3>diary</h3>
                </div>
            </a>

            <a href="<?php echo base_url(); ?>parent/user/attendance">
                <div class="section_Grid ">
                    <span class="icon_tag">
                      <!-- <i class="fas fa-book"></i> -->
                        <img src="<?php echo base_url(); ?>app_images/Attendance.png" alt=""/>
                    </span>
                    <h3>Attendance</h3>
                </div>
            </a>

            <a href="<?php echo base_url(); ?>parent/user/notice">
                <div class="section_Grid ">
                    <span class="icon_tag">
                        <i class="fas fa-bell"></i>
                    </span>
                    <h3>notice</h3>
                </div>
            </a>

            <a href="<?php echo base_url(); ?>parent/user/timeTable">
                <div class="section_Grid ">
                    <span class="icon_tag">
                        <i class="fas fa-calendar-alt"></i>
                    </span>
                    <h3>time table</h3>
                </div>
            </a>

            <a href="<?php echo base_url(); ?>parent/user/syllabus">
                <div class="section_Grid ">
                    <span class="icon_tag">
                      <!-- <i class="fas fa-book"></i> -->
                        <img src="<?php echo base_url(); ?>app_images/syllabus.png" alt=""/>
                    </span>
                    <h3>syllabus</h3>
                </div>
            </a>
            <?php
            ///echo "<pre>";print_r($this->session->all_userdata()); 
            //$parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK,'parent_id','and school_id = "'.$this->session->userdata('school_id').'" and student_id = "'.$this->session->userdata('student_id').'"');
            $parent_id = $this->session->userdata('parent_id');
            $child_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, ' and parent_id = "' . $parent_id . '"');
            if ($child_count > 1) {
                ?>

                <a href="<?php echo base_url(); ?>parent/user/select_student">
                    <div class="section_Grid ">
                        <span class="icon_tag">
                            <i class="fas fa-exchange-alt"></i>
    <!--                                        <img src="<?php echo base_url(); ?>app_images/syllabus.png" alt=""/>-->
                        </span>
                        <h3>Switch Student</h3>
                    </div>
                </a>
            <?php } ?>




        </div>


    </div>
    <?php $this->load->view('teacher/include/footer'); ?>
