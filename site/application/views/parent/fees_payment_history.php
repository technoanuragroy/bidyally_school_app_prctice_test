<?php $this->load->view('parent/include/header'); ?>
   
    <!-- Sidebar  -->
    <?php $this->load->view('parent/include/side_bar'); ?>

        <!-- Page Content  -->
        <div id="content">
            
            <?php $this->load->view('parent/include/header_nav'); ?>
            <div class="bodycontent">
                
                <h2 class="page_head"><?php echo $page_title; ?></h2>
                <div class="rowBox">
                                                                                                                                                               
                    <?php 
                        if(!empty($history)) {                                                     
                    ?>                 
                            <div class="tableGrid">                                    
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>                                                                                               
<!--                                                <th scope="col" style="text-align:left">Semester Info</th>-->
                                                <th scope="col" style="text-align:left">Fees For</th>
                                                <th scope="col" style="text-align:left; width: 100px;">Date</th>
                                                <th scope="col" style="text-align:right">Amount</th>                                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                                        
                                                foreach ($history as $payment) {
                                            ?>     
                                                    <tr data-href="<?php echo base_url(); ?>parent/user/feesPaymentDetails/<?php echo $this->my_custom_functions->ablEncrypt($payment['id']); ?>">                                                        
<!--                                                        <td style="text-align:left">
                                                            <?php 
//                                                                if($payment['class_info'] != "") {
//                                                                    $class_info_array = json_decode($payment['class_info'], true);
//                                                                    if(is_array($class_info_array)) {                                                                        
//                                                                        if(array_key_exists('semester', $class_info_array)) {
//                                                                            echo 'Semester: '.$class_info_array['semester'].', ';
//                                                                        }                                                                        
//                                                                        if(array_key_exists('section', $class_info_array)) {
//                                                                            echo 'Section: '.$class_info_array['section'].', ';
//                                                                        }
//                                                                        if(array_key_exists('roll_no', $class_info_array)) {
//                                                                            echo 'Roll No: '.$class_info_array['roll_no'];
//                                                                        }
//                                                                    } else {
//                                                                        echo 'N/A';
//                                                                    }
//                                                                }
                                                            ?>                                                                                                                                                                                                           
                                                        </td>  -->
                                                        <td style="text-align:left">
                                                            <u><?php echo $payment['fees_name']; ?></u>
                                                        </td>
                                                        <td style="text-align:left">
                                                            <?php echo date("d-m-Y", strtotime($payment['payment_datetime'])); ?>
                                                        </td>
                                                        <td style="text-align:right">
                                                            <?php 
                                                                $paid_amount = $payment['paid_amount'];
                                                                $payment_handling_fees = $payment['service_charges'];
                                                                $display_amount = $paid_amount - $payment_handling_fees;
                                                                echo INDIAN_CURRENCY_CODE.' '.number_format($display_amount, 2); 
                                                            ?>                                                            
                                                        </td>                                                            
                                                    </tr>
                                            <?php                                                    
                                                }
                                            ?>
                                            <!-- Total amount -->        
<!--                                            <tr style="border-top: 3px solid #333">
                                                <td style="text-align:left">
                                                    <b>Total Amount</b> 
                                                </td>  
                                                <td style="text-align:right">
                                                    <span class="fees_amount">

                                                    </span>    
                                                </td>                                                    
                                            </tr>                                                                                                  -->
                                        </tbody>
                                    </table>
                                </div>                                
                            </div>
                    <?php                                                 
                        } else {
                            echo 'No data available';
                        } 
                    ?>                                                                                         
                </div>
                
    <script type="text/javascript">
        $(document).ready(function() {
            
        });        
    </script>               
                
<?php $this->load->view('parent/include/footer'); ?>
                 
