<?php $this->load->view('parent/include/header'); ?>


            <!-- Sidebar  -->
           <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <div class="headingDiv"><h2><?php echo $page_title; ?></h2></div>
                    <div class="rowBox">
                        
                        <?php if(!empty($child_list)){ 
                            foreach($child_list as $child){
                                $child_url = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL,'file_url','and student_id = "'.$child['id'].'"');
                                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT,'school_id','and id = "'.$child['id'].'"');
                                $school_name = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL,'name','and id = "'.$school_id.'"');
                            ?>
                        <div class="profileGrid">
                            <a href="todays_classes.php"></a>
                            <span class="profileimg">
                                <?php if($child_url){ ?>
                                <img src="<?php echo $child_url; ?>" alt="" class="">
                                <?php }else{ ?>
                                <img src="<?php echo base_url(); ?>_images/NoImageFound.png" alt="" class="">
                                <?php } ?>
                            </span>
                            <h3><?php echo $child['name']; ?></h3>
<!--                            <span class="tag_section">Art director</span>-->
                            <a href="<?php echo base_url(); ?>parent/user/switch_student/<?php echo $child['id']; ?>" class="btn_select">Select</a>
                            <div class="school_name"><?php echo $school_name; ?></div>
                        </div>
                        <?php }} ?>
                        

                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
