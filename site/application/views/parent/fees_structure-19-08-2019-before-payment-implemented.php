<?php $this->load->view('parent/include/header'); ?>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <div class="tableGrid">
                            <?php
                                        if (!empty($fees_data)) {//echo "<pre>";print_r($fees_data); ?>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Fees Name</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //if (!empty($fees_data)) {//echo "<pre>";print_r($fees_data);
                                            foreach ($fees_data['fees_strucure'] as $fees_list) {
                                                 ?>     

                                                    <tr>
                                                        <td><?php echo $fees_list['label']; ?></td>
                                                        <td><?php echo $fees_list['amount']; ?></td>
                                                        <td><?php if($fees_list['option'] == 1){ echo "Mandetory";}else{ echo "Optional";}; ?>
<!--                                                            <span class="attn_dir_arrow"><i class="fas fa-arrow-right"></i></span>-->
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            
                                        
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                            <?php }else{?>
                            No data available
                            <?php } ?>
                        </div>


                    </div>
                    <?php $this->load->view('parent/include/footer'); ?>
