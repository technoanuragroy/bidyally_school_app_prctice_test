<?php $this->load->view('parent/include/header'); ?>


<!-- Sidebar  -->
<?php $this->load->view('parent/include/side_bar'); ?>

<!-- Page Content  -->
<?php //echo "<pre>";print_r($student_details); ?>
<div id="content">
    <?php $this->load->view('parent/include/header_nav'); ?>
    <div class="bodycontent">
        <h2 class="page_head"><?php echo $page_title; ?></h2>
        <div class="rowBox">
            <div class="tableGrid">
                <?php $school_logo = ($school_details['file_url'] != "") ? $school_details['file_url'] : base_url() . '_images/notification_icon.png'; ?> 
                <div style="width:100%;text-align:center;">
                    <div style="width: 100%; float: left; display: block; overflow: auto;/*border:1px solid blue;*/">
                        <div style="width: 100%; float: left; display: block; padding-bottom: 15px; border-bottom: 4px solid #333;">
                            <div id="logo_wrap" style="text-align: center; width: 20%; float: left; display: inline-block;">
                                <span>
                                    <img src="<?php echo $school_logo; ?>" width="96" height="96">
                                </span>
                            </div>
                            <div id="head_contain" style="width: 80%; float: left; display: inline-block; text-align: left; padding-top: 0px;/*border:1px solid;*/">                                 
                                <span style="font-weight: bold;font-size: 28px;">
                                    <?php echo $school_details['name']; ?>
                                </span>  
                                <br>
                                <span style="font-weight: bold;">
                                    <?php echo $school_details['address']; ?>
                                </span>  
                                <br>
                                <span style="font-weight: bold;">
                                    <i class="fa fa-phone-square" aria-hidden="true"></i> Mobile: <?php echo $school_details['mobile_no']; ?> |
                                </span>
                                <span style="font-weight: bold;">
                                    <i class="fa fa-envelope-square" aria-hidden="true"></i> Email:  <?php echo $school_details['email_address']; ?>    
                                </span>                                                             
                            </div>
                            <div class="studenr_des_wrap">
                                <!-- -- student description -- --> 
                                <ul class="student_description_list">
                                    <?php
                                    $student_name = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $this->session->userdata("student_id") . '"');
                                    $class_name = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $student_details[0]['class_id'] . '"');
                                    $section_name = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $student_details[0]['section_id'] . '"');
                                    $roll_no = $student_details[0]['roll_no'];
                                    ?>
                                    <li>
                                        <b>name:</b>
                                        <b><?php echo $student_name; ?></b>
                                    </li>
                                    <li>
                                        <b>class:</b>
                                        <b><?php echo $class_name . ', ' . $section_name; ?></b>
                                    </li>
                                    <li>
                                        <b>roll:</b>
                                        <b><?php echo $roll_no; ?></b>
                                    </li>
                                    <!--									<li>
                                                                                                                    <b>exam:</b>
                                                                                                                    <b>studend exam</b>
                                                                                                            </li>-->
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div style="width: 100%; float: left; display: block; overflow: auto;">
                        <table border="1" style="width:100%;">
                            <thead>
                                <tr>
                                    <th rowspan="2">Subjects</th>
                                    <?php
                                    foreach ($result['terms'] as $term_id => $term_detail) {

                                        if ($term_detail['combined_exam_results'] == 1) {
                                            $colspan = count($term_detail['exams']) + 1;
                                        } else {
                                            $colspan = count($term_detail['exams']);
                                        }
                                        ?>
                                        <th colspan="<?php echo $colspan; ?>"><?php echo $term_detail['term_name']; ?></th>
                                    <?php } ?>
                                    <?php
                                    if ($result['semester_detail']['combined_term_results'] == 1) {
                                        ?>
                                        <th rowspan="2"><?php echo $result['semester_detail']['combined_result_name']; ?></th>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <?php
                                    foreach ($result['terms'] as $term_id => $term_detail) {
                                        foreach ($term_detail['exams'] as $exam_id => $exam_detail) {
                                            ?>
                                            <th><?php echo $exam_detail['exam_name']; ?></th>
                                            <?php
                                        }
                                        if ($term_detail['combined_exam_results'] == 1) {
                                            ?>
                                            <th><?php echo $term_detail['combined_result_name']; ?></th>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $exam_sum_totals = array();
                                    $term_sum_totals = array();
                                    $sum_total = 0;
                                    foreach ($result['subjects'] as $subject_id => $subject) { 
                                ?>
                                        <tr>
                                            <td><?php echo $subject; ?></td>
                                            <?php
                                                $semester_combined_score = 0;
                                                foreach ($result['terms'] as $term_id => $term_detail) {

                                                    $term_combined_score = 0;
                                                    foreach ($term_detail['exams'] as $exam_id => $exam_detail) {
                                                        if (array_key_exists($subject_id, $exam_detail['subjects'])) {

                                                            $score = $exam_detail['subjects'][$subject_id]['score'];
                                                            if (is_numeric($score)) {
                                                                $score = $score;
                                                            } else {
                                                                $score = 0;
                                                            }
                                                            $term_combined_score += $score;
                                                            $semester_combined_score += $score;
                                                            
                                                            if(array_key_exists($exam_id, $exam_sum_totals)) {
                                                                $exam_sum_totals[$exam_id] += $score;
                                                            } else {
                                                                $exam_sum_totals[$exam_id] = $score;
                                                            }
                                            ?>
                                                            <td><?php echo $score; ?></td>
                                            <?php                                             
                                                        } else { 
                                            ?>
                                                            <td>-</td>
                                            <?php
                                                        }
                                                    }
                                                    if ($term_detail['combined_exam_results'] == 1) {
                                                        
                                                        if(array_key_exists($term_id, $term_sum_totals)) {
                                                            $term_sum_totals[$term_id] += $term_combined_score;
                                                        } else {
                                                            $term_sum_totals[$term_id] = $term_combined_score;
                                                        }
                                            ?>
                                                        <td><?php echo $term_combined_score; ?></td>
                                            <?php
                                                    }
                                                }
                                                if ($result['semester_detail']['combined_term_results'] == 1) {
                                                    
                                                    $sum_total += $semester_combined_score;
                                            ?>
                                                    <td><?php echo $semester_combined_score; ?></td> 
                                            <?php                                             
                                                } 
                                            ?>
                                        </tr>
                                <?php                                 
                                    } 
                                ?>
                                <tr>
                                    <td style="font-weight: bold;">Sum Total</td>       
                                    <?php 
                                        foreach ($result['terms'] as $term_id => $term_detail) {
                                            foreach ($term_detail['exams'] as $exam_id => $exam_detail) {
                                    ?>
                                                <td style="font-weight: bold;">
                                                    <?php 
                                                        if(array_key_exists($exam_id, $exam_sum_totals)) {
                                                            echo $exam_sum_totals[$exam_id];
                                                        } else {
                                                            echo '-';
                                                        }
                                                    ?>
                                                </td>
                                    <?php        
                                            }

                                            if ($term_detail['combined_exam_results'] == 1) {
                                    ?>      
                                                <td style="font-weight: bold;">
                                                    <?php 
                                                        if(array_key_exists($term_id, $term_sum_totals)) {
                                                            echo $term_sum_totals[$term_id];
                                                        } else {
                                                            echo '-';
                                                        }
                                                    ?>
                                                </td>
                                    <?php            
                                            }
                                        }

                                        if ($result['semester_detail']['combined_term_results'] == 1) {
                                    ?>
                                            <td style="font-weight: bold;">
                                                <?php echo $sum_total; ?>
                                            </td>    
                                    <?php            
                                        }
                                    ?>    
                                </tr>        
                            </tbody>  
                        </table>
                    </div>
                </div>


            </div>
<?php $this->load->view('teacher/include/footer'); ?>
