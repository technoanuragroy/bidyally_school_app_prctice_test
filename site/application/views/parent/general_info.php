<?php $this->load->view('parent/include/header'); ?>


            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head heading_black"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <div class="defaultContent">
                            <?php
                            if (!empty($general_info)) { ?>
                                <div class="white_backgnd">
                                <p>
                                    <?php echo $general_info[0]['info_text']; ?>

                                </p>
                                </div>

                            <?php }else{ ?>
                                 <p style="text-align:center;">No information found.</p>
                           <?php  } ?>

                               
                        </div>
                    </div>
                    <?php $this->load->view('parent/include/footer'); ?>
