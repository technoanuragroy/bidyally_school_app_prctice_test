<?php $this->load->view('parent/include/header'); ?>
<script type="text/javascript">
    function callBrowser() {
        $('.upload_ico').click();
    }
    function callBrowserFile() {
        $('.upload_ico_file').click();
    }

    function disp_cls() {
        $('.img_add_icon > li').each(function () {
            $('.img_add_icon > li > span').text('');
            //$('.overlay_ccl').find('span').remove();
            $('.img_add_icon > li > span').removeClass('overlay_note_img1');
            $(this).fadeIn(1000);
        });
    }

    function disp_file() {
        $('.file_add_icon > li').each(function () {
            $('.file_add_icon > li > span').text('');
            $('.overlay_ccls').find('span').remove();
            $('.file_add_icon > li > span').removeClass('overlay_note_img2');
            $(this).fadeIn(1000);
        });

    }
    function deleteImgFile(fileID) {
        $('.message_block').html('<p>Are you sure that you want to delete this image?</p>');
        $('.btn-yes').attr('onclick', "confirm_delete('" + fileID + "')");
    }
    function confirm_delete(fileID) {
        window.location.href = "<?php echo base_url(); ?>teacher/user/deleteFile/" + fileID;
    }
</script>

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>
            <div id="content">
                <!-- <div class="innerHeadingContent">
                    <h1>Today's class</h1>
                </div> -->

                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="innerbodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="sectionrowBox">
                        <div class="innerbodycontent3">
                            <div class="routine_timetable blockwise">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">

                                    <li class="nav-item">
                                        <a class="nav-link active" id="class-tab" data-toggle="tab" href="#classwork" role="tab" aria-controls="classwork" aria-selected="true">Class Work</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link " id="home-tab" data-toggle="tab" href="#homework" role="tab" aria-controls="homework" aria-selected="true">Home Work</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link " id="assignment-tab" data-toggle="tab" href="#assignment" role="tab" aria-controls="assignment" aria-selected="true">Assignment</a>
                                    </li>



                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="classwork" role="tabpanel" aria-labelledby="classwork-tab">
                                        <h2>Topic</h2>
                                        <div class="form-group">
                                            <?php
                                            if (!empty($note_detail)) {
                                                echo $note_detail['notes']['topic_name'];
                                            }
                                            ?>

                                        </div>
                                        <h2>Notes</h2>
                                        <div class="form-group">
                                            <?php
                                            if (!empty($note_detail)) {
                                                echo $note_detail['notes']['classnote_text'];
                                            }
                                            ?>

                                        </div>
                                        <h2>Images</h2>
                                        <ul class="photoGrid img_add_icon">
            <!--                                <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                <input type="file" name="doc_img_upload[]" class="upload_ico" style="display:none;" multiple="">
                                            </li>-->
                                            <?php
                                            if (!empty($note_detail['note_file'])) {
                                                $image_count = 0;
                                                $style = '';
                                                $count = '';
                                                $cc = 0;
                                                $ddisp = '';
                                                $class = '';
                                                foreach ($note_detail['note_file'] as $index => $filess) {
                                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                    $path = $filess['file_url'];

                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                        $cc++;
                                                    }
                                                }
                                                $cou = $cc - 3;
                                                if ($cou == 0) {
                                                    $disp_count = '';
                                                    $manage_class = '';
                                                } else {
                                                    $disp_count = '+' . $cou;
                                                    $manage_class = '<span class="overlay_note_img1"></span>';
                                                }
                                                foreach ($note_detail['note_file'] as $index => $filess) {
                                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                    $path = $filess['file_url'];
                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                        $image_count++;
                                                        $display_path = $path;
                                                        $download_path = $path;
                                                        $width = '300px';
                                                        if ($image_count > 3) {
                                                            $style = "style='display:none;'";
                                                        }
                                                        if ($image_count == 3) {
                                                            $class = 'onclick="disp_cls()" class="overlay_ccl"';
                                                            $ddisp = $disp_count;
                                                        } else {
                                                            $class = '';
                                                        }
                                                        ?>
                                                        <li <?php echo $style; ?> <?php echo $class; ?>><?php echo $manage_class; ?>
                                                            <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">
                                                                <img src="<?php echo $display_path; ?>" alt="" style="width:<?php echo $width; ?>"/>
                                                            </a>
                                                            <span><?php echo $ddisp; ?></span>
                                                            <small>
                                                                <a class="downloadFile" href="<?php echo $download_path; ?>" download="">
                                                                    <i class="fas fa-download"></i></a>
            <!--                                                    <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess['id']; ?>);">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </a>-->
                                                            </small>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                        <?php if (!empty($note_detail['note_file'])) { ?>

                                            <div class="pdfFilescontainer">
                                                <h2>files</h2>
                                                <ul class="gridPdf file_add_icon">
                <!--                                    <li><a href="javascript:" onclick="callBrowserFile()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                    <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>
                                                        <input type="file" name="doc_upload[]" class="upload_ico_file" style="display:none;" multiple="">
                                                    </li>-->
                                                    <?php
                                                    $file_count = 0;
                                                    $flag = 0;


                                                    $file_image_count = 0;
                                                    $file_style = '';
                                                    $file_count = '';
                                                    $file_cc = 0;
                                                    $file_ddisp = '';
                                                    $file_class = '';
                                                    $display_paths = '';
                                                    $download_paths = '';
                                                    $ext = '';
                                                    $path = '';
                                                    $disp_count = 0;
                                                    $cou = 0;
                                                    //$file_count = 0;
                                                    foreach ($note_detail['note_file'] as $index => $filess) {
                                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                        $path = $filess['file_url'];
                                                        if ($ext == 'xlsx' || $ext == 'XLSX' || $ext == 'xls' || $ext == 'XLS' || $ext == 'docx' || $ext == 'DOCX' || $ext == 'doc' || $ext == 'DOC' || $ext == 'pdf' || $ext == 'PDF' || $ext == 'PPTX' || $ext == 'pptx' || $ext == 'PPT' || $ext == 'ppt') {
                                                            $file_cc++;
                                                        }
                                                    }
                                                    $cou = $file_cc - 3;
                                                    if ($cou == 0) {
                                                        $disp_count = '';
                                                        $manage_class = '';
                                                    } else {
                                                        $disp_count = '+' . $cou;
                                                        $manage_class = '<span class="overlay_note_img1"></span>';
                                                    }

                                                    foreach ($note_detail['note_file'] as $index => $filess_fl) {
                                                        $flag = 0;
                                                        $extt = trim(pathinfo($filess_fl['file_url'], PATHINFO_EXTENSION));
                                                        $file_count++;
                                                        $path = $filess_fl['file_url'];


                                                        if ($file_image_count > 2) {
                                                            $file_style = "style='display:none;'";
                                                        }
                                                        if ($file_image_count == 2) {
                                                            $file_class = 'onclick="disp_file()" class="overlay_ccls"';
                                                            $file_ddisp = $disp_count;
                                                        } else {
                                                            $file_class = '';
                                                        }
                                                        if ($extt == 'xlsx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/XLSX.png';
                                                            $download_paths = $path;
                                                            $width = '100px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>><?php echo $manage_class; ?>
            <!--                                                    <span class="overlay_note_img1"></span>-->
                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
                                                                </a>
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile"  href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i></a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'xls') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/XLS.png';
                                                            $download_paths = $path;
                                                            $width = '100px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>><?php echo $manage_class; ?>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i> </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'docx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/DOCX.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>><?php echo $manage_class; ?>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $download_paths; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->

                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i></a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'doc') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/DOC.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>><?php echo $manage_class; ?>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'pdf') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/PDF.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>><?php echo $manage_class; ?>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths;?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'pptx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/PPTX.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>><?php echo $manage_class; ?>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                        <?php } ?>
                                                        <?php
                                                    }
                                                    ?>

                                                </ul>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                    <div class="tab-pane fade show" id="homework" role="tabpanel" aria-labelledby="homework-tab">
                                        <h2>Topic</h2>

                                        <div class="form-group">
                                            <?php
                                            if (!empty($note_detail_home)) {
                                                echo $note_detail_home['notes']['topic_name'];
                                            }
                                            ?>

                                        </div>
                                        <h2>Notes</h2>
                                        <div class="form-group">
                                            <?php
                                            if (!empty($note_detail_home)) {
                                                echo $note_detail_home['notes']['classnote_text'];
                                            }
                                            ?>

                                        </div>
                                        <h2>Images</h2>
                                        <ul class="photoGrid img_add_icon">
            <!--                                <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                <input type="file" name="doc_img_upload[]" class="upload_ico" style="display:none;" multiple="">
                                            </li>-->
                                            <?php
                                            if (!empty($note_detail_home['note_file'])) {
                                                $image_count = 0;
                                                $style = '';
                                                $count = '';
                                                $cc = 0;
                                                $ddisp = '';
                                                $class = '';
                                                foreach ($note_detail_home['note_file'] as $index => $filess) {
                                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                    $path = $filess['file_url'];

                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                        $cc++;
                                                    }
                                                }
                                                $cou = $cc - 3;
                                                if ($cou == 0) {
                                                    $disp_count = '';
                                                    $manage_class = '';
                                                } else {
                                                    $disp_count = '+' . $cou;
                                                    $manage_class = '<span class="overlay_note_img1"></span>';
                                                }
                                                foreach ($note_detail_home['note_file'] as $index => $filess) {
                                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                    $path = $filess['file_url'];
                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                        $image_count++;
                                                        $display_path = $path;
                                                        $download_path = $path;
                                                        $width = '300px';
                                                        if ($image_count > 3) {
                                                            $style = "style='display:none;'";
                                                        }
                                                        if ($image_count == 3) {
                                                            $class = 'onclick="disp_cls()" class="overlay_ccl"';
                                                            $ddisp = $disp_count;
                                                        } else {
                                                            $class = '';
                                                        }
                                                        ?>
                                                        <li <?php echo $style; ?> <?php echo $class; ?>><?php echo $manage_class; ?>
                                                            <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">
                                                                <img src="<?php echo $display_path; ?>" alt="" style="width:<?php echo $width; ?>"/>
                                                            </a>
                                                            <span><?php echo $ddisp; ?></span>
                                                            <small>
                                                                <a class="downloadFile" href="<?php echo $download_path; ?>" download="">
                                                                    <i class="fas fa-download"></i></a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                            </small>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                        <?php if (!empty($note_detail_home['note_file'])) { ?>
                                        <div class="pdfFilescontainer">
                                            <h2>files</h2>
                                            <ul class="gridPdf file_add_icon">
            <!--                                    <li><a href="javascript:" onclick="callBrowserFile()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>
                                                    <input type="file" name="doc_upload[]" class="upload_ico_file" style="display:none;" multiple="">
                                                </li>-->
                                                <?php
                                                $file_count = 0;
                                                $flag = 0;
                                                
                                                    $file_image_count = 0;
                                                    $file_style = '';
                                                    $file_count = '';
                                                    $file_cc = 0;
                                                    $file_ddisp = '';
                                                    $file_class = '';
                                                    $display_paths = '';
                                                    $download_paths = '';
                                                    $ext = '';
                                                    $path = '';
                                                    $disp_count = 0;
                                                    $cou = 0;
                                                    //$file_count = 0;
                                                    foreach ($note_detail_home['note_file'] as $index => $filess) {
                                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                        $path = $filess['file_url'];
                                                        if ($ext == 'xlsx' || $ext == 'XLSX' || $ext == 'xls' || $ext == 'XLS' || $ext == 'docx' || $ext == 'DOCX' || $ext == 'doc' || $ext == 'DOC' || $ext == 'pdf' || $ext == 'PDF') {
                                                            $file_cc++;
                                                        }
                                                    }
                                                    $cou = $file_cc - 3;
                                                    $disp_count = '+' . $cou;
                                                    foreach ($note_detail_home['note_file'] as $index => $filess_fl) {
                                                        $flag = 0;
                                                        $extt = trim(pathinfo($filess_fl['file_url'], PATHINFO_EXTENSION));
                                                        $file_count++;
                                                        $path = $filess_fl['file_url'];


                                                        if ($file_image_count > 2) {
                                                            $file_style = "style='display:none;'";
                                                        }
                                                        if ($file_image_count == 2) {
                                                            $file_class = 'onclick="disp_file()" class="overlay_ccl"';
                                                            $file_ddisp = $disp_count;
                                                        } else {
                                                            $file_class = '';
                                                        }
                                                        if ($extt == 'xlsx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/XLSX.png';
                                                            $download_paths = $path;
                                                            $width = '100px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile"  href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i></a>
            <!--                                                            <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'xls') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/XLS.png';
                                                            $download_paths = $path;
                                                            $width = '100px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i> </a>
            <!--                                                            <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'docx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/DOCX.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i></a>
            <!--                                                            <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'doc') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/DOC.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'pdf') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/PDF.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                        <?php } ?>
                                                        <?php
                                                    }
                                                
                                                ?>

                                            </ul>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="tab-pane fade show" id="assignment" role="tabpanel" aria-labelledby="assignment-tab">
                                        <h2>Topic</h2>

                                        <div class="form-group">
                                            <?php
                                            if (!empty($note_detail_assignment)) {
                                                echo $note_detail_assignment['notes']['topic_name'];
                                            }
                                            ?>
                                        </div>
                                        <h2>Notes</h2>
                                        <div class="form-group">
                                            <?php
                                            if (!empty($note_detail_assignment)) {
                                                echo $note_detail_assignment['notes']['classnote_text'];
                                            }
                                            ?>
                                        </div>
                                        <h2>Images</h2>
                                        <ul class="photoGrid img_add_icon">
            <!--                                <li><a href="javascript:" onclick="callBrowser()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                <input type="file" name="doc_img_upload[]" class="upload_ico" style="display:none;" multiple="">
                                            </li>-->
                                            <?php
                                            if (!empty($note_detail_assignment['note_file'])) {
                                                $image_count = 0;
                                                $style = '';
                                                $count = '';
                                                $cc = 0;
                                                $ddisp = '';
                                                $class = '';
                                                foreach ($note_detail_assignment['note_file'] as $index => $filess) {
                                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                    $path = $filess['file_url'];

                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                        $cc++;
                                                    }
                                                }
                                                //echo $cc;
                                                $cou = $cc - 3;
                                                if ($cou == 0) {
                                                    $disp_count = '';
                                                    $manage_class = '';
                                                } else {
                                                    $disp_count = '+' . $cou;
                                                    $manage_class = '<span class="overlay_note_img1"></span>';
                                                }
                                                foreach ($note_detail_assignment['note_file'] as $index => $filess) {
                                                    $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                    $path = $filess['file_url'];
                                                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'png' || $ext == 'PNG') {
                                                        $image_count++;
                                                        $display_path = $path;
                                                        $download_path = $path;
                                                        $width = '300px';
                                                        if ($image_count > 3) {
                                                            $style = "style='display:none;'";
                                                        }
                                                        if ($image_count == 3) {
                                                            $class = 'onclick="disp_cls()" class="overlay_ccl"';
                                                            $ddisp = $disp_count;
                                                        } else {
                                                            $class = '';
                                                        }
                                                        ?>
                                                        <li <?php echo $style; ?> <?php echo $class; ?>><?php echo $manage_class; ?>
                                                            <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">
                                                                <img src="<?php echo $display_path; ?>" alt="" style="width:<?php echo $width; ?>"/>
                                                            </a>
                                                            <span><?php echo $ddisp; ?></span>
                                                            <small>
                                                                <a class="downloadFile" href="<?php echo $download_path; ?>" download="">
                                                                    <i class="fas fa-download"></i></a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                            </small>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                        <?php if (!empty($note_detail_assignment['note_file'])) {  ?>
                                        <div class="pdfFilescontainer">
                                            <h2>files</h2>
                                            <ul class="gridPdf file_add_icon">
            <!--                                    <li><a href="javascript:" onclick="callBrowserFile()"><img src="<?php echo base_url(); ?>app_images/plus.png" alt="" /></a>
                                                <img src="<?php echo base_url(); ?>app_images/NoImageFound.png" alt=""/>
                                                    <input type="file" name="doc_upload[]" class="upload_ico_file" style="display:none;" multiple="">
                                                </li>-->
                                                <?php
                                                $file_count = 0;
                                                $flag = 0;
                                               
                                                    $file_image_count = 0;
                                                    $file_style = '';
                                                    $file_count = '';
                                                    $file_cc = 0;
                                                    $file_ddisp = '';
                                                    $file_class = '';
                                                    $display_paths = '';
                                                    $download_paths = '';
                                                    $ext = '';
                                                    $path = '';
                                                    $disp_count = 0;
                                                    $cou = 0;
                                                    //$file_count = 0;
                                                    foreach ($note_detail_assignment['note_file'] as $index => $filess) {
                                                        $ext = pathinfo($filess['file_url'], PATHINFO_EXTENSION);
                                                        $path = $filess['file_url'];
                                                        if ($ext == 'xlsx' || $ext == 'XLSX' || $ext == 'xls' || $ext == 'XLS' || $ext == 'docx' || $ext == 'DOCX' || $ext == 'doc' || $ext == 'DOC' || $ext == 'pdf' || $ext == 'PDF') {
                                                            $file_cc++;
                                                        }
                                                    }
                                                    $cou = $file_cc - 3;
                                                    $disp_count = '+' . $cou;
                                                    foreach ($note_detail_assignment['note_file'] as $index => $filess_fl) {
                                                        $flag = 0;
                                                        $extt = trim(pathinfo($filess_fl['file_url'], PATHINFO_EXTENSION));
                                                        $file_count++;
                                                        $path = $filess_fl['file_url'];


                                                        if ($file_image_count > 2) {
                                                            $file_style = "style='display:none;'";
                                                        }
                                                        if ($file_image_count == 2) {
                                                            $file_class = 'onclick="disp_file()" class="overlay_ccl"';
                                                            $file_ddisp = $disp_count;
                                                        } else {
                                                            $file_class = '';
                                                        }
                                                        if ($extt == 'xlsx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/XLSX.png';
                                                            $download_paths = $path;
                                                            $width = '100px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->

                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile"  href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i></a>
            <!--                                                            <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'xls') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/XLS.png';
                                                            $download_paths = $path;
                                                            $width = '100px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i> </a>
            <!--                                                            <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'docx') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/DOCX.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i></a>
            <!--                                                            <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                            <i class="fas fa-trash-alt"></i>
                                                                        </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'doc') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/DOC.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                            <?php
                                                        } if ($extt == 'pdf') {
                                                            $file_image_count++;

                                                            $display_paths = base_url() . 'app_images/PDF.png';
                                                            $download_paths = $path;
                                                            $width = '48px';
                                                            ?>
                                                            <li <?php echo $file_style; ?> <?php echo $file_class; ?>>
                                                                <span class="overlay_note_img2"></span>
<!--                                                                <a href="<?php echo $display_path; ?>" class="fancybox" data-fancybox="gallery">-->
                                                                    <img src="<?php echo $display_paths; ?>" alt="" class="pdfimg"/>
<!--                                                                </a>-->
                                                                <span><?php echo $file_ddisp; ?></span>
                                                                <small>
                                                                    <a class="downloadFile" href="<?php echo $download_paths; ?>" download="hello">
                                                                        <i class="fas fa-download"></i>
                                                                    </a>
            <!--                                                        <a class="deleteFile" href="javascript:" data-toggle="modal" data-target="#exampleModal" onclick="deleteImgFile(<?php echo $filess_fl['id']; ?>);">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>-->
                                                                </small>
                                                            </li>
                                                        <?php } ?>
                                                        <?php
                                                    }
                                                
                                                ?>

                                            </ul>
                                        </div>
                                        <?php }
                                                ?>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <?php $this->load->view('parent/include/footer'); ?>
