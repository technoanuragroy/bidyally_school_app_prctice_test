<?php $this->load->view('parent/include/header'); ?>

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    
                    <div class="rowBox">

                        <div class="AccountformContent AccountformContent_package register_container">
                            <div class="container-form payment_failure" style="margin-bottom: 20px;">
                                <?php
                                    if($this->uri->segment(4) == "success") {
                                        echo 'Thank you for your payment and support for making ' . SITE_NAME . ' a revolutionary product.';
                                    } else if($this->uri->segment(4) == "failure") {
                                        echo 'Sorry but we are unable to process your payment at this time, you try again later or you can get some help at <a href="mailto:' . SITE_EMAIL . '">' . SITE_EMAIL . '</a>';
                                    }
                                ?>
                            </div>

                            <div class="container-form">
                                <div class="topButtonset">
                                    <span class="buttonMargin buttonMarginPaymentSuccess">
                                        <?php if($this->uri->segment(4) == "success") { ?>
                                                <a href="<?php echo base_url(); ?>parent/user/dashBoard" class="btn002">Continue to Dashboard</a>
                                        <?php } else if($this->uri->segment(4) == "failure") { ?>
                                                <a href="<?php echo base_url(); ?>parent/user/dashBoard" class="btn002">Continue to Dashboard</a>&nbsp;&nbsp;
                                                <a href="<?php echo base_url(); ?>parent/user/feeStructure" class="btn002">Retry Again</a>
                                        <?php } ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
<?php $this->load->view('parent/include/footer'); ?>                
