<body>
     <?php if (ENVIRONMENT == 'production') { ?>
    <!-- Google Tag Manager (noscript) -->
    
        <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWL9Z9Q" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>

        <!-- End Google Tag Manager (noscript) -->
        <?php } ?>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">
<nav id="sidebar">
    <div class="scrollContainer">
        <div id="dismiss">
            <!-- <i class="fas fa-arrow-left"></i> -->
            <i class="fas fa-times"></i>
        </div>
        <div class="sidebar-header">
            <span class="profileimg">
                <?php
                //$school_name = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $this->session->userdata('school_id') . '"');
                $address = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT_DETAIL, 'address', 'and student_id = "' . $this->session->userdata('student_id') . '"');

                $school_url = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'file_url', 'and id = "' . $this->session->userdata('school_id') . '"');
                if ($school_url != '') {
                    ?>
                    <img src="<?php echo $school_url; ?>" alt="" class="">
                <?php } else { ?>
                    <img src="<?php echo base_url(); ?>_images/School-img.png" alt="" class="">
                <?php } ?>
            </span>
            <h3>
                <?php echo $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'father_name', 'and id = "' . $this->session->userdata('student_id') . '"'); ?>
            </h3>
            <span class="tag_location"><?php echo $address; ?></span>
        </div>
        <?php if ($this->session->userdata('student_flag') && $this->session->userdata('student_flag') == 1) { ?>
        <?php 
                    // Session dropdown
                    $school_sessions = $this->session->userdata("session_list_by_student");
                ?>    
                <form action="<?php echo base_url(); ?>parent/user/change_session" method="POST">                    
                    <select name="main_session_dropdown" class="main_session_dropdown form-control" style="height: 32px;">
                        <option disabled="true">Select Session</option>
                        <?php
                            if(!empty($school_sessions)) {
                                foreach($school_sessions as $school_ses) {
                                    
                        ?>   
                                    <option value="<?php echo $school_ses['id']; ?>" <?php if($school_ses['id'] == $this->session->userdata('session_id')) { echo 'selected="selected"'; } ?>><?php echo $school_ses['session_name']; ?></option>
                        <?php
                                }
                            }
                        ?>
                    </select>  
                </form>
        <?php } ?>
        <ul class="list-unstyled components">
            <li>
                <a href="<?php echo base_url(); ?>parent/user/dashBoard"><i class="fas fa-home"></i>Dashboard</a>
            </li>
            <?php if ($this->session->userdata('student_flag') && $this->session->userdata('student_flag') == 1) { ?>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/timeTable"><i class="fas fa-calendar-week"></i>Time Table</a>
                </li>

                <li>
                    <a href="<?php echo base_url(); ?>parent/user/dailyWorks"><i class="fas fa-book-reader"></i>Daily Work</a>
                </li>

                <li>
                    <a href="<?php echo base_url(); ?>parent/user/studentDiary" class="ads"><i class="fas fa-book-open"></i>Diary</a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>parent/user/attendance" class="ads"><i class="fas fa-user-check"></i>Attendance</a>
                </li>


                <li class="">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-external-link-alt"></i>Information</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="<?php echo base_url(); ?>parent/user/schoolCalander"><i class="far fa-calendar-alt"></i>Institute Calendar</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>parent/user/notice"><i class="fas fa-bell"></i>Notice</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>parent/user/syllabus"><i class="fas fa-book-reader"></i>Syllabus</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>parent/user/generalInformation"><i class="fas fa-info-circle"></i>Institute Info</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>parent/user/feeStructure"><i class="fas fa-money-check"></i>Fees Structure</a>
                        </li>
                        
                        <!-- Show payment history if school has payment option enabled -->
                        <?php                             
                            $payment_enabled = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL_PAYMENT_SETTINGS, 'payment_enabled', 'and school_id = "' . $this->session->userdata('school_id') . '"'); 
                            if($payment_enabled == 1) {
                        ?>                            
                                <li>
                                    <a href="<?php echo base_url(); ?>parent/user/feesPaymentHistory"><i class="fas fa-money-check"></i>Fees Payment History</a>
                                </li>
                        <?php 
                            }
                        ?>
                    </ul>
                </li>
                <li>
                  <a href="<?php echo base_url(); ?>parent/user/viewResult" class="ads"><i class="fas fa-unlock-alt"></i>Result</a>
                </li>
              <li>
                  <a href="<?php echo base_url(); ?>parent/user/parentFeedback" class="ads"><i class="fas fa-unlock-alt"></i>Parent Feedback</a>
                </li>

                <li>
                    <a href="<?php echo base_url(); ?>parent/user/changePassword"><i class="fas fa-unlock-alt"></i>Change Password</a>
                </li>
                
            <?php } ?>
            <li>
                <a href="<?php echo base_url(); ?>parent/user/initial_logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
            </li>
        </ul>

    </div>
</nav>
            
            <?php
        $check_adv_flag = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'display_ads', 'and id = "' . $this->session->userdata('school_id') . '"');
        if ($check_adv_flag == 0) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('.ads').click(function (e) {
                        
                        e.preventDefault();
                        //alert($(this).attr('href'));
                        //var value = $(this).data('encrypted');
                        var value = $(this).attr('href');

                        if ($(this).hasClass('ads')) {
                            
                            var new_url = '<?php echo base_url(); ?>'+'parent/user/customAds/?V='+value;//current_url.split('/customAds/?V')[0]+'/customAds/?V='+value;
                            window.location.href = new_url;
            
                            var loc = 'customAds/?V=' + value;
                            window.location = loc;
                        }


                    });
                });

            </script>
<?php } ?>           
            

