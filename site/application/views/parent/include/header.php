<!doctype html>
<html lang="en">
    <head>
        <?php if(ENVIRONMENT == 'production'){ ?>
        <!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-KWL9Z9Q');</script>

        <?php } ?>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bidyaaly|Menu</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/sidebarnav.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>_css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/jquery.fancybox.css">
        <!--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous"> -->


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>app_js/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
<!--        <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>-->

        <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--    <script src="<?php echo base_url(); ?>app_js/jquery-3.3.1.slim.min.js"></script>-->
        <script src="<?php echo base_url(); ?>app_js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/jquery.fancybox.js"></script>
         <script src="<?php echo base_url(); ?>_js/jquery-ui.js"></script>


        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#dismiss, .overlay').on('click', function () {
                    $('#sidebar').removeClass('active');
                    $('.overlay').removeClass('active');
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').addClass('active');
                    $('.overlay').addClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });

                $('*[data-href]').on('click', function () {
                    window.location = $(this).data("href");
                });
                $(".fancybox").fancybox({
                    //touch: false
                });



            });
        </script>




    </head>
