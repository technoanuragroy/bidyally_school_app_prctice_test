<?php $this->load->view('parent/include/header'); ?>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<body>
    <div class="wrapper">
        <div class="wrap_content heightFullwraper">

            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    
                    <div class="rowBox">
                        <?php 
                            $onetimeRazorPay = $this->session->userdata("onetimeRazorPay");
                        ?>
                        
                        <script>
                            var options = {
                                "key": "<?php echo $razorpay_credentials['RAZORPAY_KEY_ID']; ?>",  
                                "amount": "<?php echo $onetimeRazorPay["amount"]; ?>",
                                "name": "<?php echo SITE_NAME; ?>",
                                "description": "<?php echo $onetimeRazorPay["description"]; ?>",
                                "image": "<?php echo base_url(); ?>_images/main_logo.png",
                                "display_currency": "<?php echo $onetimeRazorPay["display_currency"]; ?>", 
                                "display_amount": "<?php echo $onetimeRazorPay["display_amount"]; ?>", 
                                "callback_url": "<?php echo base_url(); ?>parent/user/rzp_OnetimePaidCallback",
                                "redirect": true,
                                "handler": function (response) {                                                                                                                        
                                    //console.log(response);                                                                                                                                                                                                                                 
                                },
                                "prefill": {
                                    "name": "<?php echo $onetimeRazorPay["name"]; ?>",
                                    "email": "<?php echo $onetimeRazorPay["email"]; ?>",
                                    "contact": "<?php echo $onetimeRazorPay["phone"]; ?>"
                                },
                                "notes": {
                                    "payment_id": "<?php echo $onetimeRazorPay["payment_id"]; ?>",                                                                                                    
                                    "parent_name": "<?php echo $onetimeRazorPay["name"]; ?>"                                                                
                                },
                                "theme": {
                                    "color": "#3FA9FF"
                                },
                                "modal": {
                                    "ondismiss": function(){
                                        window.location.href = '<?php echo base_url(); ?>parent/user/paymentCompleted/failure';
                                    }
                                }
                            };
                            var rzp1 = new Razorpay(options);
                            rzp1.open();                                                  
                        </script> 
                        
                    </div>
<?php $this->load->view('parent/include/footer'); ?>
