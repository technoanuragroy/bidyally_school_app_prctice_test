<?php $this->load->view('parent/include/header'); ?>



            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                        <ul class="legends">
                            <li><span class="greenLegend"></span>Present </li>
                            <li><span class="redLegend"></span>Absent </li>
                            <li><span class="orangeLegend"></span>Partial Present </li>
                        </ul>
                        <div class="profileGrid wrapperCalender">
                            <div id="calendar_box">
                                <?php echo $this->calendar->generate($year, $month, $event); ?>
                            </div>
                        </div>


                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $("div.highlight").eq(0).removeClass("highlight").addClass("date");
                            $("div.highlight").eq(1).removeClass("highlight").addClass("content_box");
                        });                        
                    </script>