<?php $this->load->view('parent/include/header'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#date_cal').change(function () {
            $('#pastClasses').submit();
        });
    });
</script>


            <!-- Sidebar  -->
            <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent">

                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="routine_container">
                        <?php echo date('d/m/Y',strtotime($display_class_date)); ?>
                    </div>
                    <div class="routine_calender">

                        <form method="post" action="<?php echo base_url(); ?>parent/user/dailyWorks" id="pastClasses">
                            <div class="past_work">
                            <span>Past work</span>
                            <label class="openCalender">

                                <input required type="date" style="opacity: 1;" class="form-control" id="date_cal" name="class_date" >
                                <a href="javascript:" id="date_cal_val" ><i class="far fa-calendar-alt"></i></i></a>
                            </label>
                            </div>
                        </form>
                    </div>
                    <div class="rowBox">
                        <div class="">
                            <ul class="worksDaliy">
                                <?php
                                if (!empty($class_list_daywise)) {
                                    if (!empty($class_list_daywise['structured'])) {
                                        foreach ($class_list_daywise['structured'] as $key => $subject_list) {//echo "<pre>";print_r($subject_list);
                                            if ($key % 2) {
                                                ?>
                                                <li>
                                                    <div class="row_works">
                                                        <span class="leftworks">
                                                            <?php
                                                            $extra_class = '';
                                                            $subject_count = count($subject_list);
                                                            if ($subject_count == 1) {
                                                                $extra_class = 'heightClass';
                                                            }
                                                            foreach ($subject_list as $class_id => $subject) {//echo "<pre>";print_r($subject); 
                                                                ?>
                                                            <a href="<?php echo base_url(); ?>parent/user/getClassNoteDetail/<?php echo $subject['subject_id']; ?>" class="ads">
                                                                    <span class="subject_<?php echo $class_id; ?> <?php echo $extra_class; ?>">
                                                                        <h3><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $subject['subject_id'] . '"') ?></h3>
                                                                    </span></a>
                                                            <?php } ?>
                                                        </span>
                                                        <?php if ($key == 1) { ?>
                                                            <span class="rightworks" style="background-image: url(<?php echo base_url(); ?>app_images/books_img.jpg)">
                                                            <?php } else if ($key == 3) { ?>
                                                                <span class="rightworks" style="background-image: url(<?php echo base_url(); ?>app_images/chemical-laboratory.jpg)">
                                                                <?php } ?>
                                                            </span>
                                                    </div>
                                                </li>
                                            <?php } else { ?>
                                                <li>
                                                    <div class="row_works">
                                                        <span class="rightworks" style="background-image: url(<?php echo base_url(); ?>app_images/background-school.jpg)">
                                                        </span>
                                                        <span class="leftworks">
                                                            <?php foreach ($subject_list as $class_id => $subject) { ?>
                                                                <a href="<?php echo base_url(); ?>parent/user/getClassNoteDetail/<?php echo $subject['subject_id']; ?>"><span class="subject_<?php echo $class_id; ?>"><h3><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $subject['subject_id'] . '"') ?></h3></span></a>
                                                            <?php } ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <?php
                                    if (!empty($class_list_daywise['unstructured'])) {
                                        foreach ($class_list_daywise['unstructured'] as $key => $subject_list) {//echo "<pre>";print_r($subject_list); 
                                            ?>
                                            <li>
                                                <div class="row_works">

                                                    </span>
                                                    <span class="rightworks full_width">
                                                        <?php foreach ($subject_list as $class_id => $subject) { ?>
                                                            <a href="<?php echo base_url(); ?>parent/user/getClassNoteDetail/<?php echo $subject['subject_id']; ?>"><span class="subject_<?php echo $class_id; ?>"><h3><?php echo $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $subject['subject_id'] . '"') ?></h3></span></a>
                                                                    <?php } ?>
                                                    </span>
                                                </div>
                                            </li>

                                            <?php
                                        }
                                    }
                                }
                                ?>




                            </ul>
                        </div>

                    
                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
