<?php $this->load->view('parent/include/header'); ?>


            <!-- Sidebar  -->
           <?php $this->load->view('parent/include/side_bar'); ?>

            <!-- Page Content  -->
            <div id="content">
                <?php $this->load->view('parent/include/header_nav'); ?>
                <div class="bodycontent calender_wrapper">
                    <h2 class="page_head"><?php echo $page_title; ?></h2>
                    <div class="rowBox">
                      <div class="formContent">
                          
                      <form>
                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="inputFirstname">First name</label>
                            <input type="text" class="form-control" id="inputFirstname" placeholder="First name">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputLastname">Last name</label>
                            <input type="text" class="form-control" id="inputLastname" placeholder="Last name">
                          </div>
                        </div>
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Email</label>
                          <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Password</label>
                          <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputAddress">Address</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                      </div>

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputCity">City</label>
                          <input type="text" class="form-control" id="inputCity">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="inputState">State</label>
                          <select id="inputState" class="form-control">
                            <option selected>Choose...</option>
                            <option>...</option>
                          </select>
                        </div>
                        <div class="form-group col-md-2">
                          <label for="inputZip">Zip</label>
                          <input type="text" class="form-control" id="inputZip">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="customControlValidation1" required>
                          <label class="custom-control-label" for="customControlValidation1">Check this custom checkbox</label>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="custom-control custom-radio">
                          <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                          <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                          <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
                        </div>
                    </div>

                    <div class="form-group">
                      <input type="submit" class="buttonSubmit" value="Sign in"/>
                      </div>
                    </form>
                    </div>

                    </div>
                    <?php $this->load->view('teacher/include/footer'); ?>
