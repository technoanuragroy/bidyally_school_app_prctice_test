<?php $this->load->view('parent/include/header'); ?>


<!-- Sidebar  -->
<?php $this->load->view('parent/include/side_bar'); ?>

<!-- Page Content  -->
<div id="content">
    <?php $this->load->view('parent/include/header_nav'); ?>
    <div class="bodycontent">
        <div class="rowBox">
            <h2 class="page_head"><?php echo $page_title; ?></h2>
            <div class="noticedetails">
                <?php
                if (!empty($school_notice_detail)) {
                    ?>
                    <h2><?php echo $school_notice_detail['notice_data']['notice_heading']; ?></h2>
                    <div class="noticeinnercontent">
                        <span class="date__notice"><?php echo @date('M d Y', strtotime($school_notice_detail['notice_data']['issue_date'])); ?></span>
                        <!--                                         <div style="border:1px solid">-->
                        <?php
                        //echo "<pre>";print_r($school_notice_detail);
                        if (!empty($school_notice_detail['notice_file_data'])) {
                            foreach ($school_notice_detail['notice_file_data'] as $notice_file) {
                                ?>
                                <div class="downloadFiles">
                                    <span class="files_icon"><a href="<?php echo base_url(); ?>parent/user/downloadFile/<?php echo $notice_file['id']; ?>/<?php echo DOWNLOAD_KEY; ?>"><i class="fa fa-download" aria-hidden="true"></i></a></span>
                                    <span></span>
                                </div>
                            <?php }
                        }
                        ?>
                        <!--                                         </div>-->
                        <p class="phCaps"><?php echo trim(strip_tags($school_notice_detail['notice_data']['notice_text'])); ?></p>
                        <?php
                    }
                    ?>
                        <div>

                        <?php
                        //echo "<pre>";print_r($school_notice_detail);

                        if (!empty($school_notice_detail['notice_file_data'])) {
                            foreach ($school_notice_detail['notice_file_data'] as $notice_file) {
                                $notice_id = $notice_file['note_id'];
                            }
                            ?>


<!--                            <div class="downloadFiles">

                                <a href="<?php //echo base_url(); ?>parent/user/downloadAllNotice/<?php //echo $notice_id; ?>" class="download_button" onclick="download_all();"><span class="btndownload">download all</span></a>
                            </div>-->
                        <?php }
                        ?>
                    </div>
                </div>
               
            </div>
        </div>
        <?php $this->load->view('teacher/include/footer'); ?>
