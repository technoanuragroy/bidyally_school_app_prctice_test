<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SchoolApp|Register</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/style_002.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/mdb.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>app_css/all.css">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>app_js/jquery-3.4.0.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.core.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/codebase.app.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>app_js/mdb.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>_js/be_forms_validation.min.js"></script>
    </head>
    
    <body>
        <div class="process_signup_wraper">
            <div class="process_signup">
                <div class="process_signup_contain">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 process_signup_block_wrp institutes">
                            <a href="<?php echo base_url(); ?>school/signUp"></a>
                        </div>
                        <div class="col-xs-12 col-md-6 process_signup_block_wrp parents">
                            <a href="<?php echo base_url(); ?>parentSignUp/<?php echo WEB_KEY; ?>"></a>
                        </div>                                                
                    </div>
		</div>
            </div>
        </div>               
    </body>
</html>
