<?php

$config['company_forgot_password'] = array(
    'subject' => 'Bidyaaly password reset',
    'addressing_user' => 'Hi There,',
    'mail_body' => '<div style="text-align: left;">
                        Someone requested a new password for your ' . SITE_NAME . ' account.<br><a bgcolor="#C10D4A" align="center" style="color:#ffffff;background-color:#C10D4A;display:inline-block;font-size:0.8rem;font-family:Lato,Helvetica,Arial,sans-serif;text-align:center;text-decoration:none;padding:7px 32px;border-radius:3px;text-transform:uppercase;margin-top:20px" href="%reset_link%" target="_blank">Reset Password</a>
                        <br><br>If you didn`t make this request then you can simply ignore this email.<br>
                    </div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

$config['school_sign_up'] = array(
    'subject' => 'Welcome to Bidyaaly. Your first step towards a better learning environment',
    'addressing_user' => 'Dear %ContactPerson%,',
    'mail_body' => '<div style="text-align: left;">
                        We really appreciate you joining us at Bidyaaly. We hope you will love it 
                        as you see how it can help to manage all activities of %Organization_Name% 
                        with this Application.
                        <br><br>We came up with Bidyaaly to build an efficient communication system 
                        between educational organizations and teachers with parents to build a better 
                        learning environment.
                        <br><br>Over the coming days, we will be sending you emails to assist you use the App better.
                        <br><br>Your login username is %username%.
                        <br><br><b>Verify your email</b>
                        <br>You need to verify your email account in order to unlock all the features of Bidyaaly. 
                        Check your inbox for the verification email and click the verification link to activate your account to use all the features.
                        <br>
                        <a href="%verify_link%" style="color: #ffffff;background-color: #E91E63;display: inline-block;font-size: 0.8rem;font-family: Helvetica,Arial,sans-serif;text-align: center;text-decoration: none;padding: 7px 32px;border-radius: 3px;text-transform: uppercase;margin-top: 20px;">
                            Verify
                        </a>
                        <br><br>
                        If the button does not work, please copy and paste the link into your browser:
                        <br>
                        <a href="%verify_link%">%verify_link%</a>
                        <br><br>Let me know if you have any questions.
                    </div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'
    
);

$config['notice'] = array(
    'subject' => 'Notice: %ActualNoticeSubject%',
    'addressing_user' => 'Hi There,',
    'mail_body' => '<div style="text-align: left;">A new notice has been issued from %SchoolName%.
     <br>%NOTICEBODY%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['fees_payment_success_mail_to_parent'] = array(
    'subject' => 'Successful Fees Payment',
    'addressing_user' => 'Hi There,',
    'mail_body' => '<div style="text-align: left;">Fees payment of %amount% for %student_name% is successful on %payment_date%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'       
);

////////////////////////////////////////////////////////////////////////////////


$config['send_diary_from_teacher_to_parent'] = array(
    'subject' => 'Diary: %ActualDiaryHeading%',
    'addressing_user' => 'Dear Parent,',
    'mail_body' => '<div style="text-align: left;">Dairy Note issued for %studentname% of %Class%, %Section%, %SchoolName%
     <br>Note: %ActualDiary%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'        
);

////////////////////////////////////////////////////////////////////////////////

$config['send_diary_from_parent_to_teacher'] = array(
    'subject' => 'Diary: %ActualDiaryHeading%',
    'addressing_user' => 'Dear Teacher,',
    'mail_body' => '<div style="text-align: left;">Diary Note issued by Parent of %studentname% of %Class%, %Section%, %SchoolName%
    <br>Note: %ActualDiary%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['send_daily_work_to_all_parent_by_class_section'] = array(
    'subject' => 'Daily %dailywork% submitted for %class_name%, %subject%',
    'addressing_user' => 'Dear Parent,',
    'mail_body' => '<div style="text-align: left;">%TITLE% -> '
    . ' %NOTE%</div>',    
    'unsubscribe' => '%unsubscribe%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['attendance_absent'] = array(
    'subject' => 'Attendance: Absent notification',
    'addressing_user' => 'Dear Parent,',
    'mail_body' => '<div style="text-align: left;">Your child %student_name% is ABSENT on %DATE% for %class_name%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['attendance_present'] = array(
    'subject' => 'Attendance: Present notification',
    'addressing_user' => 'Dear Parent,',
    'mail_body' => '<div style="text-align: left;">Your child %student_name% is PRESENT on %DATE% for %class_name%, %subject%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['temporary_class'] = array(
    'subject' => 'Temporary Class',
    'addressing_user' => 'Hi There,',
    'mail_body' => '<div style="text-align: left;">A temporary class for %Class%, %Section%, %Subject% has been assigned to you for %date%</div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'       
);

////////////////////////////////////////////////////////////////////////////////

$config['institute_email_verification'] = array(
    'subject' => SITE_NAME . ' verify account',
    'addressing_user' => 'You\'re nearly there!',
    'mail_body' => '<div style="text-align: left;">
                        <b>Why verify?</b>
                        <br><br>Everyone using '.SITE_NAME.' require a verified email for using to prevent spam. Verifying lets you join '.SITE_NAME.' quickly and easily.
                        <br><br>We just need to verify your email address to complete your '.SITE_NAME.' signup.
                        <br>
                        <a href="%verify_link%" style="color: #ffffff;background-color: #C10D4A;display: inline-block;font-size: 0.8rem;font-family: Helvetica,Arial,sans-serif;text-align: center;text-decoration: none;padding: 7px 32px;border-radius: 3px;text-transform: uppercase;margin-top: 20px;">
                            Verify
                        </a>
                        <br><br>
                        If the button does not work, please copy and paste the link into your browser:
                        <br>
                        <a href="%verify_link%">%verify_link%</a>
                    </div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['improper_school_account'] = array(
    'subject' => '%ContactPerson%, use Bidyaaly and see the magic happen',
    'addressing_user' => 'Dear %ContactPerson%,',
    'mail_body' => '<div style="text-align: left;">
                        Thanks for showing your interest in our App, Bidyaaly. This is just to inform you that you haven’t 
                        set up your %organization_name% account properly. We may assist you to do the same.
                        <br><br><b>You need to Setup Informations</b>
                        <br>Add Classes, Section, Sessions, etc on your bidyaaly panel then setup timetable, holiday list so 
                        that all the class related data properly links and can be used by others.
                        <br><br><b>Add Staff’s</b>
                        <br>Add Teaching Staff with unique username (maybe mobile number) and password which should be further 
                        given to the teacher those will be using the App. Add all other necessary details of the Teaching Staff.
                        <br><br><b>Add Students & Parents</b>
                        <br>Add Student with all the required information along with the Parents details. The mobile number of 
                        the Parent can be same for more than one child. Students of parents having the same mobile number will 
                        be treated as the children of the same parents across the App.
                        <br><br><b>Share the App Links</b>
                        <br>Once all this is done you need to share the app links with parents and teachers so that they can 
                        log in and start using Bidyaaly.
                        <br><br><a href="https://play.google.com/store/apps/details?id=com.ablion.bidyaaly&hl=en"><img src="https://s3.ap-south-1.amazonaws.com/bidyaaly.com/global_assets/main_logo.png"></a>
                        <br><br>We know that you will love the benefits of using Bidyaaly once you start using it.
                        
                    </div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'   
);

////////////////////////////////////////////////////////////////////////////////

$config['account_about_to_expire'] = array(
    'subject' => 'Your Bidyaaly account expiring',
    'addressing_user' => 'Dear %ContactPerson%',
    'mail_body' => '<div style="text-align: left;">
                        We hope you have enjoyed the Trial Pack of Bidyaaly for %company_name%. 
                        Your trial account is going to expire on %date%. To upgrade your account to a paid account, 
                        please contact our customer care.
                        <br>
                        Hope to enter into a long term business relationship soon.
                        
                    </div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['proper_school_account'] = array(
    'subject' => "%contact_person%, Don't miss the chance to help %institute_name%!",
    'addressing_user' => 'Dear %contact_person%',
    'mail_body' => '<div style="text-align: left;">
                        Thanks for showing your interest in Bidyaaly for %institute_name%. 
                        We are happy that you have chosen to use Bidyaaly for a better learning environment. 
                        You can contact our customer support any moment for any assistance.
                        <br>
                        Enjoy using Bidyaaly.
                        
                    </div>',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

////////////////////////////////////////////////////////////////////////////////

$config['registration_detail'] = array(
    'subject' => EMAIL_SERVER.'Registration Detail for %institute_name%',
    'addressing_user' => 'Dear Admin,',
    'mail_body' => '<div style="text-align: left;"> %school_list%
    </div>',    
    'institute_name' => '%institute_name%',
    'systemtext' => '%systemtext%'    
);

$config['reg_format'] = array(
    'mail_body' => '<div style="text-align: left;">
    <br>Institute Name : %institute_name%
    <br>Contact Person Name : %contact_person_name%
    <br>Mobile Number : %mobile_number%
    <br>Email Address : %email_address%
    <br>IP Address : %ip_address%
    <br>Address : %address%
    </div>',  
);

////////////////////////////////////////////////////////////////////////////////

$config['fees_due_reminder'] = array(
    'subject' => 'Fees Due Reminder',
    'addressing_user' => 'Hi There,',
    'mail_body' => 'Name : %student_name%, Class : %class_info%, Due Amount : %due_amount%, Due Date : %due_date%',    
    'unsubscribe' => '%unsubscribe%',
    'institute_name' => '%institute_name%',
    'systemtext' => 'This is a system generated email, please do not reply to this email.'    
);
