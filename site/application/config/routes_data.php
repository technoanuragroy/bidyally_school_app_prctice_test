<?php

$route["admin"] = "admin/main";
$route["admin/logout"] = "admin/main/logout";
$route["admin/main/(:any).*"] = "admin/main/$1";


$route["school"] = "school/main";
$route["school/signUp"] = "school/main/signUp";
$route["school/login"] = "school/main/login";
$route["school/forgetPassword"] = "school/main/forgetPassword";
$route["school/userResetPassword/(:any).*"] = "school/main/userResetPassword/$1";

$route["app/".WEB_KEY] = "app/index/";
$route["teacher"] = "teacher/main";
$route["signUp/".WEB_KEY] = "app/signUp";
$route["parentSignUp/".WEB_KEY] = "app/parentSignUp";
$route["enterOTP"] = "app/enterOTP";
$route["enterDetail"] = "app/enterDetail";
$route["forgetPassword/".WEB_KEY] = "app/forgetPassword";
$route["userResetPassword/(:any).*"] = "app/userResetPassword/$1";
$route["email_Verification/(:any).*"] = "school/main/email_verification";


 ?>
