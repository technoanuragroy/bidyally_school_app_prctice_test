<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_main_model extends CI_Model {

    public function __construct() {
            parent::__construct();
            

    }
    
    
    function login_check() {

        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $sql = "SELECT * FROM ".TBL_ADMIN." WHERE username LIKE '".$username."' AND status=1";
        $query = $this->db->query($sql);
                
        $password = $this->input->post('password');                        
        $record = array();
        if ($query->num_rows() > 0) {

            $record = $query->row_array();

            if(password_verify($password, $record['password'])) {
                $session_data = array(
                    "admin_id" => $record["admin_id"],
                    "admin_username" => $record["username"],
                    "admin_email" => $record["email"], 
                    "admin_is_logged_in" => 1
                );
                $this->session->set_userdata($session_data);

                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
