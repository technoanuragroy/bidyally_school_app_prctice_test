<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }
    
    function update_admin($admin_id) {
        
       
                $update_data = array(
                                    "name" => $this->input->post("name"),
                                    "phone" => $this->input->post("phone"),
                                    "status" => $this->input->post("status"),
                                    "email" => $this->input->post("email"),
                                    );
       
        
        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }
    

    function get_admin_data() {

        $sql = "Select * from ".TBL_ADMIN."";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_section_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageSections/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_subject_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageSubjects/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_teacher_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/manageTeachers/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    function get_time_table_data($class_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        
        $day_list = $this->config->item('days_list');
        //$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
        
        foreach ($day_list as $day => $val) {
            //foreach ($period_list as $ress) {
                //$create_data[$day][$ress['id']] = '';
            $counter = 0;
                foreach ($result as $routine) {
                    if ($day == $routine['day_id']) {
                        $create_data[$day][$counter] = $routine;
                    }

                $counter++;}
            //}
        }
        //echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_period_data($sql) {

        $sql = base64_decode($sql);
        $offset = 4; //the url segment that store the offset value
        //pagination
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/managePeriods/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function create_school($admin_id) {


        $update_data = array(
            "name" => $this->input->post("name"),
            "phone" => $this->input->post("phone"),
            "status" => $this->input->post("status"),
            "email" => $this->input->post("email"),
        );


        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_school_data() {
        
        $this->db->order_by('id', 'DESC');        
        $query = $this->db->get(TBL_SCHOOL);
                        
        return $query->result_array();
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function get_sms_credit_report() {
        
        $where = " WHERE 1=1";
        
        // Setting the date conditions
        if($this->session->userdata("smscrrp_from_date") != "") {
            $fromDate = $this->session->userdata("smscrrp_from_date").' 00:00:00';            
        } else {
            $fromDate = date("Y-m-d", strtotime(" -7 days")).' 00:00:00';            
        }
        
        if($this->session->userdata("smscrrp_to_date") != "") {
            $toDate = $this->session->userdata("smscrrp_to_date").' 23:59:59';
        } else {
            $toDate = date("Y-m-d H:i:s");
        }         
        
        $where .= " AND ".TBL_SMS_CREDITS.".transaction_time BETWEEN '".$fromDate."' AND '".$toDate."'";
        
        // Setting the school condition
        if($this->session->userdata("smscrrp_school_id") != "") { 
            
            $where .= " AND ".TBL_SMS_CREDITS.".school_id=".$this->session->userdata("smscrrp_school_id");
        }        
        
        $sql = "SELECT ".TBL_SMS_CREDITS.".*, ".TBL_SCHOOL.".name AS school_name "
                . "FROM ".TBL_SMS_CREDITS." INNER JOIN ".TBL_SCHOOL." "
                . "ON ".TBL_SMS_CREDITS.".school_id=".TBL_SCHOOL.".id "
                . $where
                . " ORDER BY transaction_time DESC";
        
        // pagination
        $offset = 4; //the url segment that store the offset value
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/smsCreditReport/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function update_school_payment_setting($school_id) {
        
        $sql = "SELECT * FROM ".TBL_SCHOOL_PAYMENT_SETTINGS." WHERE school_id='".$school_id."'";
        $query = $this->db->query($sql);
                
        $online_payment_enabled = 0;
        $payment_gateway_environment = 0;
        $add_on_school_fees = 0;
        $deduct_from_school_fees = 0;
        $payment_gateway_reference_id = '';
        
        if($this->input->post('payment_enabled')) {
            if($this->input->post('payment_enabled') == 1) {
                
                if($this->input->post('online_payment_enabled')) {
                    if($this->input->post('online_payment_enabled') == 1) {
                        
                        $online_payment_enabled = 1;
                        $payment_gateway_environment = $this->input->post('payment_gateway_environment');
                        $add_on_school_fees = $this->input->post('add_on_school_fees');
                        $deduct_from_school_fees = $this->input->post('deduct_from_school_fees');
                        
                        // For live gateway, take the input value. For test gateway, take the test account id from config.
                        if($payment_gateway_environment == 1) {
                            $payment_gateway_reference_id = $this->input->post('payment_gateway_reference_id');
                        } else {
                            $payment_gateway_reference_id = RAZORPAY_ACCOUNT_ID_TEST;
                        }
                    }
                }
            }
        }
        
        // Settings data
        $setting_data = array(            
            'payment_enabled' => $this->input->post('payment_enabled'),
            'online_payment_enabled' => $online_payment_enabled,
            'payment_gateway_environment' => $payment_gateway_environment,
            'add_on_school_fees' => $add_on_school_fees,
            'deduct_from_school_fees' => $deduct_from_school_fees,
            'payment_gateway_reference_id' => $payment_gateway_reference_id
        );
        
        if($this->input->post('payment_enabled') == 0) { 
            // If payment settings already exists and admin wants to disable it
            if($query->num_rows() > 0) {
                $this->db->where('school_id', $school_id);
                $this->db->update(TBL_SCHOOL_PAYMENT_SETTINGS, $setting_data);
            }
        } else if($this->input->post('payment_enabled') == 1) {  
            // If payment settings already exists and admin wants to enable it
            if($query->num_rows() > 0) {
                $this->db->where('school_id', $school_id);
                $this->db->update(TBL_SCHOOL_PAYMENT_SETTINGS, $setting_data);
            } 
            // If no payment settings exists and admin wants to add it
            else {
                $setting_data['school_id'] = $school_id;
                $this->db->insert(TBL_SCHOOL_PAYMENT_SETTINGS, $setting_data);
            }
        }  
        
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_system_email_templates() { 
        
        $sql = "SELECT * FROM tbl_system_emails";
        $query = $this->db->query($sql);
                 
        $result = array();
        if ($query->num_rows() > 0) { 
            foreach($query->result_array() as $row) {
                $result[] = $row;
            }
        }        
        
        return $result;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    
    function add_email_template() {
        
        $data = array(
            "name" => $this->input->post("name"),
            "variable_name" => $this->input->post("variable_name"),  
            "from_email_variable" => $this->input->post("from_email_variable"),  
            "logic" => $this->input->post("logic"),
            "cron_details" => $this->input->post("cron_details")            
        );        

        $this->db->insert("tbl_system_emails", $data);
        $template_id = $this->db->insert_id();

        return $template_id;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    
    function update_email_template() {
        
        $update_data = array(
            "name" => $this->input->post("name"),
            "variable_name" => $this->input->post("variable_name"),  
            "from_email_variable" => $this->input->post("from_email_variable"),  
            "logic" => $this->input->post("logic"),
            "cron_details" => $this->input->post("cron_details")            
        ); 
        
        $this->db->where('id', $this->input->post("template_id"));
        $return = $this->db->update('tbl_system_emails', $update_data);

        return $return;
    }
}
