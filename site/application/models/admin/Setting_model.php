<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }
    
    function get_sms_settings_data() {
        
        $return_data = array();
        $key_val_array = array();
        
        $sql_a = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'sms_url'";
        $query_a = $this->db->query($sql_a);
        $result_a = $query_a->result_array();
        $sms_url = $result_a[0]['gateway_value'];
        
        $sql_b = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'sms_variables'";
        $query_b = $this->db->query($sql_b);
        $result_b = $query_b->result_array();
        $sms_key_vals = $result_b[0]['gateway_value'];
        
        $sms_keyval_break_up = json_decode($sms_key_vals,true);
        $i=0;
        foreach($sms_keyval_break_up as $key=>$v){
            $key_val_array[$i][$key] =$v;
            
            $i++;
        }
        
        $sql_c = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'system_sms'";
        $query_c = $this->db->query($sql_c);
        $result_c = $query_c->result_array();
        $system_sms = $result_c[0]['gateway_value'];

        $return_data = array(
            'sms_url' => $sms_url,
            'key_values' => $key_val_array,
            'system_sms' => $system_sms
        );
        
        return $return_data;
                
    }
    
    
    function update_sms_settings($final_key_value){
        $sms_url = $this->input->post('sms_url');
        $system_sms = $this->input->post('system_sms');
        
        $sql_a = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$sms_url."' where gateway_key = 'sms_url'";
        $query_a = $this->db->query($sql_a);
        
        $sql_b = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$final_key_value."' where gateway_key = 'sms_variables'";
        $query_b = $this->db->query($sql_b);
        
        $sql_c = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$system_sms."' where gateway_key = 'system_sms'";
        $query_c = $this->db->query($sql_c);
        
        return true;
        
    }
    
    function get_email_settings_data() {
        
        $return_data = array();
        
        $sql_a = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'email_smtp_host'";
        $query_a = $this->db->query($sql_a);
        $result_a = $query_a->result_array();
        $smtp_host = $result_a[0]['gateway_value'];
        
        $sql_b = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'smtp_port'";
        $query_b = $this->db->query($sql_b);
        $result_b = $query_b->result_array();
        $smtp_port = $result_b[0]['gateway_value'];
        
        $sql_c = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'smtp_user'";
        $query_c = $this->db->query($sql_c);
        $result_c = $query_c->result_array();
        $smtp_username = $result_c[0]['gateway_value'];
        
        $sql_d = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'smtp_password'";
        $query_d = $this->db->query($sql_d);
        $result_d = $query_d->result_array();
        $smtp_password = $result_d[0]['gateway_value'];
        
        $sql_e = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'system_email'";
        $query_e = $this->db->query($sql_e);
        $result_e = $query_e->result_array();
        $system_email = $result_e[0]['gateway_value'];
        
        

        $return_data = array(
            'smtp_host' => $smtp_host,
            'smtp_port' => $smtp_port,
            'smtp_username' => $smtp_username,
            'smtp_password' => $smtp_password,
            'system_email' => $system_email
        );
        
        return $return_data;
                
    }
    
    function update_email_settings(){
        $smtp_host = $this->input->post('smtp_host');
        $smtp_port = $this->input->post('smtp_port');
        $smtp_username = $this->input->post('smtp_username');
        $smtp_password = $this->input->post('smtp_password');
        $system_email = $this->input->post('system_email');
        
        $sql_a = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$smtp_host."' where gateway_key = 'email_smtp_host'";
        $query_a = $this->db->query($sql_a);
        
        $sql_b = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$smtp_port."' where gateway_key = 'smtp_port'";
        $query_b = $this->db->query($sql_b);
        
        $sql_c = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$smtp_username."' where gateway_key = 'smtp_user'";
        $query_c = $this->db->query($sql_c);
        
        $sql_d = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$smtp_password."' where gateway_key = 'smtp_password'";
        $query_d = $this->db->query($sql_d);
        
        $sql_e = "UPDATE ".TBL_GATEWAY_SETTINGS." set gateway_value ='".$system_email."' where gateway_key = 'system_email'";
        $query_e = $this->db->query($sql_e);
        
        return true;
        
        
    }
    
    function get_smtp_settings() {
        $return_data = array();
        
        $sql_a = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'email_smtp_host'";
        $query_a = $this->db->query($sql_a);
        $result_a = $query_a->result_array();
        $smtp_host = $result_a[0]['gateway_value'];
        
        $sql_b = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'smtp_port'";
        $query_b = $this->db->query($sql_b);
        $result_b = $query_b->result_array();
        $smtp_port = $result_b[0]['gateway_value'];
        
        $sql_c = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'smtp_user'";
        $query_c = $this->db->query($sql_c);
        $result_c = $query_c->result_array();
        $smtp_username = $result_c[0]['gateway_value'];
        
        $sql_d = "SELECT gateway_value FROM ".TBL_GATEWAY_SETTINGS." where gateway_key = 'smtp_password'";
        $query_d = $this->db->query($sql_d);
        $result_d = $query_d->result_array();
        $smtp_password = $result_d[0]['gateway_value'];
        

        $return_data = array(
            'smtp_host' => $smtp_host,
            'smtp_port' => $smtp_port,
            'smtp_username' => $smtp_username,
            'smtp_password' => $smtp_password,
        );
        
        return $return_data;
    }
}
