<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_school_data() {

        $sql = "SELECT * FROM ".TBL_SCHOOL;
        $query = $this->db->query($sql);
        
        return $query->result_array();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_sms_credit_report() {
        
        $where = " WHERE 1=1";
        
        // Setting the date conditions
        if($this->session->userdata("smscrrp_from_date") != "") {
            $fromDate = $this->session->userdata("smscrrp_from_date").' 00:00:00';            
        } else {
            $fromDate = date("Y-m-d", strtotime(" -7 days")).' 00:00:00';            
        }
        
        if($this->session->userdata("smscrrp_to_date") != "") {
            $toDate = $this->session->userdata("smscrrp_to_date").' 23:59:59';
        } else {
            $toDate = date("Y-m-d H:i:s");
        }         
        
        $where .= " AND ".TBL_SMS_CREDITS.".transaction_time BETWEEN '".$fromDate."' AND '".$toDate."'";
        
        // Setting the school condition
        if($this->session->userdata("smscrrp_school_id") != "") { 
            
            $where .= " AND ".TBL_SMS_CREDITS.".school_id=".$this->session->userdata("smscrrp_school_id");
        }        
        
        $sql = "SELECT ".TBL_SMS_CREDITS.".*, ".TBL_SCHOOL.".name AS school_name "
                . "FROM ".TBL_SMS_CREDITS." INNER JOIN ".TBL_SCHOOL." "
                . "ON ".TBL_SMS_CREDITS.".school_id=".TBL_SCHOOL.".id "
                . $where
                . " ORDER BY transaction_time DESC";
        
        // Pagination
        $offset = 4; // The url segment that store the offset value
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/smsCreditReport/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_sms_log_report() {
        
        $where = " WHERE 1=1";
        
        // Setting the date conditions
        if($this->session->userdata("smslog_from_date") != "") {
            $fromDate = strtotime($this->session->userdata("smslog_from_date").' 00:00:00');            
        } else {
            $fromDate = strtotime(date("Y-m-d", strtotime(" -7 days")).' 00:00:00');            
        }
        
        if($this->session->userdata("smslog_to_date") != "") {
            $toDate = strtotime($this->session->userdata("smslog_to_date").' 23:59:59');
        } else {
            $toDate = time();
        }         
        
        $where .= " AND ".TBL_SMS_LOG.".request_time BETWEEN '".$fromDate."' AND '".$toDate."'";
        
        // Setting the school condition
        if($this->session->userdata("smslog_school_id") != "") { 
            
            $where .= " AND ".TBL_SMS_LOG.".school_id=".$this->session->userdata("smslog_school_id");
        }        
        
        $sql = "SELECT ".TBL_SMS_LOG.".*, ".TBL_SCHOOL.".name AS school_name "
                . "FROM ".TBL_SMS_LOG." INNER JOIN ".TBL_SCHOOL." "
                . "ON ".TBL_SMS_LOG.".school_id=".TBL_SCHOOL.".id "
                . $where
                . " ORDER BY request_time DESC";
        
        // Pagination
        $offset = 4; // The url segment that store the offset value
        $this->load->library('pagination');
        $config['base_url'] = site_url() . $this->segment . '/smsLog/';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }
        $query = $this->db->query($sql . " LIMIT " . $offset . ", " . $config['per_page'] . " ");
        return $query->result_array();
    }
}
