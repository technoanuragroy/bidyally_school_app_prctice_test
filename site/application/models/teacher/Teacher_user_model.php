<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Teacher_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_todays_classes($teacher_id) {
        $data_set = array();
        $data_set_1 = array();
        $data_set_b = array();
        $sem_id_list = '';
        $query_search = '';
        if ($this->input->post('class_date')) {
            $today = $this->input->post('class_date');
        } else {
            $today = date('Y-m-d');
        }
        if ($this->uri->segment(4) && $this->uri->segment(4) != '') {
            $today = $this->uri->segment(4);
        }
        $class_date_sess = array('query_date' => $today);
        $this->session->set_userdata($class_date_sess);
        $daynum = date('N', strtotime($today));



        $semester_id_list = $this->my_custom_functions->get_all_semesters_of_running_session();

        if (!empty($semester_id_list)) {
            foreach ($semester_id_list as $semester_list) {
                $sem_id_list .= $semester_list['id'] . ',';
            }
            $sem_id = rtrim($sem_id_list, ',');
            $query_search = ' and semester_id IN(' . $sem_id . ')';


            $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE teacher_id = "' . $teacher_id . '" and day_id = "' . $daynum . '" ' . $query_search . ' ORDER BY period_start_time ASC';
            $query = $this->db->query($sql);
            $data_set = $query->result_array();

            $sql1 = 'SELECT * FROM ' . TBL_TEMP_TIMETABLE . ' WHERE teacher_id = "' . $teacher_id . '" and day_id = "' . $daynum . '" and class_date = "' . $today . '" ' . $query_search . ' ORDER BY period_start_time ASC';
            $query1 = $this->db->query($sql1);
            if ($query1->num_rows() > 0) {
                $data_set_1 = $query1->result_array();
            }
            $data_set_a = array_merge($data_set, $data_set_1);

            foreach ($data_set_a as $rows) {
                $data_set_b[] = $rows;
            }
            @ksort($data_set_b);
        }
        return $data_set_b;
    }

    function get_todays_classes_search($teacher_id, $day) {

        $daynum = $day;
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE teacher_id = "' . $teacher_id . '" and day_id = "' . $daynum . '" ORDER BY period_start_time ASC';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_time_table_data($class_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();

        $day_list = $this->config->item('days_list');
        //$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

        foreach ($day_list as $day => $val) {
            //foreach ($period_list as $ress) {
            //$create_data[$day][$ress['id']] = '';
            $counter = 0;
            foreach ($result as $routine) {
                if ($day == $routine['day_id']) {
                    $create_data[$day][$counter] = $routine;
                }

                $counter++;
            }
            //}
        }
        //echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_note_detail() {
        $data = array();

        $custom_date = $this->session->userdata('query_date');
        $custom_start_date_time = $custom_date . ' 00:00:00';
        $custom_end_date_time = $custom_date . ' 23:59:59';
        $start_time_range = strtotime($custom_start_date_time);
        $end_time_range = strtotime($custom_end_date_time);
        $sql1 = 'SELECT * FROM ' . TBL_NOTE . ' where type = 1 and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "' . $start_time_range . '" and note_issuetime <= "' . $end_time_range . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        //echo "<pre>";print_r($result1);
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" and type = 1 ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['notes'] = $row;
            $data['note_file'] = $result2;
        }

        return $data;
    }

    function get_home_note_detail() {
        $data = array();
        $custom_date = $this->session->userdata('query_date');
        $custom_start_date_time = $custom_date . ' 00:00:00';
        $custom_end_date_time = $custom_date . ' 23:59:59';
        $start_time_range = strtotime($custom_start_date_time);
        $end_time_range = strtotime($custom_end_date_time);
        $sql1 = 'SELECT * FROM ' . TBL_NOTE . ' where type = 2 and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "' . $start_time_range . '" and note_issuetime <= "' . $end_time_range . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" and type = 2 ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['notes'] = $row;
            $data['note_file'] = $result2;
        }

        return $data;
    }

    function get_assignment_note_detail() {
        $data = array();
        $custom_date = $this->session->userdata('query_date');
        $custom_start_date_time = $custom_date . ' 00:00:00';
        $custom_end_date_time = $custom_date . ' 23:59:59';
        $start_time_range = strtotime($custom_start_date_time);
        $end_time_range = strtotime($custom_end_date_time);
        $sql1 = 'SELECT * FROM ' . TBL_NOTE . ' where type = 3 and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $this->uri->segment(4) . '" and section_id = "' . $this->uri->segment(5) . '" and period_id = "' . $this->uri->segment(6) . '" and note_issuetime >= "' . $start_time_range . '" and note_issuetime <= "' . $end_time_range . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" and type = 3';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['notes'] = $row;
            $data['note_file'] = $result2;
        }

        return $data;
    }

    function student_list_search() {
        $result = array();
        $serch_string = '';
        $today = date('Y-m-d');
        $semester_id_list = '';

        if ($this->input->post('semester_id') != '') {

            $serch_string .= ' and t_s_e.semester_id  = "' . $this->input->post('semester_id') . '"';
        } else {

            if ($this->input->post('class_id') != '') {
                $semester_id = $this->my_custom_functions->get_current_semester_id($today, $this->input->post('class_id'));
                //echo "<pre>";print_r($semester_id);die;
                if (!empty($semester_id)) {
                    foreach ($semester_id as $sem_id) {
                        $semester_id_list .= $sem_id . ',';
                    }
                    $semester_ids = rtrim($semester_id_list, ',');
                    $serch_string .= ' and t_s_e.class_id = "' . $this->input->post('class_id') . '" and t_s_e.semester_id IN (' . $semester_ids . ')';
                }
            }
        }


        if ($this->input->post('section_id') != '') {
            $serch_string .= ' and t_s_e.section_id = "' . $this->input->post('section_id') . '"';
        }
        if ($this->input->post('roll_no') != '') {
            $serch_string .= ' and t_s_e.roll_no LIKE "%' . $this->input->post('roll_no') . '%"';
        }
        if ($this->input->post('student_name') != '') {
            $serch_string .= ' and t_s.name like "%' . $this->input->post('student_name') . '%"';
        }


        $sql = "SELECT t_s_e.id as enrollment_id,t_s_e.class_id,t_s_e.semester_id,t_s_e.section_id,t_s_e.roll_no,t_s.id,t_s.name from " . TBL_STUDENT_ENROLLMENT . " t_s_e INNER JOIN " . TBL_STUDENT . " t_s ON t_s_e.student_id = t_s.id "
                . " WHERE t_s_e.school_id = '" . $this->session->userdata('school_id') . "' and t_s.is_deleted = 0" . $serch_string;

        //$sql = "SELECT * FROM " . TBL_STUDENT . " WHERE 1=1 and school_id = " . $this->session->userdata('school_id') . " and is_deleted = 0 " . $serch_string;
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>";print_r($result);die;
        return $result;
    }

    function get_teacher_weekly_schedule() {
        $school_id = $this->session->userdata('school_id');
        $teacher_id = $this->session->userdata('teacher_id');
        $weeklist = $this->config->item('days_list');
        $semester_ids = '';
        $today = date('Y-m-d');
        $data = array();
        $sql_ses = "SELECT * FROM " . TBL_SESSION . " where school_id = '" . $school_id . "' and from_date <= '" . $today . "' and to_date >= '" . $today . "'";
        $query_ses = $this->db->query($sql_ses);
        $res_ses = $query_ses->result_array();
        //echo "<pre>";print_r($res_ses);die;
        foreach ($res_ses as $r_s) {
            $sql_sem = "SELECT * FROM " . TBL_SEMESTER . " where school_id = '" . $school_id . "' and session_id = '" . $r_s['id'] . "'";
            $query_sem = $this->db->query($sql_sem);
            $res_sem = $query_sem->result_array();
            foreach ($res_sem as $r_sem) {
                $semester_ids .= $r_sem['id'] . ',';
            }
        }
        $semester_id = rtrim($semester_ids, ',');
        
            foreach ($weeklist as $day => $val) {
                $data[$day] = array();
                $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "' and teacher_id = '" . $teacher_id . "' and day_id = '" . $day . "' and semester_id IN(" . $semester_id . ") order by day_id ASC, period_start_time ASC ";

                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    $result = $query->result_array();
                    $count = 0;

                    foreach ($result as $row) {
                        $data[$day][] = $row;
                        $count++;
                    }
                }

                $sql1 = "SELECT * FROM " . TBL_TEMP_TIMETABLE . " WHERE school_id = '" . $school_id . "' and teacher_id = '" . $teacher_id . "' and day_id = '" . $day . "' and class_date = '" . $today . "' order by day_id ASC, period_start_time ASC ";
                $query1 = $this->db->query($sql1);
                if ($query1->num_rows() > 0) {
                    $result1 = $query1->result_array();
                    $count = 0;

                    foreach ($result1 as $row) {
                        $data[$day][] = $row;
                        $count++;
                    }
                }
                foreach ($data as $day => $roww) {
                    ksort($data[$day]);
                }
            }
        
        return $data;
    }

    function update_note_publish_data() {
        $today_date = $this->session->userdata('query_date');
        $today_date_time_start = $today_date . ' 00:00:00';
        $day_start = strtotime($today_date_time_start);
        $today_date_time_end = $today_date . ' 23:59:59';
        $day_end = strtotime($today_date_time_end);

        $sql = "UPDATE " . TBL_NOTE . " set publish_data = 1 WHERE type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and semester_id = '" . $this->input->post('semester_id') . "' and section_id = '" . $this->input->post('section_id') . "' and period_id = '" . $this->input->post('period_id') . "' and note_issuetime >='" . $day_start . "' and  note_issuetime <='" . $day_end . "'";
        $query = $this->db->query($sql);
        return 1;
    }

    function update_note_publish_data_home() {
        $today_date = $this->session->userdata('query_date');
        $today_date_time_start = $today_date . ' 00:00:00';
        $day_start = strtotime($today_date_time_start);
        $today_date_time_end = $today_date . ' 23:59:59';
        $day_end = strtotime($today_date_time_end);

        $sql = "UPDATE " . TBL_NOTE . " set publish_data = 1 WHERE type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and semester_id = '" . $this->input->post('semester_id') . "' and section_id = '" . $this->input->post('section_id') . "' and period_id = '" . $this->input->post('period_id') . "' and note_issuetime >='" . $day_start . "' and  note_issuetime <='" . $day_end . "'";
        $query = $this->db->query($sql);
        return 1;
    }

    function update_note_publish_data_assignment() {
        $today_date = $this->session->userdata('query_date');
        $today_date_time_start = $today_date . ' 00:00:00';
        $day_start = strtotime($today_date_time_start);
        $today_date_time_end = $today_date . ' 23:59:59';
        $day_end = strtotime($today_date_time_end);

        $sql = "UPDATE " . TBL_NOTE . " set publish_data = 1 WHERE type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and semester_id = '" . $this->input->post('semester_id') . "' and section_id = '" . $this->input->post('section_id') . "' and period_id = '" . $this->input->post('period_id') . "' and note_issuetime >='" . $day_start . "' and  note_issuetime <='" . $day_end . "'";
        $query = $this->db->query($sql);
        return 1;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_noice_for_teacher() {

        $sql = 'SELECT * FROM ' . TBL_NOTICE . ' WHERE school_id=' . $this->session->userdata('school_id') . ' AND type=1 AND publish_date<=' . strtotime(date('Y-m-d'));

        // Pagination
        $offset = 4; // The url segment that store the offset value
        $this->load->library('pagination');
        $config['base_url'] = site_url() . 'teacher/user/getNotice';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }

        $result = array();

        $sql = $sql . " ORDER BY id DESC LIMIT " . $offset . "," . $config['per_page'];
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_school_notice_detail($notice_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_NOTICE . " WHERE id = '" . $notice_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;

        foreach ($result as $res) {

            $data['notice_data'] = $res;
            $sql1 = "SELECT * FROM " . TBL_NOTE_FILES . " WHERE note_id = '" . $res['id'] . "' and type = 4";
            $query1 = $this->db->query($sql1);
            $result1 = $query1->result_array();
            foreach ($result1 as $row1) {
                $data['notice_file_data'][] = $row1;
            }

            $count++;
        }

        return $data;
    }

    function get_syllabus_classwise($class_id) {
        $data = array();
        $sql1 = 'SELECT * FROM ' . TBL_SYLLABUS . ' where school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '"';
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        foreach ($result1 as $row) {
            $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" and type = 5';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            $data['syllabus'] = $row;
            $data['syllabus_file'] = $result2;
        }


        return $data;
    }

    function get_student_detail($student_id) {
        $result = array();
        $sql = "SELECT t_s_d.*,t_s.* from " . TBL_STUDENT . " t_s JOIN " . TBL_STUDENT_DETAIL . " t_s_d on t_s.id = t_s_d.student_id WHERE t_s.id = '" . $student_id . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function get_diary_detail($diary_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE id = '" . $diary_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();


        return $result;
    }

    function get_diary_attachment($diary_id) {
        $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $diary_id . '" and type = 6 ';
        $query2 = $this->db->query($sql2);
        $result2 = $query2->result_array();

        return $result2;
    }

    function get_school_calender($school_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_HOLIDAY . " WHERE school_id = '" . $school_id . "' order by start_date ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;
        foreach ($result as $res) {
            $break_start_date = explode('-', $res['start_date']);
            $year = $break_start_date[0];
            $monthe = $break_start_date[1];
            $day = $break_start_date[2];
            $data[$res['start_date']][$count] = $res;
            $count++;
        }

        return $data;
    }

    function get_student_attendance_list() {
        //echo '<pre>';print_r($_POST);die;
        $data = array();
        $data_a = array();
        $data_b = array();
        $school_id = $this->session->userdata('school_id');
        $semester_ids = '';
        $search = '';



        if ($this->input->post('semester_id')) {
            $search .= ' and t_s_e.semester_id  = "' . $this->input->post('semester_id') . '"';
        } else {
            if ($this->input->post('class_id')) {
                $today_date = date('Y-m-d');
                $semester_id_list = $this->my_custom_functions->get_current_semester_id($today_date, $this->input->post('class_id'));
                foreach ($semester_id_list as $semesters) {
                    $semester_ids .= $semesters . ',';
                }
                $semester_id = rtrim($semester_ids, ',');
                $search .= ' and t_s_e.class_id = "' . $this->input->post('class_id') . '" and t_s_e.semester_id IN (' . $semester_id . ')';
            }
        }

        if ($this->input->post('section_id')) {
            $search .= ' and t_a.section_id = "' . $this->input->post('section_id') . '"';
        }
        if ($this->input->post('roll_no')) {
            $search .= ' and t_s_e.roll_no = "' . $this->input->post('roll_no') . '"';
        }
        if ($this->input->post('student_name')) {
            $search .= ' and t_s.name like "%' . $this->input->post('student_name') . '%"';
        }
        if ($this->input->post('start_date')) {
            $search .= ' and t_a.attendance_time >= "' . strtotime($this->input->post('start_date')) . '"';
            $start_date = array('att_start_date' => $this->input->post('start_date'));
            $this->session->set_userdata($start_date);
        } else {
            $start_date = array('att_start_date');
            $this->session->unset_userdata($start_date);
        }

        if ($this->input->post('end_date')) {
            $search .= ' and t_a.attendance_time <= "' . strtotime($this->input->post('end_date')) . '"';
            $end_date = array('att_end_date' => $this->input->post('end_date'));
            $this->session->set_userdata($end_date);
        } else {
            $end_date = array('att_end_date');
            $this->session->unset_userdata($end_date);
        }

        $sql = "SELECT t_a.*,t_s.name,t_s_e.class_id,t_s_e.section_id,t_s_e.roll_no FROM " . TBL_ATTENDANCE . " t_a INNER JOIN " . TBL_STUDENT . " "
                . "t_s on t_a.student_id = t_s.id INNER JOIN " . TBL_STUDENT_ENROLLMENT . " t_s_e ON t_s.id = t_s_e.student_id  WHERE t_a.school_id = " . $school_id . " "
                . "and t_s.is_deleted = 0  " . $search;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $count = 0;
            foreach ($result as $row) {
                $data[$row['student_id']][$count] = $row;

                $count++;
            }

            foreach ($data as $student_id => $attendance_data_set) {
                $present = 0;
                $class = 0;
                foreach ($attendance_data_set as $attend) {
                    $class++;
                    if ($attend['attendance_status'] == PRESENT) {
                        $present++;
                    }
                    $data_a[$student_id]['total_present'] = $present;
                    $data_a[$student_id]['total_class'] = $class;
                }
            }
            foreach ($data_a as $student_id => $rec) {
                $avg = $rec['total_present'] / $rec['total_class'] * 100;
                $data_b[$student_id]['total_present'] = $rec['total_present'];
                $data_b[$student_id]['total_class'] = $rec['total_class'];
                $data_b[$student_id]['percentage'] = $avg;
            }
        }
        return $data_b;
    }

    function get_attendance_detail($student_id) {
        $start_date = '';
        $end_date = '';
        $search = '';
        if ($this->session->userdata('att_start_date') && $this->session->userdata('att_start_date') != '') {
            $start_date = $this->session->userdata('att_start_date');
            $search .= ' and attendance_time >="' . strtotime($start_date) . '"';
        }
        if ($this->session->userdata('att_end_date') && $this->session->userdata('att_end_date') != '') {
            $end_date = $this->session->userdata('att_end_date');
            $search .= ' and attendance_time <="' . strtotime($end_date) . '"';
        }
        $school_id = $this->session->userdata('school_id');

        $return_data = array();
        $count = 0;

        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and student_id = '" . $student_id . "' " . $search . " ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $res) {
                $data[date('Y-m-d', $res['attendance_time'])][] = $res;
            }
            foreach ($data as $attendance_date => $attendance_data) {
                $total_absent = 0;
                $total_present = 0;
                $total_attendance = count($attendance_data);
                $explode_date = explode('-', $attendance_date);
                $day = abs($explode_date[2]);
                $month = abs($explode_date[1]);
                $year = abs($explode_date[0]);
                foreach ($attendance_data as $atten_data) {
                    if ($atten_data['attendance_status'] == PRESENT) {
                        $total_present++;
                    }
                    if ($atten_data['attendance_status'] == ABSENT) {
                        $total_absent++;
                    }
                }
                if ($total_attendance == $total_present) {
                    //$class = 'present_class';
                    $class = 1;
                } else if ($total_attendance == $total_absent) {
                    //$class = 'absent_class';
                    $class = 2;
                } else {
                    //$class = 'pertial_present_class';
                    $class = 3;
                }
                $return_data[$year][$month][$day]['attendance_class'] = $class;
                $return_data[$year][$month][$day]['attendance_date'] = $attendance_date;
                $count++;
            }
        }

        return $return_data;
    }

    function get_teacher_dashboard_classes() {
        $result = array();
        $today = date('Y-m-d');
        $day_num = date('N', strtotime($today));
        $current_time = date('H:i:s');
        $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and day_id = '" . $day_num . "' and teacher_id = '" . $this->session->userdata('teacher_id') . "' and period_start_time <= '" . $current_time . "' and period_end_time >= '" . $current_time . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        } else {
            $sql1 = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and day_id = '" . $day_num . "' and teacher_id = '" . $this->session->userdata('teacher_id') . "' and period_start_time >= '" . $current_time . "' Order by period_start_time ASC LIMIT 0,1";
            $query1 = $this->db->query($sql1);
            if ($query1->num_rows() > 0) {
                $result = $query1->result_array();
            }
        }
        return $result;
    }

    function get_weekly_classes() {
        $result = array();
        $today = date('Y-m-d');
        $day_num = date('N', strtotime($today));
        $current_time = date('H:i:s');
        $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and (day_id < '" . $day_num . "' OR (day_id = '" . $day_num . "' and period_start_time < '" . $current_time . "' )) and teacher_id = '" . $this->session->userdata('teacher_id') . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result['taken_classes'] = $query->num_rows();
        } else {
            $result['taken_classes'] = '';
        }
        $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and (day_id > '" . $day_num . "' OR (day_id = '" . $day_num . "' and period_start_time > '" . $current_time . "')) and teacher_id = '" . $this->session->userdata('teacher_id') . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result['pending_classes'] = $query->num_rows();
        } else {
            $result['pending_classes'] = '';
        }
        return $result;
    }

    function get_three_months_activity_data() {
        $classwork_data = array();
        $homework_data = array();
        $assignment_data = array();
        $data = array();
        $last_three_month = date(strtotime('today - 90 days'));
        $sql = "SELECT * FROM " . TBL_NOTE . " WHERE type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $last_three_month . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $classwork_data = $query->result_array();
        }

        $sql1 = "SELECT * FROM " . TBL_NOTE . " WHERE type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $last_three_month . "'";
        $query1 = $this->db->query($sql1);
        if ($query1->num_rows() > 0) {
            $homework_data = $query1->result_array();
        }

        $sql2 = "SELECT * FROM " . TBL_NOTE . " WHERE type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $last_three_month . "'";
        $query2 = $this->db->query($sql2);
        if ($query2->num_rows() > 0) {
            $assignment_data = $query2->result_array();
        }

        $total_data = array_merge($classwork_data, $homework_data, $assignment_data);

        for ($i = 1; $i < 8; $i++) {
            $data[$i] = 0;
        }

        foreach ($total_data as $row) {
            $activity_date = $row['note_issuetime'];
            $day_num = date('N', $activity_date);
            $data[$day_num] +=1;
        }
        return $data;
    }

    function get_last_seven_days_activity_data() {
        $data = array();
        $first_dat_of_week = date("Y-m-d", strtotime('monday this week'));
        $today = date('Y-m-d', strtotime("-1 days"));
        //$today = date('Y-m-d');
        $class_list_data = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE, ' and school_id = "' . $this->session->userdata('school_id') . '" and teacher_id = "' . $this->session->userdata('teacher_id') . '" group by class_id,section_id');

        foreach ($class_list_data as $class_list) {

            //for ($i = $first_dat_of_week; $i <= $today; $i++) {
            for ($i = $first_dat_of_week; $i <= $today; $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
                $daynum = date('N', strtotime($i));
                if ($daynum == $class_list['day_id']) {
                    $q_s_date = strtotime($i . ' 00:00:00');
                    $q_e_date = strtotime($i . ' 23:59:59');
                    $counter = 0;
                    $counter += $this->my_custom_functions->get_perticular_count(TBL_NOTE, ' and school_id = "' . $this->session->userdata('school_id') . '" and note_issuetime >="' . $q_s_date . '" and note_issuetime <= "' . $q_e_date . '" and semester_id = "' . $class_list['semester_id'] . '" and section_id = "' . $class_list['section_id'] . '"');
                    //$counter += $this->my_custom_functions->get_perticular_count(TBL_NOTE, ' and type = 2 and school_id = "' . $this->session->userdata('school_id') . '" and note_issuetime >="' . $q_s_date . '" and note_issuetime <= "' . $q_e_date . '" and class_id = "'.$class_list['class_id'].'" and section_id = "'.$class_list['section_id'].'"');
                    //$counter += $this->my_custom_functions->get_perticular_count(TBL_NOTE, ' and type = 3 and school_id = "' . $this->session->userdata('school_id') . '" and note_issuetime >="' . $q_s_date . '" and note_issuetime <= "' . $q_e_date . '" and class_id = "'.$class_list['class_id'].'" and section_id = "'.$class_list['section_id'].'"');
                    //echo $i." - ".$counter;echo "<br>";
                    if ($counter == 0) {
                        $data[$i][] = array(
                            'semester_id' => $class_list['semester_id'],
                            'section_id' => $class_list['section_id']
                        );
                    }
                }
            }
        }

        //die;
        return $data;
    }

    function get_current_student_list($class_id, $section_id, $semester_id) {
        $result = array();
        $semesters = '';
        foreach ($semester_id as $semester) {
            $semesters .= $semester . ',';
        }
        $semester_ids = rtrim($semesters, ',');

        $sql = "SELECT t_s_e.id as enrollment_id,t_s_e.class_id,t_s_e.section_id,t_s_e.roll_no,t_s.id,t_s.name FROM " . TBL_STUDENT_ENROLLMENT . " t_s_e INNER JOIN " . TBL_STUDENT . " t_s ON t_s_e.student_id = t_s.id WHERE"
                . " t_s_e.class_id = '" . $class_id . "' and t_s_e.section_id = '" . $section_id . "' and t_s_e.semester_id IN (" . $semester_ids . ")";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }
    }

    function get_semester_list_running($session_list, $class_id) {
        $res = array();
        $sql = "SELECT * from " . TBL_SEMESTER . " where school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $class_id . "' and session_id IN (" . $session_list . ")";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_teacher_attendance_status($attendance_time = "") {

        // Get attendance date
        if ($attendance_time != "") {
            $today = strtotime(date('Y-m-d', $attendance_time));
        } else {
            $today = strtotime(date('Y-m-d'));
        }

        // Get first start attendance record of the teacher on that day
        $sql = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' AND type='1' ORDER BY attendance_time ASC LIMIT 0,1";
        $query = $this->db->query($sql);

        $record = array();
        if ($query->num_rows() > 0) {
            $start_attendance = $query->row_array();

            $record['start_time'] = date("h:i A", $start_attendance['attendance_time']);
            $record['end_time'] = "Not Available";
            $record['duration'] = "Not Available";

            // Get latest end attendance record of the teacher on that day
            $sql_latest = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' AND type='2' ORDER BY attendance_time DESC LIMIT 0,1";
            $query_latest = $this->db->query($sql_latest);

            if ($query_latest->num_rows() > 0) {
                $latest_attendance = $query_latest->row_array();

                $record['end_time'] = date("h:i A", $latest_attendance['attendance_time']);
            }

            // Get last inserted attendance type
            $sql_type = "SELECT type, attendance_time FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' ORDER BY attendance_time DESC LIMIT 0,1";
            $query_type = $this->db->query($sql_type);
            $result_type = $query_type->row_array();
            $record['type'] = ($result_type['type'] == 1) ? "start" : "end";

            // Change of logic
            // Get the duration from last start attendance to current time
            $total_time = 0;
            if ($record['type'] == "start") {
                $total_time = time() - $result_type['attendance_time'];
            }

            // Calculate the total present time
            if ($total_time > 0) {

                $diff = $total_time;
                $hour = floor($diff / 3600);
                $min = floor(($diff - $hour * 3600) / 60);

                $record['duration'] = $hour > 0 ? $hour . 'h:' : '0h:';
                $record['duration'] .= $min > 0 ? $min . 'min' : '0min';

                if ($record['duration'] == '') {
                    $record['duration'] = "Not Available";
                }
            }
        }

        return $record;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_teacher_attendance_status_multiple($attendance_time = "") {

        // Get attendance date
        if ($attendance_time != "") {
            $today = strtotime(date('Y-m-d', $attendance_time));
        } else {
            $today = strtotime(date('Y-m-d'));
        }

        // Get all the attendance records of the teacher on that day
        $sql = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' ORDER BY attendance_time ASC";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {

            $raw_data = $query->result_array();

            foreach ($raw_data as $key => $row) {

                if ($key == 0 OR $row['type'] == 1) { // First attendance is assumed to be the start attendance
                    $start_attendance = $row;

                    $record = array();
                    $record['type'] = "start";
                    $record['start_time'] = date("h:i A", $start_attendance['attendance_time']);
                    $record['end_time'] = "Not Available";
                    $record['duration'] = "Not Available";

                    // Check if the next attendance data is for end attendance
                    $next_key = $key + 1;
                    if (array_key_exists($next_key, $raw_data)) {
                        if ($raw_data[$next_key]['type'] == 2) {

                            $end_attendance = $raw_data[$next_key];

                            $record['type'] = "end";
                            $record['end_time'] = date("h:i A", $end_attendance['attendance_time']);

                            $diff = $end_attendance['attendance_time'] - $start_attendance['attendance_time'];
                            $hour = floor($diff / 3600);
                            $min = floor(($diff - $hour * 3600) / 60);

                            $record['duration'] = $hour > 0 ? $hour . 'h:' : '0h:';
                            $record['duration'] .= $min > 0 ? $min . 'min' : '0min';

                            if ($record['duration'] == '') {
                                $record['duration'] = "Not Available";
                            }
                        }
                    }

                    $result[] = $record;
                }
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function insert_teacher_attendance_data($attendance_time) {

        // Attendance type
        switch ($this->input->post("type")) {
            case "start" :
                $type = 1;
                break;
            case "end" :
                $type = 2;
                break;
        }

        $today = strtotime(date('Y-m-d', $attendance_time));

        // If by mistake first attendance of the day comes as "end" type, forcefully make it as "start" type attendance
        $sql = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->input->post('teacher_id') . "' AND attendance_date = '" . $today . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() == 0) {
            $type = 1;
        }

        $attendance_data = array(
            "teacher_id" => $this->input->post("teacher_id"),
            "attendance_date" => $today,
            "attendance_time" => $attendance_time,
            "latitude" => $this->input->post("latitude"),
            "longitude" => $this->input->post("longitude"),
            "type" => $type,
        );

        $this->db->insert(TBL_TEACHER_ATTENDANCE, $attendance_data);
        $attendance_id = $this->db->insert_id();

        return $attendance_id;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_teacher_attendance_list() {

        $counter = 0;
        $record = array();
        $result = array();

        if ($this->input->post('date') && $this->input->post('date') != '') {

            $today = strtotime($this->input->post('date'));

            // Get first start attendance record of the teacher on that day
            $sql = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' AND type='1' ORDER BY attendance_time ASC LIMIT 0,1";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $start_attendance = $query->row_array();

                $record[$counter]['date'] = date("jS M Y", $today);
                $record[$counter]['start_time'] = date("h:i A", $start_attendance['attendance_time']);
                $record[$counter]['end_time'] = '';
                $record[$counter]['duration'] = '';

                // Get latest end attendance record of the teacher on that day
                $sql_latest = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' AND type='2' ORDER BY attendance_time DESC LIMIT 0,1";
                $query_latest = $this->db->query($sql_latest);

                if ($query_latest->num_rows() > 0) {
                    $latest_attendance = $query_latest->row_array();

                    $record[$counter]['end_time'] = date("h:i A", $latest_attendance['attendance_time']);
                }

                // Sum duration of all the attendance breakups
                // Get all the attendance records of the teacher on that day
                $sql_all = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' ORDER BY attendance_time ASC";
                $query_all = $this->db->query($sql_all);

                $total_time = 0;
                if ($query_all->num_rows() > 0) {

                    $raw_data = $query_all->result_array();

                    foreach ($raw_data as $key => $row) {

                        if ($key == 0 OR $row['type'] == 1) {

                            $breakup_start_attendance = $row;

                            // Check if the next attendance data is for end attendance
                            $next_key = $key + 1;
                            if (array_key_exists($next_key, $raw_data)) {
                                if ($raw_data[$next_key]['type'] == 2) {

                                    $breakup_end_attendance = $raw_data[$next_key];

                                    $total_time += $breakup_end_attendance['attendance_time'] - $breakup_start_attendance['attendance_time'];
                                }
                            }
                        }
                    }
                }

                // Calculate the total present time
                if ($total_time > 0) {

                    $diff = $total_time;
                    $hour = floor($diff / 3600);
                    $min = floor(($diff - $hour * 3600) / 60);

                    $record[$counter]['duration'] = $hour > 0 ? $hour . 'h:' : '0h:';
                    $record[$counter]['duration'] .= $min > 0 ? $min . 'min' : '0min';

                    if ($record[$counter]['duration'] == '') {
                        $record[$counter]['duration'] = "Not Available";
                    }
                }
            }

            $counter++;
        } else {

            $dayLimit = date("Y-m-d", strtotime('first day of last month'));
            for ($current = date("Y-m-d"); $current >= $dayLimit; $current = date("Y-m-d", strtotime("-1 days", strtotime($current)))) {

                $today = strtotime($current);

                // Get first start attendance record of the teacher on that day
                $sql = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' AND type='1' ORDER BY attendance_time ASC LIMIT 0,1";
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {
                    $start_attendance = $query->row_array();

                    $record[$counter]['date'] = date("jS M Y", $today);
                    $record[$counter]['start_time'] = date("h:i A", $start_attendance['attendance_time']);
                    $record[$counter]['end_time'] = '';
                    $record[$counter]['duration'] = '';

                    // Get latest end attendance record of the teacher on that day
                    $sql_latest = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' AND type='2' ORDER BY attendance_time DESC LIMIT 0,1";
                    $query_latest = $this->db->query($sql_latest);

                    if ($query_latest->num_rows() > 0) {
                        $latest_attendance = $query_latest->row_array();

                        $record[$counter]['end_time'] = date("h:i A", $latest_attendance['attendance_time']);
                    }

                    // Sum duration of all the attendance breakups
                    // Get all the attendance records of the teacher on that day
                    $sql_all = "SELECT * FROM " . TBL_TEACHER_ATTENDANCE . " WHERE teacher_id='" . $this->session->userdata('teacher_id') . "' AND attendance_date = '" . $today . "' ORDER BY attendance_time ASC";
                    $query_all = $this->db->query($sql_all);

                    $total_time = 0;
                    if ($query_all->num_rows() > 0) {

                        $raw_data = $query_all->result_array();

                        foreach ($raw_data as $key => $row) {

                            if ($key == 0 OR $row['type'] == 1) {

                                $breakup_start_attendance = $row;

                                // Check if the next attendance data is for end attendance
                                $next_key = $key + 1;
                                if (array_key_exists($next_key, $raw_data)) {
                                    if ($raw_data[$next_key]['type'] == 2) {

                                        $breakup_end_attendance = $raw_data[$next_key];

                                        $total_time += $breakup_end_attendance['attendance_time'] - $breakup_start_attendance['attendance_time'];
                                    }
                                }
                            }
                        }
                    }

                    // Calculate the total present time
                    if ($total_time > 0) {

                        $diff = $total_time;
                        $hour = floor($diff / 3600);
                        $min = floor(($diff - $hour * 3600) / 60);

                        $record[$counter]['duration'] = $hour > 0 ? $hour . 'h:' : '0h:';
                        $record[$counter]['duration'] .= $min > 0 ? $min . 'min' : '0min';

                        if ($record[$counter]['duration'] == '') {
                            $record[$counter]['duration'] = "Not Available";
                        }
                    }
                }

                $counter++;
            }
        }
        $result = array_values($record);

        return $result;
    }

}
