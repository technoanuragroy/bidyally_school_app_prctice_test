<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parent_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_child_list() {
        //echo "<pre>";print_r($this->session->all_userdata());
        $result_a = array();
        $student_id_list = '';
        $parent_id = $this->session->userdata('parent_id');
        $sql = "SELECT student_id from " . TBL_PARENT_KIDS_LINK . " where parent_id = '" . $parent_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();


        foreach ($result as $dchilds_id) {
            $student_id_list .= $dchilds_id['student_id'] . ',';
        }

        $student_id = rtrim($student_id_list, ',');
        $sql_a = "SELECT * from " . TBL_STUDENT . " where id IN(" . $student_id . ")";
        $query_a = $this->db->query($sql_a);
        $result_a = $query_a->result_array();
        //echo "<pre>";print_r($result_a);die;
        return $result_a;
    }

    function get_school_calender($school_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_HOLIDAY . " WHERE school_id = '" . $school_id . "' order by start_date ASC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;
        foreach ($result as $res) {
            $break_start_date = explode('-', $res['start_date']);
            $year = $break_start_date[0];
            $monthe = $break_start_date[1];
            $day = $break_start_date[2];
            $data[$res['start_date']][$count] = $res;
            $count++;
        }

        return $data;
    }

    function get_class_list_daywise() {
        $result = array();
        $data = array();
        //echo "<pre>";print_r($_POST);die;
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $section_id = $this->session->userdata('section_id');
        $i = 1;
        $j = 1;
        $count = 1;
        if ($this->input->post('class_date')) {
            $today = $this->input->post('class_date');
        } else {
            $today = date('Y-m-d');
        }
        $class_date_sess = array('class_date' => $today);
        $this->session->set_userdata($class_date_sess);
        $weekday = date('N', strtotime($today));
        $sql = "SELECT subject_id FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and day_id = '" . $weekday . "' and subject_id != 0 order by id ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $row) {
                if ($i <= 6) {
                    $data['structured'][$j][$count] = $row;

                    if ($i % 2 == 0) {
                        $j++;
                        $count = 0;
                    }

                    $i++;
                    $count++;
                } else {
                    $data['unstructured'][$j][$count] = $row;
                    if ($i % 2 == 0) {
                        $j++;
                        $count = 0;
                    }

                    $i++;
                    $count++;
                }
            }
        }
//        echo "<pre>";
//            print_r($data); die;

        return $data;
    }

    function get_class_note_detail($subject_id) {
        $result = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        $section_id = $this->session->userdata('section_id');

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');

        $today = $this->session->userdata('class_date');
        $custom_start_date_time = $today . ' 00:00:00';
        $custom_end_date_time = $today . ' 23:59:59';
        $beginOfDay = strtotime($custom_start_date_time);
        $endOfDay = strtotime($custom_end_date_time);

//        $beginOfDay = strtotime("midnight", $today);
//        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;
        $sql = "SELECT t_n.*,t_t_t.subject_id from " . TBL_NOTE . " t_n join " . TBL_TIMETABLE . " t_t_t on t_n.period_id=t_t_t.id  WHERE t_n.type = 1 and t_n.school_id = '" . $school_id . "' and t_n.semester_id = '" . $semester_id . "' and t_n.section_id = '" . $section_id . "' and t_t_t.subject_id = '" . $subject_id . "' and note_issuetime >= " . $beginOfDay . " and note_issuetime <= " . $endOfDay . " and t_n.publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result_array();
                $data['notes'] = $row;
                $data['note_file'] = $result2;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    function get_home_note_detail($subject_id) {
        $result = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        $section_id = $this->session->userdata('section_id');
        $today = strtotime($this->session->userdata('class_date'));
        $beginOfDay = strtotime("midnight", $today);
        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');

        $sql = "SELECT t_n.*,t_t_t.subject_id from " . TBL_NOTE . " t_n join " . TBL_TIMETABLE . " t_t_t on t_n.period_id=t_t_t.id  WHERE t_n.type = 2 and t_n.school_id = '" . $school_id . "' and t_n.semester_id = '" . $semester_id . "' and t_n.section_id = '" . $section_id . "' and t_t_t.subject_id = '" . $subject_id . "' and note_issuetime > " . $beginOfDay . " and note_issuetime < " . $endOfDay . " and t_n.publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result_array();
                $data['notes'] = $row;
                $data['note_file'] = $result2;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    function get_assignment_note_detail($subject_id) {
        $result = array();
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        $section_id = $this->session->userdata('section_id');
        $today = strtotime($this->session->userdata('class_date'));
        $beginOfDay = strtotime("midnight", $today);
        $endOfDay = strtotime("tomorrow", $beginOfDay) - 1;

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');

        $sql = "SELECT t_n.*,t_t_t.subject_id from " . TBL_NOTE . " t_n join " . TBL_TIMETABLE . " t_t_t on t_n.period_id=t_t_t.id  WHERE t_n.type = 3 and t_n.school_id = '" . $school_id . "' and t_n.semester_id = '" . $semester_id . "' and t_n.section_id = '" . $section_id . "' and t_t_t.subject_id = '" . $subject_id . "' and note_issuetime > " . $beginOfDay . " and note_issuetime < " . $endOfDay . " and t_n.publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $row['id'] . '" ';
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result_array();
                $data['notes'] = $row;
                $data['note_file'] = $result2;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    function get_attendance_data() {
        $result = array();
        $data = array();
        $data_a = array();
        $data_b = array();

        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        $section_id = $this->session->userdata('section_id');
        $student_id = $this->session->userdata('student_id');

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');


        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and semester_id = '" . $semester_id . "' and section_id = '" . $section_id . "' and student_id = '" . $student_id . "' and publish_data = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            $count = 0;
            foreach ($result as $row) {

                $attendance_date = explode('-', date('Y-m-d', $row['attendance_time']));
                $month = $attendance_date[1];
                $data[$month][$count] = $row;

                $count++;
            }
            foreach ($data as $month => $attendance_data_set) {
                $present = 0;
                $class = 0;
                foreach ($attendance_data_set as $attend) {
                    $class++;
                    if ($attend['attendance_status'] == PRESENT) {
                        $present++;
                    }
                    $data_a[$month]['total_present'] = $present;
                    $data_a[$month]['total_class'] = $class;
                }
            }
            foreach ($data_a as $month => $rec) {
                $avg = $rec['total_present'] / $rec['total_class'] * 100;
                $data_b[$month]['total_present'] = $rec['total_present'];
                $data_b[$month]['total_class'] = $rec['total_class'];
                $data_b[$month]['percentage'] = $avg;
            }
            return $data_b;
        }
    }

    function get_attendance_detail($month, $year) {
        $return_data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        $section_id = $this->session->userdata('section_id');
        $student_id = $this->session->userdata('student_id');
        $return_data = array();
        $count = 0;
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');

        $month_start = strtotime($year . '-' . $month . '-01');
        $month_end = strtotime(date("Y-m-t", $month_start));

        //$sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and student_id = '" . $student_id . "' and month(attendance_time)='" . $month . "' and year(attendance_time)='" . $year . "' and publish_data = 1";
        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and semester_id = '" . $semester_id . "' and section_id = '" . $section_id . "' and student_id = '" . $student_id . "' and (attendance_time>=" . $month_start . " and attendance_time<=" . $month_end . ") and publish_data = 1";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $res) {
                $data[date('Y-m-d', $res['attendance_time'])][] = $res;
            }
            foreach ($data as $attendance_date => $attendance_data) {
                $total_absent = 0;
                $total_present = 0;
                $total_attendance = count($attendance_data);
                $explode_date = explode('-', $attendance_date);
                $day = abs($explode_date[2]);
                foreach ($attendance_data as $atten_data) {
                    if ($atten_data['attendance_status'] == PRESENT) {
                        $total_present++;
                    }
                    if ($atten_data['attendance_status'] == ABSENT) {
                        $total_absent++;
                    }
                }
                if ($total_attendance == $total_present) {
                    //$class = 'present_class';
                    $class = 1;
                } else if ($total_attendance == $total_absent) {
                    //$class = 'absent_class';
                    $class = 2;
                } else {
                    //$class = 'pertial_present_class';
                    $class = 3;
                }
                $return_data[$day]['attendance_class'] = $class;
                $return_data[$day]['attendance_date'] = $attendance_date;
                $count++;
            }
        }

        //echo "<pre>";print_r($return_data);die;
        return $return_data;
    }

    function get_teacher_weekly_schedule() {
        $data = array();
        $school_id = $this->session->userdata('school_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        $section_id = $this->session->userdata('section_id');
        $weeklist = $this->config->item('days_list');

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id = "' . $class_id . '" and session_id = "' . $session_id . '" and school_id = "' . $school_id . '" ');

        foreach ($weeklist as $day => $val) {
            $data[$day] = array();
            $sql = "SELECT * FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "' and class_id = '" . $class_id . "' and semester_id = '" . $semester_id . "' and section_id = '" . $section_id . "' and day_id = '" . $day . "' order by day_id ASC, period_start_time ASC ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $count = 0;

                foreach ($result as $row) {
                    $data[$day][] = $row;
                    $count++;
                }
            }
        }

        return $data;
    }

    function get_general_info() {
        $result = array();
        $sql = "SELECT * FROM " . TBL_GENERAL_INFO . " WHERE school_id = '" . $this->session->userdata('school_id') . "' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function get_school_notice($school_id) {

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and session_id = "' . $this->session->userdata('session_id') . '" and class_id = "' . $this->session->userdata('class_id') . '"');

        $result = array();
        $sql = "SELECT * FROM " . TBL_NOTICE . " WHERE school_id = '" . $school_id . "' and (type = 2 OR semester_id = " . $semester_id . ") and publish_date<=" . strtotime(date('Y-m-d'));
        $offset = 4; // The url segment that store the offset value
        $this->load->library('pagination');
        $config['base_url'] = site_url() . 'parent/user/notice';

        $config['total_rows'] = $this->db->query($sql)->num_rows();
        $config['per_page'] = $this->config->item('per_page');
        $config['uri_segment'] = $offset;
        $config['num_links'] = $this->config->item('num_link');
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';

        $this->pagination->initialize($config);
        if ($this->uri->segment($offset) == "") {
            $offset = 0;
        } else {
            $offset = $this->uri->segment($offset);
        }

        $result = array();

        $sql = $sql . " ORDER BY id DESC LIMIT " . $offset . "," . $config['per_page'];
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    function get_school_notice_detail($notice_id) {
        $data = array();
        $sql = "SELECT * FROM " . TBL_NOTICE . " WHERE id = '" . $notice_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $count = 0;

        foreach ($result as $res) {

            $data['notice_data'] = $res;
            $sql1 = "SELECT * FROM " . TBL_NOTE_FILES . " WHERE note_id = '" . $res['id'] . "' and type = 4 ";
            $query1 = $this->db->query($sql1);
            $result1 = $query1->result_array();
            foreach ($result1 as $row1) {
                $data['notice_file_data'][] = $row1;
            }

            $count++;
        }

        return $data;
    }

    function getDiaryList($school_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE school_id = '" . $school_id . "' and student_id = '" . $this->session->userdata('student_id') . "' order by issue_date DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        //echo "<pre>";print_r($result);
        return $result;
    }

    function get_diary_detail($diary_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE id = '" . $diary_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
    function get_diary_attachment($diary_id) {
        $sql2 = 'SELECT * FROM ' . TBL_NOTE_FILES . ' where note_id = "' . $diary_id . '" and type = 6 ';
            $query2 = $this->db->query($sql2);
            $result2 = $query2->result_array();
            
            return $result2;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_detail() {

        $result = array();

        $student_semesters = $this->my_custom_functions->get_semesters_of_a_student($this->session->userdata('student_id'), $this->session->userdata('session_id'));

        if (!empty($student_semesters)) {
            foreach ($student_semesters as $semester) {

                $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND semester_id='" . $semester['id'] . "'";
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {

                    $result[$semester['id']]['semester_name'] = $semester['semester_name'];
                    $result[$semester['id']]['structure'] = $query->row_array();
                    $result[$semester['id']]['break_ups'] = array();

                    $sqlbrkup = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE fees_id='" . $result[$semester['id']]['structure']['id'] . "' ORDER BY month ASC";
                    $querybrkup = $this->db->query($sqlbrkup);

                    if ($querybrkup->num_rows() > 0) {
                        foreach ($querybrkup->result_array() as $row) {

                            // Check if the fees is paid already
                            $paid = 0;
                            $paid = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and fees_id="' . $row['fees_id'] . '" and fees_breakup_id="' . $row['id'] . '" and school_id="' . $this->session->userdata('school_id') . '" and student_id="' . $this->session->userdata('student_id') . '" and payment_status=1');

                            if ($paid == 0) {
                                $result[$semester['id']]['break_ups'][] = $row;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_breakup_details($breakup_id) {

        $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE id='" . $breakup_id . "'";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            // Check if manual fees is enabled for student
            $sqlsetting = 'SELECT manual_fees_setting FROM ' . TBL_STUDENT . ' WHERE id="' . $this->session->userdata('student_id') . '"';
            $querysetting = $this->db->query($sqlsetting);
            $setting = $querysetting->row_array();

            if ($setting['manual_fees_setting'] == 1) {

                $sqlmanual = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $this->session->userdata('student_id') . '" AND breakup_id="' . $breakup_id . '"';
                $querymanual = $this->db->query($sqlmanual);

                if ($querymanual->num_rows() > 0) {
                    $resultmanual = $querymanual->row_array();
                    $result['manual_breakup_id'] = $resultmanual['id'];
                    $result['fees'] = $resultmanual['fees'];
                }
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_active_payment_gateways() {

        $sql = "SELECT * FROM " . TBL_PAYMENT_GATEWAY . " WHERE status=1 AND platform IN(2,3,4)";
        $query = $this->db->query($sql);

        $result = array();
        $result = $query->result_array();

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_json() {

        $total_fees = 0;
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        $breakup_id = $this->my_custom_functions->ablDecrypt($this->input->post('breakup_id'));

        $all_fees_labels = $labels[$breakup_id];
        $all_fees_amounts = $amounts[$breakup_id];
        $all_fees_options = $options[$breakup_id];

        $fees = array();
        foreach ($all_fees_labels as $keyy => $single_fees_label) {
            $fees[] = array(
                "label" => $single_fees_label,
                "amount" => $all_fees_amounts[$keyy],
                "option" => $all_fees_options[$keyy]
            );

            $total_fees += $all_fees_amounts[$keyy];
        }
        $fees_json = json_encode($fees);

        if ($total_fees == $this->input->post('total_fees')) {
            return $fees_json;
        } else {
            return false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    /* function calculate_fees_amounts($total_fees, $fees_id, $breakup_id, $payment_gateway_id) {

      // Get the school specific payment settings and fees details
      $payment_settings = $this->my_custom_functions->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $this->session->userdata('school_id')));
      $fees_structure = $this->my_custom_functions->get_details_from_id("", TBL_FEES_STRUCTURE, array("id" => $this->my_custom_functions->ablDecrypt($fees_id)));
      $fees_structure_breakup = $this->my_custom_functions->get_details_from_id("", TBL_FEES_STRUCTURE_BREAKUPS, array("id" => $this->my_custom_functions->ablDecrypt($breakup_id)));

      // Default fees calculation
      $fees_amount = $total_fees;
      $late_fine = 0;
      $service_charges = 0;
      $total_amount = $fees_amount;

      // Calculate late fine if any
      if($fees_structure['late_payment_setting'] == 1) {
      if($fees_structure['late_payment_amount'] > 0) {

      $cycle_start_time = strtotime(date("Y-").$fees_structure_breakup['month'].'-01');
      $last_time_before_fine = strtotime(date("Y-m-d", strtotime("+".($fees_structure['late_payment_day_limit'] - 1)." days", $cycle_start_time))." 23:59:59");

      if(time() > $last_time_before_fine) {

      // Fixed late fine
      if($fees_structure['late_payment_type'] == 1) {
      $late_fine = $fees_structure['late_payment_amount'];
      }
      // Percentage late fine
      else if($fees_structure['late_payment_type'] == 2) {
      $late_fine = (($fees_structure['late_payment_amount'] / 100) * $total_amount);
      }
      }
      }
      }

      $total_amount += $late_fine;

      // Calculate fees according to school payment settings
      // If the parent is charged extra
      if($payment_settings['add_on_school_fees'] > 0) {
      $service_charges = (($payment_settings['add_on_school_fees'] / 100) * $total_amount);
      $total_amount += $service_charges;
      }

      // If service charge is negotiated with school
      if($payment_settings['deduct_from_school_fees'] > 0) {
      $service_charges = (($payment_settings['deduct_from_school_fees'] / 100) * $total_amount);
      }

      $return = array(
      'fees_amount' => (int)$fees_amount,
      'late_fine' => (int)$late_fine,
      'service_charges' => (int)$service_charges,
      'total_amount' => (int)$total_amount
      );

      return $return;
      } */

    ////////////////////////////////////////////////////////////////////////////

    function insert_payment($payment_data) {

        $this->db->insert(TBL_FEES_PAYMENT, $payment_data);
        $payment_id = $this->db->insert_id();
        return $payment_id;
    }

    ////////////////////////////////////////////////////////////////////////////

    function update_payment($payment_data, $payment_id) {

        $this->db->where('id', $payment_id);
        $this->db->update(TBL_FEES_PAYMENT, $payment_data);
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    function insert_transfer($transfer_data) {

        $this->db->insert(TBL_PAYMENTS_TO_SCHOOL, $transfer_data);
        $transfer_id = $this->db->insert_id();
        return $transfer_id;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_parent_payment_history() {

        $sql = "SELECT * FROM " . TBL_FEES_PAYMENT . " WHERE student_id='" . $this->session->userdata('student_id') . "' AND payment_status=1 ORDER BY payment_datetime DESC";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $row['student_name'] = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $row['student_id'] . '"');
                $row['school_name'] = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $row['school_id'] . '"');
                $row['fees_name'] = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE_BREAKUPS, 'breakup_label', 'and id = "' . $row['fees_breakup_id'] . '"');

                $result[] = $row;
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_parent_payment_details($payment_id) {

        $sql = "SELECT * FROM " . TBL_FEES_PAYMENT . " WHERE id='" . $payment_id . "'";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $row['student_name'] = $this->my_custom_functions->get_particular_field_value(TBL_STUDENT, 'name', 'and id = "' . $row['student_id'] . '"');
            $row['school_name'] = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'name', 'and id = "' . $row['school_id'] . '"');
            $row['fees_name'] = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE_BREAKUPS, 'breakup_label', 'and id = "' . $row['fees_breakup_id'] . '"');

            $result = $row;
        }

        return $result;
    }

    function get_student_details_from_id($student_id) {
        $sql = "SELECT t_s_e.id as enrollment_id,t_s_e.class_id,t_s_e.section_id,t_s_e.roll_no,t_s.id,t_s.name from " . TBL_STUDENT . " t_s INNER JOIN " . TBL_STUDENT_ENROLLMENT . " t_s_e"
                . " ON t_s.id = t_s_e.student_id WHERE t_s.id = " . $student_id . "";
        $query = $this->db->query($sql);
        $result = array();
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $result = $row;
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_student_result() {

        $school_id = $this->session->userdata('school_id');
        $student_id = $this->session->userdata('student_id');
        $class_id = $this->session->userdata('class_id');
        $session_id = $this->session->userdata('session_id');
        
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and class_id="' . $class_id . '" and session_id="' . $session_id . '" and school_id="' . $school_id . '" ');

        $sql = "SELECT * FROM " . TBL_TERMS . " WHERE semester_id='" . $semester_id . "'";
        $query = $this->db->query($sql);

        $terms = array();        
        $subjects = array();
        
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {                                                                                                                
                                
                $term_id = $row['id'];
                
                // Main term record
                $terms[$term_id] = $row; 
                $terms[$term_id]['exams'] = array();
                
                $sql_exam = "SELECT t_e.id as exam_id, t_e.exam_name, t_e_s.subject_id, t_e_s.full_marks, t_e_s.pass_marks, t_s.score FROM "
                        . TBL_SCORE . " t_s INNER JOIN " . TBL_EXAM_SCORES . " t_e_s ON t_s.score_id=t_e_s.id "
                        . "INNER JOIN " . TBL_EXAM . " t_e ON t_e_s.exam_id=t_e.id "                        
                        . "WHERE t_s.student_id='" . $student_id . "' and t_e.term_id='" . $row['id'] . "' ";
                $query_exam = $this->db->query($sql_exam);

                if ($query_exam->num_rows() > 0) {                    
                    foreach($query_exam->result_array() as $e_s) {
                        
                        $exam_id = $e_s['exam_id'];
                        $subject_id = $e_s['subject_id'];                        
                        
                        $terms[$term_id]['exams'][$exam_id]['exam_name'] = $e_s['exam_name'];                                                                        
                        $terms[$term_id]['exams'][$exam_id]['subjects'][$subject_id] = $e_s;    

                        if(!array_key_exists($subject_id, $subjects)) {
                            $subjects[$subject_id] = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id="' . $subject_id . '"');
                        }
                    }
                }
            }
        }
        
        return array(
            'terms' => $terms,
            'subjects' => $subjects,
            'semester_detail' => $this->my_custom_functions->get_details_from_id($semester_id, TBL_SEMESTER)
        );        
    }

}
