<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function login_check() {
                        
        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $username = preg_replace('/[[:^print:]]/', '', $username);
        $password = $this->input->post('password');
        
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username = '" . $username . "' AND status=1";
        $query = $this->db->query($sql);
        
        $return = array();
        
        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $record) {              
                
                if($record['type'] == TEACHER) {

                    if (password_verify($password, $record['password'])) {
                        
                        $teacher_type = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'staff_type', 'and id = "' . $record["id"] . '"');
                        if ($teacher_type == 1) {
                                                
                            $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $record["id"] . '"');
                            $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');
                            if($school_delete_check == 0) {

                                $teacher_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'is_deleted', 'and id = "' . $record["id"] . '"');
                                if($teacher_delete_check == 0) {

                                    $return[] = $record;  
                                }
                            }
                        }
                    }
                    
                } else if($record['type'] == PARENTS) {

                    if (password_verify($password, $record['password'])) {
                        
                        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and parent_id = "' . $record["id"] . '"');
                        $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');
                        if($school_delete_check == 0) {

                            $return[] = $record;  
                        }
                    }
                    
                } else if($record['type'] == SCHOOL) {

                    if (password_verify($password, $record['password'])) {
                        
                        $school_id = $record["id"];
                        $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');
                        if($school_delete_check == 0) {

                            $return[] = $record;  
                        }
                    }
                }                               
            }
        }
        
        return $return;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function login_check_old() {

        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $password = $this->input->post('password');
        
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username = '" . $username . "' AND status=1";
        $query = $this->db->query($sql);

        $return = 0;
                
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
            
            if (password_verify($password, $record['password'])) {
                

                if ($record['type'] == TEACHER) {
                    $teacher_type = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'staff_type', 'and id = "' . $record["id"] . '"');
                    if ($teacher_type == 1) {
                        $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $record["id"] . '"');
                        $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');

                        if ($school_delete_check == 0) {
                            $teacher_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'is_deleted', 'and id = "' . $record["id"] . '"');
                            if ($teacher_delete_check == 0) {
                                $session_data = array(
                                    "teacher_id" => $record["id"],
                                    "school_id" => $school_id,
                                    "school_is_logged_in" => 1
                                );
                                $this->session->set_userdata($session_data);
                                return TEACHER;
                            } else {
                                return 0;
                            }
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                } else if ($record['type'] == PARENTS) {
                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id = "' . $record["id"] . '"');
                    $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');

                    if ($school_delete_check == 0) {
                        $session_data = array(
                            "parent_id" => $record["id"],
                            "school_id" => $school_id,
                            "parent_is_logged_in" => 1
                        );
                        $this->session->set_userdata($session_data);
                        //echo "<pre>";print_r($this->session->all_userdata());die;
                        return PARENTS;
                    } else {
                        return 0;
                    }
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    function check_parent_existance($mobile_no) {
        
        $sql = "SELECT * from " . TBL_COMMON_LOGIN . " where username = '" . $mobile_no . "' and type = " . PARENTS . " and status = 1";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
            if ($record['password'] != '') {
                return "exist";
            } else {

                $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT, 'school_id', 'and id = "' . $record["id"] . '"');
                $session_data = array(
                    "parent_id" => $record["id"],
                    "school_id" => $school_id,
                    "school_is_logged_in" => 1
                );
                $this->session->set_userdata($session_data);


                return $record["id"];
            }
        } else {
            return "0";
        }
    }

    function get_parent_list($parent_id) {
        $result = array();
        $sql = "SELECT * FROM " . TBL_PARENT_KIDS_LINK . " WHERE parent_id = " . $parent_id . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $parent_record = array();

            foreach ($result as $parents) {
                $parent_record[$parents['school_id']]['sms_credit'] = $this->my_custom_functions->get_available_sms_credits_of_school($parents['school_id']);
                $parent_record[$parents['school_id']]['parent_id'] = $parents['parent_id'];
            }


            $this->my_custom_functions->aasort($parent_record, 'sms_credit');
            $arrange_the_array = array_reverse($parent_record, true);

            $final_array = array_slice($arrange_the_array, 0, 1, true);
            foreach ($final_array as $index_school_id => $row) {
                $school_id = $index_school_id;
            }
            return $school_id;
        }
    }

}
