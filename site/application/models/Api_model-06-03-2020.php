<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function user_verification() {
        
        $record = array();
        
        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $password = $this->input->post('password');
        
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username = '" . $username . "' AND status=1";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $record = $query->row_array();

            if (password_verify($password, $record['password'])) {
                $type = $record['type'];
               
                if($type == TEACHER) {

                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $record["id"] . '"');
                    $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');
                    if($school_delete_check == 0) {

                        $teacher_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'is_deleted', 'and id = "' . $record["id"] . '"');
                        if($teacher_delete_check == 0) {

                            return $record;  
                        }
                    }
                } else if($type == PARENTS) {

                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'school_id', 'and parent_id = "' . $record["id"] . '"');
                    $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');
                    if($school_delete_check == 0) {

                        return $record;  
                    }
                } else if($type == SCHOOL) {
                    
                    $school_id = $record["id"];
                    $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');
                    if($school_delete_check == 0) {
                        
                        return $record;  
                    }
                }               
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function register_device($data) {
        
        $query = $this->db->get_where(TBL_REGISTER_DEVICE, array('user_id' => $data['user_id']));
        
        if ($query->num_rows() > 0) {
            $existing_record = $query->row();
            $update_data = array('device_id' => $data['device_id'],
                'device_type' => $data['device_type'],
                'last_update' => date('Y-m-d H:i:s')
            );
            $this->db->where('id', $existing_record->id);
            $this->db->update(TBL_REGISTER_DEVICE, $update_data);
            return $existing_record->id;
        } else {
            $this->db->insert(TBL_REGISTER_DEVICE, $data);
            $register_device_id = $this->db->insert_id();
            return $register_device_id;
        }
    }

}
