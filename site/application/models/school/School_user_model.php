<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_semester_data() {

        $sql = "SELECT * FROM " . TBL_SEMESTER . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND session_id='" . $this->session->userdata('session_id') . "'";
        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function get_session_data() {

        $sql = "Select * from " . TBL_SESSION . " where school_id = '" . $this->session->userdata('school_id') . "'";


        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_classes_data() {

        $sql = "Select * from " . TBL_CLASSES . " where school_id = '" . $this->session->userdata('school_id') . "'";

//        $offset = 4; //the url segment that store the offset value
////pagination
//        $this->load->library('pagination');
//        $config['base_url'] = site_url() . $this->segment . '/manageClasses/';
//
//        $config['total_rows'] = $this->db->query($sql)->num_rows();
//        $config['per_page'] = $this->config->item('per_page');
//        $config['uri_segment'] = $offset;
//        $config['num_links'] = $this->config->item('num_link');
//        $config['next_link'] = 'Next';
//        $config['prev_link'] = 'Prev';
//        $config['first_link'] = 'First';
//        $config['last_link'] = 'Last';
//
//        $this->pagination->initialize($config);
//        if ($this->uri->segment($offset) == "") {
//            $offset = 0;
//        } else {
//            $offset = $this->uri->segment($offset);
//        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_section_data() {

        $sql = "Select * from " . TBL_SECTION . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_section_data_search() {
        $sql = "Select * from " . TBL_SECTION . " where school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $this->input->post('class') . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_subject_data() {

        $sql = "Select * from " . TBL_SUBJECT . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $offset = 4; //the url segment that store the offset value
////pagination
//        $this->load->library('pagination');
//        $config['base_url'] = site_url() . $this->segment . '/manageSubjects/';
//
//        $config['total_rows'] = $this->db->query($sql)->num_rows();
//        $config['per_page'] = $this->config->item('per_page');
//        $config['uri_segment'] = $offset;
//        $config['num_links'] = $this->config->item('num_link');
//        $config['next_link'] = 'Next';
//        $config['prev_link'] = 'Prev';
//        $config['first_link'] = 'First';
//        $config['last_link'] = 'Last';
//
//        $this->pagination->initialize($config);
//        if ($this->uri->segment($offset) == "") {
//            $offset = 0;
//        } else {
//            $offset = $this->uri->segment($offset);
//        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_teacher_data() {
        $sql = "Select * from " . TBL_TEACHER . " where school_id = '" . $this->session->userdata('school_id') . "'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_time_table_data($semester_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();

        $day_list = $this->config->item('days_list');
//$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

        foreach ($day_list as $day => $val) {
//foreach ($period_list as $ress) {
//$create_data[$day][$ress['id']] = '';
            $counter = 0;
            foreach ($result as $routine) {
                if ($day == $routine['day_id']) {
                    $create_data[$day][$counter] = $routine;
                }

                $counter++;
            }
//}
        }
//echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_period_data() {

        $sql = "Select * from " . TBL_PERIODS . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $offset = 4; //the url segment that store the offset value
////pagination
//        $this->load->library('pagination');
//        $config['base_url'] = site_url() . $this->segment . '/managePeriods/';
//
//        $config['total_rows'] = $this->db->query($sql)->num_rows();
//        $config['per_page'] = $this->config->item('per_page');
//        $config['uri_segment'] = $offset;
//        $config['num_links'] = $this->config->item('num_link');
//        $config['next_link'] = 'Next';
//        $config['prev_link'] = 'Prev';
//        $config['first_link'] = 'First';
//        $config['last_link'] = 'Last';
//
//        $this->pagination->initialize($config);
//        if ($this->uri->segment($offset) == "") {
//            $offset = 0;
//        } else {
//            $offset = $this->uri->segment($offset);
//        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function create_school($admin_id) {


        $update_data = array(
            "name" => $this->input->post("name"),
            "phone" => $this->input->post("phone"),
            "status" => $this->input->post("status"),
            "email" => $this->input->post("email"),
        );


        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function get_student_detail($student_id) {
                
        $sql = 'SELECT t_s.*,t_s_d.* FROM ' . TBL_STUDENT . ' t_s join ' . TBL_STUDENT_DETAIL . ' t_s_d on t_s.id = t_s_d.student_id WHERE t_s.id = ' . $student_id . '';
        $query = $this->db->query($sql);
        $res = $query->result_array();

        $sql1 = "SELECT parent_id FROM " . TBL_PARENT_KIDS_LINK . " WHERE student_id = " . $student_id . "";
        $query1 = $this->db->query($sql1);
        $res1 = $query1->result_array();

        $parents_list = '';
        foreach ($res1 as $ress1) {
            $parents_list .= $ress1['parent_id'] . ',';
        }
        $parents = rtrim($parents_list, ',');

        $res2 = array();
        if($parents != "") {
            $sql2 = "SELECT id,username FROM " . TBL_COMMON_LOGIN . " WHERE id IN ( " . $parents . " ) ";
            $query2 = $this->db->query($sql2);
            $res2 = $query2->result_array();
        }

        $data['student_detail'] = $res;
        $data['username_detail'] = $res2;

        return $data;
    }

    function get_class_list($school_id) {


        $sql = "SELECT * FROM " . TBL_CLASSES . " WHERE school_id = '" . $school_id . "'";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }

    function get_class_list_semester_wise($school_id) {
        $data = array();
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
        //echo "<pre>";print_r($semester_list);
        if (!empty($semester_list)) {
            foreach ($semester_list as $semester) {
                $sql = "SELECT * FROM " . TBL_CLASSES . " WHERE school_id = '" . $school_id . "' and id = '" . $semester['class_id'] . "'";
                $query = $this->db->query($sql);
                $data[] = $query->result_array();
            }
        }

        return $data;
    }

    function update_employee_photo($teacher_id, $photo_url) {
        $update_data = array(
            "file_url" => $photo_url
        );

        $this->db->where('teacher_id', $teacher_id);
        $return = $this->db->update(TBL_TEACHER_FILES, $update_data);
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_dashboard_attendance_chart() {

        $res_this = array();
        $res_last = array();
        $final_rec_this = '';
        $final_rec_last = '';
        $time_array = array();
        $record = array();

        $record['previous_week_data'] = '';
        $record['current_week_data'] = '';
        $record['present_month_attendance'] = 0;
        $record['present_week_attendance'] = 0;
        $record['todays_attendance'] = 0;

        // Monthly attendance
        $time_array[] = $first_day_this_month = strtotime(date('Y-m-01')); // hard-coded '01' for first day
        $time_array[] = $last_day_this_month = strtotime(date('Y-m-t'));

        // Weekly attendance        
        $time_array[] = $this_week_start = strtotime('monday this week');
        $time_array[] = $this_week_end = strtotime('+6 days', $this_week_start);

        $time_array[] = $last_week_start = strtotime('monday last week');
        $time_array[] = $last_week_end = strtotime('+6 days', $last_week_start);

        // Today's attendance
        $time_array[] = $today_start = strtotime(date('Y-m-d') . ' 00:00:00');
        $time_array[] = $today_end = strtotime(date('Y-m-d') . ' 23:59:59');

        for ($i = date("Y-m-d", $this_week_start); $i <= date("Y-m-d", $this_week_end); $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
            $res_this[$i] = 0;
        }
        for ($i = date("Y-m-d", $last_week_start); $i <= date("Y-m-d", $last_week_end); $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
            $res_last[$i] = 0;
        }

        $attendance_start = min($time_array);
        $attendance_end = max($time_array);

        $sql = "SELECT attendance_time FROM " . TBL_ATTENDANCE . " WHERE school_id=" . $this->session->userdata('school_id') . " AND (attendance_time>=" . $attendance_start . " AND attendance_time<=" . $attendance_end . ") AND attendance_status=1";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {

                $attendance_date = date("Y-m-d", $row['attendance_time']);

                if ($row['attendance_time'] >= $first_day_this_month AND $row['attendance_time'] <= $last_day_this_month) {
                    $record['present_month_attendance'] += 1;
                }
                if ($row['attendance_time'] >= $this_week_start AND $row['attendance_time'] <= $this_week_end) {
                    $res_this[$attendance_date] += 1;
                    $record['present_week_attendance'] += 1;
                }
                if ($row['attendance_time'] >= $last_week_start AND $row['attendance_time'] <= $last_week_end) {
                    $res_last[$attendance_date] += 1;
                }
                if ($row['attendance_time'] >= $today_start AND $row['attendance_time'] <= $today_end) {
                    $record['todays_attendance'] += 1;
                }
            }

            foreach ($res_this as $ress) {
                $final_rec_this .= $ress . ',';
            }
            foreach ($res_last as $ress) {
                $final_rec_last .= $ress . ',';
            }
        }

        $final_rec_last = rtrim($final_rec_last, ',');
        $record['previous_week_data'] = $final_rec_last;

        $final_rec_this = rtrim($final_rec_this, ',');
        $record['current_week_data'] = $final_rec_this;

        return $record;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_dashboard_activity_chart() {

        $res_this = array();
        $res_last = array();
        $final_rec_this = '';
        $final_rec_last = '';
        $time_array = array();
        $record = array();

        $record['prev_week_acivity_data'] = '';
        $record['current_week_acivity_data'] = '';
        $record['month_activity_list'] = 0;
        $record['week_activity_list'] = 0;
        $record['day_activity_list'] = 0;

        // Monthly attendance
        $time_array[] = $first_day_this_month = strtotime(date('Y-m-01')); // hard-coded '01' for first day
        $time_array[] = $last_day_this_month = strtotime(date('Y-m-t'));

        // Weekly attendance        
        $time_array[] = $this_week_start = strtotime('monday this week');
        $time_array[] = $this_week_end = strtotime('+6 days', $this_week_start);

        $time_array[] = $last_week_start = strtotime('monday last week');
        $time_array[] = $last_week_end = strtotime('+6 days', $last_week_start);

        // Today's attendance
        $time_array[] = $today_start = strtotime(date('Y-m-d') . ' 00:00:00');
        $time_array[] = $today_end = strtotime(date('Y-m-d') . ' 23:59:59');

        for ($i = date("Y-m-d", $this_week_start); $i <= date("Y-m-d", $this_week_end); $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
            $res_this[$i] = 0;
        }
        for ($i = date("Y-m-d", $last_week_start); $i <= date("Y-m-d", $last_week_end); $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
            $res_last[$i] = 0;
        }

        $activity_start = min($time_array);
        $activity_end = max($time_array);

        $sql = "SELECT note_issuetime FROM " . TBL_NOTE . " WHERE school_id=" . $this->session->userdata('school_id') . " AND (note_issuetime>=" . $activity_start . " AND note_issuetime<=" . $activity_end . ")";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {

                $activity_date = date("Y-m-d", $row['note_issuetime']);

                if ($row['note_issuetime'] >= $first_day_this_month AND $row['note_issuetime'] <= $last_day_this_month) {
                    $record['month_activity_list'] += 1;
                }
                if ($row['note_issuetime'] >= $this_week_start AND $row['note_issuetime'] <= $this_week_end) {
                    $res_this[$activity_date] += 1;
                    $record['week_activity_list'] += 1;
                }
                if ($row['note_issuetime'] >= $last_week_start AND $row['note_issuetime'] <= $last_week_end) {
                    $res_last[$activity_date] += 1;
                }
                if ($row['note_issuetime'] >= $today_start AND $row['note_issuetime'] <= $today_end) {
                    $record['day_activity_list'] += 1;
                }
            }

            foreach ($res_this as $ress) {
                $final_rec_this .= $ress . ',';
            }
            foreach ($res_last as $ress) {
                $final_rec_last .= $ress . ',';
            }
        }

        $final_rec_last = rtrim($final_rec_last, ',');
        $record['prev_week_acivity_data'] = $final_rec_last;

        $final_rec_this = rtrim($final_rec_this, ',');
        $record['current_week_acivity_data'] = $final_rec_this;

        return $record;
    }

    ////////////////////////////////////////////////////////////////////////////

    /* function get_previous_week_data() {

      $res = array();
      $final_rec = '';

      $week_start = strtotime('monday last week');
      $week_end = strtotime('+6 days', $week_start);

      for ($i = date("Y-m-d", $week_start); $i <= date("Y-m-d", $week_end); $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
      $res[$i] = 0;
      }

      $sql = "SELECT attendance_date FROM " . TBL_ATTENDANCE . " WHERE school_id=" . $this->session->userdata('school_id') . " AND (attendance_time>=" . $week_start . " AND attendance_time<=" . $week_end . ") AND attendance_status=1";
      $query = $this->db->query($sql);

      if($query->num_rows() > 0) {
      foreach ($query->result_array() as $row) {
      $res[$row['attendance_date']] += 1;
      }

      foreach ($res as $ress) {
      $final_rec .= $ress . ',';
      }
      }

      $records = rtrim($final_rec, ',');
      return $records;
      }

      ////////////////////////////////////////////////////////////////////////////

      function get_current_week_data() {

      $res = array();
      $final_rec = '';

      $week_start = strtotime('monday this week');
      $week_end = strtotime('+6 days', $week_start);

      for ($i = date("Y-m-d", $week_start); $i <= date("Y-m-d", $week_end); $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
      $res[$i] = 0;
      }

      $sql = "SELECT attendance_date FROM " . TBL_ATTENDANCE . " WHERE school_id=" . $this->session->userdata('school_id') . " AND (attendance_time>=" . $week_start . " AND attendance_time<=" . $week_end . ") AND attendance_status=1";
      $query = $this->db->query($sql);

      if($query->num_rows() > 0) {
      foreach ($query->result_array() as $row) {
      $res[$row['attendance_date']] += 1;
      }

      foreach ($res as $ress) {
      $final_rec .= $ress . ',';
      }
      }

      $records = rtrim($final_rec, ',');
      return $records;
      }

      ////////////////////////////////////////////////////////////////////////////

      function get_attendance_count() {

      $record = array();
      $record['present_month_attendance'] = 0;
      $record['present_week_attendance'] = 0;
      $record['todays_attendance'] = 0;
      $time_array = array();

      // Monthly attendance
      $time_array[] = $first_day_this_month = strtotime(date('Y-m-01')); // hard-coded '01' for first day
      $time_array[] = $last_day_this_month = strtotime(date('Y-m-t'));

      // Weekly attendance
      $time_array[] = $week_start = strtotime('monday this week');
      $time_array[] = $week_end = strtotime('+6 days', $week_start);

      // Today's attendance
      $time_array[] = $today_start = strtotime(date('Y-m-d'). ' 00:00:00');
      $time_array[] = $today_end = strtotime(date('Y-m-d'). ' 23:59:59');

      $attendance_start = min($time_array);
      $attendance_end = max($time_array);

      $sql = "SELECT attendance_time FROM " . TBL_ATTENDANCE . " WHERE school_id=" . $this->session->userdata('school_id') . " AND (attendance_time>=" . $attendance_start . " AND attendance_time<=" . $attendance_end . ") AND attendance_status=1";
      $query = $this->db->query($sql);
      if($query->num_rows() > 0) {
      foreach($query->result_array() as $row) {
      if($row['attendance_time'] >= $first_day_this_month AND $row['attendance_time'] <= $last_day_this_month) {
      $record['present_month_attendance'] += 1;
      }
      if($row['attendance_time'] >= $week_start AND $row['attendance_time'] <= $week_end) {
      $record['present_week_attendance'] += 1;
      }
      if($row['attendance_time'] >= $today_start AND $row['attendance_time'] <= $today_end) {
      $record['todays_attendance'] += 1;
      }
      }
      }

      /*$first_day_last_month = date('Y-m-d', strtotime('first day of last month'));
      $last_day_last_month = date('Y-m-d', strtotime('last day of last month'));

      $sql1 = "SELECT id FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND attendance_status = 1 AND attendance_date>='" . $first_day_last_month . "' AND attendance_date<='" . $last_day_last_month . "'";
      $query1 = $this->db->query($sql1);
      $row['prev_month_attendance'] = $query1->num_rows();

      $diff = abs($row['present_month_attendance'] - $row['prev_month_attendance']);

      @$avg_atten = ($diff / $row['present_month_attendance']) * 100;
      $row['avg_attendance'] = $avg_atten;

      if ($row['present_month_attendance'] > $row['prev_month_attendance']) {
      $row['month_flag'] = 1;  /// Upgoing
      } else {
      $row['month_flag'] = 0;  /// Downgoing
      } */

    /* return $record;
      }

      ////////////////////////////////////////////////////////////////////////////

      function get_previous_week_activity_data() {

      $res = array();
      $prev_week_data_list = '';

      $week_start = date('Y-m-d', strtotime('monday last week'));
      $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

      $start_previous_week = strtotime($week_start);
      $end_previous_week = strtotime($week_end);

      for ($i = $week_start; $i <= $week_end; $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
      //for ($i = $week_start; $i <= $week_end; $i++) {
      $res[$i] = 0;
      }

      $sql = "SELECT note_issuetime FROM " . TBL_NOTE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND note_issuetime>='" . $start_previous_week . "' AND note_issuetime<='" . $end_previous_week . "'";
      $query = $this->db->query($sql);

      if ($query->num_rows() > 0) {
      foreach ($query->result_array() as $row) {
      $activity_date = date('Y-m-d', $row['note_issuetime']);
      $res[$activity_date] += 1;
      }

      foreach ($res as $prev_week_data) {
      $prev_week_data_list .= $prev_week_data . ',';
      }
      }

      $prev_week_final_data_list = rtrim($prev_week_data_list, ',');
      return $prev_week_final_data_list;
      }

      ////////////////////////////////////////////////////////////////////////////

      function get_current_week_activity_data() {

      $res = array();
      $current_week_data_list = '';

      $week_start = date('Y-m-d', strtotime('monday this week'));
      $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

      $start_current_week = strtotime($week_start);
      $end_current_week = strtotime($week_end);

      for ($i = $week_start; $i <= $week_end; $i = date("Y-m-d", strtotime("+1 days", strtotime($i)))) {
      //for ($i = $week_start; $i <= $week_end; $i++) {
      $res[$i] = 0;
      }

      $sql = "SELECT note_issuetime FROM " . TBL_NOTE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND note_issuetime>='" . $start_current_week . "' AND note_issuetime<='" . $end_current_week . "'";
      $query = $this->db->query($sql);

      if ($query->num_rows() > 0) {
      foreach ($query->result_array() as $row) {
      $activity_date = date('Y-m-d', $row['note_issuetime']);
      $res[$activity_date] += 1;
      }

      foreach ($res as $current_week_data) {
      $current_week_data_list .= $current_week_data . ',';
      }
      }

      $current_week_final_data_list = rtrim($current_week_data_list, ',');
      return $current_week_final_data_list;
      }

      ////////////////////////////////////////////////////////////////////////////

      function get_activity_count() {

      $record = array();
      $total_activities_list_month = 0;
      $total_activities_list_current_week = 0;
      $total_activities_list_current_day = 0;

      // Monthly activity
      $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
      $last_day_this_month = date('Y-m-t');

      $start_current_month = strtotime($first_day_this_month);
      $end_current_month = strtotime($last_day_this_month);

      $sql = "SELECT id FROM " . TBL_NOTE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND note_issuetime>='" . $start_current_month . "' AND note_issuetime<='" . $end_current_month . "'";
      $query = $this->db->query($sql);

      if ($query->num_rows() > 0) {
      $total_activities_list_month = $query->num_rows();
      }

      // Weekly activity
      $week_start = date('Y-m-d', strtotime('monday this week'));
      $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

      $start_current_week = strtotime($week_start);
      $end_current_week = strtotime($week_end);

      $sql3 = "SELECT id FROM " . TBL_NOTE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND note_issuetime>='" . $start_current_week . "' AND note_issuetime<='" . $end_current_week . "'";
      $query3 = $this->db->query($sql3);

      if ($query3->num_rows() > 0) {
      $total_activities_list_current_week = $query3->num_rows();
      }

      // Today activity
      $day_start = date('Y-m-d 00:00:00');
      $day_end = date('Y-m-d 23:59:59');

      $start_current_day = strtotime($day_start);
      $end_current_day = strtotime($day_end);

      $sql6 = "SELECT id FROM " . TBL_NOTE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND note_issuetime>='" . $start_current_day . "' AND note_issuetime<='" . $end_current_day . "'";
      $query6 = $this->db->query($sql6);

      if ($query6->num_rows() > 0) {
      $total_activities_list_current_day = $query6->num_rows();
      }

      $record['month_activity_list'] = $total_activities_list_month;
      $record['week_activity_list'] = $total_activities_list_current_week;
      $record['day_activity_list'] = $total_activities_list_current_day;

      return $record;
      } */

    function search_student() {

        $data = array();
        $search = '';
        if ($this->input->post('name') != '') {
            $search .= ' and name like "%' . $this->input->post('name') . '%"';
        }
        if ($this->input->post('rollno') != '') {
            $search .= ' and roll_no = "' . $this->input->post('rollno') . '"';
        }
        if ($this->input->post('class') != '') {
            $search .= ' and class_id = "' . $this->input->post('class') . '"';
        }
        if ($this->input->post('section') != '') {
            $search .= ' and section_id = "' . $this->input->post('section') . '"';
        }

        $sql = "SELECT * FROM " . TBL_STUDENT . " WHERE school_id = '" . $this->session->userdata('school_id') . "'" . $search . " and is_deleted = 0";
        $query = $this->db->query($sql);
        //$data_home_works = $query1->num_rows();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

    function get_permissions() {

        $query = $this->db->query("SELECT * FROM " . TBL_PERMISSION . " ORDER BY id ASC");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }
    }

    function get_login_details($id) {
        $this->db->where('id', $id);
        $query = $this->db->get(TBL_TEACHER);
        return $query->row_array();
    }

    function get_existing_permission($uid, $pid) {

        $query = $this->db->query("SELECT * FROM " . TBL_USER_PERMISSION . " WHERE uid ='$uid' AND permission = '$pid'");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function remove_permission($uid) {

        $this->db->where('uid', $uid);
        $this->db->delete(TBL_USER_PERMISSION);
    }

    function create_permission($data, $user) {
        foreach ($data as $permission => $user_type) {

            $data1 = array(
                'permission' => $permission,
                'uid' => $user,
                'allowed_type' => $user_type
            );

            $this->db->insert(TBL_USER_PERMISSION, $data1);
        }

        return $this->db->insert_id();
    }

    function get_method_permission($fname) {
        $data = array();
        $query = $this->db->query("SELECT * FROM " . TBL_PERMISSION . " WHERE page_urls REGEXP '[[:<:]]" . $fname . "[[:>:]]' AND status=1");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    function get_user_permission($pid, $uid) {
        $data = array();
        $query = $this->db->query("SELECT * FROM " . TBL_USER_PERMISSION . " WHERE uid='$uid' AND permission='$pid' AND allowed_type=1");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
        
    
//    function get_teacher_activity_report() {
//        
//        $class_id = $this->input->post('class');
//        $section_id = $this->input->post('section');
//        
//        $start_date = strtotime($this->input->post('start_date'));
//        $end_date = strtotime($this->input->post('end_date'));
//
//        $beginOfDay = strtotime($this->input->post('start_date') . ' 00:00:00');
//        $endOfDay = strtotime($this->input->post('end_date') . ' 23:59:59');
//        
//        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and session_id = "' . $this->session->userdata('session_id') . '"');
//        
//        $day_array = array();
//        $subject_array = array();
//        $main_data = array();
//        for ($current = date("Y-m-d", $start_date); $current <= date("Y-m-d", $end_date); $current = date("Y-m-d", strtotime("+1 days", strtotime($current)))) {
//            $day = date('N', strtotime($current));
//            @$day_array[$day] += 1;
//        }
//        //echo "<pre>";print_r($day_array);
//
//
//        foreach ($day_array as $day => $day_count) {
//            //echo $no_of_classes_per_day = $this->my_custom_functions->get_perticular_count(TBL_TIMETABLE,'and school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and day_id = "'.$day.'"');
//            //$sql = 'SELECT * FROM '.TBL_TIMETABLE.' WHERE school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and day_id = "'.$day.'"';
//
//           $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" and day_id = "' . $day . '" and subject_id != 0';           
//           $query = $this->db->query($sql);
//
//            if ($query->num_rows() > 0) {
//                $query_data = $query->result_array();
//                foreach ($query_data as $row) {                    
//                    $subject_array[$row['subject_id']]['period_ids'][$row['id']] = $row['id'];
//                    $subject_array[$row['subject_id']]['subject_id'] = $row['subject_id'];
//                    @$subject_array[$row['subject_id']]['subject_count'] += $day_count;
//                }
//            }
//        }
//        //echo "<pre>";print_r($subject_array); die;
//        foreach ($subject_array as $rec) {
//            $total_note = 0;
//            
////                $class_work = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and type = 1 and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" and period_id = "'.$rec['period_id'].'" and note_issuetime >= "' . $beginOfDay . '" and note_issuetime <= "' . $endOfDay . '" and publish_data = 1');
////                $home_work = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and type = 2 and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" and period_id = "'.$rec['period_id'].'" and note_issuetime >= "' . $beginOfDay . '" and note_issuetime <= "' . $endOfDay . '" and publish_data = 1');
////                $assignment_work = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and type = 3 and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" and period_id = "'.$rec['period_id'].'" and note_issuetime >= "' . $beginOfDay . '" and note_issuetime <= "' . $endOfDay . '" and publish_data = 1');
////                $total_note = $class_work + $home_work + $assignment_work;
//                
//            foreach($rec['period_ids'] as $period_id) {
//                $total_note += $this->my_custom_functions->get_perticular_count(TBL_NOTE, ' and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" and period_id = "'.$period_id.'" and note_issuetime >= "' . $beginOfDay . '" and note_issuetime <= "' . $endOfDay . '" and publish_data = 1');
//            }
//            
//            $main_data['activity'][$rec['subject_id']]['subject_count'] = $rec['subject_count'];
//            $main_data['activity'][$rec['subject_id']]['total_activity'] = $total_note;            
//        }
//        //die;
//        //echo "<pre>";print_r($main_data);die;
//        return $main_data;
//    }

     
    function get_student_list() {
        //echo "<pre>";print_r($_POST);die;
        $query_data = array();
        $class_id = $this->input->post('class');
        $section_id = $this->input->post('section');
        $sql = "SELECT * FROM " . TBL_STUDENT . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $class_id . "' and section_id = '" . $section_id . "' and is_deleted = 0";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();
        }
        return $query_data;
    }
    
    function get_student_list_classwise($class_list) {
//        $sql = "SELECT t_sem.id from ".TBL_SEMESTER." t_sem INNER JOIN ".TBL_SESSION." t_ses ON t_sem.session_id=t_ses.id WHERE t_sem.school_id = '".$this->session->userdata('school_id').""
//                . " and t_sem.class_id = ".$class_list."' and t_ses.from_date<='".date("Y-m-d")."' and t_ses.to_date>='".date("Y-m-d")."'";
//        
//        $query = $this->db->query($sql);
//
//        $result = array();
//        if ($query->num_rows() > 0) {
//            foreach ($query->result_array() as $row) {
//                $result[] = $row;
//            }
//        }
//        echo "<pre>";print_r($result);


        $query_data = array();
        $sql = "SELECT id from " . TBL_STUDENT . " WHERE school_id = '" . $this->session->userdata('school_id') . "' and class_id = '" . $class_list . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();
        }
        return $query_data;
    }

    function get_student_list_semesterwise($semester_id) {
        $query_data = array();
        $sql = "SELECT student_id as id from " . TBL_STUDENT_ENROLLMENT . " WHERE semester_id = '" . $semester_id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();
        }
        return $query_data;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_structures_for_session() {

        $semesters = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

        $result = array();
        if (!empty($semesters)) {
            foreach ($semesters as $semester) {

                $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND semester_id='" . $semester['id'] . "'";
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        $result[] = $row;
                    }
                }
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function insert_fees_breakup() {

        $breakup_labels = $this->input->post("breakup_label");
        $months = $this->input->post("month");
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        $fees_id = $this->input->post("fees_id");

        if (!empty($breakup_labels)) {
            foreach ($breakup_labels as $key => $breakup_label) {

                $month = $months[$key];

                $all_fees_labels = $labels[$key];
                $all_fees_amounts = $amounts[$key];
                $all_fees_options = $options[$key];

                $fees = array();
                foreach ($all_fees_labels as $keyy => $single_fees_label) {
                    $fees[] = array(
                        "label" => $single_fees_label,
                        "amount" => $all_fees_amounts[$keyy],
                        "option" => $all_fees_options[$keyy]
                    );
                }
                $fees_json = json_encode($fees);

                $breakup_data = array(
                    "fees_id" => $fees_id,
                    "month" => $month,
                    "breakup_label" => $breakup_label,
                    "fees" => $fees_json
                );

                $this->db->insert(TBL_FEES_STRUCTURE_BREAKUPS, $breakup_data);
            }
        }

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    function update_fees_breakup() {

        $breakup_labels = $this->input->post("breakup_label");
        $months = $this->input->post("month");
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        $fees_id = $this->input->post("fees_id");

        if (!empty($breakup_labels)) {
            foreach ($breakup_labels as $key => $breakup_label) {

                $month = $months[$key];

                $all_fees_labels = $labels[$key];
                $all_fees_amounts = $amounts[$key];
                $all_fees_options = $options[$key];

                $fees = array();
                foreach ($all_fees_labels as $keyy => $single_fees_label) {
                    $fees[] = array(
                        "label" => $single_fees_label,
                        "amount" => $all_fees_amounts[$keyy],
                        "option" => $all_fees_options[$keyy]
                    );
                }
                $fees_json = json_encode($fees);

                $breakup_data = array(
                    "fees_id" => $fees_id,
                    "month" => $month,
                    "breakup_label" => $breakup_label,
                    "fees" => $fees_json
                );

                $this->db->where('id', $key);
                $this->db->update(TBL_FEES_STRUCTURE_BREAKUPS, $breakup_data);
            }
        }

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////    

    function get_fees_breakups_for_manual_fees($fees_id, $student_id) {

        $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE fees_id='" . $fees_id . "'";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {

                // Check if any manual fees exists
                $sqlmanual = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $student_id . '" AND breakup_id="' . $row['id'] . '"';
                $querymanual = $this->db->query($sqlmanual);

                if ($querymanual->num_rows() > 0) {
                    $resultmanual = $querymanual->row_array();
                    $row['fees'] = $resultmanual['fees'];
                }

                $result[] = $row;
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_breakups_for_receiving_fees($student_id) {

        $result = array();

        $student_semesters = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));

        if (!empty($student_semesters)) {
            foreach ($student_semesters as $semester) {

                $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND semester_id='" . $semester['id'] . "'";
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {

                    $result[$semester['id']]['semester_name'] = $semester['semester_name'];
                    $result[$semester['id']]['structure'] = $query->row_array();
                    $result[$semester['id']]['break_ups'] = array();

                    $sqlbrkup = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE fees_id='" . $result[$semester['id']]['structure']['id'] . "' ORDER BY month ASC";
                    $querybrkup = $this->db->query($sqlbrkup);

                    if ($querybrkup->num_rows() > 0) {
                        foreach ($querybrkup->result_array() as $row) {

                            // Check if the fees is paid already
                            $paid = 0;
                            $paid = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and fees_id="' . $row['fees_id'] . '" and fees_breakup_id="' . $row['id'] . '" and school_id="' . $this->session->userdata('school_id') . '" and student_id="' . $student_id . '" and payment_status=1');

                            if ($paid == 0) {

                                // Check if any manual fees exists
                                $sqlmanual = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $student_id . '" AND breakup_id="' . $row['id'] . '"';
                                $querymanual = $this->db->query($sqlmanual);

                                if ($querymanual->num_rows() > 0) {
                                    $resultmanual = $querymanual->row_array();
                                    $row['fees'] = $resultmanual['fees'];
                                }

                                $result[$semester['id']]['break_ups'][] = $row;
                            } else {
                                
                                if ($this->session->userdata("receiveFeesBreakupId")) {
                                    if ($this->session->userdata("receiveFeesBreakupId") == $row['id']) {
                                        
                                        $this->session->unset_userdata("receiveFeesBreakupId");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function manuallly_update_fees_breakup() {

        $breakup_labels = $this->input->post("breakup_label");
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        if (!empty($breakup_labels)) {
            foreach ($breakup_labels as $key => $breakup_label) {

                $all_fees_labels = $labels[$key];
                $all_fees_amounts = $amounts[$key];
                $all_fees_options = $options[$key];

                $fees = array();
                foreach ($all_fees_labels as $keyy => $single_fees_label) {
                    $fees[] = array(
                        "label" => $single_fees_label,
                        "amount" => $all_fees_amounts[$keyy],
                        "option" => $all_fees_options[$keyy]
                    );
                }
                $fees_json = json_encode($fees);

                // Check if manual breakup is already present for this child
                $sql = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $this->input->post("student_id") . '" AND breakup_id="' . $key . '"';
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {
                    $manual_details = $query->row_array();
                    $manual_data = array(
                        'fees' => $fees_json
                    );
                    $this->db->where('id', $manual_details['id']);
                    $this->db->update(TBL_FEES_STRUCTURE_BREAKUPS_MANUAL, $manual_data);
                } else {
                    $manual_data = array(
                        'student_id' => $this->input->post("student_id"),
                        'semester_id' => $this->input->post("semester_id"),
                        'breakup_id' => $key,
                        'fees' => $fees_json
                    );
                    $this->db->insert(TBL_FEES_STRUCTURE_BREAKUPS_MANUAL, $manual_data);
                }
            }
        }

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    function update_manual_fees_setting() {

        if ($this->input->post('manual_fees_setting')) {
            $manual_fees_setting = 1;
        } else {
            $manual_fees_setting = 0;
        }
        $setting_data = array(
            'manual_fees_setting' => $manual_fees_setting
        );

        $this->db->where('id', $this->input->post('setting_student_id'));
        $this->db->update(TBL_STUDENT, $setting_data);
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_breakup_details($breakup_id, $student_id) {

        $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE id='" . $breakup_id . "'";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            // Check if manual fees is enabled for student
            $sqlsetting = 'SELECT manual_fees_setting FROM ' . TBL_STUDENT . ' WHERE id="' . $student_id . '"';
            $querysetting = $this->db->query($sqlsetting);
            $setting = $querysetting->row_array();

            if ($setting['manual_fees_setting'] == 1) {

                $sqlmanual = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $student_id . '" AND breakup_id="' . $breakup_id . '"';
                $querymanual = $this->db->query($sqlmanual);

                if ($querymanual->num_rows() > 0) {
                    $resultmanual = $querymanual->row_array();
                    $result['manual_breakup_id'] = $resultmanual['id'];
                    $result['fees'] = $resultmanual['fees'];
                }
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function insert_fees_for_student() {

        $total_fees = 0;
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        $fees_id = $this->my_custom_functions->ablDecrypt($this->input->post('fees_id'));
        $breakup_id = $this->my_custom_functions->ablDecrypt($this->input->post('breakup_id'));
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_FEES_STRUCTURE, "semester_id", " and id='" . $fees_id . "'");

        $all_fees_labels = $labels[$breakup_id];
        $all_fees_amounts = $amounts[$breakup_id];
        $all_fees_options = $options[$breakup_id];

        $fees = array();
        foreach ($all_fees_labels as $keyy => $single_fees_label) {
            $fees[] = array(
                "label" => $single_fees_label,
                "amount" => $all_fees_amounts[$keyy],
                "option" => $all_fees_options[$keyy]
            );

            $total_fees += $all_fees_amounts[$keyy];
        }
        $fees_json = json_encode($fees);

        if ($total_fees == $this->input->post('fees_amount')) {

            // Insert main payment data
            if ($this->input->post('manual_breakup_id')) {
                $manual_breakup_id = $this->my_custom_functions->ablDecrypt($this->input->post('manual_breakup_id'));
            } else {
                $manual_breakup_id = 0;
            }

            $class_info = $this->my_custom_functions->get_class_info_json_for_student($this->my_custom_functions->ablDecrypt($this->input->post('student_id')), $semester_id);

            $payment_data = array(
                "fees_id" => $fees_id,
                "fees_breakup_id" => $breakup_id,
                "manual_fees_breakup_id" => $manual_breakup_id,
                "fees" => $fees_json,
                "school_id" => $this->session->userdata('school_id'),
                "class_info" => $class_info,
                "student_id" => $this->my_custom_functions->ablDecrypt($this->input->post('student_id')),
                "parent_id" => $this->my_custom_functions->ablDecrypt($this->input->post('parent_id')),
                "fees_amount" => $total_fees,
                "late_fine" => $this->input->post("late_fine"),
                "service_charges" => 0,
                "total_amount" => $this->input->post("total_amount"),
                "paid_amount" => $this->input->post("total_amount"),
                "payment_datetime" => date("Y-m-d H:i:s"),
                "payment_status" => PAYMENT_STATUS_PAID,
                "payment_gateway_id" => OFFLINE_PAID_DB_ID,
                "created_by" => SCHOOL // School
            );

            $this->db->insert(TBL_FEES_PAYMENT, $payment_data);
            $payment_id = $this->db->insert_id();

            return $payment_id;
        } else {

            return false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_recent_payments_of_student($student_id) {

        $data = array();

        // Get semesters of this session
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

        $semester_string = "";
        if (!empty($semester_list)) {
            foreach ($semester_list as $semester) {
                $semester_string .= $semester['id'] . ",";
            }
            $semester_string = rtrim($semester_string, ",");
        }

        if ($semester_string != "") {

            // Get fees structures for this session
            $sql = "SELECT id FROM " . TBL_FEES_STRUCTURE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND semester_id IN(" . $semester_string . ")";
            $query = $this->db->query($sql);

            $fees_string = "";
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $fees_string .= $row['id'] . ",";
                }
                $fees_string = rtrim($fees_string, ",");
            }

            if ($fees_string != "") {

                $sql = "SELECT * FROM " . TBL_FEES_PAYMENT . " WHERE fees_id IN(" . $fees_string . ") AND student_id='" . $student_id . "' AND payment_status=1 ORDER BY payment_datetime DESC LIMIT 0,3";
                $query = $this->db->query($sql);
                $data = $query->result_array();
            }
        }

        return array_reverse($data);
    }

    ////////////////////////////////////////////////////////////////////////////

    function update_fees_structure() {

        $fees_structure_data = array(
            'general_comment' => $this->input->post('comment'),
            'late_payment_setting' => $this->input->post('late_payment_setting'),
            'late_payment_day_limit' => $this->input->post('late_payment_day_limit'),
            'late_payment_type' => $this->input->post('late_payment_type'),
            'late_payment_amount' => $this->input->post('late_payment_amount')
        );

        $this->db->where('id', $this->my_custom_functions->ablDecrypt($this->input->post('fees_id')));
        $this->db->update(TBL_FEES_STRUCTURE, $fees_structure_data);

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    function delete_fees_structure($fees_id) {

        $sql = 'SELECT id FROM ' . TBL_FEES_STRUCTURE_BREAKUPS . ' WHERE fees_id="' . $fees_id . '"';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                // Deleting manual fees records if any                
                $this->db->where('breakup_id', $row['id']);
                $delete = $this->db->delete(TBL_FEES_STRUCTURE_BREAKUPS_MANUAL);

                // Deleting breakup fees records if any                
                $this->db->where('id', $row['id']);
                $delete = $this->db->delete(TBL_FEES_STRUCTURE_BREAKUPS);
            }
        }
        // Deleting main fees record        
        $this->db->where('id', $fees_id);
        $delete = $this->db->delete(TBL_FEES_STRUCTURE);

        return $delete;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_teacher_list_temp_assign($day_num) {

        $class_detail = array();
        $sql = "SELECT * from " . TBL_TIMETABLE . " where school_id = '" . $this->session->userdata('school_id') . "' and day_id = '" . $day_num . "' and teacher_id = '" . $this->input->post('teacher_id') . "' order by period_start_time ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $teacher_list = $this->my_custom_functions->get_multiple_data(TBL_TEACHER, 'and school_id = "' . $this->session->userdata('school_id') . '" and staff_type = 1');

            // echo "<pre>";print_r($teacher_list);
            foreach ($query->result_array() as $row) {//echo "<pre>";print_r($row);
                $class_detail[$row['id']]['class_detail'] = $row;

                foreach ($teacher_list as $teachers) {

                    if ($this->input->post('available_teacher') && $this->input->post('available_teacher') == 1) {

                        $check_availablity = $this->my_custom_functions->get_perticular_count(TBL_TIMETABLE, 'and school_id = "' . $this->session->userdata('school_id') . '" and day_id = "' . $day_num . '" and teacher_id = "' . $teachers['id'] . '" and period_start_time >="' . $row['period_start_time'] . '" and period_end_time <="' . $row['period_end_time'] . '"');
                        if ($check_availablity == 0) {
                            $class_detail[$row['id']]['teacher_list'][] = $teachers;
                        }
                    } else {

                        $class_detail[$row['id']]['teacher_list'][] = $teachers;
                    }
                }
            }
        }
//        echo "<pre>";
//        print_r($class_detail);
//        die;
        return $class_detail;
    }

    ////////////////////////////////////////////////////////////////////////////
//    function get_running_and_upcoming_semesters() {
//
//        $sql = 'SELECT ' . TBL_SEMESTER . '.*, ' . TBL_SESSION . '.from_date, ' . TBL_SESSION . '.to_date '
//                . 'FROM ' . TBL_SEMESTER . ' INNER JOIN ' . TBL_SESSION . ' '
//                . 'ON ' . TBL_SEMESTER . '.session_id=' . TBL_SESSION . '.id '
//                . 'WHERE ' . TBL_SESSION . '.school_id=' . $this->session->userdata('school_id') . ' AND ' . TBL_SESSION . '.to_date>="' . date("Y-m-d") . '" ORDER BY ' . TBL_SESSION . '.from_date DESC, ' . TBL_SEMESTER . '.semester_name ASC';
//        $query = $this->db->query($sql);
//
//        $result = array();
//        if ($query->num_rows() > 0) {
//            foreach ($query->result_array() as $row) {
//
//                $result[] = $row;
//            }
//        }
//
//        return $result;
//    }

    function get_term_data() {
        $result = array();
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

        if (!empty($semester_list)) {
            foreach ($semester_list as $semester) {
                $sql = "Select * from " . TBL_TERMS . " where school_id = '" . $this->session->userdata('school_id') . "' and semester_id = '" . $semester['id'] . "'";
                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {

                        $result[] = $row;
                    }
                }
            }
        }
        return $result;
    }

    function get_exam_data() {
        $result = array();
        
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));

        if (!empty($semester_list)) {
            foreach ($semester_list as $semester) {
                $sql = "Select * from " . TBL_TERMS . " where school_id = '" . $this->session->userdata('school_id') . "' and semester_id = '" . $semester['id'] . "'";
                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        $sql = "Select * from " . TBL_EXAM . " where school_id = '" . $this->session->userdata('school_id') . "' and term_id = '".$row['id']."'";
                        $query = $this->db->query($sql);
                        foreach ($query->result_array() as $ros) {
                        $result[] = $ros;
                        }
                    }
                }
            }
        }
        return $result;




        return $query->result_array();
    }

    function get_exam_data_list($exam_id) {
        $data = array();
        $sql = "Select * from " . TBL_EXAM . " where id = '" . $exam_id . "'";
        $query = $this->db->query($sql);
        $data['exam'] = $query->result_array();

        $sql1 = "Select * from " . TBL_EXAM_SCORES . " where exam_id = '" . $exam_id . "'";
        $query1 = $this->db->query($sql1);
        $data['score_data'] = $query1->result_array();

        return $data;
    }

    function get_term_list($semester_id) {
        $term_id = '';
        $sql = "Select id from " . TBL_TERMS . " where school_id = '" . $this->session->userdata('school_id') . "' and semester_id = '" . $semester_id . "'";
        $query = $this->db->query($sql);
        $result = array();
        $terms = '';
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $term_id .= $row['id'] . ',';
            }
            $terms = rtrim($term_id, ',');
            return $terms;
        }
    }

    function get_exam_list($term_id) {

        $sql = "Select id,exam_name from " . TBL_EXAM . " where school_id = '" . $this->session->userdata('school_id') . "' and term_id  = '" . $term_id . "'";
        $query = $this->db->query($sql);
        $result = array();
        $terms = '';
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }
    }

    function get_student_data($semester_id) {
        $result = array();
        //$semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and session_id = "' . $session_id . '"');

        $sql = "select t_s.name,t_s.id  from " . TBL_STUDENT . " t_s join " . TBL_STUDENT_ENROLLMENT . " t_s_e on t_s.id = t_s_e.student_id  where  t_s_e.semester_id = '" . $semester_id . "' order by t_s_e.roll_no ASC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            //echo "<pre>";print_r($result);die;
            return $result;
        }
    }

    function student_detail($student_id) {
        $sql = "select t_s.*,t_s_e.* from " . TBL_STUDENT . " t_s join " . TBL_STUDENT_ENROLLMENT . " t_s_e on t_s.id = t_s_e.student_id where t_s.id = '" . $student_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    function score_card_detail($exam_id) {
        $sql = "Select * from " . TBL_EXAM_SCORES . " where exam_id = '" . $exam_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>";print_r($result);
        return $result;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_school_account_progress($school_id = "") {
        
        if($school_id == "") {
            $school_id = $this->session->userdata('school_id');
        }
        
        $data = array(
            'account_created' => 'activeProgress',
            'account_verified' => '',
            'create_session' => '',
            'create_class' => '',
            'create_section' => '',
            'create_class_section' => '',
            'create_semesters' => '',
            'add_staff' => '',
            'create_students' => '',
            'percentage' => 10
        );
        
        // Account verified
        $sql = "SELECT otp_verification FROM ".TBL_SCHOOL." WHERE id=".$school_id;
        $query = $this->db->query($sql);
        $school_details = $query->row_array();
        
        if($school_details['otp_verification'] == 1) {
            $data['account_verified'] = 'activeProgress';
            $data['percentage'] += 15;
        }
        
        // Create session 
        $sql = "SELECT id FROM ".TBL_SESSION." WHERE school_id=".$school_id;
        $query = $this->db->query($sql);
        $sessions = $query->num_rows();
        
        if($sessions > 0) {
            $data['create_session'] = 'activeProgress';
            $data['percentage'] += 15;
        }
        
        // Create class
        $sql = "SELECT id FROM ".TBL_CLASSES." WHERE school_id=".$school_id;
        $query = $this->db->query($sql);
        $classes = $query->num_rows();
        
        if($classes > 0) {
            $data['create_class'] = 'activeProgress';            
        }
        
        // Create section
        $sql = "SELECT id FROM ".TBL_SECTION." WHERE school_id=".$school_id;
        $query = $this->db->query($sql);
        $sections = $query->num_rows();
        
        if($sections > 0) {
            $data['create_section'] = 'activeProgress';            
        }
        
        // Calculate parcentage according to class and section combined
        if($data['create_class'] == 'activeProgress' AND $data['create_section'] == 'activeProgress') {
            $data['create_class_section'] = 'activeProgress';    
            $data['percentage'] += 15;
        }
        
        // Create semesters
        $sql = "SELECT id FROM ".TBL_SEMESTER." WHERE school_id=".$school_id;
        $query = $this->db->query($sql);
        $semesters = $query->num_rows();
        
        if($semesters > 0) {
            $data['create_semesters'] = 'activeProgress';
            $data['percentage'] += 15;
        }
        
        // Add staff
        $sql = "SELECT id FROM ".TBL_TEACHER." WHERE school_id=".$school_id;
        $query = $this->db->query($sql);
        $staffs = $query->num_rows();
        
        if($staffs > 0) {
            $data['add_staff'] = 'activeProgress';
            $data['percentage'] += 15;
        }
        
        // Create students
        $sql = "SELECT id FROM ".TBL_STUDENT." WHERE school_id=".$school_id;
        $query = $this->db->query($sql);
        $students = $query->num_rows();
        
        if($students > 0) {
            $data['create_students'] = 'activeProgress';
            $data['percentage'] += 15;
        }
        
        return $data;
    }

}
