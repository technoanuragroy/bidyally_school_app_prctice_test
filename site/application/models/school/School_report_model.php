<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class School_report_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function get_fees_payment_data() {                                   
        
        $data = array();
        
        $search = ''; 
        
        if ($this->session->userdata('fpr_start_date') AND $this->session->userdata('fpr_end_date')) {

            $start_datetime = date("Y-m-d 00:00:00", strtotime($this->session->userdata('fpr_start_date')));
            $end_datetime = date("Y-m-d 23:59:59", strtotime($this->session->userdata('fpr_end_date')));

            $search .= ' AND payment_datetime BETWEEN "' . $start_datetime . '" AND "'.$end_datetime.'"';
        }        
                
        $fpr_semester_id = "";
        $fpr_section_id = "";
        $fpr_name = "";    
        
        if ($this->session->userdata('fpr_semester_id')) {
            $fpr_semester_id = $this->session->userdata('fpr_semester_id');
        }
        if ($this->session->userdata('fpr_section_id')) {
            $fpr_section_id = $this->session->userdata('fpr_section_id');
        } 
        if ($this->session->userdata('fpr_name')) {
            $fpr_name = $this->session->userdata('fpr_name');
        }           
        
        // Get student list         
        $student_array = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id'), "semester_id" => $fpr_semester_id, "section_id" => $fpr_section_id, "name" => $fpr_name));
                   
        $student_list = "";
        if (!empty($student_array)) {
            foreach($student_array as $row) {
                $student_list .= $row['id'].",";
            }
        }
        $student_list = rtrim($student_list, ",");
        
        // Get semesters of this session
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
        
        $semester_string = "";
        if(!empty($semester_list)) {
            foreach($semester_list as $semester) {
                $semester_string .= $semester['id'].",";
            }
            $semester_string = rtrim($semester_string, ",");
        }
        
        if($semester_string != "") {
        
            // Get fees structures for this session
            $sql = "SELECT id FROM " . TBL_FEES_STRUCTURE . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND semester_id IN(".$semester_string.")";
            $query = $this->db->query($sql);
            
            $fees_string = "";
            if ($query->num_rows() > 0) {
                foreach($query->result_array() as $row) {
                    $fees_string .= $row['id'].",";
                }
                $fees_string = rtrim($fees_string, ",");
            }

            if($fees_string != "") {

                // Payment records
                if($student_list != "") {
                    $sql = "SELECT * FROM " . TBL_FEES_PAYMENT . " WHERE fees_id IN(".$fees_string.") AND school_id='" . $this->session->userdata('school_id') . "' AND payment_status=1 AND student_id IN(".$student_list.") " . $search . " ORDER BY payment_datetime DESC";
                } else {
                    $sql = "SELECT * FROM " . TBL_FEES_PAYMENT . " WHERE fees_id IN(".$fees_string.") AND school_id='" . $this->session->userdata('school_id') . "' AND payment_status=1 " . $search . " ORDER BY payment_datetime DESC";
                }        
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {
                    $data = $query->result_array();
                }
            }
        }
        
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_fees_due_data() {
        
        $data = array(); 
        
        $fdr_semester_id = "";
        $fdr_section_id = "";
        $fdr_name = "";     
        
        if ($this->session->userdata('fdr_semester_id')) {
            $fdr_semester_id = $this->session->userdata('fdr_semester_id');
        }
        if ($this->session->userdata('fdr_section_id')) {
            $fdr_section_id = $this->session->userdata('fdr_section_id');
        } 
        if ($this->session->userdata('fdr_name')) {
            $fdr_name = $this->session->userdata('fdr_name');
        }           
        
        // Get student list         
        $student_list = $this->my_custom_functions->get_student_list(array("session_id" => $this->session->userdata('session_id'), "semester_id" => $fdr_semester_id, "section_id" => $fdr_section_id, "name" => $fdr_name));
                         
        // Get semesters of this session
        $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($this->session->userdata('session_id'));
        
        $semester_string = "";
        if(!empty($semester_list)) {
            foreach($semester_list as $semester) {
                $semester_string .= $semester['id'].",";
            }
            $semester_string = rtrim($semester_string, ",");
        }
        
        if($semester_string != "") {
        
            // Get current and previous fees breakups        
            $current_month = date("Y-m-d");        
            $sql = "SELECT t_f_s_b.*, t_f_s.semester_id FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " t_f_s_b JOIN " . TBL_FEES_STRUCTURE . " t_f_s "
                  ."ON t_f_s_b.fees_id=t_f_s.id "
                  ."WHERE t_f_s_b.month<='" . $current_month . "' AND t_f_s.school_id=" . $this->session->userdata('school_id') . " AND t_f_s.semester_id IN(".$semester_string.")";                
            $query = $this->db->query($sql);

            $fees_breakups = array();
            if ($query->num_rows() > 0) {
                foreach($query->result_array() as $row) {
                    $fees_breakups[$row['semester_id']][] = $row;
                }            
            }

            // Due records               
            if(!empty($student_list) AND !empty($fees_breakups)) {

                foreach($student_list as $index => $student) {

                    $data[$index] = $student;

                    foreach($fees_breakups as $semester_id => $breakups) {

                        // If the student is enrolled in this semester
                        $is_enrolled = $this->my_custom_functions->get_perticular_count(TBL_STUDENT_ENROLLMENT, 'and student_id="' . $student['id'] . '" and semester_id="' . $semester_id . '"');

                        if($is_enrolled > 0) {

                            $data[$index]['semester_wise_dues'][$semester_id]['class_info'] = $this->my_custom_functions->get_class_info_json_for_student($student['id'], $semester_id);
                            $data[$index]['semester_wise_dues'][$semester_id]['dues'] = array(); 

                            foreach($breakups as $breakup) {                                                                         

                                // If this fee is already paid
                                $is_paid = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and school_id="' . $this->session->userdata('school_id') . '" and student_id="' . $student['id'] . '" and fees_breakup_id="' . $breakup['id'] . '"');

                                if($is_paid == 0) {

                                    $feesJson = $breakup['fees'];

                                    // Check if any manual fees exists
                                    $sqlmanual = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . " WHERE student_id='" . $student['id'] . "' AND breakup_id='" . $breakup['id'] . "'";
                                    $querymanual = $this->db->query($sqlmanual);

                                    if ($querymanual->num_rows() > 0) {
                                        $resultmanual = $querymanual->row_array();
                                        $feesJson = $resultmanual['fees'];
                                    }

                                    // Calculate fees for the due cycle
                                    $fees_amount = 0;
                                    $fees = json_decode($feesJson, true);                            
                                    foreach ($fees as $fees_list) {
                                        // Mandatory fees only
                                        if($fees_list['option'] == 2) {
                                            if( strpos($fees_list['amount'], ",") !== false ) {
                                                $fees_array = explode(",", $fees_list['amount']);  
                                                $fees_amount += $fees_array[0];
                                            } else {
                                                $fees_amount += $fees_list['amount'];                                       
                                            }
                                        }    
                                    }

                                    $data[$index]['semester_wise_dues'][$semester_id]['dues']['details'][] = $breakup['breakup_label'];
                                    $data[$index]['semester_wise_dues'][$semester_id]['dues']['amount'][] = $fees_amount;
                                    $data[$index]['semester_wise_dues'][$semester_id]['dues']['breakup_id'][] = $breakup['id'];
                                }                        
                            }

                            if(!array_key_exists('details', $data[$index]['semester_wise_dues'][$semester_id]['dues'])) {
                                unset($data[$index]['semester_wise_dues'][$semester_id]);
                            }
                        }
                    }         
                    
                    if(!array_key_exists('semester_wise_dues', $data[$index])) {
                        unset($data[$index]);
                    } else {
                        if(empty($data[$index]['semester_wise_dues'])) {
                            unset($data[$index]);
                        }
                    }
                }
            }
        }
        
        //echo '<pre>'; print_r($data); echo '</pre>'; die;
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function ajax_get_student_name() {
        
        $name_or_roll = $this->input->get("name_or_roll");
        $semester_id = $this->input->get("semester_id");
        $section_id = $this->input->get("section_id");
                
        $sql = 'SELECT t_s.name, t_s_e.roll_no '
                    . 'FROM ' . TBL_STUDENT . ' t_s INNER JOIN ' . TBL_STUDENT_ENROLLMENT . ' t_s_e '
                    . 'ON t_s.id=t_s_e.student_id '
                    . 'WHERE (t_s.name LIKE "%'.$name_or_roll.'%" OR t_s_e.roll_no LIKE "%'.$name_or_roll.'%") AND t_s.is_deleted=0 AND t_s_e.semester_id="'.$semester_id.'" AND t_s_e.section_id="'.$section_id.'" ORDER BY t_s.name ASC';
        $query = $this->db->query($sql);
        
        $rs = array();        
        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $rs[] = $row['name'];
            }
        }
        
        return $rs;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_teacher_list() {
        
        $sql = "SELECT * FROM " . TBL_TEACHER . " WHERE school_id='" . $this->session->userdata('school_id') . "'";
        $query = $this->db->query($sql);
        
        return $query->result_array();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_teacher_ids() {
        
        $sql = "SELECT id FROM " . TBL_TEACHER . " WHERE school_id='" . $this->session->userdata('school_id') . "'";
        $query = $this->db->query($sql);
        
        $data = array();
        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $data[] = $row['id'];
            }
        }
        
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_teacher_holidays_between_date_range($fromDate, $toDate) {
        
        $sql = "SELECT start_date, end_date FROM " . TBL_HOLIDAY . " WHERE school_id='" . $this->session->userdata('school_id') . "' AND session_id='" . $this->session->userdata('session_id') . "' AND (start_date BETWEEN '".$fromDate."' AND '".$toDate."' OR end_date BETWEEN '".$fromDate."' AND '".$toDate."') AND (office_open='2' OR (office_open='1' AND teaching_staff_present='0'))";
        $query = $this->db->query($sql);
        
        $data = array();
        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                // Date range
                if($row['end_date'] != '0000-00-00') {
                    for ($current = $row['start_date']; $current <= $row['end_date']; $current = date("Y-m-d", strtotime("+1 days", strtotime($current)))) {
                        if($current >= $fromDate AND $current <= $toDate) {
                            $data[$current] = $current;
                        }
                    }
                } 
                // Single date
                else {
                    $data[$row['start_date']] = $row['start_date'];
                }
            }
        }
        
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_teacher_attendance_report($encrypted_data = "") {
        
        $multiple_teacher = array();
        $fromDate = "";
        $toDate = "";        
                                        
        if($encrypted_data != "") {
            $encrArray = explode("_", $this->my_custom_functions->ablDecrypt($encrypted_data));
            
            if(count($encrArray) == 3) {                 
                $multiple_teacher = ($encrArray[0] != "") ? json_decode($encrArray[0], true) : array();                
                $fromDate = strtotime($encrArray[1]);
                $toDate = strtotime($encrArray[2]);                
            }
        } 
        
        if($fromDate == "" OR $toDate == "") {            
            $multiple_teacher = $this->session->userdata("tcrrpt_multiple_teacher");
            $fromDate = strtotime($this->session->userdata("tcrrpt_start_date"));
            $toDate = strtotime($this->session->userdata("tcrrpt_end_date"));
        }  
        
        $record = array();   
        $holidays = $this->get_teacher_holidays_between_date_range(date("Y-m-d", $fromDate), date("Y-m-d", $toDate));

        $key = 0;
        for ($current = date("Y-m-d",$fromDate); $current <= date("Y-m-d",$toDate); $current = date("Y-m-d", strtotime("+1 days", strtotime($current)))) {

            $the_day = strtotime($current);
                        
            if(!empty($multiple_teacher)) {
                $teachers = $multiple_teacher;
            } else {                
                $teachers = $this->get_teacher_ids();
            }
            
            $holiday = "";
            if(in_array($current, $holidays)) {
                $holiday = 1; // Holiday
            } else if(date("N", strtotime($current)) == 7) {
                $holiday = 2; // Sunday 
            }

            foreach($teachers as $teacher) {

                $record[$current][$teacher]['name'] = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, "name", " and id='".$teacher."'");
                $record[$current][$teacher]['start_time'] = '-';
                $record[$current][$teacher]['end_time'] = '-';
                $record[$current][$teacher]['duration'] = '-';
                $record[$current][$teacher]['holiday'] = $holiday;             

                // Get start attendance record of the teacher on that day
                $sql = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE teacher_id='".$teacher."' AND attendance_date = '".$the_day."' AND type='1' ORDER BY attendance_time ASC LIMIT 0,1";
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {                      
                    $start_attendance = $query->row_array();
                    
                    $record[$current][$teacher]['start_time'] = date("g:i a", $start_attendance['attendance_time']);  
                                        
                    if($start_attendance['latitude'] != "0" AND $start_attendance['latitude'] != "") { $record[$current][$teacher]['start_lat'] = $start_attendance['latitude']; }
                    if($start_attendance['longitude'] != "0" AND $start_attendance['longitude'] != "") { $record[$current][$teacher]['start_lon'] = $start_attendance['longitude']; }
                            
                    // Get latest end attendance record of the teacher on that day
                    $sql_latest = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE teacher_id='" . $teacher . "' AND attendance_date = '" . $the_day . "' AND type='2' ORDER BY attendance_time DESC LIMIT 0,1"; 
                    $query_latest = $this->db->query($sql_latest);    

                    if ($query_latest->num_rows() > 0) {
                        $latest_attendance = $query_latest->row_array();

                        $record[$current][$teacher]['end_time'] = date("g:i a", $latest_attendance['attendance_time']);
                                                
                        if($latest_attendance['latitude'] != "0" AND $latest_attendance['latitude'] != "") { $record[$current][$teacher]['end_lat'] = $latest_attendance['latitude']; }
                        if($latest_attendance['longitude'] != "0" AND $latest_attendance['longitude'] != "") { $record[$current][$teacher]['end_lon'] = $latest_attendance['longitude']; }
                    }
                    
                    // Sum duration of all the attendance breakups
                    // Get all the attendance records of the teacher on that day
                    $sql_all = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE teacher_id='" . $teacher . "' AND attendance_date = '" . $the_day . "' ORDER BY attendance_time ASC"; 
                    $query_all = $this->db->query($sql_all); 

                    $total_time = 0;
                    if ($query_all->num_rows() > 0) {    

                        $raw_data = $query_all->result_array();

                        foreach($raw_data as $key => $row) {

                            if($key == 0 OR $row['type'] == 1) {

                                $breakup_start_attendance = $row;

                                // Check if the next attendance data is for end attendance
                                $next_key = $key + 1;
                                if(array_key_exists($next_key, $raw_data)) {
                                    if($raw_data[$next_key]['type'] == 2) {

                                        $breakup_end_attendance = $raw_data[$next_key];

                                        $total_time += $breakup_end_attendance['attendance_time'] - $breakup_start_attendance['attendance_time'];                                                                                                
                                    }
                                }   
                            }    
                        }
                    }

                    // Calculate the total present time
                    if($total_time > 0) {

                        $diff = $total_time;
                        $hour = floor($diff / 3600);
                        $min = floor(($diff - $hour * 3600) / 60);

                        $record[$current][$teacher]['duration'] = $hour > 0 ? $hour . ' Hr ' : '';
                        $record[$current][$teacher]['duration'] .= $min > 0 ? $min . ' Min' : '';

                        $tot_min = floor($diff / 60);
                        if($tot_min < 1) {
                            $record[$current][$teacher]['duration'] = 'Not Available';
                        }
                    }  
                    
                    // Get the breakup of attendance for the day
                    $record[$current][$teacher]['break_up'] = $this->get_attendance_breakup($the_day, $teacher);
                }                
            }
        }                        

        return $record;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_attendance_breakup($the_day, $teacher) {
                                        
        // Get all the attendance records of the teacher on that day
        $sql = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE teacher_id='" . $teacher . "' AND attendance_date = '" . $the_day . "' ORDER BY attendance_time ASC"; 
        $query = $this->db->query($sql);    
        
        $result = array();
        if ($query->num_rows() > 0) {    
            
            $raw_data = $query->result_array();
            
            foreach($raw_data as $key => $row) {
                
                if($key == 0 OR $row['type'] == 1) { // First attendance is assumed to be the start attendance
                    
                    $start_attendance = $row;
                              
                    $record = array();                    
                    $record['start_time'] = date("g:i a", $start_attendance['attendance_time']);
                    $record['end_time'] = "-";    
                    $record['duration'] = "-";      
                                                                                                                                            
                    if($start_attendance['latitude'] != "0" AND $start_attendance['latitude'] != "") { $record['start_lat'] = $start_attendance['latitude']; }
                    if($start_attendance['longitude'] != "0" AND $start_attendance['longitude'] != "") { $record['start_lon'] = $start_attendance['longitude']; }
                                                  
                    // Check if the next attendance data is for end attendance
                    $next_key = $key + 1;
                    if(array_key_exists($next_key, $raw_data)) {
                        if($raw_data[$next_key]['type'] == 2) {
                            
                            $end_attendance = $raw_data[$next_key];
                
                            $record['end_time'] = date("g:i a", $end_attendance['attendance_time']);
                            
                            if($end_attendance['latitude'] != "0" AND $end_attendance['latitude'] != "") { $record['end_lat'] = $end_attendance['latitude']; }
                            if($end_attendance['longitude'] != "0" AND $end_attendance['longitude'] != "") { $record['end_lon'] = $end_attendance['longitude']; }
                                                                         
                            $diff = $end_attendance['attendance_time'] - $start_attendance['attendance_time'];
                            $hour = floor($diff / 3600);
                            $min = floor(($diff - $hour * 3600) / 60);

                            $record['duration'] = $hour > 0 ? $hour . ' hours ' : '';
                            $record['duration'] .= $min > 0 ? $min . ' min' : '';

                            $tot_min = floor($diff / 60);
                            if($tot_min < 1) {
                                $record['duration'] = "Not Available";
                            }
                        }
                    }   
                    
                    $result[$row['id']] = $record;
                }
            }                                                                        
        }
                        
        return $result;
    }
    
    function get_academic_report($student_id,$class_id,$semester_id,$session_id){
        $sql = "SELECT * FROM " . TBL_TERMS . " WHERE semester_id='" . $semester_id . "'";
        $query = $this->db->query($sql);

        $terms = array();        
        $subjects = array();
        
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {                                                                                                                
                                
                $term_id = $row['id'];
                
                // Main term record
                $terms[$term_id] = $row; 
                $terms[$term_id]['exams'] = array();
                
                $sql_exam = "SELECT t_e.id as exam_id, t_e.exam_name, t_e_s.subject_id, t_e_s.full_marks, t_e_s.pass_marks, t_s.score FROM "
                        . TBL_SCORE . " t_s INNER JOIN " . TBL_EXAM_SCORES . " t_e_s ON t_s.score_id=t_e_s.id "
                        . "INNER JOIN " . TBL_EXAM . " t_e ON t_e_s.exam_id=t_e.id "                        
                        . "WHERE t_s.student_id='" . $student_id . "' and t_e.term_id='" . $row['id'] . "' ";
                $query_exam = $this->db->query($sql_exam);

                if ($query_exam->num_rows() > 0) {                    
                    foreach($query_exam->result_array() as $e_s) {
                        
                        $exam_id = $e_s['exam_id'];
                        $subject_id = $e_s['subject_id'];                        
                        
                        $terms[$term_id]['exams'][$exam_id]['exam_name'] = $e_s['exam_name'];                                                                        
                        $terms[$term_id]['exams'][$exam_id]['subjects'][$subject_id] = $e_s;    

                        if(!array_key_exists($subject_id, $subjects)) {
                            $subjects[$subject_id] = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id="' . $subject_id . '"');
                        }
                    }
                }
            }
        }
        
        return array(
            'terms' => $terms,
            'subjects' => $subjects,
            'semester_detail' => $this->my_custom_functions->get_details_from_id($semester_id, TBL_SEMESTER)
        ); 
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_teacher_activity_report() {
        
        $string = '';
        $class_id = $this->input->post('class');
        $section_id = $this->input->post('section');
        
        $start_date = strtotime($this->input->post('start_date'));
        $end_date = strtotime($this->input->post('end_date'));

        $beginOfDay = strtotime($this->input->post('start_date') . ' 00:00:00');
        $endOfDay = strtotime($this->input->post('end_date') . ' 23:59:59');
        
        if($this->input->post('teacher_id')){
            $teacher_id = $this->input->post('teacher_id');
            $string .= " and t_t_t.teacher_id = '".$teacher_id."'";
        }
        if($this->input->post('note_type')){
            $note_type = $this->input->post('note_type');
            $string .= " and t_n.type = '".$note_type."'";
        }
        
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and session_id = "' . $this->session->userdata('session_id') . '"');
        
        $query_data = array();
        $sql = "SELECT t_n.*,t_t_t.teacher_id,t_t_t.subject_id from ".TBL_TIMETABLE." t_t_t INNER JOIN ".TBL_NOTE." t_n ON t_t_t.id = t_n.period_id WHERE t_n.school_id = '" . $this->session->userdata('school_id') . "' and t_n.semester_id = '" . $semester_id . "' and class_id = '".$class_id."' and t_n.section_id = '" . $section_id . "'  and t_n.note_issuetime >= '" . $beginOfDay . "' and t_n.note_issuetime <= '" . $endOfDay . "' and t_n.publish_data = 1 ".$string." ORDER BY t_n.note_issuetime";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();
        }
        
        return $query_data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_teacher_activity_report_diary() {                        
        
        $to_date = $this->my_custom_functions->database_date_dash($this->input->post('end_date'));
        $start_date = strtotime($this->my_custom_functions->database_date_dash($this->input->post('start_date')));
        $end_date = strtotime($to_date . ' 23:59:59');
        
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" and session_id = "' . $this->session->userdata('session_id') . '"');
        $this->session->set_userdata('tcrrpt_semester_id', $semester_id);
        
        $rec = array();
        $search = '';
        if ($this->input->post('teacher_id') != '') {
            $search = ' AND teacher_id = "' . $this->input->post('teacher_id') . '"';
        }
        
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND semester_id = '".$semester_id."' AND class_id = '" . $this->input->post('class') . "' AND section_id = '" . $this->input->post('section') . "' AND issue_date >= '" . $start_date . "' AND issue_date <= '" . $end_date . "' ".$search." AND status = 1";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();                        
            foreach ($query_data as $row) {
                @$rec[$row['student_id']] += 1;
            }
        }
        
        ksort($rec);
        return $rec;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function teacher_diary_report_details_of_student($student_id, $teacher_id) {
        
        $to_date = $this->my_custom_functions->database_date_dash($this->session->userdata('tcrrpt_end_date'));
        $start_date = strtotime($this->my_custom_functions->database_date_dash($this->session->userdata('tcrrpt_start_date')));
        $end_date = strtotime($to_date . ' 23:59:59');
        
        $data = array();
        $search = '';
        if ($teacher_id != '') {
            $search = ' AND teacher_id = "' . $teacher_id . '"';
        }
        
        // Get diary notes
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND semester_id = '".$this->session->userdata('tcrrpt_semester_id')."' AND class_id = '" . $this->session->userdata('tcrrpt_class_id') . "' AND section_id = '" . $this->session->userdata('tcrrpt_section_id') . "' AND issue_date >= '" . $start_date . "' AND issue_date <= '" . $end_date . "' ".$search." AND student_id = '" . $student_id . "' AND status = 1";
        $query = $this->db->query($sql);
       
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();                        
            foreach ($query_data as $row) {
                
                $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, "name", " and id='" . $row['teacher_id'] . "'");
                
                $row['teacher_name'] = $teacher_name;                                                
                $data['diary_notes'][] = $row;
            }
        }
        
        // Get teachers
        $sql = "SELECT teacher_id FROM " . TBL_DIARY . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND semester_id = '".$this->session->userdata('tcrrpt_semester_id')."' AND class_id = '" . $this->session->userdata('tcrrpt_class_id') . "' AND section_id = '" . $this->session->userdata('tcrrpt_section_id') . "' AND issue_date >= '" . $start_date . "' AND issue_date <= '" . $end_date . "' ".$search." AND student_id = '" . $student_id . "' AND status = 1 GROUP BY teacher_id";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();                        
            foreach ($query_data as $row) {
                
                $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, "name", " and id='" . $row['teacher_id'] . "'");
                                
                $data['teachers'][$row['teacher_id']] = array(
                    'id' => $row['teacher_id'],
                    'name' => $teacher_name
                );                                 
            }
        }
        
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_attendance_report_classwise() {
        
        $start_date = strtotime($this->my_custom_functions->database_date_dash($this->input->post('start_date')));
        $end_date = strtotime($this->my_custom_functions->database_date_dash($this->input->post('end_date')));
        
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" and session_id = "' . $this->session->userdata('session_id') . '"');
        
        $section_list = array();
        //$section_wise_student_count = array();
        
        if ($this->input->post('section') && $this->input->post('section') != '') {
            $section_list[$semester_id . '_' . $this->input->post('section')] = $semester_id . '_' . $this->input->post('section');
        } else {
            $all_sections = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" ');
            if (!empty($all_sections)) {
                foreach ($all_sections as $section) {
                    $section_list[$semester_id . '_' . $section['id']] = $semester_id . '_' . $section['id'];
                }
            }
        }

        foreach ($section_list as $section) {
            
            $class_section_array = explode("_", $section);
            //$section_wise_student_count[$class_section_array[0] . "_" . $class_section_array[1]] = $this->my_custom_functions->get_perticular_count(TBL_STUDENT_ENROLLMENT, 'and school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "'.$semester_id.'" and section_id = "' . $class_section_array[1] . '"');                    
        }
                
        $extra = '';
        if ($this->input->post('section') && $this->input->post('section') != '') {
            $extra .= ' and section_id = "' . $this->input->post('section') . '"';
        }
        
        $sql = 'SELECT * FROM ' . TBL_ATTENDANCE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and semester_id = "' . $semester_id . '" and attendance_time >="' . $start_date . '" and attendance_time <="' . $end_date . '"' . $extra . '';
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {

            $attendance_count = array();            
            $attendance_percentage = array();

            foreach ($query->result_array() as $row) { //echo "<pre>";print_r($row);
                if ($row['attendance_status'] == 1) {
                    @$attendance_count[date('Y-m-d', $row['attendance_time'])][$row['semester_id'] . '_' . $row['section_id']] += 1;
                }                                           
            }
            
            $total_attendance = $query->num_rows();
                        
            if (!empty($attendance_count)) {
                foreach ($attendance_count as $attendance_date => $attendance) {
                    foreach ($attendance as $class_section_id => $count) {

                        //$percentage = ($count / $section_wise_student_count[$class_section_id]) * 100;
                        $percentage = ($count / $total_attendance) * 100;
                        $attendance_percentage[$attendance_date][$row['semester_id'] . '_' . $row['section_id']] = $percentage;
                    }
                }
            }
            
            return array(
                'section_list' => $section_list,
                'attendance' => $attendance_percentage
            );
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_attendance_report_studentwise() {
        
        $data = array();
        $data_a = array();
        $data_b = array();
        
        $school_id = $this->session->userdata('school_id');
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" and session_id = "' . $this->session->userdata('session_id') . '"');
        
        $search = '';
        if ($this->input->post('class')) {
            $search .= ' and t_a.semester_id = ' . $semester_id . '';
        }
        
        if ($this->input->post('section')) {
            $search .= ' and t_a.section_id = ' . $this->input->post('section') . '';
        }
        
//        if ($this->input->post('roll_no')) {
//            $search .= ' and t_s.roll_no = ' . $this->input->post('roll_no') . '';
//        }
//        if ($this->input->post('student_name')) {
//            $search .= ' and t_s.name like "%' . $this->input->post('student_name') . '%"';
//        }
        
        if ($this->input->post('start_date')) {
            $search .= ' and t_a.attendance_time >= "' . strtotime($this->my_custom_functions->database_date_dash($this->input->post('start_date'))) . '"';
            $start_date = array('att_start_date' => $this->my_custom_functions->database_date_dash($this->input->post('start_date')));
            $this->session->set_userdata($start_date);
        } else {
            $start_date = array('att_start_date');
            $this->session->unset_userdata($start_date);
        }

        if ($this->input->post('end_date')) {
            $search .= ' and t_a.attendance_time <= "' . strtotime($this->my_custom_functions->database_date_dash($this->input->post('end_date'))) . '"';
            $end_date = array('att_end_date' => $this->my_custom_functions->database_date_dash($this->input->post('end_date')));
            $this->session->set_userdata($end_date);
        } else {
            $end_date = array('att_end_date');
            $this->session->unset_userdata($end_date);
        }
        
        if($this->input->post('student')) {
            $search .= ' and t_s.id = "' . $this->input->post('student') . '"';
        }

        $sql = "SELECT t_a.*,t_s.name FROM " . TBL_ATTENDANCE . " t_a JOIN " . TBL_STUDENT . " t_s on t_a.student_id = t_s.id  WHERE t_a.school_id = " . $school_id . "  " . $search;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $count = 0;
            foreach ($result as $row) {//echo "<pre>";print_r($row);
                $data[$row['student_id']][$count] = $row;

                $count++;
            }
            //echo "<pre>";print_r($data); die;
            foreach ($data as $student_id => $attendance_data_set) {
                $present = 0;
                $class = 0;
                foreach ($attendance_data_set as $attend) {
                    $class++;
                    if ($attend['attendance_status'] == PRESENT) {
                        $present++;
                    }                    
                }
                $data_a[$student_id]['total_present'] = $present;
                $data_a[$student_id]['total_class'] = $class;
            }
            foreach ($data_a as $student_id => $rec) {
                $avg = ($rec['total_present'] / $rec['total_class']) * 100;
                $data_b[$student_id]['total_present'] = $rec['total_present'];
                $data_b[$student_id]['total_class'] = $rec['total_class'];
                $data_b[$student_id]['percentage'] = $avg;
            }
            //echo "<pre>";print_r($data_b);die;
        }
        return $data_b;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_attendance_detail($student_id) {
        
        $start_date = '';
        $end_date = '';
        $search = '';
        if ($this->session->userdata('att_start_date') && $this->session->userdata('att_start_date') != '') {
            $start_date = $this->session->userdata('att_start_date');
            $search .= ' and attendance_time >="' . strtotime($start_date) . '"';
        }
        if ($this->session->userdata('att_end_date') && $this->session->userdata('att_end_date') != '') {
            $end_date = $this->session->userdata('att_end_date');
            $search .= ' and attendance_time <="' . strtotime($end_date) . '"';
        }
        $school_id = $this->session->userdata('school_id');

        $return_data = array();
        $count = 0;

        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and student_id = '" . $student_id . "' " . $search . " ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $res) {
                $data[date('Y-m-d', $res['attendance_time'])][] = $res;
            }
            foreach ($data as $attendance_date => $attendance_data) {
                $total_absent = 0;
                $total_present = 0;
                $total_attendance = count($attendance_data);
                $explode_date = explode('-', $attendance_date);
                $day = abs($explode_date[2]);
                $month = abs($explode_date[1]);
                $year = abs($explode_date[0]);
                foreach ($attendance_data as $atten_data) {
                    if ($atten_data['attendance_status'] == PRESENT) {
                        $total_present++;
                    }
                    if ($atten_data['attendance_status'] == ABSENT) {
                        $total_absent++;
                    }
                }
                if ($total_attendance == $total_present) {
                    //$class = 'present_class';
                    $class = 1;
                } else if ($total_attendance == $total_absent) {
                    //$class = 'absent_class';
                    $class = 2;
                } else {
                    //$class = 'pertial_present_class';
                    $class = 3;
                }
                $return_data[$year][$month][$day]['attendance_class'] = $class;
                $return_data[$year][$month][$day]['attendance_date'] = $attendance_date;
                $count++;
            }
        }

        //echo "<pre>";print_r($return_data);die;
        return $return_data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function diary_report_by_teacher() {                        
        
        $to_date = $this->my_custom_functions->database_date_dash($this->input->post('end_date'));
        $start_date = strtotime($this->my_custom_functions->database_date_dash($this->input->post('start_date')));
        $end_date = strtotime($to_date . ' 23:59:59');
        
        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER,'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" and session_id = "' . $this->session->userdata('session_id') . '"');
        $this->session->set_userdata('stdrpt_semester_id', $semester_id);
        
        $rec = array();
        $search = '';
        if ($this->input->post('student') != '') {
            $search = ' AND student_id = "' . $this->input->post('student') . '"';
        }
        
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND semester_id = '".$semester_id."' AND class_id = '" . $this->input->post('class') . "' AND section_id = '" . $this->input->post('section') . "' AND issue_date >= '" . $start_date . "' AND issue_date <= '" . $end_date . "' ".$search." AND status = 1";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();                        
            foreach ($query_data as $row) {
                @$rec[$row['student_id']] += 1;
            }
        }
        
        ksort($rec);
        return $rec;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function diary_report_details_of_student($student_id, $teacher_id) {
        
        $to_date = $this->my_custom_functions->database_date_dash($this->session->userdata('stdrpt_end_date'));
        $start_date = strtotime($this->my_custom_functions->database_date_dash($this->session->userdata('stdrpt_start_date')));
        $end_date = strtotime($to_date . ' 23:59:59');
        
        $data = array();
        $search = '';
        if ($teacher_id != '') {
            $search = ' AND teacher_id = "' . $teacher_id . '"';
        }
        
        // Get diary notes
        $sql = "SELECT * FROM " . TBL_DIARY . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND semester_id = '".$this->session->userdata('stdrpt_semester_id')."' AND class_id = '" . $this->session->userdata('stdrpt_class_id') . "' AND section_id = '" . $this->session->userdata('stdrpt_section_id') . "' AND issue_date >= '" . $start_date . "' AND issue_date <= '" . $end_date . "' ".$search." AND student_id = '" . $student_id . "' AND status = 1";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();                        
            foreach ($query_data as $row) {
                
                $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, "name", " and id='" . $row['teacher_id'] . "'");
                
                $row['teacher_name'] = $teacher_name;                                                
                $data['diary_notes'][] = $row;
            }
        }
        
        // Get teachers
        $sql = "SELECT teacher_id FROM " . TBL_DIARY . " WHERE school_id = '" . $this->session->userdata('school_id') . "' AND semester_id = '".$this->session->userdata('stdrpt_semester_id')."' AND class_id = '" . $this->session->userdata('stdrpt_class_id') . "' AND section_id = '" . $this->session->userdata('stdrpt_section_id') . "' AND issue_date >= '" . $start_date . "' AND issue_date <= '" . $end_date . "' AND student_id = '" . $student_id . "' AND status = 1 GROUP BY teacher_id";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            $query_data = $query->result_array();                        
            foreach ($query_data as $row) {
                
                $teacher_name = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, "name", " and id='" . $row['teacher_id'] . "'");
                                
                $data['teachers'][$row['teacher_id']] = array(
                    'id' => $row['teacher_id'],
                    'name' => $teacher_name
                );                                 
            }
        }
        
        return $data;
    }
}
