<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . 'libraries/aws/aws-autoloader.php';

use Aws\S3\S3Client;

class School_delete_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_student_list_for_delete() {
        $result = array();

        $today = date('Y-m-d') . ' 00:00:00';
        $sql = "SELECT t_s.id,t_s.is_deleted,t_s_d.file_url from " . TBL_STUDENT . " as t_s join " . TBL_STUDENT_DETAIL . " as t_s_d on t_s.id = t_s_d.student_id WHERE t_s.is_deleted >0 and t_s.delete_time <='" . $today . "' order by t_s.id asc limit 0," . DELETE_STUDENT_LIMIT . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function insert_file_data($data, $table) {
        $this->db->insert($table, $data);
        return 1;
    }

    function delete_student_records($student_id, $is_deleted) {



        ///////////////////////  PARENT KIDS LINK DELETE   /////////////////////
        $sql_a = "DELETE FROM " . TBL_PARENT_KIDS_LINK . " WHERE student_id = '" . $student_id . "'";
        $query_a = $this->db->query($sql_a);

        ///////////////////////  ATTENDANCE DELETE   /////////////////////
        $sql_b = "DELETE FROM " . TBL_ATTENDANCE . " WHERE student_id = '" . $student_id . "'";
        $query_b = $this->db->query($sql_b);

        ///////////////////////  DIARY DELETE   /////////////////////
        $sql_c = "DELETE FROM " . TBL_DIARY . " WHERE student_id = '" . $student_id . "'";
        $query_c = $this->db->query($sql_c);

        /////////////////////////DELETE SCORE ////////////////////////////////

        $sql_score = "DELETE FROM " . TBL_SCORE . " WHERE student_id = '" . $student_id . "'";
        $query_score = $this->db->query($sql_score);

        /////////////////////////DELETE STUDENT ENROLLMENT ////////////////////////////////

        $sql_enroll = "DELETE FROM " . TBL_STUDENT_ENROLLMENT . " WHERE student_id = '" . $student_id . "'";
        $query_enroll = $this->db->query($sql_enroll);

        ///////////////////////  STUDENT DETAIL DELETE   /////////////////////
        $sql_d = "DELETE FROM " . TBL_STUDENT_DETAIL . " WHERE student_id = '" . $student_id . "'";
        $query_d = $this->db->query($sql_d);

        ///////////////////////  STUDENT DELETE   /////////////////////
        $sql_e = "DELETE FROM " . TBL_STUDENT . " WHERE id = '" . $student_id . "'";
        $query_e = $this->db->query($sql_e);

        ///////////////////////  FEES STRUCTURE BREAKUPS MANUAL DELETE   /////////////////////
        $sql_break_up_manual = "DELETE FROM " . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . " WHERE student_id = '" . $student_id . "'";
        $query_break_up_manual = $this->db->query($sql_break_up_manual);

        if ($is_deleted == DELETE) {
            ///////////////////////  FEES PAYMENT DELETE   /////////////////////
            $sql_break_up_manual = "DELETE FROM " . TBL_FEES_PAYMENT . " WHERE student_id = '" . $student_id . "'";
            $query_break_up_manual = $this->db->query($sql_break_up_manual);
        } else {
            $sql_break_up_manual = "UPDATE " . TBL_FEES_PAYMENT . " set student_id = 0 WHERE student_id = '" . $student_id . "'";
            $query_break_up_manual = $this->db->query($sql_break_up_manual);
        }


        return $query_e;
    }

    function delete_parents($student_id) {
        $parent_id = $this->my_custom_functions->get_particular_field_value(TBL_PARENT_KIDS_LINK, 'parent_id', 'and student_id = "' . $student_id . '"');
        $parent_count = $this->my_custom_functions->get_perticular_count(TBL_PARENT_KIDS_LINK, 'and parent_id = "' . $parent_id . '"');
        if ($parent_count == 1) {
            $condition = array('id' => $parent_id);
            $delete_parent = $this->my_custom_functions->delete_data(TBL_PARENT, $condition);

            $reg_device_condition = array('user_id' => $parent_id);
            $delete_register_device = $this->my_custom_functions->delete_data(TBL_REGISTER_DEVICE, $reg_device_condition);

            $condition = array('id' => $parent_id);
            $delete_parent_login = $this->my_custom_functions->delete_data(TBL_COMMON_LOGIN, $condition);
        }
        return true;
    }

    function get_teacher_list_for_delete() {
        $result = array();

        $today = date('Y-m-d') . ' 00:00:00';
        $sql = "SELECT id,file_url from " . TBL_TEACHER . "  WHERE is_deleted = 1 and delete_time <='" . $today . "' order by id asc limit 0," . DELETE_TEACHER_LIMIT . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function delete_teacher_records($teacher_id) {
        ////////////////////////////////// DELETE from tbl_common_login ////////////////////////////////
        $sql_a = "DELETE FROM " . TBL_COMMON_LOGIN . " WHERE id = '" . $teacher_id . "'";
        $query_a = $this->db->query($sql_a);

        ////////////////////////////////// DELETE from tbl_diary ////////////////////////////////
        $sql_b = "DELETE FROM " . TBL_DIARY . " WHERE teacher_id = '" . $teacher_id . "'";
        $query_b = $this->db->query($sql_b);

        ////////////////////////////////// DELETE from tbl_temp_timetable ////////////////////////////////
        $sql_c = "DELETE FROM " . TBL_TEMP_TIMETABLE . " WHERE teacher_id = '" . $teacher_id . "'";
        $query_c = $this->db->query($sql_c);

        ////////////////////////////////// DELETE from tbl_timetable ////////////////////////////////
        $sql_d = "DELETE FROM " . TBL_TIMETABLE . " WHERE teacher_id = '" . $teacher_id . "'";
        $query_d = $this->db->query($sql_d);

        ////////////////////////////////// DELETE from tbl_teachers ////////////////////////////////
        $sql_e = "DELETE FROM " . TBL_TEACHER . " WHERE id = '" . $teacher_id . "'";
        $query_e = $this->db->query($sql_e);

        ////////////////////////////////// DELETE from tbl_register_device ////////////////////////////////
        $sql_f = "DELETE FROM " . TBL_REGISTER_DEVICE . " WHERE user_id = '" . $teacher_id . "'";
        $query_f = $this->db->query($sql_f);

        ////////////////////////////////// DELETE from tbl_register_device ////////////////////////////////
        $sql_g = "DELETE FROM " . TBL_USER_PERMISSION . " WHERE uid = '" . $teacher_id . "'";
        $query_g = $this->db->query($sql_g);

        return $query_g;
    }

    function get_school_list_for_delete() {
        $result = array();

        $today = date('Y-m-d') . ' 00:00:00';
        $sql = "SELECT id,file_url from " . TBL_SCHOOL . "  WHERE is_deleted = 1 and delete_time <='" . $today . "' order by id asc limit 0," . DELETE_SCHOOL_LIMIT . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function delete_school_records($school_id) {
        ///////////////////////  ATTENDANCE DELETE   /////////////////////
        $sql_a = "DELETE FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "'";
        $query_a = $this->db->query($sql_a);

        ///////////////////////  CLASSES DELETE   /////////////////////
        $sql_b = "DELETE FROM " . TBL_CLASSES . " WHERE school_id = '" . $school_id . "'";
        $query_b = $this->db->query($sql_b);

        ///////////////////////  DIARY DELETE   /////////////////////
        $sql_c = "DELETE FROM " . TBL_DIARY . " WHERE school_id = '" . $school_id . "'";
        $query_c = $this->db->query($sql_c);


        ///////////////////////  GENERAL INFO DELETE   /////////////////////
        $sql_e = "DELETE FROM " . TBL_GENERAL_INFO . " WHERE school_id = '" . $school_id . "'";
        $query_e = $this->db->query($sql_e);

        ///////////////////////  HOLIDAYS DELETE   /////////////////////
        $sql_f = "DELETE FROM " . TBL_HOLIDAY . " WHERE school_id = '" . $school_id . "'";
        $query_f = $this->db->query($sql_f);


        ///////////////////////  NOTE DELETE   /////////////////////
        $delete_date_pre = date('Y-m-d', time());
        $delete_date = $delete_date_pre . ' 00:00:00';
        $sql_g = "UPDATE " . TBL_NOTE . " set is_deleted = 1, delete_time = '" . $delete_date . "' WHERE school_id = '" . $school_id . "'";
        $query_g = $this->db->query($sql_g);

        /////////////////////////////////////////////////////////////
        ///////////////////////  SECTION DELETE   /////////////////////
        $sql_h = "DELETE FROM " . TBL_SECTION . " WHERE school_id = '" . $school_id . "'";
        $query_h = $this->db->query($sql_h);

        ///////////////////////  SMS CREDITS DELETE   /////////////////////
        $sql_i = "DELETE FROM " . TBL_SMS_CREDITS . " WHERE school_id = '" . $school_id . "'";
        $query_i = $this->db->query($sql_i);

        ///////////////////////  STUDENT DELETE   /////////////////////
        $delete_date_pre = date('Y-m-d', time());
        $delete_date = $delete_date_pre . ' 00:00:00';
        $sql_j = "UPDATE " . TBL_STUDENT . " SET is_deleted = 1, delete_time = '" . $delete_date . "' WHERE school_id = '" . $school_id . "'";
        $query_j = $this->db->query($sql_j);

        ///////////////////////  SMS SUBJECT DELETE   /////////////////////
        $sql_k = "DELETE FROM " . TBL_SUBJECT . " WHERE school_id = '" . $school_id . "'";
        $query_k = $this->db->query($sql_k);


        ///////////////////////  TEACHER DELETE   /////////////////////
        $delete_date_pre = date('Y-m-d', time());
        $delete_date = $delete_date_pre . ' 00:00:00';
        $sql_m = "UPDATE " . TBL_TEACHER . " SET `is_deleted` = 1,`delete_time` = '" . $delete_date . "' WHERE `school_id` = '" . $school_id . "'";
        $query_m = $this->db->query($sql_m);

        ///////////////////////  TEMP TIMETABLE DELETE   /////////////////////
        $sql_n = "DELETE FROM " . TBL_TEMP_TIMETABLE . " WHERE school_id = '" . $school_id . "'";
        $query_n = $this->db->query($sql_n);

        ///////////////////////  TIMETABLE DELETE   /////////////////////
        $sql_o = "DELETE FROM " . TBL_TIMETABLE . " WHERE school_id = '" . $school_id . "'";
        $query_o = $this->db->query($sql_o);

        ///////////////////////  EXAM DELETE   /////////////////////
        $sql_exam = "DELETE FROM " . TBL_EXAM . " WHERE school_id = '" . $school_id . "'";
        $query_exam = $this->db->query($sql_exam);

        ///////////////////////  EXAM SCORE DELETE   /////////////////////
        $sql_exam_sc = "DELETE FROM " . TBL_EXAM_SCORES . " WHERE school_id = '" . $school_id . "'";
        $query_exam_sc = $this->db->query($sql_exam_sc);

        ///////////////////////  NOTIFICATION SETTING DELETE   /////////////////////
        $sql_notification_st = "DELETE FROM " . TBL_NOTIFICATION_SETTINGS . " WHERE school_id = '" . $school_id . "'";
        $query_notification_st = $this->db->query($sql_notification_st);

        ///////////////////////  SCORE TABLE DELETE   /////////////////////

        $del_score = "DELETE FROM " . TBL_SCORE . " WHERE school_id = '" . $school_id . "'";
        $query_score = $this->db->query($del_score);

        ///////////////////////  SEMESTER TABLE DELETE   /////////////////////

        $del_sem = "DELETE FROM " . TBL_SEMESTER . " WHERE school_id = '" . $school_id . "'";
        $query_sem = $this->db->query($del_sem);

        ///////////////////////  SESSION TABLE DELETE   /////////////////////

        $del_ses = "DELETE FROM " . TBL_SESSION . " WHERE school_id = '" . $school_id . "'";
        $query_ses = $this->db->query($del_ses);

        ///////////////////////  TERMS TABLE DELETE   /////////////////////

        $del_terms = "DELETE FROM " . TBL_TERMS . " WHERE school_id = '" . $school_id . "'";
        $query_terms = $this->db->query($del_terms);

        ///////////////////////  PAYMENT TO SCHOOL TABLE DELETE   /////////////////////
        $sql_payment_to_school = "DELETE FROM " . TBL_PAYMENTS_TO_SCHOOL . " WHERE school_id = '" . $school_id . "'";
        $query_payment_to_school = $this->db->query($sql_payment_to_school);

        ///////////////////////  FEES STRUCTURE AND FEES STRUCTURE BREAKUP TABLE DELETE   /////////////////////

        $sql_fees_str = "SELECT * FROM " . TBL_FEES_STRUCTURE . " WHERE school_id = '" . $school_id . "'";
        $query_fees_str = $this->db->query($sql_fees_str);

        if ($query_fees_str->num_rows() > 0) {
            $result = $query_fees_str->result_array();

            foreach ($result as $row) {
                $del_fees_str_brk_up = "DELETE FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE fees_id = '" . $row['id'] . "'";
                $query_fees_str_brk_up = $this->db->query($del_fees_str_brk_up);
            }
        }

        $del_fees_str = "DELETE FROM " . TBL_FEES_STRUCTURE . " WHERE school_id = '" . $school_id . "'";
        $query_fees_str = $this->db->query($del_fees_str);

        ///////////////////////  SCHOOL PAYMENT SETTINGS TABLE DELETE   /////////////////////

        $del_sc_pay_sett = "DELETE FROM " . TBL_SCHOOL_PAYMENT_SETTINGS . " WHERE school_id = '" . $school_id . "'";
        $query_sc_pay_sett = $this->db->query($del_sc_pay_sett);


        ///////////////////////  SCHOOL DELETE   /////////////////////
        $sql_p = "DELETE FROM " . TBL_SCHOOL . " WHERE id = '" . $school_id . "'";
        $query_p = $this->db->query($sql_p);
    }

    function delete_school_notice($school_id) {
        $sql = "SELECT t_n.id,t_n_f.file_url from " . TBL_NOTICE . " as t_n join " . TBL_NOTE_FILES . " as t_n_f on t_n.id = t_n_f.note_id WHERE t_n.school_id = '" . $school_id . "' and t_n_f.type = 4";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $files) {
                $file_url = $files['file_url'];
                $data = array('file_url' => $file_url);
                $insert_data = $this->my_custom_functions->insert_data($data, TBL_DELETE_FILES);

                $file_delete_condition = array('note_id' => $files['id'], 'type' => 4);
                $delete_file_data = $this->my_custom_functions->delete_data(TBL_NOTE_FILES, $file_delete_condition);
            }
        }
        $delete_condition = array('school_id' => $school_id);
        $delete_data = $this->my_custom_functions->delete_data(TBL_NOTICE, $delete_condition);
    }

    function delete_school_syllabus($school_id) {
         $sql = "SELECT t_s.id,t_n_f.file_url from " . TBL_SYLLABUS . " as t_s join " . TBL_NOTE_FILES . " as t_n_f on t_s.id = t_n_f.note_id WHERE t_s.school_id = '" . $school_id . "' and t_n_f.type = 5";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $files) {
                $file_url = $files['file_url'];
                $data = array('file_url' => $file_url);
                $insert_data = $this->my_custom_functions->insert_data($data, TBL_DELETE_FILES);

                $file_delete_condition = array('note_id' => $files['id'], 'type' => 5);
                $delete_file_data = $this->my_custom_functions->delete_data(TBL_NOTE_FILES, $file_delete_condition);
            }
        }
        $delete_condition = array('school_id' => $school_id);
        $delete_data = $this->my_custom_functions->delete_data(TBL_SYLLABUS, $delete_condition);
    }

    function get_note_for_delete() {
        $result = array();

        $today = date('Y-m-d') . ' 00:00:00';
        $sql = "SELECT id,school_id from " . TBL_NOTE . "  WHERE is_deleted = 1 and delete_time <='" . $today . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function get_note_files_for_delete($note_id) {
        $result = array();
        $sql = "select id,file_url from " . TBL_NOTE_FILES . " where note_id = '" . $note_id . "' and type IN(1,2,3)";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function delete_photos_and_files() {



        $sql = "SELECT * FROM " . TBL_DELETE_FILES . " ORDER BY id ASC LIMIT 0," . DEL_IMAGE_LIMITATION_FOR_CRON;
        $query = $this->db->query($sql);

        $deleteIds = '';
        if ($query->num_rows() > 0) {
            $s3 = S3Client::factory(array(
                        'version' => 'latest',
                        'region' => 'ap-south-1',
                        'credentials' => array(
                            'key' => AWS_KEY,
                            'secret' => AWS_SECRET,
                        ),
            ));

            $bucket = AMAZON_BUCKET;
            $multipleObjects = array();
            foreach ($query->result_array() as $val) {

                if ($val['file_url'] != "") {
                    $photoUrlArray = explode("/", $val['file_url']);
                    $awsKey = $photoUrlArray[count($photoUrlArray) - 2] . '/' . $photoUrlArray[count($photoUrlArray) - 1];
                    //echo $awsKey;echo "<br>";

                    $multipleObjects[] = array('Key' => $awsKey);
                } else {
                    $del_sql_row = "DELETE FROM " . TBL_DELETE_FILES . " WHERE id = '" . $val['id'] . "'";
                    $query_row = $this->db->query($del_sql_row);
                }


                $deleteIds .= $val['id'] . ',';
            }

            $deleteIds = rtrim($deleteIds, ',');
            //echo "<pre>";print_r($multipleObjects);die;
            if (!empty($multipleObjects)) {
                try {
                    $result = $s3->deleteObjects(array(
                        'Bucket' => $bucket,
                        'Delete' => array(// REQUIRED
                            'Objects' => $multipleObjects,
                            'Quiet' => true,
                        ),
                    ));

                    $del_sql = "DELETE FROM " . TBL_DELETE_FILES . " WHERE id IN(" . $deleteIds . ")";
                    $query = $this->db->query($del_sql);
                } catch (S3Exception $e) {
                    echo $e->getMessage() . "\n";
                }
            }
            echo 'Hello';
        }
    }

}
