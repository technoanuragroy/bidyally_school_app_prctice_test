<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    function get_classes_data() {

        $sql = "Select * from " . TBL_CLASSES . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $offset = 4; //the url segment that store the offset value
////pagination
//        $this->load->library('pagination');
//        $config['base_url'] = site_url() . $this->segment . '/manageClasses/';
//
//        $config['total_rows'] = $this->db->query($sql)->num_rows();
//        $config['per_page'] = $this->config->item('per_page');
//        $config['uri_segment'] = $offset;
//        $config['num_links'] = $this->config->item('num_link');
//        $config['next_link'] = 'Next';
//        $config['prev_link'] = 'Prev';
//        $config['first_link'] = 'First';
//        $config['last_link'] = 'Last';
//
//        $this->pagination->initialize($config);
//        if ($this->uri->segment($offset) == "") {
//            $offset = 0;
//        } else {
//            $offset = $this->uri->segment($offset);
//        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_section_data() {

        $sql = "Select * from " . TBL_SECTION . " where school_id = '" . $this->session->userdata('school_id') . "'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_section_data_search() {
        $sql = "Select * from " . TBL_SECTION . " where school_id = '" . $this->session->userdata('school_id') . "' and class_id = '".$this->input->post('class')."'";
        $query = $this->db->query($sql);
        return $query->result_array(); 
    }
    

    function get_subject_data() {

        $sql = "Select * from " . TBL_SUBJECT . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $offset = 4; //the url segment that store the offset value
////pagination
//        $this->load->library('pagination');
//        $config['base_url'] = site_url() . $this->segment . '/manageSubjects/';
//
//        $config['total_rows'] = $this->db->query($sql)->num_rows();
//        $config['per_page'] = $this->config->item('per_page');
//        $config['uri_segment'] = $offset;
//        $config['num_links'] = $this->config->item('num_link');
//        $config['next_link'] = 'Next';
//        $config['prev_link'] = 'Prev';
//        $config['first_link'] = 'First';
//        $config['last_link'] = 'Last';
//
//        $this->pagination->initialize($config);
//        if ($this->uri->segment($offset) == "") {
//            $offset = 0;
//        } else {
//            $offset = $this->uri->segment($offset);
//        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_teacher_data() {
        $sql = "Select * from " . TBL_TEACHER . " where school_id = '" . $this->session->userdata('school_id') . "'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_time_table_data($class_id, $section_id) {
        $create_data = array();
        $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
        $query = $this->db->query($sql);
        $result = $query->result_array();

        $day_list = $this->config->item('days_list');
//$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

        foreach ($day_list as $day => $val) {
//foreach ($period_list as $ress) {
//$create_data[$day][$ress['id']] = '';
            $counter = 0;
            foreach ($result as $routine) {
                if ($day == $routine['day_id']) {
                    $create_data[$day][$counter] = $routine;
                }

                $counter++;
            }
//}
        }
//echo '<pre>';print_r($create_data);die;
        return $create_data;
    }

    function get_period_data() {

        $sql = "Select * from " . TBL_PERIODS . " where school_id = '" . $this->session->userdata('school_id') . "'";
//        $offset = 4; //the url segment that store the offset value
////pagination
//        $this->load->library('pagination');
//        $config['base_url'] = site_url() . $this->segment . '/managePeriods/';
//
//        $config['total_rows'] = $this->db->query($sql)->num_rows();
//        $config['per_page'] = $this->config->item('per_page');
//        $config['uri_segment'] = $offset;
//        $config['num_links'] = $this->config->item('num_link');
//        $config['next_link'] = 'Next';
//        $config['prev_link'] = 'Prev';
//        $config['first_link'] = 'First';
//        $config['last_link'] = 'Last';
//
//        $this->pagination->initialize($config);
//        if ($this->uri->segment($offset) == "") {
//            $offset = 0;
//        } else {
//            $offset = $this->uri->segment($offset);
//        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function create_school($admin_id) {


        $update_data = array(
            "name" => $this->input->post("name"),
            "phone" => $this->input->post("phone"),
            "status" => $this->input->post("status"),
            "email" => $this->input->post("email"),
        );


        $this->db->where('admin_id', $admin_id);
        $return = $this->db->update(TBL_ADMIN, $update_data);

        return $return;
    }

    function get_student_detail($student_id) {
        $parents_list = '';
        $sql = 'Select t_s.*,t_s_d.* from ' . TBL_STUDENT . ' t_s join ' . TBL_STUDENT_DETAIL . ' t_s_d on t_s.id = t_s_d.student_id where t_s.id = ' . $student_id . '';
        $query = $this->db->query($sql);

        $res = $query->result_array();


        $sql1 = "Select parent_id from " . TBL_PARENT_KIDS_LINK . " where student_id = " . $student_id . "";
        $query1 = $this->db->query($sql1);
        $res1 = $query1->result_array();

        foreach ($res1 as $ress1) {
            $parents_list .= $ress1['parent_id'] . ',';
        }

        $parents = rtrim($parents_list, ',');


        $sql2 = "Select id,username from " . TBL_COMMON_LOGIN . " where id IN ( " . $parents . " ) ";
        $query2 = $this->db->query($sql2);
        $res2 = $query2->result_array();



        $data['student_detail'] = $res;
        $data['username_detail'] = $res2;
//echo "<pre>";print_r($data);
        return $data;
    }

    function get_class_list($school_id) {
        $sql = "SELECT * FROM " . TBL_CLASSES . " WHERE school_id = '" . $school_id . "'";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        return $data;
    }

    function update_employee_photo($teacher_id, $photo_url) {
        $update_data = array(
            "file_url" => $photo_url
        );

        $this->db->where('teacher_id', $teacher_id);
        $return = $this->db->update(TBL_TEACHER_FILES, $update_data);
    }

    function get_previous_week_data() {
        $res = array();
        $attendance_count = 0;
        $final_rec = '';


        $week_start = date('Y-m-d', strtotime('monday last week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));
        //die;
//echo $start_week . ' // ' . $end_week;die;
        $sql = "SELECT * from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_date>='" . $week_start . "' and attendance_date<='" . $week_end . "'  order by attendance_date ASC";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }

        foreach ($data as $row) {
            if ($row['attendance_status'] == 1) {
                //$attendance_count++;

                $res[$row['attendance_date']] += 1;
            }
        }


        foreach ($res as $ress) {
            $final_rec .= $ress . ',';
        }
        $records = rtrim($final_rec, ',');
        return $records;
    }

    function get_current_week_data() {
        $final_rec = '';
        $day = date('w');
        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));
        $sql = "SELECT * from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_date>='" . $week_start . "' and attendance_date<='" . $week_end . "'  order by attendance_date ASC";
        $query = $this->db->query($sql);
        $data = $query->result_array();

        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }


        foreach ($data as $row) {
            if ($row['attendance_status'] == 1) {
                //$attendance_count++;

                $res[$row['attendance_date']] += 1;
            }
        }
        foreach ($res as $ress) {
            $final_rec .= $ress . ',';
        }
        $records = rtrim($final_rec, ',');
        return $records;
    }

    function get_attendance_count() {
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');
        $sql = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_status = 1 and attendance_date>='" . $first_day_this_month . "' and attendance_date <='" . $last_day_this_month . "'";
        $query = $this->db->query($sql);
        $data = $query->result_array();

        foreach ($data as $present_month_attendance) {
            $row['present_month_attendance'] = $present_month_attendance['count(attendance_status)'];
        }

        $first_day_last_month = date('Y-m-d', strtotime('first day of last month'));
        $last_day_last_month = date('Y-m-d', strtotime('last day of last month'));
        $sql1 = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and  attendance_status = 1 and attendance_date>='" . $first_day_last_month . "' and attendance_date <='" . $last_day_last_month . "'";
        $query1 = $this->db->query($sql1);
        $data1 = $query1->result_array();

        foreach ($data1 as $prev_month_attendance) {
            $row['prev_month_attendance'] = $prev_month_attendance['count(attendance_status)'];
        }
        $diff = abs($row['present_month_attendance'] - $row['prev_month_attendance']);


        @$avg_atten = ($diff / $row['present_month_attendance']) * 100;
        $row['avg_attendance'] = $avg_atten;


        if ($row['present_month_attendance'] > $row['prev_month_attendance']) {
            $row['month_flag'] = 1;  /// Upgoing 
        } else {
            $row['month_flag'] = 0;  /// Downgoing  
        }

        ///////////////////////////////////////  WEEKLY ATTENDANCE COUNT  ////////////////////////////////

        $day = date('w');
        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));
        $sql2 = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_status = 1 and attendance_date>='" . $week_start . "' and attendance_date <='" . $week_end . "'";
        $query2 = $this->db->query($sql2);
        $data2 = $query2->result_array();

        foreach ($data2 as $present_week_attendance) {
            $row['present_week_attendance'] = $present_week_attendance['count(attendance_status)'];
        }

        // echo "<pre>";print_r($row);die;
        $today = date('Y-m-d');
        $sql3 = "select count(attendance_status) from " . TBL_ATTENDANCE . " where school_id = '" . $this->session->userdata('school_id') . "' and attendance_status = 1 and attendance_date>='" . $today . "' and attendance_date <='" . $today . "'";
        $query3 = $this->db->query($sql3);
        $data3 = $query3->result_array();

        foreach ($data3 as $todays_attendance) {
            $row['todays_attendance'] = $todays_attendance['count(attendance_status)'];
        }


        return $row;
    }

    function get_previous_week_activity_data() {
        $data_class_works = array();
        $data_home_works = array();
        $data_assignment_works = array();
        $prev_week_data_list = '';

        $week_start = date('Y-m-d', strtotime('monday last week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

        $start_previous_week = strtotime($week_start);
        $end_previous_week = strtotime($week_end);


        $sql = "SELECT * from " . TBL_NOTE . " where type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_previous_week . "' and note_issuetime <= '" . $end_previous_week . "'";
        $query = $this->db->query($sql);
        //$data_class_works = $query->num_rows();
        if ($query->num_rows() > 0) {
            $data_class_works = $query->result_array();
        }

        $sql1 = "SELECT * from " . TBL_NOTE . " where type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_previous_week . "' and note_issuetime <= '" . $end_previous_week . "'";
        $query1 = $this->db->query($sql1);
        //$data_home_works = $query1->num_rows();
        if ($query1->num_rows() > 0) {
            $data_home_works = $query1->result_array();
        }

        $sql2 = "SELECT * from " . TBL_NOTE . " where type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_previous_week . "' and note_issuetime <= '" . $end_previous_week . "'";
        $query2 = $this->db->query($sql2);
        //$data_assignment_works = $query2->num_rows();
        if ($query2->num_rows() > 0) {
            $data_assignment_works = $query2->result_array();
        }
        $total_activities_list = array_merge($data_class_works, $data_home_works, $data_assignment_works);

        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }
        foreach ($total_activities_list as $row) {
            $activity_date = date('Y-m-d', $row['note_issuetime']);
            $res[$activity_date] += 1;
        }
        foreach ($res as $prev_week_data) {
            $prev_week_data_list .= $prev_week_data . ',';
        }

        $prev_week_final_data_list = rtrim($prev_week_data_list, ',');
        return $prev_week_final_data_list;
    }

    function get_current_week_activity_data() {
        $data_class_works = array();
        $data_home_works = array();
        $data_assignment_works = array();
        $current_week_data_list = '';

        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

        $start_current_week = strtotime($week_start);
        $end_current_week = strtotime($week_end);


        $sql = "SELECT * from " . TBL_NOTE . " where type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query = $this->db->query($sql);
        //$data_class_works = $query->num_rows();
        if ($query->num_rows() > 0) {
            $data_class_works = $query->result_array();
        }

        $sql1 = "SELECT * from " . TBL_NOTE . " where type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query1 = $this->db->query($sql1);
        //$data_home_works = $query1->num_rows();
        if ($query1->num_rows() > 0) {
            $data_home_works = $query1->result_array();
        }

        $sql2 = "SELECT * from " . TBL_NOTE . " where type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query2 = $this->db->query($sql2);
        //$data_assignment_works = $query2->num_rows();
        if ($query2->num_rows() > 0) {
            $data_assignment_works = $query2->result_array();
        }
        $total_activities_list = array_merge($data_class_works, $data_home_works, $data_assignment_works);

        for ($i = $week_start; $i <= $week_end; $i++) {
            $res[$i] = 0;
        }
        foreach ($total_activities_list as $row) {
            $activity_date = date('Y-m-d', $row['note_issuetime']);
            $res[$activity_date] += 1;
        }

        foreach ($res as $current_week_data) {
            $current_week_data_list .= $current_week_data . ',';
        }

        $current_week_final_data_list = rtrim($current_week_data_list, ',');
        return $current_week_final_data_list;
    }

    function get_activity_count() {
        $data_class_works_a = 0;
        $data_home_works_b = 0;
        $data_assignment_works_c = 0;
        $data_class_works_d = 0;
        $data_home_works_e = 0;
        $data_assignment_works_f = 0;
        $data_class_works_g = 0;
        $data_home_works_h = 0;
        $data_assignment_works_i = 0;
        $record = array();
        $total_activities_list = 0;

        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');

        $start_current_month = strtotime($first_day_this_month);
        $end_current_month = strtotime($last_day_this_month);


        $sql = "SELECT * from " . TBL_NOTE . " where type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_month . "' and note_issuetime <= '" . $end_current_month . "'";
        $query = $this->db->query($sql);
        //$data_class_works = $query->num_rows();
        ///echo $query->num_rows();die;
        if ($query->num_rows() > 0) {
            $data_class_works_a = $query->num_rows();
        }

        $sql1 = "SELECT * from " . TBL_NOTE . " where type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_month . "' and note_issuetime <= '" . $end_current_month . "'";
        $query1 = $this->db->query($sql1);
        //$data_home_works = $query1->num_rows();
        if ($query1->num_rows() > 0) {
            $data_home_works_b = $query1->num_rows();
        }

        $sql2 = "SELECT * from " . TBL_NOTE . " where type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_month . "' and note_issuetime <= '" . $end_current_month . "'";
        $query2 = $this->db->query($sql2);
        //$data_assignment_works = $query2->num_rows();
        if ($query2->num_rows() > 0) {
            $data_assignment_works_c = $query2->num_rows();
        }


        $total_activities_list_month = $data_class_works_a + $data_home_works_b + $data_assignment_works_c;




        $week_start = date('Y-m-d', strtotime('monday this week'));
        $week_end = date('Y-m-d', strtotime($week_start . '+6 days'));

        $start_current_week = strtotime($week_start);
        $end_current_week = strtotime($week_end);


        $sql3 = "SELECT * from " . TBL_NOTE . " where type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query3 = $this->db->query($sql3);
        //$data_class_works = $query->num_rows();
        if ($query3->num_rows() > 0) {
            $data_class_works_d = $query3->num_rows();
        }

        $sql4 = "SELECT * from " . TBL_NOTE . " where type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query4 = $this->db->query($sql4);
        //$data_home_works = $query1->num_rows();
        if ($query4->num_rows() > 0) {
            $data_home_works_e = $query4->num_rows();
        }

        $sql5 = "SELECT * from " . TBL_NOTE . " where type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_week . "' and note_issuetime <= '" . $end_current_week . "'";
        $query5 = $this->db->query($sql5);
        //$data_assignment_works = $query2->num_rows();
        if ($query5->num_rows() > 0) {
            $data_assignment_works_f = $query5->num_rows();
        }
        $total_activities_list_current_week = $data_class_works_d + $data_home_works_e + $data_assignment_works_f;




        $day_start = date('Y-m-d 00:00:00');
        $day_end = date('Y-m-d 23:59:59');

        $start_current_day = strtotime($day_start);
        $end_current_day = strtotime($day_end);


        $sql6 = "SELECT * from " . TBL_NOTE . " where type = 1 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_day . "' and note_issuetime <= '" . $end_current_day . "'";
        $query6 = $this->db->query($sql6);
        //$data_class_works = $query->num_rows();
        if ($query6->num_rows() > 0) {
            $data_class_works_g = $query6->num_rows();
        }

        $sql7 = "SELECT * from " . TBL_NOTE . " where type = 2 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_day . "' and note_issuetime <= '" . $end_current_day . "'";
        $query7 = $this->db->query($sql7);
        //$data_home_works = $query1->num_rows();
        if ($query7->num_rows() > 0) {
            $data_home_works_h = $query7->num_rows();
        }

        $sql8 = "SELECT * from " . TBL_NOTE . " where type = 3 and school_id = '" . $this->session->userdata('school_id') . "' and note_issuetime >= '" . $start_current_day . "' and note_issuetime <= '" . $end_current_day . "'";
        $query8 = $this->db->query($sql8);
        //$data_assignment_works = $query2->num_rows();
        if ($query8->num_rows() > 0) {
            $data_assignment_works_i = $query8->num_rows();
        }

        $total_activities_list_current_day = $data_class_works_g + $data_home_works_h + $data_assignment_works_i;


        $record['month_activity_list'] = $total_activities_list_month;
        $record['week_activity_list'] = $total_activities_list_current_week;
        $record['day_activity_list'] = $total_activities_list_current_day;

        return $record;
    }

    function search_student() {

        $data = array();
        $search = '';
        if ($this->input->post('name') != '') {
            $search .= ' and name like "%' . $this->input->post('name') . '%"';
        }
        if ($this->input->post('rollno') != '') {
            $search .= ' and roll_no = "' . $this->input->post('rollno') . '"';
        }
        if ($this->input->post('class') != '') {
            $search .= ' and class_id = "' . $this->input->post('class') . '"';
        }
        if ($this->input->post('section') != '') {
            $search .= ' and section_id = "' . $this->input->post('section') . '"';
        }

        $sql = "SELECT * FROM " . TBL_STUDENT . " WHERE school_id = '" . $this->session->userdata('school_id') . "'" . $search . "";
        $query = $this->db->query($sql);
        //$data_home_works = $query1->num_rows();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        return $data;
    }

    function get_permissions() {

        $query = $this->db->query("SELECT * FROM " . TBL_PERMISSION . " ORDER BY id ASC");

        if ($query->num_rows() > 0) {

            return $query->result_array();
        }
    }

    function get_login_details($id) {
        $this->db->where('id', $id);
        $query = $this->db->get(TBL_TEACHER);
        return $query->row_array();
    }

    function get_existing_permission($uid, $pid) {

        $query = $this->db->query("SELECT * FROM " . TBL_USER_PERMISSION . " WHERE uid ='$uid' AND permission = '$pid'");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function remove_permission($uid) {

        $this->db->where('uid', $uid);
        $this->db->delete(TBL_USER_PERMISSION);
    }

    function create_permission($data, $user) {
        foreach ($data as $permission => $user_type) {

            $data1 = array(
                'permission' => $permission,
                'uid' => $user,
                'allowed_type' => $user_type
            );

            $this->db->insert(TBL_USER_PERMISSION, $data1);
        }

        return $this->db->insert_id();
    }

    function get_method_permission($fname) {
        $data = array();
        $query = $this->db->query("SELECT * FROM " . TBL_PERMISSION . " WHERE page_urls REGEXP '[[:<:]]" . $fname . "[[:>:]]' AND status=1");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    function get_user_permission($pid, $uid) {
        $data = array();
        $query = $this->db->query("SELECT * FROM " . TBL_USER_PERMISSION . " WHERE uid='$uid' AND permission='$pid' AND allowed_type=1");

        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    function get_attendance_report_classwise() {

        $extra = '';
        $start_date = $this->my_custom_functions->database_date_dash($this->input->post('start_date'));
        $end_date = $this->my_custom_functions->database_date_dash($this->input->post('end_date'));

        $section_list = array();
        $section_wise_student_count = array();
        if ($this->input->post('section') && $this->input->post('section') != '') {
            $section_list[$this->input->post('class') . '_' . $this->input->post('section')] = $this->input->post('class') . '_' . $this->input->post('section');
        } else {
            $all_sections = $this->my_custom_functions->get_multiple_data(TBL_SECTION, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" ');
            if (!empty($all_sections)) {
                foreach ($all_sections as $section) {
                    $section_list[$section['class_id'] . '_' . $section['id']] = $section['class_id'] . '_' . $section['id'];
                }
            }
        }

        foreach ($section_list as $section) {
            $class_section_array = explode("_", $section);
            $section_wise_student_count[$class_section_array[0] . "_" . $class_section_array[1]] = $this->my_custom_functions->get_perticular_count(TBL_STUDENT, 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_section_array[0] . '" and section_id = "' . $class_section_array[1] . '"');
        }

        if ($this->input->post('section') && $this->input->post('section') != '') {
            $extra .= ' and section_id = "' . $this->input->post('section') . '"';
        }
        $sql = 'SELECT * FROM ' . TBL_ATTENDANCE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $this->input->post('class') . '" and attendance_date >="' . $start_date . '" and attendance_date <="' . $end_date . '"' . $extra . '';
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {

            $attendance_count = array();
            $attendance_percentage = array();

            foreach ($query->result_array() as $row) { //echo "<pre>";print_r($row);
                if ($row['attendance_status'] == 1) {
                    @$attendance_count[$row['attendance_date']][$row['class_id'] . '_' . $row['section_id']] += 1;
                }

                if (!empty($attendance_count)) {
                    foreach ($attendance_count as $attendance_date => $attendance) {
                        foreach ($attendance as $class_section_id => $count) {
                            $percentage = ($count / $section_wise_student_count[$class_section_id]) * 100;
                            $attendance_percentage[$row['attendance_date']][$row['class_id'] . '_' . $row['section_id']] = $percentage;
                        }
                    }
                }
            }

//            echo "<pre>";
//            print_r($section_list); 
//            print_r($attendance_percentage); 
//            die;

            return array(
                'section_list' => $section_list,
                'attendance' => $attendance_percentage
            );
        }
    }

    function get_attendance_report_studentwise() {
        //echo '<pre>';print_r($_POST);die;
        $data = array();
        $data_a = array();
        $data_b = array();
        $school_id = $this->session->userdata('school_id');
        $search = '';
        if ($this->input->post('class')) {
            $search .= ' and t_a.class_id = ' . $this->input->post('class') . '';
        }
        if ($this->input->post('section')) {
            $search .= ' and t_a.section_id = ' . $this->input->post('section') . '';
        }
//        if ($this->input->post('roll_no')) {
//            $search .= ' and t_s.roll_no = ' . $this->input->post('roll_no') . '';
//        }
//        if ($this->input->post('student_name')) {
//            $search .= ' and t_s.name like "%' . $this->input->post('student_name') . '%"';
//        }
        if ($this->input->post('start_date')) {
            $search .= ' and t_a.attendance_date >= "' . $this->my_custom_functions->database_date_dash($this->input->post('start_date')) . '"';
            $start_date = array('att_start_date' => $this->my_custom_functions->database_date_dash($this->input->post('start_date')));
            $this->session->set_userdata($start_date);
        } else {
            $start_date = array('att_start_date');
            $this->session->unset_userdata($start_date);
        }

        if ($this->input->post('end_date')) {
            $search .= ' and t_a.attendance_date <= "' . $this->my_custom_functions->database_date_dash($this->input->post('end_date')) . '"';
            $end_date = array('att_end_date' => $this->my_custom_functions->database_date_dash($this->input->post('end_date')));
            $this->session->set_userdata($end_date);
        } else {
            $end_date = array('att_end_date');
            $this->session->unset_userdata($end_date);
        }

        $sql = "SELECT t_a.*,t_s.name FROM " . TBL_ATTENDANCE . " t_a JOIN " . TBL_STUDENT . " t_s on t_a.student_id = t_s.id  WHERE t_a.school_id = " . $school_id . "  " . $search;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $count = 0;
            foreach ($result as $row) {//echo "<pre>";print_r($row);
                $data[$row['student_id']][$count] = $row;

                $count++;
            }
            //echo "<pre>";print_r($data);
            foreach ($data as $student_id => $attendance_data_set) {
                $present = 0;
                $class = 0;
                foreach ($attendance_data_set as $attend) {
                    $class++;
                    if ($attend['attendance_status'] == PRESENT) {
                        $present++;
                    }
                    $data_a[$student_id]['total_present'] = $present;
                    $data_a[$student_id]['total_class'] = $class;
                }
            }
            foreach ($data_a as $student_id => $rec) {
                $avg = $rec['total_present'] / $rec['total_class'] * 100;
                $data_b[$student_id]['total_present'] = $rec['total_present'];
                $data_b[$student_id]['total_class'] = $rec['total_class'];
                $data_b[$student_id]['percentage'] = $avg;
            }
            //echo "<pre>";print_r($data_b);die;
        }
        return $data_b;
    }

    function get_attendance_detail($student_id) {
        $start_date = '';
        $end_date = '';
        $search = '';
        if ($this->session->userdata('att_start_date') && $this->session->userdata('att_start_date') != '') {
            $start_date = $this->session->userdata('att_start_date');
            $search .= ' and attendance_date >="' . $start_date . '"';
        }
        if ($this->session->userdata('att_end_date') && $this->session->userdata('att_end_date') != '') {
            $end_date = $this->session->userdata('att_end_date');
            $search .= ' and attendance_date <="' . $end_date . '"';
        }
        $school_id = $this->session->userdata('school_id');

        $return_data = array();
        $count = 0;

        $sql = "SELECT * FROM " . TBL_ATTENDANCE . " WHERE school_id = '" . $school_id . "' and student_id = '" . $student_id . "' " . $search . " ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            foreach ($result as $res) {
                $data[$res['attendance_date']][] = $res;
            }
            foreach ($data as $attendance_date => $attendance_data) {
                $total_absent = 0;
                $total_present = 0;
                $total_attendance = count($attendance_data);
                $explode_date = explode('-', $attendance_date);
                $day = abs($explode_date[2]);
                $month = abs($explode_date[1]);
                $year = abs($explode_date[0]);
                foreach ($attendance_data as $atten_data) {
                    if ($atten_data['attendance_status'] == PRESENT) {
                        $total_present++;
                    }
                    if ($atten_data['attendance_status'] == ABSENT) {
                        $total_absent++;
                    }
                }
                if ($total_attendance == $total_present) {
                    //$class = 'present_class';
                    $class = 1;
                } else if ($total_attendance == $total_absent) {
                    //$class = 'absent_class';
                    $class = 2;
                } else {
                    //$class = 'pertial_present_class';
                    $class = 3;
                }
                $return_data[$year][$month][$day]['attendance_class'] = $class;
                $return_data[$year][$month][$day]['attendance_date'] = $attendance_date;
                $count++;
            }
        }

        //echo "<pre>";print_r($return_data);die;
        return $return_data;
    }

    function get_teacher_activity_report() {
        $class_id = $this->input->post('class');
        $section_id = $this->input->post('section');
        $start_date = strtotime($this->input->post('start_date'));
        $end_date = strtotime($this->input->post('end_date'));

        $beginOfDay = strtotime($this->input->post('start_date').' 00:00:00');
        $endOfDay = strtotime($this->input->post('end_date').' 23:59:59');

        $day_array = array();
        $subject_array = array();
        $main_data = array();
        for ($current = date("Y-m-d", $start_date); $current <= date("Y-m-d", $end_date); $current = date("Y-m-d", strtotime("+1 days", strtotime($current)))) {
            $day = date('N', strtotime($current));
            @$day_array[$day] += 1;
        }
        //echo "<pre>";print_r($day_array);


        foreach ($day_array as $day => $day_count) {
            //echo $no_of_classes_per_day = $this->my_custom_functions->get_perticular_count(TBL_TIMETABLE,'and school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and day_id = "'.$day.'"');
            //$sql = 'SELECT * FROM '.TBL_TIMETABLE.' WHERE school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and day_id = "'.$day.'"';

            $sql = 'SELECT *, COUNT(id) AS subject_count FROM ' . TBL_TIMETABLE . ' WHERE school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and section_id = "' . $section_id . '" and day_id = "' . $day . '" and subject_id != 0 GROUP BY subject_id';
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $query_data = $query->result_array();
                foreach ($query_data as $row) {
                    $subject_array['total_data'][$row['subject_id']]['subject_id'] = $row['subject_id'];
                    $subject_array['total_data'][$row['subject_id']]['subject_count'] = $row['subject_count'] * $day_count;
                }
            }
        }
        foreach ($subject_array as $res) {
            foreach ($res as $class_id => $rec) {
                $class_work = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and type = 1 and school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and note_issuetime >= "'.$beginOfDay.'" and note_issuetime <= "'.$endOfDay.'" and publish_data = 1');
                $home_work = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and type = 2 and school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and note_issuetime >= "'.$beginOfDay.'" and note_issuetime <= "'.$endOfDay.'" and publish_data = 1');
                $assignment_work = $this->my_custom_functions->get_perticular_count(TBL_NOTE, 'and type = 3 and school_id = "'.$this->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and note_issuetime >= "'.$beginOfDay.'" and note_issuetime <= "'.$endOfDay.'" and publish_data = 1');
                $total_note = $class_work + $home_work + $assignment_work;
                $main_data['activity'][$rec['subject_id']]['subject_count'] = $rec['subject_count'];
                $main_data['activity'][$rec['subject_id']]['total_activity'] = $total_note;
            }
        }
        //echo "<pre>";print_r($main_data);die;
        return $main_data;
    }
    
    function get_student_list() {
        //echo "<pre>";print_r($_POST);die;
        $query_data = array();
        $class_id = $this->input->post('class');
        $section_id = $this->input->post('section');
        $sql = "SELECT * FROM ".TBL_STUDENT." WHERE school_id = '".$this->session->userdata('school_id')."' and class_id = '".$class_id."' and section_id = '".$section_id."'";
        $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $query_data = $query->result_array();
            }
            return $query_data;
    }

}
