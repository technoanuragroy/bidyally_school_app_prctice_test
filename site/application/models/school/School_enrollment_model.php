<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class School_enrollment_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->segment = $this->uri->segment(1) . '/' . $this->uri->segment(2);
    }

    ////////////////////////////////////////////////////////////////////////////
    
    function enroll_students() {
               
        $student_list = $this->input->post('student');
        $roll_numbers = $this->input->post('roll_no');

        if(!empty($student_list)) {
            foreach ($student_list as $key => $student_id) {

                $semester_id = $this->input->post('semester_id_move');
                $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id="' . $semester_id . '" ');
                $section_id = $this->input->post('section_id_move');
                $roll_no = '';

                if(array_key_exists($key, $roll_numbers)) {
                    $roll_no = $roll_numbers[$key];
                }

                $sql = 'SELECT * FROM ' . TBL_STUDENT_ENROLLMENT . ' WHERE student_id="'.$student_id.'" AND semester_id="'.$semester_id.'"';
                $query = $this->db->query($sql);

                // If the student is already enrolled in the semester, update its section and roll number
                if($query->num_rows() > 0) {
                    $update_data = array(
                        "section_id" => $section_id,
                        "roll_no" => $roll_no,
                        "enrollment_time" => time()
                    );

                    $this->db->where(array("student_id" => $student_id, "semester_id" => $semester_id));
                    $this->db->update(TBL_STUDENT_ENROLLMENT, $update_data);
                } 
                // Insert new enrollment
                else {
                    $insert_data = array(
                        "school_id" => $this->session->userdata('school_id'),
                        "student_id" => $student_id, 
                        "class_id" => $class_id,
                        "semester_id" => $semester_id,
                        "section_id" => $section_id,
                        "roll_no" => $roll_no,
                        "enrollment_time" => time()
                    );

                    $this->db->insert(TBL_STUDENT_ENROLLMENT, $insert_data);
                }                                
            }
        }
        
        return true;
    }
}
