<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class School_main_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function login_check() {

        $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
        $sql = "SELECT * FROM " . TBL_COMMON_LOGIN . " WHERE username='" . $username . "' AND (type=".SCHOOL." OR type=".TEACHER.") AND status=1";
        $query = $this->db->query($sql);

        $password = $this->input->post('password');
        $record = array();
        if ($query->num_rows() > 0) {

            $today = strtotime(date("Y-m-d"));
            $record = $query->row_array();

            if (password_verify($password, $record['password'])) {
                
                if ($record['type'] == SCHOOL) {
                    
                    $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $record['id'] . '"');

                    if ($school_delete_check == 0) {
                        
                        $school_detail = $this->my_custom_functions->get_details_from_id($record["id"], TBL_SCHOOL);
                        
                        $session_selected = "";
                        $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_school($record["id"]);
                        
                        if(!empty($school_sessions)) {
                            foreach($school_sessions as $school_ses) {
                                if(strtotime($school_ses['from_date']) <= $today AND strtotime($school_ses['to_date']) >= $today) {
                                    $session_selected = $school_ses['id'];
                                }
                            }
                            if($session_selected == "") {
                                $session_selected = $school_sessions[0]['id'];
                            }
                        }
                        
                        $session_data = array(
                            "usertype" => $record['type'],
                            "school_id" => $record["id"],
                            "session_id" => $session_selected,
                            "school_username" => $record["username"],
                            "school_email" => $school_detail["email_address"],
                            "school_is_logged_in" => 1
                        );
                        $this->session->set_userdata($session_data);
                        
                        // Update last login data
                        $last_login_data = array(
                            "last_login_time" => time()                            
                        );
                        $this->db->where('id', $record["id"]);
                        $updated = $this->db->update(TBL_COMMON_LOGIN, $last_login_data);
                        
                        return 1;
                    } else {
                        return 0;
                    }
                } else if ($record['type'] == TEACHER) {

                    $school_id = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'school_id', 'and id = "' . $record["id"] . '"');
                    $school_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_SCHOOL, 'is_deleted', 'and id = "' . $school_id . '"');

                    if ($school_delete_check == 0) {
                        $teacher_delete_check = $this->my_custom_functions->get_particular_field_value(TBL_TEACHER, 'is_deleted', 'and id = "' . $record["id"] . '"');
                        if ($teacher_delete_check == 0) {
                            
                            $session_selected = "";
                            $school_sessions = $this->my_custom_functions->get_all_sessions_of_a_school($school_id);
                            
                            if(!empty($school_sessions)) {
                                foreach($school_sessions as $school_ses) {
                                    if(strtotime($school_ses['from_date']) <= $today AND strtotime($school_ses['to_date']) >= $today) {
                                        $session_selected = $school_ses['id'];
                                    }
                                }
                                if($session_selected == "") {
                                    $session_selected = $school_sessions[0]['id'];
                                }
                            }
                            
                            $session_data = array(
                                "usertype" => $record['type'],
                                "school_id" => $school_id,
                                "session_id" => $session_selected,
                                "teacher_id" => $record["id"],
                                "school_username" => $record["username"],
                                "school_is_logged_in" => 1
                            );
                            $this->session->set_userdata($session_data);
                            
                            // Update last login data
                            $last_login_data = array(
                                "last_login_time" => time()                            
                            );
                            $this->db->where('id', $record["id"]);
                            $updated = $this->db->update(TBL_COMMON_LOGIN, $last_login_data);
                            
                            return 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///// CREATE NEW TOKEN
    ////////////////////////
    function create_new_token($UserID) {

        $login_ID = "";
        $type = "";
        $token = "";

        if ($UserID != "") {
            $login_ID = $UserID;
            $type = 2; // 2 = Forget password
        }
//        else {
//            $login_ID = $this->session->userdata('company_id');
//            $type = 1; // 1 = Email verification
//        }

        $time = date('Y-m-d H:i:s');
        $sql = "SELECT * FROM " . TBL_VERIFICATION_TOKEN . " WHERE login_id=" . $login_ID . " AND expiry_time<'" . $time . "'";
        $query = $this->db->query($sql);

        // If there exists an expired token, delete it, create a new token
        if ($query->num_rows() > 0) {

            $this->db->where('login_id', $login_ID);
            $delete = $this->db->delete(TBL_VERIFICATION_TOKEN);

            // Create new verification token
            $token = $this->my_custom_functions->get_verification_token_code();
            $expiry_duration = '+' . VERIFICATION_EXPIRY_TIME . ' minutes';
            $varification_data = array(
                'login_id' => $login_ID,
                'token' => $token,
                'expiry_time' => date("Y-m-d H:i:s", strtotime($expiry_duration)),
                'type' => $type
            );
            $this->db->insert(TBL_VERIFICATION_TOKEN, $varification_data);
        }
        // Create a new token if there is no token
        else {

            $sql = "SELECT * FROM " . TBL_VERIFICATION_TOKEN . " WHERE login_id=" . $login_ID;
            $query = $this->db->query($sql);

            if ($query->num_rows() == 0) {

                // Create new verification token
                $token = $this->my_custom_functions->get_verification_token_code();
                $expiry_duration = '+' . VERIFICATION_EXPIRY_TIME . ' minutes';
                $varification_data = array(
                    'login_id' => $login_ID,
                    'token' => $token,
                    'expiry_time' => date("Y-m-d H:i:s", strtotime($expiry_duration)),
                    'type' => $type
                );
                $this->db->insert(TBL_VERIFICATION_TOKEN, $varification_data);
            }
        }

        return true;
    }

    function delete_verification_token($token, $type) {

        $where = array(
            'token' => $token,
            'type' => $type
        );

        $this->db->where($where);
        $delete = $this->db->delete('tbl_verification_token');

        return $delete;
    }

}
