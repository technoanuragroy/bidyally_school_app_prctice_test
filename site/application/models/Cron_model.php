<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_file_list() {
        $data = array();
        $sql = "SELECT * FROM " . TBL_DELETE_FILES . "";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }

        return $data;
    }
    
    function get_all_school_list(){
        $data = array();
        $sql = "SELECT id,name,contact_person_name,email_address,created_at from ".TBL_SCHOOL." WHERE license_expiry_date >='".date('Y-m-d')."' and is_deleted = 0";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        
        return $data;
    }
    function get_all_school_list_data(){
        $data = array();
        $sql = "SELECT id,name,contact_person_name,email_address,created_at,license_expiry_date from ".TBL_SCHOOL." WHERE license_expiry_date >'".date('Y-m-d')."' and is_deleted = 0";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }
        
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_records_for_due_reminder() {
        
        // Get the schools which have set the reminder schedules and also have the fees payment enabled
        $sql = 'SELECT tdrs.* '
             . 'FROM '.TBL_DUE_REMINDER_SCHEDULES.' tdrs INNER JOIN '.TBL_SCHOOL_PAYMENT_SETTINGS.' tsps '
             . 'ON tdrs.school_id=tsps.school_id '
             . 'WHERE tsps.payment_enabled=1';            
        $query = $this->db->query($sql);
        
        $data = array();
        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                                                                          
                if($row['reminder_schedule_type'] == 1) { // Before due date reminder
                    $due_date = date("Y-m-d", strtotime("+ ".$row['days']. " days"));
                } else if($row['reminder_schedule_type'] == 2) { // After due date reminder
                    $due_date = date("Y-m-d", strtotime("- ".$row['days']. " days"));
                }            
                        
                // Get the fees breakups which have a due date matched to the due date calculated
                $sql1 = "SELECT t_f_s_b.*, t_f_s.semester_id "
                       ."FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " t_f_s_b JOIN " . TBL_FEES_STRUCTURE . " t_f_s "
                       ."ON t_f_s_b.fees_id=t_f_s.id "
                       ."WHERE t_f_s_b.month='" . $due_date . "' AND t_f_s.school_id=" . $row['school_id'];                
                $query1 = $this->db->query($sql1);

                $fees_breakups = array();
                if ($query1->num_rows() > 0) {
                    foreach($query1->result_array() as $row1) {

                        $fees_breakups[$row1['semester_id']][] = $row1;                              
                    }            
                }
                        
                if(!empty($fees_breakups)) {
                    foreach($fees_breakups as $semester_id => $breakups) {

                        // Get student list         
                        $student_list = $this->my_custom_functions->get_student_list(array("semester_id" => $semester_id));

                        // Due records               
                        if(!empty($student_list)) {     
                            foreach($student_list as $student) {

                                $class_info = $this->my_custom_functions->get_class_info_json_for_student($student['id'], $semester_id);

                                foreach($breakups as $breakup) {                                                                         

                                    // If this fee is already paid
                                    $is_paid = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and school_id="' . $row['school_id'] . '" and student_id="' . $student['id'] . '" and fees_breakup_id="' . $breakup['id'] . '"');

                                    if($is_paid == 0) {

                                        $feesJson = $breakup['fees'];

                                        // Check if any manual fees exists
                                        $sqlmanual = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . " WHERE student_id='" . $student['id'] . "' AND breakup_id='" . $breakup['id'] . "'";
                                        $querymanual = $this->db->query($sqlmanual);

                                        if ($querymanual->num_rows() > 0) {
                                            $resultmanual = $querymanual->row_array();
                                            $feesJson = $resultmanual['fees'];
                                        }

                                        // Calculate fees for the due cycle
                                        $fees_amount = 0;
                                        $fees = json_decode($feesJson, true);                            
                                        foreach ($fees as $fees_list) {
                                            
                                            // Mandatory fees only
                                            if($fees_list['option'] == 2) {
                                                if( strpos($fees_list['amount'], ",") !== false ) {
                                                    $fees_array = explode(",", $fees_list['amount']);  
                                                    $fees_amount += $fees_array[0];
                                                } else {
                                                    $fees_amount += $fees_list['amount'];                                       
                                                }
                                            }    
                                        }

                                        $data[] = array(
                                            'class_info' => $class_info,
                                            'fees_amount' => $fees_amount,
                                            'due_date' => $breakup['month'],
                                            'student' => $student,
                                            'reminder' => $row
                                        );
                                    }                                                                                            
                                }
                            }
                        }  
                    }
                }                                                                                                                                                                                                                 
            }
        }
        
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_parents_communication_details($student_id) {
        
        $sql = 'SELECT * FROM '.TBL_PARENT_KIDS_LINK.' WHERE student_id="'.$student_id.'"';
        $query = $this->db->query($sql);
        
        $data = array('emails' => array(), 'devices' => array(), 'mobile_numbers' => array());
        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                
                $sql1 = "SELECT email FROM " . TBL_PARENT . " WHERE id='" . $row['parent_id'] . "' AND email!=''";
                $query1 = $this->db->query($sql1);
                
                if ($query1->num_rows() > 0) {
                    foreach($query1->result_array() as $row1) {    
                        
                        if(!in_array($row1['email'], $data['emails'])) {
                            $data['emails'][] = $row1['email'];
                        }
                    }   
                }
                
                $device_id = $this->my_custom_functions->get_particular_field_value(TBL_REGISTER_DEVICE, 'id', 'and user_id="' . $row['parent_id'] . '"');
                if($device_id != "") {
                    $data['devices'][] = $device_id;
                }
                
                $mobile_number = $this->my_custom_functions->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id="' . $row['parent_id'] . '"');
                if($mobile_number != "") {
                    $data['mobile_numbers'][] = array(
                        'parent_id' => $row['parent_id'],
                        'mobile_no' => $mobile_number
                    );
                }
            }
        }
        
        return $data;
    }
    
    function get_school_list_registered_today() {
        $result = array();
        $start_time = $date = strtotime('-24 hours', time()); 
        $sql = "SELECT * FROM ".TBL_SCHOOL." WHERE created_at >= '".$start_time."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }
    }
}
