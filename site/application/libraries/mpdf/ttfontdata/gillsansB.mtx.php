<?php
$name='GillSansStd-Bold';
$type='TTF';
$desc=array (
  'CapHeight' => 682.0,
  'XHeight' => 449.0,
  'FontBBox' => '[-177 -250 1167 929]',
  'Flags' => 262148,
  'Ascent' => 929.0,
  'Descent' => -250.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 165.0,
  'MissingWidth' => 500.0,
);
$unitsPerEm=1000;
$up=-125;
$ut=50;
$strp=269;
$strs=50;
$ttffile='/mnt/E/webdev/ServiceChampion/site/mpdf/ttfonts/GillSansStd-Bold.ttf';
$TTCfontID='0';
$originalsize=29924;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='gillsansB';
$panose=' 0 0 2 b 8 2 2 1 4 2 2 3';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 682, -318, 200
// usWinAscent/usWinDescent = 929, -250
// hhea Ascent/Descent/LineGap = 682, -318, 200
$useOTL=0x0000;
$rtlPUAstr='';
?>