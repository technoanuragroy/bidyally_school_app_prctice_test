<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Custom_functions {

    function __construct() {

        //parent::__construct();

        $CI = & get_instance();
        $CI->load->library('Mobile-Detect/Mobile_Detect');

        $detect = new Mobile_Detect;
        if ($detect->isMobile() OR $detect->isTablet()) {
            $CI->session->set_userdata('mobile', 1);
        }
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function get_particular_field_value($tablename, $fieldname, $where = "") {

        $CI = & get_instance();
        $str = "select " . $fieldname . " from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = "";
        if ($query->num_rows() > 0) {

            foreach ($query->result_array() as $row) {
                $field_arr = explode(" as ", $fieldname);
                if (count($field_arr) > 1) {
                    $fieldname = $field_arr[1];
                } else {
                    $fieldname = $field_arr[0];
                }
                $record = $row[$fieldname];
            }
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function get_details_from_id($id = "", $table, $extra = array()) {

        $CI = & get_instance();
        if ($id != "") {
            $CI->db->where("id", $id);
        }
        if (count($extra) > 0) {
            $CI->db->where($extra);
        }
        $query = $CI->db->get($table);

        $record = array();
        if ($query->num_rows() > 0) {
            $record = $query->row_array();
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function get_multiple_data($table = "", $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $table . " where 1=1 " . $where;

        $query = $CI->db->query($str);

        $record = array();
        $count = 0;
        foreach ($query->result_array() as $row) {
            $record[$count] = $row;
            $count++;
        }
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function get_perticular_count($tablename, $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = $query->num_rows();
        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function insert_data($data, $table) {

        $CI = & get_instance();
        $CI->db->insert($table, $data);
        return 1;
    }

    function insert_data_last_id($data, $table) {

        $CI = & get_instance();
        $CI->db->insert($table, $data);
        return $CI->db->insert_id();
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function delete_data($table, $where = array()) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->delete($table);
        return 1;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function update_data($data, $table, $where) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->update($table, $data);
        return 1;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function propercase($string) {

        $string = ucwords(strtolower($string));

        foreach (array('-', '\'', '(', ',', ', ') as $delimiter) {
            if (strpos($string, $delimiter) !== false) {
                $string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
            }
        }
        return $string;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function aasort(&$array, $key) {

        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    function aasort_a(&$array, $key) {
        //echo "here";die;
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;

        return $array;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function encrypt_string($string) {

        return openssl_digest($string, 'sha512');
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    public function check_super_admin_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('super_admin_id')) {
            redirect('super_admin');
        }
    }

    public function check_admin_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('admin_id')) {
            redirect('admin');
        }
    }

    public function check_school_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('school_id')) {
            redirect('school');
        }
    }

    public function check_teacher_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('teacher_id')) {
            redirect('app');
        }
    }

    public function check_parent_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('parent_id')) {
            redirect('app');
        }
    }

    public function check_coach_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('coach_id')) {
            redirect('coach');
        }
    }

    public function check_client_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('client_id')) {
            redirect('client');
        }
    }

    public function check_rater_security() {
        $CI = & get_instance();
        if (!$CI->session->userdata('rater_id')) {
            redirect('rater');
        }
    }

    /////////////////////////////////////////////////////////////
    // Function to create safe alise
    /////////////////////////////////////////////////////////////
    public function CreateAlise($name) {

        $name = strtolower(trim($name));

        $name = str_replace("_", "-", $name);
        $name = str_replace("+", "-Plus", $name);

        $name = str_replace("~", "", $name);
        $name = str_replace("!", "", $name);
        $name = str_replace("@", "", $name);
        $name = str_replace("#", "", $name);
        $name = str_replace("$", "", $name);
        $name = str_replace("%", "", $name);
        $name = str_replace("^", "", $name);
        $name = str_replace("&amp;", "", $name);
        $name = str_replace("&", "n", $name);
        $name = str_replace("*", "", $name);
        $name = str_replace("(", "", $name);
        $name = str_replace(")", "", $name);
        $name = str_replace("=", "", $name);
        $name = str_replace("{", "", $name);
        $name = str_replace("}", "", $name);
        $name = str_replace("[", "", $name);
        $name = str_replace("]", "", $name);
        $name = str_replace("'", "", $name);
        $name = str_replace("|", "", $name);
        $name = str_replace(":", "", $name);
        $name = str_replace(";", "", $name);
        $name = str_replace("<", "", $name);
        $name = str_replace(">", "", $name);
        $name = str_replace(",", "", $name);
        $name = str_replace(".", "", $name);
        $name = str_replace("?", "", $name);
        $name = str_replace("\"", "-", $name);
        $name = str_replace("/", "-", $name);
        $name = str_replace("\\", "-", $name);
        //	$name = ereg_replace("[^ -A-Za-z]", "", $name);
        //	$name = str_replace("1","",$name);
        //	$name = str_replace("2","",$name);
        //	$name = str_replace("3","",$name);
        //	$name = str_replace("4","",$name);
        //	$name = str_replace("5","",$name);
        //	$name = str_replace("6","",$name);
        //	$name = str_replace("7","",$name);
        //	$name = str_replace("8","",$name);
        //	$name = str_replace("9","",$name);
        //	$name = str_replace("0","",$name);

        $name = str_replace(" ", "-", $name);  //preg_replace($pattern, $replacement, $string);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);

        $first = substr($name, 0, 1);

        if ($first == "-") {
            $name = substr($name, 1);
        }

        if (strlen($name) > 50) {
            $name = substr($name, 0, 50);
        }

        $alise = $name;

        return $alise;
    }

    function get_mysqli() {

        $db = (array) get_instance()->db;
        return mysqli_connect('localhost', $db['username'], $db['password'], $db['database']);
    }

    function clean($string) {

        $string = preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
        $string = preg_replace('/\s+/', '-', $string); // Replaces all whitespaces with hyphens.

        return rtrim($string, '-');
    }

    function CreateFixedSizedImage($source, $dest, $width, $height) {

        $source_path = $source;

        list( $source_width, $source_height, $source_type ) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;

            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;

            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $width / $height;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            //
            // Triggered when source image is wider
            //
    $temp_height = $height;
            $temp_width = (int) ( $height * $source_aspect_ratio );
        } else {
            //
            // Triggered otherwise (i.e. source image is similar or taller)
            //
    $temp_width = $width;
            $temp_height = (int) ( $width / $source_aspect_ratio );
        }

        //
        // Resize the image into a temporary GD image
        //

  $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
                $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
        );

        //
        // Copy cropped region from temporary image into the desired GD image
        //

  $x0 = ( $temp_width - $width ) / 2;
        $y0 = ( $temp_height - $height ) / 2;
        //$y0 = 0; //if the cropping needs to be done form top

        $desired_gdim = imagecreatetruecolor($width, $height);
        imagecopy(
                $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $width, $height
        );

        //
        // Render the image
        // Alternatively, you can save the image in file-system or database
        //

  //header( 'Content-type: image/jpeg' );
        imagejpeg($desired_gdim, $dest);

        //
        // Add clean-up code here
    //
}

    function country_list() {
        $CI = & get_instance();
        $query = $CI->db->get('tbl_countries');
        $result = $query->result_array();
        return $result;
    }

    /////////////////////////////////////////////////////////////
    //	Function to convert view date to DATABASE format DATE
    /////////////////////////////////////////////////////////////
    function database_date($date) {
        $date_array = explode("/", $date);
        $new_date = @$date_array[2] . '-' . @$date_array[1] . '-' . @$date_array[0];
        //echo $new_date;die;
        return $new_date;
    }

    function database_date_dash($date) {
        $date_array = explode("-", $date);
        $new_date = @$date_array[2] . '-' . @$date_array[1] . '-' . @$date_array[0];
        //echo $new_date;die;
        return $new_date;
    }

    function get_random_password($chars_min = 8, $chars_max = 8, $use_upper_case = true, $include_numbers = true, $include_special_chars = true) {

        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if ($include_numbers) {
            $selection .= "1234567890";
        }
        if ($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $password .= $current_letter;
        }

        return $password;
    }

    function RandomString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            @$randstring .= $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

    function create_username() {
        $characters = '0123456789';
        $randstring = '';
        for ($i = 0; $i < 6; $i++) {
            @$randstring .= $characters[rand(0, strlen($characters))];
        }

        $check_duplicate = $this->get_perticular_count(TBL_COMMON_LOGIN, 'and username = "' . $randstring . '"');

        if ($check_duplicate > 0) {
            $this->create_username();
        }
        return $randstring;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    /// Encrypt function
    ///////////////////////////////////////////////////////////////////////////////////
    function ablEncrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = openssl_encrypt($string, ENCRYPT_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    /// Decrypt function
    ///////////////////////////////////////////////////////////////////////////////////
    function ablDecrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = base64_decode($string);
        $output = openssl_decrypt($output, ENCRYPT_METHOD, $key, 0, $iv);

        return $output;
    }

    function get_student_list_class_sectionwise($class_id, $section_id, $semester_id) {

        $CI = & get_instance();
        $semester_id_list = '';
        //echo "<pre>";print_r($semester_id);
        foreach ($semester_id as $sem_id) {
            $semester_id_list .= $sem_id . ',';
        }
        $semester_ids = rtrim($semester_id_list, ',');

        $sql = "SELECT t_s.id,t_s.name,t_s_e.class_id,t_s_e.section_id,t_s_e.roll_no,t_s_e.id as enrollment_id from " . TBL_STUDENT_ENROLLMENT . " t_s_e INNER JOIN " . TBL_STUDENT . " t_s ON t_s_e.student_id = t_s.id WHERE t_s_e.school_id='" . $CI->session->userdata('school_id') . "'"
                . " and t_s_e.class_id = '" . $class_id . "' and t_s_e.section_id = '" . $section_id . "' and t_s_e.semester_id IN (" . $semester_ids . ")";

        $query = $CI->db->query($sql);
//        $CI = & get_instance();
//        $CI->db->where('school_id', $CI->session->userdata('school_id'));
//        $CI->db->where('class_id', $class_id);
//        $CI->db->where('section_id', $section_id);
//        $CI->db->where('semester_id', $semester_id);
//        $query = $CI->db->get(TBL_STUDENT);

        $result = $query->result_array();
        //echo "<pre>";print_r($result);die;
        return $result;
    }

    function get_student_attendance_list_class_sectionwise($semester_id, $section_id, $period, $today_date) {
        $CI = & get_instance();
        $count = 0;
        $data = array();
        //$CI->db->where('school_id',$CI->session->userdata('school_id'));
        //$student_count = $this->get_perticular_count(TBL_STUDENT,'and school_id = "'.$CI->session->userdata('school_id').'" and class_id = "'.$class_id.'" and section_id = "'. $section_id .'"');


        $CI->db->where('semester_id', $semester_id);
        $CI->db->where('section_id', $section_id);
        $CI->db->where('period_id', $period);
        $CI->db->where('attendance_time', strtotime($today_date));

        $query = $CI->db->get(TBL_ATTENDANCE);
        //echo $CI->db->last_query();

        $result = $query->result_array();
//        foreach($result as $row){
//            $data[$count] = $row['student_id'];
//        $count++;}
        //echo "<pre>";print_r($result);

        return $result;
    }

    function clean_alise($input_string) {

        $input_string = str_replace("_", "", $input_string);
        $input_string = str_replace(" ", "-", $input_string);
        $input_string = str_replace("'", "", $input_string);
        $input_string = str_replace("&#39;", "", $input_string);
        $input_string = str_replace(",", "-", $input_string);
        $input_string = str_replace("*", "", $input_string);
        $input_string = str_replace("&quot;", "", $input_string);
        $input_string = str_replace("&amp;", "&", $input_string);
        $input_string = str_replace("&#039;", "", $input_string);

        return $input_string;
    }

    function generateOTP($length = 4, $chars = '123456789') {
        $chars_length = (strlen($chars) - 1);
        $string = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($string)) {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1})
                $string .= $r;
        }
        //echo $string;die;
        return $string;
    }

    function check_running_class_for_teacher() {
        $CI = & get_instance();
        $teacher_id = $CI->session->userdata('teacher_id');
        $today = date('Y-m-d');
        $current_time = date('H:i:00');
        $day_no = date('N', strtotime($today));
        $check_running_class = $this->get_perticular_count(TBL_TIMETABLE, 'and school_id = "' . $CI->session->userdata('school_id') . '" and day_id = "' . $day_no . '" and teacher_id = "' . $teacher_id . '" and period_start_time <= "' . $current_time . '" and period_end_time >= "' . $current_time . '" ');
        $time_table_id = $this->get_particular_field_value(TBL_TIMETABLE, 'id', 'and school_id = "' . $CI->session->userdata('school_id') . '" and day_id = "' . $day_no . '" and teacher_id = "' . $teacher_id . '" and period_start_time <= "' . $current_time . '" and period_end_time >= "' . $current_time . '" ');
        $data = array(
            'running_class' => $check_running_class,
            'period_id' => $time_table_id
        );

        return $data;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function sprintf_email($str = '', $vars = array(), $char = '%') {

        if (!$str)
            return '';
        if (count($vars) > 0) {
            foreach ($vars as $k => $v) {
                $str = str_replace($char . $k . $char, $v, $str);
            }
        }

        return $str;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    function CreateSystemEmail($addressing_user, $message, $unsubscribe = '', $institute_name = '', $system_text = '') {

        $CI = & get_instance();

        $CI->load->helper('file');

        $mail_template = read_file("application/views/mail_template/system/mail_template.php");
        $dynamic = $this->sprintf_email($mail_template, array(
            'addressing_user' => $addressing_user,
            'mail_body' => $message,
            'unsubscribe' => $unsubscribe,
            'institute_name' => $institute_name,
            'systemtext' => $system_text
                )
        );

        return $dynamic;
    }

    function getDateReturnDayNumber($date) {
        $query_date = strtotime($date);
        $day = date('w', $query_date);
        return $day;
    }

    function check_date_range() {
        $CI = & get_instance();
        $first_day_of_week = date("Y-m-d", strtotime('monday this week'));
        $last_day_of_week = date("Y-m-d", strtotime('sunday this week'));
        if ($CI->session->userdata('query_date') >= $first_day_of_week && $CI->session->userdata('query_date') <= $last_day_of_week) {
            return 1;
        } else {
            return 2;
        }
    }

    function user_validation($user_id) {
        //return 0;


        $user_status = $this->get_particular_field_value(TBL_COMMON_LOGIN, 'status', 'and id = "' . $user_id . '"');

        if ($user_status == 1) {
            return 1;   ////////////////   Active
        } else {
            return 2;   ///////////////   inactive
        }
    }

    function check_permission() {
        $data2 = array();
        $CI = & get_instance();

        $CI->load->model('school/School_user_model');


        $fname = $CI->uri->segment(3);

        $data2 = $CI->School_user_model->get_method_permission($fname);

        if (count($data2) != 0) {

            $permission = 0;

            foreach ($data2 as $result) {

                $permission_id = $result["id"];

                $data3 = $CI->School_user_model->get_user_permission($permission_id, $CI->session->userdata('teacher_id'));

                if (count($data3) != 0) {

                    $permission = 1;
                } else {
                    
                }
            }

            if ($permission == 0) {
                //echo"there";die;
                $CI->session->set_flashdata('permission_status', 'failure');
                redirect('school/user/accessDenied');
            }
        } else {

            //echo "no permission found for this function";
        }
    }

    function get_verification_token_code() {
        return $token = bin2hex(random_bytes(20));
    }

    function token_validation($token, $type) {

        $CI = & get_instance();

        $sql = "SELECT * FROM " . TBL_VERIFICATION_TOKEN . " WHERE token LIKE BINARY '" . $token . "' AND type='" . $type . "'";
        $query = $CI->db->query($sql);

        if ($query->num_rows() > 0) {

            $token_details = $query->row_array();

//            $timezone = $this->company_timezone($token_details['login_id']);
//            
//            date_default_timezone_set($timezone['timezone']);

            $time = date('Y-m-d h:i:s');
            $CI->db->where('id', $token_details['id']);
            $CI->db->where('expiry_time >=', $time);

            $queryv = $CI->db->get(TBL_VERIFICATION_TOKEN);

            if ($queryv->num_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    /*     * ********************************************************************* */
    /*     * ********************************************************************* */

    function get_parent_detail($parent_id) {

        $CI = & get_instance();

        $sql = "SELECT * FROM " . TBL_PARENT . " WHERE id = '" . $parent_id . "' GROUP BY id";
        $query = $CI->db->query($sql);
        $row = $query->row_array();
        
        return $row;
    }

    /*     * ********************************************************************* */
    /*     * ********************************************************************* */

    function sendSingleUserNotification($deviceTableId, $textContent, $url = "", $bigPic = "", $headingText = "") {

        $CI = & get_instance();

        // Get the device details
        $deviceDetails = $this->get_details_from_id($deviceTableId, TBL_REGISTER_DEVICE);

        // Preparing the heading and content        
        if ($headingText == "") {
            $headingText = SITE_NAME;
        }
        $heading = array(
            "en" => mb_substr($headingText, 0, mb_strlen($headingText), 'utf-8')
        );

        $textContent = strip_tags($textContent);
        $content = array(
            "en" => mb_substr($textContent, 0, mb_strlen($textContent), 'utf-8')
        );

        // Notification data
        $fields = array(
            'app_id' => ONESIGNAL_APP_ID,
            'headings' => $heading,
            'contents' => $content,
            'small_icon' => GLOBAL_ASSETS_LINK . 'notification_icon.png',
            'large_icon' => GLOBAL_ASSETS_LINK . 'notification_icon.png'
        );

        // Preparing device id according to the platform        
        if (strtolower($deviceDetails['device_type']) == "android") {
            $fields['include_android_reg_ids'] = array($deviceDetails['device_id']);
        } else if (strtolower($deviceDetails['device_type']) == "ios") {
            $fields['include_ios_tokens'] = array($deviceDetails['device_id']);
        } else {
            $fields['include_android_reg_ids'] = array($deviceDetails['device_id']);
        }

        // Setting the big picture and notification target url
        if ($bigPic != "") {
            $fields['big_picture'] = $bigPic;
        }

        if ($url != "") {
            $fields['url'] = $url;
        }

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8', 'Authorization: Basic ' . ONESIGNAL_API_AUTH_KEY));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response = json_decode($response, true);
    }

    /*     * ********************************************************************* */
    /*     * ********************************************************************* */

    function SendEmail($from, $to, $subject, $email_message, $attachment = "", $alternative_text = "") {

        $CI = & get_instance();

        $system_sms = $this->get_particular_field_value(TBL_GATEWAY_SETTINGS, 'gateway_value', 'and gateway_key  = "system_email"');

        if ($system_sms == 2) { // 2= system Email shutdown
            return false;
        } else {

            $from_array = explode(',', $from);
            $from_email = $from_array[0];
            $from_name = $from_array[1];

            $CI->load->library("email");
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $CI->email->initialize($config);

            $CI->email->from($from_email, $from_name);
            $CI->email->to($to);
            $CI->email->subject($subject);
            $CI->email->message($email_message);

            if ($attachment != '') {
                $CI->email->attach($attachment);
            }

            if ($alternative_text != '') {
                $CI->email->set_alt_message($alternative_text);
            }
//echo $from; echo $to; echo $subject; echo $email_message; die;
            $CI->email->send();
            return true;
        }
    }

    /*     * ********************************************************************* */
    /*     * ********************************************************************* */

    function sendSMS($mobile_no, $message) {

        $CI = & get_instance();

        $system_sms = $this->get_particular_field_value(TBL_GATEWAY_SETTINGS, 'gateway_value', 'and gateway_key  = "system_sms"');

        if ($system_sms == 2) { // 2= system sms shutdown
            return false;
        } else {

            $url = $this->get_particular_field_value(TBL_GATEWAY_SETTINGS, 'gateway_value', 'and gateway_key  = "sms_url"');
            $key_values = $this->get_particular_field_value(TBL_GATEWAY_SETTINGS, 'gateway_value', 'and gateway_key  = "sms_variables"');
            $sms_keyval_break_up = json_decode($key_values, true);

            $variables = '';
            foreach ($sms_keyval_break_up as $key => $v) {
                $variables .= $key . '=' . $v . '&';
            }
            $final_url = $url . $variables;

            $msgen = urlencode($message);
            //$send_url = $CI->config->item("SEND_SMS_URL") . "number=" . $mobile_no . "&message=" . $msgen;
            $send_url = $final_url . "number=" . $mobile_no . "&message=" . $msgen;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $send_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            curl_close($ch);

            //return json_decode($response, true);
            return true;
        }
    }

    /*     * ********************************************************************* */
    /*     * ********************************************************************* */

    function SendNotification($user_id, $content, $notification_type, $notification_id = "") {
        
        $CI = & get_instance();

        // User type(School, Teacher, Parent)
        $user_type = $this->get_particular_field_value(TBL_COMMON_LOGIN, 'type', 'and id="' . $user_id . '"');

        $school_id = 0;
        $device_table_ids = array();
        $mobile_numbers = array();
        $email_addresses = array();

        if ($user_type == SCHOOL) {

            $sql = "SELECT mobile_no, email_address FROM " . TBL_SCHOOL . " WHERE id='" . $user_id . "'";
            $query = $CI->db->query($sql);
            $contact = $query->row_array();

            $school_id = $user_id;
            $mobile_numbers[] = $contact['mobile_no'];
            $email_addresses[] = $contact['email_address'];
        } else if ($user_type == TEACHER) {

            $sql = "SELECT school_id, phone_no, email FROM " . TBL_TEACHER . " WHERE id='" . $user_id . "'";
            $query = $CI->db->query($sql);
            $contact = $query->row_array();

            $school_id = $contact['school_id'];
            $device_table_ids[] = $this->get_particular_field_value(TBL_REGISTER_DEVICE, 'id', 'and user_id="' . $user_id . '"');
            $mobile_numbers[] = $contact['phone_no'];
            $email_addresses[] = $contact['email'];
        } else if ($user_type == PARENTS) {

            $sql = "SELECT email FROM " . TBL_PARENT . " WHERE id='" . $user_id . "' AND email!=''";
            $query = $CI->db->query($sql);
            $emails = $query->result_array();

            $school_id = $this->get_particular_field_value(TBL_PARENT, 'school_id', 'and id="' . $user_id . '"');
            $device_table_ids[] = $this->get_particular_field_value(TBL_REGISTER_DEVICE, 'id', 'and user_id="' . $user_id . '"');
            $mobile_numbers[] = $this->get_particular_field_value(TBL_COMMON_LOGIN, 'username', 'and id="' . $user_id . '"');

            if (count($emails) > 0) {
                foreach ($emails as $email) {
                    $email_addresses[] = $email['email'];
                }
            }
        }

        // Initializing message related variables   
        $subject = "";
        $push_content = "";
        $sms_content = "";
        $email_content = "";

        if (array_key_exists("subject", $content)) {
            $subject = $content['subject'];
        }
        if (array_key_exists("from", $content)) {
            $from_address = $content['from'];
        }
        if (array_key_exists("push_content", $content)) {
            $push_content = $content['push_content'];
        }
        if (array_key_exists("sms_content", $content)) {
            $sms_content = $content['sms_content'];
        }
        if (array_key_exists("email_content", $content)) {
            $email_content = $content['email_content'];
        }

        if ($subject == "") {
            $subject = SITE_NAME;
        }
        if ($from_address == "") {
            $from_address = DONOT_REPLY_EMAIL;
        }
        if ($push_content == "") {
            $push_content = $email_content;
        }
        if ($sms_content == "") {
            $sms_content = $email_content;
        }

        // Get notification settings of company
        $settings = $this->get_details_from_id("", TBL_NOTIFICATION_SETTINGS, array("school_id" => $school_id, "notification_type" => $notification_type));

        ////////////////////////////////////////////////////////////////////////
        // Send push notifications
        // Check if it is enabled for the school
        if (!empty($settings)) {
            if ($settings['push_notification'] == 1) {

                if ($push_content != "") {

                    // Check if a database saved notification has to be triggered
                    if ($notification_id != "") {
                        
                    }
                    // If a new notification has to be triggered
                    else {
                        if (!empty($device_table_ids)) {
                            foreach ($device_table_ids as $dtid) {
                                if ($dtid != "") {

                                    $this->sendSingleUserNotification($dtid, strip_tags(trim($push_content)), "", "", $subject);
                                }
                            }
                        }
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // Send sms  
        // Check if it is enabled for the school
        if (!empty($settings)) {
            //echo $settings['sms_notification'];die;
            if ($settings['sms_notification'] == 1) {

                if ($sms_content != "") {

                    if (!empty($mobile_numbers)) {

                        // Send sms if there is sufficient credit available
                        $sms_credits = $this->get_available_sms_credits_of_school($school_id);

                        $sms_text = strip_tags($sms_content);
                        $count_sms_length = strlen($sms_text);
                        $count_no_of_sms = ceil($count_sms_length / 160);
                        $count_total_sms = $count_no_of_sms * count($mobile_numbers);

                        if ($sms_credits >= $count_total_sms) {

                            foreach ($mobile_numbers as $mobile) {

                                $return = $this->sendSMS($mobile, strip_tags($sms_content));
                                //$return = 1;
                                // Add credit out after sms is sent
                                if ($return) {
                                    $this->insert_data(array(
                                        "school_id" => $school_id,
                                        "credit_in" => 0,
                                        "credit_out" => $count_no_of_sms,
                                        "transaction_time" => date("Y-m-d H:i:s")
                                    ), TBL_SMS_CREDITS);
                                    
                                    // Insert into SMS log
                                    $this->insert_data(array(
                                        "user_id" => $user_id,
                                        "school_id" => $school_id,
                                        "mobile_no" => $mobile,
                                        "message" => strip_tags($sms_content),
                                        "request_time" => time()
                                    ), TBL_SMS_LOG);
                                }
                            }
                        }
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // Send emails 
        // Check if it is enabled for the school
        if (!empty($settings)) {

            if ($settings['email_notification'] == 1) {

                if ($email_content != "") {

                    if (!empty($email_addresses)) {
                        foreach ($email_addresses as $email) {

                            $this->SendEmail($from_address . ',' . SITE_NAME, $email, $subject, $email_content);
                        }
                    }
                }
            }
        }

//        echo '<pre>';
//        print_r($content);
//        print_r($device_table_ids);
//        print_r($mobile_numbers);
//        print_r($email_addresses);
//        echo '</pre>';
        // die;
        return true;
    }

    /*     * ********************************************************************* */
    /*     * ********************************************************************* */

    function get_available_sms_credits_of_school($school_id) {

        $CI = & get_instance();

        $sql = "SELECT credit_in, credit_out FROM " . TBL_SMS_CREDITS . " WHERE school_id='" . $school_id . "'";
        $query = $CI->db->query($sql);

        $total_credits_in = 0;
        $total_credits_out = 0;
        $available_credits = 0;

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if ($row['credit_in'] != 0) {
                    $total_credits_in += $row['credit_in'];
                }

                if ($row['credit_out'] != 0) {
                    $total_credits_out += $row['credit_out'];
                }
            }
        }

        $available_credits = $total_credits_in - $total_credits_out;
        return $available_credits;
    }

    function check_attendance_record($student_id, $semester_id, $section_id) {
        $CI = & get_instance();
        $sql = "SELECT id from " . TBL_ATTENDANCE . " where school_id = '" . $CI->session->userdata('school_id') . "' and student_id = '" . $student_id . "' and semester_id = '" . $semester_id . "' and section_id = '" . $section_id . "' and attendance_time = '" . strtotime(date('Y-m-d')) . "' and period_id = '" . $CI->input->post('period_id') . "'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
            $rec = $query->row_array();
            $id = $rec['id'];
            //echo "<pre>";print_r($rec);
        } else {
            $id = 0;
        }

        //echo $id;echo "<br>";
        return $id;
    }

    function get_active_parents() {
        $CI = & get_instance();
        $active_parents = 0;
        $sql = "SELECT parent_id from " . TBL_PARENT_KIDS_LINK . " where school_id = '" . $CI->session->userdata('school_id') . "' ";
        $query = $CI->db->query($sql);

        if ($query->num_rows() > 0) {
            $rec = $query->result_array();

            foreach ($rec as $row) {//echo "<pre>";print_r($row);
                $active_parents += $this->get_perticular_count(TBL_COMMON_LOGIN, 'and id = "' . $row['parent_id'] . '" and otp != 0');
            }
        }
        return $active_parents;
    }

    function get_feedback_user_details($parent_id) {

        $CI = & get_instance();
        $student_id = $this->get_particular_field_value(TBL_PARENT_KIDS_LINK, "student_id", " and parent_id='" . $parent_id . "'");
        $parent_name = $this->get_particular_field_value(TBL_STUDENT, "father_name", " and id='" . $student_id . "'");
        $message = '';

        // Company
        //echo "<pre>";print_r($_POST);
        $feedback_type = $CI->config->item('feedback_type');
        $feedback_type_name = $feedback_type[$CI->input->post('feedback_type')];
        $message .= '<b><u>Parent Details: </u></b><br>';
        $message .= '<b>Name: </b>' . $parent_name . '<br>';
        $message .= '<b>Feedback type: </b>' . $feedback_type_name . '<br>';
        $message .= '<b>Comment: </b>' . $CI->input->post('comments') . '<br>';
        $message .= '<b>------------------------------------------<br><br>';

        // Getting basic device details
        $ip = $this->getUserIP();
        $browser = $this->getBrowser($_SERVER['HTTP_USER_AGENT']);

        $CI->load->library('Mobile-Detect/Mobile_Detect');
        $detect = new Mobile_Detect;
        if ($detect->isiOS()) {
            $os = 'iOS';
        } else if ($detect->isAndroidOS()) {
            $os = 'AndroidOS';
        } else {
            $os = $this->getOS($_SERVER['HTTP_USER_AGENT']);
        }

        $message .= '<b><u>Basic Device Details: </u></b><br>' .
                '<b>OS: </b>' . $os . '<br>' .
                '<b>Browser: </b>' . $browser . '<br>' .
                '<b>IP Address: </b>' . $ip . '<br><br>';

        return $message;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get user browser
    ////////////////////////////////////////////////////////////////////////////
    function getBrowser($u_agent) {

        $CI = & get_instance();

        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";
        // First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }
        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        //$browser = $u_agent . " " . $bname . " " . $version . " " . $platform . " " . $pattern;
        $browser = $bname . " " . $version;

        $browser_or_webview = " (Browser)";
        if ($CI->session->userdata('mobile') AND $CI->session->userdata('wrapper')) {
            $browser_or_webview = " (Web View)";
        }

        return $browser . $browser_or_webview;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get user os
    ////////////////////////////////////////////////////////////////////////////
    function getOS($userAgent) {

        // Create list of operating systems with operating system name as array key 
        $oses = array(
            'iPhone' => '(iPhone)',
            'Windows 3.11' => 'Win16',
            'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
            'Windows 98' => '(Windows 98)|(Win98)',
            'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
            'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
            'Windows 2003' => '(Windows NT 5.2)',
            'Windows Vista' => '(Windows NT 6.0)|(Windows Vista)',
            'Windows 7' => '(Windows NT 6.1)|(Windows 7)',
            'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'Windows ME' => 'Windows ME',
            'Open BSD' => 'OpenBSD',
            'Sun OS' => 'SunOS',
            'Linux' => '(Linux)|(X11)',
            'Safari' => '(Safari)',
            'Mac OS' => '(Mac_PowerPC)|(Macintosh)',
            'QNX' => 'QNX',
            'BeOS' => 'BeOS',
            'OS/2' => 'OS/2',
            'Search Bot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp/cat)|(msnbot)|(ia_archiver)'
        );

        // Loop through $oses array
        foreach ($oses as $os => $preg_pattern) {
            // Use regular expressions to check operating system type
            if (preg_match('@' . $preg_pattern . '@', $userAgent)) {
                // Operating system was matched so return $oses key
                return $os;
            }
        }

        // Cannot find operating system so return Unknown

        return 'n/a';
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get user ip
    ////////////////////////////////////////////////////////////////////////////
    function getUserIP() {

        $ipaddress = '';

        if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else
            $ipaddress = 'Unknown IP Address';

        return $ipaddress;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_general_breakup_of_months($fees_structure_id) {

        $fees_details = $this->get_details_from_id($fees_structure_id, TBL_FEES_STRUCTURE);
        $semester_details = $this->get_details_from_id($fees_details['semester_id'], TBL_SEMESTER);
        $session_details = $this->get_details_from_id($semester_details['session_id'], TBL_SESSION);

        $break_up_structure = array();
        $cycle_type = $fees_details['cycle_type'];
        $cycle_start = $session_details['from_date'];
        $cycle_end = $session_details['to_date'];

        // Monthly breakup
        if ($cycle_type == 1) {
            for ($i = $cycle_start; $i <= $cycle_end; $i = date("Y-m-d", strtotime("+ 1 months", strtotime($i)))) {

                if (strtotime($i) <= strtotime($cycle_end)) {

                    $label = date("F, Y", strtotime($i));
                } else {

                    $label = date("F, Y", strtotime($cycle_end));
                }
                $break_up_structure[$i] = array(
                    "start_month" => date("Y-m-d", strtotime($i)),
                    "label" => $label
                );
            }
        }
        // Quarterly breakup
        else if ($cycle_type == 2) {
            for ($i = $cycle_start; $i <= $cycle_end; $i = date("Y-m-d", strtotime("+ 3 months", strtotime($i)))) {

                if (strtotime("+ 2 months", strtotime($i)) <= strtotime($cycle_end)) {

                    $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime("+ 2 months", strtotime($i)));
                } else {

                    if (date("F, Y", strtotime($i)) != date("F, Y", strtotime($cycle_end))) {
                        $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime($cycle_end));
                    } else {
                        $label = date("F, Y", strtotime($i));
                    }
                }
                $break_up_structure[$i] = array(
                    "start_month" => date("Y-m-d", strtotime($i)),
                    "label" => $label
                );
            }
        }
        // Half yearly breakup
        else if ($cycle_type == 3) {
            for ($i = $cycle_start; $i <= $cycle_end; $i = date("Y-m-d", strtotime("+ 6 months", strtotime($i)))) {

                if (strtotime("+ 5 months", strtotime($i)) <= strtotime($cycle_end)) {

                    $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime("+ 5 months", strtotime($i)));
                } else {

                    if (date("F, Y", strtotime($i)) != date("F, Y", strtotime($cycle_end))) {
                        $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime($cycle_end));
                    } else {
                        $label = date("F, Y", strtotime($i));
                    }
                }
                $break_up_structure[$i] = array(
                    "start_month" => date("Y-m-d", strtotime($i)),
                    "label" => $label
                );
            }
        }
        // Yearly breakup
        else if ($cycle_type == 4) {

            $label = date("F, Y", strtotime($cycle_start)) . ' -> ' . date("F, Y", strtotime($cycle_end));
            $break_up_structure[$cycle_start] = array(
                "start_month" => date("Y-m-d", strtotime($cycle_start)),
                "label" => $label
            );
        }

        return $break_up_structure;
    }

    ////////////////////////////////////////////////////////////////////////////

    function calculate_fees_amounts($total_fees, $fees_id, $breakup_id, $payment_gateway_id = "") {

        // Get the school specific payment settings and fees details     
        $fees_structure = $this->get_details_from_id("", TBL_FEES_STRUCTURE, array("id" => $this->ablDecrypt($fees_id)));
        $fees_structure_breakup = $this->get_details_from_id("", TBL_FEES_STRUCTURE_BREAKUPS, array("id" => $this->ablDecrypt($breakup_id)));
        $payment_settings = $this->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $fees_structure['school_id']));

        // Default fees calculation 
        $fees_amount = $total_fees;
        $late_fine = 0;
        $service_charges = 0;
        $total_amount = $fees_amount;

        // Calculate late fine if any
        if ($fees_structure['late_payment_setting'] == 1) {
            if ($fees_structure['late_payment_amount'] > 0) {

                $cycle_start_time = strtotime($fees_structure_breakup['month']);
                $last_time_before_fine = strtotime(date("Y-m-d", strtotime("+" . ($fees_structure['late_payment_day_limit'] - 1) . " days", $cycle_start_time)) . " 23:59:59");

                if (time() > $last_time_before_fine) {

                    // Fixed late fine
                    if ($fees_structure['late_payment_type'] == 1) {
                        $late_fine = $fees_structure['late_payment_amount'];
                    }
                    // Percentage late fine
                    else if ($fees_structure['late_payment_type'] == 2) {
                        $late_fine = (($fees_structure['late_payment_amount'] / 100) * $total_amount);
                    }
                }
            }
        }

        $total_amount += $late_fine;

        // Calculate fees according to school payment settings                    
        // If the parent is charged extra
        if ($payment_settings['add_on_school_fees'] > 0) {
            $service_charges = (($payment_settings['add_on_school_fees'] / 100) * $total_amount);
            $total_amount += $service_charges;
        }

        // If service charge is negotiated with school
        if ($payment_settings['deduct_from_school_fees'] > 0) {
            $service_charges = (($payment_settings['deduct_from_school_fees'] / 100) * $total_amount);
        }

        $return = array(
            'fees_amount' => (int) $fees_amount,
            'late_fine' => (int) $late_fine,
            'service_charges' => (int) $service_charges,
            'total_amount' => (int) $total_amount
        );

        return $return;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_class_info_json_for_student($student_id, $semester_id) {

        $student_details = $this->get_details_from_id($student_id, TBL_STUDENT);
        $enrollment_details = $this->get_details_from_id("", TBL_STUDENT_ENROLLMENT, array("student_id" => $student_id, "semester_id" => $semester_id));
        $semester_details = $this->get_details_from_id($semester_id, TBL_SEMESTER);
        if (!empty($semester_details)) {
            $session_name = $this->get_particular_field_value(TBL_SESSION, 'session_name', 'and id="' . $semester_details['session_id'] . '" ');
        }
        $class_name = $this->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id="' . $enrollment_details['class_id'] . '" ');
        $section_name = $this->get_particular_field_value(TBL_SECTION, 'section_name', 'and id="' . $enrollment_details['section_id'] . '" ');

        $class_info = array(
            'session' => isset($session_name) ? $session_name : "",
            'semester' => isset($semester_details['semester_name']) ? $semester_details['semester_name'] : "",
            'class' => $class_name,
            'section' => $section_name,
            'roll_no' => $enrollment_details['roll_no']
        );

        return json_encode($class_info);
    }

    function get_short_name($teacher_name) {
        $break_teacher_name = explode(' ', trim($teacher_name));

        $length = count($break_teacher_name);

        $join_teacher_name = '';
        for ($i = 0; $i < $length - 1; $i++) {

            $join_teacher_name .= substr($break_teacher_name[$i], 0, 1) . '.';
        }
        $name = $join_teacher_name . $break_teacher_name[$length - 1];

        return $name;
    }

    function get_semester_id($today, $class_id) {
        $CI = & get_instance();
        $semester_id = '';
        $sql = "SELECT * from " . TBL_SESSION . " WHERE school_id = '" . $CI->session->userdata('school_id') . "' and from_date <='" . $today . "' and to_date >= '" . $today . "'";

        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
            $session = $query->result_array();

            foreach ($session as $sessions) {
                $session_id = $sessions['id'];
            }
            $sql_a = "SELECT * FROM " . TBL_SEMESTER . " WHERE session_id = '" . $session_id . "' and class_id = '" . $class_id . "'";
            $query_a = $CI->db->query($sql_a);
            $semester = $query_a->result_array();
            foreach ($semester as $semesters) {
                $semester_id = $semesters['id'];
            }
            return $semester_id;
        }
    }

    function get_current_semester_id($today, $class_id) {//echo $class_id;
        $CI = & get_instance();
        $semester_id = array();
        $sql = "SELECT * from " . TBL_SESSION . " WHERE school_id = '" . $CI->session->userdata('school_id') . "' and from_date <='" . $today . "' and to_date >= '" . $today . "'";

        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
            $session = $query->result_array();
            //echo "<pre>";print_r($session);
            foreach ($session as $sessions) {
                //$session_id = $sessions['id'];

                $sql_a = "SELECT * FROM " . TBL_SEMESTER . " WHERE session_id = '" . $sessions['id'] . "' and class_id = '" . $class_id . "'";
                $query_a = $CI->db->query($sql_a);
                $semester = $query_a->result_array();
                //echo "<pre>";print_r($semester);
                foreach ($semester as $semesters) {
                    $semester_id[] = $semesters['id'];
                }
            }
            return $semester_id;
        }
    }

    // Rotate image using GD
    function RotateJpg($filename = '', $angle = 0, $savename = false, $ignore_quality = 0) {

        // Your original file
        //$original = imagecreatefromjpeg($filename);

        list($width_orig, $height_orig, $type) = getimagesize($filename);

        // Temporarily increase the memory limit to allow for larger images
        //ini_set('memory_limit', '32M'); 

        switch ($type) {

            case IMAGETYPE_GIF:
                $original = imagecreatefromgif($filename);
                break;
            case IMAGETYPE_JPEG:
                $original = imagecreatefromjpeg($filename);
                break;
            case IMAGETYPE_PNG:
                $original = imagecreatefrompng($filename);
                break;
            default:
                throw new Exception('Unrecognized image type ' . $type);
        }

        // Rotate
        $rotated = imagerotate($original, $angle, 0);

        // If you have no destination, save to browser
        if ($savename == false) {
            header('Content-Type: image/jpeg');
            if ($ignore_quality == "1") {
                imagejpeg($rotated, NULL);
            } else {
                imagejpeg($rotated, NULL, 100);
            }
        }
        // Save to a directory with a new filename
        else {
            if ($ignore_quality == "1") {
                imagejpeg($rotated, $savename);
            } else {
                imagejpeg($rotated, $savename, 100);
            }
        }

        // Standard destroy command
        imagedestroy($rotated);
    }

    ///////////////////////////////////////////////////////////////////////////////

    function convert_number_to_words($number) {

        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        if (!is_numeric($number)) {
            return false;
        }
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }
        if ($number < 0) {
            return $negative . $this->convert_number_to_words(abs($number));
        }
        $string = $fraction = null;
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get running sessions of a school
    ////////////////////////////////////////////////////////////////////////////
    function get_running_sessions_of_a_school($school_id) {

        $CI = & get_instance();

        $today = date("Y-m-d");
        $sql = 'SELECT * FROM ' . TBL_SESSION . ' WHERE school_id=' . $school_id . ' AND from_date<="' . $today . '" AND to_date>="' . $today . '" ORDER BY from_date DESC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get all sessions of a school
    ////////////////////////////////////////////////////////////////////////////
    function get_all_sessions_of_a_school($school_id) {

        $CI = & get_instance();

        $sql = 'SELECT * FROM ' . TBL_SESSION . ' WHERE school_id=' . $school_id . ' ORDER BY from_date DESC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get all semesters of a session
    ////////////////////////////////////////////////////////////////////////////
    function get_all_semesters_of_a_session($session_id) {

        $CI = & get_instance();

        $sql = 'SELECT t_sem.*, t_ses.from_date, t_ses.to_date '
                . 'FROM ' . TBL_SEMESTER . ' t_sem INNER JOIN ' . TBL_SESSION . ' t_ses '
                . 'ON t_sem.session_id=t_ses.id '
                . 'WHERE t_ses.id="' . $session_id . '" '
                . 'ORDER BY t_sem.semester_name ASC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }
    
    

    ////////////////////////////////////////////////////////////////////////////
    // Get all semesters of running session
    ////////////////////////////////////////////////////////////////////////////
    function get_all_semesters_of_running_session() {

        $CI = & get_instance();
        $session_id = $CI->session->userdata('running_sessions');
        $result = array();
        //echo $session_id;die;
        if($session_id != ''){
        $sql = 'SELECT t_sem.*, t_ses.from_date, t_ses.to_date '
                . 'FROM ' . TBL_SEMESTER . ' t_sem INNER JOIN ' . TBL_SESSION . ' t_ses '
                . 'ON t_sem.session_id=t_ses.id '
                . 'WHERE t_ses.id IN (' . $session_id . ') '
                . 'ORDER BY t_sem.semester_name ASC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get section list of class or semester
    ////////////////////////////////////////////////////////////////////////////
    function get_section_list($class_id = "", $semester_id = "") {

        if ($semester_id != "") {
            $class_id = $this->get_particular_field_value(TBL_SEMESTER, 'class_id', ' and id="' . $semester_id . '" ');
        }

        $section_list = $this->get_multiple_data(TBL_SECTION, ' and class_id="' . $class_id . '" and status=1');

        return $section_list;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get student list of a session/semester/section
    ////////////////////////////////////////////////////////////////////////////    
    function get_student_list($parameters = array()) {

        $CI = & get_instance();

        $student_list = array();
        $where = '';

        // If a particular semester is specified
        if (array_key_exists('semester_id', $parameters)) {
            if ($parameters['semester_id'] != "") {

                $where .= ' AND t_s_e.semester_id="' . $parameters['semester_id'] . '"';
            }
        }

        // If semester is not specified, get all the semesters of the session
        if ($where == '') {

            if (array_key_exists('session_id', $parameters)) {
                if ($parameters['session_id'] != "") {

                    $semesters = $this->get_all_semesters_of_a_session($parameters['session_id']);

                    $semesters_string = "";
                    if (!empty($semesters)) {
                        foreach ($semesters as $sem) {
                            $semesters_string .= $sem['id'] . ",";
                        }
                    }
                    $semesters_string = rtrim($semesters_string, ",");

                    if ($semesters_string != "") {
                        $where .= ' AND t_s_e.semester_id IN(' . $semesters_string . ')';
                    }
                }
            }
        }

        // If we get a session or a semester
        if ($where != '') {

            // If section is specified
            if (array_key_exists('section_id', $parameters)) {
                if ($parameters['section_id'] != "") {

                    $where .= ' AND t_s_e.section_id="' . $parameters['section_id'] . '"';
                }
            }

            // If student name is specified
            if (array_key_exists('name', $parameters)) {
                if ($parameters['name'] != "") {

                    $where .= ' AND t_s.name LIKE "%' . $parameters['name'] . '%"';
                }
            }

            // If student roll number is specified
            if (array_key_exists('roll_no', $parameters)) {
                if ($parameters['roll_no'] != "") {

                    $where .= ' AND t_s_e.roll_no LIKE "%' . $parameters['roll_no'] . '%"';
                }
            }

            $sql = 'SELECT t_s.*, t_s_e.school_id, t_s_e.class_id, t_s_e.semester_id, t_s_e.section_id, t_s_e.roll_no '
                    . 'FROM ' . TBL_STUDENT . ' t_s INNER JOIN ' . TBL_STUDENT_ENROLLMENT . ' t_s_e '
                    . 'ON t_s.id=t_s_e.student_id '
                    . 'WHERE 1=1 ' . $where . ' AND t_s.is_deleted=0 ORDER BY t_s_e.semester_id,t_s_e.section_id ASC ';
            $query = $CI->db->query($sql);

            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $student_list[$row['id']] = $row;
                }
            }
        }

        return $student_list;
    }
    
    function get_unenrolled_student_list() {
        $CI = & get_instance();
        $data = array();
        $sql = "SELECT ".TBL_STUDENT.".id,".TBL_STUDENT.".name,".TBL_STUDENT.".father_name,".TBL_STUDENT_ENROLLMENT.".semester_id from ".TBL_STUDENT." LEFT JOIN ".TBL_STUDENT_ENROLLMENT." ON ".TBL_STUDENT.".id = ".TBL_STUDENT_ENROLLMENT.".student_id WHERE ".TBL_STUDENT.".school_id = '".$CI->session->userdata('school_id')."'";
        $query = $CI->db->query($sql);
        $result = $query->result_array();
        foreach($result as $row){
            if($row['semester_id'] == ''){
                
                $data[] = $row;
            }
        }
        ////echo "<pre>";print_r($data);die;
        return $data;
        
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get semesters of a student
    // If session id is provided, will show all the semesters(the student is enrolled in) of that session
    // Else will show all active semesters(the student is enrolled in) of all sessions
    ////////////////////////////////////////////////////////////////////////////
    function get_semesters_of_a_student($student_id, $session_id = "") {

        $CI = & get_instance();

        $where = '';
        if ($session_id != "") {
            $where = 'AND t_ses.id= ' . $session_id;
        } else {
            $where = 'AND t_ses.to_date>="' . date("Y-m-d") . '"';
        }

        $sql = 'SELECT t_sem.*, t_ses.from_date, t_ses.to_date, t_stu_en.section_id, t_stu_en.roll_no, t_stu_en.id AS enrollment_id '
                . 'FROM ' . TBL_STUDENT_ENROLLMENT . ' t_stu_en INNER JOIN ' . TBL_SEMESTER . ' t_sem '
                . 'ON t_stu_en.semester_id=t_sem.id '
                . 'INNER JOIN ' . TBL_SESSION . ' t_ses '
                . 'ON t_sem.session_id=t_ses.id '
                . 'WHERE t_stu_en.student_id=' . $student_id . ' ' . $where . ' '
                . 'ORDER BY t_ses.from_date DESC, t_sem.semester_name ASC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }

    function get_student_details_from_id($student_id) {
        $CI = & get_instance();

        $sql = "SELECT t_s_e.school_id,t_s_e.student_id,t_s_e.class_id,t_s_e.semester_id,t_s_e.section_id,t_s_e.roll_no,t_s.* FROM " . TBL_STUDENT_ENROLLMENT . " t_s_e"
                . " INNER JOIN " . TBL_STUDENT . " t_s ON t_s_e.student_id = t_s.id WHERE t_s.id = '" . $student_id . "'";
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }

    function get_student_details_from_id_by_sem($student_id, $sem_id) {
        $CI = & get_instance();

        $sql = "SELECT t_s_e.school_id,t_s_e.student_id,t_s_e.class_id,t_s_e.semester_id,t_s_e.section_id,t_s_e.roll_no,t_s.* FROM " . TBL_STUDENT_ENROLLMENT . " t_s_e"
                . " INNER JOIN " . TBL_STUDENT . " t_s ON t_s_e.student_id = t_s.id WHERE t_s.id = '" . $student_id . "' and t_s_e.semester_id = '" . $sem_id . "'";
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }

    function get_student_details_from_id_with_sem_id($student_id, $semester_id) {
        $CI = & get_instance();

        $sql = "SELECT t_s_e.school_id,t_s_e.student_id,t_s_e.class_id,t_s_e.semester_id,t_s_e.section_id,t_s_e.roll_no,t_s.* FROM " . TBL_STUDENT_ENROLLMENT . " t_s_e"
                . " INNER JOIN " . TBL_STUDENT . " t_s ON t_s_e.student_id = t_s.id WHERE t_s.id = '" . $student_id . "' and t_s_e.semester_id = '" . $semester_id . "'";
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }

    function get_all_sessions_of_a_student() {

        $CI = & get_instance();

        $sql = "SELECT t_ses.*,t_sem.semester_name,t_s_e.student_id  FROM " . TBL_STUDENT_ENROLLMENT . " t_s_e"
                . " INNER JOIN " . TBL_SEMESTER . " t_sem ON t_s_e.semester_id = t_sem.id INNER JOIN " . TBL_SESSION . " t_ses"
                . " ON t_sem.session_id = t_ses.id WHERE t_s_e.school_id = '" . $CI->session->userdata('school_id') . "' and t_s_e.student_id = '" . $CI->session->userdata('student_id') . "'";

        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $row['current_session'] = '';
                if ($row['from_date'] < date('Y-m-d') && $row['to_date'] > date('Y-m-d')) {
                    $row['current_session'] = 'true';
                    $session_data = array("session_id" => $row['id']);
                    $CI->session->set_userdata($session_data);
                }
                $result[$row['id']] = $row;
            }
        }

        return $result;
    }

    function get_running_session() {
        $CI = & get_instance();
        $semester_ids = "";
        $sem_id = '';
        $today_date = date('Y-m-d');
        $sql = "SELECT id from " . TBL_SESSION . " WHERE school_id = '" . $CI->session->userdata('school_id') . "' and  from_date <='" . $today_date . "' and to_date >= '" . $today_date . "'";
        
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $semester_ids .= $row['id'] . ',';
            }

            $sem_id = rtrim($semester_ids, ',');

            
        }
        return $sem_id;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Get razorpay credentials according to environment(test or live)     
    // Each school has its own environment settings(test or live)
    ////////////////////////////////////////////////////////////////////////////
    function get_razorpay_credentials($school_id) {
        
        $return = array();
        $school_payment_settings = $this->get_details_from_id("", TBL_SCHOOL_PAYMENT_SETTINGS, array("school_id" => $school_id));
                
        // Live environment
        if($school_payment_settings['payment_gateway_environment'] == 1) {
            $return['RAZORPAY_KEY_ID'] = RAZORPAY_KEY_ID_LIVE;
            $return['RAZORPAY_KEY_SECRET'] = RAZORPAY_KEY_SECRET_LIVE;
        }
        // Test environment
        else if($school_payment_settings['payment_gateway_environment'] == 0) {
            $return['RAZORPAY_KEY_ID'] = RAZORPAY_KEY_ID_TEST;
            $return['RAZORPAY_KEY_SECRET'] = RAZORPAY_KEY_SECRET_TEST;
        }
        
        return $return;
    }
    
    ////////////////////////////////////////////////////////////////////////////    

    // Time format is UNIX timestamp or
    // PHP strtotime compatible strings
    function dateDiff($time1, $time2, $precision = 6) {
        // If not numeric then convert texts to unix timestamps
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }

        // If time1 is bigger than time2
        // Then swap time1 and time2
        if ($time1 > $time2) {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
        }

        // Set up intervals and diffs arrays
        $intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $interval) {
            // Create temp time from time1 and interval
            $ttime = strtotime('+1 ' . $interval, $time1);
            // Set initial values
            $add = 1;
            $looped = 0;
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
                // Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
            }

            $time1 = strtotime("+" . $looped . " " . $interval, $time1);
            $diffs[$interval] = $looped;
        }

        $count = 0;
        $times = array();
        // Loop thru all diffs
        foreach ($diffs as $interval => $value) {
            // Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
            // Add value and interval 
            // if value is bigger than 0
            if ($value > 0) {
                // Add s if value is not 1
                if ($value != 1) {
                    $interval .= "s";
                }
                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }

        // Return string with times
        return implode(", ", $times);
    }
    
    ////////////////////////////////////////////////////////////////////////////   
    
    function get_school_account_progress($school_id = "") {
        
        $CI = & get_instance();

        $CI->load->model('school/School_user_model');
        
        $progress = $CI->School_user_model->get_school_account_progress($school_id);
        return $progress; 
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    function get_chat_school_details($school_id = "") {
        
        $CI = & get_instance();
        
        if($school_id == "") {
            $school_id = $CI->session->userdata('school_id');
        } else {
            $school_id = $this->ablDecrypt($school_id);
        }
        
        $username = $this->get_particular_field_value(TBL_COMMON_LOGIN, "username", " and id='" . $school_id . "' and type='".SCHOOL."'");
        $school_details = $this->get_details_from_id($school_id, TBL_SCHOOL);
                                
        $return_data = array(
            "externalId" => $username,
            "name" => $school_details['contact_person_name'],
            "email" => $school_details['email_address'],
            "mobile" => $school_details['mobile_no'],            
            "properties" => array(
                "school_name" => $school_details['name'],
            )
        );
        
        return json_encode($return_data);
    }
}

?>
