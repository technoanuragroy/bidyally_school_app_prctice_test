-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 25, 2019 at 04:18 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bidyaaly`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE `tbl_admins` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Active/0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`admin_id`, `name`, `phone`, `email`, `username`, `password`, `date_created`, `status`) VALUES
(1, 'Biplob Mudi', '9749552668', 'biplob@ablion.in', 'developer', '$2y$10$hNmR7/A2VZwU0ogrf0r8d.lW07gcvyrN791CV2sX4Evf3ubnbyBYe', '2019-04-23 11:30:10', 1),
(2, 'Aparajita Datta', '9434251530', 'aparajita@ablion.in', 'aparajita', '$2y$10$E5BCzHSQaoOCm0hYv0yPqejB6xKcLLVwUQzizYpdh6YBJHGpGL9Hy', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_files`
--

CREATE TABLE `tbl_admin_files` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `file_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin_files`
--

INSERT INTO `tbl_admin_files` (`id`, `admin_id`, `file_url`) VALUES
(5, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/admin/1_1563434525');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_api_log`
--

CREATE TABLE `tbl_api_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `function_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receive_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_api_log`
--

INSERT INTO `tbl_api_log` (`id`, `user_id`, `function_name`, `receive_data`, `response_data`, `transaction_time`) VALUES
(1, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 11:08:24'),
(2, 8, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:08:26'),
(3, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 11:12:59'),
(4, 8, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:13:01'),
(5, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 11:14:11'),
(6, 8, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:14:13'),
(7, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 11:16:49'),
(8, 8, 'register_device', '{\"device_id\":\"c3ysDgiabRw:APA91bFi6C2eRnmvC0yUTisJWsy_MGk4oWuPaIW6d3HZzW-sd9Xb-F35HMqc2-hSmn2IRYGm2Q0iuTaobPmhdW0ZH1uuVRG92cdjE2tzkjl9OHo2oEwrPqy4hKaMt-VmHu8RwxoTXmsE\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:16:51'),
(9, 79, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"aparajita@ablion.in\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"79\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/WjBiWmJFb0lnRnNmUjVWeUVOZHdXZz09\",\"status\":\"true\"}]', '2019-07-13 11:17:20'),
(10, 79, 'register_device', '{\"device_id\":\"fKIfS-CaWnA:APA91bEw1m0aBIHbj0qL4YfCVzZSayLjwrb0bjCtNMjD4glLXE9xaIdrd7DKLU2qm7CtNAUZCF3d4EfjpLYlx_V07pOBdDf4KiD4Xq39-U9N_UQ4SIYHUaV2bDr1osiKQTfhejJaluyg\",\"user_id\":\"79\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:17:22'),
(11, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 11:19:36'),
(12, 8, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:19:37'),
(13, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-13 11:23:51'),
(14, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-13 11:24:13'),
(15, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 11:24:34'),
(16, 8, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 11:24:36'),
(17, 8, 'authentication', '{\"key\":\"20Bidyaalyabl04\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 12:48:58'),
(18, 89, 'authentication', '{\"key\":\"20Bidyaalyabl04\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:30:51'),
(19, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-13 15:37:17'),
(20, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:37:58'),
(21, 89, 'authentication', '{\"key\":\"20Bidyaalyabl04\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:42:03'),
(22, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:42:14'),
(23, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:43:12'),
(24, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 15:43:14'),
(25, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:43:56'),
(26, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 15:43:58'),
(27, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 15:48:58'),
(28, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 15:49:00'),
(29, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-13 16:30:40'),
(30, 15, 'register_device', '{\"device_id\":\"diLkkIX90lU:APA91bEE-gPlkOG4MUKYjDysAamM3Mf92vA55Aj5_ogv6jsdvfvV8Wjso0TwZSGwyCdl859B2-kN9lr8Ea6Y89cQOkpXQSpnp0jNrAS9oyodLDHIRa-Nwe8juMzMmBqTxA1jBAvXTdit\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 16:30:42'),
(31, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 16:31:14'),
(32, 8, 'register_device', '{\"device_id\":\"diLkkIX90lU:APA91bEE-gPlkOG4MUKYjDysAamM3Mf92vA55Aj5_ogv6jsdvfvV8Wjso0TwZSGwyCdl859B2-kN9lr8Ea6Y89cQOkpXQSpnp0jNrAS9oyodLDHIRa-Nwe8juMzMmBqTxA1jBAvXTdit\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 16:31:16'),
(33, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 16:33:55'),
(34, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 16:33:57'),
(35, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 16:34:45'),
(36, 8, 'register_device', '{\"device_id\":\"fJuzJeB_iA0:APA91bHu74R5U_a-DapP-i3H1pOOP3znoQ1AQ99-DcmEtoG9aAnpxC7I-rwClV4LxQUlV5y3jVziIeDy5U1zGim7-F5AgRcP8bS5m7yElDI4YPREtLi5_sLUi-YEwXGF7JeoA3sTPo9N\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 16:34:46'),
(37, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 16:35:31'),
(38, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 16:35:33'),
(39, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 16:41:12'),
(40, 8, 'register_device', '{\"device_id\":\"cqNshiWy8M4:APA91bFT61MOl6M85aEh4t5lfbrcHW_QZLpnUHI01x8Y0v0w6_km1mAI8_gE-ELv0yQNlyv-ucgXC_Zy29HfyfoesrFeiVtBJb9KBHllWj8EYcellznfvEwRlHTq-IJDRsGB6go2J51X\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 16:41:14'),
(41, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 17:15:32'),
(42, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 17:15:34'),
(43, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 17:18:11'),
(44, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 17:18:12'),
(45, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 17:19:29'),
(46, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 17:19:31'),
(47, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-13 17:35:11'),
(48, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 17:35:16'),
(49, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 17:35:18'),
(50, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 17:53:08'),
(51, 89, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 17:53:10'),
(52, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:05:51'),
(53, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:05:53'),
(54, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 18:07:45'),
(55, 8, 'register_device', '{\"device_id\":\"dnNHATizgdY:APA91bGKU7qNHjFxxRUaANjTREmslf-3Xaerx_EFOmLqT_NbDLSDpb87NTmp4A7IJki3oraQjLipnIgWTKdfwUQUuxcpZbiJzjjVk3fIkd6oq4J2Ikypbegdr1lqKRa4DxI8LZHiAVlp\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:07:47'),
(56, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"6018064414\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-13 18:11:02'),
(57, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:11:31'),
(58, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:11:32'),
(59, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:13:39'),
(60, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:13:41'),
(61, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:16:09'),
(62, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:16:10'),
(63, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:18:00'),
(64, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:18:03'),
(65, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:20:26'),
(66, 89, 'register_device', '{\"device_id\":\"egMpyxVAueg:APA91bEJqkworC8iVe7WxKDStzZZp6zdWzzXL5-T7aQPe3fL2IfGouJUVaaq3WWLx84EdfxyG37AIsOeNWoeCT1SVj2_ZPZuoTbU3kcPtSrIA-ecaCy4gH8olTHPpMqwTfIDOrUmntWt\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:20:28'),
(67, 85, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9999999999\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"85\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/SG5Sd2NQV2M3ZTRQZWhDNElhd09GQT09\",\"status\":\"true\"}]', '2019-07-13 18:21:24'),
(68, 85, 'register_device', '{\"device_id\":\"f4OlDU7S9w8:APA91bEMLuG6Pgb2rbw-qU7owDLSIjBOCIWIhAXZcS0b5EEm3e5DrqK-HOWt_xvApyUDENigL-XUCd9XSAbX7Lbt2w0ZmE4MgWG3a_Z3AURgbe-0GFXo0B9kw9hXzvUNlV3rN1pJxEhB\",\"user_id\":\"85\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:21:25'),
(69, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:24:55'),
(70, 89, 'register_device', '{\"device_id\":\"cL7GXdOjY2I:APA91bG_LGTLmh0R77mHEPxeoFtwFI6iU-7Kccoddt2LB4T1s2lzpJ0YuZasBSTuUk598kJzPlObkcc4nI3hykbJxQ1G-6df95ojiotBem_vghOlJoygSWp8HdY6kypksCfQoiOUnkjb\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:24:57'),
(71, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:26:39'),
(72, 89, 'register_device', '{\"device_id\":\"fLfyeEFqDRc:APA91bEKpGA9wPGtteBQbRAf8e8BHQLTgEWfdMWOHBn0EOZ77qws1i8MVithbrjKpbq48xKRmb_xJBh385pPSLFjSxOMDxT4hzinerA4bXSELH8apb5LzLoS25Rm6mwz2CvxmZU1nQS7\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:26:41'),
(73, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-13 18:43:27'),
(74, 8, 'register_device', '{\"device_id\":\"dbQlY2A36tc:APA91bFrKA16PqxYgEPs6_2nvxKT5zKDiAfTos0C10dlqLwKlMszqGaRSGtOWZvvaiFOCmUDp0BmN0QMhSg0RcjCGbWrKv9_dNiQe0fhShXVrMeYW_zbS-sq_yLx7jNdF_bfufQJF5vr\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:43:29'),
(75, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:46:37'),
(76, 89, 'register_device', '{\"device_id\":\"cL7GXdOjY2I:APA91bG_LGTLmh0R77mHEPxeoFtwFI6iU-7Kccoddt2LB4T1s2lzpJ0YuZasBSTuUk598kJzPlObkcc4nI3hykbJxQ1G-6df95ojiotBem_vghOlJoygSWp8HdY6kypksCfQoiOUnkjb\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:46:39'),
(77, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-13 18:49:36'),
(78, 89, 'register_device', '{\"device_id\":\"dj6YTA6duu0:APA91bE-w9eSup-7BkdhFTTP6GYkESEcmKDbtIL8apbcPGMJgNtWcJ55RCb9Aj8kSXY6bRtJl6chbU0CE3MBDHh1YOXEWq-kZ2u1hvEGJ6VzbzOdC85XE__YqdKcJmL5_AaAW7Ib7N1v\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-13 18:49:38'),
(79, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"admin\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-13 21:01:43'),
(80, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-14 21:03:37'),
(81, 8, 'register_device', '{\"device_id\":\"dj6YTA6duu0:APA91bE-w9eSup-7BkdhFTTP6GYkESEcmKDbtIL8apbcPGMJgNtWcJ55RCb9Aj8kSXY6bRtJl6chbU0CE3MBDHh1YOXEWq-kZ2u1hvEGJ6VzbzOdC85XE__YqdKcJmL5_AaAW7Ib7N1v\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-14 21:03:39'),
(82, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 10:06:15'),
(83, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 10:08:02'),
(84, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 10:10:35'),
(85, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 10:46:08'),
(86, 15, 'authentication', '{\"username\":\"9749552668\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-15 10:48:09'),
(87, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:15:53'),
(88, 8, 'register_device', '{\"device_id\":\"cc3nWT3iciY:APA91bGxiv8vuUD_60J3K8Bxu3MNKXBEQCjyuhpOiZfW0SSS2AEXHVbbPdFL-ZEuPDpkT0EQ8xNw3zEglyfdsrKxxYVW3hw-b7mMSadd6EHxk6uuXKBbJmhsK4TJ4UXi_ksTGd9Oes4I\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:15:56'),
(89, 0, 'authentication', '{\"username\":\"biolob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-15 11:17:12'),
(90, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:17:20'),
(91, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 11:21:02'),
(92, 89, 'register_device', '{\"device_id\":\"fLfyeEFqDRc:APA91bEKpGA9wPGtteBQbRAf8e8BHQLTgEWfdMWOHBn0EOZ77qws1i8MVithbrjKpbq48xKRmb_xJBh385pPSLFjSxOMDxT4hzinerA4bXSELH8apb5LzLoS25Rm6mwz2CvxmZU1nQS7\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:21:04'),
(93, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:21:16'),
(94, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 11:22:53'),
(95, 89, 'register_device', '{\"device_id\":\"fmdAR3cANOc:APA91bGYGDdDqsWdKTIuzW4xgkQlreW9FVZLqEQAmNTF60fGuE2Hst-UeWpBUjTGa8Ljt9HwgHQ_4QlcioRApXfuSRLCfOtEbh-0jT-x86ffLT0ePFFUdrPLHNGh6a2b1nUTSpljwSWX\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:22:55'),
(96, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:23:03'),
(97, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:25:13'),
(98, 8, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"8\",\"device_id\":\"cbTTMZBCa6k:APA91bF5aBWzhIVK8OO9Tk0eDhIp4YxUdViTeCrOl1UmAUwu_fH5sI3EcgHGOGt4e_qcsjQNIeCSDYZDf0P0WHLUi4NqQCHXjFLTrhgSJ5-CLeh2RJqVS8ZyNcSZ2qqrwlteQNJDhIzU\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:25:14'),
(99, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:27:03'),
(100, 8, 'register_device', '{\"device_id\":\"cc3nWT3iciY:APA91bGxiv8vuUD_60J3K8Bxu3MNKXBEQCjyuhpOiZfW0SSS2AEXHVbbPdFL-ZEuPDpkT0EQ8xNw3zEglyfdsrKxxYVW3hw-b7mMSadd6EHxk6uuXKBbJmhsK4TJ4UXi_ksTGd9Oes4I\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:27:04'),
(101, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 11:45:03'),
(102, 89, 'register_device', '{\"device_id\":\"fmdAR3cANOc:APA91bGYGDdDqsWdKTIuzW4xgkQlreW9FVZLqEQAmNTF60fGuE2Hst-UeWpBUjTGa8Ljt9HwgHQ_4QlcioRApXfuSRLCfOtEbh-0jT-x86ffLT0ePFFUdrPLHNGh6a2b1nUTSpljwSWX\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:45:05'),
(103, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 11:52:37'),
(104, 8, 'register_device', '{\"device_id\":\"dj6YTA6duu0:APA91bE-w9eSup-7BkdhFTTP6GYkESEcmKDbtIL8apbcPGMJgNtWcJ55RCb9Aj8kSXY6bRtJl6chbU0CE3MBDHh1YOXEWq-kZ2u1hvEGJ6VzbzOdC85XE__YqdKcJmL5_AaAW7Ib7N1v\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:52:39'),
(105, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 11:54:52'),
(106, 89, 'register_device', '{\"device_id\":\"fmdAR3cANOc:APA91bGYGDdDqsWdKTIuzW4xgkQlreW9FVZLqEQAmNTF60fGuE2Hst-UeWpBUjTGa8Ljt9HwgHQ_4QlcioRApXfuSRLCfOtEbh-0jT-x86ffLT0ePFFUdrPLHNGh6a2b1nUTSpljwSWX\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 11:54:54'),
(107, 79, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"aparajita@ablion.in\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"79\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/WjBiWmJFb0lnRnNmUjVWeUVOZHdXZz09\",\"status\":\"true\"}]', '2019-07-15 15:11:30'),
(108, 79, 'register_device', '{\"device_id\":\"fmdAR3cANOc:APA91bGYGDdDqsWdKTIuzW4xgkQlreW9FVZLqEQAmNTF60fGuE2Hst-UeWpBUjTGa8Ljt9HwgHQ_4QlcioRApXfuSRLCfOtEbh-0jT-x86ffLT0ePFFUdrPLHNGh6a2b1nUTSpljwSWX\",\"user_id\":\"79\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 15:11:32'),
(109, 89, 'authentication', '{\"username\":\"8016046414\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 17:03:37'),
(110, 89, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\",\"device_id\":\"dNuoIVdb-Yg:APA91bGya1KiqRIM9TvaVT-n-cBQACLK9PXn9EMnSgZnLSUZJSImbOihMJ5FXy_xTcxSY6oLzA9oWnuRfH0FTdiPndPtnumZOpXorkyvbURfDPB9QPCWiMQh7sCxan_p8MJR8UEW63Ds\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 17:03:39'),
(111, 89, 'authentication', '{\"username\":\"8016046414\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 17:04:57'),
(112, 89, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\",\"device_id\":\"cObFzpgb4WQ:APA91bELA-4idqBoUTrLzfn-14oGrsIMG7bShnn7eEoGGZbPiHP-TPHzkq6LkKoXEmjf_TW4pzUU7neSBm-IYuAPoRBAvAfyPyVXhy2w2zQ78X83zrC1OJnfpIXPSRaYoYAG-h-0NKca\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 17:04:58'),
(113, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 18:00:32'),
(114, 89, 'register_device', '{\"device_id\":\"dj6YTA6duu0:APA91bE-w9eSup-7BkdhFTTP6GYkESEcmKDbtIL8apbcPGMJgNtWcJ55RCb9Aj8kSXY6bRtJl6chbU0CE3MBDHh1YOXEWq-kZ2u1hvEGJ6VzbzOdC85XE__YqdKcJmL5_AaAW7Ib7N1v\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 18:00:34'),
(115, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 18:01:48'),
(116, 89, 'register_device', '{\"device_id\":\"flQxf77Zcd0:APA91bHSumB9CTdSEz04fN0MoN0C4CqEjvTXU1hmbB5pN7Z4qV_kDYmdbvVpPiua4qoqFBIWL7VnVLgu-8EJqQ3TK-cJ_4qAlMwV6okeN6pK3VmgckfYgJkCyCJAj1gfxTxRl1cvFhWV\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 18:01:49'),
(117, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-15 18:03:27'),
(118, 89, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 18:03:28'),
(119, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-15 20:41:41'),
(120, 8, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-15 20:41:43'),
(121, 0, 'authentication', '{\"base_os\":\"android\",\"username\":\"8016046414\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-16 11:13:33'),
(122, 89, 'authentication', '{\"base_os\":\"android\",\"username\":\"8016046414\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 11:13:42'),
(123, 89, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"ee2U5IbvpaY:APA91bE-8TG-sE5mpNG7e96UPUIW3HPUAUfWpXKVFZN6bZocuo5pKDidY85iX1M0uEAPBlqCD2uUMnIOt-NnwPIFyhR3HhTt1c87MTktX40moP2M51xqgcovLpSVhPrM3fpabYkzqLvg\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:13:43'),
(124, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 11:17:04'),
(125, 89, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:17:06'),
(126, 89, 'authentication', '{\"base_os\":\"android\",\"username\":\"8016046414\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 11:19:11'),
(127, 89, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"fh1ksdxN9_4:APA91bHi4LQaOD15Oh8TANeg9NaUqH8AgmKT2iy3-IGHcoSyxJLm-PdEc40eKbUiVJN1sEOx3opNhk2OsIo4D8moAtv_9UvsojdBmSEzltUsW1y4z5Z-ZqWot0t3Ttmhz9L2_FYQPsBs\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:19:13'),
(128, 89, 'authentication', '{\"username\":\"8016046414\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 11:35:56'),
(129, 89, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\",\"device_id\":\"f3sBRftDrtM:APA91bFK9c_Lahx7Fn8LTJR5U3xcRW4sqm15y18BP9pGGAXPVBkJn9u7_f0WgWaM-PcM1WO0dlqReNIotI3h6SaehC0TuLiKPlzQGEo6xYwOH7SvdJIdtBitzp8ygetcVLLtLk5YlH5m\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:35:57'),
(130, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-16 11:37:06'),
(131, 15, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:37:07'),
(132, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 11:37:31'),
(133, 89, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:37:32'),
(134, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-16 11:37:54'),
(135, 8, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:37:55'),
(136, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-16 11:42:55'),
(137, 15, 'register_device', '{\"device_id\":\"fGsDFcP4G28:APA91bEWWfJCvxSibo52lnCjLvon-WFVavTc_vWkIuwVQjsSPOl19cvrpU8XlXMwcMWu2fZHLdwyDv0mTDh08Hmbf_RvOo3OYenYYDM-Q13qvKo_LvVvnHSz-BqXPqbcBRyiGLGCfe65\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:42:56'),
(138, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-16 11:45:19'),
(139, 15, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:45:20'),
(140, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 11:45:51'),
(141, 89, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:45:51'),
(142, 78, 'authentication', '{\"base_os\":\"android\",\"username\":\"8768673981\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-16 11:53:28'),
(143, 78, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"fh1ksdxN9_4:APA91bHi4LQaOD15Oh8TANeg9NaUqH8AgmKT2iy3-IGHcoSyxJLm-PdEc40eKbUiVJN1sEOx3opNhk2OsIo4D8moAtv_9UvsojdBmSEzltUsW1y4z5Z-ZqWot0t3Ttmhz9L2_FYQPsBs\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 11:53:29'),
(144, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 12:10:01'),
(145, 89, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 12:10:03'),
(146, 89, 'authentication', '{\"base_os\":\"android\",\"username\":\"8016046414 \",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 12:26:21'),
(147, 89, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"fh1ksdxN9_4:APA91bHi4LQaOD15Oh8TANeg9NaUqH8AgmKT2iy3-IGHcoSyxJLm-PdEc40eKbUiVJN1sEOx3opNhk2OsIo4D8moAtv_9UvsojdBmSEzltUsW1y4z5Z-ZqWot0t3Ttmhz9L2_FYQPsBs\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 12:26:22'),
(148, 89, 'authentication', '{\"base_os\":\"android\",\"username\":\"8016046414\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 12:29:08'),
(149, 89, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"fh1ksdxN9_4:APA91bHi4LQaOD15Oh8TANeg9NaUqH8AgmKT2iy3-IGHcoSyxJLm-PdEc40eKbUiVJN1sEOx3opNhk2OsIo4D8moAtv_9UvsojdBmSEzltUsW1y4z5Z-ZqWot0t3Ttmhz9L2_FYQPsBs\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 12:29:09');
INSERT INTO `tbl_api_log` (`id`, `user_id`, `function_name`, `receive_data`, `response_data`, `transaction_time`) VALUES
(150, 78, 'authentication', '{\"base_os\":\"android\",\"username\":\"8768673981\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-16 12:48:47'),
(151, 78, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"fh1ksdxN9_4:APA91bHi4LQaOD15Oh8TANeg9NaUqH8AgmKT2iy3-IGHcoSyxJLm-PdEc40eKbUiVJN1sEOx3opNhk2OsIo4D8moAtv_9UvsojdBmSEzltUsW1y4z5Z-ZqWot0t3Ttmhz9L2_FYQPsBs\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 12:48:49'),
(152, 78, 'authentication', '{\"base_os\":\"android\",\"username\":\"8768673981\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-16 13:07:57'),
(153, 78, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"c6JU3eGlSq8:APA91bGw57cvatRmJjv715HnYl2lSj5SMJl-vv1c37lbZNI8SCB9Amlq17CVHyTqq1R2zVt7i19LV8nr__0twFbo6pUcKttp-tx3LO02rJjg_iAkzSs7EZ3272Bv2SMUhTiB7NYD_z6W\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 13:07:57'),
(154, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-16 13:33:13'),
(155, 8, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 13:33:14'),
(156, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-16 15:29:16'),
(157, 8, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 15:29:18'),
(158, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-16 16:08:41'),
(159, 8, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 16:08:42'),
(160, 97, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9851661168\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"97\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/c2ZiRGVvbnQ3aTIxbDR1aDlvZUNYUT09\",\"status\":\"true\"}]', '2019-07-16 17:30:36'),
(161, 97, 'register_device', '{\"device_id\":\"dSiGMk9z1zs:APA91bGPz-BfTU2EDLMApDLdgfrlKdnRtMeUv87TeZbwEg2d1nz5-p0LhkUlq5qSrX5wDGG4U6h0PwU06wT3gx3me7M5AD-UHONlNgPTBLvKb3W-yT0ZRtkkc70Ik5DpTeqJtBQEP2Zu\",\"user_id\":\"97\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 17:30:37'),
(162, 78, 'authentication', '{\"username\":\"8768673981\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-16 17:56:14'),
(163, 78, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\",\"device_id\":\"dLXCyiTMgT8:APA91bFegPZHgr-V3qw1vGAbjzd1IRm4NnH1eKtvQBz8nsw4pnijWzcQ2gmUOdnhJikYbjSGGFlg8jGxX0FCm66wSlFN5gi8f2Fa1cKoqcRJUM0PKGXpExdkw12RJPw6Ejp1c5DKrS1A\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 17:56:16'),
(164, 89, 'authentication', '{\"base_os\":\"android\",\"username\":\"8016046414 \",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 18:02:43'),
(165, 89, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"c6JU3eGlSq8:APA91bGw57cvatRmJjv715HnYl2lSj5SMJl-vv1c37lbZNI8SCB9Amlq17CVHyTqq1R2zVt7i19LV8nr__0twFbo6pUcKttp-tx3LO02rJjg_iAkzSs7EZ3272Bv2SMUhTiB7NYD_z6W\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 18:02:44'),
(166, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-16 18:59:14'),
(167, 8, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 18:59:16'),
(168, 89, 'authentication', '{\"username\":\"8016046414\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-16 19:13:40'),
(169, 89, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\",\"device_id\":\"dLXCyiTMgT8:APA91bFegPZHgr-V3qw1vGAbjzd1IRm4NnH1eKtvQBz8nsw4pnijWzcQ2gmUOdnhJikYbjSGGFlg8jGxX0FCm66wSlFN5gi8f2Fa1cKoqcRJUM0PKGXpExdkw12RJPw6Ejp1c5DKrS1A\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-16 19:13:41'),
(170, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-17 10:49:11'),
(171, 15, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 10:49:13'),
(172, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-17 10:49:42'),
(173, 15, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 10:49:43'),
(174, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016947414\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-17 10:50:08'),
(175, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016047414\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-17 10:50:17'),
(176, 89, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"8016046414\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-17 10:50:26'),
(177, 89, 'register_device', '{\"device_id\":\"cIOe2mPhbJU:APA91bFFSuVo-lLH4NzlJ1rhiNfrWyAPukxY1Oq0DevJAJmJH9Y-tmLrieS8ndk6SvCxtzA8bQ9Pba9srm1636ka9dKyoDz2FuSR-iN1Jt-zVTY0YiNxOE2Ist_xf1N2sM2Ldk6Z1ICQ\",\"user_id\":\"89\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 10:50:27'),
(178, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-17 10:58:15'),
(179, 8, 'register_device', '{\"device_id\":\"fYI-QxHeb0o:APA91bFl2G7Iz8RBxv-XdT7TrOs-sTwup2e6Rn7K0_E3ozpKs3V3mVtbN036_9lWiYuff5aKi_iyfFs97OIKAP_miC2-yMwSOHrxOmGeOHU17oIUZC7h4oQy8iDr41vU4Up3RljORqwZ\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 10:58:16'),
(180, 85, 'authentication', '{\"base_os\":\"android\",\"username\":\"9999999999\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"85\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/SG5Sd2NQV2M3ZTRQZWhDNElhd09GQT09\",\"status\":\"true\"}]', '2019-07-17 11:11:13'),
(181, 85, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"cv_j35s7o-4:APA91bFF07P5y3t9LNZ5jVIsTq2GOSztjBSX9T4Qo2e9wl95s5obproc2S5Fi_0AgUWHy4hU-IxjqAa_mlpupoK_HNiJ5s_mv0yzzKrGEftsMmGxYrE8aLFJvzLrL_ry-01eTRAF8pKY\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"85\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 11:11:14'),
(182, 30, 'authentication', '{\"username\":\"apu\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"30\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/bEFmTnlSeGllTFdXcFJRUU5RaXRIQT09\",\"status\":\"true\"}]', '2019-07-17 11:16:31'),
(183, 30, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"30\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 11:16:31'),
(184, 8, 'authentication', '{\"base_os\":\"android\",\"username\":\"biplob\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-17 11:16:32'),
(185, 8, 'register_device', '{\"device_type\":\"android\",\"device_id\":\"cv_j35s7o-4:APA91bFF07P5y3t9LNZ5jVIsTq2GOSztjBSX9T4Qo2e9wl95s5obproc2S5Fi_0AgUWHy4hU-IxjqAa_mlpupoK_HNiJ5s_mv0yzzKrGEftsMmGxYrE8aLFJvzLrL_ry-01eTRAF8pKY\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"8\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 11:16:33'),
(186, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-17 11:20:48'),
(187, 8, 'register_device', '{\"device_id\":\"eOiZyr097G0:APA91bEky3EFvgZn9ABI92-DWDk4vR7O6o-bisj-lL1rYwih1bWWBnsi2Dh2sNsztMExKRaTy1b-pzNxAXcJ1KJWMZ66Ta1uQjksKMFaq-cyHVIprgF8821F87gWrqSp3mzcmUIEtNlg\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 11:20:49'),
(188, 0, 'authentication', '{\"username\":\"apu\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-17 13:04:55'),
(189, 0, 'authentication', '{\"username\":\"apu\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-17 13:05:15'),
(190, 30, 'authentication', '{\"username\":\"apu\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"30\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/bEFmTnlSeGllTFdXcFJRUU5RaXRIQT09\",\"status\":\"true\"}]', '2019-07-17 13:05:28'),
(191, 30, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"30\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:05:30'),
(192, 44, 'authentication', '{\"username\":\"7063605278\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"44\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UmpkUnRZa3ZweTVQQ3RYODd3TSttZz09\",\"status\":\"true\"}]', '2019-07-17 13:08:12'),
(193, 44, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"44\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:08:13'),
(194, 78, 'authentication', '{\"username\":\"8768673981\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-17 13:13:29'),
(195, 78, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:13:30'),
(196, 89, 'authentication', '{\"username\":\"8016046414\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-17 13:15:37'),
(197, 89, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:15:38'),
(198, 89, 'authentication', '{\"username\":\"8016046414\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"89\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NDlNVkpUc1U0NHpwRlRPQkZjdUlLZz09\",\"status\":\"true\"}]', '2019-07-17 13:16:40'),
(199, 89, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"89\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:16:41'),
(200, 30, 'authentication', '{\"username\":\"apu\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"30\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/bEFmTnlSeGllTFdXcFJRUU5RaXRIQT09\",\"status\":\"true\"}]', '2019-07-17 13:20:15'),
(201, 30, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"30\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:20:16'),
(202, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-17 13:25:11'),
(203, 15, 'register_device', '{\"device_id\":\"eOiZyr097G0:APA91bEky3EFvgZn9ABI92-DWDk4vR7O6o-bisj-lL1rYwih1bWWBnsi2Dh2sNsztMExKRaTy1b-pzNxAXcJ1KJWMZ66Ta1uQjksKMFaq-cyHVIprgF8821F87gWrqSp3mzcmUIEtNlg\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:25:12'),
(204, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-17 13:29:43'),
(205, 8, 'register_device', '{\"device_id\":\"eOiZyr097G0:APA91bEky3EFvgZn9ABI92-DWDk4vR7O6o-bisj-lL1rYwih1bWWBnsi2Dh2sNsztMExKRaTy1b-pzNxAXcJ1KJWMZ66Ta1uQjksKMFaq-cyHVIprgF8821F87gWrqSp3mzcmUIEtNlg\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 13:29:45'),
(206, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-17 14:36:27'),
(207, 15, 'register_device', '{\"device_id\":\"eOiZyr097G0:APA91bEky3EFvgZn9ABI92-DWDk4vR7O6o-bisj-lL1rYwih1bWWBnsi2Dh2sNsztMExKRaTy1b-pzNxAXcJ1KJWMZ66Ta1uQjksKMFaq-cyHVIprgF8821F87gWrqSp3mzcmUIEtNlg\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 14:36:28'),
(208, 15, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"9749552668\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"15\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/RUZLbEpDRGpRZEwwQ0Q5R01VYWFlZz09\",\"status\":\"true\"}]', '2019-07-17 15:36:12'),
(209, 15, 'register_device', '{\"device_id\":\"cc3nWT3iciY:APA91bGxiv8vuUD_60J3K8Bxu3MNKXBEQCjyuhpOiZfW0SSS2AEXHVbbPdFL-ZEuPDpkT0EQ8xNw3zEglyfdsrKxxYVW3hw-b7mMSadd6EHxk6uuXKBbJmhsK4TJ4UXi_ksTGd9Oes4I\",\"user_id\":\"15\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 15:36:14'),
(210, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-17 15:38:48'),
(211, 8, 'register_device', '{\"device_id\":\"cc3nWT3iciY:APA91bGxiv8vuUD_60J3K8Bxu3MNKXBEQCjyuhpOiZfW0SSS2AEXHVbbPdFL-ZEuPDpkT0EQ8xNw3zEglyfdsrKxxYVW3hw-b7mMSadd6EHxk6uuXKBbJmhsK4TJ4UXi_ksTGd9Oes4I\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 15:38:49'),
(212, 44, 'authentication', '{\"username\":\"7063605278\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"44\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UmpkUnRZa3ZweTVQQ3RYODd3TSttZz09\",\"status\":\"true\"}]', '2019-07-17 16:00:00'),
(213, 44, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"44\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 16:00:00'),
(214, 30, 'authentication', '{\"username\":\"apu\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"30\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/bEFmTnlSeGllTFdXcFJRUU5RaXRIQT09\",\"status\":\"true\"}]', '2019-07-17 16:40:48'),
(215, 30, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"30\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 16:40:49'),
(216, 78, 'authentication', '{\"username\":\"8768673981\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-17 17:11:46'),
(217, 78, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 17:11:48'),
(218, 8, 'authentication', '{\"username\":\"biplob\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-17 17:12:34'),
(219, 8, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"8\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 17:12:35'),
(220, 78, 'authentication', '{\"username\":\"8768673981\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-17 17:15:41'),
(221, 78, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\",\"device_id\":\"cJaEOdyqemU:APA91bFbNa4xtcSE4PB1w8SSm7RgVF29YJD99eaNbM3-qjIeZlCxdQXyvt8GEQV7WENXI6FXiOxpCsHE0FlpbCMcUp9_BV5xXIMWfi9SGCqDv92h5G0XbctB18JEG_VAMyItmjqB7c9Z\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 17:15:41'),
(222, 30, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"apu\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"30\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/bEFmTnlSeGllTFdXcFJRUU5RaXRIQT09\",\"status\":\"true\"}]', '2019-07-17 17:21:15'),
(223, 30, 'register_device', '{\"device_id\":\"eOiZyr097G0:APA91bEky3EFvgZn9ABI92-DWDk4vR7O6o-bisj-lL1rYwih1bWWBnsi2Dh2sNsztMExKRaTy1b-pzNxAXcJ1KJWMZ66Ta1uQjksKMFaq-cyHVIprgF8821F87gWrqSp3mzcmUIEtNlg\",\"user_id\":\"30\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-17 17:21:16'),
(224, 0, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"7602358125\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-07-18 12:57:26'),
(225, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-18 13:15:36'),
(226, 8, 'register_device', '{\"device_id\":\"eOiZyr097G0:APA91bEky3EFvgZn9ABI92-DWDk4vR7O6o-bisj-lL1rYwih1bWWBnsi2Dh2sNsztMExKRaTy1b-pzNxAXcJ1KJWMZ66Ta1uQjksKMFaq-cyHVIprgF8821F87gWrqSp3mzcmUIEtNlg\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-18 13:15:36'),
(227, 8, 'authentication', '{\"key\":\"20Bidyaaly@abl04\",\"base_os\":\"android\",\"username\":\"biplob\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"8\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/NEs4UU04UXZLc0pZd292L0VHUDRDUT09\",\"status\":\"true\"}]', '2019-07-18 14:56:53'),
(228, 8, 'register_device', '{\"device_id\":\"dk_L0lQVrJQ:APA91bFdsYt1FsaK-hM_F3wLK3UFHKMaoc9CQSV2t_1ATn40nbRCQ3Y7rSeiC5EUc9B6zq3pR0kQ_M3p1XOLr8coBUfHrCYd4inIYEhk3xMz9gIkXNhI_4jmmY_k7gf99qFyHNrn0r2l\",\"user_id\":\"8\",\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-18 14:56:55'),
(229, 78, 'authentication', '{\"username\":\"8768673981\",\"base_os\":\"android\",\"key\":\"20Bidyaaly@abl04\"}', '[{\"msg\":\"validation Success\",\"user_id\":\"78\",\"login_link\":\"https:\\/\\/staging.bidyaaly.com\\/app\\/app_login\\/UTdjVjQ0NHZxVEk0dDZjcVhFQWc0dz09\",\"status\":\"true\"}]', '2019-07-18 16:06:24'),
(230, 78, 'register_device', '{\"device_type\":\"android\",\"key\":\"20Bidyaaly@abl04\",\"user_id\":\"78\",\"device_id\":\"d32XvMyOnmQ:APA91bEXYVBeu8D5-i2u2iFcmTUYy5Qn5VZQY9Jy8XeaiqaC2RPTf1Idmi7md16yp0JiYUCUH5Z-nIBrNAO8O0l1wRbw8TfESeDherFLI_bU_QwBNJDHy1lxCoszizNi3TfFCBDLdZWN\"}', '[{\"msg\":\"Device Successfully Registered\",\"status\":\"true\"}]', '2019-07-18 16:06:25'),
(231, 0, 'authentication', '{\"key\":\"Abl04Bidyaaly19\",\"username\":\"8016046414\"}', '[{\"msg\":\"Invalid username and password\",\"status\":\"false\"}]', '2019-09-02 11:07:27'),
(232, 0, 'authentication', '{\"key\":\"Abl04Bidyaaly19\",\"username\":\"biplob\"}', '', '2019-10-01 11:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_app_maintenance`
--

CREATE TABLE `tbl_app_maintenance` (
  `id` int(11) NOT NULL,
  `maintenance_mode` tinyint(4) NOT NULL COMMENT '0 = False, 1 = True',
  `maintenance_message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_app_maintenance`
--

INSERT INTO `tbl_app_maintenance` (`id`, `maintenance_mode`, `maintenance_message`) VALUES
(1, 0, 'App is under maintenance mode. Please visit again after 5 hours.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `attendance_status` int(11) NOT NULL COMMENT '1=present,2=absent',
  `publish_data` int(11) NOT NULL COMMENT '0=not published, 1=published',
  `attendance_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`id`, `school_id`, `class_id`, `section_id`, `period_id`, `student_id`, `attendance_status`, `publish_data`, `attendance_time`) VALUES
(1, 1, 1, 2, 9, 1, 1, 0, 1558959066),
(2, 1, 1, 2, 9, 2, 2, 0, 1558959066),
(4, 1, 1, 2, 14, 1, 1, 0, 1558959180),
(5, 1, 1, 2, 14, 2, 2, 0, 1558959180),
(7, 1, 1, 2, 39, 1, 2, 0, 1559020440),
(8, 1, 1, 2, 39, 2, 1, 0, 1559020440),
(10, 1, 1, 2, 53, 1, 1, 0, 1559105343),
(11, 1, 1, 2, 53, 2, 1, 0, 1559105343),
(13, 1, 1, 2, 54, 1, 1, 0, 1559192455),
(14, 1, 1, 2, 54, 2, 2, 0, 1559192455),
(16, 1, 1, 2, 66, 1, 1, 0, 1559279362),
(17, 1, 1, 2, 66, 2, 1, 0, 1559279362),
(19, 1, 1, 2, 72, 1, 2, 0, 1559288609),
(20, 1, 1, 2, 72, 2, 1, 0, 1559288609),
(22, 1, 1, 2, 39, 1, 1, 0, 1559624805),
(23, 1, 1, 2, 39, 2, 1, 0, 1559624805),
(25, 1, 1, 2, 39, 6, 2, 0, 1559624805),
(26, 1, 1, 2, 39, 7, 2, 0, 1559624805),
(27, 1, 1, 2, 54, 1, 1, 0, 1559827626),
(28, 1, 1, 2, 54, 2, 1, 0, 1559827626),
(30, 1, 1, 2, 54, 6, 1, 0, 1559827626),
(31, 1, 1, 2, 54, 7, 1, 0, 1559827626),
(32, 1, 1, 2, 66, 1, 1, 0, 1559884047),
(33, 1, 1, 2, 66, 2, 1, 0, 1559884047),
(35, 1, 1, 2, 66, 6, 1, 0, 1559884047),
(36, 1, 1, 2, 66, 7, 1, 0, 1559884047),
(37, 1, 1, 2, 1, 1, 2, 1, 1560142626),
(38, 1, 1, 2, 1, 2, 1, 1, 1560142626),
(40, 1, 1, 2, 1, 6, 1, 1, 1560142626),
(41, 1, 1, 2, 1, 7, 1, 1, 1560142626),
(42, 22, 7, 29, 100, 10, 1, 0, 1560166162),
(43, 22, 7, 29, 100, 11, 2, 0, 1560166162),
(44, 22, 7, 29, 100, 12, 1, 0, 1560166162),
(45, 22, 7, 29, 100, 13, 1, 0, 1560166162),
(46, 22, 7, 29, 100, 14, 1, 0, 1560166162),
(47, 22, 7, 29, 100, 15, 1, 0, 1560166162),
(48, 22, 7, 29, 100, 17, 1, 0, 1560166162),
(49, 23, 9, 15, 92, 8, 1, 0, 1560166429),
(50, 1, 1, 2, 39, 1, 1, 1, 1560238011),
(51, 1, 1, 2, 39, 2, 1, 1, 1560238011),
(53, 1, 1, 2, 39, 6, 1, 1, 1560238011),
(54, 1, 1, 2, 39, 7, 1, 1, 1560238011),
(55, 1, 1, 2, 53, 1, 2, 1, 1560320296),
(56, 1, 1, 2, 53, 2, 2, 1, 1560320296),
(58, 1, 1, 2, 53, 6, 1, 1, 1560320296),
(59, 1, 1, 2, 53, 7, 1, 1, 1560320296),
(60, 1, 1, 2, 54, 1, 1, 1, 1560405631),
(61, 1, 1, 2, 54, 2, 1, 1, 1560405631),
(63, 1, 1, 2, 54, 6, 1, 1, 1560405631),
(64, 1, 1, 2, 54, 7, 1, 1, 1560405631),
(65, 1, 1, 2, 66, 1, 1, 1, 1560490349),
(66, 1, 1, 2, 66, 2, 1, 1, 1560490349),
(68, 1, 1, 2, 66, 6, 1, 1, 1560490349),
(69, 1, 1, 2, 66, 7, 1, 1, 1560490349),
(70, 22, 7, 29, 111, 10, 1, 1, 1560493695),
(71, 22, 7, 29, 111, 11, 1, 1, 1560493695),
(72, 22, 7, 29, 111, 12, 1, 1, 1560493695),
(73, 22, 7, 29, 111, 13, 2, 1, 1560493695),
(74, 22, 7, 29, 111, 14, 1, 1, 1560493695),
(75, 22, 7, 29, 111, 15, 1, 1, 1560493695),
(76, 22, 7, 29, 111, 17, 1, 1, 1560493695),
(77, 23, 9, 15, 115, 8, 1, 1, 1560495389),
(78, 23, 9, 15, 115, 18, 1, 1, 1560495389),
(79, 1, 1, 2, 1, 1, 1, 0, 1560758195),
(80, 1, 1, 2, 1, 2, 1, 0, 1560758195),
(82, 1, 1, 2, 1, 6, 1, 0, 1560758195),
(83, 1, 1, 2, 1, 7, 1, 0, 1560758195),
(84, 22, 7, 29, 105, 10, 1, 1, 1560843234),
(85, 22, 7, 29, 105, 11, 1, 1, 1560843234),
(86, 22, 7, 29, 105, 12, 1, 1, 1560843234),
(87, 22, 7, 29, 105, 13, 2, 1, 1560843234),
(88, 22, 7, 29, 105, 14, 2, 1, 1560843234),
(89, 22, 7, 29, 105, 15, 1, 1, 1560843234),
(90, 22, 7, 29, 105, 17, 1, 1, 1560843234),
(91, 1, 1, 2, 39, 1, 1, 1, 1560853234),
(92, 1, 1, 2, 39, 2, 1, 1, 1560853234),
(94, 1, 1, 2, 39, 6, 1, 1, 1560853234),
(95, 1, 1, 2, 39, 7, 1, 1, 1560853234),
(96, 1, 1, 2, 52, 1, 1, 1, 1560862199),
(97, 1, 1, 2, 52, 2, 2, 1, 1560862199),
(99, 1, 1, 2, 52, 6, 1, 1, 1560862199),
(100, 1, 1, 2, 52, 7, 1, 1, 1560862199),
(101, 22, 7, 29, 117, 10, 1, 0, 1560939599),
(102, 22, 7, 29, 117, 11, 1, 0, 1560939599),
(103, 22, 7, 29, 117, 12, 1, 0, 1560939599),
(104, 22, 7, 29, 117, 13, 2, 0, 1560939599),
(105, 22, 7, 29, 117, 14, 1, 0, 1560939599),
(106, 22, 7, 29, 117, 15, 1, 0, 1560939599),
(107, 22, 7, 29, 117, 17, 1, 0, 1560939599),
(108, 1, 1, 2, 53, 1, 2, 0, 1560945518),
(109, 1, 1, 2, 53, 2, 2, 0, 1560945518),
(111, 1, 1, 2, 53, 6, 1, 0, 1560945518),
(112, 1, 1, 2, 53, 7, 1, 0, 1560945518),
(113, 1, 1, 2, 54, 1, 1, 1, 1561016665),
(114, 1, 1, 2, 54, 2, 1, 1, 1561016665),
(116, 1, 1, 2, 54, 6, 1, 1, 1561016665),
(117, 1, 1, 2, 54, 7, 1, 1, 1561016665),
(118, 22, 7, 29, 126, 10, 1, 1, 1561029870),
(119, 22, 7, 29, 126, 11, 1, 1, 1561029870),
(120, 22, 7, 29, 126, 12, 2, 1, 1561029870),
(121, 22, 7, 29, 126, 13, 2, 1, 1561029870),
(122, 22, 7, 29, 126, 14, 1, 1, 1561029870),
(123, 22, 7, 29, 126, 15, 1, 1, 1561029870),
(124, 22, 7, 29, 126, 17, 1, 1, 1561029870),
(125, 1, 1, 2, 66, 1, 2, 1, 1561096580),
(126, 1, 1, 2, 66, 2, 2, 1, 1561096580),
(128, 1, 1, 2, 66, 6, 1, 1, 1561096580),
(129, 1, 1, 2, 66, 7, 1, 1, 1561096580),
(130, 1, 1, 2, 74, 1, 1, 1, 1561181559),
(131, 1, 1, 2, 74, 2, 1, 1, 1561181559),
(133, 1, 1, 2, 74, 6, 1, 1, 1561181559),
(134, 1, 1, 2, 74, 7, 1, 1, 1561181559),
(135, 1, 1, 2, 1, 1, 1, 1, 1561367208),
(136, 1, 1, 2, 1, 2, 1, 1, 1561367208),
(138, 1, 1, 2, 1, 6, 1, 1, 1561367208),
(139, 1, 1, 2, 1, 7, 1, 1, 1561367208),
(140, 1, 1, 2, 39, 1, 1, 1, 1561464333),
(141, 1, 1, 2, 39, 2, 1, 1, 1561464333),
(142, 1, 1, 2, 39, 6, 1, 1, 1561464333),
(143, 1, 1, 2, 39, 7, 1, 1, 1561464333),
(144, 1, 1, 2, 39, 20, 1, 1, 1561464333),
(145, 1, 1, 2, 53, 1, 1, 1, 1561529339),
(146, 1, 1, 2, 53, 2, 1, 1, 1561529339),
(147, 1, 1, 2, 53, 6, 1, 1, 1561529339),
(148, 1, 1, 2, 53, 7, 1, 1, 1561529339),
(149, 1, 1, 2, 53, 20, 1, 1, 1561529339),
(150, 22, 7, 29, 117, 10, 1, 1, 1561529620),
(151, 22, 7, 29, 117, 11, 1, 1, 1561529620),
(152, 22, 7, 29, 117, 12, 1, 1, 1561529620),
(153, 22, 7, 29, 117, 13, 1, 1, 1561529620),
(154, 22, 7, 29, 117, 14, 1, 1, 1561529620),
(155, 22, 7, 29, 117, 15, 1, 1, 1561529620),
(156, 22, 7, 29, 117, 17, 1, 1, 1561529620),
(157, 1, 1, 2, 54, 1, 2, 1, 1561617482),
(158, 1, 1, 2, 54, 2, 2, 1, 1561617482),
(159, 1, 1, 2, 54, 6, 1, 1, 1561617482),
(160, 1, 1, 2, 54, 7, 1, 1, 1561617482),
(161, 1, 1, 2, 54, 20, 1, 1, 1561617482),
(162, 22, 7, 29, 126, 10, 1, 0, 1561620560),
(163, 22, 7, 29, 126, 11, 1, 0, 1561620560),
(164, 22, 7, 29, 126, 12, 1, 0, 1561620560),
(165, 22, 7, 29, 126, 13, 2, 0, 1561620560),
(166, 22, 7, 29, 126, 14, 2, 0, 1561620560),
(167, 22, 7, 29, 126, 15, 1, 0, 1561620560),
(168, 22, 7, 29, 126, 17, 1, 0, 1561620560),
(169, 1, 1, 2, 66, 1, 1, 1, 1561717966),
(170, 1, 1, 2, 66, 2, 1, 1, 1561717966),
(171, 1, 1, 2, 66, 6, 1, 1, 1561717966),
(172, 1, 1, 2, 66, 7, 1, 1, 1561717966),
(173, 1, 1, 2, 66, 20, 1, 1, 1561717966),
(174, 1, 1, 2, 66, 21, 1, 1, 1561717966),
(175, 1, 1, 2, 66, 22, 1, 1, 1561717966),
(176, 1, 1, 2, 66, 23, 1, 1, 1561717966),
(177, 1, 1, 2, 66, 24, 1, 1, 1561717966),
(178, 1, 1, 2, 74, 1, 1, 1, 1561792906),
(179, 1, 1, 2, 74, 2, 1, 1, 1561792906),
(180, 1, 1, 2, 74, 6, 1, 1, 1561792906),
(181, 1, 1, 2, 74, 7, 1, 1, 1561792906),
(182, 1, 1, 2, 74, 20, 1, 1, 1561792906),
(183, 1, 1, 2, 74, 21, 1, 1, 1561792906),
(184, 1, 1, 2, 74, 22, 1, 1, 1561792906),
(185, 1, 1, 2, 74, 23, 1, 1, 1561792906),
(186, 1, 1, 2, 74, 24, 1, 1, 1561792906),
(187, 1, 1, 2, 1, 1, 1, 1, 1561963109),
(188, 1, 1, 2, 1, 2, 1, 1, 1561963109),
(189, 1, 1, 2, 1, 6, 1, 1, 1561963109),
(190, 1, 1, 2, 1, 7, 1, 1, 1561963109),
(191, 1, 1, 2, 1, 20, 1, 1, 1561963109),
(192, 1, 1, 2, 1, 21, 1, 1, 1561963109),
(193, 1, 1, 2, 1, 22, 1, 1, 1561963109),
(194, 1, 1, 2, 1, 23, 1, 1, 1561963109),
(195, 1, 1, 2, 1, 24, 1, 1, 1561963109),
(196, 22, 7, 29, 117, 10, 1, 1, 1562153603),
(197, 22, 7, 29, 117, 11, 1, 1, 1562153603),
(198, 22, 7, 29, 117, 12, 1, 1, 1562153603),
(199, 22, 7, 29, 117, 13, 1, 1, 1562153603),
(200, 22, 7, 29, 117, 14, 1, 1, 1562153603),
(201, 22, 7, 29, 117, 15, 2, 1, 1562153603),
(202, 22, 7, 29, 117, 17, 1, 1, 1562153603),
(203, 1, 1, 2, 53, 1, 1, 1, 1562160190),
(204, 1, 1, 2, 53, 2, 1, 1, 1562160190),
(205, 1, 1, 2, 53, 6, 1, 1, 1562160190),
(206, 1, 1, 2, 53, 7, 1, 1, 1562160190),
(207, 1, 1, 2, 53, 20, 1, 1, 1562160190),
(208, 1, 1, 2, 53, 21, 1, 1, 1562160190),
(209, 1, 1, 2, 53, 22, 1, 1, 1562160190),
(210, 1, 1, 2, 53, 23, 1, 1, 1562160190),
(211, 1, 1, 2, 53, 24, 1, 1, 1562160190),
(212, 1, 1, 2, 54, 1, 1, 1, 1562219677),
(213, 1, 1, 2, 54, 2, 2, 1, 1562219677),
(214, 1, 1, 2, 54, 6, 1, 1, 1562219677),
(215, 1, 1, 2, 54, 7, 1, 1, 1562219677),
(216, 1, 1, 2, 54, 20, 1, 1, 1562219677),
(217, 1, 1, 2, 54, 21, 1, 1, 1562219677),
(218, 1, 1, 2, 54, 22, 1, 1, 1562219677),
(219, 1, 1, 2, 54, 23, 1, 1, 1562219677),
(220, 1, 1, 2, 54, 24, 1, 1, 1562219677),
(221, 1, 1, 2, 39, 1, 1, 1, 1562667757),
(222, 1, 1, 2, 39, 2, 1, 1, 1562667757),
(223, 1, 1, 2, 39, 6, 1, 1, 1562667757),
(224, 1, 1, 2, 39, 7, 1, 1, 1562667757),
(225, 1, 1, 2, 39, 20, 1, 1, 1562667757),
(226, 1, 1, 2, 39, 21, 1, 1, 1562667757),
(227, 1, 1, 2, 39, 22, 1, 1, 1562667757),
(228, 1, 1, 2, 39, 23, 1, 1, 1562667757),
(229, 1, 1, 2, 39, 24, 1, 1, 1562667757),
(230, 22, 7, 29, 105, 10, 1, 0, 1562670690),
(231, 22, 7, 29, 105, 11, 1, 0, 1562670690),
(232, 22, 7, 29, 105, 12, 1, 0, 1562670690),
(233, 22, 7, 29, 105, 13, 1, 0, 1562670690),
(234, 22, 7, 29, 105, 14, 1, 0, 1562670690),
(235, 22, 7, 29, 105, 15, 1, 0, 1562670691),
(236, 22, 7, 29, 105, 17, 1, 0, 1562670691),
(237, 1, 1, 1, 51, 16, 1, 1, 1562670930),
(238, 1, 1, 2, 1, 1, 1, 1, 1563192899),
(239, 1, 1, 2, 1, 2, 1, 1, 1563192899),
(240, 1, 1, 2, 1, 6, 1, 1, 1563192899),
(241, 1, 1, 2, 1, 7, 1, 1, 1563192899),
(242, 1, 1, 2, 1, 20, 1, 1, 1563192899),
(243, 1, 1, 2, 1, 21, 1, 1, 1563192899),
(244, 1, 1, 2, 1, 22, 1, 1, 1563192899),
(245, 1, 1, 2, 1, 23, 1, 1, 1563192899),
(246, 1, 1, 2, 1, 24, 1, 1, 1563192899),
(247, 1, 1, 2, 1, 25, 1, 1, 1563192899),
(248, 1, 1, 2, 1, 1, 1, 1, 1563791307),
(249, 1, 1, 2, 1, 2, 1, 1, 1563791307),
(250, 1, 1, 2, 1, 6, 1, 1, 1563791307),
(251, 1, 1, 2, 1, 7, 1, 1, 1563791307),
(252, 1, 1, 2, 1, 20, 1, 1, 1563791307),
(253, 1, 1, 2, 1, 21, 1, 1, 1563791307),
(254, 1, 1, 2, 1, 22, 1, 1, 1563791307),
(255, 1, 1, 2, 1, 23, 1, 1, 1563791307),
(256, 1, 1, 2, 1, 24, 1, 1, 1563791307),
(257, 1, 1, 2, 1, 25, 1, 1, 1563791307),
(258, 1, 1, 2, 1, 27, 1, 1, 1563791307),
(259, 1, 1, 2, 1, 28, 1, 1, 1563791307),
(260, 1, 1, 2, 39, 1, 1, 0, 1563888666),
(261, 1, 1, 2, 39, 2, 2, 0, 1563888666),
(262, 1, 1, 2, 39, 6, 2, 0, 1563888666),
(263, 1, 1, 2, 39, 7, 2, 0, 1563888666),
(264, 1, 1, 2, 39, 20, 2, 0, 1563888666),
(265, 1, 1, 2, 39, 21, 2, 0, 1563888666),
(266, 1, 1, 2, 39, 22, 2, 0, 1563888666),
(267, 1, 1, 2, 39, 23, 2, 0, 1563888666),
(268, 1, 1, 2, 39, 24, 2, 0, 1563888666),
(269, 1, 1, 2, 39, 25, 2, 0, 1563888666),
(270, 1, 1, 2, 39, 27, 2, 0, 1563888666),
(271, 1, 1, 2, 39, 28, 2, 0, 1563888666),
(272, 1, 1, 2, 39, 29, 2, 0, 1563888666),
(273, 1, 1, 2, 53, 1, 1, 0, 1563951856),
(274, 1, 1, 2, 53, 2, 2, 0, 1563951856),
(275, 1, 1, 2, 53, 6, 2, 0, 1563951856),
(276, 1, 1, 2, 53, 7, 2, 0, 1563951856),
(277, 1, 1, 2, 53, 20, 2, 0, 1563951856),
(278, 1, 1, 2, 53, 21, 2, 0, 1563951856),
(279, 1, 1, 2, 53, 22, 2, 0, 1563951856),
(280, 1, 1, 2, 53, 23, 2, 0, 1563951856),
(281, 1, 1, 2, 53, 24, 2, 0, 1563951856),
(282, 1, 1, 2, 53, 25, 2, 0, 1563951856),
(283, 1, 1, 2, 53, 27, 2, 0, 1563951856),
(284, 1, 1, 2, 53, 28, 2, 0, 1563951856),
(285, 1, 1, 2, 53, 29, 2, 0, 1563951856),
(429, 1, 1, 2, 1, 1, 1, 1, 1564943400),
(430, 1, 1, 2, 1, 2, 1, 1, 1564943400),
(431, 1, 1, 2, 1, 6, 1, 1, 1564943400),
(432, 1, 1, 2, 1, 7, 1, 1, 1564943400),
(433, 1, 1, 2, 1, 20, 1, 1, 1564943400),
(434, 1, 1, 2, 1, 21, 1, 1, 1564943400),
(435, 1, 1, 2, 1, 22, 1, 1, 1564943400),
(436, 1, 1, 2, 1, 23, 1, 1, 1564943400),
(437, 1, 1, 2, 1, 24, 1, 1, 1564943400),
(438, 1, 1, 2, 1, 25, 1, 1, 1564943400),
(439, 1, 1, 2, 53, 1, 1, 1, 1566325800),
(440, 1, 1, 2, 53, 2, 1, 1, 1566325800),
(441, 1, 1, 2, 53, 6, 1, 1, 1566325800),
(442, 1, 1, 2, 53, 7, 1, 1, 1566325800),
(443, 1, 1, 2, 53, 20, 1, 1, 1566325800),
(444, 1, 1, 2, 53, 21, 1, 1, 1566325800),
(445, 1, 1, 2, 53, 22, 1, 1, 1566325800),
(446, 1, 1, 2, 53, 23, 1, 1, 1566325800),
(447, 1, 1, 2, 53, 24, 1, 1, 1566325800),
(448, 1, 1, 2, 53, 25, 1, 1, 1566325800),
(510, 1, 1, 2, 74, 1, 1, 1, 1568399400),
(511, 1, 1, 2, 74, 2, 1, 1, 1568399400),
(512, 1, 1, 2, 74, 6, 2, 1, 1568399400),
(513, 1, 1, 2, 74, 7, 2, 1, 1568399400),
(514, 1, 1, 2, 74, 20, 2, 1, 1568399400),
(515, 1, 1, 2, 74, 21, 2, 1, 1568399400),
(516, 1, 1, 2, 74, 22, 2, 1, 1568399400),
(517, 1, 1, 2, 74, 23, 2, 1, 1568399400),
(518, 1, 1, 2, 74, 24, 2, 1, 1568399400),
(519, 1, 1, 2, 74, 25, 2, 1, 1568399400),
(520, 1, 1, 2, 74, 30, 2, 1, 1568399400),
(521, 1, 1, 2, 74, 34, 2, 1, 1568399400),
(522, 157, 25, 58, 144, 37, 2, 1, 1570559400),
(523, 157, 25, 58, 144, 38, 1, 1, 1570559400),
(525, 169, 28, 61, 147, 41, 1, 1, 1571250600),
(526, 169, 28, 61, 148, 41, 1, 1, 1571337000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ci_sessions`
--

CREATE TABLE `tbl_ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `timestamp` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tbl_ci_sessions`
--

INSERT INTO `tbl_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('p281iamqk5db6oke59nils4vhmguai82', '192.168.1.109', 1555938178, ''),
('kl8832frosl35c79k4ab3giqaa2nst99', '192.168.1.109', 1556026214, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('enhncirn2c5hfn6safi59cojelue33e5', '192.168.1.109', 1556199907, 'timetable_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGltZXRhYmxlIHdoZXJlIHNjaG9vbF9pZCA9ICcxJw==\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";period_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfcGVyaW9kcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('fe85k2guqftg83o5af7dgtuq1al7a7m7', '192.168.1.109', 1556111858, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";period_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfcGVyaW9kcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";timetable_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGltZXRhYmxlIHdoZXJlIHNjaG9vbF9pZCA9ICcxJw==\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('k68s79fdeh831otkfjakvose37lee7qa', '192.168.1.109', 1556276553, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('er4raa8qaqkeg0v7okmr90qa7hjhsqg3', '192.168.1.109', 1556289853, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('lgoe5afouha75oguhgb6dc398sqcejn3', '192.168.1.109', 1556370012, 'classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('1qjt56p1tc6qfc2qe1hms4si3qiuej20', '192.168.1.107', 1556360694, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('udjv1qi81lv4fqrdsajc5ek1ik2a8u2n', '192.168.1.109', 1556779739, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('p9r1tke6qj71egtlav2l4chsuq8bpe8d', '192.168.1.109', 1556802589, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('p0u0tmj1gvqrrif5dqg867fosfjmcpkd', '192.168.1.109', 1556884873, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;s_message|s:29:\"Student successfully created.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('5bpvus78s3d60i8okinrane0m1vqimkk', '192.168.1.109', 1557120033, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('3meae5cn7k88cb3tqmjig3nipcajinv4', '192.168.1.109', 1557148907, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('mrmcukfd95fd6o8rt1v72bpgohouto4e', '192.168.1.107', 1557148813, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('jduj0i3nbt20bk8bio22pepsfkmagjbp', '192.168.1.109', 1557235486, 'classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMic=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMic=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('fdvulk489e1hhihnm9npmre4hjqrl65l', '192.168.1.111', 1557211588, ''),
('tk553epo3oajqrpheed0evgiqcmd28hm', '192.168.1.138', 1557230927, 'classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('5e4sh2t6603i4ggcd2518e0ahje2m8io', '192.168.1.110', 1557232015, ''),
('3kb2m464hkaj7e013vvofhr5phmp2qq6', '192.168.1.110', 1557231894, ''),
('s4ef3nnllq5jd26oooa6dqbr1k0222um', '192.168.1.138', 1557238973, ''),
('tnhjq76mvoai9m635i8vsthb96pvkgnq', '192.168.1.110', 1557290164, ''),
('ll5a2funfdbe6v6671ke36buihhuo32h', '192.168.1.111', 1557292773, ''),
('phg58ekiouj9hududetullls577ghqqs', '192.168.1.138', 1557306233, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('ubulno35pn2vbjmjt76ee20622011bni', '192.168.1.138', 1557314478, ''),
('nd8kvthq9q6fr35sp35psb3e4nviisq2', '192.168.1.138', 1557315650, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('mamqe9bm2cuof0lak030qmscrfg47sr5', '192.168.1.109', 1557378154, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('3thjtd673vrtat79v13k1rlnj7vl0mbi', '192.168.1.109', 1557407041, 'school_id|s:1:\"1\";school_username|s:5:\"96431\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_id|s:1:\"1\";'),
('d6lp7u5ce0s7b3qc8gm57evc5imqapj4', '192.168.1.109', 1557470359, 'teacher_id|s:1:\"1\";school_is_logged_in|i:1;'),
('tsbemdjlot8rk0vs005s70djps4f2rsq', '192.168.1.109', 1557494771, 'teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_is_logged_in|i:1;school_id|s:1:\"1\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('s444og5pf19obm7fo0iqmu1emmha9r3i', '192.168.1.109', 1557560058, 'school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_id|s:1:\"8\";'),
('qe9ba1e6eculp1f26kcv25jrmq78nm3a', '192.168.1.67', 1557752389, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('ql16aegspiac68k1mil6qpmf904u3i1b', '192.168.1.109', 1557751871, 'school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_id|s:1:\"8\";'),
('qv2ifkq18h6og1gq3b9j4gt06efcke98', '192.168.1.138', 1557746609, 'school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('0mdjpe9s2omco3vjv8pk3ki73qjjhain', '192.168.1.110', 1557751841, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('bh72d99pq56fromihu7eq083ho3sd5p8', '192.168.1.109', 1557753697, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('235i488in5p4dn8m3brj2ehqv6eso60p', '192.168.1.110', 1557752503, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('783k0fu9tqarr5f6cjs6kh8ee63dgb5m', '192.168.1.110', 1557752532, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('vcdjerl3hk61ur1kh20nbnml7f33q8a1', '192.168.1.138', 1557755346, ''),
('s8o73pkkmielchl38uoff2biarddgdk2', '192.168.1.138', 1557755304, ''),
('u722tsdd1h85c3f7rbhei2irdu98fg6e', '192.168.1.138', 1557756519, 'school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('rpo2b7lam7jnqrohicnu4tdgsm2eliuo', '192.168.1.110', 1557821338, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('0dp3k8talc6jvhkb7motpvai2t0fmnie', '192.168.1.109', 1557809539, ''),
('f4i9k4e01ueu84ja001o962uphfh5pvt', '192.168.1.109', 1557838377, 'teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";usertype|s:1:\"1\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzgn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnOCc=\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('p0c05mpe8phvfhvmefol7tt78kil8gis', '192.168.1.110', 1557838383, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('uvpd92lm6s5pigibl1dpm17rjlb8sbqd', '192.168.1.110', 1557829992, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('5ukv04n0mqt9an3oj71bgj53f7o2e0tp', '192.168.1.110', 1557915853, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('it8lr01vcq0gqnm0vpbgvug3hcu37qeq', '192.168.1.109', 1557895809, ''),
('2ubs38utri1thi8anq0mt24o4el15hl8', '192.168.1.109', 1557926990, 'usertype|s:1:\"1\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;school_username|s:5:\"95340\";school_email|s:6:\"biplob\";'),
('ma8q3sv6tb773sp1h2rlpgf9rrh2ocpt', '192.168.1.110', 1557912921, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('jp0l4cn49tl5kc4icebtlkvtn7hairau', '192.168.1.108', 1557903246, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('u6o39n2qvplemsu2fa0r5gq1uvrvc7cp', '192.168.1.97', 1557905694, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('o3phuelaqlkl44eh210m9os03p9udtdc', '192.168.1.97', 1557905614, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('gb8uu7g1majrp405974b3gmj1qbj83g3', '192.168.1.62', 1557908174, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('anmqv7e3na7iopkvgltbr7drgef11gs7', '192.168.1.138', 1557913269, ''),
('a4fcao68tsv00dd07bpljqohb3rc8jrc', '192.168.1.138', 1557913520, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('8fofa8gmi12r4nkvbno8m3ehajjb2cl6', '192.168.1.97', 1557925354, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('r7ngkj2vlcpu6aktmcri26to6fobvd4g', '192.168.1.138', 1557921833, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('3p2ud3958a2ics5kjmi4ash5a143abai', '192.168.1.110', 1557981448, ''),
('7nvhfis481k8grro96k91oockq6gf1ue', '192.168.1.66', 1557983027, ''),
('1guvvs5qc27mgthqgr9ga6tif6n9pikg', '192.168.1.109', 1557988773, ''),
('e3mkc8qgt2vuged2pkqh7mmbilkaacnj', '192.168.1.109', 1558013985, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('h839ena35sm1ig1m2tpglo9ld0kjivsb', '192.168.1.109', 1558072752, ''),
('ge2m4to3tmgg6a226d57m0hvn6j87odr', '192.168.1.109', 1558099626, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";s_message|s:28:\"Notice successfully deleted.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('g8667iv6kmpinj4gni0fk3hbu3m6oe2p', '192.168.1.109', 1558333639, ''),
('d778p22ritobk765rjdsvhkagolim0ft', '192.168.1.109', 1558358962, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('vbp1qndcj8f4csuq6ebu78vcf6lq0f7v', '192.168.1.52', 1558336491, ''),
('9jccvp9a2delvdiq27rtvoudcrik546a', '192.168.1.110', 1558437687, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('38rq6vv5cfnbtktiu40f73herua8uio3', '192.168.1.110', 1558356136, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('6oepugciu7rtanv0ge6mf50bnsm9h4q3', '192.168.1.109', 1558354330, ''),
('e8lejt4h64v0hcvfipk2q4q60b4foku0', '192.168.1.110', 1558354671, ''),
('9eptdnotrj173q9rcudpm98kngppof40', '192.168.1.110', 1558413997, ''),
('tu9av013u0hdkg824peh9kol4vanr0ed', '192.168.1.110', 1558437682, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('uotscdmfs4r6etjo4pqp9858av118dsh', '192.168.1.109', 1558429282, ''),
('1qgk9q9l8i05sd77f2pgpsiljerva88j', '192.168.1.109', 1558435611, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";student_flag|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('sorrlg46f3ah4i6h2io7klcr2d1n85dl', '192.168.1.109', 1558435625, ''),
('plurkp5v1jivs8frr1mfnrko83cg08d0', '192.168.1.109', 1558445385, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('hihuc26s752vh7qhr5k130khtbo1qck5', '192.168.1.110', 1558442271, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('cv3ghbpi84lqd6uoc8gs2pijgqganb2d', '192.168.1.110', 1558441894, ''),
('21onnpaqurrlmv9jfk8gli0nqclh8fng', '192.168.1.109', 1558505597, ''),
('9m346lpm20dlm2lg4fe2r5b0cp5vjmnh', '192.168.1.109', 1558520365, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('lv6rehm4afr8skg5aib7jgb4g40lmqq0', '192.168.1.138', 1558533089, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('g9dq45778qa41h3eb5hjp7nmr1umsr5l', '192.168.1.109', 1558536610, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('c3uhkp3mfj9qfo3dn4pb1ttur16gb50h', '192.168.1.138', 1558525023, ''),
('tf19rf1f6ummvjoq0bl7bkgk85mk51k5', '192.168.1.109', 1558604902, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('2n4bpt264cthv4dosi078n906b788dg1', '192.168.1.109', 1558590642, ''),
('huf2j2la21ngtm1amccbfvqdqv63a0a1', '192.168.1.109', 1558617370, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('uop36b6tctb5ah48ihp474r715avrnrq', '192.168.1.109', 1558597351, ''),
('r8prb90mg5h2apcobmo64r8pjrt3g6me', '192.168.1.138', 1558597807, ''),
('qu3f8m88apv1llkkvd7ghliths3r69tl', '192.168.1.138', 1558598763, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('bq1dulnkt7j1kndkjeimtl5rtnb91eep', '192.168.1.109', 1558612564, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('pjbhddbqmkrirbbqeptaterdbpp76pg3', '192.168.1.138', 1558616091, ''),
('jl471sm75krpo63m1ic02t7re0m2n0am', '192.168.1.109', 1558674198, ''),
('l05ppefpa4qh505odp86q3qid10pj56b', '192.168.1.109', 1558674204, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('mu0gulhiutlpbvsq7scn1cepq3tj38jo', '192.168.1.109', 1558674224, ''),
('8rko5pk5pj12f7hja17abldv9e6107v0', '192.168.1.109', 1558690740, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";s_message|s:27:\"Diary successfully updated.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('912n19817e9qe5gi6jgasqtdabvc1q4t', '192.168.1.133', 1558679899, ''),
('gpuljtcnhp1gpcdvbralolvtd6elhm3t', '192.168.1.100', 1558679870, ''),
('604902t7o9vjea62jo9sutf3o5bdesso', '192.168.1.100', 1558680760, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('a75t6mj4rauoa7q3o0ph3iddfgon8o97', '192.168.1.109', 1558683764, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('h3pj403oomvjpf1ncv1703m8f4gnan1a', '192.168.1.100', 1558684076, ''),
('5v7j4eiuhlmi0eldofc2qo217kvb3jmm', '192.168.1.100', 1558684303, ''),
('4e88e6fhs543g0sskr2uhm6o9ebrmjs1', '192.168.1.100', 1558690677, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('3r0at59oe1m4n4pvefj0rvvnq91hni4a', '192.168.1.109', 1558701720, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:36:\"Fees Structure successfully Deleted.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('7dpd407lembb6lsg31nsqshdf3u96gul', '192.168.1.109', 1558759592, ''),
('f10s6h5fhfg4f150odl0irsrmcvc94b2', '192.168.1.109', 1558760349, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('j8c6d53ubs6d7rgvfcmk7iqj01ukcatc', '192.168.1.109', 1558761614, ''),
('21eusq43hu8qjn4gjrkgmtu9rs4bc5n6', '192.168.1.109', 1558786127, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;class_id|s:1:\"1\";section_id|s:1:\"2\";'),
('s6ketttb4unlmb9vvvdrvgp7bhq81jcb', '192.168.1.96', 1558760197, ''),
('2jujh379ntjg1lt50vsjpghpa5vosg08', '192.168.1.138', 1558764682, ''),
('qcevufjo1dnbs9jnfmhm7m0rk0o7bnpj', '192.168.1.138', 1558788175, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_id|s:2:\"11\";s_message|s:27:\"Diary successfully updated.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('u2otdeqvp2ggjf40ru0i95kv4vchsp08', '192.168.1.109', 1558781261, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:29:\"Holiday successfully created.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('5rhc9nnu1jug6dvcdpiks44bnnim2nlp', '192.168.1.109', 1558932494, ''),
('3kvucaekphfe3tvl0l4afg49ph3tpkp1', '192.168.1.109', 1558932499, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('c1tdkvi21fkhfn1ppfgv1ecnj9u5o5bg', '192.168.1.109', 1558932532, ''),
('f4bmf19lse4ee59q9cgoi0i65vlk9sdg', '192.168.1.109', 1558934372, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('l84ar5ji3u609bkoo2ujfsptltla47qb', '192.168.1.59', 1558934535, ''),
('tbl56cjrbv5nqivlrst74nf14lvu27ut', '192.168.1.109', 1558955099, ''),
('03sn432tu4a4c9c10ir0hubp9sj45o1s', '192.168.1.109', 1558959171, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('cvgm9grg7f1ok3n47t1rg37s46k5ada5', '192.168.1.109', 1558961799, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('g0dfvl3fmblfgbvpqvks0hja1mfmsrmo', '192.168.1.109', 1559029179, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('aeqt60f91ivfmvuurvdpj2ks9bdp4pac', '192.168.1.109', 1559019765, ''),
('sntpg9m453jjg3ages9g6bprmd8mrjf2', '192.168.1.109', 1559019869, ''),
('qoko376fvvoqdp2l5q698hi6vq14qkbi', '192.168.1.109', 1559029419, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('42f6v5929e5t4osjq6u4o8b9q2kkc89a', '192.168.1.110', 1559049107, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('0osuj887lvfarqhqo19s8epk6aai9omv', '192.168.1.109', 1559041329, ''),
('047nfi34itn5aobf6sni2h7n6noo46rk', '192.168.1.109', 1559050382, 'usertype|s:1:\"1\";teacher_id|s:1:\"8\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('lkht5g69dgimnu7ptssj00ddkdih6fcb', '192.168.1.109', 1559039128, ''),
('sugv5u5ckp166sklate0uhgpe2gn1ofg', '192.168.1.109', 1559039143, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('gue2ubtipifv70i7mombbeo6qp4kv8r5', '192.168.1.77', 1559039939, ''),
('loq5j1rpr5a45tjb4cd2c7gskp9uouhi', '192.168.1.110', 1559105121, ''),
('plt9qlmuud70t29r77kbehd2kko67eke', '192.168.1.110', 1559049096, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('f8hqefak4q576pb4lcdsjl34ilmardid', '192.168.1.138', 1559051658, ''),
('7kt9jsebptgjcos4iimtao6qtbkeba8t', '192.168.1.138', 1559054455, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('rrcdihus9qdpp4uqodiqda9tcotqi46g', '192.168.1.60', 1559105567, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('sij713oucblipifrebqf2g0ffgb9r6it', '192.168.1.110', 1559105407, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('cpf7pb1qflgs6r7dch5280q8op9jiebt', '192.168.1.109', 1559105808, ''),
('9u8v5ej120pnpdu5quf0mcpr9gm72s6p', '192.168.1.109', 1559109203, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('rvlvsi17aavb0cqqauhcrojvpkkloo0r', '192.168.1.110', 1559114226, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('qs8v5q7eph3mv1cd9kvplhom24ldp1rs', '192.168.1.110', 1559111134, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('hh9i7i40qakj7cr5mt0fkj7p2s98haad', '192.168.1.109', 1559109675, ''),
('dojuvuage3atdtaqkpltlb1jum0uij0g', '192.168.1.109', 1559136554, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('vabjo9ujl0s3hnp27pc5u1r14l8ipdrj', '192.168.1.110', 1559135172, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('tgs4dir483agm9tn3mtmn44n8lqmh9k4', '192.168.1.60', 1559117086, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('530tm7cu6r58ue33s0hnrfdod3g0rsvf', '192.168.1.110', 1559122213, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('9c9p3spc2jblk0ife3hu7i6lna6leeia', '192.168.1.77', 1559129718, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('qmbpdidau26q8ee85m4hcspf9dgfdt9l', '192.168.1.110', 1559134540, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('r4ag57il87ori2gcp4v0fdvm6kbsq3tq', '192.168.1.138', 1559132403, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('5rh25940fdh668h5mdsku84n0k7dv563', '192.168.1.138', 1559132370, ''),
('pa27ras5bkm8sasdccti5c0c4ujo2fuf', '192.168.1.110', 1559221190, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('hc7kdalcbtmvimlbfatgp0h74f33a9h3', '192.168.1.110', 1559203540, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('e0o7fopui0rrbmikf30t6ir7e1np6ktg', '192.168.1.109', 1559191609, ''),
('amjmmprc1d31mujk78p4376fon4ma75t', '192.168.1.109', 1559223975, 'usertype|s:1:\"1\";query_date|s:10:\"2019-05-29\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;s_message|s:24:\"Note successfully added.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('ff7cm1f6js4k749vslvafp1tu0ub2u87', '192.168.1.109', 1559191653, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('276hnnr1pu3jutltu5pl2kgsdaa86j9d', '192.168.1.133', 1559200184, ''),
('lfqmf963our4co27kv9j78nb5arm2bl7', '192.168.1.94', 1559198035, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('f71iggkd55d6ju8hlp10c1nu2ff76v3h', '192.168.1.95', 1559201478, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('oa3v5aiih6avt38pg4v63tep1374bim2', '192.168.1.133', 1559209482, ''),
('5j038oe0mbsjksljde98vfr09gltlpdj', '192.168.1.94', 1559220974, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-05-09\";'),
('22jpj2t5dg96n8oltgq6i07130eao0i5', '192.168.1.95', 1559214447, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('rmlbh5au3p1v7fddr1h9laso0i5dh2td', '192.168.1.95', 1559215327, ''),
('ihi5e56o0vsikj7f5a9v8m7r4t3bn360', '192.168.1.138', 1559225620, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-05-25\";'),
('n71bhnlqrldu2pb1nmegp5kemsnjh402', '192.168.1.110', 1559221518, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('o98bglv2pbplt1uabpcds2vq37h1d2sa', '192.168.1.95', 1559223790, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-05-22\";'),
('36r3q6v53q3vpq40av2otlua5ed2i2at', '192.168.1.47', 1559226036, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('0a6ao9ru44vgf3m4r87uuhmv4rk6gefg', '192.168.1.94', 1559223802, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-05-29\";'),
('t9v1t45nkv7o9331a1hvcseqtj08rm30', '192.168.1.110', 1559277667, ''),
('2h43s6nvbsf7psh1or29073nve7l9glp', '192.168.1.109', 1559278149, ''),
('ebd6cnbbr9qmrit9pftt2echmhg65r2t', '192.168.1.109', 1559309114, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";query_date|s:10:\"2019-05-11\";'),
('ots2i8v5httjg0gegfea8vrusocirfmu', '192.168.1.109', 1559278174, ''),
('l0mhutt1pqja56np86vgtoe9a3invsi8', '192.168.1.109', 1559308572, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('bs8gq3i9oimtohim70jsa18uigm07973', '192.168.1.100', 1559282163, ''),
('ndsdl4lsvhsbu5m5mct6d12mmb3jnjva', '192.168.1.95', 1559288818, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('bheellt5u1kg6h0tf4kqlesr6nutu00o', '192.168.1.95', 1559284299, ''),
('hfupmgie4tfhkcajnniqm71rc0dsilp1', '192.168.1.100', 1559284025, ''),
('6ofupksfadl4099kmr39cifflqvv18g7', '192.168.1.138', 1559290088, 'school_id|s:1:\"1\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";teacher_id|s:1:\"8\";'),
('09ecmsapvt9loc5srd8gtoc6eqd0t20k', '192.168.1.94', 1559295752, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-05-16\";'),
('rtnsne8akcnji8l7hd7rt131b0rueqil', '192.168.1.94', 1559285845, ''),
('s0amfhddmv246i8c8qa9ucn228e81hj7', '192.168.1.94', 1559286508, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('kc1jjrnd36t9p00pcb32e76i97appdeu', '192.168.1.110', 1559295267, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('lqfb5nmrfki3280meajn2ft88h09voo8', '192.168.1.110', 1559293610, ''),
('nlpect65tu26kmq2ikn1umu47rft5k02', '192.168.1.110', 1559300384, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('86k8bn30e11q68erm8iqd1su6srpeb0l', '192.168.1.138', 1559312079, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";school_id|s:1:\"1\";school_is_logged_in|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('vi1jbrqv5j0lemtgj7hfk29581a0kf8f', '192.168.1.95', 1559299532, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('hddqdspaa0ffb0gofc4fhiur3ubcl0o4', '192.168.1.94', 1559308012, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('svlf7ugrhfptof01rv8njf1o28f997sb', '192.168.1.95', 1559305784, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('qsff6qi3majgjqqs6bdbhrg1bnfja2jm', '192.168.1.94', 1559305789, ''),
('q38nqn9m7vabk9b48bth11vmjm5g23fb', '192.168.1.110', 1559307721, ''),
('rq3vd2og93r0ifpsf02edu5uun85dipt', '192.168.1.110', 1559567044, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('jpd99d1mk0hols4pn84hlduggjgrk2b1', '192.168.1.110', 1559537353, ''),
('unna2vthtmfrvmqht7t0idlopq1k3vjc', '192.168.1.110', 1559567119, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('506d36abisiigt2qvfk6a4id5naa561t', '192.168.1.109', 1559540496, ''),
('uhuluspncnhills4c58uatptle9cdt77', '192.168.1.109', 1559569063, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('fognvblsks2bosq5lkv057ae3b136cvb', '192.168.1.138', 1559543195, 'usertype|s:1:\"1\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('3jeucmp85pqm8e91rmgt3on9rik7mfpv', '192.168.1.96', 1559548063, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('96pvqjstbik1nqrf451s4443b8qgpdjo', '192.168.1.68', 1559543175, ''),
('s3g7g1lilsnhg9pvjq32ta0bf85agdqr', '192.168.1.68', 1559544149, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('i7f7vtmqcs6ktksvc1itkl566f73dbv5', '192.168.1.78', 1559558888, ''),
('5b5e9msakumjcg9rupp5v45f40m7ogbk', '192.168.1.138', 1559557298, ''),
('h9tg1d9bqen0a31rkl8ctof45mceslh7', '192.168.1.133', 1559557797, ''),
('hk5qh8sm2nbh3jja2q837her3lbaspkd', '192.168.1.109', 1559560093, ''),
('11imjd5giluq2th66iqkl48lfkgkjths', '192.168.1.96', 1559568979, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;s_message|s:27:\"Diary successfully updated.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('1sf9o17rk1rim61470finsa1u6elhnn0', '192.168.1.96', 1559565868, ''),
('87savpmd4kvhbu720nlbh69cqultfii9', '192.168.1.110', 1559653149, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('2qupih5cjrcqgquh8s3sit3nktu5nlb0', '192.168.1.109', 1559624173, ''),
('lkqnl1estpusnjjd3cung9p1gquvhpfq', '192.168.1.109', 1559644912, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('2ir96pbog8q1umkmn2555br3ki27lrmg', '192.168.1.109', 1559624527, ''),
('ohnv8snr8qcdr1p8jhkeq57u804901hr', '192.168.1.109', 1559654958, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('207bhmisdpo4obv607v9de1r9gkejgv6', '192.168.1.90', 1559654732, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('gmeuiv7jha6rnsdokbnl9ea5958betp6', '192.168.1.110', 1559627342, ''),
('3t2f6vqck4l9qqh69ijh0r6ppgninh11', '192.168.1.107', 1559638298, ''),
('6n4qn87cmsb6kjolmit96d14nlfcu8al', '192.168.1.107', 1559639099, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('29a8rnt518ovorr7pu1st7pm8vi2imer', '192.168.1.110', 1559651904, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('brr54be26ou00juu78k64ab6o5vlsslq', '192.168.1.133', 1559652548, 'school_id|s:1:\"1\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('a2isa3i9t4m5jrrqp50tm3un77frrqv4', '192.168.1.138', 1559657663, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('qi4e9lq9b771denrev3egabmdnc772dn', '192.168.1.87', 1559655594, 'school_id|s:1:\"1\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('1s760h7g1296j4qtvd5em1ttji4d26l4', '192.168.1.87', 1559654621, ''),
('qfgkl4t31j1cnabh2k8cqgoqsu0fgo0v', '192.168.1.110', 1559796731, ''),
('avupn7unoe19clpe8hkt090kvif04sg8', '192.168.1.110', 1559801009, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('43gtl5mqmv7o2pj2kk4prractkgk7tjv', '192.168.1.109', 1559816246, ''),
('4jqob22edkss3rfjgobp67jbjcfmt7gr', '192.168.1.109', 1559827753, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('ecvnrfb0ho9riku6o4k4nl4o4lhs03ee', '192.168.1.109', 1559827542, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('nki6tqstke11d6shrm1v929m6s51hkvv', '192.168.1.91', 1559825304, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('jfod44urcbb06u62m4dnu8sb460ok9ce', '192.168.1.91', 1559817448, ''),
('jbphit3vuj3s30o2bnv1gjb0006i06gr', '192.168.1.91', 1559817435, ''),
('ntbehem2t7qoie8leaug1cs8c39hj2gs', '192.168.1.110', 1559826186, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('m7hsb4b5e6pc9bh4a46a04q50td1t205', '192.168.1.133', 1559883229, ''),
('r4k3acqt2gu28pcqnhbdug2lneo859ms', '192.168.1.109', 1559883562, ''),
('1uncuo271qbq66shfq4t77dojubgpp6l', '192.168.1.109', 1559914012, 'usertype|s:1:\"1\";teacher_id|s:1:\"8\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzI5Jw==\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjkn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjkn\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"29\";school_username|s:5:\"86884\";school_email|s:21:\"biplob.mudi@gmail.com\";school_is_logged_in|i:1;subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzI5Jw==\";'),
('r4ui38vv0a9pr28gclgtrjbk0asl9eq4', '192.168.1.110', 1559885024, ''),
('81p8nep9e26t3vm99cu9nc695vsihqbv', '192.168.1.109', 1559883877, ''),
('lvku98ehsjkj1mqo93knih7kr62ekmi9', '192.168.1.109', 1559913388, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('rid4nh2bvplkh5ef9l5qorm3hqcqmd3f', '192.168.1.110', 1559908866, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('cbn2hampma39km13eg11m5s70p6tn2df', '192.168.1.96', 1559909365, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('ssqvqncgk1e429avasolnmfmvasqlcpm', '192.168.1.110', 1559909023, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('dt0bbt75h2bjnt7lh4mj8md96uk7d9qc', '192.168.1.138', 1559889962, ''),
('emvfpp5v8g5feecdvci3oohlavhg43hj', '192.168.1.138', 1559894610, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";'),
('pceseluoe3asf6g7esqukni7qpkh1lu1', '192.168.1.98', 1559894516, 'usertype|s:1:\"1\";school_id|s:2:\"23\";school_username|s:6:\"997925\";school_email|s:16:\"pratik@ablion.in\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjMn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjMn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIzJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIzJw==\";'),
('8c74ejrv1tp8nnsno8pf3cd3ki8rj2s4', '192.168.1.138', 1559916785, 'usertype|s:1:\"1\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('g5878aa0eaugbauqll31va453kfbf2cm', '192.168.1.109', 1559903751, ''),
('d58afga8flbs1741bq5a1t5q39fjinsj', '192.168.1.95', 1559909915, ''),
('n7lev1svooao61vmvmumf2p1t2k3e8na', '192.168.1.138', 1559981769, 'teacher_id|s:1:\"8\";school_id|s:2:\"22\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;parent_id|s:2:\"17\";'),
('mc04kfrln75f46fvbc6medi3ns9i03pm', '192.168.1.98', 1559907582, 'usertype|s:1:\"1\";school_id|s:2:\"23\";school_username|s:6:\"997925\";school_email|s:16:\"pratik@ablion.in\";school_is_logged_in|i:1;teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIzJw==\";'),
('mmf2gkkba8ka7sohu5a5cec4gr38kq5p', '192.168.1.98', 1559980859, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('mcr649gadgfr9m1ij99758okmo1bt2pg', '192.168.1.98', 1559994208, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-04\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('ctgip5trsthu415ppg8tq6om8ise60kc', '192.168.1.138', 1559997520, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('4vcsrkpt1sumtc229q9l6keuj7tq5i33', '192.168.1.138', 1559997365, 'teacher_id|s:2:\"11\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-05-28\";'),
('fr1g4u7k22pb4n3roc0as3ek3jihr0u1', '192.168.1.109', 1560145397, ''),
('vs52gulrb8knj9efcicslkrdoipuodtu', '192.168.1.76', 1560142443, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('4vs141on3n4da32vfgednomros7hgug7', '192.168.1.109', 1560170828, 'usertype|s:1:\"1\";student_id|s:1:\"5\";class_id|s:1:\"3\";section_id|s:1:\"8\";student_flag|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('ovdfsa6s4g2abotp2995q9gnu1up0jhq', '192.168.1.138', 1560175633, 'school_id|s:2:\"22\";parent_id|s:2:\"44\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";teacher_id|s:2:\"30\";'),
('2kd5ol1ll0eea1m8a1akvv7e9i6noupl', '192.168.1.109', 1560147015, ''),
('48hc0cei9e9770rnr133b94kr4ekv3iu', '192.168.1.109', 1560173116, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('nnr1g1791327reh08l6mn75lct03hm7i', '192.168.1.107', 1560169064, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('ibfkd3rquhnh0q43u56ookbk6gnl1j8o', '192.168.1.98', 1560167006, 'usertype|s:1:\"1\";school_id|s:2:\"23\";school_username|s:6:\"997925\";school_email|s:16:\"pratik@ablion.in\";school_is_logged_in|i:1;s_message|s:31:\"Information successfully Added.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('vkhoo24efffis1bfnaoj2j6ujdolokb8', '192.168.1.63', 1560167144, 'teacher_id|s:2:\"35\";school_id|s:2:\"23\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-12\";'),
('0nlhunrt4383or82lp9icftibms30dg0', '192.168.1.76', 1560171365, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('isp3j8bsodq7bv8e67sf1heijtvm1gma', '192.168.1.109', 1560172615, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('vqiq5mf33gjvpibm778d2pchv2noumbf', '192.168.1.109', 1560259511, 'usertype|s:1:\"1\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('3ffb1ota0js2i0cnakml29etbspfufbo', '192.168.1.109', 1560229345, ''),
('kvc9oa67uq1fbd3126ndadeikviaj2l1', '192.168.1.109', 1560238819, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('2cak02kfulgg0qrhc98utqig8dkiqvjj', '192.168.1.74', 1560236968, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('u322bnl51f7ntolm0rguakpahufc5p6l', '192.168.1.74', 1560238059, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('nbef67fkacerp46euga3ruoug42ia56t', '192.168.1.100', 1560237655, 'teacher_id|s:2:\"35\";school_id|s:2:\"23\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-10\";'),
('pg3sieolout9pj9feui3i64i0ihgjrq7', '192.168.1.98', 1560321464, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('di8k56mblkfnb2ij981opn8sqcqq2oc3', '192.168.1.74', 1560259137, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";'),
('4ss7945i2a0bksiob8utbgcbd8gkov5j', '192.168.1.109', 1560315331, ''),
('5coe6tpstm6men2jv2gmublr48fihfnm', '192.168.1.109', 1560346281, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";usertype|s:1:\"2\";school_username|s:6:\"biplob\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('q69lrhen7519gdr9q6q6k1ijsdvg809a', '192.168.1.109', 1560256721, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('7pfl38altjjubqe0g0miii96svf3iv9v', '192.168.1.98', 1560337613, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('nqd98mcf2o6cs8oq7p91jvcdoolo079u', '192.168.1.95', 1560333647, ''),
('inofupfs1vfig6j601rndjqib3evtldm', '192.168.1.109', 1560340878, 'school_id|s:1:\"1\";student_id|s:1:\"4\";class_id|s:1:\"3\";section_id|s:1:\"8\";student_flag|i:1;parent_id|s:2:\"17\";parent_is_logged_in|i:1;'),
('cjvvdukkglq88ptk32ih2dd6lbkq6f6t', '192.168.1.109', 1560337669, ''),
('n93708eu05l7beq6d01gk749ga9qp5g2', '192.168.1.138', 1560342579, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('s82ct2mjcn0ba5rne847g865p3blnbv9', '192.168.1.98', 1560344841, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('094ibe2pj11s4hheq8eq80rbus814em2', '192.168.1.98', 1560344906, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('6irpup781ljccbkmocee32tm6dsimq95', '192.168.1.109', 1560401266, ''),
('0c8r49pe8j14e4v6463apec91rkcj4mp', '192.168.1.109', 1560432437, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('66ak1vjhrmu2v13qvrn0ekh973pkmn9v', '192.168.1.109', 1560401304, '');
INSERT INTO `tbl_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('6okh1sf3p3b4k0j2gc3r69dh9ig5t9ce', '192.168.1.109', 1560432068, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-13\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('eredomn7sug7nqni3535fos2dkg3t7i7', '192.168.1.62', 1560404485, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('ncmb6qrgko8e7soku2ertgk35f2ejqfb', '192.168.1.62', 1560409509, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-13\";'),
('r4nr2ef3dkuvnmj6kloagjqlhkpnvj2i', '192.168.1.107', 1560420841, ''),
('89tkot6fujtdi0qs4q6re115kdmtbrf3', '192.168.1.107', 1560420894, 'school_id|s:1:\"1\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('85f8a07bd30t0gsohfbrl991m3v7sr6r', '192.168.1.62', 1560424937, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('piitoamfohs5eevu167q1hcog8gk6lue', '192.168.1.138', 1560436034, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;usertype|s:1:\"2\";school_username|s:3:\"apu\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;|N;teacher_id|s:2:\"30\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";'),
('l5tscs3f42nv3nmmalvao304os6gg4ol', '192.168.1.109', 1560487510, ''),
('m7msl339ngcihl936l6199dl2bgap63g', '192.168.1.109', 1560492170, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('md08sfcmaa0jn05ht3s071odbuduvt68', '192.168.1.109', 1560490106, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('a0r2suebkufqi6jd6j1vuunpd2mbfput', '192.168.1.97', 1560499069, 'school_id|s:2:\"23\";school_is_logged_in|i:1;parent_id|s:2:\"26\";teacher_id|s:2:\"35\";query_date|s:10:\"2019-06-10\";'),
('krgq7t28ncvkcv26jmud38gjike5n5vi', '192.168.1.69', 1560489812, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('381aht66lpenda7hrob2i3lajgn707uh', '192.168.1.70', 1560492644, 'school_id|s:2:\"22\";school_is_logged_in|i:1;usertype|s:1:\"2\";teacher_id|s:2:\"30\";school_username|s:3:\"apu\";'),
('nj9o9g28p5jmdsm1kjpstu065nt9h8eb', '192.168.1.69', 1560491652, 'school_id|s:1:\"1\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('kfaic0uc5op25u7lj4cc904avogjbhpj', '192.168.1.138', 1560490006, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('vr7lr7eud46vfucj7iq7ln75rf8f9e6i', '192.168.1.138', 1560490422, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('pdtr87holekc0l2tjkc2puc2n553jto2', '192.168.1.138', 1560493644, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";'),
('mo1o66ov4gl5crhool9jvq9fs1bnqp84', '192.168.1.69', 1560498644, 'school_id|s:2:\"23\";school_is_logged_in|i:1;student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";student_flag|i:1;class_date|s:10:\"2019-06-14\";parent_id|s:2:\"26\";parent_is_logged_in|i:1;'),
('3ll55dps6b2d45ngpovem2ndakll01i9', '192.168.1.70', 1560495661, 'school_id|s:2:\"23\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-11\";teacher_id|s:2:\"35\";'),
('mnr7trsut71ogfh000b5llfa013aiaiv', '192.168.1.133', 1560499016, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"23\";school_username|N;school_email|s:16:\"pratik@ablion.in\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjMn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjMn\";'),
('ebegs4lpi8m52qeuhsmjjc11a9mlipq8', '192.168.1.97', 1560497818, ''),
('fnockm2ovtof30rub4ia3etpflc6kr60', '192.168.1.109', 1560506565, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('qebs8ln1kibfs28itrn8qct116lf61fr', '192.168.1.109', 1560506550, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('tg1mfmn4plboajmitdo6kl3mfvgnidb4', '192.168.1.109', 1560517810, 'school_id|s:1:\"1\";school_is_logged_in|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('jv8ctivgn2nbltln4r8uqf3gp0l9ui61', '192.168.1.109', 1560507087, ''),
('5fv3r0nneaigoqbs1j2tfnj0c7iqhqql', '192.168.1.109', 1560518190, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('sbd9afs4jqkl4aa9rlmar6p1m47r514a', '192.168.1.107', 1560507570, ''),
('avajkqjjs1tmjlvnfo0mmvvpdraqveq6', '192.168.1.107', 1560507586, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('umhml0f616hhd1ropf95cmadr5slbpqf', '192.168.1.69', 1560514569, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('vkla238h1bpn43a07281r6java355fkl', '192.168.1.110', 1560751535, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('hslj9khdpqno3loe3cor34aof2605ld7', '192.168.1.109', 1560749999, ''),
('793u2pofsv92ak9hqij7k4c944eas5hd', '192.168.1.109', 1560750020, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('j9h6ib9ii0hvp6jtnbnkck6f4opqb9ea', '192.168.1.138', 1560748757, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('sjnkhpch8uutnb7ccljiej9kbhvdufb2', '192.168.1.109', 1560750392, ''),
('l9b6m4dlqphrfbh6kcc5j85elrsl0gs1', '192.168.1.109', 1560756567, 'school_id|s:1:\"1\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_flag|i:1;'),
('atetqfj93pinel4iht42gf1dsppecm78', '192.168.1.43', 1560763421, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-17\";'),
('hq380mdiu4872lv0suhp55p5b7r1bq04', '192.168.1.110', 1560753406, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('5eigo5t226vcgot79tur072dpv4ruuif', '192.168.1.110', 1560751465, ''),
('iopj72gd7sts9b9v789me777i3s412mq', '192.168.1.110', 1560751570, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('02usdg0vvgi89dh0reifuc80acvgvoca', '192.168.1.110', 1560776773, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-17\";teacher_id|s:1:\"8\";'),
('1traici3gn2e07v0m2tr8sc3nqn3ddt4', '192.168.1.42', 1560756640, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('lrqmti1v56b3l29k7lehqkoil4is0cf4', '192.168.1.109', 1560769308, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('7131npui35li393dn9itlh6um3svvpj3', '192.168.1.42', 1560767894, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('mg6kfe7k17imhjssaju7fk7kkt1dq4ho', '192.168.1.42', 1560775541, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-17\";'),
('sebcoeinrf6aqtuhf4k3es43758e0jql', '192.168.1.42', 1560768048, ''),
('40pg7vj6ivu88ngboi8d5kps2dl30dp8', '192.168.1.109', 1560769784, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('46uv1eq5dqm4cad6n1bt0q5t7vntmae7', '192.168.1.109', 1560769804, ''),
('6otqdrob26lqo9ai01803v1emtt5dujc', '192.168.1.109', 1560777091, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-17\";'),
('5en0c6hbn2qvfkjsfga335k6m0n4ldae', '192.168.1.109', 1560776773, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('q9l3u9hrlva74uo9teho154fgtijdolb', '192.168.1.44', 1560777103, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-17\";'),
('5n0p5g2k44ag7p7g88bounfu23ck98hn', '192.168.1.138', 1560777111, ''),
('h8jbecd7ljphh72mgpeel9q5si5bldj9', '192.168.1.50', 1560780721, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-17\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-06-17\";'),
('agnpib6dklha0cprh3ibb9litcernie5', '192.168.1.41', 1560778828, ''),
('9rntcatv25d72ujgqfd143bqj3a6id3o', '192.168.1.56', 1560781054, 'school_id|s:2:\"23\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-17\";'),
('neqtet46d4399bsphsq3uu3aea54tu9g', '192.168.1.110', 1560834602, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('g42vhaipa7vqn7tglja1d1fpb30lmnm6', '192.168.1.109', 1560833841, ''),
('bcpv3t4vrthcmkdtj80c25psjec9fnbd', '192.168.1.109', 1560864121, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";query_date|s:10:\"2019-06-18\";teacher_id|s:1:\"8\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('89b1laai6uko1clknrrjt3d8jklgtciq', '192.168.1.109', 1560834985, ''),
('9mvbge36b9j7rcl86a1hoc0l49ppivsm', '192.168.1.109', 1560833893, ''),
('74u0vvmd57m4td20aafr59q1uue4v30g', '192.168.1.109', 1560835468, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-18\";'),
('cnf4049jb6lnmh2q61pfq1j17nme5tcd', '192.168.1.48', 1560834201, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('okj9an9829qg1jg6tkchqhjrh2hmfhjl', '192.168.1.48', 1560834586, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-18\";'),
('n68a31oomu7pvl6qml6f30sq4n0r22ba', '192.168.1.109', 1560838814, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-10\";teacher_id|s:1:\"8\";'),
('i2uesgsp5qu3lmqlhdl1d481tbt2c2qh', '192.168.1.39', 1560838504, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-12\";teacher_id|s:1:\"8\";'),
('bqufjdunp1p15b1t0imhufjo8us88qop', '192.168.1.46', 1560842351, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('onet6lgj0jo2v4jhhujiq7dr8bvdd9n2', '192.168.1.138', 1560844067, 'usertype|s:1:\"1\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";query_date|s:10:\"2019-06-18\";'),
('k7j1fk3osh9c570rcmqt3e04ffku9ec2', '192.168.1.109', 1560855693, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-17\";'),
('tl0ie115a4db0e2elqtc1ji25tdddehe', '192.168.1.46', 1560862896, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-18\";'),
('ss3ukrsfqb88clagsm6hk0deaj27drc1', '192.168.1.110', 1560844769, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('e5kfusqnqj6r8gqgqdtaf4ndl3sj2r47', '192.168.1.48', 1560863689, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-18\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_flag|i:1;'),
('k1rr11blnk8bh05l9npj4a3it2b2sq2h', '192.168.1.109', 1560863327, 'student_id|s:1:\"3\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;class_date|s:10:\"2019-06-18\";'),
('0akhg5umt3rtjcb324nfpql31kou1ajv', '192.168.1.39', 1560854623, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('iu9ld1ctu25hekrtbpu97g0kjnha3gv2', '192.168.1.110', 1560862897, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;class_date|s:10:\"2019-06-18\";'),
('7gmtvksn97hng7oq0ogg6crgf60dea43', '192.168.1.110', 1560852789, ''),
('42ei42n0k012et2u9fov9e8ltff8rndi', '192.168.1.110', 1560852789, ''),
('3762lqvqfemvl9do1vqtp684uptuklt6', '192.168.1.54', 1560859339, 'school_id|s:2:\"22\";school_is_logged_in|i:1;parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-06-18\";'),
('9qns7pdihlp24ppu42vlhpsga1psdjoh', '192.168.1.105', 1560858643, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('ukspsa9kpggg88j39s9uvgvpqg2d4lds', '192.168.1.110', 1560863167, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-18\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('dajto484gdk4bs2mqn1093n1vj964tpp', '192.168.1.138', 1560865579, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";'),
('q2v5bgtie360qgdknefioqi5fechtja2', '192.168.1.39', 1560863715, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";class_date|s:10:\"2019-06-17\";'),
('epnvi3ev67p2drn7mi7l216effu9mrgu', '192.168.1.110', 1560924657, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('16n2skvrt6l3330mgg73po37hj95b6s9', '192.168.1.110', 1560927993, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('sks72td0rorgmljvnsn3adu9kdl136qr', '192.168.1.43', 1560926025, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-19\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_flag|i:1;'),
('r2ejs6a1egaomios8702fa92ugbm9iqd', '192.168.1.110', 1560927206, ''),
('8ud2gs6c75g33gg7cff2i7na39tcjlpq', '192.168.1.105', 1560926807, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";'),
('cnvdmtsu1h3je771jncqts0bkhe4icse', '192.168.1.34', 1560928209, 'school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('gbqajgvjv33gjbn1gkqr2sec6oe2fcd0', '192.168.1.34', 1560928439, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('ci871al8l3hl37bgv9lvqtnubnci9s44', '192.168.1.34', 1560929191, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('92rimhqamirc669cakjg8evj28q4jpv0', '192.168.1.34', 1560953029, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-19\";student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";class_date|s:10:\"2019-06-18\";teacher_id|s:2:\"30\";'),
('r3omstsiq3mi22ukejpdubkrql4ie96h', '192.168.1.138', 1560941260, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('dgngn6b6u3ck0gggvt4eg1arfteu1mpf', '192.168.1.105', 1560936404, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('drqcu6mj3kp89tckrsal0nbppetpcgul', '192.168.1.105', 1560936483, ''),
('398cquij30p30hvs52j0v2tfpabghcmb', '192.168.1.109', 1560950792, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-19\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('cgqs9imv00ufibg13rruo2ddk2d3a53u', '192.168.1.109', 1560945622, ''),
('qb88rqvd6erg1l0s2ncrqs1prd9jegma', '192.168.1.110', 1560945283, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('4uu69nbg4uu0ql536tplpl1t31hs0ft8', '192.168.1.110', 1560945469, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('p3ec3asoge1d1q6ip4836b37ja1bvdfp', '192.168.1.100', 1560945548, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-19\";'),
('mllqr7j4viepehuh12lv83egdkdpi36e', '192.168.1.43', 1560945460, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-19\";'),
('0k87ul4e2mlpov86afe67tgh1aiji7k7', '192.168.1.105', 1560950372, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('8fm6m8u4nl2kj1l6lfmaeia96vne7f08', '192.168.1.105', 1560949158, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('2q914jjfvona36albm8ua7t0ifk4auki', '192.168.1.105', 1560949229, ''),
('ujnrd25k6nd2g1p7lu7a775u8jm2hrpu', '192.168.1.105', 1560949230, ''),
('5na1jtbefpsjhg4dkvslh5a66ks6j358', '192.168.1.105', 1560949230, ''),
('5u0bpt27aurgkrv4f8dmcage14b7nuk3', '192.168.1.105', 1560949230, ''),
('ee8d6eq3nc501pq3gh8m8i2eb3rd9fre', '192.168.1.105', 1560949231, ''),
('grocmmv2bvk7s2p4164k9n8l5tdignkc', '192.168.1.105', 1560949231, ''),
('tf6nbp4acqeikl49p4n1v1bmho4g2b99', '192.168.1.105', 1560949231, ''),
('f4m6ns2k2bkfv3emr2sqnisequn50sou', '192.168.1.105', 1560949231, ''),
('avsqsicnq8tmv4qjt5c5ve9a7qjifh8s', '192.168.1.105', 1560949231, ''),
('rk47quejfujl262qqj70q64ob3galfsh', '192.168.1.105', 1560949231, ''),
('b8v16hi5ml9acsgg100hgdr7r2t1i0vg', '192.168.1.105', 1560949231, ''),
('21h6s08afbvma3aeacdu17jr64ab9dqf', '192.168.1.105', 1560949231, ''),
('u7huiaqvhn9r9cf0hc70ugu045gpemrb', '192.168.1.105', 1560949232, ''),
('rjdj5551rkujpsiakom6h4iqpn18l0dj', '192.168.1.105', 1560949232, ''),
('dp548ilgg4lk6nrksldrk71be1rlpm0j', '192.168.1.105', 1560949232, ''),
('l0cqb0csrhncem4kt1on98sa9uhs4mer', '192.168.1.105', 1560949232, ''),
('p2qbmlv8a77efbe0iijsel2okpab64o2', '192.168.1.105', 1560949232, ''),
('us0mpelirnbo63s96pegjs73qqijmd3f', '192.168.1.105', 1560949232, ''),
('3iartas22vbartpentmbk9778vdljcg5', '192.168.1.105', 1560949232, ''),
('ulra98nj5nrgjlc6q1pe3gs1qh9midct', '192.168.1.105', 1560949232, ''),
('joi4f4qm5arentisq7r7njntrqn7h30h', '192.168.1.105', 1560949232, ''),
('ihmnd9tigbvvkpcegcltidn0ddfv3ib9', '192.168.1.105', 1560949232, ''),
('a0rndnkr66urlh91d4paqn5o41ce5tvl', '192.168.1.105', 1560949232, ''),
('ktiji5nf437sqggppuls3tobje2mcmde', '192.168.1.105', 1560949232, ''),
('ga8dhegtu21p18cpj2r3pfch9jhml2qr', '192.168.1.105', 1560949232, ''),
('611pgfk2ta1sb9b2acvkifbcrut54rlb', '192.168.1.105', 1560949232, ''),
('kb22f9c8eg9459rdspsrum8tdo8vmd85', '192.168.1.105', 1560949232, ''),
('anrskp54f27q8cs06qrbfsfdpjgfhb2e', '192.168.1.105', 1560949232, ''),
('ndnjvitqhet66pus0mlceh433km8qpbi', '192.168.1.105', 1560949233, ''),
('8dafo2is06j9mu5r6pepeftf91a88cv6', '192.168.1.105', 1560949233, ''),
('iipielg38jasf70qnkoer9ki7bsvekse', '192.168.1.105', 1560949233, ''),
('pifdoj2ppb9uaqsg9c9h7jdc4kgse65u', '192.168.1.105', 1560949233, ''),
('lse6rcaer25utk3i9g7sqmh5cuc6qds5', '192.168.1.105', 1560949234, ''),
('u2jtt0lu7sm0ovis8lg5a9q1pvpash8m', '192.168.1.105', 1560949234, ''),
('7momme4p2f044dqcbvppip506dpf1khd', '192.168.1.105', 1560949234, ''),
('psde7tjpnldrhh0i07umeq532k095ssg', '192.168.1.105', 1560949234, ''),
('nh14ve3mk0hj99rltjdheou8dq4oes1p', '192.168.1.105', 1560949234, ''),
('94iu3bq2m6k51a7l7phmhq3r4t2b4co3', '192.168.1.105', 1560949234, ''),
('hm3qgth08kv8p6hc86jj13vtbfaeufb5', '192.168.1.105', 1560949234, ''),
('kknmdd10v4863uc8m6rrbv5i2ltvh1f3', '192.168.1.105', 1560949234, ''),
('9sgldh0uo0mgv306nkvjr8t5o07bg79t', '192.168.1.105', 1560949234, ''),
('hikffa8gf7ucnvffecrl6kcj3eg62c6n', '192.168.1.105', 1560949234, ''),
('smkei9stpe4qah844u57fvddcf3s6fia', '192.168.1.105', 1560949234, ''),
('a0m2ou2h2u33t78mvo6girej64inkjsr', '192.168.1.105', 1560949234, ''),
('dvikqp8afdjdukn0l1sbronfmej7ousv', '192.168.1.105', 1560949235, ''),
('0qfua3scae93p3rctha2qe48ujhu24vn', '192.168.1.105', 1560949235, ''),
('73bha58mqnnljpre7h07sno6ulebkscl', '192.168.1.105', 1560949235, ''),
('dmi9r8ata07q9sd640b7h0ms73bgnttd', '192.168.1.105', 1560949235, ''),
('n4aqdpmhi6kcb53n74b4hqhh9jrhvkgj', '192.168.1.105', 1560949235, ''),
('t2knik8aogje13hpi3avdebip418sief', '192.168.1.105', 1560949235, ''),
('6t7tqucsndtfdcaudfr9trqulg8reg76', '192.168.1.105', 1560949235, ''),
('9oqhopmkqsafuond8p03n7t0eqevet5g', '192.168.1.105', 1560949235, ''),
('s3m743sj4oc8rkv56iprdpsli551686e', '192.168.1.105', 1560949235, ''),
('atclhet8fatoe1iun9v7lnbqslf8v7e1', '192.168.1.105', 1560949235, ''),
('o5taodehino1fk7i2t3imjbdl3e9fnra', '192.168.1.105', 1560949235, ''),
('vjp85d0g16drmkhai9nkov1607frjbmn', '192.168.1.105', 1560949235, ''),
('kq5mq1t64k6250h8cm6q4nfc3bjh5gus', '192.168.1.105', 1560949235, ''),
('2ar9glf57cb47g8rbjdpp4sl1kgjlpt8', '192.168.1.105', 1560949235, ''),
('nl8ok6bkg2i9gc8od767ha41rigoab82', '192.168.1.105', 1560949235, ''),
('469qm3dm00o5t4p9dn8u5aarppgkabhc', '192.168.1.105', 1560949235, ''),
('j3l98t9gvqac3t3qvo6iukmni7haac7k', '192.168.1.105', 1560949235, ''),
('juofk13t5eejt77spslv924kl871s7u7', '192.168.1.105', 1560949235, ''),
('9t6uc254q3jot1ie6b3pvv62tvu5ta63', '192.168.1.105', 1560949235, ''),
('37bubab7ctg444i4brro48ad03jg6ai9', '192.168.1.105', 1560949235, ''),
('2nh9at912vlsh88801gpptmmk6sbth8e', '192.168.1.105', 1560949235, ''),
('6tl8q8hn16h5gdtnans78d719lcp8d9d', '192.168.1.105', 1560949235, ''),
('ttg43g6pvbfr3qemvd8lp8pfpbnn3qna', '192.168.1.105', 1560949235, ''),
('i72aa9jih67p0iqg0l04691ppk7lsb6p', '192.168.1.105', 1560949235, ''),
('eluo6vthdf6ct91i6a1464jrf7hghih2', '192.168.1.105', 1560949235, ''),
('siria25qtd868djd3rj8cc8o1egoq5sc', '192.168.1.105', 1560949235, ''),
('n64evq7i2rv9mcg7qfllheelhbf23c4k', '192.168.1.105', 1560949236, ''),
('j64jbofqj25f5npii4lkefe2ks31kfdc', '192.168.1.105', 1560949236, ''),
('nse6qo0lhot91l8n0lvl542hvjs9iqbs', '192.168.1.105', 1560949236, ''),
('loe3mvthh63g4dd7717c6p2mlv0urdrn', '192.168.1.105', 1560949236, ''),
('m291hqvvc668svntfolu3o4def909739', '192.168.1.105', 1560949236, ''),
('if6jv4ca6s2kk0bgf47i34k5gqmv3ho9', '192.168.1.105', 1560949236, ''),
('opg9c8o70461aaqu1kcq2unpdooj2hf3', '192.168.1.105', 1560949236, ''),
('cbl3gaea9qcuel30urb8lclquk7q545k', '192.168.1.105', 1560949236, ''),
('eecsvgcj1j1iplag87kr2igih51g7v7e', '192.168.1.105', 1560949237, ''),
('7jvdth2euot5de05offf50per7asonjr', '192.168.1.105', 1560949237, ''),
('hlf57t16e9ahtri242undq3f9cr2d4r4', '192.168.1.105', 1560949238, ''),
('sj1e03gsv9v10rkkl4gts4d9ta4f6b1f', '192.168.1.105', 1560949238, ''),
('44qvvbg3t11h26i1o5v3cd01slbcttu7', '192.168.1.105', 1560949239, ''),
('4vrpq8l0ul4krh0oevic22vrcnmbeiaq', '192.168.1.105', 1560949239, ''),
('9eu9tsbidblu01qde8oacmlgfjsik3ue', '192.168.1.58', 1560951197, 'school_id|s:2:\"23\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-17\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('e4g0obgmkc0kjbk98m5oq76su3vq17vl', '192.168.1.110', 1561016465, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";'),
('7njpbrja71ts24pi8trb8u3kch45spov', '192.168.1.42', 1561007049, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('q9c5612lgtkoughd3fe7oe8e1e58mjqm', '192.168.1.110', 1561035540, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-20\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_flag|i:1;'),
('kaubfcs1te8va3lmjpausltaee6ojsb9', '192.168.1.109', 1561009603, ''),
('2qvld2p4dmkiit2qadmb9evinakbb6h0', '192.168.1.109', 1561035751, 'usertype|s:1:\"2\";school_id|s:2:\"22\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-20\";query_date|s:10:\"2019-06-20\";teacher_id|s:2:\"30\";'),
('o4cu6nb2g16dghho8sfr37lih8gp145h', '192.168.1.138', 1561009445, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjIn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";'),
('iptei8f7j2ipurp6dg6eiobrs08ae0sq', '192.168.1.34', 1561035326, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_flag|i:1;class_date|s:10:\"2019-06-20\";'),
('ett1vhpla0o3lrtnmst9uj1opbp2tbmc', '192.168.1.42', 1561034731, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-20\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;'),
('jvebdneskjfbeedqi9k8fuua2bhoarmj', '192.168.1.37', 1561039053, 'school_id|s:2:\"22\";school_is_logged_in|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";class_date|s:10:\"2019-06-20\";query_date|s:10:\"2019-06-20\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_flag|i:1;'),
('7tdpjsvo7voq9l9tc8hvodp9vdemn92g', '192.168.1.105', 1561028673, ''),
('hj6m0dvig1vaq04utdb3p2kdgpmipaur', '192.168.1.138', 1561039089, 'usertype|s:1:\"1\";parent_id|s:2:\"44\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('2ai5o0o48ho0p5kaco54tlklslmjk8lk', '192.168.1.109', 1561030224, ''),
('he648egif6ghd61iftss7mprkjjplpk7', '192.168.1.109', 1561030832, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";'),
('p2gkcceidv9plc8317skmbme35heckhj', '192.168.1.95', 1561030754, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('cg1jc814uh1fv49a26vrs585opd9e7qo', '192.168.1.37', 1561032650, ''),
('32vkdm37eufm53mp200jfd0e5gh6s1q0', '192.168.1.95', 1561036079, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";'),
('1ek674njhvkmsotkfs49faldt0tnlpu2', '192.168.1.105', 1561031880, ''),
('sva5in2lnp4m4cr81ap7qo2lflrd63iv', '192.168.1.105', 1561034622, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";'),
('r9ic7jn5o7dv1e5be5ottv3rrb7cj23n', '192.168.1.105', 1561033762, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('b37ivkto26olhfbjqi2p39mbscmks962', '192.168.1.109', 1561092919, ''),
('6hth6hvp15j0c7mpfh9u8l04f2tj6ec9', '192.168.1.109', 1561104332, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;query_date|s:10:\"2019-06-21\";'),
('jukbbvd32dsoqu0c9gsf2c1ru22ej112', '192.168.1.110', 1561097715, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('f4ujdjd7ooepnamjqnf1qjkv1qniinog', '192.168.1.26', 1561096799, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('auac7d6q3nrgjht8i6ntfqh11vcoq9l2', '192.168.1.26', 1561096948, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";'),
('o3olcr7uupr9n82l8a5nrb4sjp7trabv', '192.168.1.23', 1561098311, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";teacher_id|s:1:\"8\";'),
('6vhvtdiri49lcgnvlom13mnsnsjju0g1', '192.168.1.36', 1561097777, 'student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('hkadvjmi9vfu7h1p0pnhmvqtfsnd1c20', '192.168.1.23', 1561098312, ''),
('gsdu217ao4tdrfseth59mq4h5r20cima', '192.168.1.23', 1561098271, ''),
('27ilqsdl7r035bl1rb71n22ikv1lhevs', '192.168.1.23', 1561098313, ''),
('gnlasj2vphffmr7qvsvbo4stk4uc8elv', '192.168.1.109', 1561101186, ''),
('vvqgl69hv07s41bnt0372ff1kniembh9', '192.168.1.36', 1561113716, 'student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('1d4lsm6okt2pmv49o0ttelti6a3k9nib', '192.168.1.36', 1561123379, 'school_id|s:2:\"22\";school_is_logged_in|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";class_date|s:10:\"2019-06-19\";query_date|s:10:\"2019-06-19\";teacher_id|s:2:\"30\";'),
('uum8js9pbao7mjfqse44hde5fhp89h39', '192.168.1.26', 1561122761, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-21\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;'),
('bs0rarul451qiq27jcjjj7gd3rj3ga46', '192.168.1.109', 1561119886, ''),
('e11hhffgqae1n12sggph4k6rqv7gcql4', '192.168.1.109', 1561120509, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";class_date|s:10:\"2019-06-21\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('pllcnq4gdk7q6nefsg2ghqpe16r9oliu', '192.168.1.109', 1561124413, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-19\";'),
('qd69ugvptsk9jc51gof5aoni4f36dsiv', '192.168.1.109', 1561116846, ''),
('3b0hn6ir2e5mlubvp8nfuufuj2vd398r', '192.168.1.109', 1561116846, ''),
('atf7alhe74ukenegli8rjkh6vr46v80u', '192.168.1.23', 1561123341, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-18\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-21\";teacher_id|s:1:\"8\";'),
('2i78rs26ifmhnhlr2dllg2q1l9og18sm', '192.168.1.22', 1561126302, 'school_id|s:2:\"23\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-21\";parent_id|s:2:\"26\";parent_is_logged_in|i:1;student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";student_flag|i:1;'),
('kfl4mp7rno8rr4ehgotu7l2m14lh2duu', '192.168.1.138', 1561118648, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;teacher_id|s:2:\"30\";query_date|s:10:\"2019-06-21\";s_message|s:28:\"Note successfully published.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('i8700hgt7r4vcnhl7666rv4ugjchoufv', '192.168.1.22', 1561119622, ''),
('ock7hd7tsf0fjchp0i82aqfo59b3vin1', '192.168.1.98', 1561118207, 'teacher_id|s:2:\"35\";school_id|s:2:\"23\";school_is_logged_in|i:1;'),
('2p8atv83vo3cmq7jbv3l37uvnefmc4iq', '192.168.1.98', 1561123825, 'usertype|s:1:\"1\";school_id|s:2:\"23\";school_username|s:6:\"997925\";school_email|s:16:\"pratik@ablion.in\";school_is_logged_in|i:1;teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIzJw==\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIzJw==\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMjMn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMjMn\";s_message|s:31:\"Information successfully Added.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('p8ng31n5joh4b3hle1gmqdmvr7l5bkjb', '192.168.1.105', 1561122652, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";s_message|s:25:\"Class added successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('34gcb41k0pa7l5lve44r84ojt4hml200', '192.168.1.109', 1561119963, ''),
('7h60p7d7ah7s6kdm385q5rfddia1l58m', '192.168.1.138', 1561128277, ''),
('pmbms7n5ple5vtcvcjicrdlf80hrpo6u', '192.168.1.109', 1561178970, ''),
('be9a301sv6gpla5aks1v989uabp2hjrm', '192.168.1.109', 1561210626, 'usertype|s:1:\"1\";query_date|s:10:\"2019-06-22\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;class_date|s:10:\"2019-06-22\";att_start_date|s:10:\"2019-05-01\";att_end_date|s:10:\"2019-06-22\";teacher_id|s:1:\"8\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('ud5s67j3lqusd8uloa7r7rf6dp6t3qnf', '192.168.1.109', 1561179049, ''),
('10da64odmu0v8hlbrsehitcap3t8ja1j', '192.168.1.109', 1561185931, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;class_date|s:10:\"2019-06-20\";'),
('bmipak2b0j5aepenu2ak0r5q002n7fse', '192.168.1.109', 1561179442, ''),
('ois2lhtknhe00bc4fr6b1dnrhfrrqjc2', '192.168.1.110', 1561182440, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('f4bq8u3eumch05fbvfo569kpmjnl5g9u', '192.168.1.44', 1561180430, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('1s0220e7he7hkmv595r5fck44kjb5pf3', '192.168.1.44', 1561208411, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-22\";'),
('d2ve69gco9uggv31iiqvs98r3dlqggrv', '192.168.1.110', 1561182409, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('j0ouim15qk96jugetkr5o20c3mic0fe5', '192.168.1.110', 1561209248, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";query_date|s:10:\"2019-06-22\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-22\";'),
('ju3mtjkupi9fct7176r0gtr5v7hp2h6a', '192.168.1.41', 1561186196, 'school_id|s:2:\"22\";school_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('p8t5q0mg7d4ilisa7j83s1teo1932b3q', '192.168.1.41', 1561185395, ''),
('2ta6m85ja1nrhsmj3f0hub2ni05clagu', '192.168.1.94', 1561188729, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-22\";'),
('ipnjpr2gaoon86ps14u2ajpud8gpk1g5', '192.168.1.95', 1561186414, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('3ud9kigrm101kup3fid407nghtrjfsuv', '192.168.1.95', 1561188700, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-22\";'),
('qiu8tir05dd12sipr4f3l205ihod8v5c', '192.168.1.110', 1561203267, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('203qsk1vftpbqcker4vcvutek1h3rpbh', '192.168.1.95', 1561199221, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('f2k40r8ktbp9ob6noaacfm2nshmsqpdl', '192.168.1.95', 1561209895, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;att_start_date|s:10:\"2019-05-01\";att_end_date|s:10:\"2019-06-22\";'),
('fp4vh7njmlab2d18bph6g9npeg7nhank', '192.168.1.95', 1561201293, ''),
('i192l1ags34ncjijuqh7d0m9lttpi0qc', '192.168.1.95', 1561200438, ''),
('7h77ishkn3dqk64dv59f2a00m1d56t3h', '192.168.1.109', 1561203431, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('i3s5v2iov9a49g1u7tbo5rl2vlrhvo7q', '192.168.1.41', 1561205824, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-20\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-22\";'),
('n8cju5dr06sshd7irsssl6676o5f8tc5', '192.168.1.94', 1561209815, 'school_id|s:2:\"22\";school_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";class_date|s:10:\"2019-06-20\";query_date|s:10:\"2019-06-22\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-22\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_flag|i:1;'),
('4jcjcgns1r7l9r3c5vnef539o4m7m8ip', '192.168.1.109', 1561203939, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-06-20\";'),
('p148aqfpsvm5hoq0l9t1vpjg68etrf5t', '192.168.1.105', 1561205536, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";'),
('pt4hc7953oro8m34dsqukegj8q36s6k7', '192.168.1.100', 1561208282, 'student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";school_id|s:2:\"23\";student_flag|i:1;'),
('92mgjm104g2uqlb0gqng999e30l6k2be', '192.168.1.109', 1561352098, ''),
('qcoqiii71kh3kvrunt4aclmmog3puqch', '192.168.1.109', 1561384026, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";query_date|s:10:\"2019-06-24\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-24\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;teacher_id|s:1:\"8\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('8h0tc3cikcs9b1iqpc2bdnpq44r88h6p', '192.168.1.110', 1561354982, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('n0og6aa0mve7elt98754p0rqie2fdvap', '192.168.1.110', 1561370450, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-24\";teacher_id|s:1:\"8\";'),
('umu1re00b8104bfvn446uter44ugs385', '192.168.1.71', 1561381251, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-24\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-24\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-24\";teacher_id|s:1:\"8\";'),
('gvmnocfuij7ha1m08231jiire58kgc21', '192.168.1.70', 1561366938, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('ifjlmfn70ubhtudvu0qsi0cev125tjc2', '192.168.1.98', 1561367311, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-24\";'),
('vr8nm0uk5n2pnj0ar3qfvqlqjihbpjj9', '192.168.1.109', 1561371900, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('h5jet7ol2ggk04eusashnvsk09010mv1', '192.168.1.110', 1561381901, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('uqarbkkv17cugrn3k5gst35skhdah8uh', '192.168.1.110', 1561382036, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-24\";query_date|s:10:\"2019-06-24\";teacher_id|s:1:\"8\";'),
('8oem6kshvuqi9lbr8i5a1k5md71a6c1t', '192.168.1.109', 1561371900, ''),
('05dfnasb79r263tgs55feflhbcieqncc', '192.168.1.110', 1561378665, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('c4tgersni58usqfat5vb77u2ut7p4pfm', '192.168.1.98', 1561379034, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('s3kkrs12sjcd6dd0skt6asaojoln5kvr', '192.168.1.98', 1561379637, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-24\";'),
('5muqtge60v6144vsguur9cpki74eiaau', '192.168.1.110', 1561379251, ''),
('bk4llqf0g0rtdnq7kvcd7t99o2d20slc', '192.168.1.109', 1561381658, ''),
('9vseamd1ki27o6posqeslj0fdtnij1jb', '192.168.1.110', 1561467871, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-25\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";'),
('9n49oobcfog9qe171p2mse06a26hvt2b', '192.168.1.110', 1561439263, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('79lsjsbv563f6ctuk3ua97b5u7eri0dh', '192.168.1.110', 1561468254, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-25\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('n8mncnl67i1iklfd6b0eu2ptu7t3ofps', '192.168.1.109', 1561440743, ''),
('4ebara0mge1vt6jt0ut7nvimqs0uhh2r', '192.168.1.110', 1561439816, ''),
('9a1f5i3qn70bqgctr4biga51f0jm62ab', '192.168.1.109', 1561440108, 'teacher_id|N;school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('ul8njf3overqrl5dgga0btokdjjrgj03', '192.168.1.109', 1561470094, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('0arvbk81m0m7mhdojkvs4uu8hjimbve8', '192.168.1.109', 1561441201, ''),
('n4bcdekcl3e18rnf6hlbhuo04snj05ej', '192.168.1.95', 1561441807, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('t50hfgom1ddkm29rekmf1r2kbuhv8989', '192.168.1.86', 1561442005, 'school_id|s:2:\"22\";school_is_logged_in|i:1;teacher_id|s:2:\"30\";query_date|s:10:\"2019-06-25\";'),
('ca9ao01l58iifp5h9qsv24j5pm7odqgk', '192.168.1.95', 1561442304, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-06-25\";'),
('665f7vjqclmnpkfkhpt5aaetue1c20o8', '192.168.1.78', 1561442481, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-25\";'),
('k857qtk8ta2gmecnmdudjknjcs9iv2iq', '192.168.1.98', 1561446338, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;usertype|s:1:\"2\";school_username|s:6:\"biplob\";'),
('3lf35is20anebp0gln0qeuaj9m5r0j4r', '192.168.1.98', 1561446331, ''),
('n8l9aa69gekhqkpq4vmpdd2t0k1j7phq', '192.168.1.75', 1561467793, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";query_date|s:10:\"2019-06-25\";'),
('t10198rdrnh25jffm3cul8t0nsoq4k3e', '192.168.1.105', 1561466651, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";'),
('h5jqio1e4nm1v2dlprbq79p8a7ap0mvv', '192.168.1.94', 1561458474, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('e0j14ka0greg0geo35v2uj9fb3n9m392', '192.168.1.107', 1561458527, ''),
('qu7gneuepo7b3lhg9vahcefsjo2tnhtn', '192.168.1.94', 1561464334, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-25\";s_message|s:34:\"Attendance successfully published.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('1o85kbbquo5puugdg0e4mmj94e3dae5q', '192.168.1.98', 1561470265, ''),
('3goh5o249ab4i8earke8o543akv89pni', '192.168.1.98', 1561472391, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('7env1iphc055oahg9e2ofv2knt0fe19r', '192.168.1.110', 1561526349, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('82cm3rn6tkmi9e4c6dloqi7f396kvp9n', '192.168.1.109', 1561524589, ''),
('mhjubp1vr2nrjpcsvf386a5a4an43ekh', '192.168.1.109', 1561534841, 'usertype|s:1:\"1\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('7qe4nastk1k6ukvss484d996p5shtevt', '192.168.1.110', 1561532799, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('3tao0fv1nqktceh501d8s5ilbou9e771', '192.168.1.74', 1561526675, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('g8e7or59g6gn9b3t7aoi2r4sukp2rdc0', '192.168.1.98', 1561534971, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";teacher_id|s:1:\"8\";'),
('svptlr5605qu40jsbdav1qnh7j5nasvj', '192.168.1.109', 1561526793, ''),
('14kcgqavohqiq64pfha06br13k0qakhu', '192.168.1.74', 1561533319, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('djj71hlqf32ur06a7quksvg8jb2h24pu', '192.168.1.110', 1561531537, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('j219gtekea5agc8lv46oggmfqipvu2uj', '192.168.1.138', 1561534219, ''),
('9oeuf5jqgkvkaf8it9r5hh8lrg6fao4k', '192.168.1.138', 1561534254, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('rsuj8ihulh0fmddf8q5kch2e0fbnel59', '192.168.1.72', 1561536339, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";student_flag|i:1;'),
('k6r4ho5l7h62pb5tdajk9nbqm9f15q9g', '192.168.1.75', 1561529840, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('c98ton8bf6u2lvpihtrrpt58hejdm6ov', '192.168.1.94', 1561544642, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('hsq1fprk5m2r2pv1img1kobslc811281', '192.168.1.75', 1561550172, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('9cm272g8ehj2ksoev4a921r9pofnp85s', '192.168.1.94', 1561545381, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('al7ngvss4rnhraiib02ld7ebj5e15jgr', '192.168.1.110', 1561551374, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";'),
('75regkf32l02ie17jqiidgm9vll94645', '192.168.1.110', 1561553904, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-26\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-26\";teacher_id|s:1:\"8\";'),
('o66eb3c8l0hv0nqlq1ero6hbbaorr4if', '192.168.1.74', 1561554289, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";query_date|s:10:\"2019-06-26\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-26\";'),
('7bddun743rsr5e9e7d2mdaqu8n4daa2m', '192.168.1.105', 1561551285, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('mmm7u3jar3kqcplphlbgl50tu1k53bnl', '192.168.1.72', 1561549486, 'student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('of0edl2k02i2f1ng2mscpo2840l3a7oa', '192.168.1.72', 1561550381, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-26\";query_date|s:10:\"2019-06-19\";'),
('rncjs9u57sgchk6aqdlhcv7noabu35i8', '192.168.1.72', 1561550361, ''),
('ba3k8etonj0kh8q082tqa7v0f4vo67db', '192.168.1.138', 1561552051, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('5qvlkfrusdkvkc3l87unodpnd6p81pv8', '192.168.1.110', 1561622995, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";');
INSERT INTO `tbl_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('7cqvnpakh64mmtotddd8vtkc444lmd3u', '192.168.1.109', 1561611502, ''),
('kq8rvub4hja41ulhk25tfhgpk3ncingv', '192.168.1.109', 1561642358, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";parent_id|s:2:\"15\";teacher_id|s:2:\"30\";'),
('vj2p1dofsca3i5ngjur74qu27jc0aak4', '192.168.1.110', 1561631180, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";'),
('st1vfk3vftsue27nks07u1c927qdco0m', '192.168.1.68', 1561622986, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-27\";'),
('suouu8sh3u06ts8pr2a81ft7c5qc1nlc', '192.168.1.70', 1561642547, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-24\";att_start_date|s:10:\"2019-06-27\";att_end_date|s:10:\"2019-06-27\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('09csremfc6ii23fktqsmncfefiokgm7l', '192.168.1.96', 1561619950, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('mlf7vubfd3gkqorfko3mjildn9ps9vfk', '192.168.1.96', 1561633158, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-27\";student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";class_date|s:10:\"2019-06-27\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_flag|i:1;'),
('074e05irs9uhii5n2aihv2evn1vo7pnk', '192.168.1.65', 1561631711, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;'),
('1is32t411qknoslb1ih294jkgq0enbik', '192.168.1.138', 1561632777, 'teacher_id|s:2:\"30\";usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;s_message|s:26:\"Notice successfully added.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('5853uka9n6lblfds5svadfufev34nu5t', '192.168.1.68', 1561631282, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('cqfp6u190a9seh0pa6cpn6lkf0vp7mof', '192.168.1.110', 1561631144, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('u8vc9npuq6930iltidb9lfp6sokt2o8m', '192.168.1.65', 1561642111, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('8dbud8sc63f4lk7gag3hfh7lmejlcv58', '192.168.1.109', 1561697573, ''),
('kig2e5d7rolg5u7dpkrsonodlq0pu6r8', '192.168.1.109', 1561724120, 'usertype|s:1:\"2\";student_id|s:2:\"23\";class_id|s:1:\"1\";section_id|s:1:\"2\";query_date|s:10:\"2019-06-28\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";parent_id|s:2:\"15\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('f09978q19phmne8ama086a44uushashv', '192.168.1.110', 1561701025, 'teacher_id|s:1:\"8\";school_id|s:2:\"22\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";'),
('v007g3863tfos6vl4914vitgmhdh74hq', '192.168.1.96', 1561704797, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-28\";student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";class_date|s:10:\"2019-06-28\";teacher_id|s:1:\"8\";'),
('4r1dm5fmeg07ki9ajao8pk5m0oit5b8o', '192.168.1.54', 1561699086, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('0636hq71gp6s5cd39o1j53p8d7pa3l0e', '192.168.1.110', 1561705863, 'school_id|s:2:\"22\";school_is_logged_in|i:1;teacher_id|s:2:\"30\";'),
('9h9j5uc3h7l5r2l8ovmefvhv4pn1ud1h', '192.168.1.95', 1561701635, 'parent_id|s:2:\"26\";school_id|s:2:\"23\";school_is_logged_in|i:1;parent_is_logged_in|i:1;student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";student_flag|i:1;class_date|s:10:\"2019-06-28\";'),
('002vsfn60fqqrk4i5l6tu0v1t9hj7btj', '192.168.1.95', 1561699732, 'student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";school_id|s:2:\"23\";student_flag|i:1;'),
('jl72pkv7nkikaud7v8enpvgk82o45i2d', '192.168.1.138', 1561701333, ''),
('4l1q9d3idmuq9eojkkc5qsscf1p20e9j', '192.168.1.138', 1561701913, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|N;school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('8bkenvdqsfblkbuqrbf586j4laf62vfs', '192.168.1.52', 1561708240, ''),
('167fv3a2bc72cta5dsoku6iobbln2lh9', '192.168.1.133', 1561701952, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('9g79sktgfi478s1djfvhgcef4cuetr1k', '192.168.1.52', 1561723885, 'school_id|s:2:\"22\";school_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";class_date|s:10:\"2019-06-28\";teacher_id|s:2:\"30\";query_date|s:10:\"2019-06-28\";'),
('dh72b5162t61skvgd5sdlt2plbtsfl69', '192.168.1.138', 1561715954, ''),
('87fmrhfllu6n74pn6q53qq06r3j2frcg', '192.168.1.138', 1561722573, 'teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-28\";'),
('g7840202ktss7ehgmkatclfueku93gev', '192.168.1.96', 1561722284, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-28\";'),
('80efip2edmajss25b1rca1uq4cldnaih', '192.168.1.96', 1561719441, ''),
('e79skt84ndj6b8i9fip4v2be8nohdrtc', '192.168.1.96', 1561717817, ''),
('kj4sc5c6ab710naf9amdijttnrb96pq7', '192.168.1.105', 1561724992, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('idie4jbff79bs2lhmafl749q1g2is9uf', '192.168.1.110', 1561727191, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('fehjgieaq8gteikqk3l6ilo48klidl4t', '192.168.1.96', 1561719433, ''),
('889rn81hdmkt6ubj9obtdpbdqofoddbe', '192.168.1.110', 1561727287, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-28\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-28\";'),
('3gh1f20s692e00ofuvftrk8sfhf3nqhu', '192.168.1.105', 1561721109, ''),
('piqe1caneav5vhrs83giookskt8m7a1n', '192.168.1.105', 1561721163, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('omhvfqc7djrf55hsahlp798c8h03ga2c', '192.168.1.105', 1561721311, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('lkkntnb9kvr3flf963n5hrc84om9k5id', '192.168.1.105', 1561721595, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('le3nj89gcofs7snkt7n9ic7b6r0tibsg', '192.168.1.105', 1561721653, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('tuphjvl0dcfsal6gvs0lor66je3pkm8q', '192.168.1.105', 1561721918, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('pp3t2lefqbeqml00e88udpbv90fbphc3', '192.168.1.105', 1561721971, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('l4n6qml9jkpinoaes13j5lmp0a7cr20v', '192.168.1.54', 1561727554, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-28\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_flag|i:1;'),
('lg92s46jnm9uqmkhpffdqehirb6pph0o', '192.168.1.105', 1561722274, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('b7l9dhh8tfjquh8sj1l6s5ehb192kko7', '192.168.1.109', 1561724224, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('j8bdos6lbf55m16ngtog0e2e030os9u5', '192.168.1.109', 1561728135, 'school_id|s:1:\"1\";school_is_logged_in|i:1;user_id|s:2:\"15\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('cn3s0gkhqsaeqpu8vksot7smf8sk35rl', '192.168.1.138', 1561730393, ''),
('p8v47qfse7f8t1ie5vbs90sk36k0166v', '192.168.1.138', 1561730393, ''),
('hrc4jtm8729eg2mfs4h32bjaum4pbefb', '192.168.1.110', 1561786275, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('3jpte9ekvn90fk626ikc98uncf5suak8', '192.168.1.109', 1561785778, ''),
('a0mkc0dc7jpdamt9nbef89knjosf7608', '192.168.1.109', 1561785799, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('kqekl0svpb6989t5ho85db4042s75kg4', '192.168.1.109', 1561796358, 'student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";usertype|s:1:\"1\";school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";query_date|s:10:\"2019-06-29\";'),
('i5gglstovcl37eqm17vavmdifdqtgfbk', '192.168.1.110', 1561786222, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('2b7u2d6582fblf8fulpc22f4ssv7mdhl', '192.168.1.110', 1561793823, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-29\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-29\";'),
('1mmh0p2k79ncq0sp9t36ab42k5id88tm', '192.168.1.110', 1561786329, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('0p0a1ddq3u5de7efnsl51cphv779r75r', '192.168.1.110', 1561792600, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-29\";'),
('7p1nvg6jg3a0dur2plr66nrgnj5680sf', '192.168.1.110', 1561786780, ''),
('ljdp1laso5oh8t54ouvruk2oekbf8re3', '192.168.1.66', 1561790674, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('que1c7reond65ralmqt2gafgd6i1rare', '192.168.1.66', 1561790870, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('7abvh0pgoq34drndalpoe192c1lffd9q', '192.168.1.66', 1561793838, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-06-29\";query_date|s:10:\"2019-06-29\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-06-29\";teacher_id|s:1:\"8\";'),
('ot1l2mn1pq1krsu2d33otvvpb38i15si', '192.168.1.67', 1561793200, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-29\";s_message|s:24:\"Note successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('p7jp4fsf6fn9tt79v1pp8h765tlboie7', '192.168.1.110', 1561972599, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";'),
('0er48k15u3crepn0us9d315j0f0gpfb5', '192.168.1.69', 1561795112, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-06-27\";student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";class_date|s:10:\"2019-06-27\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_flag|i:1;'),
('el6hkcenl6o05a54s6cnnom98b63b87r', '192.168.1.109', 1561795297, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-06-29\";'),
('2rc7m3dd8ln26b73nbvn5ihfqmon7bcr', '192.168.1.109', 1561796161, ''),
('nmo46huoh14jrt9g7gubcnpv3amu9d7r', '192.168.1.109', 1561960669, ''),
('tsrh9qvka6gohre11vnctka47f5eer6u', '192.168.1.109', 1561974455, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('84kii5i2bu53r39kgq08eeuc659u21n1', '192.168.1.110', 1561958403, ''),
('dtnpm15tqqr6v5brl3i6gpri2go18ui0', '192.168.1.110', 1561973362, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-01\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-01\";teacher_id|s:1:\"8\";'),
('pbj7kvo9vbcot0i38bod5hpjf56j2iaj', '192.168.1.48', 1561971427, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-01\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";'),
('f3anigg4bf3fpechmgo2lotmlv3kkgia', '192.168.1.109', 1561974459, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('vjgsk74ssk55cev48sil26j0sbadsi44', '192.168.1.77', 1561973463, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-01\";query_date|s:10:\"2019-07-01\";teacher_id|s:1:\"8\";'),
('se8tm3v8gnuvvjelmuhh1m2q26etvtq3', '192.168.1.109', 1561985653, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('4uhn6ircct4tsjdr7p2rporosk1r651u', '192.168.1.77', 1561984806, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('3lvrjunu52hisvpfob5ksv85oaaanf92', '192.168.1.109', 1562043122, ''),
('8rmq6ejjauths9q60tq8dpcpsctdum0n', '192.168.1.109', 1562043970, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('90g5tjp6kkvsf8q2p0f062e8arpc3jjl', '192.168.1.109', 1562072589, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";query_date|s:10:\"2019-07-02\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('nhv9bu5gjmm0938gfifmmhkd6h2q3pjo', '192.168.1.70', 1562051543, 'school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('hlotb8srv5p7m85s08c6lp7tig8c7b42', '192.168.1.110', 1562072838, 'school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('vktshl8fa8tv3ie9hs0fm0gsu1920d3p', '192.168.1.110', 1562073157, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;usertype|s:1:\"2\";school_username|s:6:\"biplob\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('2chnshq76jdhrmtf1oeiq0npn5gkjnhl', '192.168.1.67', 1562048618, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('ptqulkl7h4qq3f8midts97p1tskt8023', '192.168.1.67', 1562053683, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-07-02\";s_message|s:26:\"record successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('mjf2jr7p4eos8p1erjd0d9msrsef28je', '192.168.1.93', 1562048818, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('sr2aitl5ov2ai73umugge3ccnuopvb7l', '192.168.1.93', 1562052531, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-02\";att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-02\";teacher_id|s:2:\"30\";'),
('cb6fdfsbdh54un10tk4v6t7gpc4drtdr', '192.168.1.110', 1562049777, ''),
('hm0n4uqmtutnoiha0fg2gli2l9vtnmji', '192.168.1.110', 1562049777, ''),
('41o57p9ibc3069egrvvs3d8j0cnmd7im', '192.168.1.138', 1562066539, 'usertype|s:1:\"1\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnNjYn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNjYn\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzY2Jw==\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('ib66fq0ptueuj4dtm5css5v42715ndb7', '192.168.1.105', 1562062807, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('cqrk61modf4f4ic58g4n5os0ah6va4re', '192.168.1.105', 1562063114, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('7i0emafuk41rmea6o5mu30rqe35u3i67', '192.168.1.105', 1562063368, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('7e8aneo44av4npbknmuniat14kkgigit', '192.168.1.105', 1562065123, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('24nbupkssimktk40jnp9brbqvue60fhe', '192.168.1.93', 1562066646, 'school_id|s:2:\"66\";school_is_logged_in|i:1;teacher_id|s:2:\"67\";'),
('9bq5v4a4kep3gso9356oda09vhs3ufiu', '192.168.1.105', 1562064099, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('jf8rbq6sb57sa2qhojqer437msbuhckt', '192.168.1.105', 1562064307, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('0fh55pm01rceiq64j0ive1a8lb6ks46h', '192.168.1.105', 1562064444, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('1m636c2l53gcuc6cro7ebs3rg6ip7ufq', '192.168.1.105', 1562064467, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('goet9urbags6c18bgm4p0dm4nuu70n91', '192.168.1.105', 1562065150, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('97ah874e7ssohqfllqgmkck26l6b4l6l', '192.168.1.105', 1562070651, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('h642c1vhp5pvhtv0la72pl6ejh6as2g6', '192.168.1.105', 1562067927, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('qaajb39ra866nmr7dc75n2g3gn8q1b31', '192.168.1.109', 1562068094, ''),
('j081g1gmtg5i67pcrpupg44qeagmlmhi', '192.168.1.109', 1562071234, ''),
('mo6cjltkr649227nu9fbvtsqlbe78jcs', '192.168.1.105', 1562070686, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('s77n949ql671g5uhauid1rsibfuuj4jm', '192.168.1.105', 1562071286, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;'),
('5e0u1h5cf7gfr3pjh6ohgg0g1hicb3n8', '192.168.1.110', 1562141391, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('ll28je4juahubd5i3geap5nok9abkvgv', '192.168.1.109', 1562129730, ''),
('0u8ltv9hhgg3hmnfd8in3c09vsivmq41', '192.168.1.109', 1562133909, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('musgqg3rgu1d9p4nl74ohep87q3vl8oq', '192.168.1.110', 1562149739, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('mav90662ma708ek07cvt90tkibvp9frp', '192.168.1.110', 1562133364, ''),
('bp3gi303j7gqp0vhtiqjm0ekqcjtmeum', '192.168.1.110', 1562133528, ''),
('6178l8vu045eset3gfo0ls50cd3p69q7', '192.168.1.105', 1562149531, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";'),
('1e5254assjmfall7qhk4633b2vt8pppc', '192.168.1.110', 1562149783, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('mi9lpujm6nr04994mktviep3ldv20st8', '192.168.1.138', 1562158472, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-03\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('i55geu7r8h344uchfl1loenqi8jhmjr2', '192.168.1.88', 1562158204, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-03\";att_start_date|s:10:\"2019-06-10\";att_end_date|s:10:\"2019-07-03\";student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";class_date|s:10:\"2019-07-03\";'),
('jf43mufggaotn4jf63ucg4qh1jsc6oc0', '192.168.1.69', 1562155810, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('5fe21tkotcd3l5j36k6om4jmapksk1bc', '192.168.1.89', 1562160425, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-03\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-03\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;'),
('d5a8sr4401dd53jlpd0dr35qh04qmr54', '192.168.1.69', 1562158601, 'school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('vraeo1qf0272v7oek0ppta11gjhn7979', '192.168.1.97', 1562163922, 'student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";school_id|s:2:\"23\";student_flag|i:1;'),
('lt0u5r66c36b0f42pd8q1utdu9f81r40', '192.168.1.110', 1562215161, ''),
('9g2jntupf0k5msi2fu47h3db12btmd36', '192.168.1.84', 1562226010, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-04\";teacher_id|s:1:\"8\";'),
('n2aigu8ug860th3ea2j5e7uccai0e0md', '192.168.1.73', 1562219512, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('qip4cev0e4fdr4i25q8sn2meuul7os9v', '192.168.1.93', 1562226811, ''),
('5ou16nh3ejvgnddnckc69rqd8ufhvnqb', '192.168.1.84', 1562226015, ''),
('cukpsb5oj6hqfjp1a2a5usgaagn8c1fa', '192.168.1.93', 1562219853, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('g55gpria5ca684e9s6mp75fadjlvbead', '192.168.1.93', 1562226809, 'school_id|s:1:\"1\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-04\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-04\";parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_flag|i:1;'),
('d9ti62tr0tbto615fcl45hsj0chodb8a', '192.168.1.111', 1562220025, ''),
('6intmd72b6t5ea6kega5jiq7loondeb2', '192.168.1.111', 1562220078, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-07-04\";'),
('t8ih771sc0ci37ri38669n2am0bto5dd', '192.168.1.109', 1562220233, ''),
('7kh5dhbjk0a1trsn19r8acm5b091pvvg', '192.168.1.109', 1562247899, 'query_date|s:10:\"2019-07-04\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-04\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_flag|i:1;school_username|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('vh8b2o9ia0e36podnu1amf8d07n4vaad', '192.168.1.111', 1562224353, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('c077ce0ksmdklfpnj640e6nv7ah7tgg3', '192.168.1.93', 1562225680, ''),
('sn0uhv8bcqt9h281tg06c5afk4aimja4', '192.168.1.93', 1562226811, ''),
('56hnsfsnmgr1o471lke42bt7o4jsnu6g', '192.168.1.73', 1562235714, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('mek4c3b76u1ldmuj76bcigrroekd2a0s', '192.168.1.110', 1562232277, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('iu6lr9798etv3vi1aq655m9cl16r6u61', '192.168.1.73', 1562236561, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('e9ithmkrlsi8eor4u0tdif65gsva7bjk', '192.168.1.110', 1562238902, ''),
('nng3pbk686c8ogslb3ndr6i5i56rle3t', '192.168.1.109', 1562247777, ''),
('u9e8fmd6lu4farb38in5iolp4el4slns', '192.168.1.109', 1562304962, ''),
('u0oefc75c1dfh6gsvug2inhfcvrqojh3', '192.168.1.109', 1562332992, 'usertype|s:1:\"1\";teacher_id|s:1:\"8\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";'),
('hg6tuct3knnjiqml53ecner7m00m3u83', '192.168.1.111', 1562303833, ''),
('rpeedua6tedlcpbetl3b9h0030dr5b0q', '192.168.1.111', 1562313032, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('fo7hrqosa2dnicctctv0mc0oit595iii', '192.168.1.110', 1562306483, ''),
('fovirko4hf2sfjdcde6vo9lbpqivvlsp', '192.168.1.109', 1562322209, 'usertype|s:1:\"2\";teacher_id|s:1:\"4\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:10:\"9474728606\";school_is_logged_in|i:1;'),
('2ruga3g646jkteo5s80go39q9ujo1dcg', '192.168.1.138', 1562331720, 'usertype|s:1:\"1\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzIyJw==\";teacher_id|s:2:\"30\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('3sfbu7sf331p3ppnm1f0nb3rg84j028q', '192.168.1.109', 1562326307, ''),
('uhj5c0ed05gn4dc108mpfu2jvfo6eje0', '192.168.1.77', 1562332374, 'school_id|s:2:\"22\";school_is_logged_in|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";class_date|s:10:\"2019-07-05\";query_date|s:10:\"2019-07-05\";teacher_id|s:2:\"30\";'),
('rhonk47uvh20dhmpakqur1oo70jd46lc', '192.168.1.77', 1562332317, ''),
('l81hgh7hkq3mv2u5kb3g0k7qas6kup2n', '192.168.1.96', 1562329390, ''),
('hggisa4irv3gefc30c2nj6gc0co6h08h', '192.168.1.109', 1562590449, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('9v1fs2qlp1pbiounf8hohgsh6qjks9bs', '192.168.1.109', 1562561807, ''),
('ukodsqkik3a3ljuakqis29fu24vpa6d8', '192.168.1.75', 1562566119, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-08\";school_is_logged_in|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('te7hpttlf6l1lpgqpdp7i0rncrkvgu4m', '192.168.1.74', 1562564496, 'school_id|s:2:\"22\";school_is_logged_in|i:1;parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-07-05\";'),
('82ua30vjeb5huar64ioe120q25gta36b', '192.168.1.110', 1562566099, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-08\";teacher_id|s:1:\"8\";'),
('686nodejdc2g0p87opes5mcaaqo8mc6o', '192.168.1.110', 1562566166, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('5ijmqo6lrbtc1vm5hvei6pgrl1419fkk', '192.168.1.110', 1562566176, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('l6u19l522s5oki2ve1lhmcirqgvhe4m0', '192.168.1.71', 1562579019, 'student_id|s:1:\"8\";class_id|s:1:\"9\";section_id|s:2:\"15\";school_id|s:2:\"23\";student_flag|i:1;'),
('02pdms7huoj1v03h83v374oudto4hhh7', '192.168.1.98', 1562581927, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('jhribeip1jllb8mibhj97fm4uopmd76u', '192.168.1.98', 1562584600, 'school_id|s:1:\"1\";school_is_logged_in|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('87b9714qsv3mo2847ah1pb10vshnida8', '192.168.1.109', 1562582105, ''),
('a0t9rmpkjsgnqol9kqumncp4etr1klbu', '192.168.1.109', 1562591166, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-08\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-08\";teacher_id|s:1:\"8\";att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-08\";'),
('kb1fl6i8q1shgo857v3u1vp96ik5i9ql', '192.168.1.110', 1562649989, ''),
('k7rhls3e2vu6835lk0et8sgvr2odv1k4', '192.168.1.109', 1562650738, ''),
('7bhjn5p0nvoinpm8l6tuahpl3lm6t4qg', '192.168.1.109', 1562656045, 'school_id|s:1:\"1\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";query_date|s:10:\"2019-07-09\";att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-09\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('au1rq9suokvfnvfo81lbs0ktp6di3pi5', '192.168.1.86', 1562651290, 'school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-09\";school_is_logged_in|i:1;'),
('bdt11gbpfdav4iagcpj2m2upsbigfp4o', '192.168.1.80', 1562655620, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"16\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-07-09\";'),
('vpv8lm8qrg62bmmlrj5r8oa0u533b1b1', '192.168.1.95', 1562654279, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-09\";teacher_id|s:1:\"8\";s_message|s:28:\"Note successfully Published.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('3acp042hpl5jk22ae56npnv4lfdu76nb', '192.168.1.109', 1562652527, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-07-09\";'),
('29d79cm49rr5ks3nqb51s81j22vq080s', '192.168.1.109', 1562656067, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('850ofhg51eio32aa5uvbmi26bbuuckn3', '192.168.1.109', 1562678870, 'usertype|s:1:\"2\";query_date|s:10:\"2019-07-09\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('kj705an53rg6r40minkr0j6fu3bg93nh', '192.168.1.110', 1562665672, ''),
('1spd2tf6tqc1m5mueisj36smsdg15ns6', '192.168.1.110', 1562665725, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";'),
('567oaohdvtgu40gglq15jidibb20gdca', '192.168.1.78', 1562668970, ''),
('io7jhot16t2in24piordvtdlgs599ccs', '192.168.1.95', 1562667249, ''),
('0c4aqp5mitl9k6r873fook640oa6dm79', '192.168.1.95', 1562667323, ''),
('di60vcf26lpldkrueumhosto6r90esae', '192.168.1.78', 1562669320, ''),
('nbae4dub1mst0tatoqstagknhc25fg9m', '192.168.1.133', 1562669783, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('pjt9mbqqa16lfm1j0la3dqoqjmsr1ktf', '192.168.1.133', 1562669564, ''),
('mdb8enn8dv2epq5mveblq1trbfuamnpb', '192.168.1.133', 1562669611, ''),
('utucjhinc7v2l2p8j6qaa57vn8734bm9', '192.168.1.78', 1562679785, 'school_id|s:2:\"22\";school_is_logged_in|i:1;'),
('7s7hbkscig541qffls87pv6dnqattuna', '192.168.1.110', 1562670375, ''),
('lrp99s0pkpskjj8qieq737mlbhh4r17m', '192.168.1.110', 1562670388, ''),
('t0fodq5tnh4cub5d4cljvq1v8pvech2q', '192.168.1.110', 1562672191, ''),
('237aqmon0eghhu461lekbiicg5gtif0v', '192.168.1.109', 1562670864, ''),
('amvua6sterf2oj8qovfdok24jcc2stni', '192.168.1.100', 1562676315, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;s_message|s:26:\"record successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('goee3lcnphrhf53l87jmof6kgc00jin6', '192.168.1.109', 1562737769, ''),
('mp2clop6bggsi15ckaa7bovtfmjh6f9r', '192.168.1.109', 1562762957, 'usertype|s:1:\"2\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;att_start_date|s:10:\"2019-04-01\";att_end_date|s:10:\"2019-07-10\";'),
('32fq1u4rdn5gnqnuh1ip7sevoceleh4n', '192.168.1.105', 1562738427, 'usertype|s:1:\"1\";school_id|s:2:\"50\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";school_is_logged_in|i:1;teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";teacher_id|s:2:\"52\";query_date|s:10:\"2019-07-10\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";'),
('rc6uuink8n7qnedivoavti28eu78p94n', '192.168.1.105', 1562737102, ''),
('9jfbmsrgd1llekdk5b3k0l71i6739ijm', '192.168.1.109', 1562750053, ''),
('oc5ip4at2tvghuo2aq0rtpcn6ohpegfd', '192.168.1.90', 1562755208, ''),
('6pau84ruv30pjqlr5i3obi4e4th5nd4a', '192.168.1.90', 1562758832, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-10\";teacher_id|s:1:\"8\";s_message|s:24:\"Note successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('eurqr6fq2lughu5ciilvu4ocn4qqccro', '192.168.1.110', 1562755727, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;att_start_date|s:10:\"01-04-2019\";att_end_date|s:10:\"10-07-2019\";'),
('o63fnni67989vjlblmd8tr8auundnt4g', '192.168.1.109', 1562758580, ''),
('2f6klqdeal2b6qlq2bgiphlk2saf082p', '192.168.1.109', 1562825097, ''),
('u5j4ab4km33057g3egjbb22iiicmlih4', '192.168.1.109', 1562852830, 'usertype|s:1:\"1\";teacher_id|s:1:\"8\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnMSc=\";subject_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";teacher_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzEn\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:6:\"biplob\";school_is_logged_in|i:1;'),
('9hm33u1ukifu8dbh83aqernih6cef9r3', '192.168.1.105', 1562823392, ''),
('91rrgc508cc0rqnh0jndf5gcfnq29r1g', '192.168.1.105', 1562825428, 'teacher_id|s:2:\"54\";school_id|s:2:\"50\";school_is_logged_in|i:1;usertype|s:1:\"1\";school_username|s:5:\"66774\";school_email|s:20:\"sankhya555@gmail.com\";section_sql|s:64:\"U2VsZWN0ICogZnJvbSB0Ymxfc2VjdGlvbiB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";classes_sql|s:64:\"U2VsZWN0ICogZnJvbSB0YmxfY2xhc3NlcyB3aGVyZSBzY2hvb2xfaWQgPSAnNTAn\";subject_sql|s:68:\"U2VsZWN0ICogZnJvbSB0Ymxfc3ViamVjdHMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";teacher_sql|s:68:\"U2VsZWN0ICogZnJvbSB0YmxfdGVhY2hlcnMgd2hlcmUgc2Nob29sX2lkID0gJzUwJw==\";query_date|s:10:\"2019-07-11\";'),
('r0sn75a1sf0ssic4jul1kdrfmus4tioo', '192.168.1.133', 1562824843, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('ssar7n3gk7gcmie2q6bgr1qmiqlc5jsj', '192.168.1.70', 1562837875, ''),
('6qa50st8kgmuq28dmjejnu405ojcok6a', '192.168.1.109', 1562911896, 'usertype|s:1:\"1\";'),
('rsa4199hg1nk1j0rptsml09afms75v9i', '192.168.1.110', 1562840642, ''),
('0avtffekrb6f0qjfqfps7m664mutjjqi', '192.168.1.110', 1562840642, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('0bdiqvhioju173ujn9u2fhdotgfdg785', '192.168.1.105', 1562842218, ''),
('2e05t9067mngs4t3aoi34j2ks4dvs8ke', '192.168.1.109', 1562907673, ''),
('cgkpdn395svfaj19k427rco3f1p3rbdb', '192.168.1.109', 1562916898, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('96p3m3c3eqdrvriubg1ejnl6a9uv3p6p', '192.168.1.109', 1562936189, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"16\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";'),
('pn2lqpod61eeqpa8ijolivjha8nqh8ts', '192.168.1.109', 1563023738, 'usertype|s:1:\"2\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";user_id|s:2:\"15\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;'),
('bddr3blla2c9jii6btomfslfo2j4tn8n', '192.168.1.109', 1563166489, ''),
('khkca26t4h17qe2s4vk1hfk31nb81kss', '192.168.1.109', 1563196736, 'usertype|s:1:\"2\";query_date|s:10:\"2019-07-15\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('v69gpj6mt14emb48bip7m2kbbklngpul', '192.168.1.109', 1563174679, ''),
('sdddv3e9t8g64q89u0jk8315p4t5glrs', '192.168.1.110', 1563175627, ''),
('6du33n12hvsp2d2junch7tkmqim0u62u', '192.168.1.109', 1563185706, ''),
('lqfcnqq71jhb6vh5o87jnujh2rd2veq5', '192.168.1.110', 1563193192, ''),
('6d0pnik2g5n3sp3c2plout1mdr89m5lo', '192.168.1.109', 1563253138, ''),
('m1uhq7cldop413bm78pfa66kkj5p2rcg', '192.168.1.110', 1563193254, ''),
('fh589o1og4pnchnfcn4mqa29eopqobth', '192.168.1.63', 1563194459, ''),
('thiek0nvctn9g3k4qm7p97g0hah1ijpl', '192.168.1.109', 1563261209, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('9ceda50j7vicqo98rd9urus6gs00l1o2', '192.168.1.109', 1563264020, ''),
('te33mo8lglm8bkf52u3ma9chj71ft1u6', '192.168.1.109', 1563283689, 'usertype|s:1:\"2\";school_id|s:1:\"1\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_is_logged_in|i:1;att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-16\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";query_date|s:10:\"2019-07-16\";'),
('8teri2j5p7lvk1aacicieokeaaq5jqkm', '192.168.1.111', 1563339644, ''),
('4i8t314rg1rclhm5fl315v3p6p6c1fi5', '192.168.1.111', 1563339649, ''),
('skv0mc6bgu2j2qsk3h5chgra07u33kc6', '192.168.1.109', 1563340013, ''),
('kko5hi5ujevhpag01sg065elg1m64k72', '192.168.1.109', 1563365479, 'class_id|s:1:\"1\";section_id|s:1:\"2\";usertype|s:1:\"2\";school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-15\";teacher_id|s:2:\"30\";'),
('p1s7r17nbsttdboc5ss294q94719un7a', '192.168.1.109', 1563425889, ''),
('g1v3hm6qcf0bsu0t2j0f1h5h0onp1jnq', '192.168.1.109', 1563438595, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('vnj80152vnep83m4eq586limtr2tu53e', '192.168.1.109', 1563511483, ''),
('pl02g2c4nndabj5g9r35g90rjnnimq2m', '192.168.1.109', 1563520429, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";'),
('um8rpq86s74qklkllc43r90jici3ov58', '192.168.1.109', 1563523800, 'parent_id|s:2:\"82\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:29:\"Student successfully created.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('1ai4ubgtn7ftao529oeh4cciq4kqtlae', '192.168.1.109', 1563520793, ''),
('oigihcutknjlpk2q5qvg08ret8tbta8t', '192.168.1.109', 1563540862, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-19\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-19\";'),
('f84ami35cbniadutoksfjub5t7eue6q3', '192.168.1.109', 1563529489, ''),
('rcvddvim0f46ei5423fcevgnclkot864', '192.168.1.97', 1563534670, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('4gk6atbd1t38gbat8imcgmve5qitpok0', '192.168.1.109', 1563540856, ''),
('1kl9t9sdo6b456o5npflsjhdc8oqk6ul', '192.168.1.109', 1563771849, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('rr6nbkg8poco1qfir0o66928pf2h6khe', '192.168.1.133', 1563772249, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('to57ic2t15rl8e6nfb4kk1l1hu5vkdkv', '192.168.1.109', 1563789095, 'usertype|s:1:\"2\";school_id|N;teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:6:\"102308\";class_id|N;section_id|N;student_flag|i:1;'),
('fs4de9n0nacri1bmqmf71ei6kg3f6v16', '192.168.1.109', 1563801384, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-22\";s_message|s:29:\"Teacher deleted successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('b6bnfncncm481tlt726jdr667utvgu4t', '192.168.1.81', 1563795062, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-22\";'),
('7itt67d0bhc1oh0jli3277qtem6h2nek', '192.168.1.109', 1563794731, 'parent_id|s:2:\"16\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-07-22\";'),
('dfuuin6rbjmru79slim279aoqkf77i7k', '192.168.1.109', 1563864900, ''),
('0vlakevbjdf9tvt027ibp5e2gh6gvg7b', '192.168.1.109', 1563867853, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-23\";'),
('837hu17vui18o0fnlmjleqm4s9cg3jsl', '192.168.1.59', 1563866391, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('a8alavj4u43ci9q79ma9sd06cv7gf68a', '192.168.1.110', 1563873094, ''),
('cn2vqnh1qogm8fgc8dng2lnpnb1t0e3e', '192.168.1.50', 1563873130, ''),
('gjtshro51l5f3crgmpgsls1pipd5kh2v', '192.168.1.109', 1563882169, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('5tbjiks2o9b7p9757jsanjfba9ivjqfn', '192.168.1.109', 1563888896, 'usertype|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;class_date|s:10:\"2019-07-23\";'),
('lrt6ndsmmucf8g3lgnbmgn5ns9qo6ip2', '192.168.1.110', 1563887591, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-23\";'),
('uiuqdkurdjgkg5pgedjc0ionrc9fpv8s', '192.168.1.110', 1563886489, ''),
('tjovtqjiff72dhdat93b8mndvirscll3', '192.168.1.50', 1563887275, ''),
('v7hdgaf0k6lq5gmagpglj559tg6r4g88', '192.168.1.133', 1563888471, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('6go8fp8dmkun0i1rvkkcqi9skqm7q9lv', '192.168.1.64', 1563889472, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-23\";'),
('orlh6qagc6of4nb8ct64k7gj5ae1n80v', '192.168.1.133', 1563888530, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('cl3i6negmdufklepr90kfrk3gc34bc8k', '192.168.1.64', 1563888674, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-23\";'),
('uhfj7kipvh0b0c3oraa0f1qhnbgn9a97', '192.168.1.110', 1563950617, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";'),
('petmc64bpbbqccao8ut7l5p1b55sj2ok', '192.168.1.96', 1563943886, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-24\";'),
('kho4c60fkn9q2lhufk109soefgjisk72', '192.168.1.110', 1563945805, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";'),
('uptjr0grs2da99hqv01eaujrrom1tbr0', '192.168.1.96', 1563951524, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";teacher_id|s:2:\"30\";'),
('end2rgkn4fsm6774qjta868hsrb9805h', '192.168.1.110', 1563951347, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";teacher_id|s:1:\"8\";'),
('tm7194qthmu6gm328if193t45pta7ij0', '192.168.1.110', 1563950896, ''),
('h7uruq9aiamd14hl9j92fe2a9iahbkpo', '192.168.1.49', 1563950955, ''),
('gn54hqqcdps1n94m63hjr3456pkedr8a', '192.168.1.49', 1563952551, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";'),
('uml26od9pjcfnpfhf9ch15vc0snrbkmv', '192.168.1.96', 1563952381, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-24\";'),
('0bhonchgqkpjupqkmqttbega52un5m45', '192.168.1.96', 1563953674, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";'),
('kb2bpat3jvthibr6pgfba92v28j6o5r2', '192.168.1.96', 1563959059, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-24\";'),
('2o9hq388863dpaabebphs3v0duqm797h', '192.168.1.110', 1563958841, ''),
('9o9blip3v2vk0dp9l8g6c78lrbp1vb1r', '192.168.1.96', 1563962285, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-07-24\";'),
('7t4jlp7b2dr5lpc4478kjomjmglkiho1', '192.168.1.49', 1563965594, ''),
('3pved5c01o3r11rb0o1kamg785vdrtu9', '192.168.1.96', 1563973166, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('r41752hiob7bmbiba7vnst8muo253fmf', '192.168.1.109', 1564033609, ''),
('r88eb5qhqvjoif5v17k14od7j7slad2t', '192.168.1.109', 1564041161, 'school_id|s:1:\"1\";school_is_logged_in|i:1;class_id|s:1:\"1\";section_id|s:1:\"2\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";'),
('cppt5um7gf7dhp7f9t72rh84fe980rpr', '192.168.1.91', 1564036526, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-25\";'),
('01oqgtpiu9oi44t48abi7hmcorc70ral', '192.168.1.95', 1564039354, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('tq7c4s5tfal02i3k287lol0vq6iu6ake', '192.168.1.109', 1564040785, ''),
('0s1uaa1qm4sgvhnq3ihrghq3c2vgj7vc', '192.168.1.109', 1564051272, ''),
('dlafi8b2mk553r14efq2ofrojtj017r6', '192.168.1.109', 1564054415, 'parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";'),
('6ufskp46k3gdafgu67f25pmftt3r58fi', '192.168.1.110', 1564055645, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-25\";'),
('ga011kn4aedakbmo3l9thsegemedu11r', '192.168.1.109', 1564054444, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;'),
('q2sh1qr26k2gv3o62jh9cj2691urd1i5', '192.168.1.109', 1564061274, 'class_id|s:1:\"1\";section_id|s:1:\"2\";class_date|s:10:\"2019-07-25\";query_date|s:10:\"2019-07-25\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('31n98bga1hk8d0ocfl8mso5tjsl9d3u0', '192.168.1.91', 1564055664, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-25\";'),
('3o7iaik19lnbef3vfvbdrjm17l2qtk4j', '192.168.1.95', 1564054732, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-25\";'),
('55mohlhpiqf3jpf64c85it4nrds0b1sd', '192.168.1.109', 1564126283, ''),
('psrmr34mcd5klr6pgmrplrfp2q7tkmh3', '192.168.1.109', 1564144582, 'usertype|s:1:\"2\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('vnhptcl8v736feiokl5h77tgd9sjmhci', '192.168.1.110', 1564136612, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-26\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-07-26\";'),
('d1chhtk38ot4cgemolp7p6v891jlhn55', '192.168.1.55', 1564128336, 'school_id|s:1:\"1\";school_is_logged_in|i:1;');
INSERT INTO `tbl_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('ujo2h67md3rb99es2vfbfivse60nls32', '192.168.1.55', 1564136678, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-07-26\";'),
('0s64qk7jhbevjv7igi57hs2dhg6i589h', '192.168.1.110', 1564144476, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-26\";'),
('6lrptvjm5a8qequ73t52v3ogmkbpdcff', '192.168.1.69', 1564135087, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-26\";'),
('3sp1hguqrjk1g1458qrdnce19622ovlm', '192.168.1.69', 1564143284, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";'),
('qlrrheabpqdtq47no02gsuteejdpk7ts', '192.168.1.72', 1564136432, 'school_id|s:1:\"1\";class_id|s:1:\"7\";section_id|s:2:\"29\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('fo6o1m54ocos7ctdckuug2ac6s6vo5hl', '192.168.1.109', 1564146089, 'school_id|s:1:\"1\";school_is_logged_in|i:1;class_id|s:1:\"1\";section_id|s:1:\"2\";'),
('hm9205qn0pe6b3d7onfn4gl5mi5dbe27', '192.168.1.97', 1564204745, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-07-27\";att_start_date|s:10:\"2019-07-01\";att_end_date|s:10:\"2019-07-27\";'),
('i86uv3ke86glldq024ahe4k6b4jtfj7v', '192.168.1.109', 1564207800, ''),
('rh5g967et641v731jmisn44outan16dh', '192.168.1.111', 1564233417, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;smscrrp_school_id|s:0:\"\";usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;smscrrp_from_date|s:0:\"\";smscrrp_to_date|s:0:\"\";'),
('muhf2rgaapmakd5gurpgmopg4a2k7nir', '192.168.1.110', 1564378419, ''),
('nb8pt0jf43et80t3gk23853akq0qalkt', '192.168.1.110', 1564378510, 'school_id|N;school_is_logged_in|i:1;query_date|s:10:\"2019-07-29\";'),
('3evigtv40f5253ffjl84c28srvjcvu6e', '192.168.1.111', 1564405717, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('s0at8plebjdt9foec698254sv6m216s1', '192.168.1.110', 1564378598, ''),
('9lf2ls1idmtmpnnkm9gd342cku3numk5', '192.168.1.98', 1564406470, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('36vjla9phc8kg2rakpb2slcsv9v86srq', '192.168.1.109', 1564461899, ''),
('utf19ie4va8svegkafm301t4s2r40akb', '192.168.1.109', 1564486232, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:17:\"biplob1@ablion.in\";school_is_logged_in|i:1;'),
('805sgq11qh39eu4hver1q85t7mhr50vs', '192.168.1.110', 1564472199, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('shgno4739ao0h74b48q8j9m39v7d5ahm', '192.168.1.110', 1564481595, ''),
('3fcprgdqvl0kpugbbnru3f4pgrbhog4l', '192.168.1.110', 1564481600, ''),
('0mv6koekuichm1eh2ol53kiocr2ghjre', '192.168.1.110', 1564481646, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('i9ioel8f40g3tecorua171qguuhpd98g', '192.168.1.110', 1564481616, ''),
('58hjb5lsfr01eb8akq1cjgrh85ivvvm4', '192.168.1.110', 1564481779, ''),
('otpvn3m9l8m458b2oah0btrs112glkb4', '192.168.1.110', 1564481779, ''),
('04gpits37mtbhcaurstg0v0231fge2ce', '192.168.1.110', 1564481791, ''),
('sd74lgror2k6f2c0i9au39ec2bqapd6e', '192.168.1.109', 1564483857, ''),
('l84vdmbo5hopdnbhqqdoafuv08q3ln5v', '192.168.1.109', 1564486732, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('2b2auq0e1gllpmhjsrehnl6l96cavnol', '192.168.1.57', 1564490928, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('mpm6v1mab8krlv81scrl9p9n4nosv3tr', '192.168.1.110', 1564550415, ''),
('h23thlqikrev2i47a1njttosip4gng30', '192.168.1.110', 1564552536, ''),
('pvb3mtoi81etnjp0ti3qhs5f3bqps6nb', '192.168.1.93', 1564571908, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('27b8iqisenohpk9h32avohdar1m6m4cu', '192.168.1.109', 1564573668, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('2lahcm1nfe8ut9s2inf8fbmnjcpgbbq2', '192.168.1.109', 1564581098, ''),
('pcrn0v9bpfejc4q9kk4t2gf4slrhks1b', '192.168.1.109', 1564581110, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('fna69qhrm5scenpvteursbiukm73eb8f', '192.168.1.110', 1564633977, ''),
('lrvjt9i9kk0vikgbf6ipv39c2hhqhh4k', '192.168.1.109', 1564638647, ''),
('2ngh6kfkl3hgthnerivr4ej5o55pet2v', '192.168.1.109', 1564644670, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-01\";'),
('ks6rp5p5gkm04fs843vt0h6nlq77vdi7', '192.168.1.110', 1564643219, ''),
('amffcvb842gor1tqm8u6qi4jfqve370t', '192.168.1.109', 1564642035, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-08-01\";'),
('9msv28norol7rqtmqt2tt0t8bp36h44i', '192.168.1.110', 1564643219, ''),
('4986tlfssj6gckmtvjqvb6d6m075ci4e', '192.168.1.138', 1564645310, ''),
('hmtn2t8reaf5p1muts195rpqersd0hnu', '192.168.1.110', 1564651406, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;'),
('p9mh80kqd6h5ba2adkrio9mls4888g9j', '192.168.1.110', 1564650122, ''),
('t3s1t99ofd719m3c8rn066v586urg3ls', '192.168.1.110', 1564650279, ''),
('nhig2im8cc8u0bnov8g1h0lbpecjqegv', '192.168.1.110', 1564653916, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('f7svspl6alk8ukhmrfb6su3ll1fgkvu4', '192.168.1.110', 1564650420, ''),
('sgppsrkuf84od0pu92d7b346fohq0d87', '192.168.1.109', 1564663850, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-01\";usertype|s:1:\"2\";school_username|s:6:\"biplob\";'),
('bjgbkrtt86po61un3i4fspt405iovbiu', '192.168.1.90', 1564656156, 'school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('n6gi37hgpkmgrvgjn5qeq5oamhes4n3t', '192.168.1.90', 1564656169, 'school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('lvoirp4mk630mqmqhc61i9soh27jad30', '192.168.1.90', 1564660223, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";'),
('16253lsgfcuvn96rir2taetjttvoev01', '192.168.1.110', 1564660773, ''),
('i7sui5ain33091dbnjdi93e1tmlmm8op', '192.168.1.96', 1564657766, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-08-01\";'),
('53kho61397qvvgh59o8if9ccqq8jijig', '192.168.1.81', 1564656724, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('o1ojpr6j18s77rtf9kub95qja6pe5so6', '192.168.1.109', 1564659100, ''),
('nt49ntd63vhkfjoldj1h6l07634tdp0h', '192.168.1.110', 1564662300, ''),
('c2mcqpqbq4u7p0gjef16mo8badofocv9', '192.168.1.109', 1564725625, ''),
('perijmuvgemak9agjhqokivhbk50ocdn', '192.168.1.109', 1564725798, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;usertype|s:1:\"2\";school_username|s:6:\"biplob\";'),
('bdeut3ra89o2fi9m31tn09e8fhd02jc5', '192.168.1.111', 1564722291, ''),
('l7mu7chmcro19u42ha1nm1qqucu72kk6', '192.168.1.111', 1564731586, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"2\";school_email|s:15:\"joseph@mail.com\";school_is_logged_in|i:1;smscrrp_school_id|s:0:\"\";smscrrp_from_date|s:0:\"\";smscrrp_to_date|s:0:\"\";'),
('hkriba8q7rc0nmd7f4b031jk7knl7hca', '192.168.1.36', 1564722465, 'student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";school_id|s:2:\"22\";student_flag|i:1;'),
('3ncn8f3cmnqu5assl1vrgsp75c7p30n9', '192.168.1.38', 1564726332, 'school_id|s:1:\"1\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";'),
('uoblerj5nlop5ipdcd83tae6qqm09sg8', '192.168.1.38', 1564726642, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('kqtt36kninjfu54599f7rcobhh1m84d2', '192.168.1.109', 1564726466, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('hc1f63vn1obb2597tpmcjm240a48a6sl', '192.168.1.109', 1564739175, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('qq1c90romt6cfbrqn0qmts0a0ril98le', '192.168.1.111', 1564742730, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('1ohi5aqqp6n56luljn672f3pra141ead', '192.168.1.109', 1564746610, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('r3o9bnpa3ueeutcj9amfjuboho5q0koq', '192.168.1.137', 1564986426, ''),
('oqn9s6hd9hkbqsvreceupemu0mn3pr0d', '192.168.1.137', 1564986442, ''),
('doh7oc5ote3fst0va0jhplt3hf281pss', '192.168.1.138', 1564990324, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('o8c6jrem613be9242gg6lrupr8a8o3pe', '192.168.1.109', 1564990864, ''),
('njjdmmm109od320bv2frna2rjeo1rccs', '192.168.1.109', 1565000983, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('gu689lrcv974fv6pnbd7amfpl5k1ci4k', '192.168.1.109', 1565002349, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-05\";'),
('i0grjl5rjtfkd5g9b4j1f9lqt19ap4da', '192.168.1.138', 1565005205, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:26:\"Notice successfully added.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('000dmedeankuv1ees4598i67h3ee73dn', '192.168.1.109', 1565068006, ''),
('hmnmqlufbcq2septlc10tg8qcv13ka8e', '192.168.1.109', 1565094410, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-06\";school_username|s:6:\"biplob\";class_id|s:1:\"1\";section_id|s:1:\"2\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";student_flag|i:1;'),
('ifugl01skv2416um4gbm099qf790mgoe', '192.168.1.109', 1565078362, ''),
('3ge2qjrgrcf938l1l7vprt25fl56i17g', '192.168.1.111', 1565175562, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('l93mec98e9mj9jebj8u6psg5bs8ah7ti', '192.168.1.110', 1565251593, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";school_is_logged_in|i:1;'),
('8puguirv78d3a9ovmbmh28c80qqbutu1', '192.168.1.109', 1565243264, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('r7aie38eigbhqjhsrc0forvb0f602jnr', '192.168.1.109', 1565263540, 'parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('ml3n7q7379dpvj2hsf2qu1g3mtrvg9n7', '192.168.1.76', 1565243429, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('fqbhtobpg05cmkom80nhdfi2lud979u3', '192.168.1.110', 1565244307, ''),
('73ba11386ckiimf74jb7a1svr6i85q0l', '192.168.1.76', 1565250883, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('eu61jf5lsa01ki46s77t2j3uje15gqqv', '192.168.1.76', 1565247951, ''),
('qp3co3lm3ls5kr31kvnuisdle860g2lr', '192.168.1.110', 1565258804, ''),
('65d5ktvt7u1089qo7lqh8jgun1hmfknk', '192.168.1.110', 1565263392, ''),
('4g2ge4hm44bao1mpq4nv49miu2jko3ic', '192.168.1.109', 1566394981, 'class_id|s:1:\"1\";section_id|s:1:\"2\";att_start_date|s:10:\"2019-06-01\";att_end_date|s:10:\"2019-08-21\";query_date|s:10:\"2019-08-21\";usertype|s:1:\"2\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";student_flag|i:1;'),
('h292b1iai6hfk9389ll66h5t43aphiq2', '192.168.1.109', 1566392279, ''),
('da6rqr3qrlttl6u6l2dd7eca7k78uc56', '192.168.1.109', 1566449669, ''),
('8u5u8usg84lh2dmcn3dlkhcn1hailfnv', '192.168.1.109', 1566449702, 'school_id|s:1:\"1\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('61i2s058ftunblc1bmopj8re902a0rhg', '192.168.1.109', 1566478324, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;school_is_logged_in|i:1;query_date|s:10:\"2019-08-22\";att_start_date|s:10:\"2019-08-01\";att_end_date|s:10:\"2019-08-22\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";'),
('4jkaad81d8f830577l9f07lsmv802gd7', '192.168.1.110', 1566457771, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('v83jb4q5p1mmq3nf16h944t61r7g5usb', '192.168.1.110', 1566451198, ''),
('6rbv7mqc3tqfoo3vgqge5a6oa18srleo', '192.168.1.110', 1566451198, ''),
('bdso0r0ev5cqmdu68lh1o21nif2qpdoj', '192.168.1.110', 1566452730, 'school_id|s:1:\"1\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('dr3ij3jc65jit3aju883lmtvf45eeof3', '192.168.1.110', 1566451667, ''),
('6d49n3j7ktgmu6dr66upg2in7v74bvg6', '192.168.1.109', 1566457538, ''),
('l2njd93pcauv5mobnr0b9dh4ddhsfmd4', '192.168.1.109', 1566459585, ''),
('jqplcb1enrrt2ahti5uakkvjnobum7os', '192.168.1.110', 1566474168, ''),
('f01tufl8g48pv5f3da89bs33rssdl407', '192.168.1.110', 1566475668, ''),
('jgrnk9oor04be5sqrfb5l6vtdm8dp6kp', '192.168.1.109', 1566560666, ''),
('n0rbsrgfk4irsvagqrlro71r2mc6qofv', '192.168.1.109', 1566561415, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-23\";usertype|s:1:\"2\";school_username|s:6:\"biplob\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('7pv2j4fbpt5fgjicrv6p78uo0cmed9b3', '192.168.1.109', 1566561547, 'parent_id|s:2:\"94\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"30\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('ofltrtijvfj1gs059064eqhglcvemnbd', '192.168.1.109', 1566566261, 'parent_id|s:2:\"94\";parent_is_logged_in|i:1;student_id|s:2:\"30\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('hi3bhj6r8m6toho5epj1pe39amjc9qlb', '192.168.1.111', 1566566325, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('rp8i7bmvp4h1fprr0faasanqo96nj9ho', '192.168.1.111', 1566622694, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('169cnfkfgfg69q58ebcmvtc8lpgd7idb', '192.168.1.111', 1566626592, 'school_id|s:1:\"1\";school_is_logged_in|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('9259jmggb82t7o4a2ebnkldvo04q1rh1', '192.168.1.109', 1566622733, ''),
('5piaa6nsojeti567lb66dvg9ud87ot7t', '192.168.1.109', 1566633803, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('th8gqk6uh29jtf4ir0pjpsr6k3u59pta', '192.168.1.111', 1566630711, ''),
('gea763mb4qajmbc6m10l18ngn838t12v', '192.168.1.107', 1566648577, ''),
('2vuqv6adbb3bpkd3621ljgverkjjjoqb', '192.168.1.111', 1566644813, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('srip3sefdf5d31k1cs8d9s0japkk73kk', '192.168.1.109', 1566652059, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('bq92ng007liga7fisip0taqgesbhvfla', '192.168.1.107', 1566648587, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('1ri2ss70u243llc1dsvghi963a1kmuqk', '192.168.1.111', 1566652518, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('0622c8sas5msqqsak4sasg3sd9vfv7kk', '192.168.1.111', 1566652647, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:4;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";d:105;s:6:\"amount\";d:10500;}'),
('sq2d6unbj0i0et53qpobsjr76r53bmhu', '192.168.1.111', 1566793916, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('3crt0m44fgm5vobhurohovcgc6idak9f', '192.168.1.111', 1566797917, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('rp8aru2r6n7i0gsb2vmtfu97i7ju525b', '192.168.1.109', 1566798780, ''),
('em2au5164sl1pfvpiroqdfvggo3beqg6', '192.168.1.109', 1566826520, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";query_date|s:10:\"2019-08-26\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('en5iac1csiq8kco7p8l6hl5o85d4vckk', '192.168.1.111', 1566815005, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('jmibbehtuta16s9di8r4g65ov76uj3ia', '192.168.1.109', 1566818051, ''),
('p88744t5q180v07k8or31a2mvulv5upn', '192.168.1.138', 1566829309, ''),
('k6mpociif5qh8utmlhs51upjjlils09s', '192.168.1.110', 1566888496, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('9o8a63qi6qbpjo4bbk6m93pqpp00ni9k', '192.168.1.110', 1566891488, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-27\";att_start_date|s:10:\"2019-08-01\";att_end_date|s:10:\"2019-08-27\";'),
('korli6js1jij5mo59onp0884kjatmgfj', '192.168.1.110', 1566888576, ''),
('o5sqdl8n6ub3kl66qu7k1a2u9a61nr8q', '192.168.1.109', 1566898102, ''),
('tppjihpbod2g209er059ivtpc285hs8k', '192.168.1.109', 1566898158, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('k5g0r6flh54p4h2k0cehut0t91j5el4j', '192.168.1.110', 1566911386, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-27\";teacher_id|s:1:\"8\";'),
('arg0p53ira0cvng98jkkm7bn9bkjb9tr', '192.168.1.109', 1566912413, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('8vill2ql3es8qi9i8qpec1s3bt597gqb', '192.168.1.98', 1566911350, 'student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('2d49forf64v8qgo89b20k4ppe4vesa38', '192.168.1.138', 1566911747, 'e_message|s:26:\"Invalid username/password.\";__ci_vars|a:1:{s:9:\"e_message\";s:3:\"old\";}'),
('7tcc9a6korsfh4cm7c058josaf89tlnr', '192.168.1.26', 1566911756, ''),
('tfnd8tih7bik9lrg1mv80jrqdj0qpt38', '192.168.1.26', 1566911938, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-27\";s_message|s:14:\"Image Deleted.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('8qa6ace68fuf7cpvip1b888vdantk3n9', '192.168.1.110', 1566977229, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-28\";class_id|s:1:\"1\";section_id|s:1:\"2\";teacher_id|s:1:\"8\";'),
('kepnk2b54r7j1ri71pis8sh5r2pri1bm', '192.168.1.110', 1566968913, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('2hcmmuvmo4f7t1hamhj42n0v6qv98mn3', '192.168.1.109', 1566975598, ''),
('ade3e1jeban5lo3fvmvmrib54chd7o5d', '192.168.1.109', 1566994001, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";'),
('otkkb6j0d1uho4867teuvl0qqnb1hmq7', '192.168.1.109', 1567061140, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"2\";student_flag|i:1;'),
('aqbl6j86sp6r8ujbga7j8355hcg93t2q', '192.168.1.109', 1566984764, ''),
('i0pf5fbp8t6v6c1jq9kedi55km06163h', '192.168.1.110', 1566994083, 'school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('k5b5p05rs2o4fc8ravn6gscvhs1abgon', '192.168.1.109', 1567056056, ''),
('mh9sbmpiut2gc0i5p0ekbj4ucnc4j6af', '192.168.1.110', 1567057011, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('1std36is1em2qv89j5emq3omflemjkrr', '192.168.1.111', 1567060172, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('517f0dk386m18unjfkb71lc2hgqsmbfu', '192.168.1.111', 1567061942, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('59puf715cjmr1vr1vupheh2210u7le0h', '192.168.1.111', 1567076659, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('merf3lcdqsogso0fc71jg90j8sa00gg3', '192.168.1.111', 1567085037, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('vou2damsequs0mrst8t2088rtbm5sveb', '192.168.1.111', 1567149440, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:43:\"Successfully added fees structure breakups.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('m5uq4b5h81afu2rfk5dofqvjpb380697', '192.168.1.111', 1567160139, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"1\";class_date|s:10:\"2019-08-30\";school_is_logged_in|i:1;parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";student_flag|i:1;'),
('giv9m439766d2b8hdk0o19fprfc0frtu', '192.168.1.111', 1567164279, ''),
('ou8q3e20vqn05822o3ldpoo3v3ic0bgt', '192.168.1.111', 1567171399, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('f3vpfq1q687d191ah9ftqehg3bkp2puk', '192.168.1.109', 1567165427, ''),
('oueeibvo1eisp5f08dutr0fiq58hq0fp', '192.168.1.109', 1567166418, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;query_date|s:10:\"2019-08-30\";teacher_id|s:1:\"8\";'),
('dr29sr54jpp2ma3e7fgpqfokp3h0tvgh', '192.168.1.109', 1567229004, ''),
('2e9jat4su37bvk76tgot5j0d5ve9fiof', '192.168.1.111', 1567239322, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;breakup_id_manual|s:2:\"19\";'),
('4d0vdcja03grga10s0tdqfithisvbtbe', '192.168.1.109', 1567238513, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('adgqt2to96bou26sp3hf7d0pbonmmo54', '192.168.1.111', 1567400394, 'student_id|s:2:\"31\";class_id|s:1:\"4\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('hmrj5uv4h97q1ibnmpuovqits6v08ppb', '192.168.1.111', 1567400411, ''),
('jvlmkhj5oamcahgqmreimkn31mhd0m1q', '192.168.1.111', 1567420279, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-02\";parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"35\";class_id|s:1:\"4\";section_id|s:2:\"10\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";breakup_id_manual|s:2:\"19\";onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:0:\"\";s:5:\"email\";s:0:\"\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:6;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";d:26460;s:6:\"amount\";d:2646000;}'),
('kajjobdrrj5cie1iqnac7lv7skpouors', '192.168.1.109', 1567401908, ''),
('v6885svsjupbcafk1244bpaddfsmrkmo', '192.168.1.109', 1567425618, 'usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('575ji1se2mtgkj3s0frjrq121mo9qjjm', '192.168.1.109', 1567402647, ''),
('f6t1hqpfu9arn7nbc5cepcvtdcc7u338', '192.168.1.92', 1567403754, 'mobile|i:1;'),
('52gphataic9le1r5c9ip8k0666t9rsli', '192.168.1.111', 1567422690, ''),
('5katr8gj8edi4fbt5lcjdrrmq6j1buk6', '192.168.1.111', 1567424027, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"4\";section_id|s:1:\"1\";student_flag|i:1;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:0:\"\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:9;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";d:7560;s:6:\"amount\";d:756000;}'),
('onsgdrmbauqjkdgl3388q0rrumjhb4tg', '192.168.1.109', 1567425592, ''),
('57n7th1amec6i52ka38ha5pkvord16qu', '192.168.1.138', 1567432774, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"22\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('f3bc8mg69g89r2hauhskrl3ffqiunin8', '192.168.1.109', 1567485988, ''),
('kp4k3hi69q6jt4juoa12vqtcl3h535o4', '192.168.1.109', 1567487071, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('0a74k0s5kt7psf2h0p3pfbb3njkh68gv', '192.168.1.111', 1567486559, 'student_id|s:2:\"35\";class_id|s:1:\"4\";section_id|s:2:\"10\";school_id|s:1:\"1\";student_flag|i:1;'),
('cljj6q96mgsu9647kgj4lub6k55v2if0', '192.168.1.111', 1567503678, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:0:\"\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:12;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";d:11760;s:6:\"amount\";d:1176000;}usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('kpjpf4l9n7oqk5densh2lbkbd4hv7ms2', '192.168.1.98', 1567489303, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;breakup_id_manual|s:2:\"24\";student_id|s:1:\"2\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('2mlgnrgstn9hol30taq2j3b4b3o5891f', '192.168.1.98', 1567489074, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;'),
('nqfa65mi3f9f4nhsc5905gl9sq7729be', '192.168.1.90', 1567490681, 'mobile|i:1;parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;'),
('stdtvt5skpb25phr5q9ruqp0r4vds6fk', '192.168.1.133', 1567501590, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('h7172pdkeelrq6ipg69if7e7mq09s2hr', '192.168.1.90', 1567497850, 'mobile|i:1;'),
('r8d95tpi477q2igc2ksbh91n264calbb', '192.168.1.109', 1567504553, 'query_date|s:10:\"2019-09-03\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('c64cu4ghf9ovfg8ud1olkuqv51vm5lba', '192.168.1.111', 1567504959, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;'),
('bfag4rsjckjespbnrus0khh8t1phco4v', '192.168.1.90', 1567497854, 'mobile|i:1;'),
('jkvu26ig7rhl9oh2h8h2j1o9pg238mog', '192.168.1.90', 1567497924, 'mobile|i:1;'),
('7m1a3ck9i6546d80oh6fh68c1ns470gs', '192.168.1.109', 1567504673, 's_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('queq740p7rspamip7lo07s1kdag6k30e', '192.168.1.111', 1567511740, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;'),
('ttr7t2irs1i8p679ug2renl82h172reo', '192.168.1.133', 1567514798, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('sm4ib1lpgjjcvfighp0vjbbeh5jo9mp9', '192.168.1.109', 1567517196, 'usertype|s:1:\"1\";school_id|s:3:\"152\";school_username|s:6:\"163947\";school_email|s:14:\"sujoy@mail.com\";school_is_logged_in|i:1;'),
('16c3i1ajvkv5fb6t1b9rgoioag4qdqi5', '192.168.1.109', 1567517186, 'usertype|s:1:\"2\";school_id|s:3:\"152\";teacher_id|s:3:\"153\";school_username|s:7:\"rajdeep\";school_is_logged_in|i:1;'),
('m4d7cqmmthhh4rln0j28bl4lp59d7qlo', '192.168.1.111', 1567515985, ''),
('rs66ug3i3fla01imk0bub7cu6dlkhp2d', '192.168.1.111', 1567513587, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('rfclo7667ajmj2ieuo3lgs6ds819dvgo', '192.168.1.109', 1567573862, ''),
('dq575krfadj9gifg02n7j7p8keqssv96', '192.168.1.109', 1567575354, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('3pium214rf2jg0bj8t99picn9ttjkeo6', '192.168.1.133', 1567594136, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('v7e726iojvbg9lik212t7sadk1kq4bel', '192.168.1.133', 1567576665, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;s_message|s:28:\"School updated successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('dba31vdhju3rs5lq0lkmbj6sj4kdshub', '192.168.1.109', 1567579068, 'query_date|s:10:\"2019-09-04\";usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('m95bucdod4p59h1ia91p8ubikpdigukr', '192.168.1.111', 1567576005, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('dq9134sa2ha8mf5btpvgfhr22la0v94o', '192.168.1.111', 1567583986, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('q3bnc99l9si03kcpbukh84lau8kevhd8', '192.168.1.138', 1567579554, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('ak2uflomrl2jr87v9rktd5k21gumfajr', '192.168.1.111', 1567595246, ''),
('dsf0fhsa8b402dovoq2ijflikf0gl9oh', '192.168.1.111', 1567600610, 'parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;payment_total_amount|i:7245;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:0:\"\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:3;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"7245\";s:6:\"amount\";i:724500;}admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('cc8pdhqtth9jcmqlldvea58ie64rnpp7', '192.168.1.138', 1567600433, ''),
('qfbm58133fmf8qs5assukvid7sdum4b2', '192.168.1.111', 1567658777, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('4pgsmm079qnvtrelmqm20jj2cj5m3prv', '192.168.1.111', 1567660731, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;'),
('lafoqrtt5ue3b80m07pofqhhuij374fu', '192.168.1.133', 1567659771, ''),
('8dcobfcb5vko83b69it5394m470ru8qd', '192.168.1.133', 1567674519, ''),
('fcui3kjkdcoeb2h5heca6ucge7hiourg', '192.168.1.111', 1567688892, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('ppr6rbj3rqplag5sico9ph0pm5pmf9lf', '192.168.1.133', 1567745215, ''),
('uqjca381efqs7kn2q70dj6j01ffkmmep', '192.168.1.111', 1567752508, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('8r87fq15k8jh4hv88d86so00h44becog', '192.168.1.133', 1567760632, ''),
('86re55ttplg8e247eipkovkhv4ge1g7p', '192.168.1.109', 1567764070, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('08jguasqvdq226nlj7f63h6qha2ha6ln', '192.168.1.109', 1567764443, ''),
('vrb2uifhrk51n12mrkafn70h5pf2qd62', '192.168.1.109', 1567767846, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-06\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;s_message|s:26:\"record successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('sq4o9vk91chjln0hdijjd2l7s5hc9mb2', '192.168.1.111', 1567769517, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('v9ld8c38h7jbhechdeq2nh45jftphhhd', '192.168.1.109', 1567764693, ''),
('d1i2cd0ajlqi4b5on22389ucf4snbgqf', '192.168.1.109', 1567764967, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-06\";s_message|s:27:\"Diary successfully updated.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('r79sp6pf01rv1hnki2hi58lpv11vhre9', '192.168.1.111', 1567769557, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('m2outqmitdjmpjd5cuvq79an8t8b4jci', '192.168.1.111', 1567776125, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;payment_total_amount|i:6720;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:0:\"\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:7;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"6720\";s:6:\"amount\";i:672000;}class_date|s:10:\"2019-09-06\";'),
('eiemcv8lt06nu70srcpfq4tde3krn49c', '192.168.1.133', 1568004005, ''),
('r9h0e1687pp8q3ucbk4q3c2d0kfmff91', '192.168.1.110', 1568089242, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('r65lgs8vpq6s8atn9jui3pp7vkcsqngt', '192.168.1.111', 1568089900, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('m52obmkomnk4koke0oqcpot37jj6r761', '192.168.1.111', 1568114939, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;payment_total_amount|i:6720;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:9;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"6720\";s:6:\"amount\";i:672000;}'),
('tkommi9fhvqmjije2konndp6gunhbvod', '192.168.1.110', 1568094818, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('mjq4nav00lnqo5i041vgj05drd6as997', '192.168.1.110', 1568094829, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('reh2l0pobcnts2ahppc37jr1aruojren', '192.168.1.110', 1568094924, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('685atrfa0c2nj1kc8h6s0qlqngnm1ma4', '192.168.1.110', 1568112613, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('bbvtj4lgoqqk32laocvukofdcl0ttuc2', '192.168.1.110', 1568112131, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('7rnfhldo02mcpfcpu0gr77faj4t7qint', '192.168.1.111', 1568176090, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('gs27jfihg5ai837umvt0lmjoiran7qmh', '192.168.1.111', 1568194912, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('05i8ghpeoj0v402b439tharn8ij4ihq8', '192.168.1.110', 1568187225, ''),
('i4sgrisfk3gbn97peufeddk3r8tfrm6m', '192.168.1.110', 1568187246, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('rsqmpote7mlfiksk8kgc5r0clrrra8ag', '192.168.1.109', 1568188427, ''),
('i0ugimsos5qr2vkp7db40v442trio66v', '192.168.1.109', 1568204808, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('q7bgrg5st16b4k760lv9jqvm3fgnplh6', '192.168.1.111', 1568208055, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;payment_total_amount|i:11655;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:16;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:5:\"11655\";s:6:\"amount\";i:1165500;}'),
('ibitamat4v7fav8h6tkjr113sstiqdnf', '192.168.1.110', 1568206833, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('u7ghvjn5rnjtl1tb6nbq9s98lk5igntm', '192.168.1.98', 1568206971, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('ut9t95dh3ufohokc2b2vaqktvpln0u9v', '192.168.1.109', 1568204545, ''),
('vu4frdlsvb5gq1h51viadm24o7tl24sa', '192.168.1.110', 1568205981, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('23p7fep6bjenid9o382djefru91jt3j7', '192.168.1.138', 1568212819, 's_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('l550mpgtv5ppive36boomfsgg0d227b9', '192.168.1.133', 1568262626, ''),
('qor4gbo6jql66aefmndivoprok6a4g6c', '192.168.1.111', 1568264658, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('d5fs3jgpe7avd5o477f0pkmu8m7u0g6k', '192.168.1.111', 1568264876, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;payment_total_amount|i:2205;'),
('h43j53utuu051ruqi1jhs8nfkafanvog', '192.168.1.254', 1568281727, 'mobile|i:1;'),
('jdm2t3dskjjn84itcv47nar6f7dqqpau', '192.168.1.65', 1568290596, 'mobile|i:1;parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;payment_total_amount|i:1050;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Biplob Mudi\";s:5:\"email\";s:16:\"biplob@ablion.in\";s:5:\"phone\";s:10:\"9749552668\";s:10:\"payment_id\";i:20;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"1050\";s:6:\"amount\";i:105000;}'),
('rct6jvtqnqi8432aea147vuguef4en5b', '192.168.1.138', 1568291781, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;teacher_id|s:1:\"8\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('b8f3rq0l9rj27p63r0djbqjtae3hup8n', '192.168.1.133', 1568282149, ''),
('son70h6rp6g8r0ach06b0049mhtinc9k', '192.168.1.111', 1568284943, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('h9mc5u1ljj6qhl5rchb5qbeim7vtt6q6', '192.168.1.109', 1568286814, ''),
('oljj6rk4dc7v30rjbfon3rttlmdhtrqh', '192.168.1.109', 1568287144, 's_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('rg1bpok537ums0g17mnfsnlut3h28gcg', '192.168.1.111', 1568289006, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('26ij42fvldkj7ocvolndbj1q9vrsqrqo', '192.168.1.111', 1568289019, ''),
('vudsknsqah34l91macpptomj969mvc7c', '192.168.1.111', 1568289161, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;'),
('841iuk62urnh5f5v7uhqigde9b7deop5', '192.168.1.110', 1568291788, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('tn78ia1b9jo46ep964bnlnhba821i2fs', '192.168.1.111', 1568290805, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('6j91j43grflm9oi0ubpan8291drect73', '192.168.1.111', 1568293482, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;'),
('iaupgfbpl1rjc6ft7j442meb550chafl', '192.168.1.109', 1568293356, ''),
('be02ldla4g8tk6kju3vml4al4602jjsk', '192.168.1.109', 1568293391, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('ukb5sj4757q4nkvbb8ck11e9js0g5sat', '192.168.1.111', 1568294568, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('s307niuljii9mpsiou8g79ldb21d4576', '192.168.1.111', 1568350664, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('6e2n89vsa23bi82tebeu8ivdrct9nhve', '192.168.1.133', 1568350640, ''),
('2ltp2io8a871d3gpgso3bqb1682lprm1', '192.168.1.111', 1568353293, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('khmec008d1v9l06cvlaslv8vpa7qe9er', '192.168.1.109', 1568359526, ''),
('0ana4jmk07m18v72vsrhcinol2326odf', '192.168.1.109', 1568380160, 'parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;usertype|s:1:\"1\";school_id|i:155;school_username|s:6:\"605785\";school_email|s:15:\"bibhas@mail.com\";school_is_logged_in|i:1;'),
('78r2ueda72772qf5sqfsdlt9oet62bej', '192.168.1.133', 1568439299, ''),
('s0la82r1v07qs6q1gniefte94j7tvcch', '192.168.1.133', 1568377550, ''),
('0odrf1b606hojhpumo0l5q4be5p2757s', '192.168.1.111', 1568466955, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;payment_total_amount|i:1470;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:25;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"1470\";s:6:\"amount\";i:147000;}'),
('lagjddpssj1d7jnn1hg04ntlfl3688j3', '192.168.1.27', 1568439387, 'mobile|i:1;'),
('p2trjv3c4pgqm55ob2n81sp1h969qk4k', '192.168.1.27', 1568439397, 'mobile|i:1;'),
('0r0d3ute0r0c8munlek46rq843ma2tva', '192.168.1.133', 1568439532, 'teacher_id|s:2:\"11\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-14\";'),
('1dscn3kgmvk4m855fnf3hcg139epqmfm', '192.168.1.109', 1568439515, ''),
('avfv8k8f9r4361ftr2rvdffeicp35pq2', '192.168.1.109', 1568439521, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('frfscl1t34aerru54chusb89pvbj3qie', '192.168.1.109', 1568452207, 'query_date|s:10:\"2019-09-14\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;s_message|s:26:\"record successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('klg0vv06b3i8esbvkdh0f3l26g5rg6nh', '192.168.1.109', 1568446832, ''),
('rc6qumd4vv9sam6ghvvgqgi1ac5tqa06', '192.168.1.109', 1568460799, ''),
('n841epjvr5m6dbhc1bjr0jg5n4htlj9h', '192.168.1.109', 1568466364, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-14\";usertype|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";'),
('u99s2i2ig3jaqa5eh29mepqj8ofpahah', '192.168.1.111', 1568609443, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('8t1j07sijtlkti5i2n2al0pan0qpg2il', '192.168.1.111', 1568640426, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"35\";class_id|s:1:\"4\";section_id|s:2:\"10\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fpr_session|a:2:{s:10:\"start_date\";s:10:\"16-09-2019\";s:8:\"end_date\";s:10:\"16-09-2019\";}fpr_start_date|s:10:\"16-09-2019\";fpr_end_date|s:10:\"16-09-2019\";payment_total_amount|i:11760;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:28;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:5:\"11760\";s:6:\"amount\";i:1176000;}'),
('u3tteis1r3g8t7ul1uodk937lj5ejrjj', '192.168.1.138', 1568621921, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fpr_start_date|s:10:\"11-09-2019\";fpr_end_date|s:10:\"16-09-2019\";fpr_class_id|s:0:\"\";fpr_section_id|s:0:\"\";fpr_roll_no|s:0:\"\";fpr_name|s:0:\"\";'),
('hqabqct1f6jkfb4ujlmipeg9f2n114hf', '192.168.1.109', 1568637046, ''),
('ql6jmf1vdjmmtsloiud2lqvjri6t0u9i', '192.168.1.109', 1568637284, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-16\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";'),
('3sf1dfia1o2o0qle7k0hctil3bbaalv1', '192.168.1.111', 1568695614, 'student_id|s:2:\"35\";class_id|s:1:\"4\";section_id|s:2:\"10\";school_id|s:1:\"1\";student_flag|i:1;'),
('k1n8jl6i1gcpgn6v7rof3ft15j9js1cf', '192.168.1.111', 1568696378, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;');
INSERT INTO `tbl_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('53hdd7dnu19tu4kjq2enb0jper354398', '192.168.1.111', 1568701299, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fdr_class_id|s:1:\"6\";fdr_section_id|s:2:\"13\";fdr_name|s:0:\"\";'),
('lomf71em1kflgf344v3advbn4k2efmv7', '192.168.1.133', 1568699675, ''),
('0030q1fq3897urmsdio8vcetr5c92g7k', '192.168.1.111', 1568706553, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('blqtccrcih1vfr4huh70rn4e4t11gj1o', '192.168.1.138', 1568728856, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;fpr_start_date|s:10:\"17-09-2019\";fpr_end_date|s:10:\"17-09-2019\";'),
('if8tgl4knbt0ua7195uerlot253umml7', '192.168.1.111', 1568810926, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fpr_start_date|s:10:\"18-09-2019\";fpr_end_date|s:10:\"18-09-2019\";receiveFeesBreakupId|s:2:\"22\";'),
('l748av3emo90halsk4hu6g0mdkqomeav', '192.168.1.109', 1568806663, ''),
('mv6qv9ff1ncu5a9hidnmai73dlsb849e', '192.168.1.109', 1568806731, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('8f23288oogbi0t8tog41f70sugdqgcks', '192.168.1.109', 1568806778, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-18\";'),
('v3bhnpf84v90qphods30680ksnk6brm9', '192.168.1.133', 1568808222, ''),
('r1p195fadtkcfneimqf98tk5r0rke4vf', '192.168.1.133', 1568869914, ''),
('ho9d2o5ia4tkf521ut8tdd3si4tl0b1l', '192.168.1.133', 1568959460, ''),
('tcqcbcb0p71ndc3chevc2l7d6l7veh1u', '192.168.1.109', 1569234313, ''),
('n2k4r2jad8tak1hjmdt6p9m6d3bigrpm', '192.168.1.109', 1569243983, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;s_message|s:28:\"Record updated successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('fi6i15c7nkm1sb0af2gi6gfnpdugfs0h', '192.168.1.111', 1569245215, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:2:\"35\";'),
('3d5c1p765cbjc0fdc7m358kh3a9etojh', '192.168.1.111', 1569307352, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:2:\"35\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('7a0d07hsr4qd44tmk0v5deo48jcu1689', '192.168.1.110', 1569306766, ''),
('7b9di4qlb2218df5kfvha153l996bfab', '192.168.1.110', 1569306776, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('320vubfc15uqt6d3o2lf9rvd9rlv2vfs', '192.168.1.110', 1569306829, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('2jsn0i0d224epj0tek4u7ci86plstisd', '192.168.1.110', 1569306881, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('43kvik679l1ku95ln383fcu7vudcp77p', '192.168.1.110', 1569306932, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-24\";'),
('d1n2ieuucgpoqdu521sriqc46pm9nhos', '192.168.1.109', 1569310938, ''),
('vnobpu79hman7olipluqf772usk4bvo5', '192.168.1.109', 1569311258, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-24\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('03aeo2ndku5827j6cn0399k9hs8c3sk3', '192.168.1.109', 1569310940, ''),
('r5d847fdrcm6qcifi6oelqfn4m8dudit', '192.168.1.109', 1569310940, ''),
('2aavfd3gbu8qj915j102osca6v2gobh4', '192.168.1.109', 1569310940, ''),
('j8ih06e1rbfnso36mark7mufh75rs7sb', '192.168.1.111', 1569311114, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:2:\"26\";'),
('g1p2d1k3pho29ct40mrl426ob8a2gcfa', '192.168.1.109', 1569324229, ''),
('isihtfi905d79u3n2cv150tpk99slrs1', '192.168.1.109', 1569324803, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('m213c6j4khhdf5s6eo4j1j5fstm4s5oc', '192.168.1.111', 1569387721, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_email|s:16:\"biplob@ablion.in\";fdr_class_id|s:1:\"6\";fdr_section_id|s:0:\"\";fdr_name|s:0:\"\";'),
('bv6oap69t9g6mqoelre10fu0dh0ec5v4', '192.168.1.109', 1569387800, ''),
('g8a97r6lr27cfj92193u8l57m9r9ejj7', '192.168.1.109', 1569389349, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('62cpppjhr49iilu1g5072a4dd16etio7', '192.168.1.109', 1569475720, ''),
('pdc5tdf84efm6b6830ap6nnficl3070b', '192.168.1.109', 1569475856, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('89iaf9q66lheju662l9c6bi3tv8bj09n', '192.168.1.109', 1569500970, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-26\";usertype|s:1:\"2\";school_username|s:6:\"biplob\";'),
('u6j6bh8lfmm69iu0p217h0a898vvjpii', '192.168.1.109', 1569476537, ''),
('essaftn27rgl5kca4olov5dkse11r439', '192.168.1.109', 1569476540, ''),
('ehcn2rrtgcckbd8hjckir4nreqkaa89g', '192.168.1.109', 1569476540, ''),
('7b0mukm8l9s4us5gf9llhc42ofog9m6d', '192.168.1.109', 1569476540, ''),
('d1i0knog1th5hs44fu24tg266egcmmnd', '192.168.1.109', 1569477078, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('5864g3foj2761365up8bak8720jtnfdf', '192.168.1.109', 1569488017, ''),
('pvj85uml2bcivud9hc08231ur9au99pb', '192.168.1.109', 1569477201, ''),
('jgh9j737s1o51gjq046vpvoodc3ea68h', '192.168.1.109', 1569477201, ''),
('ec419qbf36cjl4ljj565oh1c92og4p72', '192.168.1.109', 1569477201, ''),
('tin5b3g5enifb541bptqjklh4oc0l5gp', '192.168.1.109', 1569477222, ''),
('vfoofr9c605tn8glkb7kgf8krqq9b4rd', '192.168.1.109', 1569477224, ''),
('l2gbgm3hccejsil6qj8k5jm22v6uqcl3', '192.168.1.109', 1569477224, ''),
('ugt7qtckhm735oq1div61mh147d1uk9b', '192.168.1.109', 1569477224, ''),
('eu3vo921v2ftrbci6q8oj56hsbo52blo', '192.168.1.109', 1569477971, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('8se1ljr2kp0rrlhsd6mgcsri1r3ui9bl', '192.168.1.111', 1569478558, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('m2qmljg9ucs21ec4nvre8ibem9dgap7o', '192.168.1.111', 1569481490, ''),
('b2hhvc3q1q2n1lcivabd4tqar2osa27a', '192.168.1.111', 1569482134, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('mr7m54ahgtttmo9iq18191uv9qu98h61', '192.168.1.111', 1569482226, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:2:\"41\";'),
('hvnu5dd3cqvog7i2elmrqlc93g0dqrej', '192.168.1.109', 1569488019, ''),
('atqcnmoi2t068s9372ruprjjjdv14hsa', '192.168.1.109', 1569488019, ''),
('7klft5k09qggv7rub61fslukhmb3gcm3', '192.168.1.109', 1569488019, ''),
('gbmdr3nfks8opngcd792e9c08uk6k9ov', '192.168.1.109', 1569562773, ''),
('j2tiq9i60q74ggpp4rfm92omb2kokvl7', '192.168.1.109', 1569578820, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-27\";'),
('n45qogi7mgtgu8irn9ascclbnlk0ifnh', '192.168.1.109', 1569562775, ''),
('eb0eqou9ronh2uuu9i3mfmu3819qpkip', '192.168.1.109', 1569562775, ''),
('ogjkkckkn94a0bfub4868vsjrc0bgtu7', '192.168.1.109', 1569562775, ''),
('gvvbqp5bga10hdt7tu04273rst215d1r', '192.168.1.109', 1569563372, ''),
('1v9pa73cvjjj6457af4mubspv20iphf7', '192.168.1.109', 1569563374, ''),
('npk70ineircsnhi3gul3o8jc0rmm9lj0', '192.168.1.109', 1569563374, ''),
('tgntk6j8cmh83sjng5mehi4i8fdeoeoe', '192.168.1.109', 1569563374, ''),
('b00nfkb52mslcl3mim4uadbt6grhvs78', '192.168.1.109', 1569563763, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('efckf5uorpgjrij229q9ed35prcuncsm', '192.168.1.109', 1569563784, ''),
('o5n1lgbt487693tlabik44luqi8rvl59', '192.168.1.109', 1569563785, ''),
('k2n5cq9kksvv98ufkhk96kl6mb0ubnea', '192.168.1.109', 1569563785, ''),
('oq4r2cn0mn27i9m62qjc48brl3lf1jmg', '192.168.1.109', 1569563785, ''),
('bv79c4dno5jsekng5g5imlp0cc96jor5', '192.168.1.109', 1569570872, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-09-27\";'),
('quadn76m3c99ulp213vmhe29d5b6qbs3', '192.168.1.109', 1569578796, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('kte8i2r36dli7817eul6b45t36qi5e9r', '192.168.1.109', 1569578851, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-27\";'),
('i3v1qd83k2ke1kohi1v4nfi1q8067re3', '192.168.1.109', 1569578899, ''),
('gfs1o0m347fnajeooj2aqeklf3avlc8c', '192.168.1.109', 1569578900, ''),
('22n48p9sa7pvqhi1kcjt4b0ml237ddal', '192.168.1.109', 1569578900, ''),
('re6ob3cfi76v61vcr1jdk5pdf9rji6hb', '192.168.1.109', 1569578900, ''),
('7niunfdqtdes9mn3goo9o5m7b4ojj1bq', '192.168.1.109', 1569581397, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('drppud1e888b4ttq7nib8pfilqp9ig7l', '192.168.1.110', 1569580894, ''),
('1ds82b69rn0nds7fe1ju6lrot49nk0r0', '192.168.1.110', 1569584103, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-27\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('cmdf60iiptfg8ulps363akdvs8715os7', '192.168.1.109', 1569589840, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('0td34scm16468dvosgjt5kblv31t7hvs', '192.168.1.109', 1569589928, ''),
('us0dg7u9h8q52gbd5sgloltvulgskjgi', '192.168.1.109', 1569589947, ''),
('1mjcr64g5o6m61jv1k5fnr37vo7644jp', '192.168.1.109', 1569589947, ''),
('nl3jio1rf2v7as74bcakgqc7djll55l9', '192.168.1.109', 1569589947, ''),
('bvqufr56357anqm398rdkvl2lsm6blvg', '192.168.1.109', 1569589958, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('tindhpbc2r12f17o03nprk8s3si4c99d', '192.168.1.109', 1569650101, ''),
('r7oaapr480vc3sf7vga1j9u0j2pgfb4r', '192.168.1.109', 1569652760, 'usertype|s:1:\"2\";school_id|s:1:\"1\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-28\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_email|s:19:\"aparajita@ablion.in\";student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('qm5j4gaac8bkrn1t70bmjg8fl1b15grq', '192.168.1.109', 1569645821, ''),
('1b7ituhcosadcrpc47i9cf75uke1l49r', '192.168.1.109', 1569645821, ''),
('rpl8aip38u72pj88q4go4qkqqmbgu6ge', '192.168.1.109', 1569645821, ''),
('mcnarljh1t9nudj1grpladuplgf3hakn', '192.168.1.109', 1569647998, ''),
('ijq3actfm1205bjgamrq6r2ur9q7vpkj', '192.168.1.109', 1569648000, ''),
('96phbcn58bvnalr0qutf9pbng7u8ess0', '192.168.1.109', 1569648000, ''),
('vobve2efaiivv5hfpf18guk2g764hder', '192.168.1.109', 1569648000, ''),
('iomh24ptmpnoms1hhqiqqnebo70884fb', '192.168.1.109', 1569652639, 'school_id|s:2:\"22\";class_id|s:1:\"7\";section_id|s:2:\"29\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:2:\"36\";student_flag|i:1;'),
('6bfmbgg67m46dt82ggsq9b5ef3h2r7oi', '192.168.1.109', 1569652681, ''),
('52ldsfh1rnduj66qnvob0bjvg8lmtgpq', '192.168.1.109', 1569652682, ''),
('8m0mjmpuj97jsdu8o68051mtus76gq07', '192.168.1.109', 1569652682, ''),
('de79uabduitq69fpgslmcg8csefheckv', '192.168.1.109', 1569652682, ''),
('sqqmed70cr85rcpq6t2751vjikpc40f1', '192.168.1.109', 1569654200, 'parent_id|s:2:\"15\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"36\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;class_date|s:10:\"2019-09-28\";'),
('slueeea32jnmtc36bid06d1flo7c7mek', '192.168.1.109', 1569654192, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-28\";att_start_date|s:10:\"2019-09-01\";att_end_date|s:10:\"2019-09-28\";admin_id|s:1:\"2\";admin_username|s:9:\"aparajita\";admin_email|s:19:\"aparajita@ablion.in\";admin_is_logged_in|i:1;school_email|s:19:\"aparajita@ablion.in\";teacher_id|s:2:\"30\";s_message|s:28:\"Note successfully published.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('tvn5cmd7eipobgk2jj0o0m4i9trve11t', '192.168.1.109', 1569668959, ''),
('tnsq9jrps197r90mkir24ntvr2ec7uga', '192.168.1.109', 1569675795, 'school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-28\";user_id|s:2:\"15\";'),
('p42sfgvf75ts3gu5vj9hktliu9krbvk7', '192.168.1.110', 1569675700, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('dvtn2ah3ifeiqrcd59570mnv7mrg5smv', '192.168.1.110', 1569674745, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('9fh50t1k2q17kutc6ojrn0fhuai1f9md', '192.168.1.109', 1569822309, ''),
('rchp1oq1jqlpsnqn85su9odbtavi7f5t', '192.168.1.109', 1569830225, 'user_id|s:2:\"15\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('1o68vuif9bj0loir74cs0bsol6acq16j', '192.168.1.109', 1569838655, ''),
('5oq6cc94vo7u62b1euh4depd3u8d6624', '192.168.1.109', 1569840525, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-09-30\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;class_date|s:10:\"2019-09-30\";'),
('s46fpc0r8n974rprbhpvoofd4s00n834', '192.168.1.109', 1569838657, ''),
('6n7b335ftphcuo6nu5mnabn70sn2q6io', '192.168.1.109', 1569838657, ''),
('84obg4eslbgbl455bt8cljvbl2db6gbt', '192.168.1.109', 1569838657, ''),
('5ph38v7fn4o5n3kq8i5e9r8ida9ld74k', '192.168.1.98', 1569848703, ''),
('fkeu30lkioka1d8uk12jq8qp2evemdj3', '192.168.1.98', 1569848803, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('e1fugtoti33vplsm1m9qmefrrpr7trk5', '192.168.1.98', 1569905485, ''),
('kghs6vhrk62jtb75dtob7b5q0u9pf2b1', '192.168.1.98', 1569905820, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('snokp6fdtern0gbd77iptv5225sec058', '192.168.1.109', 1569907931, ''),
('o2p6a7f12jtdb7a7t0dkt7dcm2758ube', '192.168.1.109', 1569907943, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('brslpc1spbe2l5fi5hsouhv76inqlfmv', '192.168.1.109', 1569907674, ''),
('fs5p7ltfnjj2g1hm599l1er0hkh658uo', '192.168.1.109', 1569907674, ''),
('017ge5fffpr8ju0t6n00b1s1e0voc1l8', '192.168.1.109', 1569907674, ''),
('tijodq8t1nhta7lofpat2sh7s8l36vi4', '192.168.1.109', 1569907721, ''),
('qa3qkfnoo03cob32q5v4dftd1lm4jd1o', '192.168.1.109', 1569907721, ''),
('1v8v6d8u3lbb9oa86tbee2kju4kteotl', '192.168.1.109', 1569907721, ''),
('pd63ouudj0fhbgt0kmntk5t2soen5pna', '192.168.1.109', 1569907925, ''),
('sc1v1rurrcu4ei22ptmr4m3v7d0umkqn', '192.168.1.109', 1569907925, ''),
('4bhp1c84o0cc12eofjcfciqn1r6v8erp', '192.168.1.109', 1569907925, ''),
('fg6mmiug97ji5todt9qmkddgbd0gnaov', '192.168.1.109', 1569907931, ''),
('3fqfucebakehi74npbis2foa0l0p5qmg', '192.168.1.109', 1569907931, ''),
('s5dfpcg1gf57phsae5thsau0tu1qj65t', '192.168.1.109', 1569907931, ''),
('beftvgsvrcnm4517j9c61i5tfs8e6m5k', '192.168.1.109', 1569907969, ''),
('qosj9of84igvmfgbvd22vjlmqd850kbf', '192.168.1.109', 1569907969, ''),
('klnuhoh6cccadv64ha9cm0i1d9hvm639', '192.168.1.109', 1569907969, ''),
('p692maq8vkglsgslpjhnfm7gjrbd5tc3', '192.168.1.109', 1569907969, ''),
('6evr3d89ore1a9nuvacbhg8t3pesuopn', '192.168.1.109', 1569913624, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-10-01\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;maintainance_message|s:64:\"App is under maintenance mode. Please visit again after 5 hours.\";__ci_vars|a:1:{s:20:\"maintainance_message\";s:3:\"new\";}'),
('s73iq8bq3b6svvcten0kpje5lii14net', '192.168.1.27', 1569913586, 'mobile|i:1;maintainance_message|s:64:\"App is under maintenance mode. Please visit again after 5 hours.\";__ci_vars|a:1:{s:20:\"maintainance_message\";s:3:\"new\";}'),
('dgn9kk9808ljml1lic80tjmalon02ikd', '192.168.1.109', 1569908489, ''),
('n98sqd2icoh86d1fqv0r189t3tilrorb', '192.168.1.109', 1569928359, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:27:\"Class deleted successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('k1mm937qkqt7135tsaljngtvgso7ud43', '192.168.1.109', 1569997403, ''),
('j7nfkbd592qann5ca9c872pcp8oo5ecs', '192.168.1.109', 1570022360, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('emavpld1idgd1k4cgkptc5483qmbas17', '192.168.1.109', 1570019147, ''),
('skeh3ko6cs7hfrkojnfjngbqvcu71um1', '192.168.1.109', 1570078645, ''),
('5dtuba8n4c88hrtrvdl9tl89ifdvs6h8', '192.168.1.109', 1570086390, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;s_message|s:27:\"Students successfully moved\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('83cevr84ru218a78cen3c7l01m3h91s9', '192.168.1.109', 1570102113, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;query_date|s:10:\"2019-10-03\";'),
('djaksra35qo1g3r42cffsaraqajl6qcb', '192.168.1.109', 1570100596, ''),
('04vma898kckvmdstvgminmmf88n2l2he', '192.168.1.109', 1570100598, ''),
('lvd8rdjaugfc9fe0frrc45i79v0ltvlq', '192.168.1.109', 1570100598, ''),
('qc1tvjvkr6249272f9f4kb32rfe0am9s', '192.168.1.109', 1570100598, ''),
('obm607uih69butahn4otqploa13apr48', '192.168.1.109', 1570167957, ''),
('nsih8t75esi64a862d7okul77gf5jv99', '192.168.1.109', 1570167959, ''),
('hqgqmj9h3rsmami586o1can6k8u1soop', '192.168.1.109', 1570167959, ''),
('n5cbs50pkbd4003fnamcgi9g6dg1ga78', '192.168.1.109', 1570167959, ''),
('5gf7cfl9nb85hu23hj7t1idq4ver9hsl', '192.168.1.109', 1570171550, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-10-04\";'),
('spjrk3pft4gh2f62rcr74j9lv8p8dljv', '192.168.1.96', 1570173614, 'mobile|i:1;'),
('3pdc76jgtfae20suqenteig819p1a4hr', '192.168.1.109', 1570190610, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;'),
('stlt869oaotirqqg0vpeppotqr52unph', '192.168.1.109', 1570189693, ''),
('n43ga17p3eqhmoo5n3hgccir7eruvb0c', '192.168.1.109', 1570189458, ''),
('sfhn9bs89d57j1j0hg4pus3u27pq73kr', '192.168.1.109', 1570189478, 's_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('00p9flvcrtj6rmu22uc7eeb5m730f1e4', '192.168.1.109', 1570599359, ''),
('p3jbus4j768nik3c6tl6cmaac0o3c33a', '192.168.1.109', 1570622145, 'query_date|s:10:\"2019-10-09\";att_start_date|s:10:\"2019-10-01\";att_end_date|s:10:\"2019-10-09\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"1\";school_id|s:3:\"157\";school_username|s:6:\"729865\";school_email|s:12:\"sem@mail.com\";school_is_logged_in|i:1;'),
('ropqv6l1btiko14uo36ogflv3f81l4dd', '192.168.1.109', 1570611850, 'parent_id|s:3:\"159\";school_id|s:3:\"157\";parent_is_logged_in|i:1;student_id|s:2:\"37\";class_id|s:2:\"25\";section_id|s:2:\"58\";student_flag|i:1;class_date|s:10:\"2019-10-09\";'),
('tgnsmuh4btbkf96qiqp53s34s96qb663', '192.168.1.109', 1570612476, ''),
('g0sk3uv0hup6qf0nrpkr1gi1fods7rqe', '192.168.1.109', 1570619068, ''),
('oo41vbmh4qk9u2a3gkbda7e0m75htj1o', '192.168.1.109', 1570627422, 'school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-10-09\";teacher_id|s:1:\"8\";'),
('tjdopreo63hb41r3othb8u83hloio9kq', '192.168.1.109', 1570682944, ''),
('c08gtsqgvr1tr2cqpcc81u4167puibkp', '192.168.1.109', 1570705132, 'query_date|s:10:\"2019-10-10\";'),
('tscos17iime1ogkvllo5tvi5ldq812gi', '192.168.1.109', 1570770270, ''),
('g4qsh76n7n9n4d5ishahk9kq2r7bs0ae', '192.168.1.109', 1570713165, 'teacher_id|s:1:\"8\";school_id|s:1:\"1\";school_is_logged_in|i:1;query_date|s:10:\"2019-10-10\";s_message|s:24:\"Note successfully saved.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('1vp2m9c74k43rqu0ltgrsrtbrr137c15', '192.168.1.109', 1570798568, 'usertype|s:1:\"1\";school_id|s:3:\"165\";school_username|s:5:\"03900\";school_email|s:18:\"school366@mail.com\";school_is_logged_in|i:1;'),
('nbr4ssr55geh4o9bbc0bd594ktnpgde2', '192.168.1.110', 1570790900, ''),
('e7gtl3nit6q06shp6op4ev1c12apubk5', '192.168.1.110', 1570799299, ''),
('9lkpb0n97cn19oh62jco9gv9fdjr570a', '192.168.1.110', 1570791186, ''),
('jlgh083hvjtr2t8hmvlc59gpqjtain64', '192.168.1.98', 1570796399, ''),
('84sbknd560iu24h4anpavaannt0m9dcs', '192.168.1.98', 1570797241, 'usertype|s:1:\"1\";school_id|s:3:\"165\";school_username|s:5:\"03900\";school_email|s:18:\"school366@mail.com\";school_is_logged_in|i:1;'),
('0lt0n15ph1rp2iudv8lq2l2jhg03kclo', '192.168.1.109', 1570800617, 'usertype|s:1:\"1\";school_id|s:3:\"165\";school_username|s:5:\"03900\";school_email|s:18:\"school366@mail.com\";school_is_logged_in|i:1;'),
('kl7aid7bj7266ogkvash773lh93hj9r1', '192.168.1.110', 1570857432, ''),
('krtd56c4asn1e8otkqj0r28mfhen3jt1', '192.168.1.111', 1570856794, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('4hbaun80s682dl87ob6eo27amtamo264', '192.168.1.111', 1570856801, ''),
('p701ap19ski8hnevbgnqt3s2nn3mtiu1', '192.168.1.111', 1570856825, 'student_id|s:2:\"31\";class_id|s:1:\"6\";section_id|s:2:\"13\";school_id|s:1:\"1\";student_flag|i:1;'),
('4msm6r7rr12of0m4j2d55e0cth9sn75o', '192.168.1.111', 1570882713, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;payment_total_amount|i:4244;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:4;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"4244\";s:6:\"amount\";i:424400;}'),
('sln55c25h0l277iju1eidj8h2j8i0b0k', '192.168.1.109', 1570865429, ''),
('em1kvvjpi2v6g8mchoab0oqt5v139ba6', '192.168.1.109', 1570885650, 'query_date|s:10:\"2019-10-12\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"1\";school_id|s:3:\"164\";school_username|s:6:\"779361\";school_email|s:13:\"john@mail.com\";school_is_logged_in|i:1;'),
('8t44en1decc2odvujigfc1rml3216eq2', '192.168.1.138', 1570875012, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('3s191mlgg18q609sih18shqa34tgptk5', '192.168.1.109', 1570862364, ''),
('5ajckch0p812h3l496u7q2cbbdbb1jqo', '192.168.1.109', 1570862364, ''),
('01qbojclodkgr1tbvudcsou23mmar1u9', '192.168.1.109', 1570862364, ''),
('k6d2ekossrnsrebqilesdqaufdccm0d0', '192.168.1.109', 1570877148, ''),
('ji4imj7s90rhe64qej525qehdrpgghtt', '192.168.1.110', 1570872672, ''),
('ei2b74cth7ncsttsggp9iqg9muk9ruqc', '192.168.1.110', 1570875928, ''),
('3meigidggq7deu51kpv2qkcs0t61ug4r', '192.168.1.110', 1570884248, ''),
('0jb384igf1sf9mu3vrqv2fejl5ve8eda', '192.168.1.110', 1570884181, ''),
('imj3tf27p079kkgdchdkjnvvjbnrkmki', '192.168.1.109', 1570882556, 'usertype|s:1:\"1\";school_id|s:3:\"167\";school_username|s:5:\"72196\";school_email|s:18:\"school368@mail.com\";school_is_logged_in|i:1;'),
('ikgbd8etev5eeqh3fvvbu1k0oso324rl', '192.168.1.110', 1571039551, ''),
('9o3r5fgnpra9gffvug6ljsq3qgunq9jv', '192.168.1.110', 1571039946, ''),
('nqr2ekjlbd97hbf8boc7tr46ffo3fude', '192.168.1.111', 1571030018, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('hu3omkbn6lpuv3lrkarh1iio4vgbkhfe', '192.168.1.111', 1571045666, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"1\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;payment_total_amount|i:3151;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:1;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:5:\"16971\";s:6:\"amount\";i:1697100;}receiveFeesBreakupId|s:1:\"1\";fpr_start_date|s:10:\"14-10-2019\";fpr_end_date|s:10:\"14-10-2019\";parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";student_flag|i:1;'),
('9270giljpjnpf0hmqgq39jjf0hqh1mf5', '192.168.1.110', 1571048493, ''),
('i6m5pesdr9rfc0r9laa083gaha119v6r', '192.168.1.138', 1571039409, ''),
('dhn95lrk7k6879a0llr7ug20m7in7b1a', '192.168.1.98', 1571039510, ''),
('ujfe9m71jcm0smkruehb75bqsp0qp479', '192.168.1.110', 1571048493, ''),
('bu20bju2m70vat99an419t6fvnkbi3rb', '192.168.1.110', 1571048493, ''),
('pd1qdds395ujabghofhecgr33f49j95g', '192.168.1.110', 1571048493, ''),
('3id0cheu38u5vvp320caqm758isuvma9', '192.168.1.110', 1571048493, ''),
('pcanlo1ui934cl0se7lhljrrfhbcg9ud', '192.168.1.110', 1571048493, ''),
('dt83togjr9r9880pj1c5oss7n50mmj99', '192.168.1.138', 1571061598, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fpr_start_date|s:10:\"14-10-2019\";fpr_end_date|s:10:\"14-10-2019\";'),
('4tjrqcpvcgdepiunas9qe8re27hpsf8j', '192.168.1.111', 1571058236, 'school_id|s:1:\"1\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";s_message|s:27:\"Session added successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('kd63a7hk58r849rslo2gdravhei6j0mj', '192.168.1.109', 1571122118, ''),
('sac6dbtabgd8lj8hi6n7vop0tp7pa1ah', '192.168.1.109', 1571123384, 'usertype|s:1:\"1\";school_id|s:1:\"1\";school_username|s:5:\"95340\";school_email|s:16:\"biplob@ablion.in\";school_is_logged_in|i:1;'),
('raqdhfu7v0rtn54cbjj2bssve2qe5tlu', '192.168.1.33', 1571060834, 'mobile|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('oi7npmjv25gftvgr06ujthbhpv5689ah', '192.168.1.138', 1571135148, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"22\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;usertype|s:1:\"1\";'),
('pf41tt5qu3suj9us5857kqhb2i84m8dv', '192.168.1.109', 1571117921, 'usertype|s:1:\"1\";school_id|s:3:\"167\";school_username|s:5:\"72196\";school_email|s:18:\"school368@mail.com\";school_is_logged_in|i:1;'),
('kf9bl3ppopep9j5564n6nheoa3o8ukmv', '192.168.1.29', 1571135642, 'mobile|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('h25qibt49fio5gmhjh9tj9a1nha88pu0', '192.168.1.29', 1571136436, 'mobile|i:1;school_id|s:2:\"22\";school_is_logged_in|i:1;query_date|s:10:\"2019-10-15\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";student_flag|i:1;payment_total_amount|i:16275;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:7:\"A desai\";s:5:\"email\";s:14:\"desai@mail.com\";s:5:\"phone\";s:10:\"7063605278\";s:10:\"payment_id\";i:4;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:5:\"16275\";s:6:\"amount\";i:1627500;}'),
('0kkmtsdhd0mkqn5rlorlu8mcvggkubqk', '192.168.1.109', 1571232208, ''),
('23scdc0b16v86ijcaqg34ptvodogs6h0', '192.168.1.109', 1571208146, 's_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('832kt5sr7338o1ph7t09sg59bm90stc5', '192.168.1.38', 1571223292, 'mobile|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('ah7gfvpioajgf2n3h9b2umoepe138s8f', '192.168.1.111', 1571223635, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:1:\"3\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;s_message|s:28:\"School updated successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('6l8fb20f2934hpch03j4hjeajmrq0eds', '192.168.1.111', 1571223762, 'school_id|s:1:\"1\";class_id|s:2:\"27\";section_id|s:2:\"60\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"40\";student_flag|i:1;'),
('ruvil7324v949hceksueb4hj3552rtqa', '192.168.1.111', 1571231297, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('3ar5abj10vr3oijo9lg24g91o5a1ordf', '192.168.1.109', 1571294833, ''),
('t0800h83qucdpjd72la2r7omafs80v70', '192.168.1.110', 1571288604, ''),
('v03arl0i8sg0jvcktvqi72jtrrguj63e', '192.168.1.110', 1571289611, ''),
('vgvv4kaqm0dcckc4olrgqc1j4mrpe3o5', '192.168.1.110', 1571290620, ''),
('03166dnmt3g61u7pvqbjh37t6169rlen', '192.168.1.111', 1571289394, ''),
('mshg9kf9uomsq9l0q74jv7f2s4udmmhp', '192.168.1.111', 1571309870, 'school_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"1\";class_date|s:10:\"2019-10-17\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fpr_start_date|s:10:\"01-10-2019\";fpr_end_date|s:10:\"17-10-2019\";fpr_class_id|s:0:\"\";fpr_section_id|s:0:\"\";fpr_name|s:0:\"\";payment_total_amount|i:4244;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Surajit Das\";s:5:\"email\";s:17:\"surajit@ablion.in\";s:5:\"phone\";s:10:\"9851661168\";s:10:\"payment_id\";i:6;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"4244\";s:6:\"amount\";i:424400;}admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;parent_id|s:2:\"95\";parent_is_logged_in|i:1;student_id|s:2:\"31\";student_flag|i:1;receiveFeesBreakupId|s:1:\"6\";'),
('jlggpprh8f8cq06ffqfntjluc4ksth72', '192.168.1.109', 1571318793, 'class_id|s:2:\"28\";section_id|s:2:\"61\";query_date|s:10:\"2019-10-17\";class_date|s:10:\"2019-10-17\";att_start_date|s:10:\"2019-10-17\";att_end_date|s:10:\"2019-10-17\";s_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('fei5bnm9q60vh86cv26grgqsgl0u0cut', '192.168.1.109', 1571294836, ''),
('fvs74bfunqreohqskmrh1hftdvaq5fk7', '192.168.1.109', 1571294836, ''),
('rq87nmpl0b5vpnttujra7cdm4hp4bio2', '192.168.1.109', 1571294836, ''),
('2octathscukfh93lucceo1rug76ff9j2', '192.168.1.109', 1571314396, ''),
('9trtbkj8so6ri4ulolrnh8b0ctear338', '192.168.1.138', 1571316980, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:1:\"1\";query_date|s:10:\"2019-10-17\";class_id|s:1:\"7\";section_id|s:2:\"29\";payment_total_amount|i:4242;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:11:\"Biplob Mudi\";s:5:\"email\";s:16:\"biplob@ablion.in\";s:5:\"phone\";s:10:\"9749552668\";s:10:\"payment_id\";i:11;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"4242\";s:6:\"amount\";i:424200;}admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_email|s:19:\"aparajita@ablion.in\";'),
('alj8fishj6qmd4u9abukn93ac5oa1nll', '192.168.1.21', 1571313427, 'mobile|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('pdq7nke8od8620gvdhlhs2mf9vtot0uh', '192.168.1.111', 1571311797, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;receiveFeesBreakupId|s:1:\"1\";'),
('85a8hi8r3mr3s049a2vnq6r5990erpvi', '192.168.1.109', 1571317092, ''),
('mlmq1vsu1ujiub081jifnvb42nlcqafc', '192.168.1.109', 1571314398, ''),
('u9opkn6citq0uf0q3jg42gve9fb8m2uu', '192.168.1.109', 1571314398, ''),
('ca7lq98ameduqofv8jnllh9rr4q0qb1u', '192.168.1.109', 1571314398, ''),
('bbtb6fis7ngtotkuoep11h852vclig49', '192.168.1.111', 1571314524, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"22\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;usertype|s:1:\"1\";'),
('h8uta3lhh2dqnukp0vm5oi04to4469l0', '192.168.1.111', 1571315896, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_id|s:2:\"22\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;usertype|s:1:\"1\";s_message|s:37:\"Students record successfully updated.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('lp4oe7p72bn5dkdp66su9qr310mctsr0', '192.168.1.111', 1571315901, 'parent_id|s:2:\"44\";school_id|s:2:\"22\";parent_is_logged_in|i:1;student_id|s:2:\"17\";class_id|s:1:\"7\";section_id|s:2:\"29\";student_flag|i:1;'),
('lephq7b119pi4kjv1lnho3bvencqkp32', '192.168.1.109', 1571317156, 's_message|s:32:\"You are successfully logged out.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('bbod2h7qtcbdohdala31gml2gvit7lsm', '192.168.1.109', 1571385827, 'query_date|s:10:\"2019-10-18\";class_id|s:2:\"28\";section_id|s:2:\"61\";usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;'),
('k3nutsrh4s9gn1uvcidjlqsf8k3k605o', '192.168.1.109', 1571385155, ''),
('t4rvvcet5a0kmdbbtr758je1splnegfm', '192.168.1.110', 1571379154, ''),
('p42hlb1n97u3e9d1t007be6jlp4dljmj', '192.168.1.110', 1571384347, 'usertype|s:1:\"1\";school_id|s:3:\"168\";school_username|s:5:\"29834\";school_email|s:21:\"biplob.mudi@gmail.com\";school_is_logged_in|i:1;'),
('dqhd5f8goq9vdlgp17utlqt29m9ffm84', '192.168.1.138', 1571382465, 'usertype|s:1:\"1\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;class_id|s:1:\"1\";section_id|s:1:\"2\";admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_email|s:16:\"biplob@ablion.in\";parent_id|s:2:\"15\";parent_is_logged_in|i:1;student_id|s:1:\"1\";student_flag|i:1;'),
('6s49d0v7ta5fbsbnv42lh8540ddfca28', '192.168.1.110', 1571384420, 'usertype|s:1:\"1\";school_id|s:3:\"168\";school_username|s:5:\"29834\";school_email|s:21:\"biplob.mudi@gmail.com\";school_is_logged_in|i:1;'),
('l00d1n0jfderlgfttmidggfc7s0e5vl5', '192.168.1.109', 1571385158, ''),
('vmfkdcanqfdu8c76es4ql2doeqsrussb', '192.168.1.109', 1571385158, ''),
('es2a2nk9t9d0fmocl353n3t2igpa3gcu', '192.168.1.109', 1571385158, ''),
('h5ufar5rh63qvl4luebr6e50roc9j3df', '192.168.1.109', 1571404488, 'query_date|s:10:\"2019-10-18\";usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('rsnccfjadoj9sfuqj9bbtqtgolagd1c0', '192.168.1.110', 1571403953, ''),
('pgia5kcueu1152cgmnldr1qpjhmln9qj', '192.168.1.138', 1571403424, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:2:\"22\";student_flag|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;usertype|s:1:\"1\";'),
('s23r0i1vi7gfl2npplrn0glh86oicui0', '192.168.1.138', 1571394980, 'parent_id|s:2:\"15\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";student_flag|i:1;'),
('s0tpb5mlmgeg31j96o2dt27ubmhl1320', '192.168.1.98', 1571404357, ''),
('0qk95s8dbt4llse20mkv87k6fkkiolfb', '192.168.1.110', 1571633216, ''),
('mmp4ro99vpuljbt6s1l8jb8cikpi73jn', '192.168.1.111', 1571634011, ''),
('eqf572i5r69r8ar6985nv5t1oke2euu3', '192.168.1.98', 1571635755, ''),
('mfu8jd32f162ktgktdvunn2hvudqe1gg', '192.168.1.98', 1571636165, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('udl9mu63usop5pujuci9t4tffd4t21hj', '192.168.1.111', 1571643584, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('19adt8tpu3mq28lv87j0eo2194i4tg7a', '192.168.1.109', 1571656780, ''),
('0j127fk63e2bpjpt4b5kh9d7o6ribrl8', '192.168.1.109', 1571664574, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;s_message|s:28:\"Record updated successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('dr7ejsvpjnouif85f411m0tacjaq1vqb', '192.168.1.109', 1571722890, ''),
('15lgdsqd58kb006ujajepdarc7ob2bjv', '192.168.1.109', 1571726115, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;usertype|s:1:\"1\";school_id|s:3:\"174\";school_username|s:6:\"978890\";school_email|s:21:\"biplob.mudi@gmail.com\";school_is_logged_in|i:1;'),
('lahjsinoqpu3osh221mcq0thbrhn5f8k', '192.168.1.111', 1571726776, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('607fs4uug9p423tuem7qt3s6irfgrjia', '192.168.1.111', 1571748247, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;class_date|s:10:\"2019-10-22\";usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('68up392f7pa0oef0qn3pm0kg30kv8re2', '192.168.1.24', 1571729003, 'mobile|i:1;'),
('espk6smbtrtvsjsas5ecuu5e46ihs9e2', '192.168.1.109', 1571750482, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;'),
('cvd91ci1cc3brrtadrl9jhmatn9id7v6', '192.168.1.111', 1571750346, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('me6d3sg9fbj0ip7n8ti3og4bmjcuveoh', '192.168.1.111', 1571748793, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('ma2ofv85l5siv8n1ff388j21jbjjc5j4', '192.168.1.30', 1571748998, 'mobile|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('8d79miskb2f39tdilrfvhogr9cnj423l', '192.168.1.30', 1571749005, 'mobile|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('i0ond5gofat3f1uos22hegklfuklvdfr', '192.168.1.30', 1571750767, 'mobile|i:1;parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('aa317omruek94scgrf7vh8ht6rdosdb1', '192.168.1.30', 1571749601, 'mobile|i:1;'),
('aa3nvsf6jdq631letqbtj4r5heeelhg7', '192.168.1.109', 1571809817, ''),
('ugpg132fg4r3tsnkdt0djf617ht74rvj', '192.168.1.109', 1571834499, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;'),
('bqdd99egcbk16aj8ckacsu3ts4vb63lb', '192.168.1.111', 1571805990, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('44et1dmsn0f8eijaekqkh5eis28j74jq', '192.168.1.111', 1571807118, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('dv646r4phb63o7a9n8fj8k3t7k7adpcu', '192.168.1.20', 1571815668, 'mobile|i:1;parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('f7civbmv1tfnoqlqbr5nvb2oq0s24s7p', '192.168.1.20', 1571814118, 'mobile|i:1;'),
('4u1v21gjh0qtkencb43h85a2u0brgtsl', '192.168.1.111', 1571814707, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('fse2756jtjcej2661tgoq09j0dbh9e3b', '192.168.1.111', 1571814720, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('ala9co88tsnri6rp98jftef0vlehsj8e', '192.168.1.111', 1571815708, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('547ep7ohggcfdshj9hul4g47clqrslij', '192.168.1.23', 1571821188, 'mobile|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;'),
('a0rmorqa6svmsfp9igbfjrq149fjnmcb', '192.168.1.34', 1571815302, 'mobile|i:1;parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('csu81v9n3a3qfi7b27p5jfeb437abc6g', '192.168.1.138', 1571821347, 'student_id|s:1:\"1\";class_id|s:1:\"1\";section_id|s:1:\"2\";school_id|s:1:\"1\";student_flag|i:1;'),
('l5fl9qkf72efuenhvl09nt6rm449jctg', '192.168.1.138', 1571841185, 'parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"40\";class_id|s:2:\"27\";section_id|s:2:\"60\";student_flag|i:1;usertype|s:1:\"1\";school_id|s:2:\"23\";school_username|s:6:\"793341\";school_email|s:16:\"pratik@ablion.in\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('imlud8qvs79vuqaubrf5b1p1q33i3jcf', '192.168.1.23', 1571824227, 'mobile|i:1;student_id|s:2:\"19\";class_id|s:2:\"12\";section_id|s:2:\"31\";school_id|s:2:\"22\";student_flag|i:1;'),
('322064ojcidet38f2keu29dq1igcg4as', '192.168.1.25', 1571835231, 'mobile|i:1;school_id|s:3:\"178\";class_id|s:2:\"30\";section_id|s:2:\"62\";parent_id|s:2:\"44\";parent_is_logged_in|i:1;student_id|s:2:\"46\";student_flag|i:1;payment_total_amount|i:1785;onetimeRazorPay|a:8:{s:11:\"description\";s:21:\"Bidyaaly Fees Payment\";s:4:\"name\";s:7:\"A desai\";s:5:\"email\";s:14:\"desai@mail.com\";s:5:\"phone\";s:10:\"7063605278\";s:10:\"payment_id\";i:1012;s:16:\"display_currency\";s:3:\"INR\";s:14:\"display_amount\";s:4:\"1785\";s:6:\"amount\";i:178500;}'),
('eujrsnu7n4n647i4rlre7t8r4t349stt', '192.168.1.111', 1571826820, 'usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;s_message|s:50:\"Manual fees setting has been updated successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('f28pnfe7p3f5rqums8dnaqd7pacufuo3', '192.168.1.109', 1571833511, ''),
('9unnd2j9u039unni3i470ntgomvsv25k', '192.168.1.111', 1571836503, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;'),
('7udq28oaf0imek3m73b6aaqgj7f2sira', '192.168.1.98', 1571836466, ''),
('gjjqon23banb6olcpu8in3eu7hi5tnnm', '192.168.1.98', 1571836682, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;'),
('3alqp29dp1a8uc9pccskm0svq0edobt0', '192.168.1.111', 1571836568, 'parent_id|s:2:\"44\";school_id|s:3:\"178\";parent_is_logged_in|i:1;student_id|s:2:\"46\";class_id|s:2:\"30\";section_id|s:2:\"62\";student_flag|i:1;'),
('hbc5de9dedppd95r607upftn1pus011b', '192.168.1.109', 1571896406, ''),
('am5q2ea4n07ck4u2loi0d89garvh15m1', '192.168.1.109', 1571922653, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;'),
('ejvdhh3b3pv23fff45gdcb5fvecrm6c0', '192.168.1.111', 1571893480, 'parent_id|s:2:\"95\";school_id|s:1:\"1\";parent_is_logged_in|i:1;student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";student_flag|i:1;'),
('8kk8lra7hd1jkc3k9oumf4co8scumffr', '192.168.1.138', 1571903716, 'admin_id|s:1:\"1\";admin_username|s:9:\"developer\";admin_email|s:16:\"biplob@ablion.in\";admin_is_logged_in|i:1;receiveFeesBreakupId|s:2:\"27\";fpr_start_date|s:10:\"24-10-2019\";fpr_end_date|s:10:\"24-10-2019\";usertype|s:1:\"1\";school_id|s:2:\"22\";school_username|s:6:\"191122\";school_email|s:19:\"aparajita@ablion.in\";school_is_logged_in|i:1;'),
('btqlbo627lu1gtco90atcm0anqcskv2o', '192.168.1.111', 1571900142, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;fpr_start_date|s:10:\"01-10-2019\";fpr_end_date|s:10:\"24-10-2019\";fpr_class_id|s:0:\"\";fpr_section_id|s:0:\"\";fpr_name|s:0:\"\";'),
('ambtso1neo4npci9etcuur1h3sesc5pb', '192.168.1.138', 1571912834, ''),
('haf8lhg0mmdqmhoslq6e4diof8a69afe', '192.168.1.111', 1571919899, 'student_id|s:2:\"31\";class_id|s:1:\"1\";section_id|s:1:\"1\";school_id|s:1:\"1\";student_flag|i:1;usertype|s:1:\"2\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;'),
('rj9nn5hsub8c6t7uq8iobitd6698i21l', '192.168.1.109', 1571916879, ''),
('7o8kavan1d96ak8s5gdgilv0ovtl7dvd', '192.168.1.138', 1571921516, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;s_message|s:31:\"Exam record added successfully.\";__ci_vars|a:1:{s:9:\"s_message\";s:3:\"old\";}'),
('imcu7v5kmdbfc66um3urh49kdcr4s7jp', '192.168.1.109', 1571982453, ''),
('bgk1qnmgp6gkj5rf7no8j0edgbr379sa', '192.168.1.109', 1571999050, 'usertype|s:1:\"1\";school_id|s:3:\"169\";school_username|s:6:\"131537\";school_email|s:18:\"biplob06@gmail.com\";school_is_logged_in|i:1;'),
('cerjceqqrq9kgn9ttvv3djqkgqg7aqhe', '192.168.1.109', 1571995867, ''),
('13qqq0991a9i0fong2agv84ubv86v4vo', '192.168.1.111', 1571999060, 'usertype|s:1:\"2\";school_id|s:1:\"1\";teacher_id|s:1:\"8\";school_username|s:6:\"biplob\";school_is_logged_in|i:1;');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes`
--

CREATE TABLE `tbl_classes` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `class_name` varchar(155) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_classes`
--

INSERT INTO `tbl_classes` (`id`, `school_id`, `class_name`, `status`) VALUES
(1, 1, 'STD 1', 1),
(2, 1, 'STD 2', 1),
(3, 1, 'STD 3', 1),
(4, 1, 'STD 4', 1),
(6, 1, 'STD 5', 1),
(7, 22, 'STD 1', 1),
(8, 23, 'Standard 1', 1),
(9, 23, 'Class 2', 1),
(10, 23, 'KG - Upper', 1),
(11, 23, 'IX', 1),
(12, 22, 'STD 2', 1),
(14, 22, 'STD 3', 1),
(15, 22, 'STD FOUR', 1),
(16, 29, 'Business 1A', 1),
(17, 29, 'Business 2A', 1),
(19, 50, '1', 1),
(20, 50, '2', 1),
(21, 50, '33', 1),
(22, 50, '4', 1),
(23, 50, '5', 1),
(24, 50, 'test class', 1),
(25, 157, 'class 1', 1),
(26, 157, 'class 2', 1),
(27, 1, 'Java Summer Class', 1),
(28, 169, 'Class 3', 1),
(29, 172, 'STD 1', 1),
(30, 178, 'STD 1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_common_login`
--

CREATE TABLE `tbl_common_login` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=school,2=teacher,3=parent',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `otp` int(10) NOT NULL,
  `otp_sending_time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active,2=inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_common_login`
--

INSERT INTO `tbl_common_login` (`id`, `type`, `username`, `password`, `otp`, `otp_sending_time`, `status`) VALUES
(1, 1, '95340', '$2y$10$QlVcpdqd2h3XDM5MmsQBXuADU0w/UHi8NDIQYn.AAqIq.c.iBa9.S', 2759, 1571123341, 1),
(2, 1, '433196', '$2y$10$VX8QHHwRM3kxjff7Smo7LuVxC1v9/JYPStOPGteC96fGs3jHhoXoe', 0, 0, 1),
(3, 1, '365208', '$2y$10$xeZOVK6wcblgw18zJGEYgOfqyHHiGjrUcbS4L3NFktSmX8ZUGXJ2S', 0, 0, 1),
(4, 2, '9474728606', '$2y$10$Z2Zj3aq/ptsacrxvdwjkeuxqdMo.qDV3JwDouPu2AHFqQ25A3NuUW', 0, 0, 1),
(5, 2, 'sib', '$2y$10$nWE2paWzcGUoKYkkjYUiR.CGYMyMgzmsQoBx3xX6dunM08mSkGgcy', 0, 0, 1),
(6, 2, 'sankhya', '$2y$10$fGXzQjdZ4fnmVa6jIWI0sOKOVG54cU88nQf0FYq.3IcwCd2p0E7bC', 0, 0, 1),
(7, 2, 'santanu', '$2y$10$WSidYpBO5D/XKwGGFKM53eq7yln7.jSu8YoUQe0bPoHJ56LE0DJAa', 0, 0, 1),
(8, 2, 'biplob', '$2y$10$JPVf91vI6XYSFodqJQSGkO/QoKkb61.SWVm.6QOQld/WnWLuWzT7.', 0, 0, 1),
(9, 2, '8250736708', '$2y$10$EH9hJPo58RDlwQAeWaDjiuT7JQHbIpL1xNbUV73OWLo4M2p0r8PLK', 0, 0, 1),
(10, 2, '8145771526', '$2y$10$NabfpGEKBQa0B77F6dJPceiz8kLy.lFsRSzXYK.4A.Kr4W0kiVWbe', 0, 0, 1),
(11, 2, 'aparajita', '$2y$10$pPM4Dc5b4FC4eGBZlZbefeDXqmlnePJ7RtYBFANrOY1nT83rBDlx6', 0, 0, 1),
(15, 3, '9749552668', '$2y$10$xLk/FV1pQStrDoBjcvQar.dg9M6n8qs3HgtoLQrhB/7ajVbwHQvUK', 3261, 0, 1),
(18, 3, '9514752589', '$2y$10$Xg0aV54BWMVcVkYV1oIoB.f9TWLBFSOBXVSoLq2d.3VDNU6hoCyOy', 5784, 0, 1),
(19, 2, 'shamak', '$2y$10$fe9CPGXpp7hO6RaOsoQu2.xk1kOtpklhWP.zDNLhKLvqfgyMxAnAC', 0, 0, 1),
(20, 3, '9510214520', '', 0, 0, 1),
(21, 3, '9585854214', '', 0, 0, 1),
(22, 1, '191122', '$2y$10$agV1dePTWCRuHwdi2RrPx.nnwXmmmsaNAzjCM.wdXcePFWY1Md7HS', 4348, 1571898880, 1),
(23, 1, '997925', '$2y$10$WeOKVPfl5SmHB7ycfKO9weEXVqd0kgFpA3THQi7VxAuFdXlMznzuS', 0, 0, 1),
(26, 3, '9932130035', '$2y$10$6tq1I5oBUGU6q48V9.hOWO5LBzvLG6wg.spqoRdBto/EdsY6JoiZi', 1984, 0, 1),
(27, 2, 'subhankar', '$2y$10$ypeqtVAKdUwfnmf88Tsdn.44k4uMqsGiV1zS.JcwnOn8ZXMutkYE.', 0, 0, 1),
(28, 2, 'harry', '$2y$10$aJRJ6bORpb9AtlbZ85ZoruN15CvwCh4JclaA8CFAcq9iqjqjXRht6', 0, 0, 1),
(29, 1, '86884', '$2y$10$tprQPvNOQxU5AbIDKzl5FuTh90cK9pxye0cHRF7rPumJSBJ6qd5bq', 0, 0, 1),
(30, 2, 'apu', '$2y$10$nKayA3n3dBr.p48ZgHAt6.VaxUWImqhVmuMD1ysypnjW.oxJ9xml6', 0, 0, 1),
(31, 2, 'farhan', '$2y$10$A2avHFt7pnCWAmmw74KebOcJC3rQ8hTp3qQ1USEDr/DnArcXDeG0S', 0, 0, 1),
(32, 2, 'sammy', '$2y$10$kCklMr8bVWFt60SyvSnvE.yN3U8p40o.UETuXK5p5Ry6.2xGo2gRm', 0, 0, 1),
(33, 2, 'tapos', '$2y$10$VEdUX9g4L2RSivCnLVaTXeCjm8Dbl7U.bNgQxI3nwGviYnUuE78FC', 0, 0, 1),
(34, 2, 'harsh', '$2y$10$Ip5QjwkUqAzGoxvgE9N3K.zeNbFdHhPfKE9IeqKZPCvdI4X7OLVAC', 0, 0, 1),
(35, 2, 'bikas', '$2y$10$beKFVyJLvOl224ZUQuw/9uJr266sccSMlqgP0k0OwWXQGPqDWpvvW', 0, 0, 1),
(36, 2, 'bhabesh', '$2y$10$0ZRWzXjwbLhL6ZneyA6YaeDU2bVQJEgV/169pmclP.W/u5ulHNGPq', 0, 0, 1),
(37, 2, 'biswa', '$2y$10$2Lavoe1M122PaFYGxXfvsOhrq/dKZkgQrbV3o4pGAhzHc9yLHvkMC', 0, 0, 1),
(38, 3, '9475126766', '$2y$10$ZwH2YeBhultJofhLz2HFQecu1iEHjwoeiekyXg5hQ1vD0OrYj3c5a', 9618, 0, 1),
(39, 3, '111111', '', 0, 0, 1),
(40, 3, '222222', '', 0, 0, 1),
(41, 3, '333333', '', 0, 0, 1),
(42, 3, '444444', '', 0, 0, 1),
(44, 3, '7063605278', '$2y$10$A0pgF1aAGEgO7DzPSQfOGuO0M9pTXHD79hK.aUAMYHiUkrvWlbX.a', 5658, 0, 1),
(46, 3, '9999988888', '$2y$10$PJzv/CWti6SVADJnZJTvgOzs6Zy9AsC.PGQq43BwW3Y0kOCkRtLwC', 7892, 0, 1),
(47, 3, '1122334455', '$2y$10$3eG.MsRJk8JDppBb/osiOOSlBtbI0GPXpkZFrGYQVyvsJy54l05sO', 6943, 0, 1),
(48, 3, '9988774455', '$2y$10$IqtGlVpTyKF1QX3s352nM.eyA19Jk0e6ew7PmCXxbX9M0.dqUAKae', 3537, 0, 1),
(49, 3, '1234567890', '', 0, 0, 1),
(50, 1, '66774', '$2y$10$sgPtj3TMop/h/Y93oGWLBeW/uj5asMzA2sohKtS2SRvb3utt7NIt6', 0, 0, 1),
(51, 2, 'sudarshan', '$2y$10$4dmL.U8wnsOUs6TzZGRUU.UE0aj06KnH2pDtTmrFtEy8gipBwoNnS', 0, 0, 1),
(52, 2, 'kankali', '$2y$10$MDltSNhnq76nnw7Vdrje0.Cn4OYhF6NCQkHrmDEJyQ8Xiaf4xcuWG', 0, 0, 1),
(53, 2, 'borda', '$2y$10$Lm.JvAeHmdI7R58/KutK/OWe5GJKF/qAv2IvgLAsecwo8DdIzxHpW', 0, 0, 1),
(54, 2, 'monkhya', '$2y$10$Ka7USmak4KqQTm5/FOPCL.KkqEFJiiQDdXsvzmbBFooER.FOlCp6G', 0, 0, 1),
(55, 1, '84102', '$2y$10$ETKOUYQvF0nwd.29FyXFMuBrMyMTnWBK/MLadakYaiEsMGYtE1H8K', 0, 0, 1),
(56, 2, 'abhijit', '$2y$10$PDJSc7syTzh9MWHr6.GR6e9WjxPpbxz1x439uNOno6U2L2J9dPHIa', 0, 0, 1),
(59, 2, 'Ranjan', '$2y$10$mfxAan7IfDY0u2gxEPEfFuZb1KhatGEovAA/DqwAKJBS6z7Jd8116', 0, 0, 1),
(60, 2, 'sanatan', '$2y$10$wWyk08ZehL1OBLitHb/t5.i2.rvsV9fnAmeYUJ2iEE/lkS9e0p9fW', 0, 0, 1),
(61, 3, '8799878998', '', 0, 0, 1),
(62, 3, '8878744785', '', 0, 0, 1),
(63, 3, '9474728606', '$2y$10$peLGHx2exCRXahU7tv5vTeCGVN0zndujwBSvBGlqz0xgrwQmOxCy2', 8651, 0, 1),
(65, 1, '38775', '$2y$10$jssfoBry5sUvBmpdgUoy6OIunNyVTMIm0wl6Lbdya3I8qOOKCQgd6', 0, 0, 1),
(66, 1, '39465', '$2y$10$E96769y06QCw4CQEUNRRfO1dR5/s0ZgtuYhV9XJZh1hyupIKx5UuK', 0, 0, 1),
(67, 2, 'rafi', '$2y$10$lXSqTWagbgm8SucxUaCYWeqjLehfaTue5orNZHOVbt2HerIF7FCZe', 0, 0, 1),
(68, 2, 'sbandh', '$2y$10$3c8aMSCtpmFwMyrem1nztOUAOYzUO8pxq2PcL4agsZcv9PtYeZKVu', 0, 0, 1),
(69, 2, 'asim', '$2y$10$lnFPkyPuY1BO.9yovzx38exQj4tZEnzoea1VmRf3gWcJ7wmnp8pTW', 0, 0, 1),
(70, 2, 'xvv', '$2y$10$MatjcJq4sTawULUf.igcve1mmOy5IZLg/L0jDwyoSFYMxhogKO99m', 0, 0, 1),
(76, 1, '441037', '$2y$10$.WwP3cjoBn.53lCQzEZ8X.czYkn0c03spp/6gVGJoWAsa6cthnaOK', 0, 0, 1),
(77, 2, '9434335143', '$2y$10$e9XNDjdPBAaiEoo.HZwzOOQMDMzEyAvhZZ1W8OnS7X9PEsu21z4UO', 0, 0, 1),
(78, 1, '310039', '$2y$10$NW7W0oULsDIUT1R9sWmkbOg51y6Trs2K514uvDppkT6TGo7g/qLwq', 0, 0, 1),
(88, 3, '9832598325', '', 0, 0, 1),
(90, 2, '9879879870', '$2y$10$A7k36e3zm4pOQ65b89.Oi.jbYzQHGHWALwXrfIdaoervQVA98MsnC', 0, 0, 1),
(91, 3, '9639639630', '', 0, 0, 1),
(92, 3, '7417417410', '', 0, 0, 1),
(93, 3, '9832198320', '', 0, 0, 1),
(94, 3, '9832198321', '$2y$10$APE1lXfnx7/tBmPj/t8w2Og6rNbSbgk.ILFUAcXBE1XdNM8LynKdS', 0, 0, 1),
(95, 3, '9851661168', '$2y$10$87628PPcJXRo16rs4qLJwe6es9Zz9sIcMB5xag3vGcSeDbLnEBYEC', 0, 0, 1),
(96, 1, '156417', '$2y$10$w7XL3bW/MFbe0MoSwCVsXeIz8/jEELCyPiLYUA.8nDnWIrSzrrn8W', 0, 0, 1),
(101, 3, '9832298322', '$2y$10$U7lkI1S6z5wMnjL8KYsgk.5iQBKljyOwMIFCp9Wa65DmavkKbDDs.', 0, 0, 1),
(102, 3, '9732297322', '$2y$10$U7lkI1S6z5wMnjL8KYsgk.5iQBKljyOwMIFCp9Wa65DmavkKbDDs.', 0, 0, 1),
(103, 1, '499342', '$2y$10$TAZGrGpi7EvvcngL5fEVQeHU.6fmb6RgqipCtZNTQmQP7/sJDAaaS', 0, 0, 1),
(104, 1, '918198', '$2y$10$uuIQF.1OB79IiWfIxiuXMuR7F/icMyDUxK5ahGVykJ2bCTg6NIgwe', 0, 0, 1),
(105, 1, '788865', '$2y$10$OvBYKzNV5lj9IxE17sC3G.wFH/gWt/CeKEt8rQ4zTYBsERby9sifi', 0, 0, 1),
(106, 1, '165027', '$2y$10$nx7whop4zyW3zJP1L017PuWNvXea3J0KIiaWHPOVJdqrhVUwtnXg6', 0, 0, 1),
(107, 1, '50610', '$2y$10$fxanj1PtQ4txb0N.bpNvmeTIB2be7smBTbH5Rsm5TIGZW0Eaqb7su', 0, 0, 1),
(108, 1, '680555', '$2y$10$qeB434MU8/m81S0ojkMzLurGPE4/DEBtTQWjwr.AVpzLI5BpaIONm', 0, 0, 1),
(109, 1, '55977', '$2y$10$nY.OQWB56/xYwqW.fDmrQ.XCGuATA/LQPyHS/mNdwiNNhReA82JyC', 0, 0, 1),
(110, 1, '048566', '$2y$10$BvI1kVbxIQsKPgExcn1oVu9XMVif57Xp95V6PKkYNbp9w/Fxj5c7.', 0, 0, 1),
(111, 1, '97416', '$2y$10$x75rWQ5QqxS/r6tVttfCEeiBHln7hDPs84RXM9hb7Phzcyt3iNxZm', 0, 0, 1),
(112, 1, '906276', '$2y$10$TWKbrO7wiQyRrtXI4/H4AuCb3Br.1owUcon6giJwimvHPfkkgwufm', 0, 0, 1),
(113, 1, '81588', '$2y$10$KT4prYkn1Lct0db334n2HOyGPuzbdv3cuG4TIJozKS3niSTbU2rpG', 0, 0, 1),
(114, 1, '0892', '$2y$10$niytTo2D/IhJRWTecpCOGefMdLFKzFcuCQWq05GqKVyRjHrLDSnEG', 0, 0, 1),
(115, 1, '40773', '$2y$10$01aaL39sL2Lu10GeHrMrY.hN0iYl04j9/S0kL6.ExmGmk9oCXagyy', 0, 0, 1),
(116, 1, '60630', '$2y$10$.dagx5rCKqqQ85XIvObcmunnukO6kAv66P8sEa6esHPSxkkUeqV6u', 0, 0, 1),
(117, 1, '5786', '$2y$10$P5YByEHNT4YiHkwcaGdiRusW0bV2jwVhAlNl3yGDBxxynyMYIj/S6', 0, 0, 1),
(118, 1, '33612', '$2y$10$u7HiOahySGPJABd.A4fjHeyPm.fIIhgb6rWJbQ3cggavdnPMqBc1G', 0, 0, 1),
(119, 1, '477934', '$2y$10$kxO9oJ7X.4WxejYASELObOFcc5w9aULGLOdKFpeytSiwFAmdh1cwe', 0, 0, 1),
(120, 1, '6858', '$2y$10$wjbVc7V4S0IzVnSbsU2pTOI8QeuWOn7l7FFz/IHAqJ8WIU8g39dS.', 0, 0, 1),
(121, 1, '128566', '$2y$10$MD2K4LKQFRFOA5kR3WvSteLU8F.EDprEgFqTxnqTzD4jkq6e2nDBK', 0, 0, 1),
(122, 1, '598191', '$2y$10$wd45J.FP7xiddG7kFEAKq.e6uhh1jJZrgUBegXeynzmXz5PUiVeLK', 0, 0, 1),
(123, 1, '851235', '$2y$10$nZPluUXRl0Iwx2E3RUDlUuc6abmhnXSYQZHGzOxNHehFY31NOOtky', 0, 0, 1),
(124, 1, '013523', '$2y$10$wFbt4Js8tY2XkEnHH3aXfOj8lEtvHQzDrrZFsCawY52178oaIFOqK', 0, 0, 1),
(125, 1, '924992', '$2y$10$XqGbZrDBarA.l2r9Ih14ruCV48T6Ottqgpz0H6xbKyY1agu8P5JyO', 0, 0, 1),
(126, 1, '465235', '$2y$10$3cSkl3hEOiQVrHQK555ZuOPoEwrqxrdYRNGayBlJQWc/pyYy74/Ri', 0, 0, 1),
(127, 1, '27734', '$2y$10$4VCOmTegae6W/7NYgAskJOE4UqyS4pQfrbx5B72cc1MPdBV2il3dK', 0, 0, 1),
(128, 1, '6018', '$2y$10$iZ4r7rpYUd27NFTK0pMV/eAUgRfM7OMP2JkrYGRZEbRtTTGRjX0l2', 0, 0, 1),
(129, 1, '107577', '$2y$10$mrqegFH06ihxrb/lovQwT.hM9jeqkk1oehEKoYERI9olyoP0V5T7i', 0, 0, 1),
(130, 1, '499491', '$2y$10$vOmlzoJY48DU0vLQmAvli.DCGo6kw.yxQA.iYkqSqoTHLA8qerXx.', 0, 0, 1),
(131, 1, '564264', '$2y$10$tPf/ms5ermSizrZ6koF2RO06Z/CJK0MQeTT4EnGxinOmdliuxiqIy', 0, 0, 1),
(132, 1, '490488', '$2y$10$ky/VeLdutyhP7Q6hVA/D.O6ARd/kGaDYWNbH4bipB7kD5KySsJUuS', 0, 0, 1),
(133, 1, '042819', '$2y$10$nE5yT4mpPVHA8i8WIeK/vu0BsmcLyGj7v8eiZt2VKjMALuj9JsjWK', 0, 0, 1),
(134, 1, '08896', '$2y$10$rQCCmSzaPZY3jWA9JOqWRe/apvtXmI5Zv9LFHtNCTBgStxgk7HIEu', 0, 0, 1),
(135, 1, '53377', '$2y$10$uwsxlygN00xqO8HVqBRB.O4Wo/ROsCfSDuMeVJqwewLWh1vGtMJTK', 0, 0, 1),
(136, 1, '9555', '$2y$10$Akrk4ZD2M98xdnXksoBfI.67vJegmyWvD8z.f//xQI9Jn5dPJJgt2', 0, 0, 1),
(137, 1, '09820', '$2y$10$FuDxJ6lLFItmTbQoPIu4I.p.uwS1pQvk.NgnY6U0.noGCr4kabjcG', 0, 0, 1),
(138, 1, '475236', '$2y$10$fm32phdjpn01L6gLNmrU4uQN6/sH8JWTL0rSLvPLM.dnOKaJleBKG', 0, 0, 1),
(139, 1, '59937', '$2y$10$r37arOtqiToSsULXS7Ib8OXZIdWUnxY5hcUAWQidyzXieeKDaau2y', 0, 0, 1),
(140, 1, '81738', '$2y$10$I4hk6rqwyj31A86YfrphF.t.rflfqAnN7tp6LRb0qTF3vq/xAV4/W', 0, 0, 1),
(141, 1, '37038', '$2y$10$UiWPfbdd0UlqdFb5PJsJZeEc9did/CSyd7/vwK.lyCqv7I.Ra/v5.', 0, 0, 1),
(142, 1, '172013', '$2y$10$Bl0Y7/K6oawEWRGaVfwoWum2op1iz.kgeo/95fNqOVwNDgS59JMR.', 0, 0, 1),
(143, 1, '95993', '$2y$10$I6NUnPb9UP7xHRF51wPUwOGOGfbLSXl.Zvjrv6wLFEmnueKJIQXzq', 0, 0, 1),
(144, 1, '98085', '$2y$10$CaOVq4YOrsNOULRtaVHHdeJNW7A7W/Eftu3iwo1v0YVSl4xEvA0VW', 0, 0, 1),
(145, 1, '256460', '$2y$10$axJDJvsBcTf8dzf7o1Q0v.2qB2zYHwoirlLkubb1jFyMUBqZfenmq', 0, 0, 1),
(146, 1, '8833', '$2y$10$zixmi6VsdJkWoXMNVk/DtuOuEE/U7wN6xwAWjcU9Anfb9oS00lhCO', 0, 0, 1),
(147, 1, '44126', '$2y$10$713TNEib03A4s/CSP.LU.eknvc/FYrVi5emiNUJ8vyWy/Rux8.sXe', 0, 0, 1),
(148, 1, '175910', '$2y$10$CoIp5EjXXtgh4LL1G9lKEeuCDjlcGGAYpEqYGx24LepqA83GduI9C', 0, 0, 1),
(149, 1, '886261', '$2y$10$jPxxRrO8Ohv4FBAAi0GWuOJYT7OWPOCtaqKydTymNQNrqnjZFKceC', 0, 0, 1),
(150, 1, '25712', '$2y$10$mOGauSQ.4q9gTLUoOQjw0O0hfGLqpAYy.5fpznVDc19iJ3Kfrcjd.', 0, 0, 1),
(151, 1, '64855', '$2y$10$UwFC8ufa8bUnTtMbWqA0y.phLMUYHf9eyHPw6/O.MKeM8HB395aEm', 0, 0, 1),
(152, 1, '163947', '$2y$10$QlVcpdqd2h3XDM5MmsQBXuADU0w/UHi8NDIQYn.AAqIq.c.iBa9.S', 0, 0, 1),
(153, 2, 'rajdeep', '$2y$10$4b9jsEmmAHtNgC21DX6G8eIGRntHBVXsY.l6cW2AJzfdJVJxOrxA2', 0, 0, 1),
(154, 1, '584341', '$2y$10$wSQiDDSnzT/1NVn9nm9xFO56iLSDWTxj7ezFA0wiRFZbMP7172aNS', 0, 0, 1),
(155, 1, '605785', '$2y$10$gULfARkFgAEfLSwq1rfL9u/re9gss97eHHzeJ2X9hW9XbyIUGJwii', 0, 0, 1),
(156, 1, '443563', '$2y$10$y39aslnrnWl939dZOYwVJeaHALn2GQdL8xu1I/jMy1MdJngzvO8z6', 0, 0, 1),
(157, 1, '729865', '$2y$10$LUyE9eD92e4zMCm1pYKKEO.uYA1TXskd/WYQBgqI9S0u8USqXU8GK', 0, 0, 1),
(158, 2, 'teachera', '$2y$10$mii7tkOv9jqmwGar2Fi63eqL3B5gu9KxrfzlzaMungvCBULPlv.q6', 0, 0, 1),
(159, 3, '9532095320', '$2y$10$V3JKDcop5FQDdput5DAebOyILNQBHKfqqhqsRqBcUrRFxIOAYq4ua', 0, 0, 1),
(160, 3, '9432094320', '$2y$10$Xu6GmZ4elQ1KNEc53WbGieHjKCpqqmPGOlrfKOU.b3lQtACXfR9uq', 0, 0, 1),
(161, 3, '9332093320', '$2y$10$XOmVLvKWql4uqe5T70DIu.zWiuaIlz4aLUHhiL/GniXHJwTMM/ZMG', 0, 0, 1),
(162, 1, '74951', '$2y$10$LEoQlAnbcS4RilS/ITShaOUMfYY18fxyfuf6IDWFRF6lq4ehPV6ry', 0, 0, 1),
(163, 1, '94451', '$2y$10$d0GBjo8MIJqWZ6hbN5vmbOBNsSx6j6rb.AdPOmJMw9X72g.nUu8vu', 0, 0, 1),
(164, 1, '779361', '$2y$10$bOxq1FaJUTWpJIlnWHC.wOb1AeUQlWGc2Rs8Km7HL8fxVZBYO/cQq', 0, 0, 1),
(165, 1, '03900', '$2y$10$bG4nq4Om7PeXgJQ7vrM7B.75dSSIRZUVo4XKC/YBdoRlReSQKHoty', 1818, 1570800617, 1),
(166, 1, '026690', '$2y$10$MsnyvYBFXhFbwH4lp/Am0eb/zT14oG5Jl1FWTlv0rGwt/2kes3tcW', 8458, 1570873944, 1),
(167, 1, '72196', '$2y$10$3KCg0hLd68OfWQvvJWdHSeZO/Si.m/bnAzIIUMf/nHLJK5y7XzNGS', 3768, 1571203295, 1),
(168, 1, '29834', '$2y$10$Z3pkR82r.d2nxav0fCLagu.Bj9SsSKtOPblOH8Z10XsC81DCJdqFK', 0, 0, 1),
(169, 1, '131537', '$2y$10$cfJcPLdG7HigFk1Ymtd0huunFew/WqpoE81MLwOmDly66izhD4BJu', 9765, 1571312021, 1),
(170, 2, 'anirban', '$2y$10$WVLsG5qLHt8nB96Lf.OLt.dOuM2oz2PI/YJ9ZI0KbGMj5h0QDZzrC', 0, 0, 1),
(171, 3, '9434114932', '$2y$10$4vMjeieANZus3/HMmHwrQOBGz2zUjvtmARLdbK7UWzhqICWv9D3OO', 0, 0, 1),
(172, 1, '141678', '$2y$10$9hENnEblVNA6Gc2Rer8uAOzrrrOr3kmPnJpe03fNboiiaruMamfju', 0, 0, 1),
(173, 1, '9874', '$2y$10$yoVRZP8cFkK0vUAjMV5d/unG2r5xM5ixVAi.tug8pgK4i1hCI5k62', 0, 0, 1),
(174, 1, '978890', '$2y$10$XHsVa4HE4rlIQ8KZ1O89uOsd.fR17nStrrIOuxIazlAp1GAvCv5FO', 0, 0, 1),
(175, 3, '9935699356', '$2y$10$B5PzH0tJUHrxmNCPn1BLXey.57GmXfPtB95nn.9DDVzI3zgHDlEEW', 0, 0, 1),
(176, 3, '7789877898', '$2y$10$d8NWRWKW/k8gOCornjGr3OCp04CApdK0mw0DNoxdkDeDUNs6Ri2m.', 0, 0, 1),
(177, 1, '811588', '$2y$10$D4M.b94oRMVNn3hqSaL2d.7.6Y/U3RWcOPS.80r2tLXby4L4WV.Fy', 5987, 1571833721, 1),
(178, 1, '793341', '$2y$10$BlZ.ACqa.S0l7kZyqZRaheNdKJyzR.MVOl4wJiDtKX56pYboTzrjK', 8342, 1571840348, 1),
(179, 1, '13674', '$2y$10$D1UCIzUdTd2E19x/SZah2ORbQ5jqMkWFBWaRE19RRfRTUuKAcP26q', 2941, 1571898483, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `country_id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`country_id`, `code`, `name`, `status`) VALUES
(1, 'AF', 'Afghanistan', 1),
(2, 'AL', 'Albania', 1),
(3, 'DZ', 'Algeria', 1),
(4, 'DS', 'American Samoa', 1),
(5, 'AD', 'Andorra', 1),
(6, 'AO', 'Angola', 1),
(7, 'AI', 'Anguilla', 1),
(8, 'AQ', 'Antarctica', 1),
(9, 'AG', 'Antigua and Barbuda', 1),
(10, 'AR', 'Argentina', 1),
(11, 'AM', 'Armenia', 1),
(12, 'AW', 'Aruba', 1),
(13, 'AU', 'Australia', 1),
(14, 'AT', 'Austria', 1),
(15, 'AZ', 'Azerbaijan', 1),
(16, 'BS', 'Bahamas', 1),
(17, 'BH', 'Bahrain', 1),
(18, 'BD', 'Bangladesh', 1),
(19, 'BB', 'Barbados', 1),
(20, 'BY', 'Belarus', 1),
(21, 'BE', 'Belgium', 1),
(22, 'BZ', 'Belize', 1),
(23, 'BJ', 'Benin', 1),
(24, 'BM', 'Bermuda', 1),
(25, 'BT', 'Bhutan', 1),
(26, 'BO', 'Bolivia', 1),
(27, 'BA', 'Bosnia and Herzegovina', 1),
(28, 'BW', 'Botswana', 1),
(29, 'BV', 'Bouvet Island', 1),
(30, 'BR', 'Brazil', 1),
(31, 'IO', 'British Indian Ocean Territory', 1),
(32, 'BN', 'Brunei Darussalam', 1),
(33, 'BG', 'Bulgaria', 1),
(34, 'BF', 'Burkina Faso', 1),
(35, 'BI', 'Burundi', 1),
(36, 'KH', 'Cambodia', 1),
(37, 'CM', 'Cameroon', 1),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 1),
(40, 'KY', 'Cayman Islands', 1),
(41, 'CF', 'Central African Republic', 1),
(42, 'TD', 'Chad', 1),
(43, 'CL', 'Chile', 1),
(44, 'CN', 'China', 1),
(45, 'CX', 'Christmas Island', 1),
(46, 'CC', 'Cocos (Keeling) Islands', 1),
(47, 'CO', 'Colombia', 1),
(48, 'KM', 'Comoros', 1),
(49, 'CG', 'Congo', 1),
(50, 'CK', 'Cook Islands', 1),
(51, 'CR', 'Costa Rica', 1),
(52, 'HR', 'Croatia (Hrvatska)', 1),
(53, 'CU', 'Cuba', 1),
(54, 'CY', 'Cyprus', 1),
(55, 'CZ', 'Czech Republic', 1),
(56, 'DK', 'Denmark', 1),
(57, 'DJ', 'Djibouti', 1),
(58, 'DM', 'Dominica', 1),
(59, 'DO', 'Dominican Republic', 1),
(60, 'TP', 'East Timor', 1),
(61, 'EC', 'Ecuador', 1),
(62, 'EG', 'Egypt', 1),
(63, 'SV', 'El Salvador', 1),
(64, 'GQ', 'Equatorial Guinea', 1),
(65, 'ER', 'Eritrea', 1),
(66, 'EE', 'Estonia', 1),
(67, 'ET', 'Ethiopia', 1),
(68, 'FK', 'Falkland Islands (Malvinas)', 1),
(69, 'FO', 'Faroe Islands', 1),
(70, 'FJ', 'Fiji', 1),
(71, 'FI', 'Finland', 1),
(72, 'FR', 'France', 1),
(73, 'FX', 'France, Metropolitan', 1),
(74, 'GF', 'French Guiana', 1),
(75, 'PF', 'French Polynesia', 1),
(76, 'TF', 'French Southern Territories', 1),
(77, 'GA', 'Gabon', 1),
(78, 'GM', 'Gambia', 1),
(79, 'GE', 'Georgia', 1),
(80, 'DE', 'Germany', 1),
(81, 'GH', 'Ghana', 1),
(82, 'GI', 'Gibraltar', 1),
(83, 'GK', 'Guernsey', 1),
(84, 'GR', 'Greece', 1),
(85, 'GL', 'Greenland', 1),
(86, 'GD', 'Grenada', 1),
(87, 'GP', 'Guadeloupe', 1),
(88, 'GU', 'Guam', 1),
(89, 'GT', 'Guatemala', 1),
(90, 'GN', 'Guinea', 1),
(91, 'GW', 'Guinea-Bissau', 1),
(92, 'GY', 'Guyana', 1),
(93, 'HT', 'Haiti', 1),
(94, 'HM', 'Heard and Mc Donald Islands', 1),
(95, 'HN', 'Honduras', 1),
(96, 'HK', 'Hong Kong', 1),
(97, 'HU', 'Hungary', 1),
(98, 'IS', 'Iceland', 1),
(99, 'IN', 'India', 1),
(100, 'IM', 'Isle of Man', 1),
(101, 'ID', 'Indonesia', 1),
(102, 'IR', 'Iran (Islamic Republic of)', 1),
(103, 'IQ', 'Iraq', 1),
(104, 'IE', 'Ireland', 1),
(105, 'IL', 'Israel', 1),
(106, 'IT', 'Italy', 1),
(107, 'CI', 'Ivory Coast', 1),
(108, 'JE', 'Jersey', 1),
(109, 'JM', 'Jamaica', 1),
(110, 'JP', 'Japan', 1),
(111, 'JO', 'Jordan', 1),
(112, 'KZ', 'Kazakhstan', 1),
(113, 'KE', 'Kenya', 1),
(114, 'KI', 'Kiribati', 1),
(115, 'KP', 'Korea, Democratic People\'s Republic of', 1),
(116, 'KR', 'Korea, Republic of', 1),
(117, 'XK', 'Kosovo', 1),
(118, 'KW', 'Kuwait', 1),
(119, 'KG', 'Kyrgyzstan', 1),
(120, 'LA', 'Lao People\'s Democratic Republic', 1),
(121, 'LV', 'Latvia', 1),
(122, 'LB', 'Lebanon', 1),
(123, 'LS', 'Lesotho', 1),
(124, 'LR', 'Liberia', 1),
(125, 'LY', 'Libyan Arab Jamahiriya', 1),
(126, 'LI', 'Liechtenstein', 1),
(127, 'LT', 'Lithuania', 1),
(128, 'LU', 'Luxembourg', 1),
(129, 'MO', 'Macau', 1),
(130, 'MK', 'Macedonia', 1),
(131, 'MG', 'Madagascar', 1),
(132, 'MW', 'Malawi', 1),
(133, 'MY', 'Malaysia', 1),
(134, 'MV', 'Maldives', 1),
(135, 'ML', 'Mali', 1),
(136, 'MT', 'Malta', 1),
(137, 'MH', 'Marshall Islands', 1),
(138, 'MQ', 'Martinique', 1),
(139, 'MR', 'Mauritania', 1),
(140, 'MU', 'Mauritius', 1),
(141, 'TY', 'Mayotte', 1),
(142, 'MX', 'Mexico', 1),
(143, 'FM', 'Micronesia, Federated States of', 1),
(144, 'MD', 'Moldova, Republic of', 1),
(145, 'MC', 'Monaco', 1),
(146, 'MN', 'Mongolia', 1),
(147, 'ME', 'Montenegro', 1),
(148, 'MS', 'Montserrat', 1),
(149, 'MA', 'Morocco', 1),
(150, 'MZ', 'Mozambique', 1),
(151, 'MM', 'Myanmar', 1),
(152, 'NA', 'Namibia', 1),
(153, 'NR', 'Nauru', 1),
(154, 'NP', 'Nepal', 1),
(155, 'NL', 'Netherlands', 1),
(156, 'AN', 'Netherlands Antilles', 1),
(157, 'NC', 'New Caledonia', 1),
(158, 'NZ', 'New Zealand', 1),
(159, 'NI', 'Nicaragua', 1),
(160, 'NE', 'Niger', 1),
(161, 'NG', 'Nigeria', 1),
(162, 'NU', 'Niue', 1),
(163, 'NF', 'Norfolk Island', 1),
(164, 'MP', 'Northern Mariana Islands', 1),
(165, 'NO', 'Norway', 1),
(166, 'OM', 'Oman', 1),
(167, 'PK', 'Pakistan', 1),
(168, 'PW', 'Palau', 1),
(169, 'PS', 'Palestine', 1),
(170, 'PA', 'Panama', 1),
(171, 'PG', 'Papua New Guinea', 1),
(172, 'PY', 'Paraguay', 1),
(173, 'PE', 'Peru', 1),
(174, 'PH', 'Philippines', 1),
(175, 'PN', 'Pitcairn', 1),
(176, 'PL', 'Poland', 1),
(177, 'PT', 'Portugal', 1),
(178, 'PR', 'Puerto Rico', 1),
(179, 'QA', 'Qatar', 1),
(180, 'RE', 'Reunion', 1),
(181, 'RO', 'Romania', 1),
(182, 'RU', 'Russian Federation', 1),
(183, 'RW', 'Rwanda', 1),
(184, 'KN', 'Saint Kitts and Nevis', 1),
(185, 'LC', 'Saint Lucia', 1),
(186, 'VC', 'Saint Vincent and the Grenadines', 1),
(187, 'WS', 'Samoa', 1),
(188, 'SM', 'San Marino', 1),
(189, 'ST', 'Sao Tome and Principe', 1),
(190, 'SA', 'Saudi Arabia', 1),
(191, 'SN', 'Senegal', 1),
(192, 'RS', 'Serbia', 1),
(193, 'SC', 'Seychelles', 1),
(194, 'SL', 'Sierra Leone', 1),
(195, 'SG', 'Singapore', 1),
(196, 'SK', 'Slovakia', 1),
(197, 'SI', 'Slovenia', 1),
(198, 'SB', 'Solomon Islands', 1),
(199, 'SO', 'Somalia', 1),
(200, 'ZA', 'South Africa', 1),
(201, 'GS', 'South Georgia South Sandwich Islands', 1),
(202, 'ES', 'Spain', 1),
(203, 'LK', 'Sri Lanka', 1),
(204, 'SH', 'St. Helena', 1),
(205, 'PM', 'St. Pierre and Miquelon', 1),
(206, 'SD', 'Sudan', 1),
(207, 'SR', 'Suriname', 1),
(208, 'SJ', 'Svalbard and Jan Mayen Islands', 1),
(209, 'SZ', 'Swaziland', 1),
(210, 'SE', 'Sweden', 1),
(211, 'CH', 'Switzerland', 1),
(212, 'SY', 'Syrian Arab Republic', 1),
(213, 'TW', 'Taiwan', 1),
(214, 'TJ', 'Tajikistan', 1),
(215, 'TZ', 'Tanzania, United Republic of', 1),
(216, 'TH', 'Thailand', 1),
(217, 'TG', 'Togo', 1),
(218, 'TK', 'Tokelau', 1),
(219, 'TO', 'Tonga', 1),
(220, 'TT', 'Trinidad and Tobago', 1),
(221, 'TN', 'Tunisia', 1),
(222, 'TR', 'Turkey', 1),
(223, 'TM', 'Turkmenistan', 1),
(224, 'TC', 'Turks and Caicos Islands', 1),
(225, 'TV', 'Tuvalu', 1),
(226, 'UG', 'Uganda', 1),
(227, 'UA', 'Ukraine', 1),
(228, 'AE', 'United Arab Emirates', 1),
(229, 'GB', 'United Kingdom', 1),
(230, 'US', 'United States', 1),
(231, 'UM', 'United States minor outlying islands', 1),
(232, 'UY', 'Uruguay', 1),
(233, 'UZ', 'Uzbekistan', 1),
(234, 'VU', 'Vanuatu', 1),
(235, 'VA', 'Vatican City State', 1),
(236, 'VE', 'Venezuela', 1),
(237, 'VN', 'Vietnam', 1),
(238, 'VG', 'Virgin Islands (British)', 1),
(239, 'VI', 'Virgin Islands (U.S.)', 1),
(240, 'WF', 'Wallis and Futuna Islands', 1),
(241, 'EH', 'Western Sahara', 1),
(242, 'YE', 'Yemen', 1),
(243, 'ZR', 'Zaire', 1),
(244, 'ZM', 'Zambia', 1),
(245, 'ZW', 'Zimbabwe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_delete_files`
--

CREATE TABLE `tbl_delete_files` (
  `id` int(11) NOT NULL,
  `file_url` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_diary`
--

CREATE TABLE `tbl_diary` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT 'Who wrote this diary note',
  `subject_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `heading` text NOT NULL,
  `diary_note` longtext NOT NULL,
  `issue_date` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=wrote by teacher,2=wrote by parent',
  `teacher_read_msg` int(11) NOT NULL COMMENT '1=unread,2=read',
  `parent_read_msg` int(11) NOT NULL COMMENT '1=unread,2=read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_diary`
--

INSERT INTO `tbl_diary` (`id`, `school_id`, `teacher_id`, `subject_id`, `student_id`, `class_id`, `section_id`, `roll_no`, `heading`, `diary_note`, `issue_date`, `parent_id`, `status`, `teacher_read_msg`, `parent_read_msg`) VALUES
(1, 1, 8, 1, 2, 1, 2, 2, 'This is a message from teacher', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 1560431183, 0, 1, 2, 0),
(2, 1, 8, 2, 2, 1, 2, 2, 'This is a message from Parent', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.', 1560431241, 15, 2, 2, 2),
(3, 1, 8, 1, 1, 1, 2, 1, 'This is a message from teacher for 14-06-2019', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.', 1560487726, 0, 1, 1, 0),
(4, 1, 8, 2, 1, 1, 2, 1, 'This is a message from Parent on 14-06-2019', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.', 1560487780, 15, 2, 2, 2),
(5, 1, 8, 3, 1, 1, 2, 1, 'This a message FROM teacher for life sc. subject', 'lar belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of th', 1560489202, 0, 1, 2, 2),
(6, 1, 8, 4, 1, 1, 2, 1, 'This is a message FROM Parent for subject Life SC.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries', 1560489310, 15, 2, 2, 2),
(7, 22, 30, 16, 11, 7, 29, 3, 'Complain', 'Very disturbing and inattentive', 1560493032, 0, 1, 2, 1),
(8, 22, 30, 16, 17, 7, 29, 20, 'Complain', 'Very disturbing and inattentive', 1560493032, 0, 1, 2, 1),
(9, 22, 30, 16, 17, 7, 29, 20, 'Complain', 'Trying to improve', 1560494158, 0, 1, 2, 2),
(10, 22, 30, 22, 17, 7, 29, 20, 'General', 'Cool', 1560494285, 0, 1, 2, 1),
(11, 23, 35, 13, 8, 9, 15, 34, 'Dress', 'Jxjdhdhd', 1560496795, 0, 1, 2, 2),
(12, 23, 35, 11, 8, 9, 15, 34, 'Bxbxb', 'Bxhxb', 1560496959, 0, 1, 2, 2),
(13, 23, 35, 9, 8, 9, 15, 34, 'Test', 'Hdhdghdjdbdb', 1560497270, 26, 2, 2, 2),
(14, 23, 35, 11, 8, 9, 15, 34, 'The math calculation for ganesh is very poor', 'Please look into it, some practice will help. ', 1560497353, 0, 1, 2, 2),
(15, 23, 35, 12, 18, 9, 15, 5, 'Abc', '', 1560498048, 0, 1, 2, 1),
(16, 22, 30, 27, 11, 7, 29, 3, 'No Morality Gained', 'Disturb Disturb', 1560843707, 0, 1, 2, 1),
(17, 22, 30, 16, 11, 7, 29, 3, 'Maths', 'Good', 1560853642, 0, 1, 2, 1),
(18, 22, 30, 14, 17, 7, 29, 20, 'Test', 'Test', 1560854008, 44, 2, 2, 2),
(19, 22, 30, 16, 17, 7, 29, 20, 'Maths', 'Test', 1560854351, 0, 1, 2, 1),
(20, 22, 30, 16, 11, 7, 29, 3, 'Test', 'Test', 1560856882, 0, 1, 2, 1),
(21, 22, 30, 16, 17, 7, 29, 20, 'Test', 'Test', 1560856882, 0, 1, 2, 1),
(22, 22, 30, 16, 17, 7, 29, 20, 'Hello', 'Kemon achen madam', 1560943342, 44, 2, 2, 2),
(23, 22, 30, 16, 17, 7, 29, 20, 'Hello', 'Bhalo achi', 1560943413, 0, 1, 2, 1),
(24, 22, 30, 16, 17, 7, 29, 20, 'Ghhh', 'View hi hi h', 1560951854, 0, 1, 2, 2),
(25, 1, 4, 7, 1, 1, 2, 1, 'Diary 44', 'Diary 4', 1561012252, 15, 2, 1, 2),
(26, 1, 8, 3, 1, 1, 2, 1, 'Diary55', 'Diary5', 1561012355, 15, 2, 1, 2),
(27, 1, 8, 2, 1, 1, 2, 1, 'Diary66', 'Diary6', 1561012376, 15, 2, 1, 2),
(28, 1, 8, 4, 1, 1, 2, 1, 'Diary77', 'Diary7', 1561012394, 15, 2, 1, 2),
(29, 1, 8, 6, 1, 1, 2, 1, 'Diary88', 'Diary8', 1561012413, 15, 2, 1, 2),
(30, 1, 8, 8, 1, 1, 2, 1, 'Diary99', 'Diary9', 1561012428, 15, 2, 2, 2),
(31, 1, 8, 4, 1, 1, 2, 1, 'Diary10', 'Diary10', 1561012445, 15, 2, 2, 2),
(32, 1, 8, 3, 1, 1, 2, 1, 'Diary11', 'Diary11', 1561012465, 15, 2, 2, 2),
(33, 22, 30, 16, 17, 7, 29, 20, 'Hi', 'Test', 1561031783, 44, 2, 2, 2),
(34, 1, 8, 5, 1, 1, 2, 1, 'Gfygg', 'Ch high fgh', 1561034441, 0, 1, 2, 2),
(35, 1, 8, 0, 1, 1, 2, 1, 'Talkative ', 'Cjdjd\r\nDjjdjf', 1561187296, 0, 1, 2, 2),
(36, 1, 8, 0, 2, 1, 2, 2, 'Talkative ', 'Cjdjd\r\nDjjdjf', 1561187296, 0, 1, 2, 2),
(38, 1, 8, 0, 6, 1, 2, 22, 'Talkative ', 'Cjdjd\r\nDjjdjf', 1561187296, 0, 1, 2, 1),
(39, 1, 8, 0, 7, 1, 2, 36, 'Talkative ', 'Cjdjd\r\nDjjdjf', 1561187296, 0, 1, 2, 1),
(40, 1, 8, 0, 1, 1, 2, 1, 'Ok', '', 1561187550, 16, 2, 2, 2),
(41, 22, 30, 25, 17, 7, 29, 20, 'Hello', 'Not interested', 1561550320, 0, 1, 2, 1),
(42, 22, 30, 16, 10, 7, 29, 2, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(43, 22, 30, 16, 11, 7, 29, 3, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(44, 22, 30, 16, 12, 7, 29, 5, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(45, 22, 30, 16, 13, 7, 29, 7, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(46, 22, 30, 16, 14, 7, 29, 10, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(47, 22, 30, 16, 15, 7, 29, 12, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(48, 22, 30, 16, 17, 7, 29, 20, 'Good morning ', 'Good behaviour', 1561631113, 0, 1, 2, 1),
(49, 22, 30, 22, 17, 7, 29, 20, 'Good morning ', 'Thanks', 1561632519, 44, 2, 2, 2),
(50, 23, 35, 11, 8, 9, 15, 34, 'Ok, we will rectify it.', '', 1561700966, 26, 2, 1, 2),
(51, 23, 35, 10, 8, 9, 15, 34, 'This is a heading', 'Ocihchhoc. Hi ih hi hi hi hi h hkchchcho h oho oh h ho jovuvjojvohvhovhcg. H h oh ho oh hi ho oh', 1561701035, 26, 2, 1, 2),
(52, 1, 8, 1, 1, 1, 2, 1, 'This is a message from teacher for english', 'hello hello hello hello hello hello hello hello hello hello hello ', 1561793364, 0, 1, 2, 2),
(53, 1, 8, 1, 1, 1, 2, 1, 'Thanks message', 'Thank you very much for informing us.', 1561794886, 16, 2, 2, 2),
(54, 1, 8, 1, 1, 1, 2, 1, 'Message 1 from teacher', 'Hello, This is message 1', 1561968113, 16, 1, 2, 2),
(55, 1, 8, 1, 1, 1, 2, 1, 'Message 2 from parent', 'Hello, this is message from parent 2', 1561968166, 16, 2, 2, 2),
(56, 1, 8, 1, 1, 1, 2, 1, 'Message 3 from teacher', 'msg 3', 1561968305, 16, 1, 2, 2),
(57, 1, 8, 1, 1, 1, 2, 1, 'message 4 from parent', 'msg4', 1561968329, 16, 2, 2, 2),
(58, 22, 30, 17, 17, 7, 29, 20, 'Notes ', 'Notes testing', 1562053683, 44, 2, 2, 2),
(59, 22, 30, 18, 17, 7, 29, 20, 'test', 'testing', 1562154739, 44, 1, 2, 1),
(60, 22, 30, 25, 17, 7, 29, 20, 'Inconsistent performance', 'Good at theory but not good at practical.', 1562670048, 44, 1, 2, 1),
(61, 22, 30, 16, 17, 7, 29, 20, 'Notes', 'Test notes', 1562670109, 44, 1, 2, 2),
(62, 22, 30, 16, 17, 7, 29, 20, 'Checked', 'Noted and will take care ', 1562676315, 44, 2, 1, 2),
(63, 1, 4, 1, 1, 1, 2, 1, 'Diary 1', 'Diary 1', 1563270102, 82, 2, 1, 2),
(64, 1, 10, 2, 1, 1, 2, 1, 'Diary 6', 'Diary 6', 1563270144, 82, 2, 1, 2),
(65, 1, 4, 3, 1, 1, 2, 1, 'Diary 7', 'Diary 7', 1563270162, 82, 2, 1, 2),
(66, 1, 8, 3, 1, 1, 2, 1, 'Diary 8', 'Diary 8', 1563270180, 82, 2, 1, 2),
(67, 1, 8, 5, 1, 1, 2, 1, 'Diary 9', 'Diary 9', 1563270202, 82, 2, 1, 2),
(68, 1, 4, 7, 1, 1, 2, 1, 'Diary 10', 'Diary 10', 1563270215, 82, 2, 1, 2),
(69, 1, 8, 2, 1, 1, 2, 1, 'Diary 11', 'Diary 11', 1563270229, 82, 2, 1, 2),
(70, 1, 4, 2, 1, 1, 2, 1, 'Diary 12', 'Diary 12', 1563270472, 82, 2, 1, 2),
(71, 1, 8, 2, 2, 1, 2, 2, 'Diary 2', 'Diary 2', 1563270614, 15, 2, 1, 2),
(72, 1, 4, 2, 2, 1, 2, 2, 'Diary 3', 'Diary 3', 1563270634, 15, 2, 1, 2),
(73, 1, 8, 3, 2, 1, 2, 2, 'Diary 4', 'Diary 4', 1563270650, 15, 2, 1, 2),
(74, 1, 4, 6, 2, 1, 2, 2, 'Diary 5', 'Diary 5', 1563270665, 15, 2, 1, 2),
(75, 1, 8, 7, 2, 1, 2, 2, 'Diary 6', 'Diary 6', 1563270678, 15, 2, 1, 2),
(76, 1, 4, 1, 2, 1, 2, 2, 'Diary 7', 'Diary 7', 1563270690, 15, 2, 1, 2),
(77, 1, 8, 8, 2, 1, 2, 2, 'Diary 8', 'Diary 8', 1563270704, 15, 2, 1, 2),
(78, 1, 4, 2, 2, 1, 2, 2, 'Diary 9', 'Diary 9', 1563270725, 15, 2, 1, 2),
(79, 1, 4, 1, 2, 1, 2, 2, 'Diary 10', 'Diary 10', 1563270739, 15, 2, 1, 2),
(80, 1, 8, 4, 2, 1, 2, 2, 'Diary 11', 'Diary 11', 1563270752, 15, 2, 1, 2),
(81, 1, 8, 1, 1, 1, 2, 1, 'Meeting', 'Test Diary', 1564641658, 92, 1, 2, 2),
(82, 1, 8, 1, 2, 1, 2, 2, 'Meeting', 'Test Diary', 1564641659, 15, 1, 2, 1),
(83, 1, 8, 1, 6, 1, 2, 22, 'Meeting', 'Test Diary', 1564641659, 20, 1, 2, 1),
(84, 1, 8, 1, 7, 1, 2, 36, 'Meeting', 'Test Diary', 1564641659, 21, 1, 2, 1),
(85, 1, 8, 1, 20, 1, 2, 19950216, 'Meeting', 'Test Diary', 1564641659, 61, 1, 2, 1),
(86, 1, 8, 1, 21, 1, 2, 225, 'Meeting', 'Test Diary', 1564641659, 0, 1, 2, 1),
(87, 1, 8, 1, 22, 1, 2, 225, 'Meeting', 'Test Diary', 1564641659, 16, 1, 2, 1),
(88, 1, 8, 1, 23, 1, 2, 458, 'Meeting', 'Test Diary', 1564641659, 63, 1, 2, 1),
(89, 1, 8, 1, 24, 1, 2, 55, 'Meeting', 'Test Diary', 1564641659, 63, 1, 2, 1),
(90, 1, 8, 1, 25, 1, 2, 1, 'Meeting', 'Test Diary', 1564641659, 0, 1, 2, 1),
(91, 1, 8, 1, 1, 1, 2, 1, 'Ok Thanks', 'This is a thanks message', 1564641731, 15, 2, 2, 2),
(92, 1, 8, 2, 1, 1, 2, 1, 'Bengali diary', 'test bengali diary', 1564641860, 92, 1, 2, 2),
(93, 1, 8, 2, 2, 1, 2, 2, 'Bengali diary', 'test bengali diary', 1564641860, 15, 1, 2, 1),
(94, 1, 8, 0, 1, 1, 2, 1, 'Other Diary', 'Test other diary', 1567764966, 92, 1, 2, 2),
(95, 1, 8, 0, 1, 1, 2, 1, 'RE:Other Diary', 'Of course other diary it is.', 1567767845, 15, 2, 1, 2),
(96, 1, 8, 1, 1, 1, 2, 1, 'Give more focus on English subject', 'Give more focus on English subject Give more focus on English subject', 1568447848, 92, 1, 2, 1),
(97, 1, 8, 2, 1, 1, 2, 1, 'Testr', 'Bengali subject test', 1568447922, 92, 1, 2, 1),
(98, 1, 8, 3, 1, 1, 2, 1, 'test life', 'this is a test life science ', 1568448097, 92, 1, 2, 1),
(99, 1, 8, 2, 1, 1, 2, 1, 'afsasfszdfv', 'sdzfgvsdg sdfg sgsfg dsf gdsfg dsdsgdszg hdszg', 1568448132, 92, 1, 2, 1),
(100, 1, 8, 2, 1, 1, 2, 1, 'afsasfszdfv', 'sdzfgvsdg sdfg sgsfg dsf gdsfg dsdsgdszg hdszg', 1568448184, 92, 1, 2, 1),
(101, 1, 8, 2, 1, 1, 2, 1, 'afsasfszdfv', 'sdzfgvsdg sdfg sgsfg dsf gdsfg dsdsgdszg hdszg', 1568448256, 92, 1, 2, 1),
(102, 1, 8, 2, 1, 1, 2, 1, 'afsasfszdfv', 'sdzfgvsdg sdfg sgsfg dsf gdsfg dsdsgdszg hdszg', 1568448296, 92, 1, 2, 1),
(103, 1, 8, 2, 1, 1, 2, 1, 'afsasfszdfv', 'sdzfgvsdg sdfg sgsfg dsf gdsfg dsdsgdszg hdszg', 1568450483, 92, 1, 2, 2),
(104, 1, 8, 1, 1, 1, 2, 1, 'This is a message from teacher', 'Hello this is a test message', 1568451150, 92, 1, 2, 1),
(105, 1, 8, 1, 2, 1, 2, 2, 'This is a message from teacher', 'Hello this is a test message', 1568451163, 15, 1, 2, 1),
(106, 1, 8, 1, 6, 1, 2, 22, 'This is a message from teacher', 'Hello this is a test message', 1568451175, 20, 1, 2, 1),
(107, 1, 8, 1, 7, 1, 2, 36, 'This is a message from teacher', 'Hello this is a test message', 1568451175, 21, 1, 2, 1),
(108, 1, 8, 1, 20, 1, 2, 19950216, 'This is a message from teacher', 'Hello this is a test message', 1568451175, 61, 1, 2, 1),
(109, 1, 8, 1, 21, 1, 2, 225, 'This is a message from teacher', 'Hello this is a test message', 1568451175, 0, 1, 2, 1),
(110, 1, 8, 1, 22, 1, 2, 225, 'This is a message from teacher', 'Hello this is a test message', 1568451175, 16, 1, 2, 1),
(111, 1, 8, 1, 23, 1, 2, 458, 'This is a message from teacher', 'Hello this is a test message', 1568451175, 63, 1, 2, 1),
(112, 1, 8, 1, 24, 1, 2, 55, 'This is a message from teacher', 'Hello this is a test message', 1568451181, 63, 1, 2, 1),
(113, 1, 8, 1, 25, 1, 2, 1, 'This is a message from teacher', 'Hello this is a test message', 1568451187, 0, 1, 2, 1),
(114, 1, 8, 1, 30, 1, 2, 12, 'This is a message from teacher', 'Hello this is a test message', 1568451187, 94, 1, 2, 1),
(115, 1, 8, 1, 34, 1, 2, 56, 'This is a message from teacher', 'Hello this is a test message', 1568451187, 102, 1, 2, 1),
(116, 1, 8, 2, 1, 1, 2, 1, 'RE:afsasfszdfv', 'This is a test message from parent to teacher biplob mudi', 1568452065, 15, 2, 1, 2),
(117, 1, 8, 2, 1, 1, 2, 1, 'RE:afsasfszdfv', 'This is another test message from Biplob Mudi.', 1568452195, 15, 2, 2, 2),
(118, 1, 8, 1, 1, 1, 2, 1, 'English', 'English', 1569646622, 92, 1, 2, 1),
(119, 1, 8, 2, 1, 1, 2, 1, 'bengali', 'Bengali', 1569646870, 92, 1, 2, 1),
(120, 1, 8, 1, 1, 1, 2, 1, 'hello1', '', 1569648054, 15, 2, 1, 2),
(121, 1, 8, 1, 1, 1, 2, 1, 'Hello2', '', 1569648314, 15, 2, 1, 2),
(122, 1, 8, 3, 1, 1, 2, 1, 'Hello3', '', 1569648349, 15, 2, 1, 2),
(123, 1, 8, 4, 1, 1, 2, 1, 'Hello 4', '', 1569648363, 15, 2, 1, 2),
(124, 1, 8, 6, 1, 1, 2, 1, 'get ready', '', 1569648503, 92, 1, 2, 1),
(125, 169, 170, 47, 41, 28, 61, 1, 'About geography Subject', 'Home work not done properly', 1571315277, 171, 2, 2, 2),
(126, 169, 170, 47, 41, 28, 61, 1, 'The diary is sending from parent panel to teacher Anirban', 'This is a diary for testing purpose', 1571315634, 171, 2, 1, 2),
(127, 169, 170, 47, 41, 28, 61, 1, 'Test Purpose', 'This is a test email', 1571315781, 171, 2, 2, 2),
(128, 169, 170, 47, 41, 28, 61, 1, 'I am receiving your diary copy ', 'I will look after this matter', 1571317207, 171, 1, 2, 1),
(129, 169, 170, 47, 41, 28, 61, 1, 'This is a message from teacher', 'Test', 1571385384, 171, 1, 2, 1),
(130, 169, 170, 47, 41, 28, 61, 1, 'This is a message from teacher', 'test', 1571385419, 171, 1, 2, 2),
(131, 169, 170, 47, 41, 28, 61, 1, 'RE:This is a message from teacher', 'Hello test', 1571385592, 171, 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam`
--

CREATE TABLE `tbl_exam` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `exam_name` varchar(255) NOT NULL,
  `exam_display_name` varchar(255) NOT NULL,
  `exam_start_date` date NOT NULL,
  `exam_end_date` date NOT NULL,
  `publish_date` int(11) NOT NULL,
  `publish_status` tinyint(4) NOT NULL COMMENT '0=not published,1=published'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_exam`
--

INSERT INTO `tbl_exam` (`id`, `school_id`, `term_id`, `exam_name`, `exam_display_name`, `exam_start_date`, `exam_end_date`, `publish_date`, `publish_status`) VALUES
(2, 169, 1, 'Unit 1', 'Unit Test 1', '2019-10-25', '2019-10-30', 1572490800, 0),
(3, 169, 1, 'Unit 2', 'Unit Test 2', '2020-01-01', '2019-10-15', 1571194800, 0),
(4, 169, 1, 'unit2', 'unit-2', '2019-10-31', '2019-11-08', 1573273800, 0),
(5, 169, 2, 'unit1', 'unit-1', '2019-10-28', '2019-12-12', 1573101000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_scores`
--

CREATE TABLE `tbl_exam_scores` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `full_marks` int(11) NOT NULL,
  `pass_marks` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_exam_scores`
--

INSERT INTO `tbl_exam_scores` (`id`, `school_id`, `exam_id`, `subject_id`, `full_marks`, `pass_marks`, `sort_order`) VALUES
(1, 169, 2, 47, 50, 40, 0),
(2, 169, 2, 48, 60, 50, 0),
(3, 169, 2, 49, 70, 60, 0),
(4, 169, 2, 51, 100, 40, 0),
(5, 169, 2, 53, 100, 40, 0),
(6, 169, 3, 47, 50, 40, 0),
(7, 169, 3, 48, 60, 50, 0),
(8, 169, 3, 49, 70, 60, 0),
(9, 169, 3, 51, 100, 40, 0),
(10, 169, 3, 53, 100, 40, 0),
(11, 169, 3, 54, 100, 50, 0),
(12, 169, 4, 50, 20, 7, 0),
(13, 169, 4, 52, 20, 7, 0),
(14, 169, 4, 54, 20, 7, 0),
(15, 169, 5, 50, 20, 7, 0),
(16, 169, 5, 52, 20, 7, 0),
(17, 169, 5, 54, 20, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_payment`
--

CREATE TABLE `tbl_fees_payment` (
  `id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `fees_breakup_id` int(11) NOT NULL,
  `manual_fees_breakup_id` int(11) NOT NULL,
  `fees` longtext NOT NULL,
  `school_id` int(11) NOT NULL,
  `class_info` text NOT NULL,
  `student_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `fees_amount` float(20,2) NOT NULL,
  `late_fine` float(20,2) NOT NULL,
  `service_charges` float(20,2) NOT NULL,
  `total_amount` float(20,2) NOT NULL,
  `paid_amount` float(20,2) NOT NULL,
  `payment_datetime` datetime NOT NULL,
  `payment_status` tinyint(1) NOT NULL COMMENT '0 = Pending, 1 = Paid',
  `payment_ref_id` varchar(100) NOT NULL,
  `payment_gateway_id` int(11) NOT NULL,
  `created_by` tinyint(1) NOT NULL COMMENT '1 = School, 3 = Parent'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_fees_payment`
--

INSERT INTO `tbl_fees_payment` (`id`, `fees_id`, `fees_breakup_id`, `manual_fees_breakup_id`, `fees`, `school_id`, `class_info`, `student_id`, `parent_id`, `fees_amount`, `late_fine`, `service_charges`, `total_amount`, `paid_amount`, `payment_datetime`, `payment_status`, `payment_ref_id`, `payment_gateway_id`, `created_by`) VALUES
(1001, 2, 1, 1, '[{\"label\":\"Admission Fees\",\"amount\":\"12001\",\"option\":\"2\"},{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 310, 95, 16003.00, 160.00, 808.00, 16971.00, 16971.00, '2019-10-14 10:47:16', 1, 'pay_DTpdP49ynZdqOD', 1, 3),
(1002, 2, 3, 3, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 310, 95, 4002.00, 40.00, 0.00, 4042.00, 4042.00, '2019-10-14 11:05:48', 1, '', 2, 1),
(1003, 2, 1, 1, '[{\"label\":\"Admission Fees\",\"amount\":\"12001\",\"option\":\"2\"},{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 31, 95, 16003.00, 160.00, 0.00, 16163.00, 16163.00, '2019-10-14 13:15:41', 1, '', 2, 1),
(1004, 5, 9, 0, '[{\"label\":\"Admission fees\",\"amount\":\"2500\",\"option\":\"2\"},{\"label\":\"Tuition Fees\",\"amount\":\"12000\",\"option\":\"2\"},{\"label\":\"Computer Fees\",\"amount\":\"1000\",\"option\":\"1\"}]', 1, '{\"session\":\"November\'19-April\'20\",\"semester\":\"Java Summer Class (November\'19-April\'20)\",\"class\":\"Java Summer Class\",\"section\":\"A\",\"roll_no\":\"1\"}', 40, 44, 15500.00, 0.00, 775.00, 16275.00, 16275.00, '2019-10-15 16:16:09', 1, 'pay_DUJlzpr090o052', 1, 3),
(1005, 2, 3, 3, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"0\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 31, 95, 3001.00, 30.00, 151.00, 3182.00, 3182.00, '2019-10-17 15:11:24', 1, 'pay_DV5jidtomWvGNR', 1, 3),
(1006, 2, 2, 2, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 31, 95, 4002.00, 40.00, 202.00, 4244.00, 4244.00, '2019-10-17 15:38:43', 1, 'pay_DV6ChWmYt8O3k1', 1, 3),
(1007, 2, 4, 4, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 31, 95, 4002.00, 0.00, 0.00, 4002.00, 4002.00, '2019-10-17 15:40:21', 1, '', 2, 1),
(1008, 2, 5, 5, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 31, 95, 4002.00, 0.00, 0.00, 4002.00, 4002.00, '2019-10-17 15:40:32', 0, '', 2, 1),
(1009, 2, 6, 6, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"1\"}', 31, 95, 4002.00, 0.00, 0.00, 4002.00, 4002.00, '2019-10-17 15:40:39', 0, '', 2, 1),
(1010, 2, 1, 0, '[{\"label\":\"Admission Fees\",\"amount\":\"10000\",\"option\":\"2\"},{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"B\",\"roll_no\":\"1\"}', 1, 92, 14000.00, 140.00, 0.00, 14140.00, 14140.00, '2019-10-17 17:03:45', 1, '', 2, 1),
(1011, 2, 2, 0, '[{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]', 1, '{\"session\":\"2019-20\",\"semester\":\"STD 1 (2019-20)\",\"class\":\"STD 1\",\"section\":\"B\",\"roll_no\":\"1\"}', 1, 15, 4000.00, 40.00, 202.00, 4242.00, 4242.00, '2019-10-17 17:31:35', 1, 'pay_DV87vISmy0Vrjd', 1, 3),
(1012, 8, 31, 0, '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"0\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]', 178, '{\"session\":\"2019-2020\",\"semester\":\"STD 1 (2019-2020)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"25\"}', 46, 44, 1700.00, 0.00, 85.00, 1785.00, 1785.00, '2019-10-23 18:21:39', 1, 'pay_DXWBXrRSBya22F', 1, 3),
(1013, 8, 27, 0, '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"0\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]', 178, '{\"session\":\"2019-2020\",\"semester\":\"STD 1 (2019-2020)\",\"class\":\"STD 1\",\"section\":\"A\",\"roll_no\":\"25\"}', 46, 44, 1700.00, 85.00, 0.00, 1785.00, 1785.00, '2019-10-24 12:20:54', 1, '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_structure`
--

CREATE TABLE `tbl_fees_structure` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `cycle_type` int(11) NOT NULL COMMENT '1 = Monthly, 2 = Quarterly, 3 = Half Yearly, 4 = Yearly',
  `general_comment` longtext NOT NULL,
  `late_payment_setting` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Disabled, 1 = Enabled',
  `late_payment_day_limit` int(11) NOT NULL,
  `late_payment_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Fixed, 2 = Percentage',
  `late_payment_amount` float(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_fees_structure`
--

INSERT INTO `tbl_fees_structure` (`id`, `school_id`, `semester_id`, `cycle_type`, `general_comment`, `late_payment_setting`, `late_payment_day_limit`, `late_payment_type`, `late_payment_amount`) VALUES
(2, 1, 2, 1, 'Test', 1, 10, 2, 1.00),
(3, 1, 4, 2, 'Quarterly Fees', 0, 0, 1, 0.00),
(4, 1, 5, 1, '', 0, 0, 1, 0.00),
(5, 1, 9, 4, 'Pay within October', 0, 0, 1, 0.00),
(6, 22, 11, 1, '', 0, 0, 1, 0.00),
(7, 22, 14, 2, '', 0, 0, 1, 0.00),
(8, 178, 15, 1, '', 1, 10, 2, 5.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_structure_breakups`
--

CREATE TABLE `tbl_fees_structure_breakups` (
  `id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `month` date NOT NULL,
  `breakup_label` varchar(255) NOT NULL,
  `fees` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_fees_structure_breakups`
--

INSERT INTO `tbl_fees_structure_breakups` (`id`, `fees_id`, `month`, `breakup_label`, `fees`) VALUES
(1, 2, '2019-08-01', 'Fees For August, 2019', '[{\"label\":\"Admission Fees\",\"amount\":\"10000\",\"option\":\"2\"},{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(2, 2, '2019-09-01', 'Fees For September, 2019', '[{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(3, 2, '2019-10-01', 'Fees For October, 2019', '[{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(4, 2, '2019-11-01', 'Fees For November, 2019', '[{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(5, 2, '2019-12-01', 'Fees For December, 2019', '[{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(6, 2, '2020-01-01', 'Fees For January, 2020', '[{\"label\":\"Tution Fees\",\"amount\":\"3000\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(7, 3, '2019-08-01', 'Fees For August, 2019 -> October, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"Transportation Fees\",\"amount\":\"500,400,350,250\",\"option\":\"1\"},{\"label\":\"Admission Fees\",\"amount\":\"3000\",\"option\":\"2\"}]'),
(8, 3, '2019-11-01', 'Fees For November, 2019 -> January, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"Transportation Fees\",\"amount\":\"500,400,350,250\",\"option\":\"1\"}]'),
(9, 5, '2019-11-05', 'Fees For November, 2019 -> April, 2020', '[{\"label\":\"Admission fees\",\"amount\":\"2500\",\"option\":\"2\"},{\"label\":\"Tuition Fees\",\"amount\":\"12000\",\"option\":\"2\"},{\"label\":\"Computer Fees\",\"amount\":\"1000\",\"option\":\"1\"}]'),
(10, 6, '2019-07-24', 'Fees For July, 2019', '[{\"label\":\"Admission Fees\",\"amount\":\"4000\",\"option\":\"2\"},{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(11, 6, '2019-08-24', 'Fees For August, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(12, 6, '2019-09-24', 'Fees For September, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(13, 6, '2019-10-24', 'Fees For October, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(14, 6, '2019-11-24', 'Fees For November, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(15, 6, '2019-12-24', 'Fees For December, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(16, 6, '2020-01-24', 'Fees For January, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(17, 6, '2020-02-24', 'Fees For February, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(18, 6, '2020-03-24', 'Fees For March, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(19, 6, '2020-04-24', 'Fees For April, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(20, 6, '2020-05-24', 'Fees For May, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1500\",\"option\":\"2\"},{\"label\":\"computer fees\",\"amount\":\"500\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"250\",\"option\":\"2\"},{\"label\":\"sports fees\",\"amount\":\"300\",\"option\":\"2\"},{\"label\":\"transportation fees\",\"amount\":\"500,600,700,1000\",\"option\":\"1\"}]'),
(21, 7, '2019-12-16', 'Fees For December, 2019 -> February, 2020', '[{\"label\":\"tuition fees\",\"amount\":\"5000\",\"option\":\"2\"}]'),
(22, 7, '2020-03-16', 'Fees For March, 2020 -> May, 2020', '[{\"label\":\"tuition fees\",\"amount\":\"5000\",\"option\":\"2\"}]'),
(23, 7, '2020-06-16', 'Fees For June, 2020 -> August, 2020', '[{\"label\":\"tuition fees\",\"amount\":\"5000\",\"option\":\"2\"}]'),
(24, 8, '2019-03-06', 'Fees For March, 2019', '[{\"label\":\"Admission fees\",\"amount\":\"2000\",\"option\":\"2\"},{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(25, 8, '2019-04-06', 'Fees For April, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(26, 8, '2019-05-06', 'Fees For May, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(27, 8, '2019-06-06', 'Fees For June, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(28, 8, '2019-07-06', 'Fees For July, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(29, 8, '2019-08-06', 'Fees For August, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(30, 8, '2019-09-06', 'Fees For September, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(31, 8, '2019-10-06', 'Fees For October, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(32, 8, '2019-11-06', 'Fees For November, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(33, 8, '2019-12-06', 'Fees For December, 2019', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(34, 8, '2020-01-06', 'Fees For January, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"}]'),
(35, 8, '2020-02-06', 'Fees For February, 2020', '[{\"label\":\"Tuition Fees\",\"amount\":\"1200\",\"option\":\"2\"},{\"label\":\"transport fees\",\"amount\":\"500,750,800\",\"option\":\"1\"},{\"label\":\"library fees\",\"amount\":\"500\",\"option\":\"2\"},{\"label\":\"Exam fees\",\"amount\":\"1500\",\"option\":\"2\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_structure_breakups_manual`
--

CREATE TABLE `tbl_fees_structure_breakups_manual` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `breakup_id` int(11) NOT NULL,
  `fees` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_fees_structure_breakups_manual`
--

INSERT INTO `tbl_fees_structure_breakups_manual` (`id`, `student_id`, `semester_id`, `breakup_id`, `fees`) VALUES
(1, 31, 2, 1, '[{\"label\":\"Admission Fees\",\"amount\":\"12001\",\"option\":\"2\"},{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]'),
(2, 31, 2, 2, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]'),
(3, 31, 2, 3, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]'),
(4, 31, 2, 4, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]'),
(5, 31, 2, 5, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]'),
(6, 31, 2, 6, '[{\"label\":\"Tution Fees\",\"amount\":\"3001\",\"option\":\"2\"},{\"label\":\"Library Fees\",\"amount\":\"1001\",\"option\":\"1\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_info`
--

CREATE TABLE `tbl_general_info` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `info_text` longtext NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_general_info`
--

INSERT INTO `tbl_general_info` (`id`, `school_id`, `info_text`, `status`) VALUES
(1, 1, '<p>The school rules have been established in partnership with the community over a long period of time. They reflect the school community&rsquo;s expectations in terms of acceptable standards of behaviour, dress and personal presentation in the widest sense. Students are expected to follow the school rules at all times when on the school grounds, representing the school, attending a school activity or when clearly associated with the school i.e. when wearing school uniform.</p><p><strong>Students have the responsibility:</strong></p><ul><li>To attend school regularly</li><li>To respect the right of others to learn</li><li>To respect their peers and teachers regardless of ethnicity, religion or gender</li><li>To respect the property and equipment of the school and others</li><li>To carry out reasonable instructions to the best of their ability</li><li>To conduct themselves in a courteous and appropriate manner in school and in public</li><li>To keep the school environment and the local community free from litter</li><li>To observe the uniform code of the school</li><li>To read all school notices and bring them to their parents&rsquo;/guardians&rsquo; attention</li></ul><p><strong>A. GENERAL CONDUCT</strong></p><p>Students are representatives of our school from leaving home until &nbsp;they return and are thus expected to set themselves a high standard of &nbsp;behaviour both inside and outside the school.</p><p><strong>B. THE SCHOOL UNIFORM and GROOMING</strong></p><p>should be worn tidily and correctly both at school and between home &nbsp;and school. The full school uniform must be worn at all times. Shirts &nbsp;are to be tucked in; socks are to be pulled up; heel straps in place. Where a situation arises concerning a student&rsquo;s uniform, written &nbsp;requests for temporary wearing of non-regulation items must be referred to a Dean or Deputy Principal.</p><p>Students are to be clean-shaven at all times while representing the school.</p><p>No visible jewellery is to be worn. Jewellery of religious or &nbsp;cultural significance may be worn but must be covered at all times.&nbsp;<br />No piercings are allowed. In particular, clear plastic studs, or otherwise, used to maintain the piercing, are not allowed.</p><p>A student&rsquo;s hair must be kept clean and tidy at all times. The length of the hair should not be shorter than a &ldquo;number 2&rdquo; razor cut. Hair should not be touching the shirt collar and should be off the face at all times. The style of the hair should not be extreme, including but not limited to mohawk, shaved styles and/or patterns, hair tied up and braided. The colour must be the student&rsquo;s own natural colour; no dye nor highlights are allowed. No hair ties, hair clips or hair accessories are to be used.</p><p>Make-up must not be worn. Students are not permitted to have visible tattoos.</p><p><strong>C. The following are not to be brought on to the school grounds:</strong></p><p>Alcohol or drugs in any form<br />Chemicals<br />Cigarettes or tobacco<br />Knives or other weapons, including BB guns<br />Matches/lighters/explosive or dangerous material<br />Pornographic or any other offensive material<br />Cameras<br />Skateboards or scooters or similar<br />Expensive bicycles or bicycle accessories or other costly equipment<br />Mobile Phones</p><p>Mobile phones will be permitted in school.<br />Students will be able to use them on the way to and from school.<br />Students will be able to use them in class as learning devices with &nbsp;the express permission of the teacher. Such permission must be sought and received before their use.<br />Students will not be permitted to use mobile phones in the school grounds, or during Form Period.<br />If a student uses a mobile phone inappropriately, or in contravention of the school&rsquo;s policy, he will be sanctioned accordingly.</p><p>Devices (Laptops / Notebooks / Tablets)</p><p>Devices will be permitted in school.<br />Students will be able to use them in class as learning devices with the express permission of the teacher.<br />Students will not be permitted to use devices in the school grounds during Interval and Lunch.<br />If a student uses a device inappropriately, or in contravention of &nbsp;the school&rsquo;s policy, he will be sanctioned accordingly. Devices will not be confiscated except in circumstances where a member of staff considers the device to be a harmful or disruptive influence.</p><p>The security and protection, within reason, of the device will be the responsibility of the student.</p><p><strong>D. The following are strictly forbidden on school property when students are under school discipline or attending a school function:</strong></p><p>smoking and the use of alcohol or other drugs, or being in the company of those who are smoking or using alcohol or drugs.<br />distributing of literature or any form of written matter (without the prior permission of the Headmaster or Deputy Headmasters).<br />E. Cyber Safety</p><p>There are times when inappropriate use of social media is brought to our attention. Students are expected to read the School&rsquo;s Cyber Safety Agreement and abide by the conditions contained in the Agreement.</p><p>F. BULLYING. Any form, verbal or physical, or any means of bullying or victimisation is strictly forbidden.</p><p>G. LITTER. It is the responsibility of every student &nbsp;in the school to ensure that the school is free from litter. Students are also encouraged to take the responsibility for litter in their immediate vicinity and place any litter in the many bins placed around &nbsp;the school.</p><p>H. EXEAT PASSES. Students are required to attend all classes and remain in the school grounds. No student may leave the school grounds during interval or lunch without prior permission. This includes collecting books/gear from cars.&nbsp;<br />When leaving school during the day to attend dental/medical or similar appointments, the boys must sign in and out at the Attendance Office or to see a Dean.</p><p>I. LEAVE APPLICATIONS. Parents, please consider &nbsp;carefully the effect on learning and the consequences of asking for leave during school time. The process of applying for leave is for parents/guardians to write to the Headmaster well in advance, notifying &nbsp;dates and the reason for absence. Each application is considered fully &nbsp;by the student&rsquo;s teachers and Deans, before being granted or otherwise by the Headmaster.</p><p>J. LATES. All boys must be at school by 8.45am. Latecomers must report to the Attendance Office before going to class. The student will only be admitted to class by the teacher upon &nbsp;presentation of a &rdquo;Late Pass&rdquo; from the Attendance Officer.</p><p>K. ABSENCES. If you know your son is going to be &nbsp;absent for any reason please call the Attendance Office and leave a &nbsp;message with student&rsquo;s name, form class, contact number and reason for the absence. After an absence from school a boy must bring, on the day &nbsp;of return, a note signed by his parents/guardian, stating the reason for the absence and take it to the Attendance Officer. In the event of the &nbsp;absence exceeding 2 days, the school should be informed whilst the &nbsp;student is absent from school.</p><p>L. All articles of clothing, shoes, books, bags and other personal property should be clearly marked with the owner&rsquo;s name.<br />No responsibility can be accepted for unmarked property. Boys &nbsp;losing or finding lost property should in the first instance report to &nbsp;Student Services.</p><p>M. BICYCLES are to be kept in the racks provided in the enclosure behind N2 and N3. This is locked during the school day &nbsp;but boys are strongly advised to secure their bikes with a lock. Basic Traffic Department road safety rules must be obeyed to and from school. Bicycles may not be ridden in school grounds. It is compulsory for all boys riding bicycles to school to wear helmets.</p><p>N. MOTOR VEHICLES. Boys wishing to bring motor &nbsp;vehicles or motor cycles to school must follow basic Traffic Department &nbsp;road safety rules. Motor vehicles may not be parked on school grounds &nbsp;and are not to be used during the school day without the prior &nbsp;permission of one of the Deputy Headmasters.</p><p>O. DAMAGE of any kind must be reported immediately to a Deputy Principal or Dean. In the case of wilful damage or negligence, boys will be expected to contribute part or all of the cost &nbsp;of repair.</p>', 1),
(2, 29, '<p>The European Toolkit for Schools offers concrete ideas for improving collaboration within, between and beyond schools with a view to enabling all children and young people to succeed in school. School leaders, teachers, parents and other people involved in different aspects of school life can find helpful information, examples of measures and resource material to inspire their efforts in providing effective and high-quality early childhood and school education. The aim of the Toolkit is to support the&nbsp;<a href=\"https://www.schooleducationgateway.eu/en/pub/resources/toolkitsforschools/general/transferability.htm\">exchange and experience</a>among school practitioners and policy makers.</p>', 1),
(3, 22, '<p>1. The office will remain open until 7 pm every business day.</p><p>The European Toolkit for Schools offers concrete ideas for improving collaboration within, between and beyond schools with a view to enabling all children and young people to succeed in school. School leaders, teachers, parents and other people involved in different aspects of school life can find helpful information, examples of measures and resource material to inspire their efforts in providing effective and high-quality early childhood and school education. The aim of the Toolkit is to support the&nbsp;<a href=\"https://www.schooleducationgateway.eu/en/pub/resources/toolkitsforschools/general/transferability.htm\">exchange and </a>experience among&nbsp;school practitioners and policymakers.</p>', 1),
(4, 23, '<p>Thank you for your interest in Delhi public School, Durgapur.<br />Please read the following Rules and Regulations properly before filling up the Registration Form.</p><ol><li>Please fill up the Registration Form in Capital Letters.</li><li>It is mandatory that Parents should fill the Regstration Form themselves.</li><li>Age criteria should be strictly maintained. In any case, if the child&rsquo;s age does not match with the admission criteria of the class, the management holds the right to cancel the Registration Form (Refer&nbsp;Age Criteria).</li><li>Attach one photocopy (Self attested by parents) of the child&rsquo;s Birth Certificate issued by Municipal Corporation or proof of birth shown in the Passport. Do not enclose Birth Certificate issued by Nursing Home / Hospital.</li><li>Paste four latest identical passport size coloured photographs of the child on Registration Form, Student Admit Card I, Student Admit Card II and Parent Card. Also paste latest passport size coloured photograph of parents or local / legal guardian on the Registration Form.</li><li>The registration Form, duly filled-up and complete in all respect, should be submitted in person to the School Office. The parents / (local/legal) guardians are requested to bring Original Money Receipt (issued at the time of purchase of Registration Form) at the time of submission of Registration Form.</li><li>Date of assessment / observation / interaction / interview will be given at the time of registration or intimated to the parents later on. The same may be changed, without prior notice, by the School.</li><li>Registration does not imply admission, which is subject to assessment procedure and availability of seats.</li><li>Applications must be complete and information mentioned in the Registration Form should be true. In case of any discrepancy with the facts, management holds the right to cancel the Registration Form as well as the admission of the child.</li></ol>', 1),
(5, 23, '<p>The School was founded in 1953 by Miss Barth at 18 Lee Road, Kolkata. In 1970, under West Bengal Society&rsquo;s Regulation Act, Calcutta International School was formed. In 2006, CIS shifted to its sprawling new campus at Anandapur, Eastern Metropolitan Bypass, Kolkata.</p><p>Originally a small institution exclusively for the children of the expatriate British community, the school evolved to become a highly reputed institution providing academic and extra mural education under Miss Barth. She was decorated with an OBE for her relentless dedication to the institution.</p><p>Today, CIS is a school with pupils from 17 nationalities who have come from different parts of the world as well as the local community. Priority is given to the children of expatriates, foreign passport holders and NRIs.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_holiday`
--

CREATE TABLE `tbl_holiday` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` longtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `office_open` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_holiday`
--

INSERT INTO `tbl_holiday` (`id`, `school_id`, `title`, `description`, `start_date`, `end_date`, `office_open`, `status`) VALUES
(1, 1, 'Short Vacation', '', '2019-05-25', '2019-05-27', 1, 1),
(2, 1, 'Foundation Day', '', '2019-07-12', '0000-00-00', 2, 1),
(3, 1, 'Special Vacation day', '', '2019-07-12', '0000-00-00', 1, 1),
(4, 23, '1st January', '', '2019-01-01', '2019-01-01', 2, 1),
(5, 23, 'Independence Day', '', '2019-08-15', '2019-08-15', 2, 1),
(6, 23, 'Summer Holiday\'s', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem</p>', '2019-05-18', '2019-06-14', 1, 1),
(7, 29, 'May Day', '', '2019-05-01', '0000-00-00', 2, 1),
(8, 29, 'Durga Puja', '', '2019-10-10', '2019-10-13', 2, 1),
(9, 22, 'Independence Day', '', '2019-08-15', '2019-08-15', 2, 1),
(10, 1, 'International Holiday', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>', '2019-05-25', '0000-00-00', 1, 1),
(11, 1, 'World Blood doner day', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>', '2019-06-14', '0000-00-00', 1, 1),
(13, 1, 'test holiday', '<p>test holiday</p>', '2019-07-12', '0000-00-00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_note`
--

CREATE TABLE `tbl_note` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=cw,2=hw,3=aw',
  `school_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `topic_name` text NOT NULL,
  `classnote_text` longtext NOT NULL,
  `note_issuetime` int(11) NOT NULL,
  `publish_data` int(11) NOT NULL COMMENT '0=not published, 1=published',
  `is_deleted` int(11) NOT NULL COMMENT '0=not deleted,1=deleted',
  `delete_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_note`
--

INSERT INTO `tbl_note` (`id`, `type`, `school_id`, `class_id`, `section_id`, `period_id`, `topic_name`, `classnote_text`, `note_issuetime`, `publish_data`, `is_deleted`, `delete_time`) VALUES
(1, 0, 1, 1, 2, 39, '', '', 1560492311, 0, 0, '0000-00-00 00:00:00'),
(2, 0, 1, 1, 2, 53, 'Ecosystem', 'An ecosystem is a community of living organisms in conjunction with the nonliving components of their environment, interacting as a system. These biotic and abiotic components are linked together through nutrient cycles and energy flows.', 1559130432, 0, 0, '0000-00-00 00:00:00'),
(3, 0, 1, 1, 2, 54, 'Class for 13th jume 2019', 'this is a class for 13th june', 1560409277, 1, 0, '0000-00-00 00:00:00'),
(4, 0, 1, 1, 2, 61, 'Hello', 'This is for testing purpose', 1559224342, 0, 0, '0000-00-00 00:00:00'),
(5, 0, 1, 1, 2, 57, 'asdad', 'SDaSDaDS', 1559223974, 0, 0, '0000-00-00 00:00:00'),
(6, 0, 1, 1, 2, 58, '', '', 1559224698, 0, 0, '0000-00-00 00:00:00'),
(8, 0, 1, 1, 2, 66, '', '', 1560492465, 1, 0, '0000-00-00 00:00:00'),
(9, 0, 1, 1, 2, 68, 'Homework Help With the Digestive System of a Frog Including a Downloadable Diagram', 'A baby frog, or a tadpole, feeds on plants such as algae and plankton. The adult frog, however, is a carnivore, eating mainly insects such as spiders, slugs, worms, or any other moving or living thing that will fit into its mouth.\r\n\r\nFor this reason, gardeners love frogs for their pest control abilities. Larger frogs can eat small animals such as rats, baby chickens or ducks. It is necessary to understand what an adult frog eats to be able to understand its digestive system, its organs, and why it works the way it does.', 1559903559, 0, 0, '0000-00-00 00:00:00'),
(10, 0, 1, 1, 1, 51, '', '', 1559997311, 0, 0, '0000-00-00 00:00:00'),
(11, 0, 1, 1, 2, 1, 'Grammar', 'Introduction with English grammar', 1560159028, 1, 0, '0000-00-00 00:00:00'),
(12, 0, 1, 1, 2, 9, 'Sound', 'vibrations that travel through the air or another medium and can be heard when they reach a person\'s or animal\'s ear.', 1560168163, 1, 0, '0000-00-00 00:00:00'),
(13, 0, 22, 7, 29, 103, 'test_cw', 'test', 1560175492, 0, 0, '0000-00-00 00:00:00'),
(14, 0, 22, 7, 29, 111, 'New work', 'Multiplication', 1560493816, 1, 0, '0000-00-00 00:00:00'),
(15, 0, 23, 9, 15, 115, 'Er class w', 'Fgvbb', 1560495843, 1, 0, '0000-00-00 00:00:00'),
(16, 0, 23, 9, 15, 93, 'Environment science ', '', 1560496413, 1, 0, '0000-00-00 00:00:00'),
(17, 0, 1, 1, 2, 1, 'Testr topic', 'This is a test topic', 1560774393, 1, 0, '0000-00-00 00:00:00'),
(18, 0, 22, 7, 29, 105, 'Test', 'Test', 1560855179, 1, 0, '0000-00-00 00:00:00'),
(19, 0, 22, 7, 29, 126, 'A test file', 'Testing testing testing testing', 1561029962, 0, 0, '0000-00-00 00:00:00'),
(20, 0, 1, 1, 2, 66, '', '', 1561103421, 0, 0, '0000-00-00 00:00:00'),
(21, 0, 1, 1, 2, 39, '', '', 1561103802, 1, 0, '0000-00-00 00:00:00'),
(22, 0, 22, 7, 29, 100, 'Maths1', 'Algebra', 1561115847, 0, 0, '0000-00-00 00:00:00'),
(23, 0, 1, 1, 2, 67, 'BEngali class', 'ben class', 1561102260, 1, 0, '0000-00-00 00:00:00'),
(24, 0, 1, 1, 2, 9, 'Past Work on 17th Physics', ' Past work on physics', 1561102260, 1, 0, '0000-00-00 00:00:00'),
(25, 0, 22, 12, 31, 129, 'Test', 'Ttttttttttttt', 1561102260, 1, 0, '0000-00-00 00:00:00'),
(26, 0, 22, 7, 29, 108, 'Test past', 'Testing past dates entry', 1561102260, 1, 0, '0000-00-00 00:00:00'),
(27, 0, 1, 1, 2, 68, '21st june  life sc class work', 'test', 1561102260, 0, 0, '0000-00-00 00:00:00'),
(28, 0, 1, 1, 2, 71, '21st geo class', 'test', 1561120835, 1, 0, '0000-00-00 00:00:00'),
(29, 0, 1, 1, 2, 65, '19th physics class work', 'test class woek', 1561120944, 1, 0, '0000-00-00 00:00:00'),
(30, 0, 1, 1, 2, 54, '20th math class work', '20th math class work', 1561035733, 1, 0, '0000-00-00 00:00:00'),
(31, 0, 22, 12, 31, 122, 'Test', 'Past work entry', 1560950008, 1, 0, '0000-00-00 00:00:00'),
(32, 0, 1, 1, 2, 74, '   Vfdc', 'Cvvb', 1561188640, 1, 0, '0000-00-00 00:00:00'),
(33, 0, 22, 7, 29, 128, '', '', 1561031934, 1, 0, '0000-00-00 00:00:00'),
(34, 0, 22, 7, 29, 117, 'Test', 'Notes test', 1561529688, 1, 0, '0000-00-00 00:00:00'),
(35, 0, 1, 1, 2, 53, 'Class work for 26th june of english sub', 'Class work for 26th june of english sub', 1561529697, 0, 0, '0000-00-00 00:00:00'),
(36, 0, 1, 1, 2, 1, 'Class for english on 24th jun 1B', 'Test', 1561379956, 0, 0, '0000-00-00 00:00:00'),
(37, 0, 22, 7, 29, 113, 'Introduction to Multiplication', 'Basic Maths Book-1, Chapter 2, Page no. 25 to 28', 1561716812, 1, 0, '0000-00-00 00:00:00'),
(38, 0, 22, 7, 29, 112, 'Introduction to Multiplication', 'Basic Maths Book-1, Chapter-4, Pages 25-28', 1561716950, 1, 0, '0000-00-00 00:00:00'),
(39, 0, 1, 1, 2, 66, '28th jun english for 1B', 'Test english class', 1561719122, 0, 0, '0000-00-00 00:00:00'),
(40, 0, 22, 7, 29, 126, 'Boolean algebra', 'Pages 11-15', 1561619939, 1, 0, '0000-00-00 00:00:00'),
(41, 0, 1, 1, 2, 74, '', '', 1561793199, 1, 0, '0000-00-00 00:00:00'),
(42, 0, 22, 7, 29, 117, '', '', 1562154512, 1, 0, '0000-00-00 00:00:00'),
(43, 0, 1, 1, 2, 53, 'Class work', '', 1562160261, 1, 0, '0000-00-00 00:00:00'),
(44, 0, 1, 1, 2, 54, 'CcW', 'Ffryff', 1562220994, 1, 0, '0000-00-00 00:00:00'),
(45, 0, 22, 7, 29, 108, 'New topic', 'Class Notes', 1562070279, 1, 0, '0000-00-00 00:00:00'),
(46, 0, 1, 1, 2, 39, 'Class work for 9th July 2019 for english', 'This is a class work', 1562652382, 1, 0, '0000-00-00 00:00:00'),
(47, 0, 1, 1, 2, 44, 'This is class work for 9th July 2019 for geography', 'Hello geo, this is class work', 1562652505, 1, 0, '0000-00-00 00:00:00'),
(48, 0, 1, 1, 2, 62, 'Class work bengali 10th july', 'Test', 1562755490, 0, 0, '0000-00-00 00:00:00'),
(49, 0, 1, 1, 2, 1, 'Test classwork', 'Test classwork test classwork', 1563192931, 1, 0, '0000-00-00 00:00:00'),
(50, 0, 1, 1, 2, 9, '15th jul test class work Physical science', '15th jul test class work Physical science', 1563190190, 1, 0, '0000-00-00 00:00:00'),
(51, 0, 1, 1, 1, 7, 'Test class work , history, 15th july', 'Test class work , history, 15th july', 1563190618, 1, 0, '0000-00-00 00:00:00'),
(52, 1, 1, 1, 2, 1, 'Class note new subject English', 'Class note new subject English', 1563792882, 1, 0, '0000-00-00 00:00:00'),
(53, 2, 1, 1, 2, 1, 'Home note new subject English', 'Home note new subject English', 1563792095, 1, 0, '0000-00-00 00:00:00'),
(54, 3, 1, 1, 2, 1, 'Assignment note new subject English', 'Assignment note new subject English', 1563792102, 1, 0, '0000-00-00 00:00:00'),
(55, 1, 1, 1, 2, 39, 'test docs', 'test ', 1563866184, 0, 0, '0000-00-00 00:00:00'),
(56, 1, 1, 1, 2, 54, 'std 1B, mathematics class work', 'Test std 1B, mathematics class work', 1564639385, 1, 0, '0000-00-00 00:00:00'),
(57, 1, 1, 1, 2, 66, 'hello', 'hello', 1566556596, 0, 0, '0000-00-00 00:00:00'),
(58, 1, 1, 1, 2, 1, '', '', 1566818456, 0, 0, '0000-00-00 00:00:00'),
(59, 1, 1, 1, 2, 39, '', '', 1566911928, 0, 0, '0000-00-00 00:00:00'),
(60, 1, 1, 1, 2, 53, '', '', 1566974905, 0, 0, '0000-00-00 00:00:00'),
(61, 2, 1, 1, 2, 53, '', '', 1566974751, 0, 0, '0000-00-00 00:00:00'),
(62, 3, 1, 1, 2, 53, '', '', 1566974854, 0, 0, '0000-00-00 00:00:00'),
(63, 1, 1, 1, 2, 74, 'This is a topic for english subject', 'Test', 1568456547, 1, 0, '0000-00-00 00:00:00'),
(64, 1, 1, 1, 2, 75, 'Bengali Poem', 'Today we learn bengali poem wrote by Rabindranath Tegor', 1568457204, 1, 0, '0000-00-00 00:00:00'),
(65, 1, 1, 1, 2, 76, 'Digestion System', 'Frog digestion system chapter 1 section A.', 1568457441, 1, 0, '0000-00-00 00:00:00'),
(66, 2, 1, 1, 2, 76, 'Home note for life science', 'This is a home note for life sc. on Digestion system of frog Chapter 1, Section A', 1568458457, 1, 0, '0000-00-00 00:00:00'),
(67, 3, 1, 1, 2, 76, 'Home Assignment Work', 'Please bring a full dissection of a frog properly', 1568458881, 1, 0, '0000-00-00 00:00:00'),
(68, 1, 22, 7, 29, 142, 'Hindi Class', 'Hindi poems', 1569654190, 1, 0, '0000-00-00 00:00:00'),
(69, 1, 157, 25, 58, 144, 'English Class', 'English Phrase notes', 1570622445, 1, 0, '0000-00-00 00:00:00'),
(70, 1, 1, 1, 2, 54, 'sdad', '', 1570713164, 1, 0, '0000-00-00 00:00:00'),
(71, 2, 1, 1, 2, 54, 'rro', '', 1570697222, 1, 0, '0000-00-00 00:00:00'),
(72, 3, 1, 1, 2, 54, 'asas', '', 1570700458, 1, 0, '0000-00-00 00:00:00'),
(73, 1, 1, 1, 2, 57, 'AA', '', 1570705402, 1, 0, '0000-00-00 00:00:00'),
(74, 2, 1, 1, 2, 57, 'BB', '', 1570705454, 0, 0, '0000-00-00 00:00:00'),
(75, 3, 1, 1, 2, 57, 'CC', '', 1570705496, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_note_files`
--

CREATE TABLE `tbl_note_files` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=cw,2=hw,3=aw,4=notice,5=syllabus',
  `note_id` int(11) NOT NULL COMMENT 'referal_id',
  `file_url` text NOT NULL,
  `last_update` datetime NOT NULL,
  `del_status` int(11) NOT NULL COMMENT '0=not delete,1=delete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_note_files`
--

INSERT INTO `tbl_note_files` (`id`, `type`, `note_id`, `file_url`, `last_update`, `del_status`) VALUES
(1, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559043501_sample.pdf', '0000-00-00 00:00:00', 0),
(2, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559043502_Photosynthesis1.jpg', '0000-00-00 00:00:00', 0),
(3, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045084_sample.pdf', '0000-00-00 00:00:00', 0),
(4, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045715_photo2.jpg', '0000-00-00 00:00:00', 0),
(5, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045716_photo3.jpg', '0000-00-00 00:00:00', 0),
(6, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045716_photo5.jpg', '0000-00-00 00:00:00', 0),
(7, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045717_photo6.jpg', '0000-00-00 00:00:00', 0),
(8, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045717_photo7.jpg', '0000-00-00 00:00:00', 0),
(9, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045717_Photosynthesis1.jpg', '0000-00-00 00:00:00', 0),
(10, 0, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559045718_photosynthesis4.jpg', '0000-00-00 00:00:00', 0),
(13, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559105535_867666156.jpg', '0000-00-00 00:00:00', 0),
(15, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559105535_1178646orig.jpg', '0000-00-00 00:00:00', 0),
(16, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559112227_filefileexampleXLSX10.xlsx', '0000-00-00 00:00:00', 0),
(17, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559112228_sample.doc', '0000-00-00 00:00:00', 0),
(19, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559112228_filefileexampleXLS10.xls', '0000-00-00 00:00:00', 0),
(20, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559112229_filefileexampleXLS10-%282%29.xls', '0000-00-00 00:00:00', 0),
(21, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559112229_filefileexampleXLS10-%281%29.xls', '0000-00-00 00:00:00', 0),
(22, 0, 2, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559128399_sample.doc', '0000-00-00 00:00:00', 0),
(23, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559197911_add1.jpg', '0000-00-00 00:00:00', 0),
(24, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559197911_add2.jpg', '0000-00-00 00:00:00', 0),
(25, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559197912_add3.jpg', '0000-00-00 00:00:00', 0),
(26, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559198999_add4.jpg', '0000-00-00 00:00:00', 0),
(27, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559199171_80.pdf', '0000-00-00 00:00:00', 0),
(28, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559199171_add5.jpg', '0000-00-00 00:00:00', 0),
(29, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223651_introimg.jpg', '0000-00-00 00:00:00', 0),
(30, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223687_herobg.jpg', '0000-00-00 00:00:00', 0),
(31, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223735_Changes-2.docx', '0000-00-00 00:00:00', 0),
(32, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223736_WhatsApp-Image-2019-05-30-at-4.27.26-PM.jpeg', '0000-00-00 00:00:00', 0),
(38, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223903_add6.jpg', '0000-00-00 00:00:00', 0),
(39, 0, 5, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223944_add1.jpg', '0000-00-00 00:00:00', 0),
(40, 0, 5, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223964_add2.jpg', '0000-00-00 00:00:00', 0),
(41, 0, 5, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223974_add3.jpg', '0000-00-00 00:00:00', 0),
(42, 0, 5, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559223974_add4.jpg', '0000-00-00 00:00:00', 0),
(43, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224076_WhatsApp-Image-2019-05-30-at-4.27.27-PM.jpeg', '0000-00-00 00:00:00', 0),
(44, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224077_WhatsApp-Image-2019-05-30-at-4.27.26-PM.jpeg', '0000-00-00 00:00:00', 0),
(45, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224154_sample.xls', '0000-00-00 00:00:00', 0),
(46, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224280_1.-Digital-Marketing.pptx', '0000-00-00 00:00:00', 0),
(47, 0, 4, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224342_1.-Digital-Marketing.pptx', '0000-00-00 00:00:00', 0),
(48, 0, 6, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224668_indexred-%281%29.jpg', '0000-00-00 00:00:00', 0),
(49, 0, 6, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559224669_WhatsApp-Image-2019-05-27-at-4.33.17-PM-%282%29.jpeg', '0000-00-00 00:00:00', 0),
(66, 0, 9, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559903537_frog1.jpg', '0000-00-00 00:00:00', 0),
(67, 0, 9, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559903537_frog2.jpg', '0000-00-00 00:00:00', 0),
(68, 0, 9, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559903559_classnotes1559112228sample.doc', '0000-00-00 00:00:00', 0),
(69, 0, 10, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559997300_competitor.xls', '0000-00-00 00:00:00', 0),
(70, 0, 10, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1559997311_photo-1471286174890-9c112ffca5b4.jpeg', '0000-00-00 00:00:00', 0),
(71, 0, 11, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560148436_gram1.jpg', '0000-00-00 00:00:00', 0),
(72, 0, 11, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560148437_gram2.jpg', '0000-00-00 00:00:00', 0),
(73, 0, 11, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560148437_gram3.jpg', '0000-00-00 00:00:00', 0),
(74, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560160558_sound1.jpg', '0000-00-00 00:00:00', 0),
(75, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560160559_sound2.jpg', '0000-00-00 00:00:00', 0),
(76, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168163_classnotes1559042821sample.doc', '0000-00-00 00:00:00', 0),
(77, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168164_classnotes1559112229filefileexampleXLS10-%281%29.xls', '0000-00-00 00:00:00', 0),
(78, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168164_classnotes1559112228sample.doc', '0000-00-00 00:00:00', 0),
(79, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168164_classnotes1559112227filefileexampleXLSX10.xlsx', '0000-00-00 00:00:00', 0),
(80, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168165_add1.jpg', '0000-00-00 00:00:00', 0),
(81, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168165_add2.jpg', '0000-00-00 00:00:00', 0),
(82, 0, 12, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560168165_add3.jpg', '0000-00-00 00:00:00', 0),
(83, 0, 13, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560175492_Changes-2.docx', '0000-00-00 00:00:00', 0),
(84, 0, 13, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560175493_Banskathi.jpeg', '0000-00-00 00:00:00', 0),
(85, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560409277_classnotes1559042821sample.doc', '0000-00-00 00:00:00', 0),
(86, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560409278_add1.jpg', '0000-00-00 00:00:00', 0),
(87, 0, 3, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560409278_add2.jpg', '0000-00-00 00:00:00', 0),
(93, 0, 8, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560492385_TAGGING.xlsx', '0000-00-00 00:00:00', 0),
(94, 0, 8, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560492450_IMG-20190613-WA0009.jpg', '0000-00-00 00:00:00', 0),
(95, 0, 8, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560492465_received2141094409552274.jpeg', '0000-00-00 00:00:00', 0),
(96, 0, 14, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560493816_TAGGING.xlsx', '0000-00-00 00:00:00', 0),
(97, 0, 14, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560493817_IMG-20190613-WA0000.jpg', '0000-00-00 00:00:00', 0),
(98, 0, 15, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560495843_IMG20190412154209.jpg', '0000-00-00 00:00:00', 0),
(99, 0, 17, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560766826_classnotes1559042821sample.doc', '0000-00-00 00:00:00', 0),
(100, 0, 17, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560766827_add1.jpg', '0000-00-00 00:00:00', 0),
(101, 0, 17, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560766827_add2.jpg', '0000-00-00 00:00:00', 0),
(102, 0, 17, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560766828_add3.jpg', '0000-00-00 00:00:00', 0),
(103, 0, 18, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560843453_IR-64.jpeg', '0000-00-00 00:00:00', 0),
(104, 0, 18, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1560855107_AvikBanerjee%28CSE%29-11454518953397.docx', '0000-00-00 00:00:00', 0),
(106, 0, 19, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561029963_IMG-20190619-WA0000.jpg', '0000-00-00 00:00:00', 0),
(124, 0, 21, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561103802_add1.jpg', '0000-00-00 00:00:00', 0),
(125, 0, 21, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561103804_add2.jpg', '0000-00-00 00:00:00', 0),
(126, 0, 21, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561103805_add3.jpg', '0000-00-00 00:00:00', 0),
(127, 0, 21, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561103805_add5.jpg', '0000-00-00 00:00:00', 0),
(128, 0, 22, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561115847_classnotes1560492385TAGGING.xlsx', '0000-00-00 00:00:00', 0),
(129, 0, 23, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117560_sample-%286%29.xls', '0000-00-00 00:00:00', 0),
(130, 0, 23, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117560_sample-%285%29.xls', '0000-00-00 00:00:00', 0),
(131, 0, 23, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117561_vector-school-bus-illustration.jpg', '0000-00-00 00:00:00', 0),
(132, 0, 23, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117561_chi7.jpg', '0000-00-00 00:00:00', 0),
(133, 0, 23, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117562_chi6.jpg', '0000-00-00 00:00:00', 0),
(134, 0, 24, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117654_assessor.xls', '0000-00-00 00:00:00', 0),
(135, 0, 24, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117654_chi5.jpg', '0000-00-00 00:00:00', 0),
(136, 0, 24, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117655_chi4.png', '0000-00-00 00:00:00', 0),
(137, 0, 24, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561117655_chi3.jpg', '0000-00-00 00:00:00', 0),
(138, 0, 25, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561118046_AMPLDATA4-APRIL-201918%3A01.xls', '0000-00-00 00:00:00', 0),
(139, 0, 25, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561118047_IMG-20190621-WA0074.jpg', '0000-00-00 00:00:00', 0),
(140, 0, 26, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561119049_AMPLDATA4-APRIL-201918%3A01.xls', '0000-00-00 00:00:00', 0),
(141, 0, 26, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561119049_IMG20190529162155.jpg', '0000-00-00 00:00:00', 0),
(142, 0, 27, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561120401_4.-A1-Games-PostersSquash.png', '0000-00-00 00:00:00', 0),
(143, 0, 28, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561120835_chi02.jpg', '0000-00-00 00:00:00', 0),
(144, 0, 29, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561120944_sample%285%29.xls', '0000-00-00 00:00:00', 0),
(145, 0, 29, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561120944_vector-school-bus-illustration.jpg', '0000-00-00 00:00:00', 0),
(146, 0, 30, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561122133_chi3.jpg', '0000-00-00 00:00:00', 0),
(147, 0, 30, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561122133_chi02.jpg', '0000-00-00 00:00:00', 0),
(148, 0, 31, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561122808_classnotes1560855107AvikBanerjee%28CSE%29-11454518953397.docx', '0000-00-00 00:00:00', 0),
(149, 0, 31, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561122808_IMG-20190619-WA0000.jpg', '0000-00-00 00:00:00', 0),
(150, 0, 32, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561188640_IMG20190621193554.jpg', '0000-00-00 00:00:00', 0),
(152, 0, 33, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561204710_classnotes1560855107AvikBanerjee%28CSE%29-11454518953397.docx', '0000-00-00 00:00:00', 0),
(153, 0, 34, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561529688_TAGGING.xlsx', '0000-00-00 00:00:00', 0),
(154, 0, 34, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561529689_IMG-20190626-WA0000.jpg', '0000-00-00 00:00:00', 0),
(155, 0, 35, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561529697_IMG-20190623-WA0004.jpg', '0000-00-00 00:00:00', 0),
(156, 0, 37, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561716812_bidyaalymulti.doc', '0000-00-00 00:00:00', 0),
(157, 0, 37, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561716813_doll-26809041280.jpg', '0000-00-00 00:00:00', 0),
(158, 0, 38, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561716950_bidyaalymulti.doc', '0000-00-00 00:00:00', 0),
(159, 0, 38, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561716950_doll-26809041280.jpg', '0000-00-00 00:00:00', 0),
(161, 0, 39, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561718220_IMG-20190628-WA0011.jpg', '0000-00-00 00:00:00', 0),
(162, 0, 40, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561792739_PolioEventJanuary2019.pdf', '0000-00-00 00:00:00', 0),
(163, 0, 41, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561792935_classnotes156171912280.pdf', '0000-00-00 00:00:00', 0),
(164, 0, 41, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561792949_classnotes1561718488syllabus156171758180.pdf', '0000-00-00 00:00:00', 0),
(165, 0, 41, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561792950_classnotes1561718488syllabus156171758180.pdf', '0000-00-00 00:00:00', 0),
(166, 0, 41, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561793075_syllabus1561199725classnotes1559112227filefileexampleXLSX10.xlsx', '0000-00-00 00:00:00', 0),
(167, 0, 41, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561793132_Screenshot2019-03-28-20-20-16-899com.flipkart.android.png', '0000-00-00 00:00:00', 0),
(168, 0, 41, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1561793199_Screenshot2019-03-28-20-17-33-573com.flipkart.android.png', '0000-00-00 00:00:00', 0),
(169, 0, 42, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562153755_vectorpdf.doc', '0000-00-00 00:00:00', 0),
(170, 0, 42, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562153756_zombihome.png', '0000-00-00 00:00:00', 0),
(171, 0, 43, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562160261_classnotes156171912280.pdf', '0000-00-00 00:00:00', 0),
(172, 0, 43, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562160261_IMG-20190702-WA0072.jpg', '0000-00-00 00:00:00', 0),
(173, 0, 44, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562219730_sample.pdf', '0000-00-00 00:00:00', 0),
(174, 0, 44, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562219731_1535463084577.jpg', '0000-00-00 00:00:00', 0),
(175, 0, 44, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562220994_file-sample500kB.doc', '0000-00-00 00:00:00', 0),
(176, 0, 45, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562329479_IMG-20190702-WA0046.jpg', '0000-00-00 00:00:00', 0),
(177, 0, 46, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562652356_assignmentnotes1562225319homenotes1562224998file-sample500kB.doc', '0000-00-00 00:00:00', 0),
(178, 0, 46, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562652357_assignmentnotes1562225329httpss3.ap-south-1.amazonaws.comschoolapp.testclassnoteclassnotes1559223944add1.jpg', '0000-00-00 00:00:00', 0),
(179, 0, 47, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562652505_assignmentnotes1562225826Financial-Sample.xlsx', '0000-00-00 00:00:00', 0),
(180, 0, 47, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562652506_IMG-20190708-WA0000.jpg', '0000-00-00 00:00:00', 0),
(181, 0, 48, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562755491_assignmentnotes1562225826Financial-Sample.xlsx', '0000-00-00 00:00:00', 0),
(182, 0, 48, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1562755491_IMG-20190710-WA0005.jpg', '0000-00-00 00:00:00', 0),
(184, 1, 52, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563791383_IMG-20190722-WA0015.jpg', '2019-07-22 15:59:43', 0),
(185, 2, 53, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1563791642_assignmentnotes1562225319homenotes1562224998file-sample500kB.doc', '2019-07-22 16:04:02', 0),
(186, 2, 53, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1563791642_IMG-20190722-WA0013.jpg', '2019-07-22 16:04:03', 0),
(187, 3, 54, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1563791785_assignmentnotes1562225826Financial-Sample.xlsx', '2019-07-22 16:06:26', 0),
(188, 3, 54, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1563791786_IMG-20190722-WA0005.jpg', '2019-07-22 16:06:26', 0),
(189, 1, 52, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563792849_4.-A1-Games-PostersSquash.png', '2019-07-22 16:24:09', 0),
(190, 1, 52, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563792882_cbse-class-1-english-syllabus-2012-131.pdf', '2019-07-22 16:24:42', 0),
(192, 6, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/image_1563801065_81.jpg', '2019-07-22 18:41:05', 0),
(193, 4, 89, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/teacher/TrilochanBanerjee_1563801314', '2019-07-22 18:45:14', 0),
(194, 1, 55, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563865685_ADMISSION-FORM1.pdf', '2019-07-23 12:38:06', 0),
(195, 1, 55, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563865686_sample.jpeg', '2019-07-23 12:38:06', 0),
(196, 1, 55, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563866184_demo1.docx', '2019-07-23 12:46:25', 0),
(197, 1, 55, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1563866185_excelfile1.xlsx', '2019-07-23 12:46:25', 0),
(200, 4, 26, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/image_1563879708_Photosynthesis11.png', '2019-07-23 16:31:48', 0),
(201, 4, 26, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/image_1563879708_Photosynthesis12.png', '2019-07-23 16:31:48', 0),
(202, 4, 27, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1563880448_Photosynthesis12.png', '2019-07-23 16:44:09', 0),
(203, 4, 27, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1563880448_Photosynthesis11.png', '2019-07-23 16:44:09', 0),
(209, 5, 7, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/syllabus/syllabus_1563882673_demo1.docx', '2019-07-23 17:21:14', 0),
(210, 5, 7, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/syllabus/syllabus_1563882674_excelfile1.xlsx', '2019-07-23 17:21:14', 0),
(211, 5, 7, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/syllabus/syllabus_1563882674_Photosynthesis0.png', '2019-07-23 17:21:15', 0),
(212, 5, 7, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/syllabus/syllabus_1563882675_ADMISSION-FORM1.pdf', '2019-07-23 17:21:15', 0),
(213, 4, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/notice_1563886212_demo1.docx', '2019-07-23 18:20:13', 0),
(214, 4, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/notice_1563886213_excelfile1.xlsx', '2019-07-23 18:20:13', 0),
(215, 4, 1, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/image_1563886213_Photosynthesis0.png', '2019-07-23 18:20:13', 0),
(216, 4, 30, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1565005204_CTM-website-comments-2019-08-02.pdf', '2019-08-05 17:10:05', 0),
(217, 1, 57, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566556596_excelfile1.xlsx', '2019-08-23 16:06:36', 0),
(218, 1, 57, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566556596_img1.png', '2019-08-23 16:06:36', 0),
(219, 1, 58, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566818126_333.jpg', '2019-08-26 16:45:28', 0),
(220, 1, 58, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566818128_abhi.jpg', '2019-08-26 16:45:28', 0),
(221, 1, 58, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566818308_81.jpg', '2019-08-26 16:48:28', 0),
(222, 1, 58, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566818456_biplob.jpg', '2019-08-26 16:50:57', 0),
(223, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969097_favouriteshoppingbag-512.png', '2019-08-28 10:41:38', 0),
(224, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969098_writepencilnote-512.png', '2019-08-28 10:41:38', 0),
(225, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969098_3.jpg', '2019-08-28 10:41:38', 0),
(226, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969098_3screenshot-dribbble.com-2019.01.22-15-58-29.png', '2019-08-28 10:41:39', 0),
(227, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969099_4screenshot-static.collectui.com-2019.01.22-15-47-03.png', '2019-08-28 10:41:39', 0),
(228, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969099_6.jpg', '2019-08-28 10:41:40', 0),
(229, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969100_7-512.png', '2019-08-28 10:41:40', 0),
(230, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566969100_10-512.png', '2019-08-28 10:41:40', 0),
(231, 2, 61, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1566974656_111.pdf', '2019-08-28 12:14:17', 0),
(232, 2, 61, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1566974657_ACG-website-feedback-2019-05-08.pdf', '2019-08-28 12:14:18', 0),
(233, 2, 61, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1566974658_ACG-website-feedback-2019-07-012.pdf', '2019-08-28 12:14:20', 0),
(234, 3, 62, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1566974854_111.pdf', '2019-08-28 12:17:35', 0),
(236, 3, 62, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1566974857_ACG-website-feedback-2019-07-012.pdf', '2019-08-28 12:17:39', 0),
(237, 1, 60, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1566974905_111.pdf', '2019-08-28 12:18:26', 0),
(238, 4, 37, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/image_1569324784_2.jpg', '2019-09-24 17:03:05', 0),
(239, 4, 43, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569475801_2.jpg', '2019-09-26 11:00:47', 0),
(240, 4, 43, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569475801_80.pdf', '2019-09-26 11:00:48', 0),
(241, 4, 43, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569475801_ACG-portal-2019-07-27.pdf', '2019-09-26 11:00:50', 0),
(242, 4, 44, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569476615_2.jpg', '2019-09-26 11:13:49', 0),
(243, 4, 44, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569476615_80.pdf', '2019-09-26 11:13:50', 0),
(244, 4, 45, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569652593_2.jpg', '2019-09-28 12:06:34', 0),
(245, 4, 45, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1569652593_80.pdf', '2019-09-28 12:06:34', 0),
(246, 1, 68, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1569654190_80.pdf', '2019-09-28 12:33:11', 0),
(247, 1, 68, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1569654191_2.jpg', '2019-09-28 12:33:12', 0),
(248, 4, 46, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570616522_81.jpg', '2019-10-09 15:52:06', 0),
(249, 4, 50, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570622080_81.jpg', '2019-10-09 17:24:44', 0),
(250, 1, 69, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1570622445_81.jpg', '2019-10-09 17:30:46', 0),
(251, 4, 55, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570627253_uocg0Fa6i8.jpg', '2019-10-09 18:51:39', 0),
(252, 4, 56, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570627357_YrmIRhqZjD.jpg', '2019-10-09 18:53:24', 0),
(253, 4, 58, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570686751_iGj41KFT1.jpg', '2019-10-10 11:22:32', 0),
(256, 4, 59, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570686992_U6avy4Aaai.jpg', '2019-10-10 11:26:32', 0),
(257, 4, 59, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570686992_U6avy4Aaai.jpg', '2019-10-10 11:26:33', 0),
(260, 4, 62, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687511_gBpPKZ239P.jpg', '2019-10-10 11:35:11', 0),
(261, 4, 62, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687511_8_0.pdf', '2019-10-10 11:35:12', 0),
(262, 4, 63, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687586_IGSlpBGyM2.jpg', '2019-10-10 11:36:27', 0),
(263, 4, 63, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687586_BcEdhoSH.jpg', '2019-10-10 11:36:27', 0),
(264, 4, 63, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687586_8_0.pdf', '2019-10-10 11:36:27', 0),
(265, 4, 64, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687989_h7464UHzcq.jpg', '2019-10-10 11:43:09', 0),
(266, 4, 64, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687989_yEqqncwiM.jpg', '2019-10-10 11:43:10', 0),
(267, 4, 64, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/notice/1570687989_8_0.pdf', '2019-10-10 11:43:10', 0),
(273, 1, 70, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes_1570693508_80.pdf', '2019-10-10 13:15:09', 0),
(274, 2, 71, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1570697113_5F2xRBqT9G.jpg', '2019-10-10 14:15:14', 0),
(275, 2, 71, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1570697194_80.pdf', '2019-10-10 14:16:35', 0),
(276, 2, 71, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/homenote/homenotes_1570697222_ACG-portal-2019-07-27.pdf', '2019-10-10 14:17:04', 0),
(278, 3, 72, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1570700418_80.pdf', '2019-10-10 15:10:19', 0),
(279, 3, 72, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1570700419_McsOwy6Vi8.jpg', '2019-10-10 15:10:20', 0),
(280, 3, 72, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/assignmentnote/assignmentnotes_1570700459_ACG-portal-2019-07-27.pdf', '2019-10-10 15:11:00', 0),
(292, 1, 70, 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/classnote/classnotes__J0JBqo8u3l.jpg', '2019-10-10 18:42:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notice`
--

CREATE TABLE `tbl_notice` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1=teacher,2=all parents,3=all parents by class',
  `class_id` varchar(50) NOT NULL COMMENT 'only if type = 3',
  `notice_heading` varchar(512) NOT NULL,
  `issue_date` date NOT NULL,
  `notice_text` longtext NOT NULL,
  `publish_date` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active,2=inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notice`
--

INSERT INTO `tbl_notice` (`id`, `school_id`, `type`, `class_id`, `notice_heading`, `issue_date`, `notice_text`, `publish_date`, `status`) VALUES
(1, 1, 1, '', 'General Meet', '2019-05-31', '<p>All teachers are hereby notified that a general meet will be held on 05-06-2019 at 12:30 P.m. All teachers are requested to attend positively.</p>', 0, 1),
(3, 1, 2, '', 'Teacher Parent Meet', '2019-05-31', '<p>All parents are hereby informed that a general teacher-parent meet will be held on 05-06-2019 at 1:30 P.M. All parents are requested to attend the meeting.</p>\r\n', 20190601, 1),
(4, 1, 3, '1', 'Special Announcement', '2019-05-31', '<p>A physical training camp will organize by the school. Interested students can contact their respective class teachers.</p>\r\n', 0, 1),
(5, 1, 1, '', 'Testing', '2019-07-10', '<p>You probably appreciate the benefits that a good student management system can add to the running of your school or college. But be aware that ongoing support is a vital component of any successful software package. It may be counterproductive to use homemade systems or to engage individuals who cannot contractually provide vital support in the future. Fedena student information system support is 24/7 and is renowned for its excellence and friendliness.</p><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p><p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 0, 1),
(6, 1, 1, '', 'Lorem Ipsum', '2019-06-03', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 0, 1),
(7, 1, 2, '', 'Seven-Day Faculty Development Programme on Business Studies 20th - 26th May, 2019', '2019-06-04', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 0, 1),
(8, 29, 1, '', 'Hello Hello Hello Hello Hello', '2019-06-07', '<p>HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello</p><p>HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello</p>', 0, 1),
(9, 22, 1, '7', 'New Session', '2019-06-08', '<p>The new session 2019-2020 will start from 15th June onwards.</p>', 0, 1),
(10, 23, 2, '', 'Summer Holiday Extended', '2019-06-14', '<p>HostGator is the most reliable service provider. Powerful hosting features and proactive customer support makes it eat the competition. It&rsquo;s always been a wise decision for me to choose HostGator as companion in my online business journey.</p>', 0, 1),
(11, 23, 1, '', 'Over time all this week', '2019-06-14', '<p>HostGator is the most reliable service provider. Powerful hosting features and proactive customer support makes it eat the competition. It&rsquo;s always been a wise decision for me to choose HostGator as companion in my online business journey.&nbsp;HostGator is the most reliable service provider. Powerful hosting features and proactive customer support makes it eat the competition. It&rsquo;s always been a wise decision for me to choose HostGator as companion in my online business journey.&nbsp;HostGator is the most reliable service provider. Powerful hosting features and proactive customer support makes it eat the competition. It&rsquo;s always been a wise decision for me to choose HostGator as companion in my online business journey.</p>', 0, 1),
(13, 23, 1, '', 'THS participation - Kolkata Clean Air', '2019-06-21', '<ul><li>The students of The Heritage School recently participated in a plantation workshop at Lion&#39;s Safari Park, an initiative undertaken by Kolkata Clean Air.&nbsp;</li></ul>', 20190624, 1),
(14, 1, 1, '', 'Test for publish date', '2019-06-22', '<p>hello</p>', 20190624, 1),
(15, 1, 2, '', 'notice issue test for all parents', '2019-06-22', '<p>Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;Test&nbsp;</p>', 20190624, 1),
(17, 1, 1, '', 'Hello notice download test', '2019-06-22', '<p>Hello notice download test</p>', 20190622, 1),
(18, 1, 1, '', 'Notice in future date 26-06-2019', '2019-06-24', '<p>Notice in future date&nbsp;26-06-2019</p>', 20190626, 1),
(19, 1, 2, '', 'Future notice for parents 27-06-2019', '2019-06-24', '<p>Future notice for parents 27-06-2019</p>', 20190627, 1),
(20, 22, 2, '', 'Terminal examination', '2019-06-27', '<p>Date of Midterm Examinations will be announced soon.&nbsp;</p>', 0, 1),
(21, 50, 3, '', '', '2019-07-02', '', 20190702, 1),
(22, 1, 1, '', 'Bidyaaly Notification', '2019-07-11', '<p>Bidyaaly push test notification</p>', 0, 1),
(23, 1, 1, '', 'Admission Notice', '2019-07-11', '<p>Admission notice push notification</p>', 20190711, 1),
(24, 1, 1, '', 'Over time all this week', '2019-07-11', '<p>Over time all this week</p><p>Over time all this week</p><p>Over time all this week</p>', 0, 1),
(25, 1, 1, '', 'Summer Holiday Extended', '2019-07-11', '<p>Over time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this weekOver time all this week</p>', 0, 1),
(26, 1, 1, '', 'Test Notice', '2019-07-23', '<p>Test</p>', 20190726, 1),
(27, 1, 1, '', 'Hello Testing', '2019-07-23', '<p>Hello</p>', 20190725, 1),
(28, 1, 1, '', 'tset', '2019-08-01', '<p>test</p>', 0, 1),
(29, 1, 1, '', 'Test 2', '2019-08-01', '<p>test2</p>', 20190801, 1),
(30, 1, 1, '', 'Notice 05.08.2019', '2019-08-05', '<p>New notice for testing purpose.</p>', 20190805, 1),
(31, 1, 1, '', 'Hello new notice', '2019-08-06', '<p>Hello new notice</p>', 1565029800, 1),
(32, 1, 1, '', 'Hello All teachers', '2019-08-21', '<p>This is a test notice for all teachers</p>', 1566325800, 1),
(33, 1, 1, '', 'Urgent Meeting', '2019-09-14', '<p>Urgent meeting will be held on 15th Sep 2019 at 4 P.M.</p>', 1568399400, 1),
(34, 1, 1, '', 'Holidays declared', '2019-09-14', '<p>Holiday list will be available in notice board.</p>', 1568399400, 1),
(35, 1, 1, '', 'Holidays declared', '2019-09-14', '<p>Durgapuja holiday from 5th oct 2019</p>', 1568399400, 1),
(36, 1, 1, '', 'Holidays declared', '2019-09-14', '<p>Durgapuja holiday from 5th oct 2019</p>', 1568399400, 1),
(37, 1, 2, '', 'Hello Parents', '2019-09-14', '<p>Hello meet will be organized by school. All parents are requested to attend.</p>', 1568399400, 1),
(38, 1, 2, '', 'Hello Parents', '2019-09-14', '<p>Hello meet will be organized by school. All parents are requested to attend.</p>', 1568399400, 1),
(39, 1, 3, '1', 'Seminar for  Parents.', '2019-09-14', '<p>There will be a seminer for Class I B students and Parents.</p><p>There will be a seminer for Class I B students and Parents.</p>', 1568399400, 1),
(40, 1, 3, '1', 'Final Test', '2019-09-14', '<p>This is my final Test&nbsp;This is my final Test&nbsp;This is my final Test&nbsp;This is my final Test</p>', 1568399400, 1),
(42, 1, 1, '', 'hello test notice', '2019-09-16', '<p>hello, this is a test notice.</p>', 1568572200, 1),
(43, 1, 1, '', 'Download all', '2019-09-26', '<p>develop Download all&nbsp;</p>', 1569436200, 1),
(44, 1, 2, '', 'Download all parents', '2019-09-26', '<p>Develop&nbsp;Download all parents</p>', 1569436200, 1),
(45, 22, 2, '', 'Testing for another school', '2019-09-28', '<p>Hello, this is a testing from Future Foundation school</p>', 1569609000, 1),
(46, 157, 1, '', 'optimize', '2019-10-09', '<p>test</p>', 1570559400, 1),
(47, 157, 1, '', 'asdads', '2019-10-09', '<p>adsada</p>', 1570559400, 1),
(48, 157, 1, '', 'asdfsdf', '2019-10-09', '<p>asfaf</p>', 1570559400, 1),
(49, 157, 1, '', 'asasd', '2019-10-09', '', 1570559400, 1),
(50, 157, 1, '', 'ghgh', '2019-10-09', '<p>ghghgh</p>', 1570559400, 1),
(51, 1, 1, '', 'dfgg', '2019-10-09', '<p>dgdgd</p>', 1570559400, 1),
(52, 1, 1, '', 'dfgg', '2019-10-09', '<p>dgdgd</p>', 1570559400, 1),
(53, 1, 1, '', 'dfgg', '2019-10-09', '<p>dgdgd</p>', 1570559400, 1),
(54, 1, 1, '', 'dfgg', '2019-10-09', '<p>dgdgd</p>', 1570559400, 1),
(55, 1, 1, '', 'dfgg', '2019-10-09', '<p>dgdgd</p>', 1570559400, 1),
(56, 1, 1, '', 'Hello Testing Hello', '2019-10-09', '<p>hello testing</p>', 1570559400, 1),
(57, 157, 1, '', 'Other file', '2019-10-10', '', 1570645800, 1),
(58, 157, 1, '', 'test', '2019-10-10', '<p>asdasd</p>', 1570645800, 1),
(59, 157, 1, '', 'Test file upload', '2019-10-10', '', 1570645800, 1),
(60, 157, 1, '', 'bbbb', '2019-10-10', '', 1570645800, 1),
(61, 157, 1, '', 'gggggg', '2019-10-10', '', 1570645800, 1),
(62, 157, 1, '', 'vvvvvvv', '2019-10-10', '', 1570645800, 1),
(63, 157, 1, '', 'vvvvvvv2', '2019-10-10', '', 1570645800, 1),
(64, 157, 1, '', 'All files randomly taken', '2019-10-10', '', 1570645800, 1),
(65, 169, 1, '', 'General Meeting will held on 26-10-2019', '2019-10-17', '<p>All teachers are hereby requested to please attend the general meet on date given.</p>', 1571250600, 1),
(66, 169, 1, '', 'Another notice for test', '2019-10-17', '<p>Hello, this is anothere notice for testing purpose.</p>', 1571250600, 1),
(67, 169, 1, '', 'Hello test notice', '2019-10-21', '<p>Hello, this is a test notice, please ignore once received.</p>', 1571596200, 1),
(68, 169, 1, '', 'test not', '2019-10-21', '<p>hello test not</p>', 1571596200, 1),
(69, 169, 1, '', 'All teachers notice', '2019-10-21', '<p>This is a notice to all teachers</p>', 1571596200, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification_settings`
--

CREATE TABLE `tbl_notification_settings` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `notification_type` int(11) NOT NULL,
  `push_notification` tinyint(1) NOT NULL,
  `email_notification` tinyint(1) NOT NULL,
  `sms_notification` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification_settings`
--

INSERT INTO `tbl_notification_settings` (`id`, `school_id`, `notification_type`, `push_notification`, `email_notification`, `sms_notification`) VALUES
(57, 155, 1, 1, 1, 0),
(58, 155, 2, 1, 1, 0),
(59, 155, 3, 1, 0, 0),
(60, 155, 4, 1, 0, 0),
(61, 155, 5, 1, 1, 1),
(62, 155, 6, 1, 1, 0),
(63, 155, 7, 1, 0, 0),
(71, 1, 1, 1, 1, 0),
(72, 1, 2, 1, 1, 0),
(73, 1, 3, 1, 0, 0),
(74, 1, 4, 1, 0, 0),
(75, 1, 5, 1, 1, 1),
(76, 1, 6, 1, 1, 1),
(77, 1, 7, 1, 0, 0),
(78, 156, 1, 1, 1, 0),
(79, 156, 2, 1, 1, 0),
(80, 156, 3, 1, 0, 0),
(81, 156, 4, 1, 0, 0),
(82, 156, 5, 1, 1, 1),
(83, 156, 6, 1, 1, 0),
(84, 156, 7, 1, 0, 0),
(85, 157, 1, 1, 1, 0),
(86, 157, 2, 1, 1, 0),
(87, 157, 3, 1, 0, 0),
(88, 157, 4, 1, 0, 0),
(89, 157, 5, 1, 1, 1),
(90, 157, 6, 1, 1, 0),
(91, 157, 7, 1, 0, 0),
(92, 162, 1, 1, 1, 0),
(93, 162, 2, 1, 1, 0),
(94, 162, 3, 1, 0, 0),
(95, 162, 4, 1, 0, 0),
(96, 162, 5, 1, 1, 1),
(97, 162, 6, 1, 1, 0),
(98, 162, 7, 1, 0, 0),
(99, 163, 1, 1, 1, 0),
(100, 163, 2, 1, 1, 0),
(101, 163, 3, 1, 0, 0),
(102, 163, 4, 1, 0, 0),
(103, 163, 5, 1, 1, 1),
(104, 163, 6, 1, 1, 0),
(105, 163, 7, 1, 0, 0),
(106, 164, 1, 1, 1, 0),
(107, 164, 2, 1, 1, 0),
(108, 164, 3, 1, 0, 0),
(109, 164, 4, 1, 0, 0),
(110, 164, 5, 1, 1, 1),
(111, 164, 6, 1, 1, 0),
(112, 164, 7, 1, 0, 0),
(113, 165, 1, 1, 1, 0),
(114, 165, 2, 1, 1, 0),
(115, 165, 3, 1, 0, 0),
(116, 165, 4, 1, 0, 0),
(117, 165, 5, 1, 1, 1),
(118, 165, 6, 1, 1, 0),
(119, 165, 7, 1, 0, 0),
(120, 166, 1, 1, 1, 0),
(121, 166, 2, 1, 1, 0),
(122, 166, 3, 1, 0, 0),
(123, 166, 4, 1, 0, 0),
(124, 166, 5, 1, 1, 1),
(125, 166, 6, 1, 1, 0),
(126, 166, 7, 1, 0, 0),
(127, 167, 1, 1, 1, 0),
(128, 167, 2, 1, 1, 0),
(129, 167, 3, 1, 0, 0),
(130, 167, 4, 1, 0, 0),
(131, 167, 5, 1, 1, 1),
(132, 167, 6, 1, 1, 0),
(133, 167, 7, 1, 0, 0),
(134, 168, 1, 1, 1, 0),
(135, 168, 2, 1, 1, 0),
(136, 168, 3, 1, 0, 0),
(137, 168, 4, 1, 0, 0),
(138, 168, 5, 1, 1, 1),
(139, 168, 6, 1, 1, 0),
(140, 168, 7, 1, 0, 0),
(148, 172, 1, 1, 1, 0),
(149, 172, 2, 1, 1, 0),
(150, 172, 3, 1, 0, 0),
(151, 172, 4, 1, 0, 0),
(152, 172, 5, 1, 1, 1),
(153, 172, 6, 1, 1, 0),
(154, 172, 7, 1, 0, 0),
(155, 173, 1, 1, 1, 0),
(156, 173, 2, 1, 1, 0),
(157, 173, 3, 1, 0, 0),
(158, 173, 4, 1, 0, 0),
(159, 173, 5, 1, 1, 1),
(160, 173, 6, 1, 1, 0),
(161, 173, 7, 1, 0, 0),
(162, 174, 1, 1, 1, 0),
(163, 174, 2, 1, 1, 0),
(164, 174, 3, 1, 0, 0),
(165, 174, 4, 1, 0, 0),
(166, 174, 5, 1, 1, 1),
(167, 174, 6, 1, 1, 0),
(168, 174, 7, 1, 0, 0),
(176, 169, 1, 1, 1, 1),
(177, 169, 2, 1, 1, 0),
(178, 169, 3, 1, 0, 0),
(179, 169, 4, 1, 0, 0),
(180, 169, 5, 1, 1, 1),
(181, 169, 6, 1, 1, 0),
(182, 169, 7, 1, 1, 0),
(183, 177, 1, 1, 1, 0),
(184, 177, 2, 1, 1, 0),
(185, 177, 3, 1, 0, 0),
(186, 177, 4, 1, 0, 0),
(187, 177, 5, 1, 1, 1),
(188, 177, 6, 1, 1, 0),
(189, 177, 7, 1, 0, 0),
(190, 178, 1, 1, 1, 0),
(191, 178, 2, 1, 1, 0),
(192, 178, 3, 1, 0, 0),
(193, 178, 4, 1, 0, 0),
(194, 178, 5, 1, 1, 1),
(195, 178, 6, 1, 1, 0),
(196, 178, 7, 1, 0, 0),
(197, 179, 1, 1, 1, 0),
(198, 179, 2, 1, 1, 0),
(199, 179, 3, 1, 0, 0),
(200, 179, 4, 1, 0, 0),
(201, 179, 5, 1, 1, 1),
(202, 179, 6, 1, 1, 0),
(203, 179, 7, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_parents`
--

CREATE TABLE `tbl_parents` (
  `table_id` int(11) NOT NULL,
  `id` int(11) NOT NULL COMMENT 'parent id',
  `school_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_parents`
--

INSERT INTO `tbl_parents` (`table_id`, `id`, `school_id`, `name`, `email`) VALUES
(1, 15, 1, 'Biplob Mudi', 'biplob@ablion.in'),
(2, 15, 1, '', ''),
(5, 18, 1, 'Samaresh', 'samaresh@mail.com'),
(6, 20, 1, '', ''),
(7, 21, 1, '', ''),
(8, 26, 23, 'Pratik', 'pratikdutta@gmail.com'),
(9, 38, 22, 'Emily', 'Emily@mail.com'),
(10, 42, 22, '', ''),
(14, 44, 22, 'A desai', 'desai@mail.com'),
(16, 46, 22, 'single child Test', 'singlechieldtest@mail.com'),
(17, 47, 22, 'Sankh', 'sankh@mail.com'),
(18, 48, 22, 'Tamal', 'tamal@mail.com'),
(20, 44, 22, '', ''),
(21, 62, 1, '', ''),
(23, 63, 1, 'Surajit', 'surajit@mail.com'),
(24, 63, 1, '', ''),
(25, 79, 1, '', ''),
(29, 83, 1, '', ''),
(31, 86, 1, '', ''),
(32, 87, 1, '', ''),
(33, 15, 1, 'Shipra Mudi', 'shipra@ablion.in'),
(34, 15, 1, '', ''),
(35, 88, 1, '', ''),
(36, 91, 1, '', ''),
(37, 92, 1, '', ''),
(38, 93, 1, '', ''),
(39, 94, 1, '', ''),
(40, 95, 1, 'Surajit Das', 'surajit@ablion.in'),
(45, 101, 1, '', ''),
(46, 102, 1, '', ''),
(47, 95, 1, 'Surajit Das', 'surajit@ablion.in'),
(48, 15, 22, '', ''),
(49, 159, 157, '', ''),
(50, 160, 157, '', ''),
(51, 161, 157, '', ''),
(52, 44, 1, '', ''),
(53, 171, 169, 'Biplob Mudi', 'biplob.mudi@gmail.com'),
(54, 175, 169, '', ''),
(55, 38, 22, '', ''),
(56, 176, 169, '', ''),
(57, 176, 169, '', ''),
(58, 44, 178, '', ''),
(59, 44, 178, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_parents_kids_link`
--

CREATE TABLE `tbl_parents_kids_link` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_parents_kids_link`
--

INSERT INTO `tbl_parents_kids_link` (`id`, `school_id`, `parent_id`, `student_id`) VALUES
(1, 1, 15, 1),
(2, 1, 15, 2),
(4, 1, 17, 4),
(5, 1, 18, 5),
(6, 1, 20, 6),
(7, 1, 21, 7),
(8, 23, 26, 8),
(9, 22, 38, 9),
(13, 22, 42, 13),
(14, 22, 17, 14),
(15, 22, 43, 15),
(16, 1, 17, 16),
(17, 22, 44, 17),
(18, 1, 45, 5),
(19, 1, 16, 1),
(21, 22, 46, 10),
(22, 22, 47, 11),
(23, 22, 48, 12),
(24, 23, 43, 18),
(27, 23, 26, 18),
(28, 22, 44, 19),
(29, 1, 61, 20),
(30, 1, 62, 0),
(31, 1, 16, 22),
(32, 1, 63, 23),
(33, 1, 63, 24),
(47, 1, 15, 27),
(48, 1, 15, 28),
(49, 1, 88, 28),
(50, 1, 91, 29),
(51, 1, 92, 1),
(53, 1, 93, 28),
(54, 1, 94, 30),
(55, 1, 95, 31),
(60, 1, 101, 34),
(61, 1, 102, 34),
(62, 1, 95, 35),
(63, 22, 15, 36),
(64, 157, 159, 37),
(65, 157, 160, 38),
(66, 157, 161, 39),
(67, 1, 44, 40),
(68, 169, 171, 41),
(69, 169, 175, 42),
(70, 22, 38, 43),
(71, 169, 176, 44),
(72, 169, 176, 45),
(73, 178, 44, 46),
(74, 178, 44, 47);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paymentgateway`
--

CREATE TABLE `tbl_paymentgateway` (
  `id` int(11) NOT NULL,
  `payment_gateway_name` varchar(255) NOT NULL,
  `label_for_ui` varchar(255) NOT NULL,
  `platform` varchar(200) NOT NULL COMMENT 'Multiple values. 1 = Admin, 2 = Web, 3 = Android, 4 = Ios',
  `default_gateway` tinyint(1) NOT NULL COMMENT '1 = This is the default gateway',
  `display_tip` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Active, 0 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_paymentgateway`
--

INSERT INTO `tbl_paymentgateway` (`id`, `payment_gateway_name`, `label_for_ui`, `platform`, `default_gateway`, `display_tip`, `status`) VALUES
(1, 'Razorpay', 'Razorpay', '2,3,4', 1, '', 1),
(2, 'Offline', 'Offline', '1', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payments_to_school`
--

CREATE TABLE `tbl_payments_to_school` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `razorpay_account_id` varchar(100) NOT NULL,
  `payment_ref_id` varchar(100) NOT NULL,
  `amount` float(20,2) NOT NULL,
  `payment_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payments_to_school`
--

INSERT INTO `tbl_payments_to_school` (`id`, `school_id`, `payment_id`, `razorpay_account_id`, `payment_ref_id`, `amount`, `payment_datetime`) VALUES
(3, 1, 18, 'acc_D7hn3seRC4j0qg', 'trf_DHFRNbNOKBZHG8', 1000.00, '2019-09-12 15:34:46'),
(4, 1, 19, 'acc_D7hn3seRC4j0qg', 'trf_DHG2tKqGjsfjDZ', 1500.00, '2019-09-12 16:10:17'),
(5, 1, 20, 'acc_D7hn3seRC4j0qg', 'trf_DHHgXSaj6vHbl3', 1000.00, '2019-09-12 17:46:30'),
(12, 1, 28, 'acc_D7hn3seRC4j0qg', 'trf_DIsCGZJpCtGGDK', 11200.00, '2019-09-16 18:08:31'),
(13, 1, 3, 'acc_D7hn3seRC4j0qg', 'trf_DT9dyR9muoAsWZ', 4042.00, '2019-10-12 17:42:33'),
(14, 1, 4, 'acc_D7hn3seRC4j0qg', 'trf_DT9kANrMQXq1Ts', 4042.00, '2019-10-12 17:48:25'),
(15, 1, 1, 'acc_D7hn3seRC4j0qg', 'trf_DTpdYCzIvi1umN', 16163.00, '2019-10-14 10:47:17'),
(16, 1, 4, 'acc_D7hn3seRC4j0qg', 'trf_DUJm5dur6LZS2Y', 15500.00, '2019-10-15 16:16:10'),
(17, 178, 1012, 'acc_D7hn3seRC4j0qg', 'trf_DXWBclToLHOrjD', 1700.00, '2019-10-23 18:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permissions`
--

CREATE TABLE `tbl_permissions` (
  `id` bigint(20) NOT NULL,
  `section_name` varchar(150) NOT NULL,
  `page_urls` text NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permissions`
--

INSERT INTO `tbl_permissions` (`id`, `section_name`, `page_urls`, `status`) VALUES
(1, 'Dashboard', 'dashBoard', 1),
(2, 'Information', 'manageClasses,manageSections,manageSubjects', 1),
(3, 'Manage Staff', 'manageTeachers', 1),
(4, 'Manage Time Table', 'manageTimeTable', 1),
(5, 'Manage Student', 'manageStudent', 1),
(6, 'Manage Notice', 'manageNotice', 1),
(7, 'General Info', 'manageHolidays,manageSyllabus,generalInformation,manageFeesStructure', 1),
(8, 'Report', 'attendanceReport,activityReport', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_register_device`
--

CREATE TABLE `tbl_register_device` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_register_device`
--

INSERT INTO `tbl_register_device` (`id`, `user_id`, `device_id`, `device_type`, `last_update`) VALUES
(1, 15, '', '', '0000-00-00 00:00:00'),
(2, 95, 'dthzke5fFnw:APA91bG7VmZVwtUHYG_8w-fJH3YxziNFdG6IXpCngpJYhQ9y0NAwTsQn9rv6wxqeth8dxLM8jVlH2NCeS8gDKcJ-hFsvBavYULBpulCDAQK5OmC16gBuDLJ-axsJAQcoLafIifXlt9n4', 'Android', '2019-09-06 17:01:00'),
(3, 8, 'dYpf_QXMLY4:APA91bGdQbgHhWcwKg_MsZ1_nJHkBJjkUkizsllwAofaLmEkuDNIghwa5VECtL5_UbaDrqdZaZmCwKkYX_FHh65Bt4h4kIQe58v58PeQxT4qD2FvLQ4R0fr8gBHDSY6N50PuXxHLTn-e', 'Android', '0000-00-00 00:00:00'),
(4, 15, 'dYpf_QXMLY4:APA91bGdQbgHhWcwKg_MsZ1_nJHkBJjkUkizsllwAofaLmEkuDNIghwa5VECtL5_UbaDrqdZaZmCwKkYX_FHh65Bt4h4kIQe58v58PeQxT4qD2FvLQ4R0fr8gBHDSY6N50PuXxHLTn-e', 'Andriod', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schools`
--

CREATE TABLE `tbl_schools` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact_person_name` varchar(255) NOT NULL,
  `mobile_no` bigint(20) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `country_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `file_url` text NOT NULL,
  `no_of_license` int(11) NOT NULL,
  `license_expiry_date` date NOT NULL,
  `expiry_module` int(11) NOT NULL COMMENT '0=expiry module on,1=expiry module off',
  `is_deleted` int(11) NOT NULL COMMENT '0=not deleted,1=deleted',
  `display_ads` int(11) NOT NULL COMMENT '0=enabled,1=disabled',
  `delete_time` datetime NOT NULL,
  `otp_verification` tinyint(1) NOT NULL COMMENT '0=false,1=true',
  `email_verification` tinyint(1) NOT NULL COMMENT '0=false,1=true',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_schools`
--

INSERT INTO `tbl_schools` (`id`, `name`, `contact_person_name`, `mobile_no`, `email_address`, `ip_address`, `country_id`, `address`, `file_url`, `no_of_license`, `license_expiry_date`, `expiry_module`, `is_deleted`, `display_ads`, `delete_time`, `otp_verification`, `email_verification`, `created_at`, `updated_at`) VALUES
(1, 'Model School', 'Siddhart Kapoor', 9749552668, 'biplob@ablion.in', '', 0, 'INDRAKANAN', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/school_logo/1_1566561775', 100, '2019-10-31', 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 0),
(2, 'St Merry School', 'Joseph', 9685478548, 'joseph@mail.com', '', 0, 'Test', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(3, 'St Thomas', 'Michele', 8547896587, 'mike@mail.com', '', 0, 'INDRAKANAN', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(22, 'Future Foundation', 'Emily Bose', 9434251530, 'aparajita@ablion.in', '', 0, 'A.C Mitra Lane Burdwan', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/school_logo/22_1569651810', 100, '2020-09-13', 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 0),
(23, 'Next Gen Education School', 'Pratik Dutt', 9932130035, 'pratik@ablion.in', '', 0, 'Mumbai', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(29, 'Global Business School', 'Harsh Patel', 9434114932, 'biplob.mudi@gmail.com', '', 0, 'burdwan', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(50, 'FALTU ESKOOL', 'Sankhyadip', 9832776530, 'sankhya555@gmail.com', '', 0, 'Northern Altas Porbotmala', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(55, 'tytyqety', 'yutt', 787687585, 'ffff@email.com', '', 0, 'tewffwefw', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(65, 'Loreto Convent', 'Miss Joseph', 9968544789, 'josh@mail.com', '', 0, '', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(66, 'New Age School', 'Ms. Annie Jobby', 9999999999, 'annie.jobby05@gmail.com', '', 0, 'Burdwan', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(76, 'Central school', 'Mohidhar Chatterjee', 9965874458, 'mahi@mail.com', '', 0, 'tset', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(78, 'hello', 'test hello', 9874563210, 'biplob1@ablion.in', '', 0, 'INDRAKANAN', '', 0, '0000-00-00', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(96, 'Ramkrishna School', 'Swami Nityananda', 7896896589, 'nitt@mail.com', '', 0, 'tset', '', 100, '2019-10-08', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(142, 'Ashirbad', 'scott', 9638527410, 'scott@mail.com', '', 0, '', '', 100, '2019-10-17', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(147, 'Saraswati Bidyalaya', 'Anup Saha', 7894561230, 'anup@mail.com', '', 0, '', '', 100, '2019-10-18', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(148, 'Adarsh School', 'Arun De', 1123456789, 'arun@mail.com', '', 0, '', '', 100, '2019-10-18', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(149, 'Siksha Sadan', 'Jyoti Roy', 7896541232, 'jyoti@mail.com', '', 0, '', '', 100, '2019-10-18', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(150, 'Siksha Sadan', 'Jyoti Roy', 7896541232, 'jyoti@mail.com', '', 0, '', '', 100, '2019-10-18', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(151, 'Rashbihari Bidyalaya', 'Sujoy', 4457896558, 'sujoy@mail.com', '', 0, '', '', 100, '2019-10-18', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(152, 'Rashbihari Bidyalaya', 'Sujoy', 4457896558, 'sujoy@mail.com', '', 0, '', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/school_logo/152_1567517123', 100, '2019-10-18', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(154, 'RamMohan High School', 'Swapan Das', 8799658745, 'swapan@mail.com', '', 0, '', '', 100, '2019-10-28', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(155, 'VIdyasagar School', 'Bibhas Pal', 7789658745, 'bibhas@mail.com', '', 0, '', '', 100, '2019-10-28', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(156, 'St. Joseph School', 'Joseph Johnson', 7789555545, 'biplob@ablion.in', '', 0, '', '', 100, '2019-11-09', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(157, 'Semester School', 'Semester', 7845129630, 'sem@mail.com', '', 0, '', '', 100, '2019-11-23', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(162, 'Protocol Address', 'test', 9632587410, 'test@mail.com', '103.43.80.131', 99, '', '', 100, '2019-11-25', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0),
(163, 'hello', 'test', 7894561230, 'hello@mail.com', '103.43.80.131', 99, 'Barddhaman, West Bengal, India', '', 100, '2019-11-25', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 1570777768),
(164, 'school 365', 'John Doe', 7845129630, 'john@mail.com', '103.43.80.131', 99, 'Barddhaman, West Bengal, India', '', 100, '2019-11-25', 0, 0, 0, '0000-00-00 00:00:00', 1, 1, 1570783618, 1570783618),
(165, 'School366', 'school366', 8745965231, 'school366@mail.com', '192.168.1.109', 99, '', '', 100, '2019-11-25', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 1570793651, 1570793651),
(166, 'school367', 'school367', 9965499654, 'school167@mail.com', '192.168.1.109', 99, '', '', 100, '2019-11-26', 0, 0, 0, '0000-00-00 00:00:00', 0, 1, 1570857753, 1570857753),
(167, 'School368', 'school368', 9749552668, 'school368@mail.com', '192.168.1.109', 99, '', '', 100, '2019-11-26', 0, 0, 0, '0000-00-00 00:00:00', 0, 1, 1570881101, 1570881101),
(168, 'Rolex School of Science', 'John Rolex', 9749552668, 'biplob.mudi@gmail.com', '192.168.1.109', 99, '', '', 100, '2019-11-16', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 1571293482, 1571293482),
(169, 'Home School', 'Biplob Mudi', 9749552668, 'biplob06@gmail.com', '192.168.1.109', 99, '', '', 100, '2019-11-16', 0, 0, 0, '0000-00-00 00:00:00', 1, 1, 1571309199, 1571309199),
(172, 'Rockford', 'Danny Ben', 2236547789, 'danny@mail.com', '192.168.1.109', 99, '', '', 100, '2019-11-17', 0, 0, 0, '0000-00-00 00:00:00', 1, 1, 1571376487, 1571376487),
(173, 'Semford', 'Dr. Bhadra', 5478966546, 'biplob.mudi@gmail.com', '192.168.1.109', 99, '', '', 100, '2019-11-17', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 1571381315, 1571381315),
(174, 'Test School', 'test person', 7894564567, 'biplob.mudi@gmail.com', '192.168.1.109', 99, '', '', 100, '2019-11-17', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 1571383188, 1571383188),
(177, 'Orient school', 'Biplob Mudi2', 9749552668, 'biplob.mudi@gmail.com', '192.168.1.109', 99, '', '', 100, '2019-11-21', 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 1571743160, 1571743160),
(178, 'pioneer school', 'A Datta', 9641631360, 'aparajita@solutiononline.co.in', '192.168.1.138', 99, 'Test', '', 100, '2019-11-22', 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 1571831960, 1571831960),
(179, 'New Age Public School', 'upasona', 8768673981, 'upasonabanejee068@gmail.com', '192.168.1.138', 99, '', '', 100, '2019-11-23', 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 1571898467, 1571898467);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_school_payment_settings`
--

CREATE TABLE `tbl_school_payment_settings` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `payment_enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = DIsabled, 1 = Enabled',
  `add_on_school_fees` int(11) NOT NULL COMMENT 'Amount in percentage',
  `deduct_from_school_fees` int(11) NOT NULL COMMENT 'Amount in percentage',
  `payment_gateway_reference_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_school_payment_settings`
--

INSERT INTO `tbl_school_payment_settings` (`id`, `school_id`, `payment_enabled`, `add_on_school_fees`, `deduct_from_school_fees`, `payment_gateway_reference_id`) VALUES
(1, 1, 1, 5, 0, ''),
(2, 178, 1, 5, 0, 'acc_D7hn3seRC4j0qg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section`
--

CREATE TABLE `tbl_section` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_section`
--

INSERT INTO `tbl_section` (`id`, `school_id`, `class_id`, `section_name`, `status`) VALUES
(1, 1, 1, 'A', 1),
(2, 1, 1, 'B', 1),
(3, 1, 1, 'C', 1),
(4, 1, 2, 'A', 1),
(5, 1, 2, 'B', 1),
(6, 1, 2, 'C', 1),
(7, 1, 3, 'A', 1),
(8, 1, 3, 'B', 1),
(9, 1, 3, 'C', 1),
(10, 1, 4, 'A', 1),
(11, 1, 4, 'B', 1),
(12, 1, 4, 'C', 1),
(13, 1, 6, 'A', 1),
(14, 1, 6, 'B', 1),
(15, 23, 9, 'Section A', 1),
(16, 23, 9, 'B', 1),
(17, 23, 9, 'C', 1),
(18, 23, 9, '2 D', 1),
(19, 22, 15, 'A', 1),
(21, 22, 15, 'B', 1),
(22, 22, 15, 'C', 1),
(23, 29, 16, 'BNS1A', 1),
(24, 29, 16, 'BNS1B', 1),
(25, 29, 16, 'BNS1C', 1),
(26, 29, 17, 'BNS2A', 1),
(27, 29, 17, 'BNS2B', 1),
(28, 29, 17, 'BNS2C', 1),
(29, 22, 7, 'A', 1),
(30, 22, 7, 'B', 1),
(31, 22, 12, 'A', 1),
(32, 22, 12, 'B', 1),
(33, 22, 14, 'A', 1),
(35, 22, 14, 'B', 1),
(36, 50, 19, 'swamiji', 1),
(37, 50, 19, 'gandhiji', 1),
(38, 50, 19, 'netaji', 1),
(39, 50, 20, 'swamiji', 1),
(40, 50, 20, 'gandhiji', 1),
(41, 50, 20, 'netaji', 1),
(42, 50, 20, 'gandhiji', 1),
(43, 50, 20, 'swamiji', 1),
(44, 50, 20, 'netaji', 1),
(45, 50, 21, 'sdsaA', 1),
(46, 50, 21, 'SADSAF', 1),
(47, 50, 22, 'DSFSDF', 1),
(48, 50, 22, 'FFBB', 1),
(49, 50, 22, 'SFMLO', 1),
(50, 50, 22, 'NMKLIK', 1),
(51, 1, 6, 'A Temp', 0),
(52, 1, 1, 'A Temp', 0),
(53, 1, 1, 'B Temp', 0),
(54, 1, 1, 'C Temp', 0),
(55, 1, 2, 'A Temp', 0),
(56, 1, 2, 'B Temp', 0),
(57, 1, 2, 'C Temp', 0),
(58, 157, 25, 'A', 1),
(59, 157, 25, 'B', 1),
(60, 1, 27, 'A', 1),
(61, 169, 28, 'A', 1),
(62, 178, 30, 'A', 1),
(63, 178, 30, 'B', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_semester`
--

CREATE TABLE `tbl_semester` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `semester_name` varchar(255) NOT NULL,
  `combined_term_results` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `combined_result_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_semester`
--

INSERT INTO `tbl_semester` (`id`, `school_id`, `session_id`, `class_id`, `semester_name`, `combined_term_results`, `combined_result_name`) VALUES
(2, 1, 1, 1, 'STD 1 (2019-20)', 0, ''),
(4, 1, 1, 2, 'STD 2 (2019-20)', 0, ''),
(5, 1, 1, 3, 'STD 3 (2019-20)', 0, ''),
(6, 157, 3, 25, 'class 1 (SESS 2019-20)', 0, ''),
(7, 157, 3, 26, 'class 2 (SESS 2019-20)', 0, ''),
(8, 157, 4, 25, 'class 1 (SESS 2020-21)', 0, ''),
(9, 1, 6, 27, 'Java Summer Class (November\'19-April\'20)', 0, ''),
(10, 169, 7, 28, 'Class 3 (2019-20)', 1, 'Block Test'),
(11, 22, 8, 7, 'STD 1 (2019-2020)', 0, ''),
(14, 22, 10, 7, 'STD 1 (Dec19-Nov20)', 0, ''),
(15, 178, 11, 30, 'STD 1 (2019-2020)', 0, ''),
(16, 178, 13, 30, 'STD 1 (2020-2021)', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `session_name` varchar(255) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`id`, `school_id`, `session_name`, `from_date`, `to_date`) VALUES
(1, 1, '2019-20', '2019-08-01', '2020-01-31'),
(2, 1, '2020-21', '2020-04-01', '2021-03-31'),
(3, 157, 'SESS 2019-20', '2019-04-01', '2020-03-31'),
(4, 157, 'SESS 2020-21', '2020-04-01', '2021-03-31'),
(5, 1, 'Five Months Session', '2019-07-01', '2019-11-30'),
(6, 1, 'November\'19-April\'20', '2019-11-05', '2020-04-04'),
(7, 169, '2019-20', '2019-04-01', '2020-03-31'),
(8, 22, '(2019-2020)', '2019-07-24', '2020-06-19'),
(9, 172, '2019-20', '2019-04-01', '2020-03-31'),
(10, 22, 'Dec19-Nov20', '2019-12-16', '2020-09-15'),
(11, 178, '2019-2020', '2019-03-06', '2020-02-20'),
(12, 22, '2020-2021', '2020-03-20', '2021-02-28'),
(13, 178, '2020-2021', '2020-03-20', '2021-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sms_credits`
--

CREATE TABLE `tbl_sms_credits` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `credit_in` int(11) NOT NULL,
  `credit_out` int(11) NOT NULL,
  `transaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sms_credits`
--

INSERT INTO `tbl_sms_credits` (`id`, `school_id`, `credit_in`, `credit_out`, `transaction_time`) VALUES
(1, 2, 0, 150, '2019-07-27 16:52:45'),
(2, 1, 500, 0, '2019-07-27 17:21:13'),
(3, 3, 200, 0, '2019-07-27 18:13:18'),
(4, 3, 250, 0, '2019-07-27 18:16:53'),
(5, 1, 0, 1, '2019-08-21 12:19:42'),
(6, 1, 0, 1, '2019-08-21 12:19:43'),
(7, 1, 0, 1, '2019-08-21 12:19:43'),
(8, 1, 0, 1, '2019-08-21 12:19:44'),
(9, 1, 0, 1, '2019-08-21 12:19:44'),
(10, 1, 0, 1, '2019-08-21 12:19:45'),
(11, 1, 0, 1, '2019-08-21 12:19:45'),
(12, 1, 0, 1, '2019-08-21 12:19:45'),
(13, 1, 0, 1, '2019-08-21 12:19:46'),
(14, 1, 0, 1, '2019-08-21 12:19:46'),
(15, 1, 0, 1, '2019-08-21 12:19:47'),
(16, 1, 0, 1, '2019-08-21 12:19:47'),
(17, 1, 0, 1, '2019-08-21 12:19:48'),
(18, 1, 0, 1, '2019-08-21 12:19:48'),
(19, 1, 0, 1, '2019-08-21 12:19:50'),
(20, 1, 0, 1, '2019-08-21 12:19:51'),
(21, 1, 0, 1, '2019-08-21 12:19:51'),
(22, 1, 0, 1, '2019-08-21 12:19:51'),
(23, 1, 0, 1, '2019-08-21 13:29:04'),
(24, 1, 0, 1, '2019-08-21 15:11:51'),
(25, 1, 0, 1, '2019-08-21 15:37:10'),
(26, 1, 0, 1, '2019-08-24 10:30:11'),
(27, 1, 0, 1, '2019-08-29 12:00:49'),
(28, 1, 0, 1, '2019-09-02 10:57:36'),
(29, 1, 0, 1, '2019-09-03 12:51:12'),
(30, 1, 0, 1, '2019-09-03 17:25:21'),
(31, 1, 0, 1, '2019-09-04 11:20:39'),
(32, 1, 0, 1, '2019-09-06 15:06:13'),
(33, 1, 0, 1, '2019-09-06 15:06:13'),
(34, 1, 0, 1, '2019-09-06 15:06:13'),
(35, 1, 0, 1, '2019-09-06 15:06:14'),
(36, 1, 0, 1, '2019-09-06 15:06:14'),
(37, 1, 0, 1, '2019-09-06 15:06:15'),
(38, 1, 0, 1, '2019-09-06 15:06:15'),
(39, 1, 0, 1, '2019-09-06 15:06:16'),
(40, 1, 0, 1, '2019-09-06 15:06:16'),
(41, 1, 0, 1, '2019-09-06 15:06:16'),
(42, 1, 0, 1, '2019-09-06 15:06:17'),
(43, 1, 0, 1, '2019-09-06 15:06:17'),
(44, 1, 0, 1, '2019-09-06 15:06:18'),
(45, 1, 0, 1, '2019-09-06 15:06:19'),
(46, 1, 0, 1, '2019-09-06 15:06:19'),
(47, 1, 0, 1, '2019-09-06 15:06:19'),
(48, 1, 0, 1, '2019-09-06 15:06:20'),
(49, 1, 0, 1, '2019-09-06 15:06:20'),
(50, 1, 0, 1, '2019-09-06 15:06:20'),
(51, 1, 0, 1, '2019-09-06 15:11:07'),
(52, 1, 0, 1, '2019-09-06 15:12:03'),
(53, 1, 0, 1, '2019-09-06 15:46:07'),
(54, 1, 0, 1, '2019-09-06 16:34:06'),
(55, 1, 2, 0, '2019-09-14 11:16:56'),
(56, 1, 0, 1, '2019-09-14 11:30:58'),
(57, 1, 0, 1, '2019-09-14 11:34:48'),
(58, 1, 50, 0, '2019-09-14 12:15:07'),
(59, 1, 0, 1, '2019-09-14 17:08:19'),
(60, 1, 0, 1, '2019-09-14 17:08:31'),
(61, 1, 0, 1, '2019-09-14 17:08:31'),
(62, 1, 0, 1, '2019-09-14 17:17:05'),
(63, 1, 0, 1, '2019-09-14 17:17:16'),
(64, 1, 0, 1, '2019-09-14 17:17:16'),
(65, 1, 0, 1, '2019-09-14 17:17:28'),
(66, 1, 0, 1, '2019-09-14 17:17:28'),
(67, 1, 0, 1, '2019-09-14 17:17:40'),
(68, 1, 0, 1, '2019-09-14 17:17:40'),
(69, 1, 0, 1, '2019-09-14 17:17:53'),
(70, 1, 0, 1, '2019-09-14 17:18:06'),
(71, 1, 0, 1, '2019-09-14 17:18:18'),
(72, 1, 0, 1, '2019-09-14 17:18:30'),
(73, 1, 0, 1, '2019-09-14 17:18:36'),
(74, 1, 0, 1, '2019-09-14 17:18:48'),
(75, 1, 0, 1, '2019-09-14 17:18:54'),
(76, 1, 0, 1, '2019-09-14 17:19:06'),
(77, 1, 0, 1, '2019-09-14 17:19:18'),
(78, 1, 0, 1, '2019-09-14 17:19:18'),
(79, 1, 0, 1, '2019-09-14 17:19:30'),
(80, 1, 0, 1, '2019-09-14 17:19:30'),
(81, 1, 0, 1, '2019-09-14 17:19:30'),
(82, 1, 0, 1, '2019-09-14 17:21:08'),
(83, 1, 0, 1, '2019-09-14 17:34:45'),
(84, 1, 0, 1, '2019-09-14 17:34:57'),
(85, 1, 0, 1, '2019-09-14 17:34:57'),
(86, 1, 0, 1, '2019-09-14 17:35:09'),
(87, 1, 0, 1, '2019-09-14 17:35:09'),
(88, 1, 0, 1, '2019-09-14 17:35:09'),
(89, 1, 0, 1, '2019-09-14 17:35:15'),
(90, 1, 0, 1, '2019-09-14 17:35:21'),
(91, 1, 0, 1, '2019-09-14 17:35:21'),
(92, 1, 0, 1, '2019-09-14 17:35:21'),
(93, 1, 0, 1, '2019-09-14 17:36:57'),
(94, 1, 0, 1, '2019-09-14 17:37:08'),
(95, 1, 0, 1, '2019-09-14 17:37:08'),
(96, 1, 0, 1, '2019-09-14 17:37:20'),
(97, 1, 0, 1, '2019-09-14 17:37:20'),
(98, 1, 0, 1, '2019-09-14 17:37:20'),
(99, 1, 0, 1, '2019-09-14 17:37:26'),
(100, 1, 0, 1, '2019-09-14 17:37:32'),
(101, 1, 0, 1, '2019-09-14 17:37:32'),
(102, 1, 0, 1, '2019-09-14 17:37:32'),
(103, 1, 0, 1, '2019-09-16 18:06:16'),
(104, 1, 0, 1, '2019-09-16 18:08:31'),
(105, 22, 500, 0, '2019-09-30 11:15:26'),
(106, 1, 0, 1, '2019-10-12 17:42:34'),
(107, 1, 0, 1, '2019-10-12 17:48:25'),
(108, 1, 0, 1, '2019-10-15 16:16:10'),
(109, 1, 0, 1, '2019-10-17 15:11:25'),
(110, 1, 0, 1, '2019-10-17 15:38:43'),
(111, 169, 10, 0, '2019-10-21 17:11:28'),
(112, 169, 0, 1, '2019-10-21 17:11:34'),
(113, 169, 0, 1, '2019-10-21 17:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `registration_no` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `gender` tinyint(4) NOT NULL COMMENT '1=male,2=female',
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `class_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `roll_no` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_deleted` int(11) NOT NULL COMMENT '0=not deleted,1=deleted ',
  `delete_time` datetime NOT NULL,
  `manual_fees_setting` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Disabled, 1 = Enabled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`id`, `school_id`, `registration_no`, `name`, `dob`, `gender`, `father_name`, `mother_name`, `class_id`, `semester_id`, `section_id`, `roll_no`, `status`, `is_deleted`, `delete_time`, `manual_fees_setting`) VALUES
(1, 1, 'N01', 'Aarav Srivastav', '2012-05-31', 1, 'Aarush Srivastav', 'Kaira Srivastav', 1, 2, 2, '01', 1, 0, '0000-00-00 00:00:00', 1),
(2, 1, 'N02', 'Satish Srivastav', '2014-05-09', 1, 'Aarush Srivastav', 'Kaira Srivastav', 1, 2, 2, '02', 1, 0, '0000-00-00 00:00:00', 0),
(4, 1, '1234123', 'Anup Roy', '2010-05-09', 1, 'M S Roy', 'S Roy', 3, 0, 8, '01', 1, 0, '0000-00-00 00:00:00', 0),
(5, 1, '12341234', 'Joyti Kumar', '2011-06-22', 1, 'sd kumar', 'p sahu', 3, 0, 8, '02', 1, 0, '0000-00-00 00:00:00', 0),
(6, 1, '1458752', 'Alo Sinha', '2012-05-12', 2, 'DN Sinha', 'A sinha', 1, 2, 2, '22', 1, 0, '0000-00-00 00:00:00', 0),
(7, 1, '251455236', 'Nivedita Bose', '2013-02-01', 2, 'aa', 'aa', 1, 2, 2, '36', 1, 0, '0000-00-00 00:00:00', 0),
(8, 23, '2345', 'Ganesh Biswas', '2010-06-07', 1, 'Pritam Biswas', 'Riya Biswas', 9, 0, 15, '34', 1, 0, '0000-00-00 00:00:00', 0),
(9, 22, '1001', 'Laboni Dey', '2010-08-05', 2, 'S S Dey', 'L Dey', 15, 0, 19, '01', 1, 0, '0000-00-00 00:00:00', 0),
(10, 22, '1002', 'Janhabi sharma', '2014-06-17', 2, 'M D Sharma', 'S Sharma', 7, 0, 29, '02', 1, 0, '0000-00-00 00:00:00', 0),
(11, 22, '1003', 'Shankha Chowdhury', '2014-06-26', 1, 'S S Chowdhury', 'Nilam Chowdhury', 7, 0, 29, '03', 1, 0, '0000-00-00 00:00:00', 0),
(12, 22, '1004', 'Kingshuk Sen', '2014-01-04', 1, 'K Sen', 'M sen', 7, 0, 29, '05', 1, 0, '0000-00-00 00:00:00', 0),
(13, 22, '1006', 'Rahul Singh', '2015-02-25', 1, 'R Singh', 'Parmit Singh', 7, 0, 29, '07', 1, 0, '0000-00-00 00:00:00', 0),
(14, 22, '1007', 'Parmit Patel', '2014-08-12', 2, 'Rajib Patel', 'Madhu Patel', 7, 0, 29, '10', 1, 0, '0000-00-00 00:00:00', 0),
(15, 22, '1010', 'Soumi Jash', '2015-03-01', 2, 'S Jash', 'K Jash', 7, 0, 29, '12', 1, 0, '0000-00-00 00:00:00', 0),
(16, 1, '1011', 'Ruchira Banerjee', '2019-03-03', 2, 'Kousik Banerjee', 'Rita Banerjee', 6, 0, 51, '14', 1, 0, '0000-00-00 00:00:00', 0),
(17, 22, '1020', 'Arjun Desai', '2014-02-12', 1, 'a b Desai', 's desai', 7, 11, 29, '20', 1, 0, '0000-00-00 00:00:00', 1),
(18, 23, '34r33q', 'Kuntal Nandi', '2012-11-01', 1, 'sdfsdfsdfsdf', 'sdfsdfsdf', 9, 0, 15, '5', 1, 0, '0000-00-00 00:00:00', 0),
(19, 22, '985632', 'Kajal Desai', '2014-12-23', 2, 'a b Desai', 's desai', 12, 0, 31, '15', 1, 0, '0000-00-00 00:00:00', 0),
(20, 1, 'SRHS15897', 'Ruso Mukherjee', '1989-06-06', 1, 'Arnab Mukherjee', 'Tania Mukherjee', 1, 2, 2, '19950216', 1, 0, '0000-00-00 00:00:00', 0),
(21, 1, 'KL201', 'Subir Mudi', '1975-06-03', 1, 'Subhas Mudi', 'Sumana Mudi', 1, 2, 2, '225', 1, 0, '0000-00-00 00:00:00', 0),
(22, 1, 'KL202', 'Subir Mudi', '1975-06-03', 1, 'Subhas Mudi', 'Sumana Mudi', 1, 2, 2, '225', 1, 0, '0000-00-00 00:00:00', 0),
(23, 1, 'ANP01', 'Subhadip', '1999-06-21', 1, 'Sankhya', 'sumana', 1, 2, 2, '458', 1, 0, '0000-00-00 00:00:00', 0),
(24, 1, 'ANF66', 'Aritra', '1997-06-12', 1, 'Sumit', 'sumi', 1, 2, 2, '55', 1, 0, '0000-00-00 00:00:00', 0),
(25, 1, '7872059427', 'Biplob Mudi', '2014-07-31', 1, 'Manik Lal Mudi', 'Chandrabati Mudi', 1, 2, 2, '01', 1, 0, '0000-00-00 00:00:00', 0),
(27, 1, 'SRHS1995aa', 'Brijesh Shrivastav', '2013-07-20', 1, 'Aarush Srivastav', 'Kaira Srivastav', 2, 0, 56, '66', 1, 0, '0000-00-00 00:00:00', 1),
(28, 1, 'SRHS1995bbb', 'Subham Shrivastav', '2011-07-18', 1, 'Aarush Srivastav', 'Kaira Srivastav', 2, 0, 56, '67', 1, 0, '0000-00-00 00:00:00', 0),
(29, 1, 'S77', 'Siddharam Das', '2012-07-16', 1, 'Saddy Das', 'Swiddy Das', 2, 0, 5, '09', 1, 0, '0000-00-00 00:00:00', 1),
(30, 1, '12', 'Saurav banerjee', '2010-03-10', 1, 'Manik Banerjee', 'sumana banerjee', 1, 2, 2, '12', 1, 0, '0000-00-00 00:00:00', 0),
(31, 1, '12131213', 'Surajit Das', '0000-00-00', 2, 'Surajit Das', 'Jhuma Mukherjee Das', 1, 2, 1, '1', 1, 0, '0000-00-00 00:00:00', 1),
(34, 1, '50001', 'Ayan Khan', '2012-08-20', 1, 'Raja Khan', 'Sulata Khan', 1, 2, 2, '56', 1, 0, '0000-00-00 00:00:00', 0),
(35, 1, '1213121355', 'Jhuma Mukherjee', '0000-00-00', 2, 'Surajit Das', 'Jhuma Mukherjee Das', 1, 0, 1, '11', 1, 0, '0000-00-00 00:00:00', 1),
(36, 22, 'S1510', 'Pradip Mukherjee', '2017-05-08', 1, 'Anirban', 'Paramita', 7, 0, 29, '45', 1, 0, '0000-00-00 00:00:00', 0),
(37, 157, '01', 'Student A', '2014-04-02', 2, 'Father A', 'Mother A', 25, 6, 58, '01', 1, 0, '0000-00-00 00:00:00', 0),
(38, 157, '02', 'Student B', '2014-04-17', 1, 'Father B', 'Mother B', 25, 6, 58, '02', 1, 0, '0000-00-00 00:00:00', 0),
(39, 157, '01-session 2020-21', 'Student C', '2015-06-10', 1, 'Father C', 'Mother C', 25, 8, 58, '01', 1, 0, '0000-00-00 00:00:00', 0),
(40, 1, '16323', 'Mihir Datta', '2008-10-14', 1, 'N Datta', 'S Datta', 27, 9, 60, '21', 1, 0, '0000-00-00 00:00:00', 0),
(41, 169, '001', 'Sumit Roy', '1984-07-31', 1, 'Pramodh Roy', 'Pratima Roy', 28, 10, 61, '01', 1, 0, '0000-00-00 00:00:00', 0),
(42, 169, '501', 'Raja Mukherjee', '2014-05-10', 1, 'Suman Mukherjee', 'Sumona Mukherjee', 28, 10, 61, '15', 1, 0, '0000-00-00 00:00:00', 0),
(43, 22, '256547', 'Anjali Basu', '2009-04-12', 2, 'M Basu', 'S Basu', 15, 0, 19, '30', 1, 0, '0000-00-00 00:00:00', 0),
(45, 169, '5', 'Aritra Chatterjee', '2011-08-14', 1, 'Gaurav Chatterjee', 'Lekha Chatterjee', 28, 10, 61, '19', 1, 0, '0000-00-00 00:00:00', 0),
(46, 178, '3656562', 'Apu Bose', '2019-10-16', 2, 'S bose', 'S bose', 30, 15, 62, '25', 1, 0, '0000-00-00 00:00:00', 0),
(47, 178, '54', 'd bose', '2019-10-15', 1, 'L Bose', 'P Bose', 30, 16, 62, '6', 1, 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_details`
--

CREATE TABLE `tbl_student_details` (
  `student_id` int(11) NOT NULL,
  `address` longtext NOT NULL,
  `aadhar_no` bigint(20) DEFAULT NULL,
  `emergency_contact_person` varchar(120) NOT NULL,
  `emergency_contact_no` bigint(20) DEFAULT NULL,
  `blood_group` varchar(10) NOT NULL,
  `file_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_details`
--

INSERT INTO `tbl_student_details` (`student_id`, `address`, `aadhar_no`, `emergency_contact_person`, `emergency_contact_no`, `blood_group`, `file_url`) VALUES
(1, 'INDRAKANAN', 0, '', 9749552668, '', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/student/AaravSrivastav_1563887335'),
(2, '', 0, '', 0, '', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/student/SatishSrivastav_1563887371'),
(4, 'ac bose road', 1458325698567010, 'sk roy', 1236147510238964, 'O ve+', ''),
(5, 'abcd', 9874586952140215, 'aa', 9514752863, 'B ve+', ''),
(6, 'ffff', 7896589625410121, 'aa', 9856254102, 'O -ve', ''),
(7, 'aa', 3698563201458965, 'aa', 9555220014, 'AB +ve', ''),
(8, '', 0, '', 0, '', ''),
(9, 'Chotonilpur Burdwan', 12345678910111, 'Lata Dey', 9475126766, 'B +ve', ''),
(10, 'Birhata Burdwan', 14774102589632, 'P Sharma', 6320120145, 'AB +Ve', ''),
(11, 'Kantapukur Burdwan', 7777555566662222, 'N Chowdhury', 9633698745, 'O +Ve', ''),
(12, 'Khoshbagan Burdwan', 36925896352147, 'M Sen', 6541236961, 'AB+Ve', ''),
(13, 'Bara Bazar Burdwan', 9223372036854775807, 'P Singh', 9944444400, 'B +Ve', ''),
(14, 'Baronilpur Burdwan', 1122330022556644, 'G Patel', 9434251527, 'B +Ve', ''),
(15, 'Ullash Burdwan', 7788445511229966, 'M Mitra', 9475126766, 'A +ve', ''),
(16, 'DVC More Burdwan', 5555555566666666, 'L Basu', 9434251530, 'O +Ve', ''),
(17, 'ac mita lane burdwan', 1471471010254214, 'l desai', 6541452145, 'o +ve', ''),
(18, '', 0, '', 0, '', ''),
(19, 'a c mitra lane', 96325625412014, 'I desai', 365363936520122, 'A +ve', ''),
(20, '', 0, '', 0, '', ''),
(21, 'testr', 0, 'ppo', 0, 'l+', ''),
(22, '', 0, '', 0, '', ''),
(23, '', 0, '', 0, '', ''),
(24, '', 0, '', 0, '', ''),
(25, '', 0, '', 0, '', ''),
(27, 'INDRAKANAN', 0, '', 9749552668, '', ''),
(28, '', 0, '', 0, '', ''),
(29, 'INDRAKANAN', 0, '', 9749552668, '', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/student/SiddharamDas_1565260662'),
(30, '', 0, '', 0, '', ''),
(31, 'Dhaniakhali', 0, 'Surajit Das', 9851661168, 'B+', ''),
(34, 'B.V Road', 789655895458, 'Suman Khan', 8896588745, 'A+', ''),
(35, '', 0, 'Surajit Das', 9474728606, '', ''),
(36, '', 0, '', 9749552668, '', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/student/PradipMukherjee_1569650389'),
(37, '', 0, '', 0, '', ''),
(38, '', 0, '', 0, '', ''),
(39, '', 0, '', 0, '', ''),
(40, 'bwn', 141654646131, '', 9874585214, '', ''),
(41, '', 0, '', 0, '', ''),
(42, '', 0, '', 0, '', ''),
(43, 'A.C mitra lane burdwan', 258954125630, 'reba roy', 9632562541, 'B+', ''),
(44, '', 0, '', 0, '', ''),
(45, '', 0, '', 0, '', ''),
(46, 'burdwan', 7898565412512, 'M datta', 9896525412, 'O+', ''),
(47, 'bwn', 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_semester_link`
--

CREATE TABLE `tbl_student_semester_link` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `roll_no` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subjects`
--

CREATE TABLE `tbl_subjects` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `subject_name` varchar(128) NOT NULL,
  `subject_code` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subjects`
--

INSERT INTO `tbl_subjects` (`id`, `school_id`, `subject_name`, `subject_code`, `status`) VALUES
(1, 1, 'English', 'ENG', 1),
(2, 1, 'Bengali', 'BEN', 1),
(3, 1, 'Life Science', 'LIFE SC', 1),
(4, 1, 'Physical Science', 'PH Sc', 1),
(5, 1, 'Geography', 'GEO', 1),
(6, 1, 'History', 'HIS', 1),
(7, 1, 'Mathematics', 'MATH', 1),
(8, 1, 'Environmental Science', 'ENV SC', 1),
(9, 23, 'English', 'EN', 1),
(10, 23, 'English Reasoning', 'EN', 1),
(11, 23, 'Mathametics', 'MH', 0),
(12, 23, 'History', 'HIS', 1),
(13, 23, 'Environmental Science', 'ES', 1),
(14, 22, 'ENGLISH (Language)', 'Eng-1', 1),
(15, 22, 'ENGLISH (Literature)', 'Eng-2', 1),
(16, 22, 'MATHEMATICS', 'Maths-1', 1),
(17, 22, 'History', 'H1', 1),
(18, 22, 'GEOGRAPHY', 'G-1', 1),
(19, 22, 'SANSKRIT', 'SK-1', 0),
(20, 22, 'BENGALI (Language)', 'Beng-1', 1),
(21, 22, 'BENGALI-2', 'Beng-2', 1),
(22, 22, 'PHYSICS', 'P-1', 1),
(23, 22, 'CHEMISTRY', 'C-1', 1),
(24, 22, 'BIOLOGY', 'Bio-1', 1),
(25, 22, 'COMPUTER', 'Comp-1', 1),
(26, 22, 'COMMERCE', 'Comm-1', 1),
(27, 22, 'MORAL SCIENCE', 'MS-1', 1),
(28, 29, 'Information Technology Management', '', 1),
(29, 29, 'Financial  Management', '', 1),
(30, 29, 'Human Resource Management', '', 1),
(31, 29, 'Risk management', '', 1),
(32, 29, 'Business Management', '', 1),
(33, 22, 'HINDI', '', 1),
(34, 50, 'sub1', '', 1),
(35, 50, 'sub2', '', 1),
(36, 50, 'sub3', '', 1),
(37, 50, 'sub4', '', 1),
(38, 50, 'sub5', '', 1),
(39, 50, 'sub6', '', 1),
(40, 50, 'sub7', '', 1),
(41, 50, 'sub8', '', 1),
(42, 50, 'sub9', '', 1),
(43, 50, 'sub10', '', 1),
(44, 157, 'English', '', 1),
(45, 157, 'Hindi', '', 1),
(46, 157, 'Bengali', '', 1),
(47, 169, 'Geography', '', 1),
(48, 169, 'English', '', 1),
(49, 169, 'Bengali', '', 1),
(50, 169, 'Mathematics', '', 1),
(51, 169, 'History', '', 1),
(52, 169, 'Life Science', '', 1),
(53, 169, 'Physical Science', '', 1),
(54, 169, 'Environmental Science', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_syllabus`
--

CREATE TABLE `tbl_syllabus` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `syllabus_text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_syllabus`
--

INSERT INTO `tbl_syllabus` (`id`, `school_id`, `class_id`, `syllabus_text`) VALUES
(7, 1, 1, '<p>CBSE&nbsp;board is specified as a prestigious educational board of India as it evaluates students&rsquo; learning needs from time to time and according to that, it amends its syllabus and consequently, it makes necessary changes in other learning resources including worksheets and question papers. Moreover, CBSE board maintains a trustworthy aspect in all manners. Most importantly, it keeps its syllabus updated with all current information and hence, students, who are pursuing their studies under this board, can also keep themselves updated in all respects. Moreover, CBSE syllabus is quite worthwhile for students as all important topics are incorporated in each CBSE syllabus in a sequential manner. The board sets its each syllabus in such a way that can provide a thorough knowledge about any subject as per students&rsquo; standard. Therefore, by following CBSE syllabus rigorously, students become prepared for any competitive exams.</p><p>CBSE syllabus for class 1 is ideally structured for class 1 students. Maths, English, Hindi, Science are included in this syllabus. According to students&rsquo; standard, each topic is included in this syllabus along with relevant attractive images. This syllabus is deliberately designed to meet students&rsquo; learning requirements in a thorough manner. Moreover, this syllabus helps students in enhancing their learning capabilities and other skills including communication skill. Moreover, the CBSE board also offers suitable worksheets and appropriate question papers for this class. Students can get the syllabus for class 1 CBSE online.</p><p>English is an important subject and knowing English helps students in choosing their desired career. A good grounding is quite necessary to gain a thorough understanding in this subject. According to the standard, the CBSE board prepares each syllabus in a suitable manner. In this context, CBSE English syllabus for class 1 is an ideal example of a well-structured syllabus. All basic topics are included in this syllabus along with some colorful images and relevant examples so that students feel comfortable to follow this syllabus. Spelling errors and other important topics are included in this syllabus in a sequential manner. Moreover, students should follow this syllabus thoroughly to make a good base of this syllabus.</p><p>Hindi is included in the CBSE syllabus. As per the students&rsquo; standard, the board includes this subject in each syllabus. It is also available for class 1 students. The CBSE Hindi syllabus for class 1 contains the basic knowledge of this subject. Each chapter is included in this syllabus along with the informative examples and colorful images. Therefore, students feel comfortable to follow this syllabus thoroughly. Hindi Alphabets, word making, missing letter searching, basic understanding of Hindi grammar and other relevant topics are included in this syllabus. Additionally, students can learn the Hindi names of different animals, fruits and others. Moreover, students should follow CBSE Hindi syllabus for class 1 rigorously to understand the advanced level included in higher class.</p><p>Math is a basic subject and students start learning Math from their childhood. Students become familiar with this subject when they start counting balls and their other toys. Keeping in mind students&rsquo; learning needs, the CBSE board has designed suitable Math syllabus for each class. CBSE syllabus for class 1 Maths is a well-evaluated syllabus, through which students can learn this subject thoroughly. All important Math topics are included in this syllabus in a sequential manner. Therefore, students can learn this subject comfortably. &nbsp;Apart from the basic chapters, the board also prepares some Math assessments and includes these in this syllabus to give students a thorough understanding about this subject.</p>'),
(8, 22, 7, '<p>New Syllabus for Std 1</p>'),
(9, 23, 9, '<p>CBSE Syllabus for Class 2 English</p><p>English is a basic subject and it is included in each CBSE syllabus in a requisite manner. CBSE syllabi are prepared under the guidance of subject experts and hence, each CBSE syllabus is structured in such a&nbsp;manner that can meet students&rsquo; learning needs properly. This syllabus is designed with colorful images and relevant examples. Therefore, students feel comfortable to follow this syllabus. Additionally, based on the students&rsquo; learning capability, the board prepares some useful assessments which make this subject more interesting for them. Through this syllabus, students can get to know the basic English grammar, as well. Moreover, students should collect and follow this syllabus properly to build a good grounding of this subject.</p><p>&nbsp;</p><p>&nbsp;<a href=\"http://cbse.edurite.com/downloadAttachment/4460\">&nbsp;&nbsp;Download CBSE Class 2 English Syllabus</a></p><p>CBSE Syllabus for Class 2 Maths</p><p>Math is an interesting subject and to get good command over this subject, students need to have a good grounding of Math. Keeping in mind this scenario, the CBSE board has designed suitable Math syllabus for each class. CBSE syllabus for class 2 Maths is ideally structured by including all important topics in a sequential manner. Through this syllabus, students can learn numbers, addition, subtraction, multiplication, division and other mathematical applications thoroughly. Moreover, this syllabus is perfectly designed so by following this syllabus thoroughly, students can build a good base for this subject. Additionally, students can practice some Maths worksheets designed by this board.</p><p>&nbsp;</p><p>&nbsp;<a href=\"http://cbse.edurite.com/downloadAttachment/3522\">&nbsp;&nbsp;Download CBSE Class 2 Maths Syllabus</a></p><p>CBSE Syllabus for Class 2 Science</p><p>Science is a vital subject, which gives us knowledge about many incidents happen in and around our daily life. This subject is included in each CBSE syllabus in a requisite manner. CBSE syllabus for class 2 Science is a worthwhile syllabus as it gives students a thorough understanding about many Science topics which are suitable as per their standard. Some basic topics of Phyiscs, Chemistry and Biology are included in this syllabus in a sequential manner. Moreover, the board explains each topic clearly by using a good number of examples along with relevant images. Therefore, students feel comfortable to follow this syllabus thoroughly. Students can also practice some science worksheets to get clear knowledge about this subject.</p><p>&nbsp;</p><p>&nbsp;<a href=\"http://cbse.edurite.com/downloadAttachment/3523\">&nbsp;&nbsp;Download CBSE Class 2 Science Syllabus</a></p><p>CBSE Syllabus for Class 2 Hindi</p><p>Hindi is included in each CBSE syllabus in a requisite manner. According to the student&rsquo;s standard, the board prepares each Hindi syllabus in a right manner by including all topics along with their relevant images and examples. Therefore, students feel comfortable to follow this syllabus throughout the academic session. CBSE syllabus for class 2 Hindi is a well-structured syllabus in all manners. Word making, missing letter searching and some interesting assessments are included in this syllabus. Additionally, through this syllabus, students can get basic understanding about Hindi grammar which is quite necessary to build a good understanding in this subject. Moreover, students are suggested to follow CBSE syllabus for class 2 Hindi thoroughly.</p><p>&nbsp;</p><p>&nbsp;<a href=\"http://cbse.edurite.com/downloadAttachment/4461\">&nbsp;&nbsp;Download CBSE Class 2 Hindi Syllabus</a></p>'),
(10, 23, 11, '<p>A</p>'),
(11, 23, 11, '<p>B</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_system_emails`
--

CREATE TABLE `tbl_system_emails` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `variable_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `from_email_variable` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `logic` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `cron_details` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_system_emails`
--

INSERT INTO `tbl_system_emails` (`id`, `name`, `variable_name`, `from_email_variable`, `logic`, `cron_details`) VALUES
(1, 'Forgot Password Email', 'company_forgot_password', 'DONOT_REPLY_EMAIL', 'Password recovery email from company panel.\r\nUsed for email only', ''),
(3, 'Fees Payment Success Mail To Parent', 'fees_payment_success_mail_to_parent', 'DONOT_REPLY_EMAIL', 'After successful payment, Parent will get this email.\r\nUsed for SMS, Email, Push', ''),
(4, 'Send Notice', 'notice', 'DONOT_REPLY_EMAIL', 'After sending notice, all teacher and/or parent will receive the notice.\r\nUsed for SMS, Email, Push', ''),
(5, 'Send diary from teacher to parent', 'send_diary_from_teacher_to_parent', 'DONOT_REPLY_EMAIL', 'After writing diary teacher will send diary to all or particular parent. \r\nUsed for SMS, Email, Push', ''),
(6, 'Send diary from parent to teacher', 'send_diary_from_parent_to_teacher', 'DONOT_REPLY_EMAIL', 'After writing diary parent will send diary to particular teacher. \r\nUsed for SMS, Email, Push', ''),
(7, 'send class note to all parents by class and section', 'send_daily_work_to_all_parent_by_class_section', 'DONOT_REPLY_EMAIL', 'When classwork submitted and published, notification will send to all parents by class and section.\r\nUsed for SMS, Email, Push', ''),
(8, 'Attendance Absent notification', 'attendance_absent', 'DONOT_REPLY_EMAIL', 'If any student absent in a class then notification will sent to their parents.\r\nUsed for SMS, Email, Push', ''),
(9, 'Attendance Present notification', 'attendance_present', 'DONOT_REPLY_EMAIL', 'If any student present in a class then notification will sent to their parents.\r\nUsed for SMS, Email, Push', ''),
(10, 'Temporary class notification', 'temporary_class', 'DONOT_REPLY_EMAIL', 'If a temporary class is assign to a teacher then teacher will get notification.\r\nUsed for SMS, Email, Push', ''),
(11, 'Institute Email Verification', 'institute_email_verification', 'DONOT_REPLY_EMAIL', 'Email triggered from dashboard of school who have not verified email yet.\r\nEmail only.', ''),
(12, 'Institute Account not set up properly', 'improper_school_account', 'DONOT_REPLY_EMAIL', 'Email triggered from Cron to school who have not set up there account properly. Email only', 'Cron runs everyday at 2:00 am (Indian time). \r\nUser gets email once every 3rd,7th,15th,21st day upto 30days.'),
(13, 'Account about to expire', 'account_about_to_expire', 'DONOT_REPLY_EMAIL', 'Email triggered from Cron to school before \r\n1 day of account expiry date. Email only.', 'Cron runs everyday at 2:30 am (Indian time). \r\nUser gets email once before 1 day of account expiring date.'),
(14, 'Institute Account set up properly', 'proper_school_account', 'DONOT_REPLY_EMAIL', 'Email triggered from Cron to school who have set up there account properly.\r\nEmail only.', 'Cron runs everyday at 3:00 am (Indian time). \r\nUser gets email once 8th day from school create date if the account set up properly.'),
(15, 'Institute Sign Up', 'school_sign_up', 'DONOT_REPLY_EMAIL', 'Sign up email from school panel.\r\nUsed for email only', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `staff_type` int(11) NOT NULL COMMENT '1=teaching,2=non-teaching',
  `name` varchar(255) NOT NULL,
  `phone_no` bigint(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `file_url` text NOT NULL,
  `is_deleted` int(11) NOT NULL COMMENT '0=not deleted,1=deleted',
  `delete_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`id`, `school_id`, `staff_type`, `name`, `phone_no`, `email`, `file_url`, `is_deleted`, `delete_time`) VALUES
(4, 1, 1, 'Surajit Das', 9474728606, 'surajit@ablion.in', '', 0, '0000-00-00 00:00:00'),
(5, 1, 1, 'SibShankar Mukherjee', 8016308513, 'sibshankar@ablion.in', '', 0, '0000-00-00 00:00:00'),
(6, 1, 1, 'Sankhyadip Chaudhuri', 6294701640, 'sankhya@ablion.in', '', 0, '0000-00-00 00:00:00'),
(7, 1, 1, 'Santanu Banerjee', 8916365771, 'shantanu@ablion.in', '', 0, '0000-00-00 00:00:00'),
(8, 1, 1, 'Biplob Mudi', 9749552668, 'biplob@ablion.in', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/teacher/BiplobMudi_1563886730', 0, '0000-00-00 00:00:00'),
(9, 1, 1, 'Debnath Bhattacharya', 8250736708, 'debnath@ablion.in', '', 0, '0000-00-00 00:00:00'),
(10, 1, 1, 'Kuntal Mukherjee', 8145771526, 'kuntal@ablion.in', '', 0, '0000-00-00 00:00:00'),
(11, 1, 1, 'Aparajita Datta', 9434251530, 'aparajita@ablion.in', '', 0, '0000-00-00 00:00:00'),
(19, 1, 0, 'Shamak Banerjee', 9630258741, 'shamak@ablion.in', '', 0, '0000-00-00 00:00:00'),
(27, 1, 1, 'Subhankar Mandal', 9865889789, 'subha@mail.com', '', 0, '0000-00-00 00:00:00'),
(28, 1, 2, 'Hari Haran', 5598744589, 'harry@mail.com', '', 0, '0000-00-00 00:00:00'),
(30, 22, 1, 'Aparajita Datta', 987456542, 'aparajita@ablion.in', '', 0, '0000-00-00 00:00:00'),
(31, 29, 1, 'Farhan Akhtar', 9968544587, 'farhan@mail.com', '', 0, '0000-00-00 00:00:00'),
(32, 22, 1, 'Shamak Banerjee', 8523699635, 'shamak@ablion.in', '', 0, '0000-00-00 00:00:00'),
(33, 22, 2, 'Tapas Das', 7410212325, 'tapas.das@gmail.com', '', 0, '0000-00-00 00:00:00'),
(34, 29, 1, 'Harsh', 8965887458, 'harsh@mail.com', '', 0, '0000-00-00 00:00:00'),
(35, 23, 1, 'Bikas Gupta', 0, 'abc@abc.com', '', 0, '0000-00-00 00:00:00'),
(36, 29, 2, 'Bhabesh Joshi', 9960235687, 'bhabesh@mail.com', '', 0, '0000-00-00 00:00:00'),
(37, 22, 1, 'Biswajit Das', 6589658965, 'biswajit@ablion.in', '', 0, '0000-00-00 00:00:00'),
(51, 1, 2, 'Sudarshan Jana', 9658745989, 'sudarshan@mail.com', '', 0, '0000-00-00 00:00:00'),
(52, 50, 1, 'gurojit das', 5452353777, 'surajit@ablion.in', '', 0, '0000-00-00 00:00:00'),
(53, 50, 1, 'bilpob da', 8857872787, 'biplob@ablion.in', '', 0, '0000-00-00 00:00:00'),
(54, 50, 1, 'sankhya', 9832776530, 'sankhyadip@ablion.in', '', 0, '0000-00-00 00:00:00'),
(56, 1, 1, 'Abhijit Mandal', 9999988888, 'abhijit@mail.com', '', 0, '0000-00-00 00:00:00'),
(59, 1, 1, 'Ranjan Chatterjee', 9965887458, 'ranjan@mail.com', '', 0, '0000-00-00 00:00:00'),
(60, 1, 1, 'Sanatan Mandal', 9999966666, 'sanatan@mail.com', '', 0, '0000-00-00 00:00:00'),
(67, 66, 1, 'Rafikur Rahaman', 8653694046, 'rafi@ablion.in', '', 0, '0000-00-00 00:00:00'),
(68, 1, 1, 'Santanu Bandhu', 9655874896, 'sbandhu@mail.com', '', 0, '0000-00-00 00:00:00'),
(69, 1, 1, 'Asim Sarkar', 9968744587, 'asim@mail.com', '', 0, '0000-00-00 00:00:00'),
(77, 76, 1, 'Venkatesh', 9434335143, 'venkatesh@mail.com', '', 0, '0000-00-00 00:00:00'),
(90, 1, 1, 'Sadananda Das', 9879879870, 'sad@mail.com', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/teacher/SadanandaDas_1565259947', 0, '0000-00-00 00:00:00'),
(153, 152, 1, 'Rajdeep Mukherjee', 9865478965, 'rajdeep@mail.com', 'https://s3.ap-south-1.amazonaws.com/schoolapp.test/teacher/', 0, '0000-00-00 00:00:00'),
(158, 157, 1, 'Teacher A', 7894561230, 'teacherA@mail.com', '', 0, '0000-00-00 00:00:00'),
(170, 169, 1, 'Anirban Mukherjee', 9749552668, 'biplob06@gmail.com', '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_temp_timetable`
--

CREATE TABLE `tbl_temp_timetable` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `period_start_time` time NOT NULL,
  `period_end_time` time NOT NULL,
  `period_name` varchar(128) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `attendance_class` int(11) NOT NULL COMMENT '1=teacher have to take attendance for this period',
  `status` int(11) NOT NULL,
  `class_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_temp_timetable`
--

INSERT INTO `tbl_temp_timetable` (`id`, `school_id`, `day_id`, `class_id`, `section_id`, `period_start_time`, `period_end_time`, `period_name`, `subject_id`, `teacher_id`, `attendance_class`, `status`, `class_date`) VALUES
(1, 1, 1, 1, 2, '10:00:00', '11:40:00', '1st Period', 1, 4, 1, 1, '2019-07-08'),
(4, 1, 3, 1, 2, '16:30:00', '19:00:00', '6th Period', 5, 4, 0, 1, '2019-08-21'),
(5, 1, 6, 1, 2, '17:00:00', '19:00:00', '7th Period', 7, 8, 0, 1, '2019-09-14'),
(7, 169, 5, 28, 61, '13:30:00', '15:00:00', '2nd Period', 47, 170, 1, 1, '2019-10-18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_terms`
--

CREATE TABLE `tbl_terms` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `term_name` varchar(255) NOT NULL,
  `combined_exam_results` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `combined_result_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_terms`
--

INSERT INTO `tbl_terms` (`id`, `school_id`, `semester_id`, `term_name`, `combined_exam_results`, `combined_result_name`) VALUES
(1, 169, 10, '1st Term1', 1, 'TestTest'),
(2, 169, 10, 'Term2', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timetable`
--

CREATE TABLE `tbl_timetable` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `period_start_time` time NOT NULL,
  `period_end_time` time NOT NULL,
  `period_name` varchar(128) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `attendance_class` int(11) NOT NULL COMMENT '1=teacher have to take attendance for this period',
  `status` int(11) NOT NULL,
  `class_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_timetable`
--

INSERT INTO `tbl_timetable` (`id`, `school_id`, `day_id`, `class_id`, `section_id`, `period_start_time`, `period_end_time`, `period_name`, `subject_id`, `teacher_id`, `attendance_class`, `status`, `class_date`) VALUES
(1, 1, 1, 1, 2, '10:00:00', '11:40:00', '1st Period', 1, 8, 1, 1, '0000-00-00'),
(2, 1, 1, 1, 1, '11:40:00', '12:20:00', '2nd Period', 2, 7, 0, 1, '0000-00-00'),
(3, 1, 1, 1, 1, '12:20:00', '13:00:00', '3rd Period', 7, 4, 0, 1, '0000-00-00'),
(4, 1, 1, 1, 1, '13:00:00', '13:40:00', '4th Period', 3, 6, 0, 1, '0000-00-00'),
(5, 1, 1, 1, 1, '13:40:00', '14:10:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(6, 1, 1, 1, 1, '14:10:00', '14:40:00', '5th Period', 8, 9, 0, 1, '0000-00-00'),
(7, 1, 1, 1, 1, '14:40:00', '15:10:00', '6th Period', 6, 8, 0, 1, '0000-00-00'),
(9, 1, 1, 1, 2, '11:40:00', '12:20:00', '2nd Period', 4, 8, 1, 1, '0000-00-00'),
(10, 1, 1, 1, 2, '12:20:00', '13:00:00', '3rd Period', 3, 7, 0, 1, '0000-00-00'),
(11, 1, 1, 1, 2, '13:00:00', '13:40:00', '4th Period', 1, 11, 0, 1, '0000-00-00'),
(12, 1, 1, 1, 2, '13:40:00', '14:10:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(13, 1, 1, 1, 2, '14:10:00', '14:40:00', '5th Period', 2, 10, 0, 1, '0000-00-00'),
(14, 1, 1, 1, 2, '15:40:00', '16:10:00', '6th Period', 5, 8, 1, 1, '0000-00-00'),
(15, 1, 1, 1, 3, '11:00:00', '11:40:00', '1st Period', 3, 7, 0, 1, '0000-00-00'),
(16, 1, 1, 1, 3, '11:40:00', '12:20:00', '2nd Period', 2, 10, 0, 1, '0000-00-00'),
(17, 1, 1, 1, 3, '12:20:00', '13:00:00', '3rd Period', 1, 11, 0, 1, '0000-00-00'),
(18, 1, 1, 1, 3, '13:00:00', '13:40:00', '4th Period', 7, 4, 0, 1, '0000-00-00'),
(19, 1, 1, 1, 3, '13:40:00', '14:10:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(20, 1, 1, 1, 3, '14:10:00', '14:40:00', '5th Period', 4, 8, 0, 1, '0000-00-00'),
(21, 1, 1, 1, 3, '15:10:00', '15:30:00', 'Sports', 0, 10, 0, 1, '0000-00-00'),
(22, 1, 1, 2, 4, '11:00:00', '11:40:00', 'Geometry', 5, 5, 0, 1, '0000-00-00'),
(23, 1, 1, 2, 4, '11:40:00', '12:20:00', '2nd', 7, 9, 0, 1, '0000-00-00'),
(24, 1, 1, 2, 4, '12:20:00', '13:00:00', '3rd', 4, 10, 0, 1, '0000-00-00'),
(25, 1, 1, 2, 4, '13:00:00', '13:40:00', '4th', 1, 8, 0, 1, '0000-00-00'),
(26, 1, 1, 2, 4, '13:40:00', '14:10:00', 'Lunch', 0, 0, 0, 1, '0000-00-00'),
(27, 1, 1, 2, 4, '14:10:00', '14:40:00', '5th', 8, 6, 0, 1, '0000-00-00'),
(28, 1, 1, 2, 4, '14:40:00', '15:10:00', '6th', 3, 11, 0, 1, '0000-00-00'),
(29, 1, 1, 2, 5, '11:00:00', '11:40:00', '1st', 1, 10, 0, 1, '0000-00-00'),
(30, 1, 1, 2, 5, '11:40:00', '12:20:00', '2nd', 6, 11, 0, 1, '0000-00-00'),
(32, 1, 1, 2, 5, '12:20:00', '13:00:00', '3rd', 4, 6, 0, 1, '0000-00-00'),
(33, 1, 1, 2, 5, '13:00:00', '13:40:00', '4th', 8, 9, 0, 1, '0000-00-00'),
(34, 1, 1, 2, 5, '13:40:00', '14:10:00', 'Lunch', 0, 0, 0, 1, '0000-00-00'),
(35, 1, 1, 2, 5, '14:10:00', '14:40:00', '5th', 1, 11, 0, 1, '0000-00-00'),
(36, 1, 1, 2, 5, '14:40:00', '15:10:00', '6th', 2, 4, 0, 1, '0000-00-00'),
(39, 1, 2, 1, 2, '11:00:00', '11:40:00', '1st', 1, 8, 1, 1, '0000-00-00'),
(40, 1, 2, 1, 2, '11:40:00', '12:20:00', '2nd', 2, 5, 0, 1, '0000-00-00'),
(41, 1, 2, 1, 2, '12:20:00', '13:00:00', '3rd', 3, 6, 0, 1, '0000-00-00'),
(42, 1, 2, 1, 2, '13:00:00', '13:40:00', '4th', 4, 7, 0, 1, '0000-00-00'),
(43, 1, 2, 1, 2, '13:40:00', '14:10:00', 'Lunch', 0, 0, 0, 1, '0000-00-00'),
(44, 1, 2, 1, 2, '14:10:00', '15:10:00', '5th', 5, 8, 0, 1, '0000-00-00'),
(45, 1, 2, 1, 1, '11:00:00', '11:40:00', '1st Period', 6, 10, 0, 1, '0000-00-00'),
(46, 1, 2, 1, 1, '11:40:00', '12:20:00', '2nd Period', 8, 9, 0, 1, '0000-00-00'),
(47, 1, 2, 1, 1, '12:20:00', '13:00:00', '3rd Period', 3, 6, 0, 1, '0000-00-00'),
(48, 1, 2, 1, 1, '13:00:00', '13:40:00', '4th Period', 7, 4, 0, 1, '0000-00-00'),
(49, 1, 2, 1, 1, '13:40:00', '14:10:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(50, 1, 2, 1, 1, '14:10:00', '14:40:00', '5th Period', 2, 7, 0, 1, '0000-00-00'),
(51, 1, 2, 1, 1, '14:40:00', '15:10:00', '6th Period', 1, 8, 1, 1, '0000-00-00'),
(52, 1, 2, 1, 2, '15:10:00', '16:00:00', 'Sports', 0, 8, 1, 1, '0000-00-00'),
(53, 1, 3, 1, 2, '11:00:00', '11:40:00', '1st Period', 1, 8, 1, 1, '0000-00-00'),
(54, 1, 4, 1, 2, '11:00:00', '11:40:00', '1st Period', 7, 8, 1, 1, '0000-00-00'),
(55, 1, 4, 1, 2, '11:40:00', '12:00:00', 'Project Work', 0, 7, 0, 1, '0000-00-00'),
(56, 1, 4, 1, 2, '12:00:00', '12:30:00', '2nd Period', 2, 4, 0, 1, '0000-00-00'),
(57, 1, 4, 1, 2, '12:30:00', '13:00:00', '3rd Period', 6, 8, 0, 1, '0000-00-00'),
(58, 1, 4, 1, 2, '13:00:00', '13:40:00', '4th Period', 3, 8, 0, 1, '0000-00-00'),
(59, 1, 4, 1, 2, '13:40:00', '14:10:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(60, 1, 4, 1, 2, '14:10:00', '14:40:00', '5th Period', 5, 8, 0, 1, '0000-00-00'),
(61, 1, 4, 1, 2, '14:40:00', '19:00:00', 'Physical Training (P.T)', 0, 8, 1, 1, '0000-00-00'),
(62, 1, 3, 1, 2, '11:40:00', '12:20:00', '2th Period', 2, 8, 0, 1, '0000-00-00'),
(63, 1, 3, 1, 2, '12:20:00', '13:35:00', '3rd Period', 3, 8, 0, 1, '0000-00-00'),
(64, 1, 3, 1, 2, '13:00:00', '14:30:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(65, 1, 3, 1, 2, '14:30:00', '16:30:00', '5th Period', 4, 8, 1, 1, '0000-00-00'),
(66, 1, 5, 1, 2, '10:00:00', '11:40:00', '1st Period', 1, 8, 1, 1, '0000-00-00'),
(67, 1, 5, 1, 2, '11:40:00', '12:20:00', '2nd Period', 2, 8, 0, 1, '0000-00-00'),
(68, 1, 5, 1, 2, '12:20:00', '13:00:00', '3rd Period', 3, 8, 0, 1, '0000-00-00'),
(69, 1, 5, 1, 2, '13:00:00', '13:40:00', '4th Period', 4, 8, 0, 1, '0000-00-00'),
(70, 1, 5, 1, 2, '13:40:00', '14:10:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(71, 1, 5, 1, 2, '14:10:00', '14:50:00', '5th Period', 5, 8, 0, 1, '0000-00-00'),
(72, 1, 5, 1, 2, '14:50:00', '15:30:00', '6th Period', 6, 8, 1, 1, '0000-00-00'),
(73, 1, 5, 1, 2, '15:30:00', '16:15:00', '7th Period', 7, 8, 0, 1, '0000-00-00'),
(74, 1, 6, 1, 2, '10:30:00', '11:10:00', '1st Period', 1, 8, 1, 1, '0000-00-00'),
(75, 1, 6, 1, 2, '11:40:00', '12:30:00', '2nd Period', 2, 8, 0, 1, '0000-00-00'),
(76, 1, 6, 1, 2, '12:30:00', '13:10:00', '3rd Period', 3, 8, 0, 1, '0000-00-00'),
(77, 1, 6, 1, 2, '13:10:00', '13:40:00', '4th Period', 4, 8, 0, 1, '0000-00-00'),
(78, 1, 6, 1, 2, '13:40:00', '14:15:00', 'Lunch Break', 0, 0, 0, 1, '0000-00-00'),
(79, 1, 6, 1, 2, '14:15:00', '15:30:00', '5th Period', 5, 8, 0, 1, '0000-00-00'),
(80, 1, 6, 1, 2, '15:30:00', '17:00:00', '6th Period', 6, 8, 0, 1, '0000-00-00'),
(81, 1, 6, 1, 2, '17:00:00', '19:00:00', '7th Period', 7, 4, 0, 1, '0000-00-00'),
(82, 1, 6, 4, 11, '13:00:00', '13:40:00', '4th', 7, 11, 1, 1, '0000-00-00'),
(83, 1, 6, 4, 10, '14:10:00', '14:40:00', '5th', 3, 11, 0, 1, '0000-00-00'),
(84, 1, 6, 3, 8, '14:40:00', '15:10:00', '6th', 7, 11, 0, 1, '0000-00-00'),
(85, 1, 6, 3, 7, '15:10:00', '15:40:00', '7th', 7, 11, 0, 1, '0000-00-00'),
(86, 1, 3, 1, 2, '16:30:00', '19:00:00', '6th Period', 5, 8, 0, 1, '0000-00-00'),
(87, 1, 5, 6, 13, '14:10:00', '14:50:00', '5th', 5, 11, 1, 1, '0000-00-00'),
(88, 1, 4, 1, 2, '19:00:00', '19:30:00', '6th Period', 1, 8, 0, 1, '0000-00-00'),
(89, 1, 4, 1, 2, '19:30:00', '20:30:00', '7th Period', 2, 8, 0, 1, '0000-00-00'),
(90, 1, 4, 1, 2, '20:00:00', '20:30:00', '8th Period', 3, 8, 0, 1, '0000-00-00'),
(91, 1, 4, 1, 2, '20:30:00', '21:00:00', '9th Period', 4, 8, 0, 1, '0000-00-00'),
(92, 23, 1, 9, 15, '10:00:00', '10:40:00', '1st Period', 9, 35, 1, 1, '0000-00-00'),
(93, 23, 1, 9, 15, '11:00:00', '11:40:00', '2nd Period', 13, 35, 0, 1, '0000-00-00'),
(94, 23, 1, 9, 15, '12:00:00', '12:40:00', '3rd Period', 12, 35, 0, 1, '0000-00-00'),
(95, 22, 1, 7, 29, '10:00:00', '11:00:00', '1st Period', 14, 32, 1, 1, '0000-00-00'),
(97, 22, 1, 7, 29, '12:00:00', '13:00:00', '3rd Period', 18, 37, 0, 1, '0000-00-00'),
(98, 22, 1, 7, 29, '13:00:00', '13:30:00', 'LUNCH', 0, 0, 0, 1, '0000-00-00'),
(100, 22, 1, 7, 29, '14:30:00', '15:30:00', '5th Period', 25, 30, 1, 1, '0000-00-00'),
(101, 22, 1, 7, 29, '15:30:00', '16:30:00', '6th Period', 27, 37, 1, 1, '0000-00-00'),
(102, 22, 2, 7, 29, '10:00:00', '11:00:00', '1st Period', 15, 37, 1, 1, '0000-00-00'),
(103, 22, 1, 7, 29, '11:00:00', '12:00:00', '2nd Period', 16, 30, 0, 1, '0000-00-00'),
(104, 22, 2, 7, 29, '11:00:00', '12:00:00', '2nd Period', 15, 37, 0, 1, '0000-00-00'),
(105, 22, 2, 7, 29, '12:00:00', '13:00:00', '3rd Period', 16, 30, 1, 1, '0000-00-00'),
(106, 22, 2, 7, 29, '13:00:00', '13:30:00', 'LUNCH', 0, 0, 0, 1, '0000-00-00'),
(107, 22, 1, 7, 29, '13:30:00', '14:30:00', '4th Period', 22, 30, 0, 1, '0000-00-00'),
(108, 22, 2, 7, 29, '13:30:00', '14:30:00', '4th Period', 22, 30, 0, 1, '0000-00-00'),
(109, 22, 2, 7, 29, '14:30:00', '15:30:00', '5th Period', 23, 32, 0, 1, '0000-00-00'),
(110, 22, 2, 7, 29, '15:30:00', '16:30:00', '6th Period', 23, 32, 0, 1, '0000-00-00'),
(111, 22, 5, 7, 29, '10:00:00', '11:00:00', '1St Period', 16, 30, 1, 1, '0000-00-00'),
(112, 22, 5, 7, 29, '11:00:00', '12:00:00', '2nd Period', 16, 30, 0, 1, '0000-00-00'),
(113, 22, 5, 7, 29, '12:00:00', '13:30:00', '3rd Period', 22, 30, 0, 1, '0000-00-00'),
(114, 22, 5, 7, 29, '14:00:00', '15:00:00', '4th Period', 14, 32, 0, 1, '0000-00-00'),
(115, 23, 5, 9, 15, '12:00:00', '13:00:00', '2nd Period', 10, 35, 1, 1, '0000-00-00'),
(116, 23, 5, 9, 15, '13:00:00', '13:40:00', 'Tiffin', 0, 0, 0, 1, '0000-00-00'),
(117, 22, 3, 7, 29, '10:00:00', '11:00:00', '1st Period', 22, 30, 1, 1, '0000-00-00'),
(118, 22, 3, 7, 29, '11:00:00', '12:00:00', '2nd Period', 14, 32, 0, 1, '0000-00-00'),
(119, 22, 3, 7, 29, '12:00:00', '13:00:00', '3rd Period', 15, 37, 0, 1, '0000-00-00'),
(120, 22, 1, 7, 29, '13:30:00', '15:00:00', '4th Period', 16, 30, 0, 1, '0000-00-00'),
(121, 22, 3, 7, 29, '13:30:00', '15:30:00', '4th Period', 16, 30, 0, 1, '0000-00-00'),
(122, 22, 3, 12, 31, '15:30:00', '16:30:00', '6th Period', 25, 30, 0, 1, '0000-00-00'),
(123, 50, 1, 19, 36, '10:10:00', '10:55:00', 'gonit', 34, 52, 1, 1, '0000-00-00'),
(124, 50, 2, 20, 40, '11:20:00', '12:30:00', 'biggan', 35, 54, 0, 1, '0000-00-00'),
(125, 50, 3, 20, 41, '11:50:00', '12:40:00', 'bangla', 39, 53, 0, 1, '0000-00-00'),
(126, 22, 4, 7, 29, '10:00:00', '11:00:00', '1st Period', 25, 30, 1, 1, '0000-00-00'),
(127, 22, 4, 12, 31, '11:00:00', '12:00:00', '2nd Period', 25, 30, 0, 1, '0000-00-00'),
(128, 22, 4, 7, 29, '12:00:00', '14:00:00', '3rd Period', 22, 30, 0, 1, '0000-00-00'),
(129, 22, 5, 12, 31, '17:20:00', '18:20:00', '5th Period', 25, 30, 1, 1, '0000-00-00'),
(130, 50, 1, 20, 40, '06:00:00', '06:00:00', 'NBV', 38, 54, 0, 1, '0000-00-00'),
(131, 50, 1, 21, 46, '06:00:00', '06:00:00', 'OI', 37, 54, 1, 1, '0000-00-00'),
(132, 50, 5, 20, 43, '06:00:00', '06:00:00', 'UIUY', 42, 53, 0, 1, '0000-00-00'),
(133, 50, 1, 20, 44, '06:00:00', '06:00:00', 'CSGVBSG', 35, 54, 0, 1, '0000-00-00'),
(134, 50, 1, 20, 39, '06:00:00', '06:00:00', 'DG', 40, 54, 0, 1, '0000-00-00'),
(135, 1, 3, 1, 1, '11:00:00', '12:30:00', '1st Period', 1, 4, 1, 1, '0000-00-00'),
(136, 1, 3, 1, 2, '19:10:00', '20:00:00', 'Extra Class', 3, 8, 0, 1, '0000-00-00'),
(137, 1, 5, 1, 2, '19:00:00', '20:30:00', 'last period', 3, 8, 0, 1, '0000-00-00'),
(138, 1, 5, 1, 2, '11:00:00', '11:40:00', '1st Period', 2, 56, 1, 1, '0000-00-00'),
(139, 1, 5, 2, 4, '12:15:00', '13:15:00', '2nd Period', 4, 68, 0, 1, '0000-00-00'),
(140, 1, 1, 6, 13, '11:00:00', '11:40:00', '1st Period', 1, 4, 1, 1, '0000-00-00'),
(141, 1, 7, 1, 1, '11:00:00', '11:45:00', '1st Period', 8, 11, 0, 1, '0000-00-00'),
(142, 22, 6, 7, 29, '11:00:00', '11:40:00', '1st Period', 33, 30, 0, 1, '0000-00-00'),
(143, 22, 6, 7, 29, '11:46:00', '12:30:00', '2nd Period', 27, 30, 0, 1, '0000-00-00'),
(144, 157, 3, 25, 58, '11:00:00', '13:30:00', '1st Period', 44, 158, 1, 1, '0000-00-00'),
(145, 157, 3, 25, 58, '13:30:00', '16:00:00', '2nd Period', 45, 158, 0, 1, '0000-00-00'),
(146, 157, 3, 25, 58, '16:00:00', '19:00:00', '3rd Period', 46, 158, 0, 1, '0000-00-00'),
(147, 169, 4, 28, 61, '18:00:00', '19:00:00', '1st Period', 47, 170, 1, 1, '0000-00-00'),
(148, 169, 5, 28, 61, '13:30:00', '15:00:00', '2nd Period', 47, 170, 1, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_permissions`
--

CREATE TABLE `tbl_user_permissions` (
  `id` bigint(20) NOT NULL,
  `uid` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  `allowed_type` tinyint(1) NOT NULL COMMENT '1=allowed,2=not-allowed'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_permissions`
--

INSERT INTO `tbl_user_permissions` (`id`, `uid`, `permission`, `allowed_type`) VALUES
(22, 4, 7, 1),
(21, 4, 6, 1),
(20, 4, 5, 1),
(19, 4, 4, 1),
(18, 4, 3, 1),
(17, 4, 2, 1),
(16, 4, 1, 1),
(8, 8, 1, 1),
(9, 8, 2, 1),
(10, 8, 3, 1),
(11, 8, 4, 1),
(12, 8, 5, 1),
(13, 8, 6, 1),
(14, 8, 7, 1),
(15, 8, 8, 1),
(23, 4, 8, 1),
(24, 30, 1, 2),
(25, 30, 2, 2),
(26, 30, 3, 2),
(27, 30, 4, 2),
(28, 30, 5, 1),
(29, 30, 6, 2),
(30, 30, 7, 2),
(31, 30, 8, 2),
(32, 153, 1, 1),
(33, 153, 2, 1),
(34, 153, 3, 1),
(35, 153, 4, 1),
(36, 153, 5, 1),
(37, 153, 6, 1),
(38, 153, 7, 1),
(39, 153, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verification_token`
--

CREATE TABLE `tbl_verification_token` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expiry_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` tinyint(1) NOT NULL COMMENT '1=email, 2=forgetpassword'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_verification_token`
--

INSERT INTO `tbl_verification_token` (`id`, `login_id`, `token`, `expiry_time`, `type`) VALUES
(6, 172, 'f20b9ceda238c00c0ef739fb349d57608d9f6679', '2019-10-19 10:58:07', 1),
(7, 173, 'f34c73c722326468d47486ca5c7bc910f3e53deb', '2019-10-19 12:18:35', 1),
(8, 174, '57ea397db68b767b2519e3c3a581a242e3ccf91a', '2019-10-19 12:49:48', 1),
(9, 174, 'c009cf2df228673204020ee4711ee99802c4443b', '2019-10-19 12:57:36', 1),
(10, 174, '1a7b2b3b55d875619978fcc30a88043df9f902aa', '2019-10-19 12:58:46', 1),
(11, 174, '4cf900dd5a2fba5a0309cbc40e028d5239cd74bc', '2019-10-19 13:00:39', 1),
(14, 174, 'cb5ad8a9189ffeb409389e2e9265e1a3d6587e00', '2019-10-23 11:19:12', 1),
(15, 174, '204044bf13eddf95d2a84484b4c7cfe9b660ef19', '2019-10-23 11:20:50', 1),
(16, 174, '5ae8df0daef87f36df33e921c44c276410ff6c3c', '2019-10-23 11:22:11', 1),
(17, 177, '212e722de2b2a79fdb492c243fe1b1ed951f6f74', '2019-10-23 16:49:20', 1),
(18, 178, '53ce24bb77b3a2a27297235bc40c66fd344ad75e', '2019-10-24 17:29:20', 1),
(19, 178, '65c1f16e70c9264234cd11014d54086284d9d08b', '2019-10-24 17:33:11', 1),
(20, 178, 'c3ae26c5521af0f27d271513b136874876afb961', '2019-10-24 19:52:52', 1),
(21, 178, '16842b056a0c6d92f8199c3d19a830d7762bab6b', '2019-10-24 19:54:12', 1),
(22, 23, '30e100b78da3602f2fa839f1caf9e3be02171a09', '2019-10-24 20:03:01', 1),
(23, 179, '9af1a7f1768121c47a8a478d1780fbc7905af1b3', '2019-10-25 11:57:47', 1),
(24, 179, '32546a344c3801ac4a0560079b27c7f5e642218f', '2019-10-25 11:58:32', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `tbl_admin_files`
--
ALTER TABLE `tbl_admin_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_api_log`
--
ALTER TABLE `tbl_api_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`user_id`),
  ADD KEY `transaction_time` (`transaction_time`);

--
-- Indexes for table `tbl_app_maintenance`
--
ALTER TABLE `tbl_app_maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `attendance_status` (`attendance_status`);

--
-- Indexes for table `tbl_ci_sessions`
--
ALTER TABLE `tbl_ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_activity_idx` (`timestamp`);

--
-- Indexes for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_name` (`class_name`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_common_login`
--
ALTER TABLE `tbl_common_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `otp_sending_time` (`otp_sending_time`),
  ADD KEY `otp` (`otp`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `tbl_delete_files`
--
ALTER TABLE `tbl_delete_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_diary`
--
ALTER TABLE `tbl_diary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `issue_date` (`issue_date`);

--
-- Indexes for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `exam_start_date` (`exam_start_date`),
  ADD KEY `exam_end_date` (`exam_end_date`),
  ADD KEY `publish_date` (`publish_date`);

--
-- Indexes for table `tbl_exam_scores`
--
ALTER TABLE `tbl_exam_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `exam_id` (`exam_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `tbl_fees_payment`
--
ALTER TABLE `tbl_fees_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `fees_breakup_id` (`fees_breakup_id`);

--
-- Indexes for table `tbl_fees_structure`
--
ALTER TABLE `tbl_fees_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `class_id` (`semester_id`);

--
-- Indexes for table `tbl_fees_structure_breakups`
--
ALTER TABLE `tbl_fees_structure_breakups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`fees_id`);

--
-- Indexes for table `tbl_fees_structure_breakups_manual`
--
ALTER TABLE `tbl_fees_structure_breakups_manual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `child_id` (`student_id`),
  ADD KEY `class_id` (`semester_id`),
  ADD KEY `breakup_id` (`breakup_id`);

--
-- Indexes for table `tbl_general_info`
--
ALTER TABLE `tbl_general_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_holiday`
--
ALTER TABLE `tbl_holiday`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_note`
--
ALTER TABLE `tbl_note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `period_id` (`period_id`),
  ADD KEY `note_issuetime` (`note_issuetime`);

--
-- Indexes for table `tbl_note_files`
--
ALTER TABLE `tbl_note_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `tbl_notice`
--
ALTER TABLE `tbl_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `publish_date` (`publish_date`);

--
-- Indexes for table `tbl_notification_settings`
--
ALTER TABLE `tbl_notification_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `notification_type` (`notification_type`);

--
-- Indexes for table `tbl_parents`
--
ALTER TABLE `tbl_parents`
  ADD PRIMARY KEY (`table_id`),
  ADD KEY `id` (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `tbl_parents_kids_link`
--
ALTER TABLE `tbl_parents_kids_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `school_id` (`school_id`) USING BTREE;

--
-- Indexes for table `tbl_paymentgateway`
--
ALTER TABLE `tbl_paymentgateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_payments_to_school`
--
ALTER TABLE `tbl_payments_to_school`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- Indexes for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_register_device`
--
ALTER TABLE `tbl_register_device`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`user_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `tbl_schools`
--
ALTER TABLE `tbl_schools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `contact_person_name` (`contact_person_name`),
  ADD KEY `mobile_no` (`mobile_no`),
  ADD KEY `email_address` (`email_address`);

--
-- Indexes for table `tbl_school_payment_settings`
--
ALTER TABLE `tbl_school_payment_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_section`
--
ALTER TABLE `tbl_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_semester`
--
ALTER TABLE `tbl_semester`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_id` (`session_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `from_dsate` (`from_date`),
  ADD KEY `to_date` (`to_date`);

--
-- Indexes for table `tbl_sms_credits`
--
ALTER TABLE `tbl_sms_credits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `registration_no` (`registration_no`),
  ADD KEY `name` (`name`),
  ADD KEY `mother_name` (`mother_name`),
  ADD KEY `father_name` (`father_name`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `roll_no` (`roll_no`),
  ADD KEY `delete_time` (`delete_time`);

--
-- Indexes for table `tbl_student_details`
--
ALTER TABLE `tbl_student_details`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `tbl_student_semester_link`
--
ALTER TABLE `tbl_student_semester_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `semester_id` (`semester_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_syllabus`
--
ALTER TABLE `tbl_syllabus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `tbl_system_emails`
--
ALTER TABLE `tbl_system_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`);

--
-- Indexes for table `tbl_temp_timetable`
--
ALTER TABLE `tbl_temp_timetable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `day_id` (`day_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `tbl_terms`
--
ALTER TABLE `tbl_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `semester_id` (`semester_id`);

--
-- Indexes for table `tbl_timetable`
--
ALTER TABLE `tbl_timetable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `day_id` (`day_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `tbl_user_permissions`
--
ALTER TABLE `tbl_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`,`permission`);

--
-- Indexes for table `tbl_verification_token`
--
ALTER TABLE `tbl_verification_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`login_id`),
  ADD KEY `expiry_time` (`expiry_time`),
  ADD KEY `token` (`token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_admin_files`
--
ALTER TABLE `tbl_admin_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_api_log`
--
ALTER TABLE `tbl_api_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT for table `tbl_app_maintenance`
--
ALTER TABLE `tbl_app_maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=527;

--
-- AUTO_INCREMENT for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_common_login`
--
ALTER TABLE `tbl_common_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT for table `tbl_delete_files`
--
ALTER TABLE `tbl_delete_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_diary`
--
ALTER TABLE `tbl_diary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_exam_scores`
--
ALTER TABLE `tbl_exam_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_fees_payment`
--
ALTER TABLE `tbl_fees_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1014;

--
-- AUTO_INCREMENT for table `tbl_fees_structure`
--
ALTER TABLE `tbl_fees_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_fees_structure_breakups`
--
ALTER TABLE `tbl_fees_structure_breakups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_fees_structure_breakups_manual`
--
ALTER TABLE `tbl_fees_structure_breakups_manual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_general_info`
--
ALTER TABLE `tbl_general_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_holiday`
--
ALTER TABLE `tbl_holiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_note`
--
ALTER TABLE `tbl_note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `tbl_note_files`
--
ALTER TABLE `tbl_note_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=293;

--
-- AUTO_INCREMENT for table `tbl_notice`
--
ALTER TABLE `tbl_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `tbl_notification_settings`
--
ALTER TABLE `tbl_notification_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT for table `tbl_parents`
--
ALTER TABLE `tbl_parents`
  MODIFY `table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tbl_parents_kids_link`
--
ALTER TABLE `tbl_parents_kids_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `tbl_paymentgateway`
--
ALTER TABLE `tbl_paymentgateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_payments_to_school`
--
ALTER TABLE `tbl_payments_to_school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_register_device`
--
ALTER TABLE `tbl_register_device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_school_payment_settings`
--
ALTER TABLE `tbl_school_payment_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_section`
--
ALTER TABLE `tbl_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tbl_semester`
--
ALTER TABLE `tbl_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_session`
--
ALTER TABLE `tbl_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_sms_credits`
--
ALTER TABLE `tbl_sms_credits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_student_semester_link`
--
ALTER TABLE `tbl_student_semester_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbl_syllabus`
--
ALTER TABLE `tbl_syllabus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_system_emails`
--
ALTER TABLE `tbl_system_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_temp_timetable`
--
ALTER TABLE `tbl_temp_timetable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_terms`
--
ALTER TABLE `tbl_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_timetable`
--
ALTER TABLE `tbl_timetable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `tbl_user_permissions`
--
ALTER TABLE `tbl_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbl_verification_token`
--
ALTER TABLE `tbl_verification_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
